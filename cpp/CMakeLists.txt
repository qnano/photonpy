cmake_minimum_required(VERSION 3.12 FATAL_ERROR)
#cmake_policy(SET CMP0094 NEW)

option(CUDA_DISABLED "CUDA_DISABLED" OFF)

project(smlmlib LANGUAGES CXX)

include(CheckLanguage)

if(NOT CUDA_DISABLED)
	check_language(CUDA)
endif()

#set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

set(EXECUTABLE_OUTPUT_PATH "smlmlib/bin")

#find_package(Python COMPONENTS Interpreter Development)
#find_package(pybind11 REQUIRED)
#find_package(CUDA)

# This is used on linux but ignored by the visual studio builds
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin/release)


set(SourceFiles 
	Context.cpp 
#	CameraCalibration.cu 
#	ContinuousFRC.cu
	DebugImageCallback.cpp
#	DriftEstimation.cu
	CudaUtils.cu
#	ImageProcessor.cpp
#	ImgFilterQueue.cu
	LinkLocalizations.cpp
	Rendering.cpp
#	ROIQueue.cpp
	StringUtils.cpp
#	FFT.cpp
#	simflux/SIMFLUX.cu
#	SpotDetection/PSFCorrelationSpotDetector.cu
#	SpotDetection/SpotDetector.cu
	Estimators/EstimationQueue.cpp
	Estimators/EstimatorImpl.cu
	Estimators/Estimator.cpp
	Estimators/Gaussian/GaussianPSF.cu
	Estimators/CSpline/CubicSplinePSF.cu
	)

list(TRANSFORM SourceFiles PREPEND "SMLMLib/")

add_library(smlmlib SHARED ${SourceFiles} )
target_include_directories(smlmlib PUBLIC SMLMLib) #${CMAKE_BINARY_DIR})
target_compile_definitions(smlmlib PUBLIC SMLM_EXPORTS)
target_compile_features(smlmlib PUBLIC cxx_std_14)
set_property(TARGET smlmlib PROPERTY POSITION_INDEPENDENT_CODE ON)

#message(STATUS "CMAKE_BINARY_DIR: ${CMAKE_BINARY_DIR}")

if(CMAKE_CUDA_COMPILER AND NOT CUDA_DISABLED)
	enable_language(CUDA)
	
	#if (UNIX)
			# cuda compiler path for linux, as cmake cant find it
			#	set(CMAKE_CUDA_COMPILER /usr/local/cuda/bin/nvcc)
			#	endif (UNIX)

   enable_language(CUDA)
   add_compile_definitions(CUDA_ENABLED)

   set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --gpu-architecture=sm_50 --expt-extended-lambda")
   #message(WARNING "No CUDA compiler found or disabled by user defined command line variable '-DCUDA_DISABLED=True'.
   #You may need to specify its path like \t'export CUDACXX=/usr/local/cuda/bin/nvcc'")

	message("Targeting CUDA build...")


	#target_compile_features(dme PUBLIC cxx_std_11)

   #add_library(dme STATIC src/DriftEstimation.cu)
   #target_include_directories(dme PUBLIC include)
   set_target_properties(smlmlib PROPERTIES CUDA_SEPARABLE_COMPILATION ON POSITION_INDEPENDENT_CODE ON)
   set_target_properties(smlmlib PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELEASE "bin/release/")
   set_target_properties(smlmlib PROPERTIES RUNTIME_OUTPUT_DIRECTORY_DEBUG "bin/debug/")
   set_property(TARGET smlmlib PROPERTY CUDA_ARCHITECTURES 72 80)
   target_compile_options(smlmlib PUBLIC $<$<COMPILE_LANGUAGE:CUDA>:  # seems to be unnecssary but keep the code block for the moment
                        --expt-relaxed-constexpr
			--expt-extended-lambda
			-Xptxas -O3 -Xcompiler -O2
                         >)
                        # -rdc=true
   set(CUDA_NVCC_FLAGS ${CUDA_NVCC_FLAGS} -use_fast_math)                     # --gpu-architecture=sm_50

   link_directories(/usr/local/cuda/lib64)
   #include_directories("${CUDA_INCLUDE_DIRS}")

   #pybind11_add_module(spline src/pybind_spline.cpp)
   #target_link_libraries(dme PRIVATE dme_cu_impl dme_cpu_impl)

else()  # NO CUDA
	#target_include_directories(dme_cpu_impl PUBLIC include)

	message("Targeting non-CUDA build...")

	message(WARNING "No CUDA compiler found or disabled by user defined command line variable '-DCUDA_DISABLED=True'.
		In case you want to use CUDA, you may need to specify its path like \t'export CUDACXX=/usr/local/cuda/bin/nvcc'")

   #pybind11_add_module(spline src/pybind_spline.cpp)

	add_library(dspline_cpu SHARED ${FILES})
	set_target_properties(dspline_cpu PROPERTIES RUNTIME_OUTPUT_DIRECTORY_RELEASE "bin/release/")
	set_target_properties(dspline_cpu PROPERTIES RUNTIME_OUTPUT_DIRECTORY_DEBUG "bin/debug/")
	target_compile_features(dspline_cpu PUBLIC cxx_std_14)
	set_property(TARGET dspline_cpu PROPERTY POSITION_INDEPENDENT_CODE ON)
	target_compile_definitions(dspline_cpu PUBLIC SMLM_EXPORTS)

endif()

