#include <array>
#include <tuple>
#include <cassert>

#include "SolveMatrix.h"
#include "MathUtils.h"
#include "ThreadUtils.h"
const float pi = 3.141592653589793f;

void cudaInit();

#include "Estimators/CSpline/CubicSplinePSF.h"
#include "DebugImageCallback.h"
#include "TIFFReadWrite.h"

#include "StringUtils.h"
#include "CudaUtils.h"
#include <stdio.h>

#include "RandomDistributions.h"
#include "Estimators/Estimator.h"
#include "Estimators/Gaussian/GaussianPSF.h"

#include "GetPreciseTime.h"

#include "ROIQueue.h"
#include "palala.h"

struct Limit {
	float min, max;
};

template<typename TModel, typename SampleIndexer>
__device__ void ComputeLevMarAlphaBeta(float* alpha, float* beta, const float *theta,
	SampleIndexer sample, const TModel& model)
{
	const int K = model.K;
	model.ComputeDerivatives([&](int smpIndex, float mu, const float(&jacobian)[K]) {
		float sampleValue = sample(smpIndex);
		if (sampleValue < 1e-6f) sampleValue = 1e-6f;

		float mu_c = mu > 1e-6f ? mu : 1e-6f;
		float invmu = 1.0f / mu_c;
		float x_f2 = sampleValue * invmu * invmu;

		for (int i = 0; i < K; i++)
			for (int j = i; j < K; j++)
				alpha[K * i + j] += jacobian[i] * jacobian[j] * x_f2;

		float beta_factor = 1 - sampleValue * invmu;
		for (int i = 0; i < K; i++) {
			beta[i] -= beta_factor * jacobian[i];
		}
	}, theta);

	// fill below diagonal
	for (int i = 1; i < K; i++)
		for (int j = 0; j < i; j++)
			alpha[K * i + j] = alpha[K * j + i];
}

template<typename TModel, typename Indexer>
PLL_DEVHOST void Iterate(const TModel& model, Indexer sample, float* theta, Limit* limits)
{
	const float lambdaStep = 0.1f;
	typedef float T;
	const int K = TModel::K;
	T alpha[K * K] = {};
	T LU[K * K] = {};
	T beta[K] = {};
	T step[K];

	ComputeLevMarAlphaBeta(alpha, beta, theta, sample, model);

	for (int k = 0; k < K; k++)
		alpha[k * K + k] *= 1 + lambdaStep;

	if (!Cholesky<float,K>(alpha, LU))
		return;
	if (!SolveCholesky(LU, beta, step))
		return;

	for (int k = 0; k < K; k++) {
		float t = theta[k];

		t += step[k];
		t = fmax(limits[k].min, t);
		t = fmin(limits[k].max, t);
		theta[k] = t;
	}

}

struct Gauss2D_Model_XYIBg
{
	int w, h;
	float2 sigma;

	typedef float T;
	enum { K = 4 };

	template<typename TCallback>
	PLL_DEVHOST void ComputeDerivatives(TCallback cb, const float* theta) const
	{
		const T OneOverSqrt2PiSigmaX = 1.0f / (sqrtf(2 * MATH_PI) * sigma.x);
		const T OneOverSqrt2SigmaX = 1.0f / (sqrtf(2) * sigma.x);
		const T OneOverSqrt2PiSigmaY = 1.0f / (sqrtf(2 * MATH_PI) * sigma.y);
		const T OneOverSqrt2SigmaY = 1.0f / (sqrtf(2) * sigma.y);

		T tx = theta[0];
		T ty = theta[1];
		T tI = theta[2];
		T tbg = theta[3];

		for (int y = 0; y < h; y++)
		{
			T Yexp0 = (y - ty + .5f) * OneOverSqrt2SigmaY;
			T Yexp1 = (y - ty - .5f) * OneOverSqrt2SigmaY;
			T DeltaY = 0.5f * erf(Yexp0) - 0.5f * erf(Yexp1);
			T dEy = OneOverSqrt2PiSigmaY * (exp(-Yexp1 * Yexp1) - exp(-Yexp0 * Yexp0));
			for (int x = 0; x < w; x++)
			{
				T Xexp0 = (x - tx + .5f) * OneOverSqrt2SigmaX;
				T Xexp1 = (x - tx - .5f) * OneOverSqrt2SigmaX;
				T DeltaX = 0.5f * erf(Xexp0) - 0.5f * erf(Xexp1);
				T dEx = OneOverSqrt2PiSigmaX * (exp(-Xexp1 * Xexp1) - exp(-Xexp0 * Xexp0));

				T mu = tbg + tI * DeltaX * DeltaY;
				T dmu_dx = tI * dEx * DeltaY;
				T dmu_dy = tI * dEy * DeltaX;
				T dmu_dI0 = DeltaX * DeltaY;
				T dmu_dIbg = 1;
				const T jacobian[] = { dmu_dx, dmu_dy, dmu_dI0, dmu_dIbg };
				cb(y * w + x, mu, jacobian);
			}
		}
	}
};

template<typename Indexer>
void Localize(int n, float sigma, int roisize, Indexer sampleData, const float* d_initial, float* d_theta, int numIterations)
{
	LaunchKernel(n, [=]__device__(int i) {
		const float b = 1.0f;
		Gauss2D_Model_XYIBg model = { roisize,roisize,{sigma,sigma} };

		Limit limits[] = {
			{ b,roisize - b - 1.0f},
			{b,roisize - b - 1.0f},
			{10,1e8f},
			{0.01f,1e8f}
		};

		float theta[4] = { roisize * 0.5f,roisize * 0.5f, 1000, 1.0f };

		if (d_initial) {
			for (int j = 0; j < 4; j++)
				theta[j] = d_initial[i * 4 + j];
		}

		for (int it = 0; it < numIterations; it++)
			Iterate(model, sampleData(i), theta, limits);

		for (int j = 0; j < 4; j++)
			d_theta[i * 4 + j] = theta[j];
	});
}


void debugImgCallback(int w, int h, int numImg, const float* data, const char *label)
{
	DebugPrintf("writing debug image %s (%d x %d)\n", label, w, h);

	if (numImg == 1) {
		WriteTIFF(SPrintf("%s.tiff", label).c_str(), data, w, h, false);
	}
	else {
		for (int i = 0; i < numImg; i++) {
			WriteTIFF(SPrintf("%s_%04d.tiff", label, i).c_str(), &data[i * w * h], w, h, false);
		}
	}
}

std::vector<float> transpose(std::vector<float> src, int h)
{
	size_t w = src.size() / h;
	std::vector<float> r(src.size());

	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			r[j * h + i] = src[i * w + j];

	return r;
}

struct TransposeIndexer
{
	float* data;
	int pitchInElems;

	TransposeIndexer(DeviceImage<float>& img) : data(img.data) , pitchInElems(img.PitchInPixels())
	{}

	struct Dim2Indexer {
		float* data;
		int pitch;

		__device__ float& operator()(int i) const { return data[i * pitch]; }
	};

	__device__ Dim2Indexer operator()(int i)const { return Dim2Indexer{ data + i, pitchInElems }; }
};

struct ImageIndexer
{
	float* data;
	int pitchInElems;

	ImageIndexer(DeviceImage<float>& img) : data(img.data), pitchInElems(img.PitchInPixels())
	{}

	struct Dim2Indexer {
		float* data;
		__device__ float& operator()(int i) const { return data[i]; }
	};

	__device__ Dim2Indexer operator()(int i) const { return Dim2Indexer{ &data[i * pitchInElems] }; }
};



template<typename T, int ND>
struct ArrayAccess
{
	T* data;
	int pitch[ND-1];
	int dims[ND];

	ArrayAccess(DeviceImage<T>& img)
	{
		data = img.data;
		pitch[0] = img.PitchInPixels();
		dims[0] = img.height;
		dims[1] = img.width;
	}

	struct Indexer
	{
	};

	Indexer operator[](int i)
	{

	}
};

template<typename T, int TILE_DIM, int BLOCK_ROWS>
__global__ void ArrayTransposeKernel(ArrayAccess<T, 2> input, ArrayAccess<T, 2> output)
{
	int x = threadIdx.x + TILE_DIM * blockIdx.x;
	int y = threadIdx.y + TILE_DIM * blockIdx.y;

	__shared__ T tile[TILE_DIM][TILE_DIM + 1];
	if (x < input.dims[1]) {
		for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS) {
			if (y + j >= input.dims[0]) break;
			tile[threadIdx.y + j][threadIdx.x] = input.data[input.pitch[0] * (y + j) + x];
		}
	}

	__syncthreads();

	x = blockIdx.y * TILE_DIM + threadIdx.x;  // transpose block offset
	y = blockIdx.x * TILE_DIM + threadIdx.y;

	if (x < output.dims[1]) {
		for (int j = 0; j < TILE_DIM; j += BLOCK_ROWS) {
			if (y + j >= output.dims[0]) break;
			output.data[(y + j) * output.pitch[0] + x] = tile[threadIdx.x][threadIdx.y + j];
		}
	}
}

template<typename T>
void CudaArrayTranspose2(ArrayAccess<T, 2> input, ArrayAccess<T, 2> output, cudaStream_t stream)
{
	const int TILE_DIM = 32, BLOCK_ROWS=8;
	dim3 numThreads(TILE_DIM, BLOCK_ROWS);
	dim3 numBlocks((input.dims[1] + numThreads.x - 1) / numThreads.x, (input.dims[0] + TILE_DIM - 1) / TILE_DIM);
	ArrayTransposeKernel<T, TILE_DIM, BLOCK_ROWS> <<<numBlocks, numThreads, TILE_DIM* (TILE_DIM + 1) * sizeof(T), stream >>> (input, output);
	ThrowIfCUDAError(cudaGetLastError());
}


template<typename T>
void CudaArrayTranspose(ArrayAccess<T, 2> input, ArrayAccess<T, 2> output, cudaStream_t stream)
{
	LaunchKernel(input.dims[0], input.dims[1], [=]__device__(int y, int x) {
		output.data[output.pitch[0] * x + y] = input.data[input.pitch[0] * y + x];
	}, 0, stream);
}

void testCudaTranspose()
{
	int W = 4000, H = 8000;
	std::vector<float> src(W * H);
	for (int y = 0; y < H; y++)
		for (int x = 0; x < W; x++)
			src[y * W + x] = y * 10 + x;

	std::vector<float> h_result = transpose(src, H);

	DeviceImage<float> image(src.data(), W, H);
	DeviceImage<float> output(H, W);

	int reps = 200;
	double tslow = Profile([&]() {
		for (int i = 0; i < reps; i++)
			CudaArrayTranspose(ArrayAccess<float, 2>(image), ArrayAccess<float, 2>(output), 0);
		cudaStreamSynchronize(0);
	});

	double tfast = Profile([&]() {
		for (int i = 0; i < reps; i++)
			CudaArrayTranspose2(ArrayAccess<float, 2>(image), ArrayAccess<float, 2>(output), 0);
		cudaStreamSynchronize(0);
		});

	DebugPrintf("Slow transpose: %3.2f GPixel/s. Fast transpose: %3.2f GPixel/s\n",
		(double)W * H * reps / tslow * 1e-9f, (double)W * H * reps / tfast * 1e-9f);

	std::vector<float> cmp = output.AsVector();
	for (int y = 0; y < output.height; y++) {
		for (int x = 0; x < output.width; x++)
			if (h_result[y * output.width + x] != cmp[y * output.width + x]) {
				DebugPrintf("[%d,%d]: cpu=%f,  cuda=%f\n", y, x, h_result[y * output.width + x], cmp[y * output.width + x]);
				break;
			}
	}
}


void testROIQueue()
{
	ROIQueue* rq = new ROIQueue({ 1,12,12 });

	int n = 500000;
#ifdef _DEBUG
	n = 100;
#endif
	std::vector<float> roi(12 * 12);

	DebugPrintf("Testing ROIQueue: 1M PushROI:");
	double t= Profile([&]() {
		ParallelFor(10, [&](int j) {
			for (int i = 0; i < n; i++)
				rq->PushROI(roi.data(), i, 0, 0, 0, 1.0f);
		});
	});

	DebugPrintf("%d PushROIs/s", (int)(n * 10 / t));
}

void testAllocs()
{
	int n = 10000000;
	double t = Profile([&]() {
		std::vector< std::vector<int> > list;
		list.reserve(n);
		for (int i = 0; i < n; i++) {
			list.push_back(std::vector<int>(50));
		}
	});
	DebugPrintf("%d std::vector<>/s\n", (int)(n / t));
}

void testFisherInvert()
{

	Vector<float, 16> fi { 1.5367470e+02, 1.4359629e-01, -1.5063307e-02, -6.3911791e+00,
		1.4359629e-01, 1.5385300e+02, -1.4435887e-02, -6.1394849e+00,
		-1.5063307e-02, -1.4435887e-02, 8.5789303e-04, 6.9690019e-02,
		-6.3911791e+00, -6.1394849e+00, 6.9690019e-02, 1.3310702e+01 };

	auto r = InvertMatrix(fi);

	auto crlb=r.diagonal().sqrt();
	DebugPrintf("sx=%f, sy=%f", crlb[0], crlb[1]);
}

void test(const Vector<int, 3>& y) {
	DeviceArray<int> d(3);

	int* v = d.data();

	LaunchKernel(1, [=]__device__(int i) {
		v[0] = y[0];
		v[1] = y[1];
		v[2] = y[2];
	});

	cudaStreamSynchronize(0);

	auto result = d.ToVector();

	PrintVector(result.data(), 3, " %d");
}

int main() {

	SetDebugImageCallback(debugImgCallback);
	cudaInit();

	test(Vector<int,3>{ 1,3,5 });

	return 0;

	int w = 4;
	Int3 s(4, 4, 4);
	std::vector<float> data(s[0]*s[1]*s[2]);
	for (int i = 0; i < data.size(); i++) {
		data[i] = cos(i*0.1f);
	}
	Int3 cs = s - 3;
	std::vector<float> spline(cs[0]*cs[1]*cs[2] * 64);
	//CSpline_Compute(s, data.data(), spline.data());

	DeviceArray<int> test(10);
	parallel_for_cuda(10, PALALA(int i, const int* x) {
		
	}, test);


	for (int i = 0; i < spline.size(); i++)
		DebugPrintf("%f\n", spline[i]);

	//testFisherInvert();

	//testCudaTranspose();
	//testAllocs();
	return 0;
	//testROIQueue();

	int roisize = 16;
	float sigma = 1.8f;
	int n = 20000;
	Estimator* estim = Gauss2D_CreatePSF_XYIBg(roisize, sigma, sigma, true);

#ifdef _DEBUG
	n = 10;
#endif

	std::vector<float> samples(roisize * roisize * n);
	std::vector<Vector4f> params(n);

	for (int i = 0; i < n; i++)
		params[i] = { roisize * 0.5f-1,roisize * 0.5f, 1000, 10 };
	
	estim->ExpectedValue(samples.data(),  (const float*)params.data(), 0, 0, n);

	DebugPrintf("Sampling...");  
	Profile([&]() {
		for (int i = 0; i < n * roisize * roisize; i++)
			samples[i] = rand_poisson<float>(samples[i]);
		});
	DeviceArray<float> d_samples(samples);
	DeviceArray<Vector4f> d_params(params);

	std::vector<Vector4f> initial(n);
	for (int i = 0; i < n; i++)
		initial[i] = { roisize * 0.5f, roisize * 0.5f, 1000, 0.0f };
	DeviceArray<Vector4f> d_initial(initial);

	auto samples_tr = transpose(samples, n);
	DeviceImage<float> d_sampleimg_tr(n, roisize * roisize);
	DeviceImage<float> d_sampleimg(roisize * roisize, n);
	d_sampleimg_tr.CopyFromHost(samples_tr.data());
	d_sampleimg.CopyFromHost(samples.data());

	int numIterations = 20, reps = 100;

	DebugPrintf("Estimating using cuEstimator...");
	double t_ = Profile([&]() {
		cuEstimator* e = estim->Unwrap();
		for (int i = 0; i < reps; i++)
			e->Estimate(d_samples.data(), 0, 0, (const float*)d_initial.data(), (float*)d_params.data(), 0, 0, n, 0, 0, 0);

		cudaStreamSynchronize(0);
	});
	DebugPrintf("cuEstimator: %d spots/s.\n", (int)(n * reps / t_));
	auto h_r = d_params.ToVector();
	for (int i = 0; i < std::min(20, (int)h_r.size()); i++) {
		auto r = h_r[i];
		DebugPrintf("cuEstimator results: [%d] x:%.2f, y: %.2f, I: %.0f, b:%.2f\n", i, r[0], r[1], r[2], r[3]);
	}

	ImageIndexer imgIndexer(d_sampleimg);

	double t1 = Profile([&]() {
		for (int i=0;i<reps;i++) 
			Localize(n, sigma, roisize, imgIndexer, (const float*) d_initial.data(), (float*)d_params.data(), numIterations);
		cudaStreamSynchronize(0);   
	});
	DebugPrintf("Testing roi major: %d spots/s.\n", (int)(n * reps / t1));
	h_r = d_params;
	for (int i = 0; i < std::min(20, (int)h_r.size()); i++) {
		auto r = h_r[i];
		DebugPrintf("[%d] x:%.2f, y: %.2f, I: %.0f, b:%.2f\n", i, r[0], r[1], r[2], r[3]);
	}

	TransposeIndexer transposeIndexer(d_sampleimg_tr);
	double t2 = Profile([&]() {
		for (int i = 0; i < reps; i++)
			Localize(n, sigma, roisize, transposeIndexer, (const float*)d_initial.data(), (float*)d_params.data(), numIterations);
		cudaStreamSynchronize(0);
		});
	DebugPrintf("Testing sample major: %d spots/s.\n", (int)(n * reps / t2));

	h_r = d_params;
	for (int i = 0; i < std::min(20, (int)h_r.size()); i++) {
		auto r = h_r[i];
		DebugPrintf("[%d] x:%.2f, y: %.2f, I: %.0f, b:%.2f\n", i, r[0], r[1], r[2], r[3]);
	}

	delete estim;
	return 0;
}

