// Measure cuda memory use
// 
// photonpy - Single molecule localization microscopy library
// © Jelmer Cnossen 2018-2021
#include "CudaUtils.h"
#include "ThreadUtils.h"

void EmptyKernel(cudaStream_t s) {
	LaunchKernel(1, [=]__device__(int i) {}, 0, s);
}

CDLL_EXPORT int CudaGetNumDevices()
{
	int c;
	cudaGetDeviceCount(&c);
	return c;
}

CDLL_EXPORT bool CudaSetDevice(int index)
{
	return cudaSetDevice(index) == cudaSuccess;
}

CDLL_EXPORT bool CudaGetDeviceInfo(int index, int& numMultiprocessors, char* name, int namelen)
{
	cudaDeviceProp prop;
	if (cudaGetDeviceProperties(&prop, index) != cudaSuccess)
		return false;

	numMultiprocessors = prop.multiProcessorCount;
	strncpy(name, prop.name, namelen);//strcpy_s(name, namelen, prop.name);
	return true;
}


