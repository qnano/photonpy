

#include "MCLocalizer.h"
#include "Estimators/Estimator.h"
#include "ThreadUtils.h"
#include "Context.h"

struct PDF
{
public:
	PDF(const PDFInfo& pdfInfo) : 
		pdf(pdfInfo.pdf, pdfInfo.pdf+pdfInfo.size), 
		start(pdfInfo.start), 
		end(pdfInfo.end) {
	}
	std::vector<float> pdf;
	float start, end;

	float eval(float x) {
		int i = int( (x - start) / (end - start) * pdf.size() + 0.5f);
		if (i < 0 || i >= pdf.size()) return 0.0f;
		return pdf[i];
	}
};

class MCLocalizer  : ContextObject {
public:
	std::vector<float> samples, bg;

	int maxEmitters, numFrames;
	Estimator* psf;
	PDF intensityPDF;

	MCLocalizer(int maxEmitters, int nframes, Estimator* psf, const PDFInfo* intensityPDF, Context* ctx) :
		ContextObject(ctx),
		maxEmitters(maxEmitters),
		numFrames(nframes),
		psf(psf),
		intensityPDF(*intensityPDF)
	{
		if (psf->NumParams() != 5)
			throw std::runtime_error("Only 3D PSFs are supported (should have parameters X,Y,Z,I,bg");
	}
	~MCLocalizer() {}

	void SetSamples(const float* data, const float* bg, int numROIs)
	{
		int size = numROIs * numFrames * psf->SampleCount();
		samples.assign(data, data + size);
		if (bg) this->bg.assign(bg, bg + size);
	}

	std::vector<float> SeparateParameters(int numROIs, const float* params)
	{
		std::vector<float> psf_params(numROIs * numFrames * maxEmitters * 5);

		int paramsPerEval = maxEmitters * (3 + numFrames);
		for (int e = 0; e < numROIs; e++) {
			for (int i = 0; i < maxEmitters; i++) {
				const float* src = &params[paramsPerEval * e + (3 + numFrames) * i];
				for (int j = 0; j < numFrames; j++) {
					float* dst = &psf_params[(e * numFrames * maxEmitters + i * numFrames + j) * 5];

					// XYZ
					for (int axis = 0; axis < 3; axis++)
						dst[axis] = src[axis];
					dst[3] = src[3 + j]; // the emitter intensity for frame j
					dst[4] = 0.0f; // background
				}
			}
		}
		return psf_params;
	}

	void ComputeExpVal(float* expval, const float* params, int numEvals)
	{
		std::vector<float> psf_expval(numEvals * numFrames * maxEmitters * psf->SampleCount());
		std::vector<float> psf_params = SeparateParameters(numEvals, params);

		psf->ExpectedValue(psf_expval.data(), psf_params.data(), 0, 0, numEvals * numFrames * maxEmitters);

		int npixels = numFrames * psf->SampleCount();
		ParallelFor(numEvals, [&](int eval) {
			for (int p = 0; p < npixels; p++) {
				float accum = 0.0f;
				for (int em = 0; em < maxEmitters; em++) {
					accum += psf_expval[maxEmitters * npixels * eval + em * npixels + p];
				}
				expval[eval * npixels + p] = accum;
			}
		});
	}

	void ComputeLikelihood(float* ll, const float* params, const float* backgrounds, const int* sampleIndices, int numEvals)
	{
		std::vector<float> psf_expval(numEvals * numFrames * maxEmitters * psf->SampleCount());
		std::vector<float> psf_params = SeparateParameters(numEvals, params);

		psf->ExpectedValue(psf_expval.data(), psf_params.data(), 0, 0, numEvals * numFrames * maxEmitters);

		int pixelsPerEval = psf->SampleCount() * numFrames;

		ParallelFor(numEvals, [&](int eval) {
			float logsum = 0.0f;

			const float* evalSample = &samples[pixelsPerEval * sampleIndices[eval]];
			const float* evalBg = &bg[pixelsPerEval * sampleIndices[eval]];
			const float* evalExpVal = &psf_expval[eval * maxEmitters * pixelsPerEval];

			for (int p = 0; p < pixelsPerEval; p++) {
				float pixelExpVal = evalBg[p] + backgrounds[eval];
				for (int em = 0; em < maxEmitters; em++) 
					pixelExpVal += evalExpVal[pixelsPerEval * em + p];

				// poisson log prob:
				if (pixelExpVal < 1e-10f) pixelExpVal = 1e-10f; 
				logsum += evalSample[p] * log(pixelExpVal) - pixelExpVal;
			}

			ll[eval] = logsum;
		});
	}
};

// C API

CDLL_EXPORT MCLocalizer* MCL_Create(Context* ctx, int maxEmitters, int nframes, Estimator* psf, const PDFInfo* intensityPDF)
{
	try {
		return new MCLocalizer(maxEmitters, nframes, psf, intensityPDF, ctx);
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}

CDLL_EXPORT void MCL_Destroy(MCLocalizer* mcl)
{
	try {
		delete mcl;
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
	}
}

CDLL_EXPORT void MCL_SetSamples(MCLocalizer* mcl, const float* data, const float* background, int numROIs)
{
	try {
		mcl->SetSamples(data, background, numROIs);
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
	}
}


CDLL_EXPORT void MCL_ComputeLikelihood(MCLocalizer* mcl, float* ll, const float* params, const float* backgrounds, const int* sampleIndices, int numEvals)
{
	try {
		mcl->ComputeLikelihood(ll, params, backgrounds, sampleIndices, numEvals);
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
	}
}

CDLL_EXPORT void MCL_ComputeExpVal(MCLocalizer* mcl, float* expval, const float* params, int numEvals)
{
	try {
		mcl->ComputeExpVal(expval, params, numEvals);
	}
	catch (const std::runtime_error& e) {
		DebugPrintf("%s\n", e.what());
	}
}

