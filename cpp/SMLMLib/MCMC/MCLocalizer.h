#pragma once

#include "DLLMacros.h"

class Estimator;
class MCLocalizer;
class Context;

struct PDFInfo {
	const float* pdf;
	int size;
	float start, end; // 0 outside of this range
	// p(x) = pdf[ (x-start) / (end-start) * size ]. Although maybe we need interpolation?
};

CDLL_EXPORT MCLocalizer* MCL_Create(Context* ctx, int maxEmitters, int nframes, Estimator* psf, const PDFInfo* intensity_pdf);
CDLL_EXPORT void MCL_Destroy(MCLocalizer* mcl);

// data and background both have shape [numROIs, nframes, psf.samplesize]
CDLL_EXPORT void MCL_SetSamples(MCLocalizer* mcl, const float* data, const float* background, int numROIs);

// ll has shape [numEvals]
// params has shape [numEvals, numEmitters, (3 + numframes)] 
// each emitter parameter set has format X,Y,Z,I0, I1,I2,...
// sampleIndices has shape [numEvals] and indexes into the set of samples defined using MCL_SetSamples
// computes the log-likelihood of the samples given all the emitter PSFs summed together + the expected fluorescence background
CDLL_EXPORT void MCL_ComputeLikelihood(MCLocalizer* mcl, float* ll, const float* params, const float* backgrounds, const int* sampleIndices, int numEvals);

CDLL_EXPORT void MCL_ComputeExpVal(MCLocalizer* mcl, float* expval, const float* params, int numEvals);
