/*

DSpline is short for differentiable spline, a spline where we can differentiate w.r.t 
both input coordinates (x,y,z) and weights

f(x,y,z) = sum_{a=0..3} sum_{i=0..3} x^a A_{a,i} * 
			sum_{b=0..3} sum_{j=0..3} y^b A_{b,j} *
			sum_{c=0..3} sum_{k=0..3} z^c A_{c,k} * W_{i,j,k}

layout of coefficients per voxel is as binary counting:
W000 = f
W001 = D(f,x)
W010 = D(f,y)
W011 = D(f,xy)
W100 = D(f,z)
W101 = D(f,xz)
W110 = D(f,yz)
W111 = D(f,xyz)
*/

#include "DLLMacros.h"
#include "CudaUtils.h"
#include "palala.h"
#include "Vector.h"
#include "KahanSum.h"
#include "Estimators/CSpline/CubicSplinePSF.h"

// weights array of shape [D*2,H*2,W*2]
CDLL_EXPORT void DSpline_Eval(Vector3f* pos, int count, int roisize, const Int3& splineDims,
	const float* weights, float* output)
{
	Int3 gridsize = splineDims/2; // every spline knot has 2 coefficients per axis, 2*2*2=8 in total
	for (int roi = 0; roi < count; roi++) {
		float* out = &output[roisize * roisize * roi];
		Vector3f p = pos[roi];

		p[0] -= gridsize[2] * 0.5f;
		p[1] -= gridsize[1] * 0.5f;

		auto weight = [=](int x, int y, int z) {
			return weights[splineDims[2] * (z * splineDims[1] + y) + x];
		};

		const float A[] = {
			1, 0, 0, 0,
			0, 1,0 , 0,
			-3,-2,3,-1,
			2,1,-2, 1
		};

		float pz = clamp(p[2], 0.0f, gridsize[0] - 1.0001f);
		int vz = int(pz);
		float sz = pz - vz;//subpixel z

		// perfectly reasonable number of nested loops here
		for (int y = 0; y < roisize; y++) {
			float py = clamp(y - p[1], 0.0f, gridsize[1] - 1.0001f);
			int vy = int(py);
			float sy = py - vy;
			for (int x = 0; x < roisize; x++) {
				float px = clamp(x - p[0], 0.0f, gridsize[2] - 1.0001f); 
				int vx = int(px);
				float sx = px - vx;

				float val = 0.0f;
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						for (int k = 0; k < 4; k++) {
							float xexp = 1.0f;
							for (int a = 0; a < 4; a++) {
								float yexp = 1.0f;
								for (int b = 0; b < 4; b++) {
									float zexp = 1.0f;
									for (int c = 0; c < 4; c++) {
										float w = weight(vx * 2 + i, vy * 2 + j, vz * 2 + k);
										val += xexp * yexp * zexp * A[a * 4 + i] * A[b * 4 + j] * A[c * 4 + k] * w;

										zexp *= sz;
									}
									yexp *= sy;
								}
								xexp *= sx;
							}
						}
					}
				}

				out[y * roisize + x] = val;
			}
		}
	}
}

// returns the gradient of the weights w.r.t the loglikelihood of the pixel data assuming poisson distribution
// mu(w) = I * psf(w) + bg
// LL = log p(mu(w)) = x log mu(w) - mu(w)
// LL = log p(mu(w)) = x log (I*psf(w)+bg) - (I*psf(w)+bg)
// D(LL,w) = D(mu,w) * (x / mu(w) - 1)
// D(LL,w) = I*D(psf,w) * (x / (I*psf(w)+bg) - 1)
CDLL_EXPORT void DSpline_LLWeightsGradient(Vector3f* emitterPos, const float* emitterIntensities,
	int emitterCount, int roisize, Int3& splineDims,
	const float* weights, float* weightsGradient, const float* pixels, const float* pixelBackgrounds, float* roi_ll, float* psf_, bool cuda)
{
	// every pixel modifies 64 weights
	// after computing these per-pixel weight gradients, we can sum them together to get the complete gradient
	//std::vector<float> partialWeightGradients(roisize * roisize * emitterCount * 64); 
	Int3 gridsize = splineDims / 2; // every spline knot has 2 coefficients per axis, 2*2*2=8 in total

	for (int roi = 0; roi < emitterCount; roi++) {
		const float* roi_pixels = &pixels[roi * roisize * roisize];
		const float* roi_bg = &pixelBackgrounds[roi * roisize * roisize];
		//float* out = &expval[roisize * roisize * roi];
		Vector3f p = emitterPos[roi];
		p[0] -= gridsize[2] * 0.5f;
		p[1] -= gridsize[1] * 0.5f;

		const float A[] = {
			1, 0, 0, 0,
			0, 1,0 , 0,
			-3,-2,3,-1,
			2,1,-2, 1
		};

		auto getWeight = [=](int x, int y, int z) {
			return weights[splineDims[2] * (z * splineDims[1] + y) + x];
		};

		auto getWeightDerivative = [=](int x, int y, int z) -> float& {
			return weightsGradient[splineDims[2] * (z * splineDims[1] + y) + x];
		};
	//	auto getWeightGradient = )

		float pz = clamp(p[2], 0.0f, gridsize[0] - 1.0001f);
		int vz = int(pz);
		float sz = pz - vz;//subpixel z

		KahanSum<float> ll;

		// 
		//float* pwg_roi = &partialWeightGradients[roisize * roisize * 64 * roi];
		//auto partialWeightGradient = [&](int x, int y, int w) -> float& { return pwg_roi[(y * roisize + x) * 64 + w]; };

		// perfectly reasonable number of nested loops here
		for (int y = 0; y < roisize; y++) {
			float py = clamp(y - p[1], 0.0f, gridsize[1] - 1.0001f);
			int vy = int(py);
			float sy = py - vy;

			for (int x = 0; x < roisize; x++) {
				float px = clamp(x - p[0], 0.0f, gridsize[2] - 1.0001f);
				int vx = int(px);
				float sx = px - vx;

				float dfdws[64];
				float psf = 0.0f;

				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						for (int k = 0; k < 4; k++) {
							float dfdw = 0.0f;
							float xexp = 1.0f;
							for (int a = 0; a < 4; a++) {
								float yexp = 1.0f;
								for (int b = 0; b < 4; b++) {
									float zexp = 1.0f;
									for (int c = 0; c < 4; c++) {
										float w = getWeight(vx * 2 + i, vy * 2 + j, vz * 2 + k);
										float tmp = xexp * yexp * zexp * A[a * 4 + i] * A[b * 4 + j] * A[c * 4 + k];

										dfdw += tmp;
										psf += tmp * w;
										zexp *= sz;
									}
									yexp *= sy;
								}
								xexp *= sx;
							}

							dfdws[k * 16 + j * 4 + i] = dfdw;
							//partialWeightGradient(x, y, k * 16 + j * 4 + i) = dfdw *
						}
					}
				}

				if (psf < 0.0f) psf = 0.0f;

				psf_[roisize * roisize * roi + y * roisize + x] = psf;

				float I = emitterIntensities[roi];
				float bg = roi_bg[y * roisize + x];
				float sample = roi_pixels[y * roisize + x];
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						for (int k = 0; k < 4; k++) {
							// We need both the derivative of the value w.r.t weight, and the value itself:
							// D(LL,w) = I*D(psf,w) * (x / (I*psf(w)+bg) - 1)
							float expval = I * psf + bg;
							if (expval < 1e-9f) expval = 1e-9f;
							float D_ll = I * dfdws[k * 16 + j * 4 + i] * (sample / expval - 1.0f);
							ll += sample * log(expval) - expval;
							//partialWeightGradient(x, y,  k * 16 + j * 4 + i) = pwg;

							getWeightDerivative(vx * 2 + i, vy * 2 + j, vz * 2 + k) += D_ll;
						}
					}
				}


			}
		}

		roi_ll[roi] = ll;
	}

}


CDLL_EXPORT Estimator* DSpline_CreatePSF(int roisize, Int3& splineDims, CSpline_Calibration& calib, const float* weights, int csplineMode, bool cuda, Context* ctx)
{
	//CSpline_Calibration calib;
	Int3 gridsize = splineDims / 2;
	float* coefs = (float*)calib.coefs; //((gridsize-1).prod() * 64);
	Int3 shape_out = gridsize - 1;
	DebugPrintf("assuming coefs size: %d\n", (shape_out.prod() * 64));

	const float A[] = {
		1, 0, 0, 0,
		0, 1,0 , 0,
		-3,-2,3,-1,
		2,1,-2, 1
	};
	auto getWeight = [=](int x, int y, int z) {
		return weights[splineDims[2] * (z * splineDims[1] + y) + x];
	};
	for (int z = 0; z < shape_out[0]; z++) {
		for (int y = 0; y < shape_out[1]; y++) {
			for (int x = 0; x < shape_out[2]; x++) {
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						for (int k = 0; k < 4; k++) {

							float sum = 0.0f;
							for (int a = 0; a < 4; a++) {
								for (int b = 0; b < 4; b++) {
									for (int c = 0; c < 4; c++) {
										float w = getWeight(x * 2 + i, y * 2 + j, z * 2 + k);
										sum += A[a * 4 + i] * A[b * 4 + j] * A[c * 4 + k] * w;
									}
								}
							}

							coefs[(z * shape_out[1] * shape_out[2] + y * shape_out[2] + x) * 64 + 16 * k + 4 * j + i] = sum;
						}
					}
				}
			}
		}
	}

	return CSpline_CreatePSF(roisize, calib, 0, csplineMode, cuda, ctx);
}


