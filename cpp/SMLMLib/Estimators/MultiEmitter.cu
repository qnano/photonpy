/*

Parameter format: b, X0, Y0, [Z0,] I0, ...
Constant format: [*background values, *psf constants]
*/
#include "DLLMacros.h"
#include "Estimator.h"
#include "ThreadUtils.h"
#include "Estimation.h"
#include "DebugImageCallback.h"



class MultiEmitterEstimator : public cuEstimator
{
public:

	class DeviceBuffers
	{
	public:
		DeviceBuffers(int smpcount, int numROIs, int numconst, int numEmitters, int numParams, int numPsfParams);
		~DeviceBuffers() {}
		DeviceArray<float> psf_deriv,  // [numROIs, numEmitters, numParams, smpcount]
			psf_ev,  // [numROIs, numEmitters, smpcount]
			psf_const,
			lm_alphabeta, 
			lm_lu,
			psf_params; // [numROIs, numEmitters, numPsfParams]
		DeviceArray<int> psf_roipos;
		int numROIs; 
	};

	bool debugMode = false;
	cuEstimator* psf;
	int numEmitters; // # emitters in 1 ROI
	std::mutex streamDataMutex;
	std::unordered_map<cudaStream_t, DeviceBuffers> streamData;

	DeviceBuffers* GetDeviceBuffers(cudaStream_t stream, int numspots);

	DeviceBuffers* UpdateBuffers(const float* d_params, const float* d_const, const int* d_roipos, int numROIs, cudaStream_t stream);

	MultiEmitterEstimator(cuEstimator* psf, int numEmitters, std::vector<ParamLimit> minmax, bool debugMode);

	// d_image[numspots, SampleCount()], d_params[numspots, NumParams()]
	virtual void ExpectedValue(float* expectedvalue, const float* d_params, const float* d_const, const int* d_roipos, int numspots, cudaStream_t stream);

	// d_deriv[numspots, NumParams(), SampleCount()], d_expectedvalue[numspots, SampleCount()], d_params[numspots, NumParams()]
	// psf_deriv output format: [numspots, NumParams(), SampleCount()]
	virtual void Derivatives(float* deriv, float* expectedvalue, const float* params, const float* d_const, const int* d_roipos, int numspots, cudaStream_t stream);

	// d_sample[numspots, SampleCount()], d_params[numspots, numparams], d_initial[numspots, NumParams()], d_params[numspots, NumParams()], d_iterations[numspots]
	//virtual void Estimate(const float* d_sample, const float* d_const, const int* d_roipos, const float* d_initial, float* d_params, float* d_diagnostics, int* iterations,
	//	int numspots, float* trace, int traceBufLen, cudaStream_t stream, int maxIterations);
};

CDLL_EXPORT Estimator* MultiEmitter_CreateEstimator(Estimator* psf, int numEmitters, const float* minmax, bool debugMode, Context* ctx)
{
	try {
		if (psf->NumParams() != 4 && psf->NumParams() != 5 || !psf->Unwrap())
			return 0;

		int n = 1 + (psf->NumParams() - 1) * numEmitters;

		std::vector<ParamLimit> limits(n);
		for (int i = 0; i < n; i++)
			limits[i] = { minmax[i], minmax[i + n] };

		auto* estim = Estimator_WrapCUDA(new MultiEmitterEstimator(psf->Unwrap(), numEmitters, limits, debugMode));
		if (ctx) estim->SetContext(ctx);
		return estim;
	}
	catch (const std::runtime_error & e) {
		DebugPrintf("%s\n", e.what());
		return 0;
	}
}


MultiEmitterEstimator::DeviceBuffers::DeviceBuffers(int smpcount, int numROIs, int numConst, int numEmitters, int numParams, int numPsfParams)
	: numROIs(numROIs),
	psf_deriv(smpcount* numParams* numROIs* numEmitters),
	psf_const(numConst*numROIs*numEmitters),
	psf_ev(smpcount* numROIs* numEmitters), 
	psf_params(numROIs *numEmitters * numPsfParams),
	lm_alphabeta(numROIs* numParams* (numParams + 1)), 
	lm_lu(numROIs* numParams* numParams),
	psf_roipos(numROIs*numEmitters)
{}


MultiEmitterEstimator::DeviceBuffers* MultiEmitterEstimator::GetDeviceBuffers(cudaStream_t stream, int numROIs)
{
	return LockedFunction(streamDataMutex, [&]() {
		auto it = streamData.find(stream);

		if (it != streamData.end() && it->second.numROIs < numROIs) {
			streamData.erase(it);
			it = streamData.end();
		}

		if (it == streamData.end())
			it = streamData.emplace(stream, DeviceBuffers(SampleCount(), numROIs, psf->NumConstants(), numEmitters,(int) ParamLimits().size(), psf->NumParams())).first;
		return &it->second;
		});
}


MultiEmitterEstimator::DeviceBuffers* MultiEmitterEstimator::UpdateBuffers(const float* d_params, const float* d_const, const int* d_roipos, int numROIs, cudaStream_t stream)
{
	auto* db = GetDeviceBuffers(stream, numROIs);
	int K = NumParams();
	float* psf_params = db->psf_params.data();
	int K_psf = psf->NumParams();
	int numEmitters = this->numEmitters;
	int psfCoords = K_psf - 1; 
	int* psf_roipos = db->psf_roipos.data();
	int sampleIndexDims = psf->SampleIndexDims();
	float* psf_const = db->psf_const.data();
	int numconst = NumConstants();
	int psf_numconst = psf->NumConstants();
	int smpcount = SampleCount();

	// Copy parameters to a new array to call the PSF object with
	LaunchKernel(numROIs, [=]__device__(int roi) {
		const float* roiparam = &d_params[K * roi];
		float* dst = &psf_params[roi * numEmitters * K_psf];
		for (int j = 0; j < numEmitters; j++) {
			dst[K_psf * j + K_psf - 1] = 0.0f; // background
			for (int c = 0; c < psfCoords; c++)
				dst[K_psf * j + c] = roiparam[1 + psfCoords * j + c];

			for (int k = 0; k < sampleIndexDims; k++)
				psf_roipos[roi * numEmitters * sampleIndexDims + j * sampleIndexDims + k] = d_roipos[roi * sampleIndexDims + k];

			// copy constants per ROI to constants per psf
			for (int i = 0; i < numconst; i++) {
				psf_const[roi * numEmitters * psf_numconst + i] = d_const[numconst * roi + smpcount + i];
			}
		}
	}, 0, stream);

	psf->Derivatives(db->psf_deriv.data(), db->psf_ev.data(), db->psf_params.data(), psf_const, psf_roipos, numROIs*numEmitters, stream);

	if (debugMode) {
		std::vector<float> h_params = db->psf_params.ToVector();
		std::vector<float> h_ev = db->psf_ev.ToVector();
		std::vector<float> h_deriv = db->psf_deriv.ToVector();
		int roisize = psf->SampleSize().back();
		int m = psf->SampleSize().size() == 3 ? psf->SampleSize()[0] : 1;
		ShowDebugImage(K_psf, numEmitters, numROIs, h_params.data(), "PSF Params");
		ShowDebugImage(roisize, roisize*m, numEmitters * numROIs, h_ev.data(), "PSF EV");
		ShowDebugImage(roisize, roisize*m, numEmitters * K_psf * numROIs, h_deriv.data(), "PSF Deriv");
	}

	return db;
}


static std::string MakeParamFormat(int N, int numPSFParams)
{
	std::string paramList = "b";

	if (numPSFParams == 5) {
		for (int i = 0; i < N; i++)
			paramList += SPrintf(", x%d, y%d, z%d, I%d", i, i, i, i);
	}
	else {
		for (int i = 0; i < N; i++)
			paramList += SPrintf(", x%d, y%d, I%d", i, i, i);
	}
	return paramList;
}

// psflimits are assumed to be x,y,z,I,bg
static std::vector<ParamLimit> MakeParamLimits(std::vector<ParamLimit> psfLimits, int N)
{
	std::vector<ParamLimit> limits{ psfLimits.back() };
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < psfLimits.size()-1; j++)
			limits.push_back(psfLimits[j]);
	}
	return limits;
}

MultiEmitterEstimator::MultiEmitterEstimator(cuEstimator* psf, int numEmitters, std::vector<ParamLimit> limits, bool debugMode)
: 
	cuEstimator(
		psf->SampleSize(), 
		psf->NumConstants() + psf->SampleCount(), 0, 
		MakeParamFormat(numEmitters, psf->NumParams()).c_str(),
		limits
	),
	numEmitters(numEmitters), 
	psf(psf),
	debugMode(debugMode)
{}


void MultiEmitterEstimator::ExpectedValue(float* expval, const float* d_params, const float* d_const, const int* d_roipos, int numROIs, cudaStream_t stream)
{
	auto* db = UpdateBuffers(d_params, d_const, d_roipos, numROIs, stream);
	int smpcount = psf->NumConstants();

	const float* psf_ev = db->psf_ev.data();
	int numEmitters = this->numEmitters;
	int K = NumParams();
	int numconst = this->NumConstants();

	LaunchKernel(numROIs, smpcount, [=]__device__(int roi, int smp) {
		float bg = d_params[roi * K];
		float sum = bg + d_const[roi * numconst + smp];
		for (int i = 0; i < numEmitters; i++) {
			sum += psf_ev[numEmitters * smpcount * roi + smpcount * i + smp];
		}
		expval[roi * smpcount + smp] = sum;
	}, 0, stream);
}

void MultiEmitterEstimator::Derivatives(float* deriv, float* expectedvalue, const float* params, const float* d_const, const int* d_roipos, int numROIs, cudaStream_t stream)
{
	auto* db = UpdateBuffers(params, d_const, d_roipos, numROIs, stream);

	const float* psf_ev = db->psf_ev.data();
	const float* psf_deriv = db->psf_deriv.data();
	int numEmitters = this->numEmitters;
	int K = NumParams();
	int smpcount = SampleCount();
	int K_psf = psf->NumParams();
	int numconst = this->NumConstants();

	// copy results
	LaunchKernel(numROIs, smpcount, [=]__device__(int roi, int smp) {
		float bg = params[roi * K];
		float sum = bg + d_const[roi * numconst + smp];

		float* roi_deriv = &deriv[K * smpcount * roi];

		for (int i = 0; i < numEmitters; i++) { // go through all emitters within a single ROI
			sum += psf_ev[numEmitters * smpcount * roi + smpcount * i + smp];

			for (int j = 0; j < K_psf-1; j++) // go through all psf parameters except background
				roi_deriv[smpcount * (j + (K_psf-1) * i + 1) + smp] = 
					psf_deriv[numEmitters * smpcount * K_psf * roi + smpcount * K_psf * i + smpcount * j + smp];
		}
		roi_deriv[smpcount * 0 + smp] = 1.0f;//bg derivative
		expectedvalue[roi * smpcount + smp] = sum;
	}, 0, stream);
}
