import numpy as np

from photonpy import Context,PostProcessMethods

if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # Generate a bunch of ground truth points
    W = 6
    N = 10
    S = 20
    gt = np.random.uniform(0,W,size=(N,2))

    # Generate random distributions from those points as if noisy localization is done
    loc_prec = 0.25
    pts = np.repeat(gt, S, 0)
    pts += np.random.normal(0, loc_prec,size=pts.shape)
    
    crlb = np.ones((len(pts),2)) * loc_prec

    plt.figure()
    plt.scatter(pts[:,0],pts[:,1], s=1,marker='.')
    
    dist=loc_prec*4
    def drawUpdate(mapping, centers):
        plt.figure()
        plt.scatter(pts[:,0],pts[:,1], c=mapping, s=10, marker='o')

        for k in range(len(centers)):
            circle = plt.Circle((centers[k,0], centers[k,1]), dist, color='r',fill=False)
            plt.gca().add_artist(circle)
        
        print(np.max(mapping))
    
    with Context() as ctx:
        clusterpos, cl_crlb, mapping = PostProcessMethods(ctx).ClusterLocs(pts, crlb, dist, drawUpdate)
        
        plt.scatter(clusterpos[:,0],clusterpos[:,1], s=2, marker='o', label='Cluster mean')
        
        plt.legend()