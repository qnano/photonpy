
# Steps to run this:
# 
# 1. Download the software from "Real-time 3D single-molecule localization using experimental point spread functions"
#    https://www.nature.com/articles/nmeth.4661#Sec21
# 
# 2. Download the bead calibration stack from 
#    http://bigwww.epfl.ch/smlm/challenge2016/datasets/Tubulin-A647-3D/Data/data.html
# 
# 3. Use above software to generate the cspline calibration .mat file
#    Adjust the path and run this code
#
import numpy as np
import matplotlib.pyplot as plt

from photonpy import Context,GaussianPSFMethods
from photonpy.cpp.cspline import CSpline_Calibration, CSpline

import time

from photonpy.smlm.psf import psf_to_zstack

def nudge(v, amount):
    return v*np.random.uniform(1-amount,1+amount,size=v.shape)

with Context(debugMode=False) as ctx:
    #psf_fn = 'C:/data/TetrapodUtrecht/TP_STORM_191213_exp50_LP50_okayrecon/calibration/TFScal_vPSF2um_1622_3Dcorr.mat'
    roisize= 45
    psf_fn = 'C:/data/LS/tetrapod_0610.mat'

    calib = CSpline_Calibration.from_file_nmeth(psf_fn)
    psf = CSpline(ctx).CreatePSF_XYZIBg(roisize, calib, fitMode=CSpline.TiltedBg)
    lmp=psf.GetLevMarParams()
    psf.SetLevMarParams(-0.1, iterations=100)#1e-12)
    #print(lmp)

    intensity=2000
    bg = 20
    N = 200
    theta = np.repeat([[roisize/2,roisize/2,0,intensity,bg,0,0]], N, axis=0)
    theta=theta[:,:psf.numparams]
    theta[:,[0,1]] =roisize/2#  np.random.uniform(-2,2,size=(N,2))+roisize/2
    theta[:,2]= np.linspace(-1,1,N)
    #theta[:,2] = np.linspace(-0.5,0.5, N)
     
    smp = psf.GenerateSample(theta)
    fi = psf.FisherMatrix(theta)

    crlb = psf.CRLB(theta)
    pixelsize=65
    plt.figure()    
    plt.plot(theta[:,2], crlb[:,2] * 1000, label="Z")
    plt.plot(theta[:,2], crlb[:,0]  * pixelsize, label="X")
    plt.plot(theta[:,2], crlb[:,1] * pixelsize, label="Y")
    plt.legend()
    plt.ylabel('CRLB [nm]')
    plt.xlabel('Z position [um]')
    plt.title(f'3D CRLB for I={intensity} photons and bg={bg} photons/pixel')
    plt.show()
    
    plt.figure()
    #zpos = np.repeat(np.linspace(-1,1,50))
    estim_z = psf.Estimate(smp, initial=nudge(theta,0.2))[0][:,2]
    plt.plot(theta[:,2], estim_z, label="Estimated")
    plt.plot(theta[:,2], theta[:,2], label='Ground truth')
    plt.xlabel("Z position [um]")
    plt.ylabel("Z position [um]")
    plt.title('Estimated Z vs ground truth Z')
    
    zstack = psf_to_zstack(psf, zrange=np.linspace(-1, 1, 8),plot=True)
    
    gauss_psf = GaussianPSFMethods(ctx).CreatePSF_XYIBg(12, 1.5, cuda=True)
    gauss_psf.CRLB([[roisize/2,roisize/2,1000,0]])
    
    plt.figure()
    plt.hist(zstack.flatten(),bins=50)
    
    plt.show()
