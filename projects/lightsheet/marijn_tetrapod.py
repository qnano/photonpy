


from projects.lightsheet.ls_localize3D import localize3D

if True:
    basepath = 'C:/data/TetrapodUtrecht/TP_STORM_191213_exp30_LP50_badrecon/'
    fn = basepath+'rawdata/STORM_MMStack_Pos0.ome.tif'
    psf_fn = basepath+'calibration/TFScal_vPSF2um_1622_3Dcorr.mat'

    localize3D(fn, psf_fn, None,
               50, 
               detectorThreshold=2,
               cameraGain=0.5,
              cameraOffset=100, 
               chisqThreshold=2.5,
               psf_zrange=(-1.2,1.0),
               numDetectionZPlanes=10,
               borderxy=20,
               lightsheet=False,
               drift3D=True,
               pixelsize=65)



if True:
    basepath = 'C:/data/TetrapodUtrecht/TP_STORM_190822_goodrecon/'
    fn = basepath+'rawdata/vPSF2um_pos4_2_MMStack_Pos.tif'
    psf_fn = basepath+'calibration/TFScal_vPSF2um_1622_3Dcorr.mat'

    localize3D(fn, psf_fn, None, 
               roisize=53, 
               detectorThreshold=1.5,
               cameraGain=0.5,
              cameraOffset=100, 
               chisqThreshold=3,
                psf_zrange=(-1.25,1.1),
               numDetectionZPlanes=20,
               borderxy=20,
               lightsheet=False,
               pixelsize=65,
               maxframes=0,
               drift3D=True,
               psfFitterLambda=-1e-14,
               driftFramesPerSplinePt=200)


if True:
    basepath = 'C:/data/TetrapodUtrecht/TP_STORM_191213_exp50_LP50_okayrecon/'
    fn = basepath+'rawdata/STORM_MMStack_Pos0.ome.tif'
    psf_fn = basepath+'calibration/TFScal_vPSF2um_1622_3Dcorr.mat'

    localize3D(fn, psf_fn, None,
               50, 
               detectorThreshold=2,
               cameraGain=0.5,
              cameraOffset=100, 
               chisqThreshold=2.5,
                psf_zrange=(-1.2,1.1),
               numDetectionZPlanes=10,
               borderxy=20,
               lightsheet=False,
               pixelsize=65,
               drift3D=True,
               driftFramesPerSplinePt=200)


