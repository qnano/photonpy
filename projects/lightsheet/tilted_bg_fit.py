# -*- coding: utf-8 -*-

from photonpy import Context, Gaussian, SpotDetector,PSFCorrelationSpotDetector,Dataset
import numpy as np
import matplotlib.pyplot as plt


with Context() as ctx:
    roisize=12
    psf=Gaussian(ctx).CreatePSF_XYITiltedBg(roisize, 2, cuda=True)
    
    # -*- coding: utf-8 -*-

    n=6
    params= np.repeat( [[roisize/2,roisize/2,500,10,0,0.1]], n, 0)
    smp = psf.GenerateSample(params)
    #plt.figure()
    #plt.imshow(smp[0])
    estim = psf.Estimate(smp)[0]
    mu = psf.ExpectedValue(estim)
    
    img = np.concatenate(np.concatenate((smp,mu),-1),0)
    plt.figure()
    plt.imshow(img)
    
