# -*- coding: utf-8 -*-
"""
Created on Tue May 25 17:48:00 2021

@author: labuser
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 19:07:16 2020

@author: labuser
"""

import numpy as np
import matplotlib.pyplot as plt
import tifffile
from photonpy import Context, GaussianPSFMethods, SpotDetector, PSFCorrelationSpotDetector, Dataset

import matplotlib.gridspec as gridspec
from scipy import ndimage
from skimage import io
from scipy import ndimage
if True:
    with Context() as ctx:
        roisize=13
        hw=roisize//2
        fname="bead-X1.tif"
        
        img=io.imread(fname).astype(float)
        
        img=(img-101)*0.5
        
        gauss_api = GaussianPSFMethods(ctx)
        psf = gauss_api.CreatePSF_XYIBgSigma(roisize, 1.5, cuda=True)
        
        p=np.zeros([10,10,2])
        count=0
        for i in range(10):
            for j in range(10):
                c=np.unravel_index(img[count].argmax(), img[count].shape)
                subimg=img[count,c[0]-hw:c[0]+hw+1,c[1]-hw:c[1]+hw+1]
                bg=np.median(subimg)
                result=psf.Estimate([subimg],initial=[[hw,hw,np.sum(subimg-bg),bg,1.5]])[0]
                
                p[i,j,1]=(result[0,1]+c[0])
                p[i,j,0]=(result[0,0]+c[1])
                count=count+1
                
    pavg=np.mean(p,axis=1)
    plt.figure()
    plt.plot(pavg)
    plt.show()   
    np.savetxt("offset.txt",pavg)         
                
        
        
            