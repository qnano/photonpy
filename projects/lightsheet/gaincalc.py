# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 16:34:59 2020

@author: jelmer
"""


import numpy as np
import tifffile
import matplotlib.pyplot as plt


fn = 'C:/data/LS/cam_calib/dark.tif'
dark = tifffile.imread(fn)

fn = 'C:/data/LS/cam_calib/bright.tif'
light =tifffile.imread(fn)

offset = np.mean(dark,0)

plt.figure()
plt.hist(offset.flatten(),bins=200)
plt.title('Offset per pixel')

sig = light-offset
v = np.var(sig, 0)
m = np.mean(sig,0)

gain = v/m
gain[gain==0] = np.mean(gain)

plt.figure()
plt.hist(gain.flatten(), bins=200)
plt.title('Variance / Mean per pixel')

checksig = sig/gain

plt.figure()
plt.hist( (np.var(checksig,0)/np.mean(checksig,0)).flatten() ,bins=100,range=[0.99,1.01])
plt.title('Var/Mean should be around 1 for poisson')

plt.figure()
plt.imshow(gain)
plt.colorbar()
plt.title('Gain')
