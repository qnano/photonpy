# -*- coding: utf-8 -*-

import matplotlib as mpl
#mpl.use('svg')
"""
new_rc_params = {
#    "font.family": 'Times',
    "font.size": 15,
    "font.serif": [],
    "svg.fonttype": 'none'} #to store text as text, not as path
mpl.rcParams.update(new_rc_params)
"""

import matplotlib.pyplot as plt
import numpy as np
import h5py
import os
#import scipy.stats
import pickle
#import photonpy.simflux.util.crlb as crlb_calc

from photonpy import Dataset

def savefig(path):
    plt.savefig(path + ".png")
    plt.savefig(path + ".svg")


        
def format_func(value, tick_number):
    value *= 2*np.pi
    # find number of multiples of pi/2
    N = int(np.round(2 * value / np.pi))
    if N == 0:
        return "0"
    elif N == 1:
        return r"$\pi/2$"
    elif N == 2:
        return r"$\pi$"
    elif N % 2 > 0:
        return r"${0}\pi/2$".format(N)
    else:
        return r"${0}\pi$".format(N // 2)


    
def generate(pickedfn, fraction_of_pts):
    ds = Dataset.load(pickedfn)
    #ds = ds[(ds.pos[:,2]<0.1) & (ds.pos[:,2] > -0.8)]
    
    groups = ds.data.group
    pos = ds.data.estim.pos
    
    #crlb.jacobian_silm_XYIBg(theta,roisize,sigma,mod)
    bincounts = np.bincount(groups)
    print(f"{len(bincounts)} molecules for {pickedfn}")
    precision = np.zeros((len(bincounts),ds.dims))
    crlbs = np.zeros((len(bincounts),ds.dims))
    centers = np.zeros((len(bincounts),ds.dims))
    intensities = np.zeros(len(bincounts))
    bg = np.zeros(len(bincounts))
        
    for k in range(len(bincounts)):
        estim_group = pos[groups==k]
        precision[k] = np.std(estim_group,0)
        center = np.mean(estim_group,0)
   
        crlbs[k] = np.mean(ds.data.crlb.pos[groups==k],0)
        intensities[k] = np.mean(ds.photons[groups==k])
        centers[k] = center
        bg[k] = np.mean(ds.background[groups==k])
        
    sel = np.nonzero( (bincounts > 10) & (intensities > 800))[0]

    local_pos = pos-centers[groups]
    local_pos = np.concatenate([local_pos[groups==i] for i in sel])
     
    precision = precision[sel]
    crlbs = crlbs[sel]
    centers = centers[sel]
    
    
    print(intensities)
    print(bg)
#    sel = centers[:,2] > 
    
    return precision,crlbs,centers,local_pos

def scatter_clip( plots, xlim, ylim,stepsize):
    for xp, yp, label in plots:
        which = (xp>xlim[0]) & (xp<xlim[1]) & (yp>ylim[0]) & (yp<ylim[1])
        plt.scatter(xp[which], yp[which], label=label)
    plt.xlim(xlim)
    plt.ylim(ylim)
    
    #plt.gca().xaxis.set_ticks(np.arange(round(xlim[0]),round(xlim[1]),stepsize))
    #plt.gca().yaxis.set_ticks(np.arange(round(ylim[0]),round(ylim[1]),stepsize))
    
    
    
def peak(x, c):
    return np.exp(-np.power(x - c, 2) / 16.0)

def lin_interp(x, y, i, half):
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def half_max_x(x, y):
    half = max(y)/2.0
    signs = np.sign(np.add(y, -half))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    return [lin_interp(x, y, zero_crossings_i[0], half),
            lin_interp(x, y, zero_crossings_i[1], half)]

def process(dataname, files, fraction_of_pts=1,stepsize=1,histbins=20,pixelsize=65):    
    units=['nm','nm', 'photons', 'photons/pixel']
    axname=['X','Y','Z']
    scale=[pixelsize,pixelsize,1000]
    
    resultdir = os.path.split(files[0][0])[0] + '/results/'
    os.makedirs(resultdir, exist_ok=True)

    results = [generate(f[0], fraction_of_pts=fraction_of_pts) for f in files]
    
    for r in results:
        local_pos=r[3]
        print(local_pos.shape)
        fig,ax=plt.subplots(1,local_pos.shape[1])
        binrange=[80,80,200]
        for i in range(local_pos.shape[1]):
            y,bins,patches = ax[i].hist(local_pos[:,i]*scale[i],range=[-binrange[i],binrange[i]],bins=30)
            ax[i].set_xlabel(f'{axname[i]} position [nm]')
            ax[i].set_title(f'{axname[i]} position [nm]')
            print(f"{axname[i]} precision: {np.std(local_pos[:,i]*scale[i])}")

            # find the two crossing points
            hmx = half_max_x(bins,y)
            
            # print the answer
            fwhm = hmx[1] - hmx[0]
            print("FWHM:{:.3f}".format(fwhm))
            
            # a convincing plot
            half = max(y)/2.0
            #ax[.plot(x,y)
            ax[i].plot(hmx, [half, half])

    fig=plt.figure()
    lim=min([np.max(r[0][:,[0,1]]*scale[0]) for r in results])
    
    print(f"plot limits: {lim}")

    l = []    
    for i in range(len(files)):
        prec,crlb,centers,_=results[i]
        l.append( (prec[:,0]*scale[0],prec[:,1]*scale[1],f'Measured precision ({files[i][1]})') )
        l.append( (crlb[:,0]*scale[0],crlb[:,1]*scale[1],f'CRLB ({files[i][1]})') )

    scatter_clip(l,     
        xlim=(0,lim+0.5),
        ylim=(0,lim+0.5),
        stepsize=stepsize)
    
    plt.legend()
    plt.xlabel(f'Precision in X [{units[0]}]' )
    plt.ylabel(f'Precision in Y [{units[1]}]' )
    plt.title(f'CRLB vs measured precision ({dataname})')
    savefig(f"{resultdir}/{dataname}-crlb-vs-exp-xy")
    
    # Histogram plots
    binrange=[30,30,100]
    for ax in range(3):
        fig = plt.figure(figsize=(6,4))
        plt.hist([r[0][:,ax]*scale[ax] for r in results], label=[f[1]+" - Precision" for f in files], range=[0,binrange[ax]], bins=histbins)
        plt.hist([r[1][:,ax]*scale[ax] for r in results], label=[f[1]+" - CRLB" for f in files], range=[0,binrange[ax]], bins=histbins)
        plt.legend()
        plt.ylabel(f'Localized binding sites')
        plt.xlabel(f'Precision in {axname[ax]} [nm]')
        savefig(f"{resultdir}/{dataname}-crlb-vs-exp-histogram-{axname[ax]}")

    precm = [ np.median(r[0],0) for r in results ]             
    for i,(prec,crlb,centers,_) in enumerate(results):
        label = files[i][1]
        print(f"Median precision {label} ({dataname}): X= {precm[i][0]*pixelsize:.3f} Y= {precm[i][1]*pixelsize:.3f}")

    if len(results)>1:
        print(f"Measured X improvement {files[1][1]}/{files[0][1]}: {precm[1][0]/precm[0][0]:.2f}")
        print(f"Measured Y improvement {files[1][1]}/{files[0][1]}: {precm[1][1]/precm[0][1]:.2f}")
        print(f"Measured I improvement {files[1][1]}/{files[0][1]}: {precm[1][2]/precm[0][2]:.2f}")

def r(pos,angle):
    result=np.zeros([len(pos),2])
    for i in range(len(pos)):
        x = np.cos(angle) * pos[i,0] + np.sin(angle) * pos[i,1]
        y = -np.sin(angle) * pos[i,0] + np.cos(angle) * pos[i,1]
        result[i,0]=x
        result[i,1]=y
    return result
# process_movie pipeline (with sigma interpolation)
path = 'D:/data/nanoruler/'
"""
process(  'CRLB vs experimental precision', 
    [(path+'locs_undrifted_picked.hdf5', 'Tetrapod PSF', False)], 
    fraction_of_pts = 1,stepsize=2, pixelsize=65, histbins=40)

plt.show()
"""
from sklearn.decomposition import PCA
from scipy.ndimage.interpolation import rotate
for k in range(1,16):
    
    path = 'D:/data/nanoruler/locs_undrifted_picked'+str(k)+'.hdf5'
    ds = Dataset.load(path)
    
    groups = ds.data.group
    pos = ds.data.estim.pos
    if (len(pos)>=50):
        
        
        
        center=np.mean(pos,0)
        #pos=pos-center
        plt.scatter(pos[:,0],pos[:,1])
        plt.show()
        pca = PCA(n_components=2)
        pca.fit(pos)
        c=pca.components_
        v=c[:,0]
        #pos = r(pos, np.degrees(np.arctan(v[1]/v[0])))
        pos = pca.transform(pos)
        if k==1:
            al=pos
        else:
            al= np.concatenate((al, pos), axis=0)
plt.scatter(al[:,0],al[:,1])

plt.show()
plt.hist(al[:,0]*65,bins=50)
plt.show()

g0=al[al[:,0]<-0.5]

g1=al[al[:,0]>-0.5]
g1=g1[g1[:,0]<1.0]

g2=al[al[:,0]>1.0]

c0=np.mean(g0,axis=0)

c1=np.mean(g1,axis=0)

c2=np.mean(g2,axis=0)

d1=65*(c1[0]-c0[0])
d2=65*(c2[0]-c1[0])

#plt.hist(al[:,0]*65-c1[0]*65,bins=50)
plt.hist(g0[:,0]*65-c1[0]*65, bins=20, alpha=0.5, label="Spot 1")
plt.hist(g1[:,0]*65-c1[0]*65, bins=18, alpha=0.5, label="Spot 2")
plt.hist(g2[:,0]*65-c1[0]*65, bins=12, alpha=0.5, label="Spot 3")

plt.axvline(x=c0[0]*65-c1[0]*65,ymin=0,ymax=0.8,color="black", linestyle="--")
plt.axvline(x=c1[0]*65-c1[0]*65,ymin=0,ymax=0.8,color="black", linestyle="--")
plt.axvline(x=c2[0]*65-c1[0]*65,ymin=0,ymax=0.8,color="black", linestyle="--")
plt.text(c0[0]*65-c1[0]*65-30, 32, f"center: {c0[0]*65-c1[0]*65:.2f} nm", fontsize=8)
plt.text(c1[0]*65-c1[0]*65-30, 32, f"center: {c1[0]*65-c1[0]*65:.2f} nm", fontsize=8)
plt.text(c2[0]*65-c1[0]*65-30, 32, f"center: {c2[0]*65-c1[0]*65:.2f} nm", fontsize=8)
plt.xlabel("Position (nm)")
plt.ylabel("Counts")

plt.ylim([0,35])
fn="nanoruler"
plt.savefig(fn,dpi=200)
plt.show()