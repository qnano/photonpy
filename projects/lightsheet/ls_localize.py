# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 17:25:41 2020

@author: jelmer
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from photonpy import Context, Gaussian, SpotDetector,PSFCorrelationSpotDetector,Dataset
import photonpy.smlm.process_movie as process_movie
import pickle

import photonpy.utils.multipart_tiff as ppt

def view_rois(pixels):
    import napari
    with napari.gui_qt():
        napari.view_image(pixels)
        
def print_crlb():
    sigma = [2.2954125, 2.322116]
    roisize = 12
    with Context() as ctx:
        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, cuda=False) 
        
        pixelsize=65
        n=50
        params = np.repeat([[roisize/2,roisize/2,1000,0]],n,0)
        params[:,3] = np.linspace(5,100,n)
        crlb = psf.CRLB(params)
        plt.figure()
        plt.plot(params[:,3],crlb[:,0]*pixelsize)
        plt.xlabel('Background [photons/pixel]')
        plt.ylabel('Precision in X [nm]')


def approx_gain(fn):
    mov=np.array([img for img in ppt.tiff_read_file(fn, maxframes=2000)])
    v=np.var(mov,0)
    m=np.mean(mov,0)
    gain=v/m

    plt.figure()
    plt.hist(gain.flatten(),bins=500)
    plt.title('Gain histogram')
        
    plt.figure()
    plt.imshow(gain)
    plt.colorbar()
    plt.title(f'Approximate gain: var(pixels)/mean(pixels). Mean gain:{np.mean(gain):.3f}')
    return gain


def ls_localize(fn, sigma, roisize=12, 
                sigmaDetectorThreshold=20, 
                detectorThreshold2=2,
                cameraGain=0.5, cameraOffset=102.6,
                maxframes=1000):
    
    fn_noext = os.path.splitext(fn)[0]
    offsets_fn= fn_noext+"_offsets.pickle"
    with open(offsets_fn, "rb") as f:
        fov_offsets = pickle.load(f).astype(np.int32)

    outdir = fn_noext + "_results/"
    os.makedirs(outdir, exist_ok=True)

    cachedir = fn_noext + "_cache/"
    os.makedirs(cachedir, exist_ok=True)
        
    rois_fn = fn_noext+"_cache/rois.npy"

    #approx_gain(fn)    
    #return None
    
    border = 60

    def crop_frames(src):
        for img in src:
            yield img[border:-border,border:-border]
    
    # Use the mean of the first 200 frames as a spot detector background offset
    sdbg = (np.mean([img for img in crop_frames(ppt.tiff_read_file(fn, maxframes=1000))], 0) - cameraOffset) / cameraGain
    plt.figure()
    plt.imshow(sdbg)
    plt.savefig(outdir + "spotdetect-bg.png")
    plt.close()
    
    imgshape = sdbg.shape
    full_fov = np.array(imgshape)
    full_fov[0] += np.max(fov_offsets)
    
    chisqThreshold = 2*roisize**2
    
    detectionSigma=2
        
    with Context(debugMode=False) as ctx:
        sd = SpotDetector(sigma, roisize=roisize,minIntensity=sigmaDetectorThreshold, backgroundImage=sdbg)
        calib = process_movie.create_calib_obj(cameraGain, cameraOffset, imgshape, ctx)

        tiffread = lambda: crop_frames(ppt.tiff_read_file(fn, maxframes=maxframes))
        numrois,numframes = process_movie.detect_spots(sd, calib, tiffread(), sumframes=1, output_fn=rois_fn, batch_size=20000, ctx=ctx)
        print(f"Extracted {numrois} ROIs from {numframes} frames - high threshold {sigmaDetectorThreshold} to estimate accurate PSF sigma..")

        psf = Gaussian(ctx).CreatePSF_XYITiltedBg(roisize, cuda=True)
        sigmas = np.repeat([[detectionSigma,detectionSigma]], numrois, 0)
        locs = process_movie.localize_rois(rois_fn, psf, constants=sigmas)

        initial_estim = np.zeros((len(locs.estim),8))
        initial_estim[:,:6] = locs.estim
        initial_estim[:,6:] = 3
        
        psf_sigma = Gaussian(ctx).CreatePSF_XYITiltedBgSigmaXY(roisize, detectionSigma, cuda=True)
        locs_sigma = process_movie.localize_rois(rois_fn, psf_sigma, initial_estim=initial_estim)
        plt.figure()
        plt.hist(locs_sigma.chisq, bins=50, range=[0,chisqThreshold*2])
        plt.title(f'Chi square values for X,Y,I,tilted bg,sx,sy fits. Median={np.median(locs_sigma.chisq)}')

        print(f'Applying chi square filter (threshold={chisqThreshold})...')
        locs_sigma.Filter(locs_sigma.chisq<chisqThreshold)
        ds_sigma = Dataset.fromQueueResults(locs_sigma, imgshape)

        plt.figure()
        plt.hist(ds_sigma.data.estim.sigma,bins=50)
        plt.title('sigma')
        plt.savefig(outdir + "sigma.png")
        #plt.close()

        # Reprocess with fixed sigma
        sigma = np.median(ds_sigma.data.estim.sigma, 0)        
        print(f"Sigma:{sigma}",flush=True)
        
        psf = Gaussian(ctx).CreatePSF_XYITiltedBg(roisize, cuda=True)
        sigmas = np.repeat([sigma], numrois, 0)
        locs = process_movie.localize_rois(rois_fn, psf, constants=sigmas)
        locs.Filter(locs.chisq<chisqThreshold)
        
        sigmas = np.repeat([sigma], len(locs.estim), 0)
        sample = psf.GenerateSample(locs.estim, constants=sigmas)
        chisq_perfect = psf.ChiSquare(locs.estim, sample, constants=sigmas)
        
        plt.figure()
        plt.hist(chisq_perfect, bins=100, range=[0,3*roisize**2])
        plt.title('Chi square for resampled ROIs')

        plt.figure()
        plt.hist(locs.chisq, bins=100, range=[0,3*roisize**2])
        plt.title(f'Chi square values for X,Y,I,tilted bg fits. Median={np.median(locs.chisq)}')

        # Add offsets depending on frames
        ds = Dataset.fromQueueResults(locs, full_fov, pixelsize=65)
        ds.sigma = sigma
        ds.pos[:,1] += fov_offsets[(ds.frame%100)%(len(fov_offsets))]
        ds.save(outdir +"locs.hdf5")
        drift,est_precision = ds.estimateDriftMinEntropy(framesPerBin=50,initializeWithRCC=True,maxdrift=8)
        ds.applyDrift(drift)
        ds.save(outdir + "locs_undrifted.hdf5")

        
    return ds,fov_offsets

#fn = 'C:/data/LS/2409 nanoruler/0.15sec/movie.tif'
#fn = 'C:/data/LS/2409 nanoruler/0.10sec/movie.tif'
fn = 'D:/data/sols/11/movie.tif'

sigma = 2.5
ds,fov_offsets=ls_localize(fn, sigma, sigmaDetectorThreshold=50, maxframes=0, detectorThreshold2=10)

#print_crlb()

#if r is not None:
#    ds,fov_offsets,estim,chisq=r
