import os
import numpy as np
import matplotlib.pyplot as plt
from photonpy import Context, GaussianPSFMethods, SpotDetector, PSFCorrelationSpotDetector, Dataset
import photonpy.smlm.process_movie as process_movie
import pickle

import photonpy.utils.multipart_tiff as ppt

from scipy.ndimage import  gaussian_filter

def view_rois(pixels):
    import napari
    with napari.gui_qt():
        napari.view_image(pixels)

def crop_frames(src, aoi):
    for img in src:
        yield img[aoi['top']:aoi['top']+aoi['height'], aoi['left']:aoi['left']+aoi['width']]

def get_backgrounds(frames, galvosteps, outdir):
    bg = np.zeros((galvosteps, frames.shape[1], frames.shape[2]), dtype=np.float32)
    for i in range(galvosteps):
        bg[i] = np.median(frames[i::galvosteps],0)
        bg[i] = gaussian_filter(bg[i], sigma=10)

        plt.figure()
        plt.imshow(bg[i])
        plt.savefig(outdir + f"spotdetect-bg-step{i}.png")

    return bg


def finetuneStepOffsets(ds:Dataset, numsteps):
    ds = ds[:]
    ds.frame = ds.frame % numsteps
          
    drift, prec = ds.estimateDriftMinEntropy(framesPerBin=1, initializeWithRCC=False)
        

    return drift    

def ls_localize(fn, aoi, sigma,
                roisize=10, 
                detectorThreshold=20, 
                cameraGain=0.5, cameraOffset=100,
                maxframes=1000, maxCrlb=0.2, framesPerBin=500,
                pixelsize=108.3, chisqThreshold=300):

    fn_noext = os.path.splitext(fn)[0]
    
    offsets = np.loadtxt(os.path.split(fn)[0]+'/offset.txt')
    offsets -= np.min(offsets,0)
    print( f'Galvo offsets: {offsets}' )
    galvoSteps = len(offsets)

    outdir = fn_noext + "_results/"
    os.makedirs(outdir, exist_ok=True)

    cachedir = fn_noext + "_cache/"
    os.makedirs(cachedir, exist_ok=True)
        
    rois_fn = fn_noext+"_cache/rois.npy"

    mov = (np.array([img for img in crop_frames(
        ppt.tiff_read_file(fn, startframe=galvoSteps*1000, maxframes=galvoSteps*1400), aoi)]) - cameraOffset) * cameraGain

    sdbg = get_backgrounds(mov, galvoSteps, outdir)
    sdbg = sdbg.mean(0)

    plt.figure()
    plt.imshow(sdbg)
    plt.savefig(outdir + 'spotdetect-bg.png')

    imgshape = sdbg.shape
    full_fov = np.array(imgshape)
    full_fov[0] += np.max(offsets[:,1])
        
    detectionSigma=2
        
    with Context(debugMode=False) as ctx:
        sd = SpotDetector(sigma, roisize=roisize,minIntensity=detectorThreshold, backgroundImage=sdbg)
        calib = process_movie.create_calib_obj(cameraGain, cameraOffset, imgshape, ctx)

        gauss_api = GaussianPSFMethods(ctx)

        tiffread = lambda: crop_frames(ppt.tiff_read_file(fn, maxframes=maxframes), aoi)
        numrois,numframes = process_movie.detect_spots(sd, calib, tiffread(), sumframes=1, output_fn=rois_fn, batch_size=20000, ctx=ctx)
        print(f"Extracted {numrois} ROIs from {numframes} frames - high threshold {detectorThreshold} to estimate accurate PSF sigma..")

        psf = gauss_api.CreatePSF_XYITiltedBg(roisize, cuda=True)
        sigmas = np.repeat([[detectionSigma,detectionSigma]], numrois, 0)
        locs = process_movie.localize_rois(rois_fn, psf, constants=sigmas)

        initial_estim = np.zeros((len(locs.estim),8))
        initial_estim[:,:6] = locs.estim
        initial_estim[:,6:] = 3
        
        psf_sigma = gauss_api.CreatePSF_XYITiltedBgSigmaXY(roisize, detectionSigma, cuda=True)
        locs_sigma = process_movie.localize_rois(rois_fn, psf_sigma, initial_estim=initial_estim)
        plt.figure()
        plt.hist(locs_sigma.chisq, bins=50, range=[0,chisqThreshold*2])
        plt.title(f'Chi square values for X,Y,I,tilted bg,sx,sy fits. Median={np.median(locs_sigma.chisq)}')
        plt.legend()
        plt.savefig(outdir + 'chisq-sigmafits.png')

        print(f'Applying chi square filter (threshold={chisqThreshold})...')
        locs_sigma.Filter(locs_sigma.chisq<chisqThreshold)
        ds_sigma = Dataset.fromQueueResults(locs_sigma, full_fov, pixelsize=pixelsize)

        ds_sigma = ds_sigma[np.max(ds_sigma.crlb.pos,1) < 0.2]
        ds_sigma.pos -= offsets[ds_sigma.frame%galvoSteps]
        ds_sigma.save(outdir +"locs-sigma.hdf5")

        plt.figure()
        plt.hist(ds_sigma.data.estim.sigma,bins=50, label=['X','Y'])
        plt.title('sigma')
        plt.legend()
        plt.savefig(outdir + "sigma.png")
        #plt.close()

        # Reprocess with fixed sigma
        sigma = np.median(ds_sigma.data.estim.sigma, 0)        
        print(f"Sigma:{sigma}",flush=True)
        
        psf = gauss_api.CreatePSF_XYITiltedBg(roisize, cuda=True)
        sigmas = np.repeat([sigma], numrois, 0)
        locs = process_movie.localize_rois(rois_fn, psf, constants=sigmas)
        print('Filtering on chi-square: ')
        locs.Filter(locs.chisq<chisqThreshold)
        
        # Add offsets depending on frames
        ds = Dataset.fromQueueResults(locs, full_fov, pixelsize=pixelsize)
        ds = ds[np.max(ds.crlb.pos,1) < maxCrlb]
        print(f'Remaining after crlb filter: {len(ds)}')
        ds.sigma = sigma
        ds.pos -= offsets[ds.frame%galvoSteps]
        ds.save(outdir +"locs.hdf5")
        
        #for i in range(galvoSteps):
        #    ds[ds.frame % galvoSteps == i].save(outdir + f'locs-step{i}.hdf5')
        
        ds_undrifted = ds[:]
        
        drift = ds.estimateDriftRCC(ds.numFrames // 20)# estimateDriftMinEntropy(framesPerBin=framesPerBin,initializeWithRCC=True,maxdrift=8, pixelsize=pixelsize)
        ds_undrifted.applyDrift(drift)
        ds_undrifted.save(outdir + "locs_undrifted.hdf5")

        fine_offsets = finetuneStepOffsets(ds_undrifted, numsteps=galvoSteps)
        plt.savefig('galvostep-dme.png')
        plt.savefig('galvostep-dme.svg')
        
        # apply 
        ds.pos -= fine_offsets[ds.frame % galvoSteps]
        drift,est_precision = ds.estimateDriftMinEntropy(framesPerBin=framesPerBin, 
                                                         initialEstimate=drift,pixelsize=pixelsize, maxdrift=8, apply=True)
        
        drift=ds.estimateDriftRCC(framesPerBin=3000)
        ds.applyDrift(drift)
        
        ds.save(outdir + 'locs_galvo_undrifted.hdf5')
        
        plt.figure()
        plt.plot(offsets[:,1], label='Bead offset')
        plt.plot(offsets[:,1] + fine_offsets[:,1], label='Bead offset + DME offset')
        plt.legend()
        
                        
    return ds, offsets, fine_offsets

if True:
    fn = 'C:/data/cos7-2605-1_c/cos7-X0.tif'
    aoi = {
           'top': 0,
           'height': 170,
           'left':0,
           'width':512
    }


if False:
    fn = 'C:/data/nanoruler4_c/cos7-X0.tif'
    
    aoi = {
           'top': 0,
           'height': 150,
           'left':0,
           'width':512
    }

if False:
    fn = 'C:/data/nanoruler2/cos7-X0.tif'

    aoi = {
           'top': 130,
           'height': 70,
           'left':64,
           'width':512-64
    }


sigma = 2.5

ds, offsets, fine_offsets = ls_localize(fn, aoi, sigma, detectorThreshold=10, maxframes=0,
                maxCrlb=0.3,framesPerBin=400,
                chisqThreshold=200)

#print_crlb()

#if r is not None:
#    ds,fov_offsets,estim,chisq=r
