import os.path
import sys
import pptk
import argparse
import tifffile
import numpy as np
import pandas as pd
from tqdm import tqdm
import time

sys.path.append( '../..' )
import photonpy.cpp.spotdetect as spotdetect
from photonpy.cpp.lib import SMLM
from photonpy.cpp.context import Context
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
from photonpy.cpp.bspline import BSplinePSF_Params, BSpline
from projects.bsplines.bsp_utils.csv_to_3D_plot import plot_pandas_data
from photonpy.cpp.cspline import CSpline, CSpline_Calibration
from photonpy.cpp.estim_queue import EstimQueue
from photonpy.cpp.spotdetect import PSFConvSpotDetector
import projects.bsplines.bspline_fit.localize_3D as bsploc

import photonpy.smlm.psf as psffuncs

# from simflux.tiff_to_locs import create_calib_obj
# from simflux.tiff_to_locs import process_movie
# import simflux.read_tiff as read_tiff


# flag for activating some additional output for Debug purposes
_DEBUG_MODE = False
# if True, the debug version of SMLM library will be used
_USING_DEBUG_SMLM = False


def process_movie_gaussian_2d(movie):
    psfSigma = 2
    roisize = 14
    min_intensity = 100

    localizations = []
    cuda = False

    Is = []
    with SMLM(debugMode=False) as smlm, Context(smlm) as ctx:
        spotDetectorConfig = spotdetect.SpotDetectorConfig(
            psfSigma, roisize, minIntensity=min_intensity
        )
        processFrame = spotdetect.SpotDetectionMethods(ctx).ProcessFrame

        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, psfSigma, cuda)
        for i, f in enumerate(tqdm(movie)):
            rois, cornerpos, scores = processFrame(f, spotDetectorConfig, 100)
            theta, diag, traces = psf.ComputeMLE(rois)
            crlb = psf.CRLB(theta)

            for c, t, crlb, trace, d in zip(cornerpos, theta, crlb, traces, diag):
                x = c[1] + t[0]
                y = c[0] + t[1]
                z = 0

                xs = t[0]
                ys = t[1]
                zs = 0
                Is.append(t[2])

                localizations.append(
                    (i, x, y, z, crlb[0], crlb[1], crlb[2], xs, ys, zs)
                )
    return localizations


def lower_intensity(imin, psplit):
    eps = np.finfo(float).eps
    epsy = 100 * eps
    tempim = np.round(imin).astype("int32")

    diffim = imin - tempim
    imoutA = np.random.binomial(tempim, psplit)
    imoutA = imoutA + (imoutA / (imin + epsy)) * diffim
    imoutB = imin - imoutA
    return imoutA.astype("float64")


def process_movie_splines(
    movie_file: str,
    calibration_file: str,
    offset: float,
    emGain: float,
    roisize: int,
    psf_sigma: float,
    min_intensity: int,
    method: str,
):
    """Localize points with better precision from a blinking movie file.

    Using B-spline model from a calibration stacks file (.mat) we can acquire
    better localization precision of the fluorophores from the blinking
    molecules file (.tif), so called movie file.

    :param movie_file: (str) filename of .tif the movie file with blinking
        molecules. Every slice means time slice, so it should contain some
        blinking spots.

    :param calibration_file: (str) filename of .mat file with all needed
        B-spline parameters like B-net coefficients. All parameters:
        'spline',    # structure with B-spline parameters like vertices with
                     # coordinates, tetrahedrons, polynomial degree,
                     # continuity order, spline dimension, B-net coefficients
        'spline_x',  # 1st order directional derivatives in B-form
        'spline_y',
        'spline_z',
        'XI',        # positions of every pixels according to
                     # the average PSF stack
        'Z',         # ???
        'n_pixels',  # full ROI size that was used for producing B-spline PSF model
        'n_stacks',  # number of calibration stacks used for producing
                     # B-spline PSF model
        'n_zslices', # number of z slices in one PSF stack that was used for
                     # producing B-spline PSF model
        'z_range'    # PSF can change in interval [-z_range/2, +z_range/2] um

    :param offset: (float) camera parameter - offset in number of photons

    :param emGain: (float) camera parameter - EM Gain.
        Usually in number of photons. The movie will be calculated as
        movie = (movie - offset) / emGain

    :param roisize: (int) number of pixels for one PSF for cutting it
        out and for estimation.
        It should be smaller than ROI for B-spline calibration.

    :param psf_sigma: (float) estimated sigma for cutting PSFs from the movie.

    :param min_intensity: (float) number of photons that will be estimated
        as noise. Increase this parameter if you have a lot more points in the
        final image than expected.

    :param method: (str) method that are used for fitting and that were used
        for producing the calibration PSF stack model.
        Now can be "bspline" and "cspline"

    :return: localizations (2D numpy array) with out parameters of localization:
        First index is a bead index.
        localizations[:,0] - index. Always go from 0 to Nbeads-1
        localizations[:,1] - estimated x position in pixels (?)
        localizations[:,2] - estimated y position
        localizations[:,3] - estimated z position
        localizations[:,4] - CRLB x
        localizations[:,5] - CRLB y
        localizations[:,6] - CRLB z
        localizations[:,7] - some value that regulates color
        localizations[:,8] - some value that regulates color
        localizations[:,9] - some value that regulates color
    """

    # now this flag should be always equal to True
    cuda = True

    if method == "bspline":
        CalibrationClass = BSplinePSF_Params
        ModelClass = BSpline
    elif method == "cspline":
        CalibrationClass = CSpline_Calibration
        ModelClass = CSpline
    else:
        raise Exception(f'Unknown method type "{method}"')

    start_time = time.time()
    with Context(debugMode=False) as ctx:
        # creating B-spline calibration object from file with B-coefficients
        print(f"Reading calibration file\n\t{calibration_file}")
        calib = CalibrationClass.from_file(calibration_file)
        psf = ModelClass(ctx).CreatePSF_XYZIBg(roisize, calib, cuda=cuda)
        psf.SetLevMarParams(10, 40)

        # min intensity to detect any blinking as PSF
        detection_threshold = 1000
        maxFilterSizeXY = 5  # ???

        # read .tif movie file
        print(f"Reading movie file\n\t{movie_file}")
        movie = None
        with tifffile.TiffFile(movie_file) as tif:
            movie = tif.asarray()

        bgimg = movie[0] * 0  # ???
        # number of z layers (slices)
        n_zslices = calib.n_zslices
        # full z range in um. [-z_range/2.0, +z_range/2.0]
        z_range = calib.z_range
        psf_zrange = np.linspace(-z_range / 2.0, z_range / 2.0, n_zslices)
        psfstack = psffuncs.psf_to_zstack(psf, psf_zrange)

        # this sets up the template-based spot detector.
        # MinPhotons is not actually photons, still just AU.
        sd = PSFConvSpotDetector(
            psfstack,
            bgimg,
            minPhotons=detection_threshold,
            maxFilterSizeXY=maxFilterSizeXY,
            debugMode=False,
        )

        # roipos_in_movie - left upper corner of movie in pixels (int, int) with
        #                   psf
        print(f"\ncutting PSFs from the movie file...")
        roipos_in_movie, cutted_psfs = bsploc.process_movie(movie, sd, roisize, ctx)

        # initial guess for X and Y
        initial_guess = np.ones((len(cutted_psfs), 5)) * [
            roisize / 2,
            roisize / 2,
            0,
            0,
            1,
        ]
        # initial guess for Z
        initial_guess[:, 2] = psf_zrange[roipos_in_movie[:, 2]]
        # initial guess for Intensity (?)
        initial_guess[:, 3] = np.sum(cutted_psfs, (1, 2))

        print("subpixel localization of PSFs...")
        estim, ids, CRLB = bsploc.localization(psf, cutted_psfs, initial_guess)
        cutted_psfs = cutted_psfs[ids]
        roipos_in_movie = roipos_in_movie[ids]

        # Filter out all ROIs with chi-square,
        # this gets rid of ROIs with multiple spots.
        print(
            f"calculating expected value to get rid of ROIs with bad "
            f"chi-square and multiple spots..."
        )
        expval = psf.ExpectedValue(estim)
        chisq = np.sum((cutted_psfs - expval) ** 2 / (expval + 1e-9), (1, 2))

        std_chisq = np.sqrt(2 * psf.samplecount + np.sum(1 / np.mean(expval, 0)))

        # Filter out all spots that have a chi-square > expected value + 2 * std.ev.
        chisq_threshold = psf.samplecount + 2 * std_chisq
        accepted = chisq < chisq_threshold
        rejected = np.logical_not(accepted)
        print(
            f"Chi-Square threshold: {chisq_threshold:.1f}. "
            f"Removing {np.sum(accepted == False)}/{len(cutted_psfs)} spots"
        )

    end_time = time.time()
    print(f"\nTotal time of fitting = {end_time - start_time} sec")

    # writing results
    print("Writing result in the array...")
    Npsfs_found = len(estim)
    localizations = np.zeros((Npsfs_found, 10), dtype=np.float32)
    localizations[:, 0] = np.linspace(0, Npsfs_found - 1, num=Npsfs_found)
    # I don't know why the x,y shifts are confused, but they are
    localizations[:, 1] = estim[:, 0] + roipos_in_movie[:, 1]
    localizations[:, 2] = estim[:, 1] + roipos_in_movie[:, 0]
    localizations[:, 3] = estim[:, 2]

    localizations[:, 4] = CRLB[:, 0]
    localizations[:, 5] = CRLB[:, 1]
    localizations[:, 6] = CRLB[:, 2]

    zmin = np.min(localizations[:, 3])
    zmax = np.max(localizations[:, 3])

    localizations[:, 7] = 0.0
    localizations[:, 8] = (localizations[:, 3] - zmin) / (zmax - zmin)
    localizations[:, 9] = 0.0

    return localizations, cutted_psfs


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process TIFF file")

    parser.add_argument("-i", "--input", required=True, help="Input TIFF file (.tif)")

    parser.add_argument(
        "-o",
        "--output",
        default=None,
        help="Output file. Can be .csv or .xlsx, or .pkl",
    )

    parser.add_argument(
        "--calibration", required=True, help="Path to file with calibration PSF stacks"
    )

    # only B-spline (bspline) and C-spline (cspline) method can be used now
    parser.add_argument("--method", default="bspline", help="Proccessing method")

    parser.add_argument("--offset", type=float, required=True, help="Offset")

    parser.add_argument("--emgain", type=float, required=True, help="EM gain")

    parser.add_argument(
        "--roi",
        type=int,
        required=True,
        help="Region Of Interest size in pixels " "(x,y pixel size of one PSF)",
    )

    parser.add_argument(
        "--minintensity",
        type=float,
        required=True,
        help="Minimal intensity that will be " "considered as noise",
    )

    parser.add_argument(
        "--sigma",
        type=float,
        default=None,
        help="Estimated sigma of PSF. " "By default it is 1/2*(ROI/3-1)",
    )

    parser.add_argument(
        "--plot", default=False, action="store_true", help="Show pointcloud 3D viewer"
    )

    parser.add_argument(
        "-n",
        "--num-frames",
        type=int,
        default=None,
        help="Number of frames to process. " "Leave blank to process all",
    )

    parser.add_argument("--skip", type=int, default=0, help="Number of frames to skip.")

    parser.add_argument(
        "--pixelsize",
        type=float,
        default=-1.0,
        help="Pixelsize in micrometers. Need for 3D plot.",
    )

    args = parser.parse_args()

    # setting sigma by default if it hasn't been set
    if args.sigma is None:
        args.sigma = 0.5 * (args.roi / 3.0 - 1.0)
        if args.sigma < 0:
            raise Exception(
                "Use more pixels for ROI or set the estimated "
                "sigma parameter using --sigma flag "
                "(for example, --sigma 1.666)"
            )

    # choosing method for processing the movie file
    if args.method == "gaussian":
        raise Exception("Please, change Gaussian 2D function for " "processing movie")
        # localizations = process_movie_gaussian_2d( movie )
    elif args.method == "gaussian3d":
        raise Exception("gaussian 3D method for processing movie " "is not implemented")
    elif args.method == "bspline" or args.method == "cspline":
        localizations, all_ROIs = process_movie_splines(
            args.input,
            args.calibration,
            offset=args.offset,
            emGain=args.emgain,
            roisize=args.roi,
            psf_sigma=args.sigma,
            min_intensity=args.minintensity,
            method=args.method,
        )

    dataframe = pd.DataFrame(
        localizations,
        columns=[
            "frame",
            "x",
            "y",
            "z",
            "crlb_x",
            "crlb_y",
            "crlb_z",
            "xs",
            "ys",
            "zs",
        ],
    )

    # saving the results into some file
    if args.output is not None:
        if args.output.endswith(".xlsx"):
            dataframe.to_excel(args.output, index=False)
        elif args.output.endswith(".csv"):
            dataframe.to_csv(args.output, index=False, sep="\t")
        elif args.output.endswith(".pkl"):
            dataframe.to_pickle(args.output)
        else:
            print("Output format not recognized")

    # write .tif file with all cropped ROIs from the movie
    if _DEBUG_MODE:
        out_dir = os.path.dirname(args.output)
        rois_file = os.path.join(out_dir, "all_cropped_ROIs.tif")

        print(f"Saving all cropped ROIs in file\n\t{rois_file}")
        with tifffile.TiffWriter(rois_file) as tif:
            for roi in all_ROIs:
                tif.save(roi, compress=2)
        print("Saving complete")

    # plotting points in 3D viewer
    if args.plot:
        plot_pandas_data(
            data=dataframe,
            xy_pixelsize=args.pixelsize,
            x_name="x",
            y_name="y",
            z_name="z",
        )
