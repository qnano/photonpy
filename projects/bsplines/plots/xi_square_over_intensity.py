import sys
import tifffile
import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from tqdm import tqdm

sys.path.append( '../' )
import bsplines.psfsim.fftpupil_psf as zola3d
from bsplines.bsp_utils.aux_functions import cropping_spots_array, save_PSFs_separately
from bsplines.b_spline_fitter import BSplineFitter
sys.path.append( '../..' )
import photonpy.bspline as bs
from photonpy.smlmlib.base import SMLM
from photonpy.cpp.context import Context
from photonpy.smlm.psf import PSF

from bsplines.calibration.create_calibration import calibrate_b_spline
from bsplines.plots.process_separated_spots import process_separate_spots_movie, \
    comparing_shifts, plot_scattered_points, CRLB_with_real_pos

# debug flag
_DEBUG = True
# if true, debug version of SMLM library will be used
_DEBUG_SMLM = False
#  random seed value. Use None in order to use system time as the seed
_RANDOM_SEED_VALUE = 1 # None

# default name for xi2 over intensity graph between
# theory: E[Xi2] = K (number of pixels)
# and experiment: xi2 calculated between PSF model and Poissoned PSF model
_DEFAULT_NAME_FOR_Xi2_EXP_COMPARISON_THEORY_AND_EXPERIMENT = 'xi2_expected_th_and_exper'


def initialize_PSF_generator( ROI:int ):
    """Initializing PSF generator and Zernike polynoms"""

    NOLL_IDX = 6

    zernike_coeff = np.zeros( 20, dtype=np.float32 )
    zernike_coeff[NOLL_IDX - 2] = 1.0

    psf_gen = zola3d.PSF(
        psfsize=ROI,
        lam=0.6,
        pixelsize=0.1,
        NA=1.49,
        maxnoll=len( zernike_coeff ) + 1,
        ref_imm_fluid=1.5,
        ref_sample=1.33
    )

    return psf_gen, zernike_coeff


def save_calibration_PSFs( PSFs, filename:str ):
    """Save calibration PSFs 4D array in the file"""

    store = {
        'PSFs': PSFs
    }

    scipy.io.savemat( filename, store )


def scattered_graph_for_random_subpix_spots_coord( subpix_coordinates,
                                                   filename:str=None ):
    """Creates scattered graph for x,y positions"""

    plt.scatter( subpix_coordinates[:, 0], subpix_coordinates[:, 1] )
    plt.title( 'Randomly generated subpixel positions for spots' )
    plt.xlabel( 'x position, pixels' )
    plt.ylabel( 'y position, pixels' )

    if filename is not None and len( filename ) > 0:
        plt.savefig( filename )

    plt.show()


def  get_spline_estimated_spots( psf:PSF, ROI_spline:int,
                                 subpix_coordinates, z_pos:float,
                                 I:float, bg:float ):
    """Fill estimated spots array using spline PSF object

    :param psf: (PSF) spline PSF object of a PSF model.
    :param ROI_spline: (int) number of pixels through x or y axis for
        the spline model
    :param subpix_coordinates: (2D float array) [spot idx][axis idx:0->x, 1->y]
        subpixel coordinates according to the center in pixels (not in um!)
    :param z_pos: (float) z position in um for this spots.
    :param I: (float) intensity for estimating spots.
    :param bg: (float) background levels for estimating spots.

    :return: (3D float array) [spot idx][x idx][y idx]
        estimated by PSF spline model spots.
    """

    # calculate estimated by spline spots
    centr_pos = ROI_spline / 2.0 - 0.5
    thetas = np.zeros( (N_spots, 5), dtype=np.float32 )
    thetas[:, 0] = centr_pos + subpix_coordinates[:, 0]
    thetas[:, 1] = centr_pos + subpix_coordinates[:, 1]
    thetas[:, 2] = z_pos
    thetas[:, 3] = I
    thetas[:, 4] = bg

    b_spline_estimated_spots = psf.ExpectedValue( thetas )
    return b_spline_estimated_spots


def calc_variance_expected_xi2( spots_real ):
    """Calculate sigma using Xi2 variance with magic formula for Poisson dist."""

    ROI = spots_real.shape[1]
    one_over_spots = 1.0 / spots_real
    one_over_spots[spots_real <= 0] = 0

    one_over_spots_sum_per_spot = np.sum( one_over_spots, axis=(1, 2) )

    variance = 2.0 * ROI * ROI + one_over_spots_sum_per_spot

    return np.average( variance )


def calc_xi2( spots_model, spots_poiss_values ):
    """Calculates Xi2 for estimated and real PSF spots, sigma Xi2"""

    # calc Xi2
    bad_indices = spots_model <= 0

    xi2_per_spot = np.square( spots_model - spots_poiss_values ) / spots_model
    xi2_per_spot[bad_indices] = 0
    xi2_per_spot = np.sum( xi2_per_spot, axis=(1,2) )

    xi2 = np.average( xi2_per_spot )

    return xi2, xi2_per_spot.std( ddof=1 )


def expected_xi2( normalized_spots, intensities, backgrounds,
                  use_poisson_noise:bool=True, folder_name:str=None ):
    N_bg = len( backgrounds )
    N_intensities = len( intensities )
    ROI_spots = normalized_spots.shape[2]

    real_xi2 = np.zeros( (N_bg, N_intensities), dtype=float )
    sigma_real_xi2 = np.zeros( (N_bg, N_intensities), dtype=float )
    variance_xi2 = np.zeros( (N_bg, N_intensities), dtype=float )

    # array for real spots with applied intensity and background
    spots_real = np.zeros( (N_spots, ROI_spots, ROI_spots), dtype=np.float32 )

    for ibg in range( N_bg ):
        for idx in range( len(intensities) ):
            spots_real = normalized_spots * intensities[idx] + backgrounds[ibg]

            if use_poisson_noise:
                spots_estimated = np.random.poisson( spots_real ).\
                    astype( np.float32 )
            else:
                spots_estimated = np.copy( spots_real )

            real_xi2[ibg, idx], sigma_real_xi2[ibg, idx] = \
                calc_xi2( spots_real, spots_estimated )

            variance_xi2[ibg, idx] = calc_variance_expected_xi2( spots_real )

    # check that expected values is equal to the number of pixels
    average_estimated_real_xi2 = np.average( real_xi2 )
    expected_xi2 = ROI_spots * ROI_spots

    average_sigma_per_bg = np.average( sigma_real_xi2, axis=1 )
    std_estimated_real_xi2 = np.average( average_sigma_per_bg )

    print( '\nCalculating sigma for expected xi2:' )
    print( f'average estimated expected xi2 = {average_estimated_real_xi2}' )
    print( f'sigma estimated expected xi2 = {std_estimated_real_xi2}' )
    print( f'expected xi2 value = {expected_xi2}' )

    if average_estimated_real_xi2 - std_estimated_real_xi2 < \
          expected_xi2 < \
              average_estimated_real_xi2 + std_estimated_real_xi2:
        print( '[OK] inside the sigma interval' )
    else:
        print( '[ERROR] outside of sigma interval', file=sys.stderr )

    # it will be good if you figure out how to use comparison between variance
    expected_K = np.zeros( N_intensities, dtype=float  )
    expected_K.fill( expected_xi2 )

    # plot xi2 expected from theory E[Xi2] = K (number of pixels) and compare
    # it with xi2 expected from experiment: Poissoned PSF model compare with
    # just PSF model
    for ibg in range( N_bg ):
        bg = backgrounds[ibg]
        plt.errorbar( intensities, expected_K, yerr=np.sqrt( variance_xi2[ibg] ),
                      label=f'xi2 exp theor.' )
        plt.errorbar( intensities, real_xi2[ibg], yerr=sigma_real_xi2[ibg],
                      label=f'xi2 exp exper.' )

        plt.title( f'Xi2 expected theory and experiment, bg = {bg}' )
        plt.xlabel( 'Intensity' )
        plt.ylabel( 'Xi2' )

        plt.xscale( 'log' )
        plt.legend()

        if _DEBUG:
            filename = folder_name + \
                _DEFAULT_NAME_FOR_Xi2_EXP_COMPARISON_THEORY_AND_EXPERIMENT + \
                f'_bg_{bg}.png'
            plt.savefig( filename )

        plt.show()

    return real_xi2, sigma_real_xi2


def plot_xi2_over_intensity_graph( intensities,
                                   xi2_arr, xi2_sigma,
                                   real_xi2,
                                   sigma_real_xi2,
                                   backgrounds,
                                   title:str,
                                   filename:str=None ):
    """Plot graph with errorbars for Xi2 over intensity and expect. Xi2

    :param intensities: (1D float array) intensities which were used in Xi2
        test.
    :param xi2_arr: (2D float array) [background idx][spot idx]
        Xi2 from comparison between estimated PSF spots and real PSF spots.
    :param xi2_sigma: (2D float array) [background idx][spot idx]
        Xi2 error bar from comparison between estimated PSF spots and
        real PSF spots.
    :param real_xi2: (2D float array) [background idx][spot idx]
        total number of pixels in one PSF z-splice (the same as ROIxROI).
        This number is also equal to the expected value of Xi2.
    :param sigma_real_xi2: (2D float array) [background idx][spot idx]
        Error for expected Xi2 value.
        var[Xi2] =  2*N_spots + sum( 1 / value_i ) over i
        Calculated for every spot and then averaged.
    :param backgrounds: (1D float array) background levels. For every background
        level graphs for estimated and expected xi2 will be produced.
    :param title: (str) title for the Xi2 over intensity graph.
    :param filename: (str) filename for saving the graph.
    """

    if np.sum( xi2_sigma < 0.0 ) > 0:
        print( 'ERROR: all xi2 sigmas should be >= 0 !' )
        print( f'\tyou have {np.sum( xi2_sigma<0.0 )} negative values' )

    for ibg in range( len( backgrounds ) ):
        plt.errorbar( intensities, xi2_arr[ibg], yerr=xi2_sigma[ibg],
                      label=f'xi2 est bg={backgrounds[ibg]}', marker='x' )

        plt.errorbar( intensities, real_xi2[ibg], yerr=sigma_real_xi2[ibg],
                      label=f'xi2 exp bg={backgrounds[ibg]}', marker='x' )

    plt.legend()

    aver_xi2_exp = np.average( real_xi2 )
    xi2_limit = 3.0 * aver_xi2_exp
    xi2_bottom_limit = aver_xi2_exp - 10.0 - 2.0 * np.max( sigma_real_xi2 )
    plt.ylim( bottom=xi2_bottom_limit, top=xi2_limit )

    plt.title( title )
    plt.ylabel( 'Xi2' )
    plt.xlabel( 'Intensity' )
    plt.xscale( 'log' )

    if filename is not None and len( filename ) > 0:
        plt.savefig( filename )

    plt.show()


def save_xi2_over_intensity_into_txt( xi2_filename_txt:str,
                                      intensities, backgrounds,
                                      xi2_arr, xi2_sigma_arr,
                                      real_xi2, sigma_real_xi2 ):
    """Saves xi2 results into .txt file

    For parameters see plot_xi2_over_intensity_graph() function.
    """

    f = open( xi2_filename_txt, 'w+' )

    N_bg = len( backgrounds )

    # creating the title string for a .txt file
    title_s = 'I'
    for ibg in range( N_bg ):
        bg = backgrounds[ibg]
        title_s += f'\txi2_bg_{bg}\t' \
                   f'xi2_sigma_bg_{bg}\t' \
                   f'xi2_exp_bg_{bg}\t' \
                   f'xi2_sigma_exp_bg_{bg}'

    f.write( title_s + '\n' )

    # writing data to the file
    for ispot in range( len(intensities) ):
        s = f'{intensities[ispot]}'

        for ibg in range( N_bg ):
            s += f'\t{xi2_arr[ibg, ispot]}' \
                 f'\t{xi2_sigma_arr[ibg, ispot]}\t' \
                 f'{real_xi2[ibg, ispot]}' \
                 f'\t{sigma_real_xi2[ibg, ispot]}'

        f.write( s + '\n' )

    f.close()


def plot_CRLB_and_sigma( CRLB_real, CRLB_est, sigma_std,
                         intensities, backgrounds,
                         filename_base:str ):
    """Plot sigma, CRLB real, CRLB estimated graphs.

    One graph per axis (x,y) and one graph per background level.

    :param CRLB_real: (3D array) [axis: 0->x, 1->y][bg index][intensity index]
        CRLB with real subpixel positions of the points.

    :param CRLB_est: (3D array) [axis: 0->x, 1->y][bg index][intensity index]
        CRLB estimated after fitting stage of the spline model into data.

    :param sigma_std: (3D array) [axis: 0->x, 1->y][bg index][intensity index]
        STD sigma for estimated subpixel position. Calculated from difference
        between real subpixel positions and the estimated ones.

    :param intensities: (1D array) intensities which were used for experiments.
    :param backgrounds: (1D array) background levels. Usually, small array
        which contains only 1-3 elements.

    :param filename_base: (str) base name for .png files. To this file
        bg level and x or y will be added: "base_bg_{bg}_{x or y}.png"
    """

    # [axis][ibg][intensity]
    N_bg = len( backgrounds )

    for iaxis in [0, 1]:
        if iaxis == 0:
            name = 'x'
        else:
            name = 'y'

        for ibg in range( N_bg ):
            plt.plot( intensities, CRLB_real[iaxis, ibg],
                      label='CRLB real', marker='x' )
            plt.plot( intensities, CRLB_est[iaxis, ibg],
                      label='CRLB est', marker='x' )
            plt.plot( intensities, sigma_std[iaxis, ibg],
                      label='sigma std', marker='x' )

            plt.xscale( 'log' )
            plt.yscale( 'log' )

            plt.legend()
            plt.xlabel( 'intensity' )
            plt.ylabel( f'sigma {name}, pixels' )
            plt.title( f'CRLB and std for bg={backgrounds[ibg]}' )

            if filename_base is not None:
                plt.savefig( filename_base + \
                             f'_bg_{backgrounds[ibg]}_{name}.png' )

            plt.show()




if __name__ == '__main__':
    np.random.seed( _RANDOM_SEED_VALUE )

    ####### PARAMETERS #########################################################
    folder_name = '../../../../OUTPUT/xi2_over_intensity/'
    PSFs_calibr_stacks_file = folder_name + 'simulation_calibration_PSFs.mat'
    PSFs_calibr_stack_with_shifts = folder_name + 'prepeared_calibration_PSFs.mat'
    b_spline_data_file = folder_name + 'b_spline_data.mat'

    # debug filenames
    normalized_spots_file = folder_name + 'normalized_generated_spots.tif'
    estimated_spots_file = folder_name + 'esimated_spots.tif'
    difference_in_spots_file = folder_name + 'difference_in_spots'
    scattered_coordinate_plot_file = folder_name + 'random_coordinates.png'
    precision_graph_base_name = folder_name + 'precision_graph'
    xi2_filename = folder_name + 'xi2_over_intensity.png'
    xi2_filename_txt = folder_name + 'xi2_over_intensity.txt'
    sigma_over_intensity_file_base = folder_name + 'sigma_over_intensity'

    ### CONTROL PARAMETERS
    # if True, MATLAB B-spline fitting stage will be launched asynchronously
    use_matlab_async = True
    # if True, number of points for building sigma over intensity graphs
    # will be filtered by the max number of iterations
    use_filtering_for_number_of_iterations = False
    # if True, sigma over intensity will be calculated and plots will be produced
    build_sigma_over_intensity_graphs = True
    ###

    ### Calibration stack parameters
    # number of calibration PSFs to generate
    N_calibr = 9 # 6
    # intensity for generation calibration PSFs
    I_calibr = 1e5 # 1e5
    # background level for generation calibration PSFs
    bg_calibr = 20 # 20
    # if True, Poisson noise will be used for spots
    use_poisson_noise_for_spots = True
    # if True, Poisson noise will be used for calibration PSF stacks
    use_poisson_noise_for_calibr = True
    # z range for full PSF stack in um. PSF changes in [-z_range, +z_range]
    z_range = 0.6
    # number of z slices for one PSF stack
    n_z_slices = 151
    # number of pixels for one PSF z slice for x or y direction
    ROI_calibr = 15
    # range in pixels for random subpixel position for calibration PSF stacks
    # [-range_for_shifts[i], +range_for_shifts[i]]
    range_for_shifts = [0.4999, 0.4999, 0.08] # [0.4999, 0.4999, 0.08]
    # pixelsize in X,Y in um
    pixelsize = 0.1

    ### B-spline parameters
    polynomial_degree = 5
    n_mesh_cubes = 5
    continuity = 1

    ### Fixed spots parameters for Xi2 comparison
    # z position in um for plotting Xi2 over intensity figure
    z_position_for_test = 0.0
    # uniform distribution will be used for generating spots coordinates
    # in X: [-subpixel_generation_range[0], +subpixel_generation_range[0]]
    # in Y: [-subpixel_generation_range[1], +subpixel_generation_range[1]]
    subpixel_generation_range = [0.4, 0.4]
    # number of spots to generate
    N_spots = 2000
    # number of pixels for one z-slice spot. Should be less than ROI_calibr
    ROI_spots = 13
    # background level for spots
    bg_spots = [1] # [1, 10, 100]

    ### Xi2 varying parameters
    intensities = np.logspace( 1, 5, num=15 )
    ############################################################################


    # parameters checks
    if ROI_spots >= ROI_calibr:
        raise Exception( 'ROI for spots should be less than ROI for '
                         'calibration' )
    if ROI_spots % 2 != ROI_calibr % 2:
        raise Exception( 'if ROI calibration is odd, than ROI spots should '
                         'be also odd.\n'
                         'If ROI calibration is even, than ROI spots '
                         'should be also even.' )

    # PSF generator and Zernike amplitudes
    print( 'Initializing PSF generator...' )
    psf_gen, zernk_ampl = initialize_PSF_generator( ROI=ROI_calibr )

    # generate calibration stacks
    print( 'Initializing random subpixel positions for PSFs '
           'calibration stacks...' )
    random_subpix_pos = np.zeros( (N_calibr, 3), dtype=np.float32 )
    for i in range( random_subpix_pos.shape[1] ):
        random_subpix_pos[:, i] =np.random.uniform( -range_for_shifts[i],
                                                    range_for_shifts[i],
                                                    size=N_calibr )

    print( 'Generating calibration PSFs...' )
    PSF_calibr_stacks = np.zeros(
        (N_calibr, n_z_slices, ROI_calibr, ROI_calibr ), dtype=np.float32
    )

    z_step = 2.0 * z_range / ( n_z_slices - 1 )

    if _DEBUG:
        print( 'Generated subpixel positions for calibration PSFs:' )
        print( 'x\ty\tz' )
        print( random_subpix_pos, '\n' )

    random_subpix_pos_um = np.copy( random_subpix_pos )
    random_subpix_pos_um[:, 0] *= pixelsize
    random_subpix_pos_um[:, 1] *= pixelsize
    random_subpix_pos_um[:, 2] *= z_step

    for istack in tqdm( range( N_calibr ), desc='Gen. calib. PSFs' ):
        PSF_calibr_stacks[istack] = psf_gen.psf3D(
            offsetXYZ=random_subpix_pos_um[istack, :],
            zern_coeff=zernk_ampl,
            z_range=z_range,
            n_z_slices=n_z_slices,
            normalize=True )
    print( '' )

    # use intensity, background and Poisson noise for PSF stacks
    print( 'Applying intensity, background' )
    PSF_calibr_stacks *= I_calibr
    PSF_calibr_stacks += bg_calibr

    if use_poisson_noise_for_calibr:
        print( '\tand Poisson noise' )
        PSF_calibr_stacks = np.random.poisson( PSF_calibr_stacks )
    print( '\tto calibration PSFs...' )

    # saving calibration PSFs
    print( f'Saving calibration PSFs into\n\t{PSFs_calibr_stacks_file}' )
    save_calibration_PSFs( PSF_calibr_stacks, PSFs_calibr_stacks_file )

    if _DEBUG:
        print( f'\tsaving calibration PSFs into\n\t\t{folder_name}...' )
        save_PSFs_separately( PSF_calibr_stacks, folder_name,
                              base_name='calibr_PSF' )

    # calculate shifts
    print( 'Calculate shifts for calibration PSFs...' )
    calibrate_b_spline( movie_name=PSFs_calibr_stacks_file,
                        mat_outfile=PSFs_calibr_stack_with_shifts,
                        roisize=ROI_calibr,
                        zrange=z_range,
                        zstep=z_step,
                        offset=0,
                        emGain=1,
                        conversion=1,
                        shifts_replacement=None )

    # fit B-spline model using B-spline code from MATLAB
    # P.S. can be launched in parallel with spots generation
    print( 'Launching B-spline fitter...' )
    b_fitter = BSplineFitter( input_calibr_PSFs_file=PSFs_calibr_stack_with_shifts,
                              output_bspline_file=b_spline_data_file,
                              polynomial_degree=polynomial_degree,
                              n_mesh_cubes=n_mesh_cubes,
                              continuity_order=continuity,
                              use_matlab_async=use_matlab_async )
    b_fitter.do_fitting()
    print( 'B-spline fitting stage is finished' )

    # spots generation using PSF generator (normalized spots)
    print( 'Generating uniformly distributed subpixel positions...' )
    subpix_coordinates = np.zeros( (N_spots, 2), dtype=np.float32 )
    subpix_coordinates[:, 0] = np.random.uniform( -subpixel_generation_range[0],
                                                  subpixel_generation_range[0],
                                                  size=N_spots )
    subpix_coordinates[:, 1] = np.random.uniform( -subpixel_generation_range[1],
                                                  subpixel_generation_range[1],
                                                  size=N_spots )

    if _DEBUG:
        scattered_graph_for_random_subpix_spots_coord( subpix_coordinates,
                                    filename=scattered_coordinate_plot_file )

    #     translate to micrometers
    subpix_coordinates_um = subpix_coordinates * pixelsize

    normalization_constant = psf_gen.get_normalization_constant(
        zern_coeff=zernk_ampl,
        z_range=z_range,
        n_z_slices=n_z_slices
    )

    normalized_spots = np.zeros( (N_spots, ROI_calibr, ROI_calibr),
                                 dtype=np.float32 )

    print( 'Generating normalized spots...' )
    for ispot in tqdm( range( N_spots ), desc='Gen. norm. spots' ):
        emXYZ = [subpix_coordinates_um[ispot, 0],
                 subpix_coordinates_um[ispot, 1],
                 z_position_for_test]

        normalized_spots[ispot], _ = psf_gen.psf( emXYZ, zernk_ampl )
    print( '' )

    normalized_spots /= normalization_constant
    print( f'Cropping spots to {ROI_spots} ROI size...' )
    normalized_spots = cropping_spots_array( normalized_spots, ROI_spots )

    #############################################################################
    # normalized_spots_sum = np.sum( normalized_spots, axis=(1,2) )
    # normalized_spots /= normalized_spots_sum[:, None, None]
    #############################################################################

    if _DEBUG:
        print( '\tsaving normalized spots into file...\n'
               f'\t\t{normalized_spots_file}' )
        with tifffile.TiffWriter( normalized_spots_file ) as tif:
            tif.save( normalized_spots, compress=2 )


    # load coefficients from MATLAB output file
    ParamsClass = bs.BSplinePSF_Params
    SplineClass = bs.BSpline

    # waiting MATLAB to finish B-spline fitting stage
    b_fitter.synchronize()

    print( f'Loading B-spline coefficients from\n\t{b_spline_data_file}' )
    spline_params = ParamsClass.from_file( b_spline_data_file )
    ROI_spline = spline_params.n_pixels

    N_intensities = len( intensities )
    N_bg = len( bg_spots )

    # calculating sigma ( sqrt(variance) ) for expected xi2
    real_xi2, sigma_real_xi2 = expected_xi2(
        normalized_spots=normalized_spots, intensities=intensities,
        backgrounds=bg_spots, use_poisson_noise=use_poisson_noise_for_spots,
        folder_name=folder_name
    )

    # array for estimated xi2. for every background level
    # there will be separated graph
    xi2_arr = np.zeros( (N_bg, N_intensities), dtype=float )
    xi2_sigma_arr = np.zeros( (N_bg, N_intensities), dtype=float )

    # arrays with CRLB and sigma
    if build_sigma_over_intensity_graphs:
        CRLB_real_arr = np.zeros( (2, N_bg, N_intensities), dtype=float )
        CRLB_est_arr = np.zeros( (2, N_bg, N_intensities), dtype=float )
        sigma_std_arr = np.zeros( (2, N_bg, N_intensities), dtype=float )

    # calculating xi2 estimated values
    with SMLM( debugMode=_DEBUG_SMLM ) as smlm, Context( smlm ) as ctx:
        spline_obj = SplineClass( ctx )
        psf = spline_obj.CreatePSF_XYZIBg( roisize=ROI_spline,
                                           calib=spline_params,
                                           cuda=False )

        # saving B-spline model spots
        if _DEBUG:
            b_spline_estimated_spots = get_spline_estimated_spots(
                psf=psf, ROI_spline=ROI_spline,
                subpix_coordinates=subpix_coordinates,
                z_pos=z_position_for_test, I=1000, bg=0
            )

            b_spline_estimated_spots = cropping_spots_array(
                b_spline_estimated_spots, ROI_spots
            )

            print( f'Saving estimated B-spline spots into\n'
                   f'\t{estimated_spots_file}...' )

            with tifffile.TiffWriter( estimated_spots_file ) as tif:
                tif.save( b_spline_estimated_spots, compress=2 )

        print( 'Starting xi2 test for different intensities' )
        z_positions = np.ones( N_spots ) * z_position_for_test
        for ibg in range( N_bg ):
            counter = 0
            bg = bg_spots[ibg]
            for I in intensities:
                b_spline_estimated_spots = get_spline_estimated_spots(
                    psf=psf, ROI_spline=ROI_spline,
                    subpix_coordinates=subpix_coordinates,
                    z_pos=z_position_for_test,
                    I=I, bg=bg
                )

                b_spline_estimated_spots = cropping_spots_array(
                    b_spline_estimated_spots,
                    ROI_spots
                )

                # calculated real spots
                spots_real = normalized_spots * I + bg
                if use_poisson_noise_for_spots:
                    spots_real = np.random.poisson( spots_real ).\
                        astype( np.float32 )

                # comparison
                xi2_arr[ibg, counter], xi2_sigma_arr[ibg, counter] = \
                    calc_xi2( b_spline_estimated_spots, spots_real )


                if build_sigma_over_intensity_graphs:
                    # calc CRLB for estimated positions
                    print( '\nCalculating precision graph...' )
                    results, crlb, _ = process_separate_spots_movie(
                        spots=spots_real,
                        bspline_params=spline_params,
                        ctx=ctx
                    )

                    # calc CRLB for real positions
                    crlb_real = CRLB_with_real_pos(
                        psf=psf,
                        shifts_real=subpix_coordinates,
                        z_pos=z_positions,
                        ROI=ROI_spots,
                        intensities=np.ones(N_spots) * I,
                        background=bg
                    )

                    # correct because of the coordinate systems mismatch
                    shifts_est = results.estim[:, 0:2]
                    shifts_est -= ROI_spots / 2.0 - 0.5

                    # filtering
                    if use_filtering_for_number_of_iterations:
                        max_iter_num = np.max( results.iterations )
                        only_good = ( results.iterations < max_iter_num )
                    else:
                        only_good = np.zeros( N_spots, dtype=bool )
                        only_good.fill( True )

                    subpix_coordinates_flt = subpix_coordinates[only_good]
                    shifts_est_flt = shifts_est[only_good]
                    crlb_real_flt = crlb_real[only_good]
                    crlb_est_flt = (crlb[only_good])[:, 0:2]

                    # sigma from STD of difference
                    title = f'Difference between real and estimated shifts, ' \
                            f'I={I:.1f}, bg={bg}'
                    filename = precision_graph_base_name + \
                               f'_I_{I:.1f}_bg_{bg}.png'

                    sg_diff = comparing_shifts(
                        shifts_real=subpix_coordinates_flt,
                        shifts_est=shifts_est_flt,
                        graph_filename=filename,
                        title=title
                    )

                    # average for CRLB and writing STD, CRLBs into array
                    crlb_real_loc = np.mean( crlb_real_flt, axis=0 )
                    crlb_est_loc = np.mean( crlb_est_flt, axis=0 )

                    for iaxis in [0, 1]:
                        CRLB_real_arr[iaxis, ibg, counter] = crlb_real_loc[iaxis]
                        CRLB_est_arr[iaxis, ibg, counter] = crlb_est_loc[iaxis]
                        sigma_std_arr[iaxis, ibg, counter] = sg_diff[iaxis]

                counter += 1
                if counter % 1 == 0:
                    print( f'\txi2 for intensity {counter} from {len(intensities)} '
                           f'is calculated' )

            print( f'Test for {ibg + 1} bg from {N_bg} backgrounds is finished' )

    # graphs
    if build_sigma_over_intensity_graphs:
        plot_CRLB_and_sigma( CRLB_real_arr, CRLB_est_arr, sigma_std_arr,
                             intensities, bg_spots,
                             filename_base=sigma_over_intensity_file_base )

    plot_xi2_over_intensity_graph( intensities, xi2_arr, xi2_sigma_arr,
                                   real_xi2=real_xi2,
                                   sigma_real_xi2=sigma_real_xi2,
                                   backgrounds=bg_spots,
                                   title=f'Xi2, bg={bg_spots}, '
                                         f'N spots = {N_spots}, '
                                         f'N calibr. stacks = {N_calibr}',
                                   filename=xi2_filename )

    save_xi2_over_intensity_into_txt( xi2_filename_txt=xi2_filename_txt,
                                      intensities=intensities,
                                      backgrounds=bg_spots,
                                      xi2_arr=xi2_arr,
                                      xi2_sigma_arr=xi2_sigma_arr,
                                      real_xi2=real_xi2,
                                      sigma_real_xi2=sigma_real_xi2 )

    for ibg in range( N_bg ):
        print( f'Sigma point cloud bg {bg_spots[ibg]}:' )
        print( 'I\tsigma_x\tsigma_y' )

        for iintens in range( N_intensities ):
            print( f'{intensities[iintens]}\t'
                   f'{sigma_std_arr[0, ibg, iintens]}\t'
                   f'{sigma_std_arr[1, ibg, iintens]}' )

        print( '' )

    print( 'DONE!' )
