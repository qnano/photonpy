"""
File for processing separated spots inside .tif file.

Input:
1) .tif file where every slice is one ROI with PSF. PSFs can have subpixel
    shift. This shift will be estimated by the algorithm.

2) .mat calibration file from MATLAB fitting B-spline stage which contains
    all B-net coefficients which are necessary for B-spline fitting.

Output:
    result object.
"""
import sys
import argparse
import tifffile
import numpy as np
import os.path
import time
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib.patches as pltpatch

sys.path.append( '../..' )
sys.path.append( '..' )
from smlmlib.base import SMLM
from smlmlib.context import Context
from smlmlib.bspline import BSplinePSF_Params, BSpline
from smlmlib.psf_queue import PSF_Queue
from smlmlib.psf import PSF
from blinking_sim.blinking_simulated_true_data_reader import BlinkingSimTrueDataReader


# if True, the debug version of SMLM library will be used
_USING_DEBUG_SMLM = False

# if True, CUDA will be used for localization. Usually, if the queue is used,
# you need to use CUDA
_USE_CUDA_FOR_LOCALIZATION = True

# if True, spots which weren't converged for the fixed number of iterations
# will be saved in
_NAME_FOR_NOT_CONVERGED_SPOTS_TIFF = 'fitting_not_converged_spots.tif'
_DEBUG_SAVE_NOT_CONVERGED_SPOTS_TIFF = True

# if True, time of execution for a fitting stage will be printed
_QUEUE_FITTING_TIME_MEASUREMENT = False


def process_separate_spots_movie( spots, bspline_params, ctx:Context,
                                  method:str='bspline' ):
    """Fitting separate spots using PSF model.

    :param spots: (3D array) [spot idx][x pix idx][y pix idx]
        array with spots which will be used for finding the best possible
        position of PSFs.

    :param bspline_params:  structure with B-spline parameters (coefficients)
        that approximated PSF calibration stacks by PSF spline model.

    :param ctx: (Context) context with which SMLM library should work.

    :param method: (str) name of the method which is used for fitting.
        Now only 'bspline' is available

    :return: result  object with
        column names for theta, estimated position inside ROI, Fisher matrix,
        number of fitting iterations that were used.

        crlb (2D array) [value idx][spot idx]
            CRLB for values.

        psf (PSF object) PSF object that was used for estimation.
    """
    if _QUEUE_FITTING_TIME_MEASUREMENT:
        st_time = time.time()

    ROI = spots.shape[1]
    Nspots = spots.shape[0]

    # do fitting in the queue
    if method == 'bspline':
        psf = BSpline( ctx ).CreatePSF_XYZIBg( ROI, bspline_params,
                                           cuda=_USE_CUDA_FOR_LOCALIZATION )
    else:
        raise Exception( 'Unknown method for creating a PSF object' )

    if _QUEUE_FITTING_TIME_MEASUREMENT:
        init_time = time.time() - st_time
        st_time = time.time()

    q = PSF_Queue( psf, batchSize=2048 )
    print( 'Launching queue...' )

    q.Schedule( spots, ids = np.arange( Nspots ) )
    q.WaitUntilDone()
    results = q.GetResults()
    results.SortByID()
    print( '' )

    crlb = results.CRLB()

    if _QUEUE_FITTING_TIME_MEASUREMENT:
        queue_time = time.time() - st_time
        print( f'Fitting spots using splines:\n'
               f'initialization time = {init_time} sec\n'
               f'queue fitting time = {queue_time} sec\n' )

    return results, crlb, psf


def process_separate_spots_movie_file( sep_spots_file:str,
                                       calibration_PSFs_file:str,
                                       ctx:Context,
                                       method:str='bspline' ):
    """Founding the best spot position using calibration stacks from file.

    :param sep_spots_file: (str) filename for .tif file where every slice
        is one PSF z slice with subpixel shift from the center.

    :param calibration_PSFs_file: (str) filename of .mat file with calibration
        B-spline parameters that approximated PSF calibration stacks.

    :param ctx: (Context) context with which position finding algorithm
        should work.

    :param method: (str) name of the method which is used for fitting.
        Now only 'bspline' is available

    :returns
        result structure with a lot of parameters such as estimated positions,
            number of iterations,

        crlb (2D array) for estimated points

        psf (PSF object) which was used for founding the best position

        spots (3D array) [spot idx][x pix idx][y pix idx]
            array with spots which will be used for finding the best possible
            position of PSFs.
    """

    with tifffile.TiffFile( sep_spots_file ) as tif:
        spots = tif.asarray()

    # load calibration stacks into PSF for fitting
    calibr_stacks = BSplinePSF_Params.from_file( calibration_PSFs_file )

    results, crlb, psf = process_separate_spots_movie( spots, calibr_stacks,
                                                       ctx, method )

    return results, crlb, psf, spots


def widering( left_val:float, right_val:float, wider_coef:float ):
    """Func for widering interval for graphs

    in order to see all the points on the graph, you need to place limits
    properly: a little bit less than left value (min) and a little bit more
    than right value (max)
    """

    if wider_coef < 1:
        raise Exception( 'Widering coefficient should be more or eq than 1' )

    if left_val > right_val:
        raise Exception( 'Left value should be less than right value' )

    dif = right_val - left_val
    delta = dif * ( wider_coef - 1.0 )

    return left_val - delta, right_val + delta


def plot_scattered_points( X, Y,
                           title:str, xlabel:str, ylabel:str,
                           centered:bool=True,
                           sigma_xy=None,
                           graph_filename:str=None,
                           Nbins:int=20 ):
    """Creates plot with points in x,y

    if centered is true, the center will be in (0,0) and the limits for x,y
    will be the same.
    """

    # values define the location of histogram graphs and scattered graph
    # in the canvas. This values should be in interval [0, 1]
    left_sc_cv = 0.15
    width_sc_cv = 0.65
    bottom_sc_cv = 0.1
    height_sc_cv = 0.65
    spacing = 0.005

    histX_height = 0.95 - bottom_sc_cv - width_sc_cv
    histY_width = 0.95 - left_sc_cv - width_sc_cv

    # creating boxes in the whole canvas for plots
    scatter_box = [left_sc_cv, bottom_sc_cv, width_sc_cv, height_sc_cv ]
    scatter_canvas = plt.axes( scatter_box )

    histX_box = [left_sc_cv, bottom_sc_cv + height_sc_cv + spacing,
                 width_sc_cv, histX_height]
    histX_canvas = plt.axes( histX_box )

    histY_box = [left_sc_cv + width_sc_cv + spacing, bottom_sc_cv,
                 histY_width, height_sc_cv]
    histY_canvas = plt.axes( histY_box )

    # ticks for histogram reversed
    histX_canvas.set_title( title )
    histX_canvas.tick_params( direction='in', labelbottom=False )
    histY_canvas.tick_params( direction='in', labelleft=False )

    # scattered plot
    scatter_canvas.scatter( X, Y )
    scatter_canvas.set_xlabel( xlabel )
    scatter_canvas.set_ylabel( ylabel )

    _WIDERING_COEF = 1.1
    # if graph is the only one point
    leftX, rightX, leftY, rightY = None, None, None, None

    if centered:
        maxX_abs = np.max( np.abs( X ) )
        maxY_abs = np.max( np.abs( Y ) )
        # wider a little bit a graph in order to see all the points
        max_abs = max( maxX_abs, maxY_abs )

        if max_abs > 0:
            left, right = widering( -max_abs, max_abs, _WIDERING_COEF )

            leftX, leftY = left, left
            rightX, rightY = right, right
    else:
        maxX = np.max( X )
        minX = np.min( X )

        maxY = np.max( Y )
        minY = np.min( Y )

        if maxX != minX:
            # wider a little bit a graph in order to see all the points
            leftX, rightX = widering( minX, maxX, _WIDERING_COEF )
            leftY, rightY = widering( minY, maxY, _WIDERING_COEF )

    # if graph is not just a one point, plot histograms
    if leftX is not None and rightX is not None and \
            leftY is not None and rightY is not None:
        scatter_canvas.set_xlim( (leftX, rightX) )
        scatter_canvas.set_ylim( (leftY, rightY) )

        # adjusting bins for histograms
        binwidth_x = ( rightX - leftX ) / Nbins
        binwidth_y = ( rightY - leftY ) / Nbins

        bins_x = np.arange( leftX, rightX + binwidth_x, binwidth_x )
        bins_y = np.arange( leftY, rightY + binwidth_y, binwidth_y )

        # plotting histogram graphs for X and Y
        histX_canvas.hist( X, bins=bins_x )
        histY_canvas.hist( Y, bins=bins_y, orientation='horizontal' )

        histX_canvas.set_xlim( scatter_canvas.get_xlim() )
        histY_canvas.set_ylim( scatter_canvas.get_ylim() )

    scatter_canvas.grid()

    # plot sigma ellipse
    if sigma_xy is not None:
        sigma_xy_3 = [ sigma_xy[0] * 3.0, sigma_xy[1] * 3.0 ]
        ellipse = pltpatch.Ellipse( (0,0),
                                    width=sigma_xy_3[0] * 2.0,
                                    height=sigma_xy_3[1] * 2.0,
                                    edgecolor='red', facecolor='none' )

        scatter_canvas.add_patch( ellipse )

    if graph_filename is not None and len( graph_filename ) > 0:
        plt.savefig( graph_filename )

    plt.show()


def comparing_shifts( shifts_real, shifts_est, graph_filename:str=None,
                      title:str=None, verbose:bool=False ):
    """Comparing real and estimated by the fitting algorithm shifts, plot

    For graph average value of difference should be 0, however, because
    of the finite amount of calibration PSF stacks, average PSF stack will
    not be in the center (in 0,0,0) => position estimation for spots also have
    some fixed shift. That is why I use substraction of mean difference
    value for the scattered graph.

    shifts_real: (2D array) [spot idx][axis idx]
        real subpixel positions of spots in X and Y (usualy, which were
        used during the simulation) in pixels.
    shifts_est: (2D array) [spot idx][axis idx]
        estimated by some algorithm positions of spots in X and Y (for example,
        by fitting B-spline model stage).
    graph_filename: (str) if not None, graph with this name will be saved.
    title: (str) if not None, this title will be used for the graph.
        However, if it is None, default title
        'Difference between real and estimated shifts' will be used.

    verbose: (bool) if true, additional debug info will be printed

    :return (2 floats as array) sigma (STD) of difference values point cloud.
        STD(shifts_real - shifts_est)
    """

    dif = np.zeros( shape=shifts_real.shape, dtype=shifts_real.dtype )
    dif[:, 0] = shifts_real[:, 0] - shifts_est[:, 0]
    dif[:, 1] = shifts_real[:, 1] - shifts_est[:, 1]

    Nspots = float( len( shifts_real ) )

    # calculate sigma precision
    sigma_prec = np.sqrt( (dif**2).sum( axis=0 ) / Nspots )
    if verbose:
        print( f'Sigma precision = {sigma_prec}' )

    # average value of difference. If shifts are uniformly distributed and
    # shifts estimation is working correctly, this value should be close to 0
    aver_difference = np.average( dif, axis=0 )
    if verbose:
        print( f'Average shift = {aver_difference}' )

    dif_centered = dif - aver_difference
    # sigma for difference cloud point
    sigma_point_cloud = dif.std( axis=0 )
    if verbose:
        print( f'Sigma point cloud = {sigma_point_cloud}' )

    # PLOT
    if title is None:
        title = 'Difference between real and estimated shifts'
    plot_scattered_points( dif_centered[:,0], dif_centered[:,1],
                           title=title,
                           xlabel='dif shift X, pixels',
                           ylabel='dif shift Y, pixels',
                           graph_filename=graph_filename,
                           sigma_xy=sigma_point_cloud )

    return sigma_point_cloud


def CRLB_with_real_pos( psf:PSF, shifts_real, z_pos, ROI:int,
                        intensities, background:float ):
    """Calculates CRLB with real values (positions)

    :param psf: PSF object that can calculate a lot of stuff, including CRLB
    :param shifts_real: (2D float array) [bead idx][axis idx: 0->x, 1->y]
        subpixel shift for spot in the coordinate system like
              |
              |      x
        ------|------>
              |
              v y
        So shift (0,0) means that spot is located right in the middle of ROI
    :param z_pos: (1D float array) [bead idx] z positions of the spots
    :param ROI: (int) ROI size for spots in pixels.
    :param intensities: (1D float array) [bead idx] intensity of every spot
    :param background: (float) background for every spot.

    :return: (2D float numpy array) [bead idx][axis idx: 0->x, 1->y]
        CRLB for real positions for X and Y.
    """

    # this is important, because subpixel position is calculated from the
    # corner pixel, but in the file shift positions are calculated from
    # the center of ROI
    shift_for_subpix_pos = ROI / 2.0 - 0.5

    Nspots = len( shifts_real )
    theta = np.zeros( (Nspots, 5), dtype=shifts_real.dtype )

    theta[:, 0:2] = shifts_real + shift_for_subpix_pos
    theta[:, 2] = z_pos
    theta[:, 3] = intensities
    theta[:, 4] = background

    crlb = psf.CRLB( theta=theta )
    crlb_XY = crlb[:,0:2]

    return crlb_XY


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Process separated spots .tif "
                    "file in order to calculate "
                    "shifts" )

    parser.add_argument( "-i", "--input", required=True,
                         help="Input TIFF file (.tif)" )

    parser.add_argument( "--calibration", required=True,
                         help="Path to file with calibration PSF stacks (.mat)" )

    parser.add_argument( "--realdata", required=True,
                         help="Path to file with real simulated data (.txt)" )

    # only B-spline method can be used now
    parser.add_argument( "--method", default="bspline",
                         help="Proccessing method" )

    args = parser.parse_args()

    # read real data
    real_data_reader = BlinkingSimTrueDataReader.from_file( args.realdata )
    z_pos = real_data_reader.real_positions[:,2]

    Nspots = len( z_pos )

    shifts_real = np.zeros( (Nspots, 2), dtype=float )
    shifts_real[:, 0] = real_data_reader.subpixel_shifts[:, 1]
    shifts_real[:, 1] = real_data_reader.subpixel_shifts[:, 0]

    with SMLM( debugMode=_USING_DEBUG_SMLM ) as smlm, Context( smlm ) as ctx:
        results, crlb, psf, spots = process_separate_spots_movie_file(
            sep_spots_file=args.input,
            ctx=ctx,
            calibration_PSFs_file=args.calibration,
            method=args.method
        )
        print( '' )

        folder_name = os.path.dirname( args.input )
        # checking number of not converged spots and saving if it is needed
        max_number_of_iterations = results.iterations.max()
        not_converged = (results.iterations == max_number_of_iterations)
        print( f'Number of not converged spots during the fitting stage = '
               f'{not_converged.sum()} from {Nspots}' )

        if _DEBUG_SAVE_NOT_CONVERGED_SPOTS_TIFF:
            file = os.path.join( folder_name, _NAME_FOR_NOT_CONVERGED_SPOTS_TIFF )
            not_conv_spots = spots[not_converged]

            print( 'Saving not converged during the fitting stage spots in file' )
            print( f'\t{file}' )
            with tifffile.TiffWriter( file ) as tif:
                tif.save( not_conv_spots, compress=2 )

        # shifts in results are written according to the corner
        # and not to the center of the ROI
        ROI = results.sampleshape[0]

        shifts_est = results.estim[:, 0:2]
        # -0.5 is here, because for fitting stage if point
        # in the position inside ROI (0,0), it means that the point is located in
        # the center of the left upper pixel and not in the corner of this pixel
        shifts_est -= ROI / 2.0 - 0.5

        # estimated values CRLB plot
        crlbXYZ = crlb[:, :3]

        # calculate CRLB with real positions
        crlb_real_XY = CRLB_with_real_pos( psf, shifts_real, z_pos, ROI,
                                           real_data_reader.intensities,
                                           real_data_reader.background )

    # check that z positions are the same
    z_aver = np.average( z_pos )
    z_sum = np.sum( np.abs( z_pos - z_aver ) )
    if z_sum != 0.0:
        raise Exception( 'your z positions should be the same for all points.'
                         ' Otherwise CRLB and precision estimation will be '
                         'not accurate' )
    z_pos = z_pos[0]

    # comparing estimated and real shifts
    comparing_shifts( shifts_real=shifts_real, shifts_est=shifts_est )

    plot_scattered_points( crlbXYZ[:,0], crlbXYZ[:,1], title='CRLB estimated',
                           xlabel='CRLB x', ylabel='CRLB y',
                           centered=False )
    av_CRLB_X = np.average( crlbXYZ[:,0] )
    av_CRLB_Y = np.average( crlbXYZ[:,1] )
    print( f'Average estimated CRLB X = {av_CRLB_X}' )
    print( f'Average estimated CRLB Y = {av_CRLB_Y}' )

    plot_scattered_points( crlb_real_XY[:,0], crlb_real_XY[:,1],
                           title='CRLB real',
                           xlabel='CRLB x', ylabel='CRLB y',
                           centered=False )
    av_CRLB_X = np.average( crlb_real_XY[:,0] )
    av_CRLB_Y = np.average( crlb_real_XY[:,1] )
    print( f'Average real CRLB X = {av_CRLB_X}' )
    print( f'Average real CRLB Y = {av_CRLB_Y}' )
    print( 'All units are in pixels (not in um)' )

    print( 'DONE!' )
