rem working with real dataset B-splines
rem py "calibration/create_calibration.py" --offset 100 --roi 13 --emgain 1 --zrange 350 --step 10 --method bspline -o ../../../OUTPUT/stacks.mat -i "C:\Work\Documents\TUDelft\SMLM challenge datasets\calibration stack\sequence-as-stack-Beads-AS-Exp.tif"

rem working with real dataset C-splines
rem py "calibration/create_calibration.py" --offset 0 --roi 13 --emgain 1 --zrange 600 --method cspline -o ../../../OUTPUT/stacks.npz -i "../../../OUTPUT/simulation_calibration_PSFs.mat"

rem working with a simulated by blinking_simulation.py movie
py "calibration/create_calibration.py" --offset 0 --roi 13 --emgain 1 --zrange 600 --method bspline -o ../../../OUTPUT/stacks.mat -i "../../../OUTPUT/simulation_calibration_PSFs.mat"