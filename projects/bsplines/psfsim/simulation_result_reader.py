import math                         # some small math functions
import numpy as np                  # working with arrays
import scipy.io                     # load .mat file


class SimulationDataReader:
    """Structure which can read data from calibration PSFs simulation.

    Used for read .mat file from Vectorial model PSFs simulation.
    Also it transforms PSFs matrix from MATLAB format to usual format.

    Attributes:
        PSFs (numpy 4D arr): [bead index][z index][x index][y index]
            array with simulated calibration PSFs (beads).

        true_shifts (numpy 2D arr): [bead index][axis index (0-x,1-y,2-z)]
            shifts that were used in MATLAB script for bead generation.
            Shift of 0-index bead is always 0.

        n_zslices (int): number of slices through z for every bead.

        z_range (float): z position inside a bead is changing in interval
            [-z_range, +z_range].

        roi (int): size of a bead in pixels through X or Y axis.
            Region of interest.

        Nbeads (int): number of simulated beads.
    """

    def __init__( self, matfilename ):
        mat = scipy.io.loadmat( matfilename )

        self.PSFs = mat['save_PSF']
        self.true_shifts = mat['true_shifts']
        self.n_zslices = mat['n_zslices'][0][0]
        self.z_range = mat['z_range'][0][0]

        self.roi = self.PSFs.shape[0]
        self.Nbeads = self.PSFs.shape[3]

        #self.PSFs = np.moveaxis( self.PSFs, [3,2,0,1], [0,1,2,3] )
        self.PSFs = np.transpose( self.PSFs, [3,2,0,1] )

    def toMovie( self, background:float=0, use_poisson_noise:bool=False, space=-1 ):
        """Translates beads stack into a 'movie' file.

        'movie' array has a ( n_zslices, N, N ) shape.
        Where
        N = Nbeads_per_size * ( ROI + space ) + space.
        Nbeads_per_size = upper bound of sqrt(Nbeads). This is max number of
            beads for one side (Ox or Oy).

        Basically, all the beads will be placed near each other in the array
        like (this is example of one z slice):

          space    ROI     space    ROI
        <------><-------><------><-------> ...
        ^
        |  space
        |
        v
        ^       ROI_DATA         ROI_DATA
        | ROI   ROI_DATA         ROI_DATA
        v       ROI_DATA         ROI_DATA  ...
        ^
        |  space
        |
        v
        ^       ROI_DATA         ROI_DATA
        | ROI   ROI_DATA         ROI_DATA
        v       ROI_DATA         ROI_DATA  ...
                ...              ...

        Args:
            space (int): number of pixels between beads in the movie file.
                Any value less than 0 is considered as not set => default
                value will be used. Default is ROI / 2

            background (float): background level around PSFs

            use_poisson_noise (bool): if true, Poisson noise will be used for
                background generation around PSFs
        """

        # space less than 0 is considered as not set => default value is used
        if space == -1:
            space = int( self.roi / 2 )

        # max number of beads per one side (Ox or Oy) in the out movie file
        Nbeads_per_side = math.ceil( np.sqrt( self.Nbeads ) )

        # total number of pixels for one side in the out movie file
        Npix_all_side = Nbeads_per_side * ( self.roi + space ) + space

        # reserve memory for the movie
        movie = np.zeros( (self.n_zslices, Npix_all_side, Npix_all_side),
                          dtype=self.PSFs.dtype )

        # fill with background
        movie.fill( background )

         # add Poisson noise if needed
        if use_poisson_noise:
            movie = np.random.poisson( movie )

        for ibead in range(self.Nbeads):
            # bead index in number of beads
            idx_x_bead = ibead % Nbeads_per_side
            idx_y_bead = int( ibead / Nbeads_per_side )

            # the bead corner in number of pixels
            idx_x_pix = space + idx_x_bead * ( self.roi + space )
            idx_y_pix = space + idx_y_bead * ( self.roi + space )

            # slices in order to place the bead into the movie array
            bead_slice_x = slice( idx_x_pix, idx_x_pix + self.roi, 1 )
            bead_slice_y = slice( idx_y_pix, idx_y_pix + self.roi, 1 )

            # placing the bead into the movie array
            movie[:, bead_slice_x, bead_slice_y]\
                = self.PSFs[ibead]

        return movie
