% Function for debugging simulate_PSF_Vectoral_model.m
% because if you launch MATLAB script from python, you can't
% put a breakpoint in the MATALB script in order to debug it.

clear all;
clc;

simulate_PSF_Vectoral_model( ...
    '../../../../OUTPUT/simulated_Vectoral_PSF_debug_from_MATLAB', ... % name of output .mat file
    ...
    14, ... % ROI size. 14
    151, ... % number of slices in the PSF through Z direction
    300.0, ... % PSF is changing in interval [-z_range, +z_range]. In nm.
    6, ... % number of PSFs that will be generated
    ...
    1.49, ... % numerical aperture
    690.0, ... % wavelngth (?)
    100.0, ... % pixelsize in nanometers
    [2.0 2.0 50.0], ... % aberrations used for generating PSFs
    ...                 % for [X, Y, Z] directions
    true, ... % if true, Poisson noise will be used for generating PSFs
    [0.1 0.1 0.1], ... % the shift offset will be generated as a float
    ...                % number in interval
    ...                % [-OFFSET_RAND_COEF/2, +OFFSET_RAND_COEF/2]
    ...                % in pixels
    [] ... % if this array is not empty, instead of randomly
    ...    % generated shifts, it will be used
);
