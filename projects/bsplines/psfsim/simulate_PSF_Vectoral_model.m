% Function for generating aberration PSFs using Vectorial model.
% It generates sertain amount of calibration PSFs in 3D
% with random shifts and with Poisson noise.


function simulate_PSF_Vectoral_model( ...
    outfile, ... % name of .mat file name with simulated PSFs
    ...
    roiSize, ... % region of interest of one PSF
    ...          % Number of pixels for X or Y direction in PSF
    n_z_slices, ... % number of slices in the PSF through Z direction
    z_range, ... % PSF is changing in interval [-z_range, +z_range]. In nm.
    Nbeads, ...  % number of PSFs that will be generated
    ...
    NA, ...      % numerical aperture
    lambda, ...  % wavelngth in nm
    pixelsize, ... % pixelsize in nanometers
    aberrations, ... % aberrations (Zernike orders [n1,m1,A1,n2,m2,A2,...] 
    ...              % with n1,n2,... the radial orders, 
    ...              % m1,m2,... the azimuthal orders, 
    ...              % and A1,A2,... the Zernike coefficients in lambda rms, 
    ...              % so 0.072 means diffraction limit)
    ...
    USE_POISON_NOISE, ... % if true, Poisson noise will be used
    ...                   % for generating PSFs
    OFFSET_RAND_COEF, ... % the shift offset will be generated as a float
    ...                   % number in interval
    ...                   % [-OFFSET_RAND_COEF/2, +OFFSET_RAND_COEF/2]
    ...                   % in pixels
    defined_shifts ... % if this array is not empty, instead of randomly
    ...                % generated shifts, it will be used
)
    fprintf( 'Launching simulate_PSF_Vectoral_model()...\n' )
    addpath('../../../../3rd_party/VectorFitter');

    %% Setting parameters for Vectoral PSF model
    fprintf( 'Simulate PSF using Vectoral model\n\n' );
   
    %% Setting parameters to the structure for generating PSF
    parameters = set_parameters_willem;

    parameters.NA = NA;

    parameters.pixelsize = pixelsize;
    parameters.samplingdistance = parameters.pixelsize;
    parameters.beaddiameter = 0;
    parameters.lambda = lambda;
    parameters.zrange = [-z_range, z_range];
    parameters.aberrations = aberrations;

    parameters.Mx = double( roiSize );
    parameters.My = double( roiSize );
    parameters.Mz = double( n_z_slices );

    parameters.xrange = parameters.samplingdistance * parameters.Mx / 2;
    parameters.yrange = parameters.samplingdistance * parameters.My / 2;

    %% Print parameters to the output
    fprintf( 'outfile = %s\n\n', outfile );

    fprintf( 'stack parameters:\n' );
    fprintf( 'N beads = %d\n', Nbeads );
    fprintf( 'ROI size = %d\n', roiSize );
    fprintf( 'Number of z slices = %d\n', n_z_slices );
    fprintf( 'Z range = -%g..%g\n\n', z_range, z_range );

    fprintf( 'Aberration and additional distortions parameters:\n' );
    fprintf( 'NA = %g\n', NA );
    fprintf( 'lambda = %g\n', lambda );
    fprintf( 'pixelsize = %g\n', pixelsize );
    fprintf( 'aberrations = [%g, %g, %g]\n\n', ...
              aberrations(1), aberrations(2), aberrations(3) );

    fprintf( 'Constants:\n' );
    
    if isempty( defined_shifts )
        fprintf( 'random coef x = %g\n', OFFSET_RAND_COEF(1) );
        fprintf( 'random coef y = %g\n', OFFSET_RAND_COEF(2) );
        fprintf( 'random coef z = %g\n', OFFSET_RAND_COEF(3) );
    else
        for i=1:Nbeads
            ind = i*3-2;
            fprintf( 'shift_bead_%d = %g %g %g\n', i-1, ...
                                                   defined_shifts( ind ), ...
                                                   defined_shifts( ind+1 ), ...
                                                   defined_shifts( ind+2 ) );
        end
    end
    
    if USE_POISON_NOISE == true
        fprintf( 'USE POISON NOISE = true\n' );
    else
        fprintf( 'USE POISON NOISE = false\n' );
    end

     
    %% Generating PSF
    % generating random shifts of calibration beads
    fprintf( '-------------------------------------------------------\n' );
    offsets = generating_offsets( Nbeads, defined_shifts, OFFSET_RAND_COEF );

    % Array for saving PSF beads
    save_PSF = single( zeros( roiSize, roiSize, n_z_slices, Nbeads ) );

    step_z = 2.0 * single(z_range) / single( n_z_slices - 1 );
    
    fprintf( 'Generating beads...\n' );
    for i=1:Nbeads
        tic;
        x_shift = offsets(i, 1);
        y_shift = offsets(i, 2);
        z_shift = offsets(i, 3);

        % why do shifts here are mixed by Willem?
        parameters.xemit = y_shift * parameters.pixelsize;
        parameters.yemit = x_shift * parameters.pixelsize;
        parameters.zemit = z_shift * step_z;

        % calculate pupil matrix
        [XPupil, YPupil, wavevector, wavevectorzimm, ...
         Waberration, PupilMatrix] ...
             = get_pupil_matrix( parameters );

        % calculate field matrix for focal stack
        [XImage, YImage, ZImage, FieldMatrix] = get_field_matrix( ...
            PupilMatrix, ...
            wavevector, ...
            wavevectorzimm, ...
            parameters ...
        );

        save_PSF(:, :, :, i) = get_psf( FieldMatrix, parameters );

        fprintf( '  %d from %d beads has been generated. ', i, Nbeads );
        toc;
        fprintf( '\n' );
    end

    %% Normalize PSFs
    fprintf( 'Normalizing...\n' );
    % sum for every z slice for every bead
    sum_per_z_slice = squeeze( sum( save_PSF, [1,2] ) );
    
    % maximum through z slices for every bead
    max_z_slice_sum = squeeze( max( sum_per_z_slice, [], 1 ) );
    
    % divide values for every bead by the maximum of sum(x,y) through z
    % slices
    for i=1:Nbeads
        save_PSF(:,:,:,i) = save_PSF(:,:,:,i) / max_z_slice_sum(i);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % this part of code is for checking the normalization
%     for i=1:Nbeads
%         save_PSF_0 = save_PSF(:,:,:,i);
%         sum_for_one_bead = squeeze( sum( save_PSF_0, [1,2] ) );
%         size_sss = numel( sum_for_one_bead );
%         xx = linspace(1, size_sss, size_sss );
%         plot( xx, sum_for_one_bead );
%     end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Save result to the .mat file
    fprintf( 'Saving results to\n%s.mat\nfile... ', outfile );
    n_zslices = n_z_slices;
    n_stacks = Nbeads;
    true_shifts = -offsets;
    tic;
    save( outfile, ...
         'save_PSF', ...
         'true_shifts', ...
         'n_zslices', ...
         'n_stacks', ...
         'z_range');
    toc;

    fprintf( '\nMATLAB SCRIPT is DONE!\n' );

end


% Generates offsets for Nbeads simulated PSFs
%
% Nbeads (int): number of beads for generating calibration PSF stacks.
%
% defined_shifts (1D array): if this array is not empty, it will be
%     read by this function. It isn't possible to transfer 2D,3D etc.
%     arrays from python to MATLAB. That's why this array contains
%     all shifts in 1D form:
%
%     [shift_x bead 1, shift_y bead 1, shift_z bead 1,
%      shift_x bead 2, shift_y bead 2, shift_z bead 2, ...]
%
% OFFSET_RAND_COEF (1D array): used in the case than defined_shifts array
%     is empty. Then offsets will be generated randomly from the interval
%     ( -OFFSET_RAND_COEF/2, +OFFSET_RAND_COEF/2 )
%     This array should contain 3 elements for x, y, z offset intervals.
%
% Returns:
%     offsets (2D array): (bead idx, axis idx)
%         offset for every PSF bead that can be used in Vectorial
%         modelling.
%
function [offsets] = generating_offsets( Nbeads, ...
                                         defined_shifts, ...
                                         OFFSET_RAND_COEF )
    offsets = zeros( Nbeads, 3 );
    
    if ~isempty( defined_shifts )
        fprintf( 'Assigning shifts to MATLAB shifts...\n' );
        
        % you can't use 2D arrays as an argument from python to MATLAB,
        % so you need to shape your array into 1D array.
        % this thing is doing reshaping back
        for i=1:Nbeads
            offsets(i, 1) = defined_shifts( i*3 - 2 );
            offsets(i, 2) = defined_shifts( i*3 - 1 );
            offsets(i, 3) = defined_shifts( i*3 );
        end
        
        % check size of the arrays
        if size( offsets, 1 ) ~= Nbeads
            ME = MException( 'MATLAB:exception', ...
                     'offsets array size should be equal to number of beads.\nOffset array size = %d,\nnumber of beads = %d', ...
                     size( offsets, 1 ), ...
                     Nbeads );
                 
            throw( ME );
        end
        
        % shift of the 1st bead should be ALWAYS [0 0 0]
        if offsets(1,1) ~= 0.0 || offsets(1,2) ~= 0.0 || offsets(1,3) ~= 0.0
            ME = MException( 'MATLAB:exception', ...
            "Shifts of the first bead always should be equal to [0 0 0]" );
        
            throw(ME);
        end
        
        % because we actually producing offsets of an emitter
        offsets = -offsets;
    else
        fprintf( 'Generating random offsets...\n' );
        rng shuffle
        offsets = (rand(Nbeads, 3) - 0.5) .* OFFSET_RAND_COEF;
        % 0 bead always will be generated without offset
        offsets(1, :) = [0, 0, 0];
    end
end
