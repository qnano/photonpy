"""
Calculate zernike basis functions

zernike,zernike_rad and noll_to_zern come from libtim:
https://github.com/tvwerkhoven/libtim-py/blob/master/libtim/zern.py
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import factorial as fac


def zernike_rad(m, n, rho):
    """
    Make radial Zernike polynomial on coordinate grid **rho**.
    @param [in] m Radial Zernike index
    @param [in] n Azimuthal Zernike index
    @param [in] rho Radial coordinate grid
    @return Radial polynomial with identical shape as **rho**
    """
    if np.mod(n - m, 2) == 1:
        return rho * 0.0

    wf = rho * 0.0
    for k in range(int((n - m) / 2 + 1)):
        wf += (
            rho ** (n - 2.0 * k) * (-1.0) ** k * fac(n - k) / (fac(k) * fac((n + m) / 2.0 - k) * fac((n - m) / 2.0 - k))
        )

    return wf


def zernike(m, n, rho, phi, norm=True):
    """
    Calculate Zernike mode (m,n) on grid **rho** and **phi**.
    **rho** and **phi** should be radial and azimuthal coordinate grids of identical shape, respectively.
    @param [in] m Radial Zernike index
    @param [in] n Azimuthal Zernike index
    @param [in] rho Radial coordinate grid
    @param [in] phi Azimuthal coordinate grid
    @param [in] norm Normalize modes to unit variance
    @return Zernike mode (m,n) with identical shape as rho, phi
    @see <http://research.opt.indiana.edu/Library/VSIA/VSIA-2000_taskforce/TOPS4_2.html> and <http://research.opt.indiana.edu/Library/HVO/Handbook.html>.
    """
    nc = 1.0
    if norm:
        nc = (2 * (n + 1) / (1 + (m == 0))) ** 0.5
    nc *= rho<=1
    if m > 0:
        return nc * zernike_rad(m, n, rho) * np.cos(m * phi)
    if m < 0:
        return nc * zernike_rad(-m, n, rho) * np.sin(-m * phi)
    return nc * zernike_rad(0, n, rho)


def noll_to_zern(j):
    """
    Convert linear Noll index to tuple of Zernike indices.
    j is the linear Noll coordinate, n is the radial Zernike index and m is the azimuthal Zernike index.
    @param [in] j Zernike mode Noll index
    @return (n, m) tuple of Zernike indices
    @see <https://oeis.org/A176988>.
    """
    if j == 0:
        raise ValueError("Noll indices start at 1, 0 is invalid.")

    n = 0
    j1 = j - 1
    while j1 > n:
        n += 1
        j1 -= n

    m = (-1) ** j * ((n % 2) + 2 * int((j1 + ((n + 1) % 2)) / 2.0))
    return (n, m)


def zernike_to_noll( n:int, m:int ):
    """Convert Zernike polynom indices to Noll indices

    @param [in] n Azimuthal Zernike index
    @param [in] m Radial Zernike index
    @return int with Noll index for this Zernike pair
    @see <https://en.wikipedia.org/wiki/Zernike_polynomials>
    """

    add_num = 0
    if m >= 0 and n % 4 >= 2 or \
       m <= 0 and n % 4 <= 1:
        add_num = 1

    return n*(n+1) / 2 + abs( m ) + add_num


def zernike_map(resolution, maxnoll, zernikeUnitCircle=None):
    if zernikeUnitCircle is None:
        zernikeUnitCircle = resolution/2
    cr = np.arange( -( resolution - 1 ) // 2, ( resolution + 1 ) // 2) \
         / zernikeUnitCircle
    X,Y = np.meshgrid(cr,cr)
    rho = np.sqrt(X**2+Y**2)
    phi = np.arctan2(Y,X)
        
    zmap = np.zeros((maxnoll-1, resolution, resolution), dtype=np.float32)
    for i in range(maxnoll-1):
        n, m = noll_to_zern(i+2)
        zmap[i] = zernike(m,n,rho,phi)

    return zmap


def imshow_imgs(imgs, title=None, cols=10, maxrows=10,colorbar=False):
    n = imgs.shape[0]
    imgh = imgs.shape[1]
    imgw = imgs.shape[2]

    nrows = (n+cols-1)//cols
    
    if nrows > maxrows:
        nrows = maxrows
        n=maxrows*cols
        
    img = np.zeros((imgh*nrows,imgw*cols))
    step_in_z_idx = max( imgs.shape[0] // n, 1 )
    for k in range(n):
        y = k//cols
        x = k%cols
        img[imgh*y:imgh*(y+1),imgw*x:imgw*(x+1)]=imgs[k * step_in_z_idx]

    # plot img as 2D picture
    fig=plt.figure()
    plt.imshow(img)
    if title:
        plt.title(title)
    plt.show()
    return fig


if __name__ == "__main__":
    resolution = 700 # 64
    maxnoll = 37 # 37
    zenrike_unit_circle = 325 # 30
    zmap = zernike_map(resolution, maxnoll, zenrike_unit_circle)

    imshow_imgs(zmap, "Zernike basis functions", 6,  6)
