"""
Generate PSFs using fourier optics, using the model from ZOLA3D

Jelmer Cnossen 
28-10-2019
"""
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    import zernike
else:
    import bsplines.psfsim.zernike as zernike

# Flag for debug purpose
_DEBUG = False


class PSF:
    """Generates PSF according to Zola 3D model (scalar model).

    You can set shifts for PSF for generating it.
    Coordinate system for PSF is (important for setting shifts):
    |-------|--------|
    |       |        |
    |       |        |
    |-------|------> X
    |       |        |
    |       |        |
    |-------v Y -----|
    """

    # default value for FFT resolution
    _FFT_RESOLUTION_DEFAULT = 255

    def __init__( self, psfsize:int,
                  lam:float,
                  pixelsize:float,
                  NA:float,
                  maxnoll:int,
                  focal=0,
                  ref_imm_fluid:float=1.5,
                  ref_sample:float=1.33,
                  evanescent:bool=False,
                  fftres:int=-1 ):

        if fftres < 0:
            fftres = PSF._FFT_RESOLUTION_DEFAULT

        # this shift should be used for even ROI sizes in order to get PSF
        # right in the middle of the ROIxROI region
        self.additional_shift_for_even_ROI = [0.0, 0.0]
        if psfsize % 2 == 0:
            d = pixelsize * 0.5
            self.additional_shift_for_even_ROI = [-d, -d]

        self.maxFreq = NA/lam
        self.maxPixelFreq = self.maxFreq * pixelsize * fftres
        self.NA = NA
        self.lam = lam
        self.pixelsize = pixelsize
        self.fftres = fftres
        self.ref_imm_fluid = ref_imm_fluid
        self.ref_sample = ref_sample
        self.focal = focal
        
        self.zmap = zernike.zernike_map( fftres, maxnoll, self.maxPixelFreq )

        cr = np.arange( -(self.fftres-1)//2, (self.fftres+1)//2 )
        X,Y = np.meshgrid( cr,cr )
        S = 1 / ( self.fftres * self.pixelsize )
        self.kx = X * S
        self.ky = Y * S
        R = np.sqrt( X**2 + Y**2 )
        self.amp = R <= self.maxPixelFreq
        self.krsq = np.sqrt( self.kx**2 + self.ky**2+0j )
        self.kz1 = np.sqrt( (ref_imm_fluid / lam) ** 2 - self.krsq + 0j )
        self.kz2 = np.sqrt( (ref_sample / lam) ** 2 - self.krsq + 0j )
        if not evanescent:
            self.krsq = np.real( self.krsq )
            self.kz1 = np.real( self.kz1 )
            self.kz2 = np.real( self.kz2 )
            
        self.psfsize=psfsize

#	float S = 1.0f / (fftsize * pixelsize);
#	float maxFreq = params.MaxFreq()*pixelsize*fftsize;

    def get_normalization_constant( self, zern_coeff, z_range:float,
                                    n_z_slices:int ):
        """Return normalization constant"""

        psf0 = self.psf3D( offsetXYZ=[0.0, 0.0, 0.0], zern_coeff=zern_coeff,
                           z_range=z_range, n_z_slices=n_z_slices,
                           normalize=False )

        return self.normalize_PSF( psf0 )

    def normalize_PSF( self, PSF_3d ):
        """Normalizes PSF 3D

        Now the maximum sum value through z slices is used for division.

        \:return normalization constant
        """

        sum_intensity = np.sum( PSF_3d, axis=(1,2) )
        max_z_slice_sum = np.max( sum_intensity )

        if _DEBUG:
            slice_with_max_intens = np.argmax( sum_intensity )
            print( 'DEBUG from normalize_PSF():' )
            print( f'\tslice with max intensity = {slice_with_max_intens}' )
            print( f'\ttotal number of slices = {len( sum_intensity )}' )

        PSF_3d /= max_z_slice_sum

        return max_z_slice_sum

    # zern_coeff:zernike coefficients starting at noll=2
    def pupil( self, emXYZ, zern_coeff ):
        zern_phase = np.sum( zern_coeff[:,None,None] * self.zmap, 0 )

        phase = zern_phase + \
                2 * np.pi * ( emXYZ[0]*self.kx + emXYZ[1]*self.ky +
                              emXYZ[2]*self.kz2 - self.focal*self.kz1 )
        pupil = self.amp * np.exp( 1j*phase )
        return pupil
    
    def psf( self, emXYZ, zern_coeff ):
        """Return 2D PSF z slice shifted to emXYZ from center.

        Coordinate system for PSF is (important for setting shifts):
        |-------|--------|
        |       |        |
        |       |        |
        |-------|------> X
        |       |        |
        |       |        |
        |-------v Y -----|

        \:param emXYZ: (3 elements) value of X,Y shifts and Z position of
            the emitter.
        \:param zern_coeff: (1D array) amplitudes for Zernike polynomials.
            It adds aberrations.
        """
        emXYZ[0] += self.additional_shift_for_even_ROI[0]
        emXYZ[1] += self.additional_shift_for_even_ROI[1]

        p = self.pupil( emXYZ , zern_coeff )
        img = np.fft.fftshift( np.abs( np.fft.fft2( np.fft.fftshift( p ) ) )**2 )
        img = img[self.fftres//2 - self.psfsize//2 :
                      self.fftres//2 + (self.psfsize+1)//2,
                  self.fftres//2 - self.psfsize//2 :
                      self.fftres//2 + (self.psfsize+1)//2]
        return img, p

    def psf3D( self, offsetXYZ, zern_coeff, z_range:float, n_z_slices:int,
               normalize:bool=True ):
        """Generates a 3D PSF stack

        \:param[in] offsetXYZ (1D array with floats) - offset in um
            for XY and in number of slices for Z.
            Can be positive or negative floats, but not bigger than 1.

        \:param[in] zern_coeff (1D array with floats) - Zernike coefficients
            amplitudes. Noll indexing is used. However, 0 here matches 2 in
            Noll indexing. 1 matches 3, etc. (so, +2)

        \:param[in] z_range (float) - z range in one direction in micrometers.
            The PSF stack changes in interval [-z_range, +z_range].

        \:param[in] n_z_slices (int) - number of z slices in the PSF stack.

        \:param[in] normalize (bool) - flag. If true, the PSF 3D stack will be
            normalized before output.

        :return 3D numpy array with PSF. [z slice idx][x idx][y idx]
        """

        # check the arguments
        if len( offsetXYZ ) != 3:
            raise Exception( f'Offset array for XYZ should have length 3. '
                             f'Now it has length {len( offsetXYZ )}' )
        if ( abs(offsetXYZ[0]) > 1.0 ) or \
                ( abs(offsetXYZ[0]) > 1.0 ) or \
                ( abs(offsetXYZ[0]) > 1.0 ):
            print( 'Warning: your offset for PSF bead is more than '
                   '1 pixel or z_slice' )

        if z_range <= 0:
            raise Exception( f'z range for PSF 3D generator should be more '
                             f'than 0. Now it is {z_range} um' )
        if n_z_slices < 2:
            raise Exception( f'Number of z slices should be more or equal to 2. '
                             f'Now it is {n_z_slices}' )
        # all z positions that will be covered
        z_positions = np.linspace( -z_range, z_range, num=n_z_slices )

        x_shift = offsetXYZ[0]
        y_shift = offsetXYZ[1]
        z_shift = offsetXYZ[2]

        PSF = np.zeros( (n_z_slices, self.psfsize, self.psfsize),
                        dtype=np.float32 )

        # every z slice is produced using (k_x,k_y) pupil function
        # and 2D Fourier transform
        for iz in range( len( z_positions ) ):
            emXYZ = [ x_shift, y_shift, z_positions[iz] + z_shift ]
            PSF_z_slice, _ = self.psf( emXYZ, zern_coeff )
            PSF[iz] = PSF_z_slice

        if normalize:
            self.normalize_PSF( PSF )

        return PSF


def normalize(u):
    return u/np.max(u)


if __name__ == "__main__":

    np.random.seed(0)
    # zc = np.random.uniform(0.05,0.05,size=20)
    zc = np.random.uniform( 0.0, 0.0, size=20 )
    
    # tetrapod abberation
    # zc[11] = 1
    # zc[12] = 0

    # astigmatic aberration
    zc[4] = 1 # vertical astigmatism Z 2 2 or 6 in Noll index
    
    NA = 1.49         # 1.3
    fftres = 256      # 256
    pixelsize = 0.05  # 0.05
    lam = 0.6         # 0.6
    ROI = 31

    n1 = 1.5          # 1.5
    n2 = 1.33         # 1.33

    psfgen = PSF( fftres,
                  lam,
                  pixelsize,
                  NA,
                  len(zc) + 1,
                  focal=0,
                  evanescent=False,
                  psfsize=ROI,
                  ref_imm_fluid=n1, ref_sample=n2 )

    psf, pupil = psfgen.psf([0,0,0], zc)

    psf_3d = psfgen.psf3D( offsetXYZ=[0,0,0], zern_coeff=zc, z_range=3,
                           n_z_slices=36 )

    sum_per_z_slice = np.sum( psf_3d, axis=(1,2) )
    
    plt.figure()
    plt.imshow(np.angle(pupil))

    imgs = np.zeros((36,*psf.shape))
    x_shift = 0.1
    y_shift = 0.3

    for i,z in enumerate(np.linspace(-3,3,36)):
        imgs[i] = psfgen.psf( [x_shift * pixelsize,
                               y_shift * pixelsize,
                               z],
                              zc)[0]
        #imgs[i] = normalize(imgs[i])
    #zernike.imshow_imgs(imgs, "PSFs over z range", 6,6)
    zernike.imshow_imgs( psf_3d, "PSFs over z range", 6, 6 )
    