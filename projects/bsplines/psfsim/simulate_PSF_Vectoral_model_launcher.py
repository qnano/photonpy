"""
This python script reads parameters for Vectorial simulation calibration stacks
PSFs and launches MATLAB script.
"""

import os                # for adding path with this script to MATLAB path
import configparser      # parse INI file with PSF simulation parameters
import argparse          # parse arguments
import matlab.engine     # launch MATLAB script from python
import math              # for checking NaN values
import numpy as np
import tifffile
from simulation_result_reader import SimulationDataReader


# default INI filename
INI_FILENAME = 'simulation_config.ini'

# if True, simulated Vectorial PSF stacks will be saved separately in .tif files
_SAVE_SIMULATED_STACKS_SEPARATELY = True
_BASE_STACK_FILENAME = 'simulated_Vectorial_stack'

class PSFVectorialSim:
    """Class for reading INI file for simulation and launching MATLAB script.

    Uses Vectorial model for simulating astigmatic calibration beads.
    All simulation code is written in MATLAB, so we need to launch it.

    Attributes:
        outfile (str): filename for .mat file (without extension).

        Nbeads (int): number of calibration beads that will be simulated.

        roisize (int): number of pixels for Ox or Oy axis of one calibration
            bead. For example, 14x14 bead means ROI==14

        n_z_slices (int): number of z slices for one calibration stack (bead).

        z_range (float): the calibration stack is changing in interval
            [-z_range, +z_range] in nm

        background (float): background level.

        intensity (float): intensity for the simulated calibration beads.

        NA (float): numerical aperture.

        lambda_nm (float): lambda for the light in nm.

        pixelsize (float): one pixel size in nm.

        aberration (MATLAB 1D array []): contains 3 elements. 0 - x aberration,
            1 - y aberration, 2 - z aberration.

        use_poisson_noise (bool): if true, Poisson noise will be used for
            calibration beads simulation.

        offset_random_shift_range (MATLAB 1D array []): contains 3 elements.
            0 - x shift range, 1 - y shift range, 2 - z shift range.
            All beads will be shifted to the random number from
            [-shift_range, +shift_range].

        folder_with_matlab_script (str): usually, constant name. Just a name
            where MATLAB script for simulation is located.
    """

    def __init__( self ):
        # filenames
        self.outfile = 'simulated_Vectorial_model_PSF'

        # stack parameters
        self.Nbeads = 0
        self.roisize = 0
        self.n_z_slices = 0
        self.z_range = 0.0

        # PSF parameters
        self.background = 0.0
        self.intensity = 0.0

        # aberration and distortion parameters
        self.NA = 1.49
        self.lambda_nm = 690.0
        self.pixelsize = 100.0
        # self.aberration = matlab.double( [0.0, 0.0, 0.0] )
        self.aberration = np.array( [0.0, 0.0, 0.0] )

        # random and noise parameters
        self.use_poisson_noise = False
        # self.offset_random_shift_range = matlab.double( [0.0, 0.0, 0.0] )
        self.offset_random_shift_range = np.array( [0.0, 0.0, 0.0] )
        self.use_defined_shifts = False
        self.defined_shifts = np.zeros( [self.Nbeads, 3], dtype=float )

        self.folder_with_matlab_script = os.path.dirname( __file__ )

    def __str__( self ):
        """This function can be used for printing all parameters for debug"""

        s = ''
        s += 'outfile = ' + self.outfile + '\n'
        s += '\n'

        s += 'Nbeads = ' + str( self.Nbeads ) + '\n'
        s += 'roisize = ' + str( self.roisize ) + '\n'
        s += 'n_z_slices = ' + str( self.n_z_slices ) + '\n'
        s += 'z_range = ' + str( self.z_range ) + '\n'
        s += '\n'

        s += 'background = ' + str( self.background ) + '\n'
        s += 'intensity = ' + str( self.intensity ) + '\n'
        s += '\n'

        s += 'NA = ' + str( self.NA ) + '\n'
        s += 'lambda = ' + str( self.lambda_nm ) + '\n'
        s += 'pixelsize = ' + str( self.pixelsize ) + '\n'
        s += 'aberration_x = ' + str( self.aberration[0] ) + '\n'
        s += 'aberration_y = ' + str( self.aberration[1] ) + '\n'
        s += 'aberration_z = ' + str( self.aberration[2] ) + '\n'
        s += '\n'

        s += 'use_poisson_noise = ' + str( self.use_poisson_noise ) + '\n'
        if self.use_defined_shifts:
            for i in range( self.Nbeads ):
                shfts = self.defined_shifts[i]
                s += f'shift_bead_{i} = {shfts[0]} {shfts[1]} {shfts[2]}' + '\n';
        else:
            s += 'offset_random_shift_range_x = '\
               + str( self.offset_random_shift_range[0] ) + '\n'
            s += 'offset_random_shift_range_y = '\
               + str( self.offset_random_shift_range[1] ) + '\n'
            s += 'offset_random_shift_range_z = '\
               + str( self.offset_random_shift_range[2] ) + '\n'

        return s

    def _read_defined_shifts( self, randomPrms ):
        """Reads predefined shifts if any from the config file"""

        self.defined_shifts = np.zeros( [self.Nbeads, 3], dtype=float )

        self.use_defined_shifts = False
        for i in range( self.Nbeads ):
            opt = f'shift_bead_{i}'
            s_bead_shifts = randomPrms.get( opt, fallback=None )

            if s_bead_shifts is not None:
                self.use_defined_shifts = True
                s_bead_shifts = s_bead_shifts.split()

                if len( s_bead_shifts ) != 3:
                    raise ValueError( 'There should be 3 float numbers for'
                                      'a bead shift: for example,'
                                      '\'0.35 -0.1 0.2\'' )

                self.defined_shifts[i][0] = float( s_bead_shifts[0] )
                self.defined_shifts[i][1] = float( s_bead_shifts[1] )
                self.defined_shifts[i][2] = float( s_bead_shifts[2] )

        if not self.use_defined_shifts:
            self.defined_shifts = np.full( self.defined_shifts.shape, math.nan )
        else:
            # check that shift of the 1st bead is [0 0 0]
            if self.defined_shifts[0][0] != 0.0 or \
                    self.defined_shifts[0][1] != 0.0 or \
                    self.defined_shifts[0][2] != 0.0:
                raise ValueError( 'Bead shift for 0 bead should be '
                                  'always equal to [0 0 0]' )

            self.offset_random_shift_range[0] = 0.0
            self.offset_random_shift_range[1] = 0.0
            self.offset_random_shift_range[2] = 0.0

    def read_from_config( self, ini_filename ):
        """Reads all parameters from INI configuration file"""

        config = configparser.ConfigParser(
            interpolation=configparser.ExtendedInterpolation()
        )

        config.read( ini_filename )

        self.outfile = config['filenames']['outfile']

        stackPrms = config['stack_parameters']
        self.Nbeads = stackPrms.getint( 'Nbeads' )
        self.roisize = stackPrms.getint( 'roisize' )
        self.n_z_slices = stackPrms.getint( 'n_z_slices' )
        self.z_range = stackPrms.getfloat( 'z_range' )

        PSF_prms = config['PSF_parameters']
        self.background = PSF_prms.getfloat( 'background' )
        self.intensity = PSF_prms.getfloat( 'intensity' )

        aberPrms = config['aberration_parameters']
        self.NA = aberPrms.getfloat( 'NA' )
        self.lambda_nm = aberPrms.getfloat( 'lambda' )
        self.pixelsize = aberPrms.getfloat( 'pixelsize' )
        self.aberration[0] = aberPrms.getfloat( 'aberration_x' )
        self.aberration[1] = aberPrms.getfloat( 'aberration_y' )
        self.aberration[2] = aberPrms.getfloat( 'aberration_z' )

        randomPrms = config['random']
        self.use_poisson_noise = randomPrms.getboolean( 'use_poisson_noise' )
        self.offset_random_shift_range[0] =\
            randomPrms.getfloat( 'offset_random_shift_range_x', fallback=0.0 )
        self.offset_random_shift_range[1] =\
            randomPrms.getfloat( 'offset_random_shift_range_y', fallback=0.0 )
        self.offset_random_shift_range[2] =\
            randomPrms.getfloat( 'offset_random_shift_range_z', fallback=0.0 )

        self._read_defined_shifts( randomPrms )


    def call_MATLAB_launcher( self ):
        """Calls MATLAB function for simulation of Vectorial model PSFs"""

        eng = matlab.engine.start_matlab()

        eng.addpath( self.folder_with_matlab_script )

        if self.use_defined_shifts:
            N = np.prod( self.defined_shifts.shape )
            matlab_shift_arr = matlab.double(
                [list( self.defined_shifts.reshape( N ) )]
            )
        else:
            matlab_shift_arr = matlab.double( [[]] )

        eng.simulate_PSF_Vectoral_model(
            self.outfile,

            self.roisize,
            self.n_z_slices,
            self.z_range,
            int(self.Nbeads),

            self.NA,
            self.lambda_nm,
            self.pixelsize,
            matlab.double( [list( self.aberration )] ),

            self.use_poisson_noise,
            matlab.double( [list( self.offset_random_shift_range )] ),
            matlab_shift_arr,

            nargout=0
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Generator of aberrated calibration PSF stacks"
    )

    parser.add_argument(
        "-i", "--ini", required=False,
        default=INI_FILENAME,
        help="INI filename with parameters for the simulation" )

    args = parser.parse_args()

    # reading parameters from INI file
    inifilename = args.ini
    sim_launcher = PSFVectorialSim()
    sim_launcher.read_from_config( inifilename )

    # input parameters
    print( 'Print from python:' )
    print( sim_launcher )
    print( '==========================================================\n' )

    print( '\nCalling MATLAB script...' )
    sim_launcher.call_MATLAB_launcher()

    if _SAVE_SIMULATED_STACKS_SEPARATELY:
        print( f'\nReading simulated data from\n\t{sim_launcher.outfile}\n' )

        data = SimulationDataReader( sim_launcher.outfile )
        output_folder = os.path.dirname( sim_launcher.outfile )
        PSFs = data.PSFs
        name_base = _BASE_STACK_FILENAME

        print( 'Saving simulated PSF stacks separately:' )
        for i in range( len(PSFs) ):
            name = output_folder + f'/{name_base}_{i}.tif'
            with tifffile.TiffWriter( name ) as tif:
                tif.save( PSFs[i], compress=2 )
            print( f'\tsaving {name} (stack {i}) is complete' )

    print( 'DONE!' )