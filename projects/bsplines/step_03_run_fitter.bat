rem working with real data B-splines
rem py "process_movie.py" --pixelsize 0.1 --offset 100 --emgain 1 --roi 13 --minintensity 100 --plot --method bspline --sigma 2 --calibration ../../../OUTPUT/calibration_bspline.mat -o ../../../OUTPUT/fits.csv -i "C:/Work/Documents/TUDelft/SMLM challenge datasets/MT1 3D fitting/sequence-as-stack-MT1.N1.LD-AS-Exp.tif"

rem working with real data C-splines
rem py "process_movie.py" --pixelsize 0.1 --offset 100 --emgain 1 --roi 11 --minintensity 20 --plot --method cspline --calibration "C:\Work\Documents\TUDelft\SMLM challenge datasets\calibration stack\sequence-as-stack-Beads-AS-Exp_3Dcorr.mat" -o ../../../OUTPUT/fits.csv -i "C:\Work\Documents\TUDelft\SMLM challenge datasets\MT1 3D fitting\sequence-as-stack-MT1.N1.LD-AS-Exp.tif"

rem working with simulated by blinking_simulation.py data B-splines
py "process_movie.py" --pixelsize 0.1 --offset 0 --emgain 1 --roi 11 --minintensity 30 --plot --method bspline --calibration ../../../OUTPUT/calibration_bspline.mat -o ../../../OUTPUT/fits.csv -i "../../../OUTPUT/simulation_movie.tif"
