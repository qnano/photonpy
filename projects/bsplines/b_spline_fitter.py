"""
Class for launching B-spline fitting stage in MATLAB from python.
"""

import os                # for adding path with this script to MATLAB path
import matlab.engine     # launch MATLAB script from python
import argparse          # parse arguments


_DEFAULT_INPUT_FILE = '../../../OUTPUT/stacks.mat'
_DEFAULT_OUTPUT_FILE = '../../../OUTPUT/calibration_bspline.mat'
_DEFAULT_DEGREE = 5
_DEFAULT_N_CUBES = 5
_DEFAULT_CONTINUITY_ORDER = 1


class BSplineFitter:
    """Do B-spline fitting for several calibration PSF pixel stacks

    Produce B-coefficients for PSF model using PSf calibration stacks as
    data points. One pixel == one data point. Because of different subpixel
    shifts, pixels in PSF calibration stacks are slightly moved according to
    each over => more points for B-coefficients estimation.

    Input .mat file should contain:

    n_pixels (int) - ROI size
    n_z_slices (int) - number of z slices in one PSF stack
    z_range (float) - full z range for one PSF stack. PSF changes in
                      [-z_range/2, +z_range/2]
    stacks (4D float array) - [bead idx][z index][y index][x index]
        PSF stacks with values in every pixel
    shifts (2D float array) - [bead idx][axis idx: 0->x, 1->y, 2->z]
        subpixel positions according to the center of the ROI for every
        PSF calibration stack.

    Other parameters are unnecessary for B-spline fitter.

    Attributes:
        input_calibr_PSFs_file (str) - name of .mat file with PSF calibration
            stacks parameters

        output_bspline_file (str) - output .mat file name with B-spline
            parameters like B-coefficients

        poly_degree (int) - B-spline polynomial degree. Bigger degree requires
            more input PSF calibration stacks with different subpixel positions.

        n_mesh_cubes (int) - number of cubes in the tetrahedron mesh which
            B-splines use. Total number of cubes in the mesh will be
                n_mesh_cubes x n_mesh_cubes x n_mesh_cubes
            One cube contains 6 tetrahedrons.
            Bigger number of cubes requires more input PSF calibration stacks
            with different subpixel positions.

        continuity_order (int) - continuity order between tetrahedrons.
            Bigger continuity order requires less number of PSF calibration
            stacks.

        use_matlab_async (bool) - if True, MATLAB B-spline fitting stage will
            be launched asynchronously. method synchronize() should be used
            in order to wait the fitting stage.

        future - future for MATLAB engine. Used for synchronization.

        eng - MATLAB engine. Used as a field in order to not be deleted
            before synchronization.
    """

    def __init__( self, input_calibr_PSFs_file:str,
                  output_bspline_file:str,
                  polynomial_degree:int,
                  n_mesh_cubes:int,
                  continuity_order:int,
                  use_matlab_async:bool=False ):

        self.input_calibr_PSFs_file = input_calibr_PSFs_file
        self.output_bspline_file = output_bspline_file
        self.poly_degree = polynomial_degree
        self.n_mesh_cubes = n_mesh_cubes
        self.continuity_order = continuity_order
        self.use_matlab_async = use_matlab_async
        self.future = None
        self.eng = None

        self.folder_with_matlab_script = os.path.dirname( __file__ )

    def do_fitting( self ):
        """Launch B-spline fitting using MATLAB"""

        self.eng = matlab.engine.start_matlab()
        self.eng.addpath( self.folder_with_matlab_script )

        self.future = self.eng.step_02_fit_spline(
            self.input_calibr_PSFs_file,
            self.output_bspline_file,

            float( self.poly_degree ),
            float( self.n_mesh_cubes ),
            float( self.continuity_order ),
            nargout=0,
            background=self.use_matlab_async
        )

    def synchronize( self ):
        """Method which should be used if MATLAB is launched in async mode

        Waits MATLAB to finish B-spline fitting stage in order to produce
        .mat file with coefficients.
        """

        if self.use_matlab_async:
            check_that_matlab_is_over = self.future.done()
            if not check_that_matlab_is_over:
                print( f'\nWAITING MATLAB TO FINISH\n' )
            self.future.result()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Test for launching MATLAB B-spline fitting stage "
                    "from python"
    )

    parser.add_argument(
        "-i", "--input",
        default=_DEFAULT_INPUT_FILE,
        help="Input .mat file with calibration PSFs" )

    parser.add_argument(
        "-o", "--output",
        default=_DEFAULT_OUTPUT_FILE,
        help="Output .mat file with all B-spline parameters" )

    parser.add_argument(
        "-d", "--degree", default=_DEFAULT_DEGREE, type=int,
        help="B-spline polynomial degree" )

    parser.add_argument(
        "--ncubes", default=_DEFAULT_N_CUBES, type=int,
        help="Number of cubes in the tetrahedron 3D mesh" )

    parser.add_argument(
        "--continuity", default=_DEFAULT_CONTINUITY_ORDER, type=int,
        help="Continuity order between tetrahedrons in the B-spline" )

    args = parser.parse_args()

    print( 'Test for launching MATLAB B-spline fitting stage from python' )
    bfitter = BSplineFitter(
        input_calibr_PSFs_file=args.input,
        output_bspline_file=args.output,
        polynomial_degree=args.degree,
        n_mesh_cubes=args.ncubes,
        continuity_order=args.continuity
    )

    bfitter.do_fitting()

    print( 'DONE!' )