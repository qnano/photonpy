% BSPLINEN_CONSTRUCTBASISKT
%
%   Syntax:
%       basisKt = bsplinen_constructBasisKt(polybasis)
%
%   Description:
%       Function constructs the per simplex multinomial coefficient vector
%       for a given polynomial basis
%
%       Input parameters are:
%           - PolyBasis : double [dHat x n + 1]
%
%       Output parameters are:
%           - basisKt   : double [dHat x 1]
%
%   -------------------------------
%   File part of Simplex Spline toolbox
%   C.C. de Visser & E. de Weerdt
%   07-08-2009
%   11-07-2011
%   -------------------------------
function basisKt = bsplinen_constructBasisKt(PolyBasis, d)

if (nargin == 1)
    %   Polynomial degree
    d       = sum(PolyBasis(1,:),2);

    %   Calc the basis coefficient vector |kappa|! / kappa! 
    basisKt =  factorial(d)./prod(factorial(PolyBasis),2);
elseif (nargin == 2)
    n = PolyBasis;
    basis = bsplinen_constructBasis(n, d);
    basisKt =  factorial(d)./prod(factorial(basis),2);
end

return
%------------------------ end of file -------------------------------------