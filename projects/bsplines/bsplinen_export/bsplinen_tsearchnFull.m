% BSPLINEN_TSEARCHNFULL performs a simplex membership search for the data, in which a 
%   datapoint can be a member of more than 1 simplex. In contrast with TSEARCHN, 
%   BSPLINEN_TSEARCHNFULL is fully functional on non-convex triangulations.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%
%   [INCLUSIONMAP LAMBDADATA] = BSPLINEN_TSEARCHNFULL(PHI, TRI, XI, multimember)
%     performs a partitioned simplex membership search like TSEARCHN, but allows a datapoint 
%     to be contained by more than one parent simplex. Additionally, BSPLINEN_TSEARCHNFULL
%     is fully functional on non-convex triangulations when TSEARCHN returns erroneous results.
%
%   Input parameters of BSPLINEN_TSEARCHNFULL are.
%
%       PHI a matrix holding vertex coordinates, every row is corresponds
%        with a single vertex.
%
%       TRI a tesselation matrix as produced with DELAUNAYN
%
%       XI the data points, each row corresponds with a single observation
%
%       OPTIONS is a BSPLINEN_OPTIONS structure, or alternatively, a [1x1]
%        double holding the size of the partition.
%
%
function [InclusionMap BaryData] = bsplinen_tsearchnFull(PHI, TRI, XI, multimember)

    if (nargin < 4)
        multimember = 0;
    end
    
    InclusionMap = nan(size(XI, 1), 1);
    BaryData = nan(size(XI, 1), size(XI, 2) + 1);
    
    for t = 1:size(TRI, 1)
        [imap bdat] = tsearchn(PHI, TRI(t, :), XI);
        idx = find(~isnan(imap));
        if (~isempty(idx))
            InclusionMap(idx) = t;
            BaryData(idx, :) = bdat(idx, :);
        end
    end
    
    
    
    
    