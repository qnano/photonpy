
% LRSPLINEN_GENREG_OLS constructs the regressor matrix X and vector Y which
%   can be used with the linear regression equation Y = X*c + e.
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%
function [X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis, options)

    global STRUCTURES_LOADED;
    global struct_bsplinen_options;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    else
        if (isempty(STRUCTURES_LOADED))
            bsplinen_structures;
        end
    end

    if (isempty(XI))
        warning('The global data location vector is empty, returning...');
        return;
    end

    % load default options structure if none is supplied
    if (nargin < 6)
        options = struct_bsplinen_options;
    end
    if (isempty(options))
        options = struct_bsplinen_options;
    end


    if (options.stat_outputmsg == 1)
        if (strcmpi(options.stat_COVmethod, 'LS'))
            fprintf('BSPLINEN_GENREG_XY: Calculating Regressor Statistics for Ordinary Least Squares Estimator (OLS).\n');
        end
    end

    % sort the simplex indices
    TRI = sort(TRI, 2);
    % the number of simplices
    J = size(TRI, 1);
    % the number of B-coefficients per simplex
    [dhat nb] = size(polybasis);
    % the number of data points
    N = size(XI, 1);

    % precalculate the polynomial normalization coefficient
    % Kt = |kappa|! / kappa!
    basisKt = bsplinen_constructBasisKt(polybasis);

    % perform the datapoint membership search and construct X
    if (N <= options.data_partsize)
        % Dataset XI is small enough to handle all at once
        if (options.stat_outputmsg == 1), disp(sprintf('BSPLINEN_GENREG_XY: Performing Barycentric coordinate transformations...'));end
        [IMap LDat] = tsearchn(PHI, TRI, XI);
        % remove any datapoints that are outside of the triangulation
        nind = find(~isnan(IMap));
        IMap = IMap(nind);
        LDat = LDat(nind, :);
        XI = XI(nind, :);
        Z = Z(nind);
        N = length(nind);
        % create the dispersion matrix blocks
        if (options.stat_outputmsg == 1), disp(sprintf('BSPLINEN_GENREG_XY: Starting creation of regressor Matrix X and Y...'));end
        % create the dispersion matrix
        [X, Y] = lrsplinen_buildD_XY(IMap, LDat, Z, N, J, polybasis, basisKt, dhat);
    else
        xistart = 1;
        xiend = xistart + options.data_partsize;
        partcount = ceil(N / options.data_partsize);
        if (options.stat_outputmsg == 1), disp(sprintf('BSPLINEN_GENREG_XY: Dataset is partitioned in to %d partitions of size %d.', partcount, options.data_partsize));end
        IMap = zeros(N, 1);
        LDat = zeros(N, nb);
        for i = 1:partcount
            if (options.stat_outputmsg == 1), disp(sprintf('BSPLINEN_GENREG_XY: Performing simplex membership search for data partition %d...', i));end
            [IMaptmp LambdaTmp] = tsearchn(PHI, TRI, XI(xistart:xiend, :));
            nind = find(~isnan(IMaptmp));
            IMap = IMaptmp(nind);
            LDat = LambdaTmp(nind, :);
            IMap(xistart:xiend) = IMaptmp;
            Z = Z(nind);
            N = length(nind);
            LDat(xistart:xiend, :) = LambdaTmp;
            xistart = xiend + 1;
            xiend = xiend + options.data_partsize;
            if (xiend > N)
                xiend = N;
            end
        end

        if (options.stat_outputmsg == 1), disp(sprintf('BSPLINEN_GENREG_XY: Starting creation of regressor Matrix X and Y...'));end
        % create the dispersion matrix
        [X, Y] = lrsplinen_buildD_XY(IMap, LDat, Z, N, J, polybasis, basisKt, dhat);

    end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function basisKt = bsplinen_constructBasisKt(PolyBasis, d)

    if (nargin == 1)
        %   Polynomial degree
        d       = sum(PolyBasis(1,:),2);

        %   Calc the basis coefficient vector |kappa|! / kappa!
        basisKt =  factorial(d)./prod(factorial(PolyBasis),2);
    elseif (nargin == 2)
        n = PolyBasis;
        basis = bsplinen_constructBasis(n, d);
        basisKt =  factorial(d)./prod(factorial(basis),2);
    end



function Btk = bsplinen_constructBtk(polybasis, basisKt, Lambda)

    % the total number of B-coefs
    sizeBt = size(polybasis, 1);

    if (isempty(Lambda))
        Btk = [];
        return
    end

    % the total number of datapoints
    sizeX = size(Lambda, 1);

    %   Computation of Btk
    Btk = zeros(sizeX, sizeBt);
    for i = 1:sizeBt
        Btk(:,i)     = basisKt(i)*prod(Lambda.^(ones(sizeX,1)*polybasis(i,:)),2);
    end


function [X, Y] = lrsplinen_buildD_XY(IncMap, LDat, Z, N, J, polybasis, basisKt, dhat)

    % the vector holding the per-simplex data volume
    dvol = zeros(J, 1);
    % vector holding offsets in the SORTED Inclusion map
    OS = zeros(J, 4);
    % the global regressor vector in barycentric coordinates
    X = sparse(N, dhat * J);
    % dhat
    dhat = size(polybasis, 1);

    % sort the data
    [IncSort Ind] = sort(IncMap);
    LambdaSort = LDat(Ind, :);
    Y = Z(Ind);

    % count the number of datapoints per simplex
    for i = 1:N
        index = IncSort(i);
        dvol(index) = dvol(index) + 1;
    end
    % get the indices of the per-simplex data blocks
    offset = 1;
    for t = 1:J
        newoffset = offset + dvol(t);
        OS(t, :) = [offset (newoffset-1) ((t-1)*dhat+1) t*dhat];
        offset = newoffset;
    end

    % go through all simplices and calculate the per-simplex dispersion matrix blocks
    for t = 1:J
        % the per simplex regressor vector
        Xt = bsplinen_constructBtk(polybasis, basisKt, LambdaSort(OS(t, 1):OS(t, 2), :));
        X(OS(t,1):OS(t,2), OS(t,3):OS(t,4)) = Xt;
    end
