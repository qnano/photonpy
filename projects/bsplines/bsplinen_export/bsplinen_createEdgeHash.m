function hash = bsplinen_createEdgeHash(simp1, simp2)
    
    if (simp1 > simp2)
        tmp = simp2;
        simp2 = simp1;
        simp1 = tmp;
    end
    
    hash = sprintf('%d_%d', simp1, simp2);