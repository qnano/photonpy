% BSPLINEN_STRUCTURES defines all structures used in the BSPLINEN algorithm
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2009-2012
%              email: c.c.devisser@tudelft.nl
%              version 9.0
%
%   This file creates the following structures:
%
%       STRUCT_BSPLINEN
%   
%       STRUCT_FUZZYSPLINEN
%
%       STRUCT_GLOBALSPLINEN
%
%       STRUCT_DSPLINEN
%
%       STRUCT_MULTIREG
%
%       STRUCT_BMODEL
%
%       STRUCT_BMODEL_OPTIONS
%
%       STRUCT_BSPLINEN_OPTIONS
%
%       STRUCT_BSPLINEN_PLOTOPTS 
%
%       STRUCT_BSPLINEN_EDGE
%
%       STRUCT_BSPLINEN_EDGE2
%
%       STRUCT_VERTEX
%
%       STRUCT_SIMPLEX
%
%       STRUCT_STAR
%
%       STRUCT_FLAG
%
%       STRUCT_BSPLINEN_STATISTICS



% variable used for checking if the BSPLINEN structures have been loaded
global STRUCTURES_LOADED;
STRUCTURES_LOADED = 1;

% the N-variate bspline structure
global struct_bsplinen;
struct_bsplinen = struct('form', 'NB-', ... % form of spline function (user definable)
'ver', 8, ... % current version of struct
'PHI', [], ... % [Nv x n] coordinate matrix holding the vertex coordinates used in the triangulation
'TRI', [], ... % [T x n+1] index matrix defines the triangulation by indexing vertices in PHI
'coefs', [], ... % [T*dhat x 1] the B-coefficients of this spline function
'coefvars', [], ... % [T*dhat x 1] B-coefficient variances of this spline function
'coefCRLB', [], ... % [T*dhat x 1] Cramer-Rao bounds of B-coefficients of this spline function
'coefvarsEmp', [],... % [T*dhat x 1] Empirically calculated B-coefficient variances of this spline function
'resbounds1sigma', [], ... % [T*dhat x 1] 1-sigma empirically calculated confidence bounds on the B-coefs
'resmean', [], ... % [T*dhat x 1] mean of empirically calculated confidence bounds on the B-coefs
'dim', 2, ... % spline dimension (dim >= 1)
'degree', 1, ... % spline polynomial degree (degree >= 0)
'continuity', 0, ... % spline continuity order (-1 <= continuity < degree)
'number', 0, ... % total number of B-coefs (not used currently)
'deriv_factor', 0); % Derivative multiplication factor

% the N-variate fuzzy spline structure
global struct_fuzzysplinen;
struct_fuzzysplinen = struct('form', 'NB-', 'ver', 7, 'PHI', [], 'TRI', [], ...
    'coefs', [], 'coefvars', [], 'coefCRLB', [], ...
    'resbounds1sigma', [], 'resmean', [], ...
    'dim', 0, 'number', 0, 'degree', [], 'continuity', 0, 'deriv_factor', 0);

% % the N-variate bspline structure in global coordinates
global struct_globalbsplinen;
struct_globalbsplinen = struct('form', 'NB-', 'ver', 1, 'PHI', [], 'TRI', [], ...
    'coefs', [], 'polybasis', [], 'Lambda', [], 'Delta', [],...
    'dim', 0, 'number', 0, 'degree', 0, 'continuity', 0);

% DSABRE specific structure
global struct_DSABRE;
struct_DSABRE = struct('form', 'DSABRE', ...
    'spline', [], ... % Smooth local spline function (after DDA process)
    'spline0', [], ... % semi-continuous local spline function (after PME process)
    'Iglobal', [], ... % indices of B-coefficients of this partition into the global spline B-coefficient vector
    'Icorecoefs', [], ... % indices of B-coefficients in the core partition
    'Icurrentpart', [], ... % index of this partition in the global set of partitions (both linear and grid index)
    'isMasterpart', [], ... % switch indicating this partition is the master partition or not
    'Tt', [], ... % total number of triangles in this partition
    'iBrtBrBrt', [], ... % local partition reconstruction matrix
    'Ired2full', [], ... % indices from reduced B-coefs (in kernel of H) to full B-coefficients
    'Ifull2red', [], ... % indices from full B-coefficients to reduced B-coefficients (in kernel of H)
    'Yidx', [], ... % indices of frpm raw slope measurements to reconstruction vector Y (based on slope sensor simplex membership etc)
    'IVirtcontrolparts', [], ... % indices into B-coefficients in neighboring partitions that are used to determine DDA and PME offsets
    'VirtNBSet', []); % list of LOCAL indices of all virtual neighbor B-coefs, list of GLOBAL indices of all virtual neighbor B-coefs 

% Distributed spline structure
global struct_dsplinen;
struct_dsplinen = struct('form', 'D-', ... % form of spline function (user definable)
    'ver', 1, ... % current version of struct
    'index', 1, ... % partition index of this spline fn
    'spline0', [], ... % a STRUCT_BSPLINEN structure; this is the initial/temporary local spline function
    'spline', [], ... % a STRUCT_BSPLINEN structure; this is the local spline function
    'NBspline', [], ... % the neighbor master spline
    'Iglobal', [], ... % B-coef indices of B-coefficients in GLOBAL spline function
    'IlocalcontrolR0', [], ... % B-coef indices in LOCAL spline function of R0 continuity equations on edges of spline.TRI 
    'IglobalcontrolR0', [], ... % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of spline.TRI
    'INBlocalcontrolR0', [], ... % B-coef indices in LOCAL Neighbor spline function of R0 continuity equations on edges of MASTER neigbor-spline.TRI
    'INBglobalcontrolR0', [], ... % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of MASTER neigbor-spline.TRI
    'IAllglobalcontrolRn', [],... % B-coef indices in GLOBAL spline function of R0-Rn continuity equations of this partition and its neighbors
    'virtualPMEOffset',[],... % this is the virtual PME offsets of neighboring partitions of the current partition
    'VirtNBSet',[],... % cell array containing data on all neighbors of current partition
    'isMasterpart', 0,...
    'Icontrolparts', [],... % indices of control partitions
    'HsubLocal', [], ... % the sub-partition of the global smoothness matrix H that drives only local continuity 
    'HsubCo', [], ... % the sub-partition of the global smoothness matrix H that drives co-partition continuity between neigboring partitions
    'HnbEdge', [], ... % dual of the B-coefficients for Dual Ascent method
    'dualC_vpme', [], ... % the virtual PME component of the B-coefficients for Dual Ascent method 
    'invBtB', [], ... % dual of the B-coefficients for Dual Ascent method
    'dualInnovationMSE', inf, ... % innovation MSE of the dual of the B-coefficients for Dual Ascent method
    'offsetC', 0, ... % PME offset value
    'convergedPME', 0, ... % convergence switch of PME iteration, 0 = not converged, 1 = converged
    'convergedH', 0, ... % convergence switch of Dual Ascent smoothness iteration, 0 = not converged, 1 = converged
    'itcountPME', 0, ... % number of iterations for convergence of PME for current partition
    'itcountH', 0, ... % number of iterations for convergence of Dual Ascent smoothnes iteration for current partition
    'bcoords', []); % coordinates of b-coefficients of LOCAL spline)

% the Multi-Regression spline input structure
global struct_multireg;
struct_multireg = struct('type', 'spline', 'PT', [], 'XI', [], 'n', 1, 'd', 1, 'r', -1);

% the N-variate model containing a bsplinen1 model and its confidence bounds
global struct_bmodel;
struct_bmodel = struct('model', 0, 'stdmodel', 0, 'meanmodel', 0, 'confmodel', 0, 'Nsigma', 0);

% the N-variate model containing a bsplinen1 model and its confidence bounds
global struct_bmodel_options;
struct_bmodel_options = struct('degree', 6, 'continuity', 1, 'Nsigma', 10,...
    'statradius', .1, 'statEvalGridCount', 10, 'statInterpGridCount', 50,...
    'calcAutocorr', 0, 'perSimplex', 0, 'outputmsg', 0, 'plotstatistics', 0);

% the bsplinen options structure
global struct_bsplinen_options;
struct_bsplinen_options = struct('scheme', 'ls', 'solver', 'iterative', ...
    'parallelCores', 0, ...
    'filterH', 0, 'sortedH', 1, 'fullrankH', 0, ...
    'saveDELTA', 0, 'saveH', 0, 'saveBfb', 0, 'saveCOV', 0, 'saveX', 0, ...
    'loadDELTA', 0, 'loadH', 0, 'loadBfb', 0, 'loadCOV', 0, 'loadX', 0, ...
    'outputmsg', 0, ...
    'outputDELTA', 0, 'outputH', 0, 'outputBfb', 0, 'outputLambda', 0, ...
    'outputInclusionMap', 1,...
    'stat_calc', 0, 'stat_COVmethod', 'LS', 'stat_check', 0, 'stat_outputmsg', 0, ...
    'stat_outputX', 0, 'stat_outputCOV', 0, 'stat_outputSIGMA', 0, 'stat_correlationRadius', .2, ...
    'mreg_reduceMode', 'svd', 'mreg_outputMinv', 1, 'mreg_simpleMode', 0,...
    'resan_calc', 0, 'resan_userelativeRadius', 1, 'resan_radius', .1, ...
    'resan_calcAutocorr', 0, 'resan_perSimplex', 0, 'resan_outputmsg', 0, 'resan_plotstatistics', 0, ...
    'resan_evalGridCount', 10, 'resan_interpGridCount', 50,...
    'data_partsize', 100000, ...
    'bbuild_maxblkdiagsize', 500, 'bbuild_checkrank', 0,...
    'phi_optimizePHI', 0, 'phi_protectCHull', 1, 'phi_normalizedata', 1,...
    'phi_complexityMode', 1, 'phi_complexityLevel', .3, ...
    'phi_minGridCount', 2, 'phi_maxGridCount', 64,...
    'phi_cvhullres', 0.15, 'phi_gridres', 5, ...
    'tri_delaunayopts', [], 'tri_optimizePHI', 0, 'tri_protectCHull', 1, ...
    'tri_mindata', 50, 'tri_maxdata', -1, 'tri_cvhullres', 0.15, 'tri_gridres', 0.2, ...
    'tri_perfreq', 1e-2, 'tri_refinemode', 3, 'tri_refineMaxIts', 25, ...
    'tri_minangle', 20, 'tri_minvolume', .01, 'tri_maxvolume', .25, ...
    'tri_partitionmode', 'grid', 'tri_partitioncount', [], 'tri_partitionoverlap', 0, 'tri_partitionmaster', 0,...
    'solver_tol', 1e-6, 'solver_gradtol', 1e-10, 'solver_eps', 1e-6, 'solver_maxiter', 250, ...
    'solver_outputmsg', 0, 'solver_maxEpochs', 250, ... 
    'distrib_mode', 0, 'distrib_starlevel', 1, 'distrib_starmode', 1, 'distrib_trisubsets', [], 'distrib_deltasubsets', [], ...
    'distrib_smoothmode', 1, 'distrib_symmetricpart', 1, 'distrib_outputmsg', 1, ...
    'native_eval', 0, 'native_eval_newbcoefs', 0, 'native_tsearchn', 0, ...
    'native_construct', 0, 'native_gridres', 32, 'native_fulltsearchn', 0, 'native_dir', './bin', ...
    'saveMatPath', '.\\Matrix');

struct_bsplinen_options.tri_delaunayopts = {'Qbb', 'Qc', 'QJ1e-6'};    
    
% plot options structure
global struct_bsplinen_plotopts;
struct_bsplinen_plotopts = struct('plotSpline', 1, 'plotTRI', 0, 'plotBCoefs', 0, 'plotBNet', 0, 'plotData', 0, ...
    'plotPosition', [0 200, 640 480], ...
    'renderer', [], 'openGLmode', 'software',...
    'defaultAxesFontSize', 10, 'defaultTextFontSize', 10, 'paperPositionMode', 'auto',...
    'plotGrid', 1, 'plotAxes', 1, 'view', [-42 53], 'axis', [],...
    'material', [.3 .8 .9 25], 'poslight', 1, 'camlight', 1, ...
    'poslightPosition', [.5 .5 15], 'poslightStyle', 'local', 'camlightStyle', 'headlight',...
    'lighting', 'phong', 'shading', 'interp', 'colormap', 'jet',...
    'axisLabels', [], 'titleString', []);
   

% the edge structure
global struct_edge;
struct_edge = struct('ID', '', 'index', 0, 'simplex1', 0, 'simplex2', 0, 'ssv1', 0, 'ssv2', 0, 'cssv1', 0, 'cssv2', 0, 'edgevertices', []);

% new edge structure
global struct_edge2;
struct_edge2 = struct('index', 0, 'defined', 0, 'simplex1', 0, 'simplex2', 0, ...
    'ssv1', 0, 'ssv2', 0, 'ssv1bary', [], 'ssv2bary', [], 'cssv1', 0, 'cssv2', 0, 'edgevertices', [], ...
    'Hsub', [], 'conteq_ind', [], 'conteq_val', []);

% the star structure
global struct_star;
struct_star = struct('index', '0', 'starvertex', 0, 'simplices', [], 'vertices', [], 'edges', []);

% the vertex structure
global struct_vertex;
struct_vertex = struct('index', [], 'parent', 0, 'coords', []);

% the simplex structure
global struct_simplex;
struct_simplex = struct('index', 0, 'n', 0, 'datacount', 0, ...
    'vertices', [], 'vertexindices', [], 'dataindex', [], ...
    'data', [], 'dataval', [], 'databary', [], 'cindices', []);

% flag structure
global struct_flag;
struct_flag = struct('code', 0, 'preload_Bfb', 0, ...
    'preload_H', 0, 'preload_DELTA', 0, 'preload_PHI', 0, 'preload_TRI', 0);

% the output structure
global struct_bsplinen_statistics;
struct_bsplinen_statistics = struct('sindex', 0, 'mean', 0, 'var', 0, 'std',...
    0, 'rms', 0, 'autocorr', 0, 'maxerr', 0, 'maxerrind', 0, 'minerr', 0,...
    'minerrind', 0, 'samplesize', 0, 'PNstd', 0, 'Nstd', 0, 'P1Nstd', 0);


