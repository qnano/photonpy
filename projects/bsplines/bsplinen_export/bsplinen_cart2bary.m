% BSPLINEN_CART2BARY.m
%
%   Syntax:
%       lambda = bsplinen_cart2bary(simplex, X)
%
%   Desciption:
%       Function converts the cartesian coordinates of X into barycentric
%       coordinates LAMBDA with respect to SIMPLEX.
%
%       Input parameters are:
%           - simplex   : double [n + 1 x n]
%           - X         : double [Npoints x n]
%
%           SIMPLEX is a matrix containing the vertex coordinates of a
%           simplex in n space. The rows in SIMPLEX are the vertices, the
%           columns the coordinates. Therefore SIMPLEX always has n+1 rows
%           and n columns.
%           X is a maxtrix holding points in n-space in cartesian
%           coordinates. X must have n columns, with n the dimension of
%           SIMPLEX.
%
%       Output from BSPLINEN_CART2BARY is:
%           - lambda    : double [Npoints  x n + 1]
%
%           LAMBDA the barycentric coordinates of X with respect to
%           SIMPLEX. When LAMBDA contains only positive values, X is within
%           the convex hull of SIMPLEX. If LAMBDA contains one or more
%           negative values, then X is outside of the convex hull of
%           SIMPLEX.
%
%   -------------------------------
%   File part of Simplex spline toolbox
%   C.C. de Visser & E. de Weerdt
%   07-08-2009
%   -------------------------------
function lambda = bsplinen_cart2bary(simplex, X)

% The reference vertex is always chosen as the first simplex vertex.
% This can be done because barycentric coordinates are not dependent on
% the reference point.
v0      = simplex(1, :);

%   Determine size of the data
vcount  = size(simplex, 1);
Xcount  = size(X, 1);

%   Initialize output lambda
lambda  = zeros(Xcount, vcount);

%   Assemble matrix A and compute its inverse
A    = (simplex(2:end,:)-ones(vcount-1,1)*v0)';
invA = inv(A);

%   Perform the transformation
%   ... compute the relative coordinates
p   = X - ones(Xcount,1)*v0;

%   ... compute first part of lambda
lambda(:,2:end) = (invA*p')';

%   ... compute second part of lambda
lambda(:,1)     = 1-sum(lambda(:,2:end),2);

return
%------------------------- end of file ------------------------------------