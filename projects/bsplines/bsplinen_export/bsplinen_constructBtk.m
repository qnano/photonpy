
% BSPLINEN_CONSTRUCTBTK constructs the per simplex polynomial basis for
%   a given set of barycentric coordinates.
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2009
%              email: c.c.devisser@tudelft.nl
%
%   BTK = BSPLINEN_CONSTRUCTBTK(POLYBASIS, BASISKT, X)
%   Btk is given by the following relationship.
%
%       Btk(x) = |kappa|! / kappa! * lambda^kappa
%
%   where |kappa|! / kappa! is given as input argument BASISKT to speed up
%   computation.
%
%   Input parameters are:
%
%       POLYBASIS is the array holding the polynomial basis function indices.
%           It is an array with size ((DIMENSIONS + ORDER)! / DIMENSIONS!) x (DIMENSIONS+1).
%           The polynomial indices are stored row by row. The polynomial
%           indices are (row) sorted in descending order. Construct
%           POLYBASIS with BSPLINEN_CONSTRUCTBASIS.
%
%       BASISKT the multinomial coefficient vector. This is precalculated
%           because the Matlab function FACTORIAL performs very bad.
%           Construct BASISKT with BSPLINEN_CONSTRUCTBASISKT.
%
%       LAMBDA an N x n+1 matrix holding the data coordinates IN BARYCENTRIC
%           COORDINATES.
%
%
%   Ouput from BSPLINEN_CONSTRUCTBTK is:
%
%       An N x dhat matrix holding the polynomial values
%           in barycentric coordinates of the data locations X (also in barycentric coordinates).
%
function Btk = bsplinen_constructBtk(polybasis, basisKt, Lambda)

    % the total number of B-coefs
    sizeBt = size(polybasis, 1);

    if (isempty(Lambda))
        Btk = [];
        return 
    end
    
    % the total number of datapoints
    sizeX = size(Lambda, 1);

    %   Computation of Btk
    Btk = zeros(sizeX, sizeBt);
    for i = 1:sizeBt
        Btk(:,i)     = basisKt(i)*prod(Lambda.^(ones(sizeX,1)*polybasis(i,:)),2);
    end

return
%------------------------------ end of file -------------------------------
