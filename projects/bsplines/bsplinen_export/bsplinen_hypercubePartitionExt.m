% BSPLINEN_HYPERCUBEPARTITIONEXT Partitions a triangulation into a set of sub-triangulations using
%   a hypercube partitioning scheme.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          version 1.1
%
%             HypercubeData{i, 1}: partition boundary coordinates;
%             HypercubeData{i, 2}: partition simplex indices;
%             HypercubeData{i, 3}: TRIsub, the subtriangulation of TRI.
%             HypercubeData{i, 4}: simplex indices of STAR simplices in neighboring partitions
%             HypercubeData{i, 5}: TRIstarsub, the subtriangulation of TRI containing the STAR simplices
%             HypercubeData{i, 6}: LOCAL Simplex indices in HypercubeData{i, 3}; All simplices in the STAR simplex set that share an edge with Partition i.
%             HypercubeData{i, 7}: The vertex indices of all edge vertices of Partition i.
%             HypercubeData{i, 8}: Linear Hypercube indices and hypercube coordinate indices ([row, col] etc) of CURRENT partition
%                   HypercubeData{i, 8}(1): the linear hypercube index, HypercubeData{i, 8}(2:end) The  hypercube coordinate indices of the hypercube i.
%             HypercubeData{i, 9}: Linear Hypercube indices and hypercube coordinate indices ([row, col] etc) of NEIGHBORING partitions
%                   HypercubeData{i, 9}(1): the linear hypercube index, HypercubeData{i, 9}(2:end) The  hypercube coordinate indices of the neighbors 
%                   of hypercube i.

function HypercubeData = bsplinen_hypercubePartitionExt(TRI, PHI, InputSet, Edges, X, Y)
    
    HypercubeData = [];
    doDataSearch = 0;

    if (nargin < 2)
        return;
    elseif (nargin < 4)
        Edges = {};
    elseif (nargin >= 6)
        if (~isempty(X) && ~isempty(Y))
            doDataSearch = 1;
        end
    end
    
    % check whether InputSet is a bsplinen_options struct or a simple array holding the gridres
    if (isstruct(InputSet))
        bopts = InputSet;
        GridCount = bopts.tri_partitioncount;
    else
        GridCount = InputSet;
        % load default options structure
        bsplinen_structures;
        bopts = struct_bsplinen_options;
    end
    
    T = size(TRI, 1);
    n = size(PHI, 2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the hypercube counter

    % knotseqtype 0: the user supplied the knot sequence
    % knotseqtype 1: the user supplied the number of cells per dimension
    knotseqtype = 0;
    if (~iscell(GridCount))
        knotseqtype = 1;
        hypercube_count = zeros(n, 1);
        if (length(GridCount) == 1)
        	hypercube_count(:) = GridCount;
        else 
            hypercube_count = GridCount;
        end
    else
        hypercube_count = zeros(n, 1);
        for i = 1:n
            hypercube_count(i) = length(GridCount{i}) - 1;
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the analysis grid

    % get the min and max bounds
    Extremes = zeros(n, 2);
    HCGrid = cell(n, 1);
    for i = 1:n
        Extremes(i, 1) = min(PHI(:, i));
        Extremes(i, 2) = max(PHI(:, i));
    end

    % create the initial vertex set
    KnotSet = cell(n, 1); % the set of knots containing the hypercube coordinates
    HCIdxSet = cell(n, 1); % set of knots used to create the index array of hypercube indices
    HCSizeSet = cell(n, 1); % contains, per dimension, the size of the hypercubes
    
    KnotSetUnity = cell(n, 1);
    HCUnityGridSet = cell(n, 1);
    
    % go through all dimensions and construct knot sets for the hypercube grid
    for i = 1:n
        hcsize = (Extremes(i, 2)-Extremes(i, 1)) / hypercube_count(i);
        if (knotseqtype == 0)
            KnotSet{i} = GridCount{i};
        elseif (knotseqtype == 1)
            KnotSet{i} = Extremes(i, 1):hcsize:Extremes(i, 2);
        end
        HCIdxSet{i} = 1:hypercube_count(i);
        HCSizeSet{i} = KnotSet{i}(2:end) - KnotSet{i}(1:end-1);
        KnotSetUnity{i} = [0; 1];
    end
    
    % build the unity hypercube triangulation
    [HCUnityGridSet{:}] = ndgrid(KnotSetUnity{:});

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the hypercube grid
    CubeCoords = zeros(length(HCUnityGridSet{1}(:)), n);
    for i = 1:n
        CubeCoords(:, i) = HCUnityGridSet{i}(:);
    end
    % sort rows of vertices and vertex indices
    CubeCoords = sortrows(CubeCoords);    

    % Create the hypercube indexer
    HCGridIdxSet = cell(n, 1);
    [HCGridIdxSet{:}] = ndgrid(HCIdxSet{:});
    HCCount = numel(HCGridIdxSet{1});
    HCIndexer = zeros(HCCount, n);
    for i = 1:n
        HCIndexer(:, i) = HCGridIdxSet{i}(:);
    end
    Ncubes = size(HCIndexer, 1);
    ImaxHC = max(HCIndexer);
    
    % Go through all hypercubes, determine data content and analyze data range
    HypercubeData = cell(Ncubes, 9);
    TRImembershipcount = zeros(size(TRI, 1), 2);
    NeighborCubeSet = cell(Ncubes, 2); % list of neighboring cubes
    
    Nprog = 1;
    for i = 1:Ncubes
                
        % index of the i-th hypercube
        hcidx = HCIndexer(i, :);

        % calc hypercube direct neigbors
        Ineighborcubes = [ones(n) * diag(hcidx) - eye(n); ones(n) * diag(hcidx) + eye(n)];
        % remove invalid neighbors (indices < 0)
        validcount = 0;
        Ivalid = zeros(size(Ineighborcubes, 1), 1);
        for j = 1:size(Ineighborcubes, 1)
            if (all(Ineighborcubes(j, :) > 0) && all(Ineighborcubes(j, :) <= ImaxHC))
                validcount = validcount + 1;
                Ivalid(validcount) = j;
            end
        end
        % determine neighbor linear indices
        NSet = Ineighborcubes(Ivalid(1:validcount), :); 
        ILinNSet = zeros(size(NSet, 1), 1);
        for j = 1:size(NSet, 1)
            for k = 1:size(HCIndexer)
                if (~any(NSet(j,:) - HCIndexer(k, :)))
                    ILinNSet(j) = k;
                    break;
                end
            end
        end
        NeighborCubeSet{i, 1} = [ILinNSet NSet];
        
        % calculate hypercube coordinates
        CubeCoordsScaled = CubeCoords;
        for j = 1:n
            % calc hypercube coords
            CubeCoordsScaled(:, j) = CubeCoords(:, j) * HCSizeSet{j}(hcidx(j)) + KnotSet{j}(hcidx(j));
        end  
        HypercubeData{i, 1} = CubeCoordsScaled;
        %HypercubeData{i, 2} = [];

        % find all simplices that are located within CubeCoordsScaled
        tcount = 0;
        for t = 1:size(TRI, 1)
    
            % check whether simplex t has already been assigned!
            if (TRImembershipcount(t, 2) >= n+1)%(n+1)/2)
                % more than half of the vertices of t are already assigned!
                % tcount = tcount + 1;
                continue;
            end
                
            simp = PHI(TRI(t, :), :);
            incube = 1;
            % go through all dimensions
%             t1 = all(ones(size(simp))*(diag(CubeCoordsScaled(end, :) + 1e-6)) > simp, 2)
%             t2 = all(ones(size(simp))*(diag(CubeCoordsScaled(1, :) - 1e-6)) < simp, 2)
            vtest = all( [ones(size(simp))*(diag(CubeCoordsScaled(end, :) + 1e-6)) > simp, ones(size(simp))*(diag(CubeCoordsScaled(1, :) - 1e-6)) < simp], 2);
%             for j = 1:n % faster than matrix op!
%                 t1 = (CubeCoordsScaled(end, j) + 1e-6) > simp(:, j);
%                 t2 = (CubeCoordsScaled(1, j) - 1e-6) < simp(:, j);
%                 if (~all( (CubeCoordsScaled(end, j) + 1e-6) > simp(:, j) )) % check upper bound of HC
%                     % some of the n-th component of the simplex vertices outside max bounds of the hypercube
%                     incube = 0;
%                     break;
%                 end
%                 if (~all( (CubeCoordsScaled(1, j) - 1e-6) < simp(:, j) )) % check lower bound of HC
%                     % some of the n-th component of the simplex vertices outside min bounds of the hypercube
%                     incube = 0;
%                     break;
%                 end
%             end
            if (~any(vtest))
                continue;
            end
            % put simplex t in the hypercube that contains most of its vertices
            mvcount = sum(vtest);
            if (mvcount > TRImembershipcount(t, 2))
                TRImembershipcount(t, 1) = i;
                TRImembershipcount(t, 2) = mvcount;
            end
%             if (~all(vtest))
%                 incube = 0;
%             end
            % if the complete simplex is inside the hypercube, add its global indices to the
            % member simplices of this hypercube
%             if (incube)
%                 tcount = tcount + 1;
%                 HypercubeData{i, 2}(tcount) = t;
%             end
        end
        
    end    
        
    
    % list of to be removed (empty) hypercube partitions
    IremoveHypercubes = [];
    
    for i = 1:Ncubes
        hcidx = HCIndexer(i, :);
        
        HypercubeData{i, 2} = find(TRImembershipcount(:, 1) == i);
        % set the GLOBAL simplex indices
        %Ihypercubesimp = 
        %HypercubeData{i, 2} = HypercubeData{i, 2}(1:tcount);
        if (isempty(HypercubeData{i, 2}))
            IremoveHypercubes = [IremoveHypercubes; i];
            continue;
        end
        
        tripart = TRI(HypercubeData{i, 2}, :);
        % the triangulation partition in GLOBAL vertex indices
        HypercubeData{i, 3} = tripart;
        
        % get triangulation edges
        [tmp tmp edgeSimps edgeVerts Edges] = bsplinen_getFullTriangulationEdges(tripart, PHI, []);
        IedgeSimps = find(edgeSimps);
        HypercubeData{i, 6} = IedgeSimps;
        HypercubeData{i, 7} = edgeVerts;
        HypercubeData{i, 8} = [i hcidx];
        HypercubeData{i, 9} = NeighborCubeSet{i};

        % add simplex star to the hypercube data
        if (bopts.distrib_starlevel > 0)
            
            if (isempty(HypercubeData{i, 6}))
                continue;
            end
            
            % get star structure for this hypercube
            StarStruct = bsplinen_getStarStruct(TRI, bopts.distrib_starlevel, 1, HypercubeData{i, 2}(IedgeSimps));
            Istarsimp = setdiff(StarStruct, HypercubeData{i, 2});
            
            % find hypercube neighbors, and determine the Level (L0 is master)
            
            % save data
            HypercubeData{i, 4} = Istarsimp;
            HypercubeData{i, 5} = TRI(Istarsimp, :);
%             HypercubeData{i, 6} = IedgeSimps;
%             HypercubeData{i, 7} = edgeVerts;
%             HypercubeData{i, 8} = [i hcidx];
%             HypercubeData{i, 9} = NeighborCubeSet{i};
        end
        
    end

    if (~isempty(IremoveHypercubes))
        HypercubeData(IremoveHypercubes, :) = [];
    end
    
 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	 HELPER FUNCTIONS START HERE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    
function [NB, NI, NEV] = bsplinen_findneighbors(TRI, nCount)
    
    % declare cell arrays
    NB = {};    % vertex index array
    NI = {};    % simplex index array
    NEV = {};   % edge vertex array
    
    % The simplex dimension
    n = size(TRI, 2) - 1; 
    % Total number of simplices
    T = size(TRI, 1);
    % maximum simplex index
    vidxMax = max(TRI(:));

    % Return if there are less than 2 simplices in TRI or nCount has been
    % chosen smaller than 1.
    if (length(TRI) <= 1 || nCount < 1)
        disp('FINDNEIGHBOURS2: ERROR, the number specifying the minimum vertex count is too small, returning...');
        return;
    end
    
    % If the required number of shared vertices is larger than or equal to the simplex
    % dimension n, it must be reduced to n-1, as it does not make sense to
    % try and match every vertex between two simplices (which only happens
    % when the triangulation is erroneous).
    if nCount > n
        disp(sprintf('FINDNEIGHBOURS2: WARNING, The dimension of the simplices in TRI is %d. \n The required number of shared vertices was chosen to be %d. The required shared vertex count will be reduced to %d', n, nCount, n));
        nCount = n;
    end
   
    
    % create the row indices into the sparse adjacency matrix
    zeros(T, n+1);
    for i = 1:n+1
        rowIdx(:, i) = (1:T)';
    end
    rowIdxLin = rowIdx(:);

    % create the sparse adjacency matrix
    TRIADJ = sparse(rowIdxLin, TRI(:), 1, T, vidxMax);
    
    
    % now go through TRI and retrieve all simplices having nCount equal
    % vertices
    for i = 1:T
        % the to-be-tested simplex
        SIMPTEST = TRI(i, :);
%         % the adjacency information for the to-be-tested simplex
%         SIMPTESTIDX = TRIADJ(i, :);
        
        % the columns holding the vertex indices of SIMPTEST
        colIdx = ones(T, 1) * SIMPTEST;
        colIdxLin = colIdx(:);
        
        % the binary adjacency matrix for a single simplex
        TRIADJTEST = sparse(rowIdxLin, colIdxLin, 1, T, vidxMax);
        
        % perform the boolean adjacency test
        TRIBOOL = TRIADJ & TRIADJTEST;
        % count the number of vertex matches per simplex
        TRICOUNT = sum(TRIBOOL, 2);
        % the neigboring simplices have n matching vertices
        NbSimpsIdx = find(TRICOUNT == nCount);
                
        % counter keeping track of neighbour count
        ncount = 0;
        
        % add the data of the neigboring simplices of SIMPTEST to the lists 
        for j = 1:length(NbSimpsIdx)
            ncount = ncount + 1;
            % insert the neigbouring simplex itself
            nsimp = TRI(NbSimpsIdx(j), :);
            NB{i, ncount} = nsimp;
            % and insert its index
            NI{i, ncount} = NbSimpsIdx(j);
            % add the edge vertices
            NEV{i, ncount} = find(TRIBOOL(NbSimpsIdx(j),:))';
        end
        
       
        
    end

    
    
function Lambda = bsplinen_cart2bary(simplex, X)
    
    % The reference vertex is always chosen as the first simplex vertex.
    % This can be done because barycentric coordinates are not dependent on
    % the reference point.
    v0      = simplex(1, :);
    vcount2 = size(simplex, 1) - 1;
    Xcount  = size(X, 1);
    
    Lambda = zeros(Xcount, vcount2 + 1);
    %vcount2 = length(simplex(:, 1)) - 1;
    
    % assemble matrix A
    A = zeros(vcount2, vcount2);
    count = 1;
    for i = 2:length(simplex(:, 1))
        A(:, count) = (simplex(i, :) - v0)';
        count = count + 1;
    end
    
    
    for i = 1:Xcount
        % relative coordinates of x
        p = (X(i, :) - v0)';

        % the last (n) barycentric coordinates. 
        lambda1 = A \ p;

        % the first barycentric coordinate; lambda0
        lambda0 = 1 - sum(lambda1);

        % insert lambda0 into the Lambda vector
        Lambda(i, 1) = lambda0;
        Lambda(i, 2:end) = lambda1;
    end

function [ssvIndex ssvCol] =  bsplinen_getSSV(simplex1, simplex2)

    % simplex 1 and simplex 2 must both be row vectors!
    
    % find the ssv: the ssv is not shared between simplices!
    ssvIndex = 0;
    ssvCol = 0;
    for i = 1:length(simplex1)
    
        edgevert = find(simplex1(i) == simplex2(:));
        if (isempty(edgevert) == 1)
            ssvIndex = simplex1(i);
            ssvCol = i;
            break;
        end
    
    end
        
    
function Edges = bsplinen_getEdges(PHI, TRI)

    global STRUCTURES_LOADED;
    global struct_edge2;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    end
    
    T = size(TRI, 1);

    % the number of vertices per simplex
    v_count = length(TRI(1, :));
    % dimension
    n = v_count - 1;

    %EdgeMap = sparse(length(TRI(:, 1)), length(TRI(:, 1)));
    EdgeMap = sparse(size(TRI, 1), size(TRI, 1));
    Edges = {};

    % find the natural neighbours for every simplex in TRI
    [NB, NI, NEV] = bsplinen_findneighbors(TRI, n);
    
    
    if (isempty(NB))
        return;
    end
    
    edgeCount = 1;
    % go through all simplices
    % for i = 1:T
    for i = 1:size(NB, 1)
        % define the ssv's for this edge
        % go through all neighboring simplices of the simplex at 'i'
        for nb = 1:size(NB, 2)

            if (~isempty(NB{i, nb}))

                % setup the edge structure
                edge = struct_edge2;
                edge.index = edgeCount;                
                edge.simplex1 = i;
                edge.simplex2 = NI{i, nb};
                edge.edgevertices = NEV{i, nb};
                
                % get the ssv of the first simplex 
                [edge.ssv1 edge.cssv1] = bsplinen_getSSV(TRI(i, :), NB{i, nb});
                % get the ssv of the second simplex 
                [edge.ssv2 edge.cssv2] = bsplinen_getSSV(NB{i, nb}, TRI(i, :));

                % calculate the barycentric coordinates of the SSV's with
                % respect to the target simplices
                edge.ssv1bary = bsplinen_cart2bary(PHI(TRI(edge.simplex2, :), :), PHI(edge.ssv1, :));
                edge.ssv2bary = bsplinen_cart2bary(PHI(TRI(edge.simplex1, :), :), PHI(edge.ssv2, :));

                % check whether we already have an edge for the same
                % set of simplices (the dual edge...).
                s1 = edge.simplex1;
                s2 = edge.simplex2;
                if (s1 > s2)
                    s1 = edge.simplex2;
                    s2 = edge.simplex1;
                end
                if (EdgeMap(s1, s2) == 0)
                    EdgeMap(s1, s2) = 1;
                    Edges{edgeCount, 1} = edge;
                    edgeCount = edgeCount + 1;
                end
                
            end

        end 

    end    
    
    
    test=1;
    
    
function [TRIedges PHIedges edgeSimps edgeVerts Edges] = bsplinen_getFullTriangulationEdges(TRI, PHI, Edges, alledges)

    bsplinen_structures;
    
    TRIedges = {};
    PHIedges = {};
    edgeSimps = [];
    edgeVerts = [];
    
    if (nargin < 3)
        % get all edges in the triangulation
        Edges = bsplinen_getEdges(PHI, TRI);
        alledges  = 1;
    elseif (nargin < 4)
        if (isempty(Edges))
            Edges = bsplinen_getEdges(PHI, TRI);
        end            
        alledges = 1;
    elseif (nargin == 4)
        if (isempty(Edges))
            Edges = bsplinen_getEdges(PHI, TRI);
        end            
    end
    
    % if only external edges are required, then return the results from the (simpeler) function bsplinen_getTriangulationEdges.
    if (~alledges)
        [TRIedges PHIedges edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);
        return;
    end
    
    % dimension of TRI
    n = size(PHI, 2);
    
    T = size(TRI, 1);

    
    % compile a list that translates simplices to edges
    SimpEdgeList = cell(T, 1);
    for i = 1:size(Edges, 1)
        edge = Edges{i};
        SimpEdgeList{edge.simplex1} = [SimpEdgeList{edge.simplex1}; i];
        SimpEdgeList{edge.simplex2} = [SimpEdgeList{edge.simplex2}; i];
    end
    
    idxset = (1:n+1)';
    % the permutation vector for the edge facet indices    
    facetidx = nchoosek(idxset, n);

    % the triangulation edges are all edge facets that are not in Edges...
    TRIedges = cell(T, 1);
    PHIedges = cell(T, 1);
    edgeSimps = zeros(T, 1);
    edgeVerts = zeros((n+1)*T, 1);
    
    % go through all simplices
    evcount = 0;
    for i = 1:T
        % vertex indices of simplex i
        simpidx = TRI(i, :);
        % get the facets of simplex i
%         facets = simpidx(facetidx);
        facets = simpidx(facetidx); % CDV 16-01-2012
        
        % The principle is that any facets that are not in any edge structure, are edge facets!

        edgeidx = SimpEdgeList{i};
        if (isempty(edgeidx))
            % no edges, so all facets are edge facets!
            TRIedges{i} = facets;
            PHIedges{i} = PHI(facets', :);
            edgeVerts((evcount+1):(evcount+numel(facets))) = facets(:);
            evcount = evcount + numel(facets); % total number of vertex indices for this facet
            continue;
        end
        
        % Go through all facets and see if they are edge facets
        edgefacetlist = zeros(size(facets, 1), 1);
        edgefacetcount = 1;
        hasedgefacets = 0;
        for fi = 1:size(facets, 1)
            facet = sort(facets(fi, :));
            isedgefacet = 1;
            % go through all edges of simplex 'simp'
            for j = 1:length(edgeidx)
                edge = Edges{edgeidx(j)};
%                 ev = edge.edgevertices;
                ev = sort(edge.edgevertices); % CDV 16-01-2012

                % find the mutual vertex indices in the current facet and the edge vertices
%                 mutualverts = intersect(facet, ev); % slow function
%                 if (length(mutualverts) == n)
%                     % there are n mutual vertices, so this facet is actually an edge facet!
%                     isedgefacet = 0;
%                     break;
%                 end
                if (~any(facet' - ev))
                    % there are n mutual vertices, so this facet is actually an edge facet!
                    isedgefacet = 0;
                    break;
                end
            end
            if (isedgefacet)
                edgefacetlist(edgefacetcount) = fi;
                edgefacetcount = edgefacetcount + 1;
                hasedgefacets = 1;
            end
            
        end
        
        if (hasedgefacets)
            edgefacetlist = edgefacetlist(1:edgefacetcount-1);
            % insert edge facets
            edgefacets = facets(edgefacetlist, :);
            TRIedges{i} = edgefacets;
            PHIedges{i} = PHI(TRIedges{i}', :);
            edgeSimps(i) = 1;
            edgeVerts(evcount+1:evcount+numel(edgefacets)) = edgefacets(:);
            evcount = evcount + numel(edgefacets); % total number of vertex indices for this facet
        end
        
        
    end
    
    % cutoff the edge vertex list, and make it unique
    edgeVerts = unique(edgeVerts(1:evcount));

    
function Star = bsplinen_getStar(TRI, V)

    if (length(V) == 1)
        [Star cind] = find(TRI == V);
    else
        Star = cell(length(V), 1);
        for i = 1:length(V)
           [star cind] = find(TRI == V(i));
           Star{i} = star;
        end
    end


function StarStruct = bsplinen_getStarStruct(TRI, starlevel, starstrength, simpInd)

    if (starstrength <= 0)
        starstrength = 1;
    end
    
    Simplices = simpInd;

    if (starlevel == 0)
        StarStruct = simpInd;
    end
    
    % assemble the complete star structure of level 'starlevel'
    for level = 1:starlevel
        % get all vertices in the simplex
        StarStruct = [];
        for t = 1:size(Simplices, 1)
            % go through all the vertices of the simplex 't' and assemble their stars.
            vertices = TRI(Simplices(t), :);
            simpStar = [];
            for i = 1:length(vertices)
                % get the star of  vertex 'i' in simplex 't'
                star = bsplinen_getStar(TRI, vertices(i));

                % add the simplices in star, if they are not already added
                for j = 1:length(star)
                    if (~any(simpStar == star(j)) )
                        simpStar = [simpStar; star(j)];
                    end
                end
            end
            % filter simpStar on the star vertex count
            if (starstrength > 1)
                simpStarTmp = [];
                for i = 1:length(simpStar)                    
                    % start counting the number of vertices that simpStar(i)
                    % shares with Simplices(t)
                    simpv = TRI(simpStar(i), :);
                    count = 0;
                    for j = 1:length(simpv)
                        if (any(vertices == simpv(j)))
                            count = count + 1;
                        end
                    end
                    if (count >= starstrength)
                        simpStarTmp = [simpStarTmp; simpStar(i)];
                    end
                end
                simpStar = simpStarTmp;
            end
            
            % add the simplices in simpStar to all the simplices in star
            % level 'level', if they are not already added
            for i = 1:length(simpStar)
                if ( ~any(StarStruct == simpStar(i)) ) 
                    StarStruct = [StarStruct; simpStar(i)];
                end
            end
            %StarStruct = [StarStruct; simpStar];
            
        end
        Simplices = StarStruct;
        
    end
    
    if (length(simpInd) > 1)
        return;
    end
    
    % finally, add the vertices of simplex 'simpInd' to the top of the list 
    % (if it is not already there)
    subsimp = find(StarStruct == simpInd);
    if (isempty(subsimp))
        StarStruct = [simpInd; StarStruct];
    else
        if (subsimp ~= 1)
            StarStruct(subsimp) = [];
            StarStruct = [simpInd; StarStruct];
        end
    end






    
    