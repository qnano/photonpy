% function cdot_u = bsplinen_HJB_cdot(t, coefs, u, Dt, invBdtBdBdt, BPxFxN, BPyFyN, BPxxpBPyyN)
function c_dot = bsplinen_HJB_cdotBWDGal(t, coefs, invMassN2, Kb_ax1N, Kb_ax2N, negKb_axxN)

    p2 = Kb_ax2N * coefs; % second co-state in kernel of HK 
    u = 1*sign(p2); % maximizing optimal controller for system
%     u = ones(size(u));
    
%     % save u for diagnostics
%     global InputSet;
%     global Ode45FuncIts;
%     Ode45FuncIts = Ode45FuncIts + 1;
%     InputSet(Ode45FuncIts, :) = u';

    Hxyu = Kb_ax1N*coefs + p2.*u;
    c_dot = invMassN2 * (negKb_axxN*coefs - Hxyu);

    