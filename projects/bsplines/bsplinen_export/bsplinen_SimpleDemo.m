%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bsplinen_SimpleDemo provides a simple demonstration of the scattered data fitting capabilities of multivariate Simplex B-splines.
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.0
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


close all;

% load structures from disk
bsplinen_structures;

% init random number generator
rand('twister', 101);

% polynomial polybasis properties
n = 2; % spline dimension
d = 3; % polynomial degree
r = 0; % continuity order


printfigs     = 0; % print figures switch

simpdatacount = 10; % data content per simplex

vertex_count  = 39; % number of vertices used in triangulation

evalres       = .01; % resolution for eval
gridres       = .01; % data resolution for gridded data


% view angle and elevation
viewaz = -132;
viewel = 48;

figpath = '.\figures\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Calculate polynomial basis function multi-index permutations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

polybasis = bsplinen_constructBasis(n, d);
dhat      = size(polybasis, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Construct random triangulation in ([0,1],[0,1])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PHI = rand(vertex_count, 2); % generate vertex array
TRI = delaunayn(PHI); % create triangulation
TRI = sortrows(TRI); % sort vertex indices

TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)

% the number of simplices
T = size(TRI, 1);

fprintf('Total number of simplices: %d, total number of vertices: %d\n', T, vertex_count);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Data generating functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%fx = 'x1.^2.*sin(5*x1 - 5*x2.^2)';
%fx = 'x1.^2.*cos(10*x1 - 15*x2)';
%fx = 'exp(-(15*(x1-.5).^2 + 10*(x2-.5).^2))';
fx = 'x1.^2.*cos(10*x1 - 5*x2) - x2.^2.*sin(10*x1 + 5*x2)';
%fx = '(x1-1).^2.*cos(10*(x1-1) - 15*(x2-1)) - (x2-1).^2.*sin(10*(x1-1) + 15*(x2-1))';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evaluation locations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% evaluation locations (square grid)
[xxe yye] = ndgrid((0:evalres:1)', (0:evalres:1)');
XIeval = [xxe(:), yye(:)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Dataset generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generate the data locations
XI = zeros(T*simpdatacount, 2);

% use barycentric coordinate rules to create uniformly distributed random dataset inside each simplex
for i = 1:T
    % generate data in barycentric coordinates
    Set = rand(simpdatacount, 3);
    % normalize the barycentric coordinates
    sumSet = sum(Set, 2);
    Set = Set./[sumSet sumSet sumSet];
    % transform to cartesian coords
    Xt = bsplinen_bary2cart(PHI(TRI(i, :), :), Set);

    XI((i-1)*simpdatacount+1:i*simpdatacount,:) = Xt;
end

% % add datapoints at vertex locations (CHEATING!!!)
% XI = [XI; PHI];


% function values
x1 = sym('x1');
x2 = sym('x2');

% create identification dataset with some matlab magic
x1 = XI(:,1);  x2 = XI(:,2);
Z = eval(fx);
% create evaluation dataset
x1 = XIeval(:,1);  x2 = XIeval(:,2);
Zeval = eval(fx);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Estimate B-coefficients
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI}, {});

% create the matrix holding the smoothness constraints
H = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, r);
% Href = rref(H);

% create the regression matrix for the given dataset, triangulation, and degree.
[X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis);


% dispersion matrix construction
B = X'*X;
% construct complete KKT matrix
M = full([B H'; H sparse(size(H, 1), size(H, 1))]);

% simple constrained least squares estimator for B-coefficients & Lagrange multipliers (very inefficient!)
bcoefs_Lmult = pinv(M) * [X'*Y; zeros(size(H,1), 1)];

% estimated b-coefficients are located in the top T*dhat rows of bcoefs_Lmult
bcoefs       = bcoefs_Lmult(1:size(B, 1));

% Construct spline function structure
spline = struct_bsplinen;
% Set the bsplinen structure properties
spline.PHI        = PHI;
spline.TRI        = TRI;
spline.degree     = d;
spline.continuity = r;
spline.dim        = n;
spline.coefs      = bcoefs;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evaluate spline function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

val_spline      = bsplinen_evalExt(spline, XIeval);
err_spline      = Zeval - val_spline;
rms_spline      = sqrt(mse(err_spline));
rmsrel_spline   = rms_spline / sqrt(mse(Zeval));

% calculate locations of B-coefficients
Bcoords = bsplinen_calcBCoefCoordinates(spline);
% triangulate B-coef locations
TRIBcoords = delaunayn(Bcoords);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Do the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
close all;

%%
plotID = 101;

figure(plotID);
set(plotID, 'Position', [1 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
trimesh(TRI, PHI(:, 1), PHI(:, 2), 'Color', 'b');
axis([-.2 1.2 -.2 1.2]);
count = 1;
bind = 1;
plot(XI(:,1), XI(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 0 0],'MarkerSize', 3);% make it look nice
for i = 1:size(Bcoords, 1)
    plot(Bcoords(i,1), Bcoords(i,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 1 0],'MarkerSize', 1);% make it look nice
    Bcoef = sprintf('c_%d_%d_%d', polybasis(bind, 1), polybasis(bind, 2), polybasis(bind, 3));
    bind = bind + 1;
    if (count == 1)
        offset = [-.025 -.045];
    elseif (count == 2)
        offset = [-.08 .035];
    elseif (count == 3)
        offset = [.025 .035];
    end
    %text(Bcoords(i, 1)+offset(1), Bcoords(i, 2)+offset(2), Bcoef);
    if (bind > dhat)
        count = count + 1;
        bind = 1;
    end
end
for i = 1:size(TRI, 1)
    str = sprintf('t_{%d}', i);
    xytext = bsplinen_bary2cart(PHI(TRI(i, :), :), [1/3 1/3 1/3]);
    text(xytext(1), xytext(2), str);
end
for i = 1:size(PHI, 1)
    str = sprintf('v_{%d}', i);
    text(PHI(i, 1)+.025, PHI(i, 2), str);
end
xlabel('x');
ylabel('y');
titstr = sprintf('Triangulation consisting of %d simplices', T);
title(titstr);
if (printfigs == 101 || printfigs == 1)
    fname = sprintf('fig_Triangulation_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 1001;

figure(plotID);
set(plotID, 'Position', [500 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_spline);
maxz = max(val_spline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_spline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(-23, 36);
if (printfigs == 1001 || printfigs == 1)
    fname = sprintf('fig_SplineResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Finally, some outputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n---------------------------------------------------------------------\n');
fprintf('Spline error RMS = %d, relative error RMS = %2.2f [%s]\n', rms_spline, 100*rmsrel_spline, '%');
fprintf('---------------------------------------------------------------------\n');
