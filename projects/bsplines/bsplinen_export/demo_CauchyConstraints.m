
% load structures from disk
bsplinen_structures;

% init random number generator
rand('twister', 101);

printfigs     = 0; % print figures switch

% polynomial polybasis properties
n = 2; % dimension of objective function
d = 3; % polynomial degree of objective function
r = 1; % continuity order of objective function


% boundary condition settings
BCval = 0; % value of boundary constraint
dBC = 0; % degree of boundary conditions

% triangulation settings
griddedTRI    = 1;
TRIgridcount  = [4 4];
TRItype       = 1;
vertex_count  = 20; % number of vertices used in triangulation

evalres       = 50; % resolution for eval
datares       = 100; % data resolution for gridded data

% view angle and elevation
viewaz = -132;
viewel = 48;

figpath = '.\figures\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Calculate polynomial basis function multi-index permutations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

polybasis = bsplinen_constructBasis(n, d); % Objective function polynomial basis indices
dhat      = size(polybasis, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Set options for constructing simplex splines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

polybasis = bsplinen_constructBasis(n, d);
dhat = size(polybasis, 1);

% bsplinen options
bopts = struct_bsplinen_options;
bopts.outputmsg             = 0;
bopts.solver_maxiter        = 200;
bopts.solver_tol            = 1e-6;
bopts.solver_gradtol        = 1e-10;
bopts.solver_eps            = 1e-6;
bopts.solver_doplots        = 0;
bopts.solver_outputmsg      = 0;
bopts.solver                = 'iterative';%'ls''iterative''direct';
bopts.saveH                 = 1;
bopts.loadH                 = 0;
bopts.filterH               = 0;
bopts.fullrankH             = 1;
bopts.sortedH               = 1;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Initial condition functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% fx = '.5*x1 + 1*x2';
% fx = 'x1.^2.*sin(5*x1 - 5*x2.^2)';
% fx = 'x1.^2.*cos(10*x1 - 15*x2)';
% fx = 'exp(-(15*(x1-.5).^2 + 10*(x2-.5).^2))';
% fx = 'x1.^2.*cos(10*x1 - 15*x2) - x2.^2.*sin(10*x1 + 15*x2)';
% fx = '(x1-1).^2.*cos(10*(x1-1) - 15*(x2-1)) - (x2-1).^2.*sin(10*(x1-1) + 15*(x2-1))';
fx = 'franke(x1, x2)'; 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Data & Evaluation locations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% data locations
[xx yy] = ndgrid((0:1/(datares-1):1)', (0:1/(datares-1):1)');
XI = [xx(:), yy(:)];
   
% evaluation locations (square grid)
[xxe yye] = ndgrid((0:1/(evalres-1):1)', (0:1/(evalres-1):1)');
XIeval = [xxe(:), yye(:)];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Construct triangulation in ([0,1],[0,1])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (griddedTRI)
    % create a gridded triangulation
    XItmp = [0 0; 0 1; 1 0; 1 1]; 
    [TRI PHI] = bsplinen_CDVtriangulate(XItmp, TRIgridcount);
    TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)
else
    PHI = rand(vertex_count, 2); % generate vertex array
    TRI = delaunayn(PHI); % create triangulation
    TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)
end
% the number of simplices
T = size(TRI, 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Dataset generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% function values
x1 = sym('x1');
x2 = sym('x2');
    
% create identification dataset with some matlab magic
x1 = XI(:,1);  x2 = XI(:,2);
Z = eval(fx);
x1 = XIeval(:,1);  x2 = XIeval(:,2);
Zeval = eval(fx);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Build Nominal Unconstrained Spline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

disp('Building Nominal Unconstrained LS Spline...');

tic;
[fnLS outLS] = bsplinen({PHI, TRI}, [XI Z], {}, d, r, bopts);
time0 = toc;

Bcoords = bsplinen_calcBCoefCoordinates(fnLS);
TRIBcoords = delaunayn(Bcoords);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%            Define constraints on the vertices of simplex 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

% Important observation: the derivative of the B-form polynomials is
% determined by the value of this derivative at the location of the
% B-coefficients. All evaluation values on edges and lines of
% B-coefficients then have the same derivative value as the derivative at
% the B-coefs!

[DC, dc] =  bsplinen_constructCauchyConstraints(TRI, PHI, d, dBC, BCval);

% DC(end-10:end, :) = [];
% dc(end-10:end) = [];

% 
% % make DC of full rank...
% if (~isempty(DC))
%     DCorig = DC;
%     DCrref = rref([DC dc]);
%     rankDC = rank(DCrref);
%     DC = DCrref(1:rankDC, 1:end-1);
%     dc = DCrref(1:rankDC, end);
% end
% %fprintf('Required n/o constraints: %d, current n/o constraints: %d (d=%d, dBC=%d)\n', numConstraints*length(constrainedTRI), length(dc), d, dBC);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               Build Nominal Constrained LS Spline
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic;
[fnECLS outECLS] = bsplinen({PHI, TRI}, [XI Z], {DC, dc}, d, r, bopts);
time1 = toc;


% check whether constraints have been met

mseconstraints = sqrt(mse(DC * fnECLS.coefs));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Calculate performance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%val_recident = Xreg * c_hatrls;

% Unconstrained LS performance
val_ls            = bsplinen_eval(fnLS, XIeval);
err_ls            = Zeval - val_ls;
max_err_ls        = max(abs(err_ls));
RMS_ls            = sqrt(mse(err_ls));
val_ls_VAL_grid   = reshape(val_ls, size(xxe));

% Constrained LS performance
val_ecls          = bsplinen_eval(fnECLS, XIeval);
err_ecls          = Zeval - val_ecls;
max_err_ecls      = max(abs(err_ecls));
RMS_ecls          = sqrt(mse(err_ecls));
val_ecls_VAL_grid = reshape(val_ecls, size(xxe));


fprintf('\n*******************************************************************************************\n');
fprintf('------------------------------- Validation Results --------------------------------------\n');
fprintf('Nominal (d=%d, r=%d) \t\t\t\tRMS = %2.4f, \tmax error = %2.4f, \t(time: %2.3f [s])\n', d, r, RMS_ls, max_err_ls, time0);
fprintf('Diff. Constrained (d=%d, r=%d) \tRMS = %2.4f, \tmax error = %2.4f, \t(time: %2.3f [s])\n', d, r, RMS_ecls, max_err_ecls, time1);
fprintf('MSE of DC*coefs = %d\n', mseconstraints);
fprintf('------------------------------- Validation Results --------------------------------------\n');
fprintf('*******************************************************************************************\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Test Continuity on triangulation boundaries
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%disp('Calculating derivatives...');

% return the edges of the triangulation
[TRIedges, PHIedges, edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);

% calculate normals
Normals = bsplinen_calcEdgeNormals(TRI, PHI, PHIedges);

% calculate and save all spline derivatives up to order d.
FnLSDiffSet = cell(d, 4);
FnECLSDiffSet = cell(d, 4);
for i = 1:d
    % Unconstrained derivatives
    FnLSDiffSet{i, 1} = bsplinen_deriv(fnLS, i, [1 0]);
    FnLSDiffSet{i, 2} = bsplinen_deriv(fnLS, i, [0 1]);
    FnLSDiffSet{i, 3} = bsplinen_eval(FnLSDiffSet{i, 1}, XIeval);
    FnLSDiffSet{i, 4} = bsplinen_eval(FnLSDiffSet{i, 2}, XIeval);
    % Constrained derivatives
    FnECLSDiffSet{i, 1} = bsplinen_deriv(fnECLS, i, [1 0]);
    FnECLSDiffSet{i, 2} = bsplinen_deriv(fnECLS, i, [0 1]);
    FnECLSDiffSet{i, 3} = bsplinen_eval(FnECLSDiffSet{i, 1}, XIeval);
    FnECLSDiffSet{i, 4} = bsplinen_eval(FnECLSDiffSet{i, 2}, XIeval); 
    
    if (i == dBC)
        if (dBC > 0)
            fnLS_dx = bsplinen_deriv(fnLS, dBC, [1 0]);
            fnLS_dy = bsplinen_deriv(fnLS, dBC, [0 1]);
            fnECLS_dx = bsplinen_deriv(fnECLS, dBC, [1 0]);
            fnECLS_dy = bsplinen_deriv(fnECLS, dBC, [0 1]);
        else
            fnLS_dx = bsplinen_deriv(fnLS, d-1, [1 0]);
            fnLS_dy = bsplinen_deriv(fnLS, d-1, [0 1]);
            fnECLS_dx = bsplinen_deriv(fnECLS, d-1, [1 0]);
            fnECLS_dy = bsplinen_deriv(fnECLS, d-1, [0 1]);
        end
    end
end
if (dBC == 0)
    fnLS_dx = bsplinen_deriv(fnLS, d-1, [1 0]);
    fnLS_dy = bsplinen_deriv(fnLS, d-1, [0 1]);
    fnECLS_dx = bsplinen_deriv(fnECLS, d-1, [1 0]);
    fnECLS_dy = bsplinen_deriv(fnECLS, d-1, [0 1]);
end

% calc derivative values of constraint order
val_fnLSdxm = bsplinen_eval(fnLS_dx, XIeval);
val_fnLSdym = bsplinen_eval(fnLS_dy, XIeval);
val_fnECLSdxm = bsplinen_eval(fnECLS_dx, XIeval);
val_fnECLSdym = bsplinen_eval(fnECLS_dy, XIeval);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
close all;

warning('off', 'MATLAB:gui:latexsup:BadTeXString');

az = -147;
el = 26;



%%
plotID = 1001;

figure(plotID);
set(plotID, 'Position', [1 500 800 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
subplot(1, 2, 1);
hold on;
plot(XI(:,1), XI(:,2), 'o', 'MarkerEdgeColor', [.5 .5 .5], 'MarkerFaceColor', [.5 .5 .5], 'MarkerSize', 1);
trisurf(TRI, PHI(:,1), PHI(:,2), ones(size(PHI(:,2))), 'EdgeColor', [0 0 0], 'FaceColor', 'none');
plot3(PHI(:,1), PHI(:,2), ones(size(PHI(:,2))), 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 0], 'MarkerSize', 5);
axis([-.1 1.1 -.15 1.1]);
view(180, 90);
xlabel('x');
ylabel('y');
title(sprintf('Triangulation (T=%d) and dataset (N=%d)', T, length(Z)));

subplot(1, 2, 2);
hold on;
grid on;
surf(xxe, yye, reshape(Zeval, size(xxe)), 'EdgeColor', [0 0 0]); 
surf(xxe, yye, reshape(Zeval, size(xxe)), 'EdgeColor', [0 0 0]); 
deltaz = .03 * range(fnECLS.coefvars);
view(az, el); 
xlabel('x');
ylabel('y');
title('z(x, y)', 'interpreter', 'none');
if (printfigs == 1001)
    fname = sprintf('fig_TRIData_T%d_v1', T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end



%caxisvals = [1.4*min(-Zeval + val_ecrls_VAL) max(-Zeval + val_ecrls_VAL)];


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

plotID = 3100;

val_ls_VAL_grid(val_ls_VAL_grid < -2) = nan;
%caxisvals = [1.4*min(-Zeval + val_ecrls_VAL) max(-Zeval + val_ecrls_VAL)];
figure(plotID);
set(plotID, 'Position', [1 50 800 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
subplot(1, 2, 1);
hold on;
grid on;
surf(xxe, yye, val_ls_VAL_grid, 'EdgeColor', [0 0 0]);
view(az, el); 
xlabel('x');
ylabel('y');
title(sprintf('Unconstrained Spline (d=%d, r=%d, T=%d)', d, r, T));

subplot(1, 2, 2);
hold on;
grid on;
surf(xxe, yye, val_ecls_VAL_grid, 'EdgeColor', [0 0 0]);
view(az, el);
xlabel('x');
ylabel('y');
title(sprintf('Cauchy Constrained Spline (d=%d, r=%d, T=%d, dBC=%d)', d, r, T, dBC));
if (printfigs == 3100)
    fname = sprintf('fig_DConstraints_Demo_d%dr%dm%d_T%d', d, r, dBC, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
if (dBC > 0)

%     if (plotderivatives)
        plotxcount = ceil(sqrt(d));
        plotycount = ceil(d/plotxcount);
        totplots = plotxcount * plotycount;
        
        plotID = 4001;
        figure(plotID);
        set(plotID, 'Position', [0 50 600 800], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
        for i = 1:totplots
            subplot(plotxcount, plotycount, i);
            grid on; hold on;
            if (i > d)
                continue;
            end
            if (length(FnLSDiffSet{i, 3}) > 1)
                surf(xxe, yye, reshape(FnLSDiffSet{i, 3}, size(xxe)));
            end
            view(az, el);
            xlabel('x_0'); ylabel('x_1');
            title(sprintf('LS %s^{%d} f / (%s x)^{%d}', '\delta', i, 'delta', i));
        end

        plotID = 4002;
        figure(plotID);
        set(plotID, 'Position', [0 200 600 800], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
        for i = 1:totplots
            subplot(plotxcount, plotycount, i);
            grid on; hold on;
            if (i > d)
                continue;
            end
            if (length(FnECLSDiffSet{i, 4}) > 1)
                surf(xxe, yye, reshape(FnLSDiffSet{i, 4}, size(xxe)));
            end
            view(az, el);
            xlabel('x'); ylabel('y');
            title(sprintf('LS %s^{%d} s_1 / (%s y)^{%d}', '\delta', i, 'delta', i));
        end

        
        
        plotID = 5001;
        figure(plotID);
        set(plotID, 'Position', [600 50 600 800], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
        for i = 1:totplots
            subplot(plotxcount, plotycount, i);
            grid on; hold on;
            if (i > d)
                continue;
            end
            if (length(FnECLSDiffSet{i, 3}) > 1)
                surf(xxe, yye, reshape(FnECLSDiffSet{i, 3}, size(xxe)));
            end
            view(az, el);
            xlabel('x_0'); ylabel('x_1');
            title(sprintf('ECLS %s^{%d} f / (%s x)^{%d}', '\delta', i, 'delta', i));
        end

        plotID = 5002;
        figure(plotID);
        set(plotID, 'Position', [600 200 600 800], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
        for i = 1:totplots
            subplot(plotxcount, plotycount, i);
            grid on; hold on;
            if (i > d)
                continue;
            end
            if (length(FnECLSDiffSet{i, 4}) > 1)
                surf(xxe, yye, reshape(FnECLSDiffSet{i, 4}, size(xxe)));
            end
            view(az, el);
            xlabel('x'); ylabel('y');
            title(sprintf('ECLS %s^{%d} s_1 / (%s y)^{%d}', '\delta', i, 'delta', i));
        end

    



%%
end


