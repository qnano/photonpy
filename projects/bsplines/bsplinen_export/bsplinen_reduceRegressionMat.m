% BSPLINEN_REDUCEREGRESSIONMAT eliminates constrained B-coefficients from given regression matrix 
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
% 
%   [XRED BCOEFNEWIDX] = BSPLINEN_REDUCEREGRESSIONMAT(X, HK, D, R) 
%       Eliminates constrained B-coefficients from the columns of matrix X based on the constraint
%       relations in the matrix HK. For R = 0 the reduced regression matrix is sparse, while for R > 0
%       XRED tends to be non-sparse.
%       
%   Input to BSPLINEN_REDUCEREGRESSIONMAT:
%       
%       X = the [N x (T * dhat)] regression matrix, with N the number of observations, T the total number of
%           simplices, and dhat the number of Bernstein basis functions per simplex.
%       HK = the [C x (T * dhat)] constraint matrix, with C the number of constraint equations, T the total number of
%           simplices, and dhat the number of Bernstein basis functions per simplex. WARNING: HK should
%           be of full rank for the sparse reduction mode (R = 0). For R > 1 a nullspace method is used
%           which is not required to be of full rank.
%       D = the degree of the B-form basis functions.
%       R = the order of continuity of the spline. For R = 0 a sparse reduced regression matrix is 
%           produced, while for R > 0 a nullspace method is used to reduce X into a non sparse matrix.
%
%   Output from BSPLINEN_REDUCEREGRESSIONMAT:
%
%       XRED = the [N x ( (T-M) * dhat )] regression matrix in which M columns have been removed.
%       IRED2FULL = [T*dhat x 1] vector of indices from the reduced size B-coefficient vector to the
%           full B-coefficient vector.
%       IFULL2RED = [T*dhat x 1] vector of indices from the full size B-coefficient vector to the
%           reduced size B-coefficient vector.
%       NULLHK = [M x T*dhat] matrix wich is a basis for the nullspace of H
%
%   Use as follows for the LS case and direct elimination: 
%   [Xred Ired2full Ifull2red] = bsplinen_reduceRegressionMat(X, HK, d, r);
%   Bcoefsred = inv(Xred' * Xred) * Xred' * Y;
%   Bcoefs(Ifull2red) = Bcoefsred(Ired2full);
%  
%   Use as follows for the LS case and null-space projection
%   [Xred Ired2full Ifull2red nullHK] = bsplinen_reduceRegressionMat(X, HK, d, r);
%   Bcoefsred = inv(Xred' * Xred) * Xred' * Y;
%   Bcoefs = nullHK * Bcoefsred;
%
function [Xred Ired2full Ifull2red NullHK] = bsplinen_reduceRegressionMat(X, HK, d, r)

    Xred        = X;
    % index array which translates the new B-coefficient array to the original array.
    BcoefnewIdx = [];
    Ired2full   = [];
    Ifull2red   = [];
    NullHK      = [];
    
    
    sparsemode = 1;
    if (r > 0)
        sparsemode = 0;
    end
    
    Tdhat = size(HK, 2);

    % The sparse reduction mode is (at this point) only available for R = 0, and for a full rank HK
    % matrix.
    if (sparsemode)

        % count the number of hard constraints
        testH = sum(HK, 2);
        Ifixed = find(testH);
        Nfixed = length(Ifixed);

        [rIfixed cIfixed] = find(HK(Ifixed, :));
        % get indices of fixed B-coefficients, which can be eliminated immediately
        cmatfixed = cIfixed;

        % calc number of constraints
        Nconstraints = size(HK, 1) - Nfixed;
        % extract the continuity constraints from the constraint matrix
        Hr = HK(1:Nconstraints, :);

        % find indices of related B-coefficients 
        [ridx cidx] = find(Hr == -1);
        [ridx2 cidx2] = find(Hr == 1); % no longer contains fixed B-coefs!

        % technology progresses, politics evolve

        % sort constrained B-coefficient indices
        cmat1 = sortrows([ridx cidx]);
        cmat2 = sortrows([ridx2 cidx2]);

        % construct the initial translation matrix
        transmat0 = [cmat1(:,2) cmat2(cmat1(:,1), 2)];
        % transmat is the ALL-IMPORTANT translation matrix. 
        % Note that the indices of the to-be-removed columns from Xred are placed in transmat(:,2) 
        transmat = sortrows(transmat0, 1);

        % Perform column swapping and index substitution to get the minimal set of B-coefficients
        for j = 1:size(transmat, 1)
            % the set of 2 coefficients to be tested
            testcoefs = transmat(j, :);
            % check which b-coefficient has the highest index
            if (testcoefs(1) < testcoefs(2))
                % now find all locations in transmat which are equal to testcoefs(2)
                [ridx cidx] = find(transmat == testcoefs(2));
                for k = 1:length(ridx)
                    % substitute all places in transmat(ridx, cidx) to testcoefs(1)
                    if (ridx(k) ~= j)
                        transmat(ridx(k), cidx(k)) = testcoefs(1);
                    end
                end
            else
                % find all locations in transmat which are equal to testcoefs(2)
                [ridx cidx] = find(transmat == testcoefs(1));
                for k = 1:length(ridx)
                    % substitute all places in transmat(ridx, cidx) to testcoefs(2)
                    if (ridx(k) ~= j)
                        transmat(ridx(k), cidx(k)) = testcoefs(2);
                    end
                end
            end

        end
        transmat = sort(transmat, 2);


        % eliminate columns from the regression matrix
        for i = 1:size(transmat, 1)
            Xred(:, transmat(i, 1)) = Xred(:, transmat(i, 1)) + Xred(:, transmat(i, 2));
        end

        % create the index list of to-be-removed columns
        [Iremove Iunique] = unique([transmat(:,2); cmatfixed]);

        % now we can remove columns from X 
        Xred(:, Iremove) = [];


        % Construct full translation matrix
        Ikeep = setdiff((1:Tdhat)', Iremove); % the indices of the remaining B-coefs in the GLOBAL B-coefficient vector
        transreduced = [(1:length(Ikeep))', Ikeep]; % the translation mat from GLOBAL to REDUCED B-coefficient vector

        % The translation between constrained B-coefs in the GLOBAL vector to the REDUCED vector
        % Note that transmat(:,1) can still contain indices of B-coefficients that have no translation to 
        % Ikeep as they are removed by hard constraints.
        idxtrans = zeros(size(transmat, 1), 1); 
        remtransmatidx = zeros(size(transmat, 1), 1); 
        count = 0;
        count2 = 0;
        for i = 1:size(transmat, 1)
            idx = find(transreduced(:,2) == transmat(i,1));
            if (~isempty(idx))
                count = count + 1;
                idxtrans(count) = idx;
            else
                count2 = count2 + 1;
                remtransmatidx(count2) = i;
            end
        end
        idxtrans = idxtrans(1:count);
        remtransmatidx = remtransmatidx(1:count2);
        % remove any translations from transmat that are associated with hard constrained B-coefs
        transmat(remtransmatidx, :) = [];


        % the full translation matrix from REDUCED to GLOBAL vector
        fulltransmat = [[Ikeep; transmat(:,2)] [transreduced(:,1); idxtrans]];

        %%
        % use as follows: Bcoefs(Ifull2red) = Bcoefsred(Ired2full);
        Ifull2red = fulltransmat(:,1);
        Ired2full = fulltransmat(:,2);   
        
        % construct the sparse nullspace matrix
        NullHK = sparse(size(HK, 2), max(Ired2full));  
        NullHK(sub2ind(size(NullHK), Ifull2red, Ired2full)) = 1; 
              

    else
        % find the sparse kernel of HK
%         NullHK = null(full(HK));
        NullHK = bsplinen_sparse_null(HK);
        % the reduced regression matrix is simply X * ker(HK)
        Xred    = X * NullHK;
        
        % reduced B-coefficients can be estimated as follows: 
        % Bcoefs_reduced = inv(Xred'*Xred) * Xred' * Y, while the complete vector
        % of B-coefficients are reconstructed as follows: Bcoefs = nullHK * Bcoefs_reduced 

    end
    
    
    
    