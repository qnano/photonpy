   
close all

% rng('default');

n = 2; % dimension
d = 4; % polynomial degree

griddedTRI    = 0;
TRIgridcount  = [4 4];
TRItype       = 2;
vertex_count  = 20; % number of vertices used in triangulation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Construct random triangulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~griddedTRI)
    PHI = rand(vertex_count, 2); % generate vertex array
    TRI = delaunayn(PHI); % create triangulation
    TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)
else
    XItmp = [0 0; 0 1; 1 0; 1 1];
    uniquevertices = 2;
    [TRI, PHI] = bsplinen_CDVtriangulate2(XItmp, TRIgridcount, TRItype, uniquevertices);
end 

% the number of simplices
T = size(TRI, 1);


% return the edges of the triangulation
[TRIfacets, PHIfacets, edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);
   
EdgeCoefs = bsplinen_getBCoefsOnFacets(TRI, PHI, TRIfacets, d);    
    
%%
plotID = 1003;
figure(plotID);
set(plotID, 'Position', [0 200 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
axis equal
trimesh(TRI, PHI(:,1), PHI(:,2), 'Color', 'k');
for i = 1:size(PHIfacets, 1)
    if (~isempty(PHIfacets{i}))
        edge = PHIfacets{i};
        line(edge(:,1), edge(:,2), 'Color', [.5 .5 .5], 'linewidth', 3);
    end
end
for i = 1:length(EdgeCoefs)
    EC = EdgeCoefs{i};
    for k = 1:length(EC)
        xybcoefs = EC{k};
        plot(xybcoefs(:,3), xybcoefs(:,4), 'd', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 0 1],'MarkerSize', 8);
    end
end
plot(PHI(:,1), PHI(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
xlabel('x');
ylabel('y');


    
    
    
    