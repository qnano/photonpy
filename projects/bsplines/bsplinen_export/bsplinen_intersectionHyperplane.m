% BSPLINEN_INTERSECTIONHYPERPLANE calculates the intersection between a line and a hyperplane
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%                          version 1.1
%
%   Input to BSPLINEN_INTERSECTIONHYPERPLANE is
%
%       Xplane: a [n x n] matrix holding the n points that specify an n-plane. The columns of Xplane form
%           the coordinate components, the rows the different points.
%       Xline: a [2 x n] matrix holding the 2 vectors that specify a line in n-space. The columns of Xline form
%           the coordinate components, the rows the vectors.
%
%   Output from BSPLINEN_INTERSECTIONHYPERPLANE is
%
%       Xint: a [1 x n] vector holding the intersection point between Xplane and Xline.
%
function Xint = bsplinen_intersectionHyperplane(Xplane, Xline)

    % determine dimension
    n = size(Xplane, 2);
    
    % the distance vector
    DXline = Xline(2, :) - Xline(1, :);
    
    % determine matrices A and B containing the linear hyperplane and line equations
    A = [ones(1, n+1); [Xplane' Xline(1, :)']];
    B = [ones(1, n) 0; [Xplane' DXline']];

    % this function is based on the 3-dimensional example in http://mathworld.wolfram.com/Line-PlaneIntersection.html
    scaleT = - det(A) / det(B);
    
    % determine the coordinate Xint where the line intersects the hyperplane using the scale factor scaleT.
    Xint = Xline(1,:) + DXline * scaleT;