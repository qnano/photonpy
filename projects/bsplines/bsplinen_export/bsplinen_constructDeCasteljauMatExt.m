% bsplinen_constructDeCasteljauMatExt constructs the one step de Casteljau matrix that reduces
%   the B-coefficients of a given degree to degree minus one. bsplinen_constructDeCasteljauMatExt 
%   does not have any other depedendies with the simplex spline toolbox.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.0
%
%   Syntax:
%       Castel = bsplinen_constructDeCasteljauMatExt(barycoord, degree)
%
%   Description:
%
%       Input parameters:
%           - barycoord : double [1 x n+1]; the directional coordinate of a cartesian vector
%           - degree    : double [1 x 1]; the degree of the de Casteljau matrix
%
%       Output parameters:
%           - Castel    : de Casteljau matrix for the given degree and directional coordinate
%
%
function Castel = bsplinen_constructDeCasteljauMatExt(barycoord, degree)

   
    % dimension + 1
    np1 = size(barycoord, 2);
    % dimension
    n = np1 - 1;
    % polynomial degree
    d = degree;
    
    if (d < 1)
        Castel = 0;
        return;
    elseif (d == 1)
        Castel = barycoord;
        return;
    end
    
    % construct the d-1 basis 
    basis = bsplinen_constructBasis(n, d-1);
    % the total number of B-coefs of the (d-1)-th basis 
    dhatdm1 = size(basis, 1);
    % the total number of B-coefs of the d-th basis 
    dhatd = bsplinen_calcdHat(n, d);
    

    % Construct the de Casteljau duals matrix
    [duals dualsindices] = bsplinen_deCasteljauDuals(basis, d);
    
    % Fill the de Casteljau matrix one row at a time using the de Casteljau
    % duals. The structure is as in [Hu et al., 2007].
    Castel = sparse(dhatdm1, dhatd);
    for i = 1:dhatdm1
        rowidx = dualsindices((i-1)*np1+1:i*np1, :);
        Castel(i, rowidx(:, 2)) = barycoord;
    end
    
    

function cval = bsplinen_calcIndexTotalValue(c_index)

    r = length(c_index);
    power = r - 1;
    
    cval = 0;
    for j = 1:r
        cval = cval + c_index(j) * 10^power;
        power = power - 1;
    end

    
    
function [IndexC IndexLocal] = bsplinen_getcindex(polybasis_size, CIndexArr, simplex_index, c_local_indices)

    clocal_size = length(c_local_indices(:, 1));
    
    if (clocal_size == 1)
        ctotval = bsplinen_calcIndexTotalValue(c_local_indices);

        IndexLocal = CIndexArr(ctotval);

        IndexC = (simplex_index - 1) * polybasis_size + IndexLocal;
    else
        
        IndexC = zeros(clocal_size, 1);
        IndexLocal = zeros(clocal_size, 1);
        for cind = 1:clocal_size
            clocal = c_local_indices(cind, :);
            ctotval = bsplinen_calcIndexTotalValue(clocal);

            IndexLocal(cind) = CIndexArr(ctotval);

            IndexC(cind) = (simplex_index - 1) * polybasis_size + IndexLocal(cind);
        end
    end
    
    
 function CIndexArr = bsplinen_constructCIndexArray(polybasis)

    dhat = length(polybasis(:, 1));
    
    CIndexArr = sparse(dhat, 1);
    
    for i = 1:dhat
    
        ctotval = bsplinen_calcIndexTotalValue(polybasis(i, :));
        CIndexArr(ctotval) = i;
    
    end
    
        
    
function dHat = bsplinen_calcdHat(n, d)

    dHat = factorial(n + d) ./ (factorial(n) * factorial(d));
   
    

function PolyBasis = bsplinen_constructBasis(n, d)

    
    % the polynomial orders 0 and 1 require no calculations
    if (d == 0)
        PolyBasis = zeros(1, n+1);
        return;
    elseif (d == 1)
        PolyBasis = eye(n+1, n+1);
        return;
    end
    
    % gamma is the increment basis, which is always an identity matrix of
    % size [n+1] x [n+1].
    gamma = eye(n+1, n+1);
    basis0 = gamma;
    
    % now construct the basis from the ground up by sequentially adding the
    % the rows of gamma to each row of basis0...
    for degree = 2:d
        count = 1;
        % allocate the minimum amount of space needed for the basis
        % function indices.
        basis = zeros(length(basis0(:, 1))*(n+1), n+1);
        % go through every basis row
        for k = 1:length(basis0(:, 1))
            % go through every gamma row -1
            for r = 1:(n+1)
                % the newly proposed B-coefficient index
                cindex = basis0(k, :) + gamma(r, :);
                % NOTE: basis will contain lots of duplicates which will be removed later.
                basis(count, :) = cindex;
                count = count + 1;
                 
            end
        end

        % Filter basis0 for duplicates, because otherwise the size of the
        % basis would explode.
        
        % sort the basis in descending order
        basis = flipud(sortrows(basis));
        oldind = -1 * ones(1, n+1);
        count = 1;
        basis0 = zeros(bsplinen_calcdHat(n, degree), n+1);
        for i = 1:length(basis(:, 1))
            cind = basis(i, :);
            found = 0;
            % Compare the new index 'cind' with the previous 'oldind'.
            for j = 1:length(cind)
                if (cind(j) == oldind(j))
                    found = found + 1;
                end
            end
            % put the first found value in the basis0 matrix
            if (found < n+1)
                basis0(count, :) = cind;
                count = count + 1;
                oldind = cind;
            end
        end
        
    end
    
    PolyBasis = basis0;
    
    
    
function [duals dualindices] = bsplinen_deCasteljauDuals(polybasis, maxdegree)

    dualsold    = polybasis;
    duals       = [];
    d           = sum(dualsold(1,:), 2); % degree
    deltad      = maxdegree - d; % degree change
    
    Np1         = size(dualsold, 2); % dimension + 1    
    
    
    for j = 1:deltad
        
        % temporary duals matrix
        duals = zeros(size(dualsold, 1) * Np1, (j+1) * Np1);
        
        for i = 1:size(dualsold,1);
            rowidx = (i-1)*(Np1)+1:i*Np1;
            % update all columns to the left of the new column
            for k = 1:j-1
                duals(rowidx, (k-1)*Np1+1:k*Np1) = ones(Np1, Np1)*diag(dualsold(i, (k-1)*Np1+1:k*Np1));
            end
            duals(rowidx, (j-1)*Np1+1:j*Np1) = ones(Np1, Np1)*diag(dualsold(i, (j-1)*Np1+1:end));
            
            % this is the new column in duals
            duals(rowidx, j*Np1+1:end) = ones(Np1, Np1)*diag(dualsold(i, (j-1)*Np1+1:end)) + eye(Np1);
            
        end
        
        % reset old duals matrix
        dualsold = duals;

    end

    dualindices = [];
    
    % if requested, the global indices for every row in DUALS will be found
    if (nargout > 1)
        dualindices = zeros(size(duals, 1), (deltad+1));
        % go through all columns and find the appropriate indices in
        % a complete polybasis of the correct degree.
        for j = 1:deltad+1
            % use the CIndexArray to speed up index searching (=hashtable like datastructure)
            pbasis = bsplinen_constructBasis(Np1-1, d+j-1);
            CIndexArr = bsplinen_constructCIndexArray(pbasis);
            kappaold = nan(1, Np1);
            cindex = nan;
            for i = 1:size(duals, 1)
                kappa = duals(i, (j-1)*Np1+1:j*Np1);
                if (all(kappa == kappaold))
                    dualindices(i, j) = cindex;
                else
                    cindex = bsplinen_getcindex(size(pbasis,1), CIndexArr, 1, kappa);
                    dualindices(i, j) = cindex;
                    kappaold = kappa;
                end                
                
            end
        end
    end  
    
    
    

    
    