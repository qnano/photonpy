% demo_calcTriangulationEdgeNormals shows how to calculate the normal vectors to the edge of general triangulations
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%   Normals = bsplinen_getSimplexNormals(Edges, PHIedges)
%
close all

rng('default');

n = 2; % dimension

griddedTRI    = 0;
TRIgridcount  = [4 4];
TRItype       = 1;
vertex_count  = 30; % number of vertices used in triangulation
doOutsideNormals = 1; % -1: no normals direction finding (more efficient), 0: all normals point inward, 1: all normals point outwards
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Construct random triangulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~griddedTRI)
    PHI = rand(vertex_count, 2); % generate vertex array
    TRI = delaunayn(PHI); % create triangulation
    TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)
else
    XItmp = [0 0; 0 1; 1 0; 1 1];
    uniquevertices = 2;
    [TRI, PHI] = bsplinen_CDVtriangulate2(XItmp, TRIgridcount, TRItype, uniquevertices);
end 

% the number of simplices
T = size(TRI, 1);


% return the edges of the triangulation
[TRIedges, PHIedges, edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);

% calculate normals
Normals = bsplinen_calcEdgeNormals(TRI, PHI, PHIedges, doOutsideNormals);


%%
plotID = 1003;
figure(plotID);
set(plotID, 'Position', [0 200 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
axis equal
trimesh(TRI, PHI(:,1), PHI(:,2), 'Color', 'k');
for i = 1:size(PHIedges, 1)
    if (~isempty(PHIedges{i}))
        edge = PHIedges{i};
        line(edge(:,1), edge(:,2), 'Color', [.5 .5 .5], 'linewidth', 3);
        
        normvectors = Normals{i};
        for j = 1:size(normvectors,1)
           nv = .1*normvectors(j, :);
           midedge = mean(edge(2*(j-1)+1:1:(j*2), :));
           line([midedge(1) midedge(1)+nv(1)], [midedge(2) midedge(2)+nv(2)], 'color', 'r');
        end
    end
    
end
plot(PHI(:,1), PHI(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
xlabel('x');
ylabel('y');

    