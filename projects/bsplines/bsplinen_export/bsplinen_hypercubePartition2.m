% BSPLINEN_HYPERCUBEPARTITION2 Partitions a triangulation into a set of sub-triangulations using
%   a hypercube partitioning scheme.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          version 1.1
%
%             HypercubeData{i, 1}: partition boundary coordinates;
%             HypercubeData{i, 2}: partition simplex indices;
%             HypercubeData{i, 3}: TRIsub, the subtriangulation of TRI.
%             HypercubeData{i, 4}: simplex indices of STAR simplices in neighboring partitions
%             HypercubeData{i, 5}: TRIstarsub, the subtriangulation of TRI containing the STAR simplices
%             HypercubeData{i, 6}: LOCAL Simplex indices in HypercubeData{i, 3}; All simplices in the STAR simplex set that share an edge with Partition i.
%             HypercubeData{i, 7}: The vertex indices of all edge vertices of Partition i.
%             HypercubeData{i, 8}: Linear Hypercube indices and hypercube coordinate indices ([row, col] etc) of CURRENT partition
%                   HypercubeData{i, 8}(1): the linear hypercube index, HypercubeData{i, 8}(2:end) The  hypercube coordinate indices of the hypercube i.
%             HypercubeData{i, 9}: Linear Hypercube indices and hypercube coordinate indices ([row, col] etc) of NEIGHBORING partitions
%                   HypercubeData{i, 9}(1): the linear hypercube index, HypercubeData{i, 9}(2:end) The  hypercube coordinate indices of the neighbors 
%                   of hypercube i.

function HypercubeData = bsplinen_hypercubePartition2(TRI, PHI, InputSet, Edges, X, Y)
    
    HypercubeData = [];
    doDataSearch = 0;

    if (nargin < 2)
        return;
    elseif (nargin < 4)
        Edges = {};
    elseif (nargin >= 6)
        if (~isempty(X) && ~isempty(Y))
            doDataSearch = 1;
        end
    end
    
    % check whether InputSet is a bsplinen_options struct or a simple array holding the gridres
    if (isstruct(InputSet))
        bopts = InputSet;
        GridCount = bopts.tri_partitioncount;
    else
        GridCount = InputSet;
        % load default options structure
        bsplinen_structures;
        bopts = struct_bsplinen_options;
    end
    
    T = size(TRI, 1);
    n = size(PHI, 2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the hypercube counter

    % knotseqtype 0: the user supplied the knot sequence
    % knotseqtype 1: the user supplied the number of cells per dimension
    knotseqtype = 0;
    if (~iscell(GridCount))
        knotseqtype = 1;
        hypercube_count = zeros(n, 1);
        if (length(GridCount) == 1)
        	hypercube_count(:) = GridCount;
        else 
            hypercube_count = GridCount;
        end
    else
        hypercube_count = zeros(n, 1);
        for i = 1:n
            hypercube_count(i) = length(GridCount{i}) - 1;
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the analysis grid

    % get the min and max bounds
    Extremes = zeros(n, 2);
    HCGrid = cell(n, 1);
    for i = 1:n
        Extremes(i, 1) = min(PHI(:, i));
        Extremes(i, 2) = max(PHI(:, i));
    end

    % create the initial vertex set
    KnotSet = cell(n, 1); % the set of knots containing the hypercube coordinates
    HCIdxSet = cell(n, 1); % set of knots used to create the index array of hypercube indices
    HCSizeSet = cell(n, 1); % contains, per dimension, the size of the hypercubes
    
    KnotSetUnity = cell(n, 1);
    HCUnityGridSet = cell(n, 1);
    
    % go through all dimensions and construct knot sets for the hypercube grid
    for i = 1:n
        hcsize = (Extremes(i, 2)-Extremes(i, 1)) / hypercube_count(i);
        if (knotseqtype == 0)
            KnotSet{i} = GridCount{i};
        elseif (knotseqtype == 1)
            KnotSet{i} = Extremes(i, 1):hcsize:Extremes(i, 2);
        end
        HCIdxSet{i} = 1:hypercube_count(i);
        HCSizeSet{i} = KnotSet{i}(2:end) - KnotSet{i}(1:end-1);
        KnotSetUnity{i} = [0; 1];
    end
    
    % build the unity hypercube triangulation
    [HCUnityGridSet{:}] = ndgrid(KnotSetUnity{:});

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create the hypercube grid
    CubeCoords = zeros(length(HCUnityGridSet{1}(:)), n);
    for i = 1:n
        CubeCoords(:, i) = HCUnityGridSet{i}(:);
    end
    % sort rows of vertices and vertex indices
    CubeCoords = sortrows(CubeCoords);    

    % Create the hypercube indexer
    HCGridIdxSet = cell(n, 1);
    [HCGridIdxSet{:}] = ndgrid(HCIdxSet{:});
    HCCount = numel(HCGridIdxSet{1});
    HCIndexer = zeros(HCCount, n);
    for i = 1:n
        HCIndexer(:, i) = HCGridIdxSet{i}(:);
    end
    Ncubes = size(HCIndexer, 1);
    ImaxHC = max(HCIndexer);
    
    % Go through all hypercubes, determine data content and analyze data range
    HypercubeData = cell(Ncubes, 9);
    TRImembershipcount = zeros(size(TRI, 1), 2);
    NeighborCubeSet = cell(Ncubes, 2); % list of neighboring cubes
    
    Nprog = 1;
    for i = 1:Ncubes
                
        % index of the i-th hypercube
        hcidx = HCIndexer(i, :);

        % calc hypercube direct neigbors
        Ineighborcubes = [ones(n) * diag(hcidx) - eye(n); ones(n) * diag(hcidx) + eye(n)];
        % remove invalid neighbors (indices < 0)
        validcount = 0;
        Ivalid = zeros(size(Ineighborcubes, 1), 1);
        for j = 1:size(Ineighborcubes, 1)
            if (all(Ineighborcubes(j, :) > 0) && all(Ineighborcubes(j, :) <= ImaxHC))
                validcount = validcount + 1;
                Ivalid(validcount) = j;
            end
        end
        % determine neighbor linear indices
        NSet = Ineighborcubes(Ivalid(1:validcount), :); 
        ILinNSet = zeros(size(NSet, 1), 1);
        for j = 1:size(NSet, 1)
            for k = 1:size(HCIndexer)
                if (~any(NSet(j,:) - HCIndexer(k, :)))
                    ILinNSet(j) = k;
                    break;
                end
            end
        end
        NeighborCubeSet{i, 1} = [ILinNSet NSet];
        
        % calculate hypercube coordinates
        CubeCoordsScaled = CubeCoords;
        for j = 1:n
            % calc hypercube coords
            CubeCoordsScaled(:, j) = CubeCoords(:, j) * HCSizeSet{j}(hcidx(j)) + KnotSet{j}(hcidx(j));
        end  
        HypercubeData{i, 1} = CubeCoordsScaled;
        %HypercubeData{i, 2} = [];

        % find all simplices that are located within CubeCoordsScaled
        tcount = 0;
        for t = 1:size(TRI, 1)
    
            % check whether simplex t has already been assigned!
            if (TRImembershipcount(t, 2) >= n+1)%(n+1)/2)
                % more than half of the vertices of t are already assigned!
                % tcount = tcount + 1;
                continue;
            end
                
            simp = PHI(TRI(t, :), :);
            incube = 1;
            % go through all dimensions
%             t1 = all(ones(size(simp))*(diag(CubeCoordsScaled(end, :) + 1e-6)) > simp, 2)
%             t2 = all(ones(size(simp))*(diag(CubeCoordsScaled(1, :) - 1e-6)) < simp, 2)
            vtest = all( [ones(size(simp))*(diag(CubeCoordsScaled(end, :) + 1e-6)) > simp, ones(size(simp))*(diag(CubeCoordsScaled(1, :) - 1e-6)) < simp], 2);
%             for j = 1:n % faster than matrix op!
%                 t1 = (CubeCoordsScaled(end, j) + 1e-6) > simp(:, j);
%                 t2 = (CubeCoordsScaled(1, j) - 1e-6) < simp(:, j);
%                 if (~all( (CubeCoordsScaled(end, j) + 1e-6) > simp(:, j) )) % check upper bound of HC
%                     % some of the n-th component of the simplex vertices outside max bounds of the hypercube
%                     incube = 0;
%                     break;
%                 end
%                 if (~all( (CubeCoordsScaled(1, j) - 1e-6) < simp(:, j) )) % check lower bound of HC
%                     % some of the n-th component of the simplex vertices outside min bounds of the hypercube
%                     incube = 0;
%                     break;
%                 end
%             end
            if (~any(vtest))
                continue;
            end
            % put simplex t in the hypercube that contains most of its vertices
            mvcount = sum(vtest);
            if (mvcount > TRImembershipcount(t, 2))
                TRImembershipcount(t, 1) = i;
                TRImembershipcount(t, 2) = mvcount;
            end
%             if (~all(vtest))
%                 incube = 0;
%             end
            % if the complete simplex is inside the hypercube, add its global indices to the
            % member simplices of this hypercube
%             if (incube)
%                 tcount = tcount + 1;
%                 HypercubeData{i, 2}(tcount) = t;
%             end
        end
        
    end    
        
    
    % list of to be removed (empty) hypercube partitions
    IremoveHypercubes = [];
    
    for i = 1:Ncubes
        hcidx = HCIndexer(i, :);
        
        HypercubeData{i, 2} = find(TRImembershipcount(:, 1) == i);
        % set the GLOBAL simplex indices
        %Ihypercubesimp = 
        %HypercubeData{i, 2} = HypercubeData{i, 2}(1:tcount);
        if (isempty(HypercubeData{i, 2}))
            IremoveHypercubes = [IremoveHypercubes; i];
            continue;
        end
        
        tripart = TRI(HypercubeData{i, 2}, :);
        % the triangulation partition in GLOBAL vertex indices
        HypercubeData{i, 3} = tripart;
        
        % add simplex star to the hypercube data
        if (bopts.distrib_starlevel > 0)
            % get triangulation edges
            [tmp tmp edgeSimps edgeVerts Edges] = bsplinen_getFullTriangulationEdges(tripart, PHI, []);
            
            IedgeSimps = find(edgeSimps);
            if (isempty(IedgeSimps))
                continue;
            end
            
            % get star structure for this hypercube
            StarStruct = bsplinen_getStarStruct(TRI, bopts.distrib_starlevel, 1, HypercubeData{i, 2}(IedgeSimps));
            Istarsimp = setdiff(StarStruct, HypercubeData{i, 2});
            
            % find hypercube neighbors, and determine the Level (L0 is master)
            
            
            % save data
            HypercubeData{i, 4} = Istarsimp;
            HypercubeData{i, 5} = TRI(Istarsimp, :);
            HypercubeData{i, 6} = IedgeSimps;
            HypercubeData{i, 7} = edgeVerts;
            HypercubeData{i, 8} = [i hcidx];
            HypercubeData{i, 9} = NeighborCubeSet{i};
        end
        
    end

    if (~isempty(IremoveHypercubes))
        HypercubeData(IremoveHypercubes, :) = [];
    end
    
    
    
    