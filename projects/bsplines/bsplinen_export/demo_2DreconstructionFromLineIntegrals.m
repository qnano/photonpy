
close all;

bsplinen_structures;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
d = 5; % degree of approximating polyomials (both in 2D as in 1D).

[polybasis, basisKt] = bsplinen_constructBasis(2, d);
dhat2D = size(polybasis, 1);

[polybasis1D, basisKt1D] = bsplinen_constructBasis(1, d);
dhat1D = size(polybasis1D, 1);


PHI = [-.5 -.5; .9 -.5; -.1 .75;]; % choose these freely (as long as the vertices are not degenerate)!
TRI = [1 2 3]; % always holds for 1 simplex
Nlines = dhat2D; % we need as many line integrals as dhat2D!
randLineOffsetAngle = .05; % random offset of lines (default = 0.05), if set to zero linear dependencies in the line coordinates cause singular reconstruction matrix
measurementNoiseMag = 0; % measurement noise (default = 0); note that method is extremely sensitive to noise...

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Init algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simplex2D = PHI(TRI(1, :), :);
simplex1D = [0; 1];

datares = 50; % resolution used just for estimating the B-coefficients (not part of the reconstruction algorithm.

% define 2D spline
spline2D = struct_bsplinen;
spline2D.PHI = PHI;
spline2D.TRI = TRI;
spline2D.dim = 2;
spline2D.degree = d;
% define general 1D spline
spline1D = struct_bsplinen;
spline1D.PHI = [0; 1];
spline1D.TRI = [1 2];
spline1D.dim = 1;
spline1D.degree = d;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate dataset, just for initializing 2D b-coefs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% data grid
[xxd yyd] = ndgrid(-1:2/(datares-1):1, -1:2/(datares-1):1);
XI = [xxd(:) yyd(:)];
XI = [XI; PHI];

% filter out all datapoints outside of simplex2D
[Imap bxn] = tsearchn(PHI, TRI, XI);
delidx = find(isnan(Imap));
bxn(delidx,:) = [];
XI(delidx,:) = [];
x = XI;

% generate random data in the simplex2D
% bx = [rand(Ndata, 3); eye(3)]; % rand data with 3 vertex locations added
% bxn = bx./(sum(bx,2)*ones(1,size(bx,2))); % normalized random barycentric coordinates
% x = bsplinen_bary2cart(PHI(TRI(1,:), :), bxn);
Ndata = size(x, 1);

% generate some data values
y = 10*x(:,1).^2.*sin(10*x(:,2));

% Computation of regressor matrix Btk
Btk = zeros(Ndata, dhat2D);
for i = 1:dhat2D
    Btk(:,i) = basisKt(i)*prod(bxn.^(ones(Ndata,1)*polybasis(i,:)),2);
end
% Estimate B-coefs
spline2D.coefs = inv(Btk'*Btk)*Btk'*y;

Bcoords = bsplinen_calcBCoefCoordinates(spline2D);

TRIeval = delaunayn(x);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Generate family of lines intersecting the simplex
%       Note that it is extremely important to have random offsets in the lines, not doing so
%       will cause singularities in the reconstruction matrix but only if d > 2!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Xlines = cell(Nlines,1);
% set up array of lines in the [-1 1 -1 1] interval (centered at (0,0)
angledelta = 2*pi / (2*Nlines);
randoffset = randLineOffsetAngle * 2*pi * rand(Nlines, 1);
for i = 1:Nlines
    x1 = cos((i-1)*angledelta + randoffset(i));
    x2 = cos((i-1)*angledelta + pi);
    y1 = sin((i-1)*angledelta + randoffset(i));
    y2 = sin((i-1)*angledelta + pi);
    Xlines{i} = round([[x1;x2] [y1;y2]], 3);
end


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Determine Transformation matrix such that c_1D = Tj_1D_2D for each line segment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Splines1D = cell(Nlines, 4);
Nints = 0;
for i = 1:Nlines
    
    xline = Xlines{i};
    % calculate the line-simplex intersection points; they demark the 1-D simplices!
    [Xint, FacetIntIdx] = bsplinen_getLineSimplexIntersections(simplex2D, xline); % only works with line intersections
    if (isempty(Xint))
        warning('The given line does not intersect the given simplex2D, continuing...');
        continue; % no intersection with this simplex2D
    end
    Nints = Nints + 1;
    
    % init 1D spline
    spline1Di = spline1D;
    
    % Determine transformation matrix Tj_1D_2D that transforms B-coefs from 2D to 1D 
    [Tj_1D_2Di, spline1Di, Bcoords1D, Bcoords1D_1D] = bsplinen_calcSubDimPoly(spline2D, spline1Di, Xint);
    
    % save results
    Splines1D{Nints,1} = Tj_1D_2Di;
    Splines1D{Nints,2} = spline1Di;
    Splines1D{Nints,3} = Bcoords1D;
    Splines1D{Nints,4} = Bcoords1D_1D;
    
end

% remove any lines that did not intersect the simplex
if (Nints < Nlines) 
    Splines1D{Nints+1,:} = [];
end


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Calculate line integrals which form the measurement set Y_int. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Y_int = zeros(Nlines, 1);
for i = 1:Nlines
    spline1Di       = Splines1D{i,2};
    Bcoords1D       = Splines1D{i,3};
    Bcoords1D_1D    = Splines1D{i,4};
    
    % Build measurement dataset by calculating line integrals along intersecting lines.
    intSx = bsplinen_integrateBform(spline1Di); % NOTE: wrong results may be caused by all simplices having the same size ([0; 1])!
    mnoise = 0;
    if (measurementNoiseMag > 0)
        mnoise = measurementNoiseMag * (rand-.5); % add noise 
    end
    Y_int(i) = intSx + mnoise; % note that the method seems extremely sensitive to noise...
    
    % Numerical check of results: 1D spline evaluation results should be equal to 2D spline evaluation results on the line...
    valbc_2di = bsplinen_evalExt(spline2D, Bcoords1D); 
    valbc_1di = bsplinen_evalExt(spline1Di, Bcoords1D_1D); 
    rms_err_vali = sqrt(1/length(valbc_1di)*(valbc_2di-valbc_1di)'*(valbc_2di-valbc_1di));
    if (rms_err_vali > 1e-10)
        fprintf('WarningL Residual between 1D and 2D spline values: %d\n', rms_err_vali);
    end
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Formulate inverse problem: Y_int = D * c_2
%       D = V * [ones(1, dhat1D) * Tj_1D_2Di]_i=1:length(Y_int), with V = a constant depending only on the
%           vertex coordinates of the simplex.
%       We then get: c_2 = inv(D'*D)*D'*Y_int;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D = zeros(Nlines, dhat2D)
for i = 1:Nlines
    Tj_1D_2Di = Splines1D{i,1};
    spline1Di = Splines1D{i,2};
    
    % calculate constant Vi
    Vi = bsplinen_volSimplex(spline1Di.PHI) / dhat1D;
    % calculate the regression matrix Di
    Di = Vi * ones(1, dhat1D) * Tj_1D_2Di;
    % save results
    D(i, :) = Di;
end
% formulate dispersion matrix (DtD = inv(COV(DtD))).
DtD = D'*D;
rankDtD = rank(DtD);

% estimate coefficients of 2D spline from line integral measurements!
c_2D_hat = (D'*D) \ (D' * Y_int);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Validate performance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

err = c_2D_hat - spline2D.coefs;
errRMS = sqrt(1/length(err) * (err'*err));

[spline2D.coefs c_2D_hat]
fprintf('%d b-coefs reconstructed from %d line integrals, rank of DtD = %d, error rms(c_2D-c_2D_hat)= %d\n', dhat2D, Nlines, rankDtD, errRMS);

% evaluate true and reconstructed splines
recspline2D = spline2D;
recspline2D.coefs = c_2D_hat;
val_2d = bsplinen_evalExt(spline2D, x); 
val_rec2d = bsplinen_evalExt(recspline2D, x); 


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Plot Results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;

plotID = 101;
figure(plotID);
set(plotID, 'Position', [0 400 500 500], 'defaultaxesfontsize', 12, 'defaulttextfontsize', 12, 'PaperPositionMode', 'auto');
hold on;
trimesh(TRI, PHI(:,1), PHI(:,2), 'color', 'b');
plot(x(:,1), x(:,2), 'b.');
for i = 1:Nlines
    xline = Xlines{i};
    line(xline(:,1), xline(:,2), 'color', 'k', 'linewidth', 1);
end
plot(Bcoords1D(:,1), Bcoords1D(:,2), 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 0], 'MarkerSize', 6);
plot(Xint(:,1), Xint(:,2), 'ro');
axis([-1 1 -1 1]);
title('Simplex with locations of line integrals');

%%
az = -140;
el = 40;

% [Angles, Volumes, Spheres, Metric, Data] = bsplinen_measureTRIExt(x, TRIeval, []);
% deltri = find(Angles(:,1) < 3);
% TRIeval(deltri, :) = [];


plotID = 201;
figure(plotID);
set(plotID, 'Position', [1000 100 600 600], 'defaultaxesfontsize', 12, 'defaulttextfontsize', 12, 'PaperPositionMode', 'auto');
hold on;
grid on;
trimesh(TRI, PHI(:,1), PHI(:,2), 'color', 'b');
trisurf(TRIeval, x(:,1), x(:,2), val_2d-val_rec2d);
% plot3(Bcoords1D(:,1), Bcoords1D(:,2), p_bc2D, 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 0], 'MarkerSize', 6);
poslight = light('Position',[0.5 3.5 6],'Style','local');
hlight = camlight('headlight');
material([.3 .8 .9 25]);
% shading interp;
lighting phong;
view(az, el);
xlabel('x_1');
ylabel('x_2');
title('Reconstruction error');

plotID = 301;
figure(plotID);
set(plotID, 'Position', [300 100 1000 500], 'defaultaxesfontsize', 12, 'defaulttextfontsize', 12, 'PaperPositionMode', 'auto');
subplot(1, 2, 1);
hold on;
grid on;
trimesh(TRI, PHI(:,1), PHI(:,2), 'color', 'b');
trisurf(TRIeval, x(:,1), x(:,2), val_2d);
% plot3(Bcoords1D(:,1), Bcoords1D(:,2), p_bc2D, 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 0], 'MarkerSize', 6);
poslight = light('Position',[0.5 3.5 6],'Style','local');
hlight = camlight('headlight');
material([.3 .8 .9 25]);
% shading interp;
lighting phong;
view(az, el);
xlabel('x_1');
ylabel('x_2');
title('Original Spline Function');

subplot(1, 2, 2);
hold on;
grid on;
trimesh(TRI, PHI(:,1), PHI(:,2), 'color', 'b');
trisurf(TRIeval, x(:,1), x(:,2), val_rec2d);
% plot3(Bcoords1D(:,1), Bcoords1D(:,2), p_bc2D, 'o', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', [1 0 0], 'MarkerSize', 6);
poslight = light('Position',[0.5 3.5 6],'Style','local');
hlight = camlight('headlight');
material([.3 .8 .9 25]);
% shading interp;
lighting phong;
view(az, el);
xlabel('x_1');
ylabel('x_2');
titstr = fprintf('Reconstructed Spline Function (d=%d)', dhat2D);
title('Reconstructed Spline Function');




