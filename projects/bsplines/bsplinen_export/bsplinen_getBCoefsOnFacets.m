% bsplinen_getBCoefsOnFacets returns the b-coefficient indices located on the given list of facets
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%   Input to bsplinen_getBCoefsOnFacets:
%   
%   Note also that TRIfacets is the output from bsplinen_getTriangulationEdges2(fn.TRI, fn.PHI):
%       [TRIfacets, PHIfacets, edgeSimps, Edges] = bsplinen_getTriangulationEdges2(fn.TRI, fn.PHI);
%
%   Output from bsplinen_getBCoefsOnFacets:
%       EdgeCoefs: [T x 1] cell array which for each element (corresponding to a simplex) contains a cell array with elements:
%           EC = EdgeCoefs{j}
%           with EC{k} a [d+1 x 2+n] matrix having columns:
%               EC{k}(:,1) = local B-coefficient indices
%               EC{k}(:,2) = global B-coefficient indices
%               EC{k}(:,[3 end]) = B-coefficient Cartesian coordinates
%           
%   Note that EdgeCoefs{j} can be empty if simplex j is not a simplex present in TRIfacets.
%
%
%   Use as follows:
%       for i = 1:length(EdgeCoefs)
%           EC = EdgeCoefs{i};
%           for k = 1:length(EC)
%               xybcoefs = EC{k};
%               plot(xybcoefs(:,3), xybcoefs(:,4), 'd', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 0 1],'MarkerSize', 8);
%           end
%       end
%   
function EdgeCoefs = bsplinen_getBCoefsOnFacets(TRI, PHI, TRIfacets, d)

    n = size(TRI, 2)-1;
    % sort the vertices for every simplex in TRI 
    TRI = sort(TRI, 2);
    T = size(TRI, 1);
    
    polybasis  = bsplinen_constructBasis(n, d);
    dhat = size(polybasis, 1);
    
    EdgeCoefs = cell(length(TRIfacets), 1);
    
    % go through all vertex indices
    for j = 1:length(TRIfacets)
        if (isempty(TRIfacets{j}))
            continue;
        end
        
        trie = TRIfacets{j};
        EC = cell(1, size(trie, 1));
        
        for k = 1:size(trie, 1)
            % vertex index of out of edge vertex
            vidx = setdiff(TRI(j,:), trie(k,:));
            bcoef0idx = find(TRI(j,:) == vidx);

            % the B-coefficients on this edge will have zeros at al indices bcoef0idx
            Ibcoefs = find(polybasis(:,bcoef0idx) == 0); % local B-coef indices
            globalIbcoefs = (j-1)*dhat + Ibcoefs;
            
            % bcoef Cartesian coordinates
            bary_bcoefcoords = (polybasis(Ibcoefs, :)) / d;
            cart_bcoefcoords = bsplinen_bary2cart(PHI(TRI(j, :), :), bary_bcoefcoords);
            
            EC{k} = [Ibcoefs globalIbcoefs cart_bcoefcoords];
        end
        
        EdgeCoefs{j} = EC;

    end     
    
    
    
    
