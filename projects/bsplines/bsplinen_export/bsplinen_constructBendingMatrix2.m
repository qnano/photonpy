% bsplinen_constructBendingMatrix2.m
%
%   Syntax:
%       Kb = bsplinen_constructBendingMatrix2(TRI,PHI,d,coefs)
%
%   Description:
%       This function constructs the galerkin Bending matrix given
%       by : integral ( sum B_mu c_mu [B'(x) * B(x)] ) dx
%
%       where c_mu are the B-coefficient vector of the spline interpolating 
%       function for the PDE coefficient
%
%       Input parameters:
%           - d         : double [1 x 1] :      spline degree
%           - TRI       : double [J x n+1] :    triangulation
%           - PHI       : double [Nv x n+1]:    vertices
%           - coefs     : double [J dhat x 1]:  coefficients of the interpolating function
%
%       Output parameters:
%           - Mass        : double [dhat*J x dhat*J]
%
%   -------------------------------
%   H.J.Tol 2014, C.C. de Visser 2014 
%
%   -------------------------------
function Kb = bsplinen_constructBendingMatrix2(TRI,PHI,d,coefs)

%properties
n       = size(PHI,2);
J       = size(TRI, 1);
[polybasis, ~] = bsplinen_constructBasis(n, d);
dhat    = size(polybasis, 1); 

%intialize
Kb      = sparse(J*dhat, J*dhat);

% precalc factorials as they are rediculously slow in Matlab!
fact_n = factorial(n);
fact_d = factorial(d);
fact_3d = factorial(3*d);
nchoosek_3dn_n = nchoosek(3*d+n,n);

large_eyepb = kron(speye(dhat), (ones(dhat, 1))); % block diagonal matrix containing blocks of ones(dhat, 1).
huge_eyepb = kron(speye(dhat), (ones(dhat^2, 1))); % block diagonal matrix containing blocks of ones(dhat*dhat, 1).

% dhat per-polybasis row copies of polybasis
reprow_polybasis = large_eyepb * polybasis;

% dhat*dhat complete copies of polybasis 
rep_rep_polybasis = repmat(polybasis, dhat^2, 1);
% dhat*dhat per-polybasis row copies of polybasis 
rep_reprow_polybasis = repmat(reprow_polybasis, dhat, 1);

% dhat*dhat*dhat per-polybasis row copies of polybasis (used for gamma enumeration)
reprep_reprow_polybasis = huge_eyepb * polybasis;

% combinatorial combination of all possible values of gamma, mu, and nu.
all_gamma_mu_nu = reprep_reprow_polybasis + rep_reprow_polybasis + rep_rep_polybasis; % (gamma + mu) + polybasis for ALL values of (gamma + mu)!
% factorial of all_gamma_mu_nu
fact_all_gamma_mu_nu = factorial(all_gamma_mu_nu);

% the GENERIC per-simplex multinomial coefficient for all basis function terms
Kb_tj = prod(fact_all_gamma_mu_nu,2)./( prod(factorial(reprep_reprow_polybasis),2) .* prod(factorial(rep_reprow_polybasis),2) .* prod(factorial(rep_rep_polybasis),2) );
% reshape for fast calculation of bending matrix
res_Kb_tj = reshape(Kb_tj, dhat^2, dhat);

for t = 1:J
    % Get the simplex t.
    simpt = PHI(TRI(t, :), :);
    
    % assemble matrix A with the i-th column containing the distance between
    % the i-th simplex vertex and the 0-th vertex.
    A = zeros(n);
    count = 1;
    for j = 2:length(simpt(:, 1))
        A(:, count) = (simpt(j, :) - simpt(1, :))';
        count = count + 1;
    end
    
    % the general formula for the volume of a simplex is:
    % volT = 1/n! * det(v1-v0; v2-v0; ... ; vn-v0)
    % however, it seems that det(A) is sometimes negative, resulting in
    % a negative volume, which is why the abs() operator is added.
    volT = (1 / fact_n) * abs(det(A));
    
    %select the per simplex coefficients
    coefs_t = coefs((t-1)*dhat+1:t*dhat);
    % reshape into a form that it can be directly multiplied with res_Kb_tj
    rep_coefs_t = kron(speye(dhat), coefs_t');
        
    % calculate specific coefficient*multinomial coefs for this simplex
    Kb_t = rep_coefs_t * res_Kb_tj;
    
    %index list
    idx = (t-1)*dhat+1:t*dhat;
    idy = idx;
    
    %Block diagonal full triangulation mass matrix
    Kb(idy, idx) = ((fact_d* fact_d * fact_d) /  fact_3d ) ...
            *(volT / nchoosek_3dn_n) * Kb_t;    
    
end



% bsplinen_constructBendingMatrix.m
%   
%   Old version, uses multi-loop algorithms.
%   -> use this algorithm for translation to C++
%
% function Kb = bsplinen_constructBendingMatrix(TRI,PHI,d,coefs)
% 
%     %properties
%     n       = size(PHI,2);
%     J       = size(TRI, 1);
%     dhat    = bsplinen_calcdHat(n, d);
%     [polybasis, ~] = bsplinen_constructBasis(n, d);
% 
%     %intialize
%     Kb      = sparse(J*dhat, J*dhat);
% 
%     % large_eyepb = blkdiag(
%     large_eyepb = kron(speye(dhat), (ones(dhat, 1))); % block diagonal matrix containing blocks of ones(dhat, 1).
%     % precalc factorials as they are rediculously slow in Matlab!
%     fact_n = factorial(n);
%     fact_d = factorial(d);
%     fact_3d = factorial(3*d);
%     fact_pbasis = factorial(polybasis);
% 
%     for t = 1:J
%         % Get the simplex t.
%         simpt = PHI(TRI(t, :), :);
% 
%         % assemble matrix A with the i-th column containing the distance between
%         % the i-th simplex vertex and the 0-th vertex.
%         A = zeros(n);
%         count = 1;
%         for j = 2:length(simpt(:, 1))
%             A(:, count) = (simpt(j, :) - simpt(1, :))';
%             count = count + 1;
%         end
% 
%         % the general formula for the volume of a simplex is:
%         % volT = 1/n! * det(v1-v0; v2-v0; ... ; vn-v0)
%         % however, it seems that det(A) is sometimes negative, resulting in
%         % a negative volume, which is why the abs() operator is added.
%         volT = (1 / fact_n) * abs(det(A));
% 
%         %initialze per simplex bending matrix
%         Kb_t = sparse(dhat,dhat);
%         %select the per simplex coefficients
%         coefs_t = coefs((t-1)*dhat+1:t*dhat);
% 
%         %contruct the multi-nomial coefficient of the integral
%         for ii = 1:dhat
%             gamma  = polybasis(ii,:); %test function multi-index
%             fact_gamma = factorial(gamma);
%             KT = 0;  %set to zero
%             %sum over all B-coefficients of the interpolating function      
%             for kk = 1:length(coefs_t)      
%                 mu  = polybasis(kk,:); %interpolating coefficient multi-index            
%                 %combined mult-index : kappa+gamma+mu
%                 nu = polybasis + ones(size(polybasis,1),1) * (gamma + mu);
% 
%                 %multinomial coefficient of the integral
%                 KT = KT + coefs_t(kk)* (prod(factorial(nu),2))./...
%                     ( prod(fact_gamma,2) ...
%                     *prod(fact_pbasis,2)*prod(factorial(mu),2)); 
%             end             
%             Kb_t(ii,:) = KT'; 
% 
%         end    
%         %index list
%         idx = (t-1)*dhat+1:t*dhat;
%         idy = idx;
% 
%         %Block diagonal full triangulation mass matrix
%         Kb(idy, idx) = ((fact_d* fact_d * fact_d) /  fact_3d ) ...
%                 *(volT / nchoosek(3*d+n,n) ) * Kb_t;    
% 
%     end