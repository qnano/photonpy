% BSPLINE_CONSTRUCTBASIS.m
%
%   Syntax:
%       [PolyBasis, BasisKt] = bsplinen_constructBasis(n, d)
%
%   Description:
%       Function creates the polynomial basis function indices for the
%       multivariate B-spline for a given dimension and order. The indices
%       are sorted lexicographically as proposed by Hu et al., in [Hu et
%       al, 2007]. 
%
%       Input parameters are:
%           - n     : double [1 x 1]
%           - d     : double [1 x 1]
%           N a scalar holding the total number of dimensions for the
%           spline function.
%
%           D a scalar holding the order of the basis polynomials.
%
%       Output parameters are:
%           - PolyBasis : double [dHat x n + 1]
%           POLYBASIS an array with size ((N + D)! / N!D!) x (N+1).
%           The polynomial indices are stored row by row. The polynomial
%           indices are (row) sorted in descending order.
%
%           - BasisKt : int [dHat x 1]
%           Holds the multinomial coeffcient values
%
%   -------------------------------
%   File part of the Simplex Spline Toolbox
%   C.C. de Visser
%   07-08-2009
%   22-02-2012
%   -------------------------------
function [PolyBasis, BasisKt] = bsplinen_constructBasis(n, d)

% the polynomial orders 0 and 1 require no calculations
if (d == 0)
    PolyBasis = zeros(1, n+1);
    BasisKt = 0;
    return;
end

PolyBasis = eye(n+1, n+1);
if (d == 1)
    if (nargout > 1)
        BasisKt =  ones(n+1, 1);
    end
    return;
end

% Recursively increase the degree of the polynomial basis. This method
% requires no sorting or removal of nonunique rows. 
for i = 2:d
    dhat = size(PolyBasis, 1);
    % increment vector of 1's
    inc = ones(dhat, 1);
    % the new matrix of polynomial basis indices
    basisnew = zeros(dhat * (n+1), n+1);
    % Go through all dimensions and add the increment vector (1's) to all
    % elements in the j-th column.
    for j = 1:n+1
        idx = (j-1)*dhat+1:j*dhat;
        basisnew(idx, j+1:end) = PolyBasis(:, j+1:end);
        basisnew(idx, j) = PolyBasis(:, j) + inc;
    end

    % remove all terms that do not sum up to d
    PolyBasis = basisnew(sum(basisnew, 2) == i, :);

end

if (nargout > 1)
    BasisKt =  factorial(d)./prod(factorial(PolyBasis),2);
end


return
%---------------------------------- end of file ---------------------------