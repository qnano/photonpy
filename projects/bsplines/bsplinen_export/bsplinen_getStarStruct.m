

function StarStruct = bsplinen_getStarStruct(TRI, starlevel, starstrength, simpInd)

    if (starstrength <= 0)
        starstrength = 1;
    end
    
    Simplices = simpInd;

    if (starlevel == 0)
        StarStruct = simpInd;
    end
    
    % assemble the complete star structure of level 'starlevel'
    for level = 1:starlevel
        % get all vertices in the simplex
        StarStruct = [];
        for t = 1:size(Simplices, 1)
            % go through all the vertices of the simplex 't' and assemble their stars.
            vertices = TRI(Simplices(t), :);
            simpStar = [];
            for i = 1:length(vertices)
                % get the star of  vertex 'i' in simplex 't'
                star = bsplinen_getStar(TRI, vertices(i));

                % add the simplices in star, if they are not already added
                for j = 1:length(star)
                    if (~any(simpStar == star(j)) )
                        simpStar = [simpStar; star(j)];
                    end
                end
            end
            % filter simpStar on the star vertex count
            if (starstrength > 1)
                simpStarTmp = [];
                for i = 1:length(simpStar)                    
                    % start counting the number of vertices that simpStar(i)
                    % shares with Simplices(t)
                    simpv = TRI(simpStar(i), :);
                    count = 0;
                    for j = 1:length(simpv)
                        if (any(vertices == simpv(j)))
                            count = count + 1;
                        end
                    end
                    if (count >= starstrength)
                        simpStarTmp = [simpStarTmp; simpStar(i)];
                    end
                end
                simpStar = simpStarTmp;
            end
            
            % add the simplices in simpStar to all the simplices in star
            % level 'level', if they are not already added
            for i = 1:length(simpStar)
                if ( ~any(StarStruct == simpStar(i)) ) 
                    StarStruct = [StarStruct; simpStar(i)];
                end
            end
            %StarStruct = [StarStruct; simpStar];
            
        end
        Simplices = StarStruct;
        
    end
    
    if (length(simpInd) > 1)
        return;
    end
    
    % finally, add the vertices of simplex 'simpInd' to the top of the list 
    % (if it is not already there)
    subsimp = find(StarStruct == simpInd);
    if (isempty(subsimp))
        StarStruct = [simpInd; StarStruct];
    else
        if (subsimp ~= 1)
            StarStruct(subsimp) = [];
            StarStruct = [simpInd; StarStruct];
        end
    end
    