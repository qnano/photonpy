
% bsplinen_fulltriCasteljauMat constructs the full-triangulation de Casteljau matrix from degree d to degree dA 
%   in the direction dU.
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%              version 3.0
% 
%  Castel = bsplinen_fulltriCasteljauMat(PHI, TRI, d, dA, dU)
%      constructs the full-triangulation de Casteljau matrix from degree d to degree d-dA
%
function Castel = bsplinen_fulltriCasteljauMat(PHI, TRI, d, dA, dU)

    
    T = size(TRI,1);
    n = size(PHI, 2);
    
    BdA = [];
    % Go through all simplices, and construct the de Casteljau matrix for each
    partdatacount = 0;
    partdatacountC = 0;

    % determine the de Casteljau matrix for each simplex
    
    dhat = bsplinen_calcdHat(n, d);
    dhatdA = bsplinen_calcdHat(n, dA);
    
    Castel = sparse(T * dhatdA, T * dhat);

    for t = 1:T
        simpt = PHI(TRI(t, :), :);

        % Calculate the barycentric coordinates of U with respect to simplex t.
        nZ = simpt(1,:) - dU; % normalized relative direction
        alpha = bsplinen_cart2bary(simpt, simpt(1,:));
        beta = bsplinen_cart2bary(simpt, nZ);
        % the directional coordinate for simplex t
        dircoord = alpha - beta;     

        % Build the de d-to-(d-dA) de Casteljau matrix using the directional coordinates dircoord of dU
        tCastel = [];
        for da = d:-1:dA+1
            if (da == d)
                tCastel = bsplinen_constructDeCasteljauMatExt(dircoord, d);
            else
                tCastel = bsplinen_constructDeCasteljauMatExt(dircoord, da) * tCastel;
            end
        end
        
        % insert block in global de Casteljau matrix
        ridx = (t-1)*dhatdA+1:(t)*dhatdA;
        cidx = (t-1)*dhat+1:(t)*dhat;
        Castel(ridx, cidx) = tCastel;

    end
    

