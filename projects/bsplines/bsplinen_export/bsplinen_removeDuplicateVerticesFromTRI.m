

function [TRInew PHInew replaceidx] = bsplinen_removeDuplicateVerticesFromTRI(TRI, PHI)

    % first sort rows of PHI
    [PHItmp sortidx] = sortrows(PHI);
    % the inverse sorting from PHItmp to PHI such that PHItmp(invsortidx,:) - PHI = 0
    [tmp invsortidx] = sort(sortidx);

    % Build the vector into the SORTED vertex matrix containing the unique vertex indices
    sortedreplacevect = zeros(size(PHI, 1), 1); % the sorted replacement vector
    sortedreplaceneeded = zeros(size(PHI, 1), 1); % the sorted switch vector holding 1's wherever a replacement is needed
    sortedreplacevect(1) = 1;
    lastunique = 1;
    % go through sorted rows 
    for i = 2:size(PHItmp, 1)
        if ( all(PHItmp(i-1, :) == PHItmp(i, :)) )
            % same coordinates in this row as in previous!
            % this means vertex i is the same as the last unique vertex in the SORTED vertex matrix...
            sortedreplacevect(i) = lastunique;
            sortedreplaceneeded(i) = 1;
        else
            % we have a unique vertex
            sortedreplacevect(i) = i;
            lastunique = i;
        end
    end
    % At this point we have that PHItmp(sortedreplacevect,:) - PHItmp = 0
    % We now have a vector of replacement indices into the SORTED vertex matrix; this vector must
    % first be translated into indices in the PHItmp matrix such that PHItmp(replacevect_tmp,:) - PHI = 0
    replacevect_tmp = sortedreplacevect(invsortidx);
    % Now the indices need to be translated to PHI indices such that PHItmp(replacevect_tmp,:) = PHI(replacevect,:)
    % but also that PHI(replacevect,:) - PHI = 0
    replacevect = sortidx(replacevect_tmp);
    % Translate the replacement switch vector into the UNSORTED vertex matrix
    replaceneeded = sortedreplaceneeded(invsortidx);
    offsetneeded = cumsum(replaceneeded);
    % Get the indices to be replaced by some other
    replaceidx = find(replaceneeded);

    % Remove duplicates!
    PHI(replaceidx, :) = [];

    % perform the replacement
    linTRI = TRI(:);
    % no need to search linTRI because we can use the values of linTRI as indices in replaceneeded!!!
    for i = 1:length(linTRI)
        if (replaceneeded(linTRI(i)))
            linTRI(i) = replacevect(linTRI(i));
        end
    end

    % offset the triangulation indices to mirror removed vertices
    [sortlinTRI sortlinidx] = sort(linTRI);
    [tmp invsortlinidx] = sort(sortlinidx);% inverse sorting
    % remove offset from vertex index
    sortlinTRI = sortlinTRI - offsetneeded(sortlinTRI);

    linTRInew = sortlinTRI(invsortlinidx);
    % assign to output matrices
    TRInew = reshape(linTRInew, size(TRI));
    PHInew = PHI;


