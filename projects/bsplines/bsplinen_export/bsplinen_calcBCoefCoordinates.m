
% BSPLINEN_CALCBCOEFCOORDINATES calculates the coordinates of the B-coefficients in their parent simplex
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2010
%              email: c.c.devisser@tudelft.nl
%                          Version: 2.0
%
function [Bcoords BcoordsBary] = bsplinen_calcBCoefCoordinates(varargin)

Bcoords = [];
BcoordsBary = [];

if (nargin == 1)
    if (~isstruct(varargin{1}))
        warning('Single argument input must be of type STRUCT_BSPLINEN');
        return;
    end
    fn = varargin{1};
elseif (nargin == 4) 
    bsplinen_structures;
    fn = struct_bsplinen;
    fn.PHI = varargin{1};
    fn.TRI = varargin{2};
    fn.dim = varargin{3};
    fn.degree = varargin{4};
else
    warning('Function must be called with either 1 argument (STRUCT_BSPLINEN) or with 4 arguments (PHI, TRI, dim, degree)');
    return;
end

basis = bsplinen_constructBasis(fn.dim, fn.degree);
dhat = size(basis, 1);
V = size(fn.TRI, 1);

Bcoords = zeros(size(V * dhat, 1), fn.dim);
BcoordsBary = zeros(size(V * dhat, 1), fn.dim + 1);

if (fn.degree == 0)
    for i = 1:V

        % go through all domain points
        for j = 1:dhat
            % barycentric coordinates of the domain point at j
            b = 1/(fn.dim+1) * ones(1, fn.dim+1);
            simp = fn.PHI(fn.TRI(i, :), :);
            % barycentric Bcoords
            BcoordsBary(j + (i-1)*dhat, :) = b;
            % transform to global coordinates
            Bcoords(j + (i-1)*dhat, :) = bsplinen_bary2cart(simp, b);
        end

    end

else
    % go through all simplices
    for i = 1:V

        % go through all domain points
        for j = 1:dhat
            % barycentric coordinates of the domain point at j
            b = basis(j, :) / fn.degree;
            simp = fn.PHI(fn.TRI(i, :), :);
            % barycentric Bcoords
            BcoordsBary(j + (i-1)*dhat, :) = b;
            % transform to global coordinates
            Bcoords(j + (i-1)*dhat, :) = bsplinen_bary2cart(simp, b);
        end

    end

end






