% bsplinen_createDistributedSpline creates a distributed spline function
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
function [DSPLINES GlobalSpline PartitionData H EDGES] = bsplinen_createDistributedSpline(PHI, TRI, d, r, bopts)

    bsplinen_structures;
    
    DSPLINES = cell(prod(bopts.tri_partitioncount), 1);
    GlobalSpline = struct_bsplinen;
    
    n = size(PHI, 2);
    polybasis = bsplinen_constructBasis(n, d);
    dhat = size(polybasis, 1);
    T = size(TRI, 1);
    
    GlobalSpline.dim = n;
    GlobalSpline.degree = d;
    GlobalSpline.continuity = r;
    GlobalSpline.PHI = PHI;
    GlobalSpline.TRI = TRI;
    GlobalSpline.coefs = ones(dhat*size(TRI, 1), 1);
    
    
    PartitionData = bsplinen_hypercubePartitionExt(TRI, PHI, bopts); % creates a hypercube partitioning of triangulation
    NParts = size(PartitionData, 1);
    
    % create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
    [tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI});

    % create the matrix holding the smoothness constraints
    [H IHedges] = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, r, bopts);

    % if only a single partition was created, then create new partition set for each simplex
    if (NParts == 1)
        PartitionData = cell(T, size(PartitionData, 2));
        NParts = T;

        for i = 1:NParts
            PartitionData{i, 1} = [];
            PartitionData{i, 2} = i;
            PartitionData{i, 3} = TRI(i, :);
        end

    end
    
    % Create a local spline function for each triangulation partition
    for i = 1:NParts
        
        Ipartsimps      = PartitionData{i, 2};
        tri             = PartitionData{i, 3};
        Ilocaledgesimp  = PartitionData{i, 6}; % edge simplex idx in Ipartsimps
        ICurrentPart    = PartitionData{i, 8};
        INeighborParts  = PartitionData{i, 9};

        % D-Spline structure
        DSPL            = struct_dsplinen; 
        % Local spline function
        spl             = struct_bsplinen; 
        spl.dim         = n;
        spl.degree      = d;
        spl.continuity  = r;
        spl.TRI         = tri;
        spl.PHI         = PHI;
        spl.coefs       = ones(size(spl.TRI, 1)*dhat, 1);
        
        % B-coef indices in global spline
        Ibcoefs = ((Ipartsimps-1) * dhat * ones(1, dhat) + ones(size(Ipartsimps, 1), dhat) * diag(1:dhat))';
        lIbcoefs = Ibcoefs(:);

        % get all simplex edges of the partition edge simplices (speeds up further searching)
        Iedgesimps = Ipartsimps(Ilocaledgesimp);
        I2edgesimps = [Iedgesimps; Iedgesimps];
        partEDGES = cell(length(EDGES), 1);
        pecount = 0;
        for j = 1:length(EDGES)
            if (EDGES{j}.simplex1 > EDGES{j}.simplex2)
                tmp = EDGES{j}.simplex1;
                EDGES{j}.simplex1 = EDGES{j}.simplex2;
                EDGES{j}.simplex2 = tmp;
            end
            edgemat = [ones(length(Iedgesimps), 1) * EDGES{j}.simplex1; ones(length(Iedgesimps), 1) * EDGES{j}.simplex2];
            ts = [];
            if (~isempty(edgemat) && ~isempty(I2edgesimps))
                ts = [ones(length(Iedgesimps), 1) * EDGES{j}.simplex1; ones(length(Iedgesimps), 1) * EDGES{j}.simplex2] - I2edgesimps;
            end
            Iesimps = find(ts == 0);
            if (~isempty(Iesimps))
                pecount = pecount + 1;
                partEDGES{pecount} = EDGES{j};
            end
        end
        partEDGES = partEDGES(1:pecount); % now contains only edges that exist between simplices in the partition edges

        % Determine the partitions on which this partitions depends for the PME
        IControlPartitions = nan(size(INeighborParts, 1), 1); % this is a linear index!
        pidx = ICurrentPart(2:end) - bopts.tri_partitionmaster - 1; % offset to zero-based IMasterOffset index
        pcount = 0;
        for j = 1:size(INeighborParts)
            nbp = INeighborParts(j, 2:end) - bopts.tri_partitionmaster - 1;
            if (~any(abs(nbp) > abs(pidx)))
                pcount = pcount + 1;
                IControlPartitions(pcount) = INeighborParts(j, 1);
            end 
        end
        if (~all(isnan(IControlPartitions)))
            IControlPartitions = sort(IControlPartitions(1:pcount));
            IControlPartitions = IControlPartitions(1);
        else 
            IControlPartitions = [];
        end
        
        
        % get global indices of all B-coefs located at the edges that separate this partition from its master partitions
        IBcoefsglobal = [];
        IBcoefslocal = [];
        if (~isempty(IControlPartitions))
            Iedges = zeros(length(partEDGES),1);
            Iedgecount = 0;
%             for j = 1:1%length(IControlPartitions)
            INBsimp          = PartitionData{IControlPartitions(1), 2}; % partition simplex idx
            INBlocaledgesimp = PartitionData{IControlPartitions(1), 6}; % edge simplex idx
            INBedgesimps     = INBsimp(INBlocaledgesimp);
            for k = 1:length(partEDGES)
                dualINBedgesimps = [INBedgesimps; INBedgesimps];
                ts = [ones(length(INBedgesimps), 1) * partEDGES{k}.simplex1; ones(length(INBedgesimps), 1) * partEDGES{k}.simplex2] - dualINBedgesimps;
                Iesimps = find(ts == 0);
                if (~isempty(Iesimps))
                    Iedgecount = Iedgecount + 1;
                    Iedges(Iedgecount) = partEDGES{k}.index;
                end
            end
%             end
            if (~isempty(Iedges))
                Iedges = Iedges(1:Iedgecount);
                % get global indices of all B-coefs located at triangulation edges
                [IBcoefsglobal BCOEFS] = bsplinen_getBCoefIndicesAtEdges(EDGES(Iedges), polybasis);
            end
            % determine indices of control B-coefs in local spline function
            % EXTREMELY IMPORTANT TO GET CORRECT INTERSECT! Problem has to do with how bsplinen_getEdges works...
            [tmp tmp IBcoefslocal] = intersect(IBcoefsglobal(:,1), lIbcoefs);
            if (isempty(IBcoefslocal))
                % swap columns as there is a problem with column ordering in bsplinen_getEdges
                [tmp tmp IBcoefslocal] = intersect(IBcoefsglobal(:,2), lIbcoefs);
                tmp = IBcoefsglobal(:,2);
                IBcoefsglobal(:,2) = IBcoefsglobal(:,1);
                IBcoefsglobal(:,1) = tmp;
            end
        else
            IBcoefsglobal = [];
        end

        % Start analyzing the global continuity matrix
        IHrows = zeros(size(H, 1), 1);
        count = 0;
        for j = 1:size(H, 1)
            if (any(H(j, lIbcoefs) ~= 0))
                count = count + 1;
                IHrows(count) = j;
            end
        end
        Ir = IHrows(1:count); % row indices in H: equal to active equations in H
        Hsub = H(Ir, :);
        IHcols = zeros(size(H, 1),1);
        count = 0;
        for j = 1:size(Hsub, 2)
            if (any(Hsub(:, j) ~= 0))
                count = count + 1;
                IHcols(count) = j;
            end
        end
        IAllglobalcontrolRn = IHcols(1:count, 1);
%         [tmp IglobalIHcols] = intersect(Ic, lIbcoefs); % intersect = VERY SLOW

        HsubCo    = Hsub(:, IAllglobalcontrolRn); % H-sub-matrix containing inner AND outer partition smoothness constraints
        HsubLocal = Hsub(:, lIbcoefs); % H-sub-matrix containing only inner partition smoothness constraints

        % calculate simplex indices of all B-coefficients in the global control index list
        IglobalcontrolSimps = ceil(IAllglobalcontrolRn./(dhat));
        IglobalcontrolSimpParts = zeros(size(IglobalcontrolSimps));
        % determine partition index for each of the control simps
        for j = 1:length(Ipartsimps)
            ips = Ipartsimps(j);
            idx = find(IglobalcontrolSimps == ips);
            IglobalcontrolSimpParts(idx) = i;
        end
        
        for j = 1:size(INeighborParts, 1)
            Inb = INeighborParts(j, 1);

            for k = 1:length(Ipartsimps)
                ips = Ipartsimps(k);
                idx = find(IglobalcontrolSimps == ips);
                IglobalcontrolSimpParts(idx) = j;
            end
        end
        
        
% %         % determine indices of B-coefs that form interpartition connections.
%         NPcols = setdiff(IAllglobalcontrolRn, lIbcoefs); % Indices of B-coefficients in neighboring partition
%         % find row indices that correspond with NPcols
%         INrows = zeros(size(H, 1), 1);
%         count = 0;
%         for j = 1:length(NPcols)
%             idx = find(H(:, NPcols(j)));
%             INrows(count+1:count+length(idx)) = idx;
%             count = count + length(idx);
%         end
%         INrows = unique(INrows(1:count));
%         % find B-coef indices that correspond with equations linking neigboring partitions
%         INBcoefs = zeros(size(H, 1), 1);
%         count = 0;
%         for j = 1:length(INrows)
%             idx = find(H(INrows(j), :));
%             INBcoefs(count+1:count+length(idx)) = idx;
%             count = count + length(idx);
%         end
%         INBcoefs = unique(INBcoefs(1:count)); % these indices are used during stage 1 of the DA algorithm: PME!
%         [IinpartBcoefs IinpartINcols] = intersect(lIbcoefs, INBcoefs); % find B-coefficient indices (same as IBcoefsglobal(:,2))
%         HsubPME = H(INrows, :);
%         [tmp IinIbcoefs] = intersect(lIbcoefs, IinpartBcoefs);

        % Assign data to DSPLINEN fields
        DSPL.index                  = i; % partition index of this spline fn
        DSPL.spline0                = spl; % a STRUCT_BSPLINEN structure; this is the initial/temporary local spline function
        DSPL.spline                 = spl; % a STRUCT_BSPLINEN structure; this is the local spline function
        DSPL.Iglobal                = lIbcoefs; % B-coef indices of B-coefficients in GLOBAL spline function
        if (~isempty(IControlPartitions))
            DSPL.IglobalcontrolR0   = IBcoefsglobal(:,1); % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of spline.TRI
            DSPL.INBglobalcontrolR0 = IBcoefsglobal(:,2); % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of MASTER NEIGHBOR-spline.TRI
            DSPL.Icontrolparts      = IControlPartitions(1); % indices of control partition
        else
            DSPL.IglobalcontrolR0   = []; % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of spline.TRI
            DSPL.INBlocalcontrolR0  = []; % B-coef indices in LOCAL Neighbor spline function of R0 continuity equations on edges of MASTER neigbor-spline.TRI 
            DSPL.INBglobalcontrolR0 = []; % B-coef indices in GLOBAL spline function of R0 continuity equations on edges of MASTER NEIGHBOR-spline.TRI            
            DSPL.Icontrolparts      = []; % indices of control partition
        end
        DSPL.IAllglobalcontrolRn    = IAllglobalcontrolRn; % B-coef indices in GLOBAL spline function of R0-Rn continuity equations of this partition and its neighbors
        DSPL.IlocalcontrolR0        = IBcoefslocal; % B-coef indices in LOCAL spline function of R0 continuity equations on edges of spline.TRI 
        DSPL.HsubLocal              = HsubLocal; % the sub-partition of the global smoothness matrix H that drives local continuity (also between neigboring partitions)
        DSPL.HsubCo                 = HsubCo; % the sub-partition of the global smoothness matrix H that drives local continuity (also between neigboring partitions)
        DSPL.dualC                  = zeros(size(Hsub, 1), 1); % dual of the B-coefficients for Dual Ascent method
        DSPL.bcoords                = bsplinen_calcBCoefCoordinates(spl); % coordinates of b-coefficients of LOCAL spline)
            
        DSPLINES{i} = DSPL;
        
    end
    
    % set the master spline + local master spline control B-coef indices
    for i = 1:NParts
        DSPL = DSPLINES{i};
        if (~isempty(DSPL.Icontrolparts))
            NeigborDSPL = DSPLINES{DSPL.Icontrolparts};
            DSPL.NBspline = NeigborDSPL.spline;
            [tmp tmp INBcoefslocal] = intersect(DSPL.INBglobalcontrolR0, NeigborDSPL.Iglobal);
%             [tmp tmp INBcoefslocal] = intersect(NeigborDSPL.Iglobal, DSPL.INBglobalcontrolR0);
            DSPL.INBlocalcontrolR0  = INBcoefslocal; % B-coef indices in LOCAL Neighbor spline function of R0 continuity equations on edges of MASTER neigbor-spline.TRI
        end
        
        DSPLINES{i} = DSPL;
    end


        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cval = bsplinen_calcIndexTotalValue(c_index)

    r = length(c_index);
    power = r - 1;
    
    cval = 0;
    for j = 1:r
        cval = cval + c_index(j) * 10^power;
        power = power - 1;
    end


    
function CIndexArr = bsplinen_constructCIndexArray(polybasis)

    dhat = length(polybasis(:, 1));
    
    CIndexArr = sparse(dhat, 1);
    
    for i = 1:dhat
    
        ctotval = bsplinen_calcIndexTotalValue(polybasis(i, :));
        CIndexArr(ctotval) = i;
    
    end
    
    
function [IBcoefsglobal BCOEFS] = bsplinen_getBCoefIndicesAtEdges(EDGES, polybasis)

    np1 = size(polybasis, 2);
    dp1 = sum(polybasis(1, :)) + 1;

    BCOEFS = cell(size(EDGES,1), 1);
    IBcoefsglobal = zeros(size(EDGES,1)*dp1, 2);
    
    % create a sparse matrix using the index total value of the
    % B-coefficient indices as keys to retrieve (very fast) the index
    % value of B-coefficients in the global B-vector
    CIndexArr = bsplinen_constructCIndexArray(polybasis);

    % go through all edges, and determine B-coefficient indices in the edges
    for i = 1:size(EDGES, 1)
                                
        % Formulate Dummy 0-th order continuity equations for this edge
        % columns: 1 = order, 2 = c1, 3 = c1 in global c, 4 = c1 in local, 5 = c2 + gamma, 6 = c2 + gamma in global c, 7 = c2 + gamma in local, 8 = gamma
        [continuityConds tmp] = bsplinen_continuityConditions(polybasis, EDGES{i}, {zeros(np1, 1)}, 0, [], CIndexArr, []);
        Ibcoefs1 = [continuityConds{:, 3}]';
        Ibcoefs2 = unique([continuityConds{:, 6}]');
        
        BCOEFS{i, 1} = Ibcoefs1;
        BCOEFS{i, 2} = Ibcoefs2;
        
        IBcoefsglobal((i-1)*dp1+1:i*dp1, :) = [Ibcoefs1 Ibcoefs2];
    end
    
    
    
function [IndexC IndexLocal] = bsplinen_getcindex(polybasis_size, CIndexArr, simplex_index, c_local_indices)

    clocal_size = length(c_local_indices(:, 1));
    
    if (clocal_size == 1)
        ctotval = bsplinen_calcIndexTotalValue(c_local_indices);

        IndexLocal = CIndexArr(ctotval);

        IndexC = (simplex_index - 1) * polybasis_size + IndexLocal;
    else
        
        IndexC = zeros(clocal_size, 1);
        IndexLocal = zeros(clocal_size, 1);
        for cind = 1:clocal_size
            clocal = c_local_indices(cind, :);
            ctotval = bsplinen_calcIndexTotalValue(clocal);

            IndexLocal(cind) = CIndexArr(ctotval);

            IndexC(cind) = (simplex_index - 1) * polybasis_size + IndexLocal(cind);
        end
    end


     
    
function c2Indices = bsplinen_getCSubjects(polybasis, ssv1col, ssv2col, c1, gamma)

    % get the index values at all columns but the ssv1 column
    kc2 = zeros(1, length(c1)-1);
    count = 1;
    lenc1 = length(c1);
    for i = 1:lenc1
        if (i ~= ssv1col)
            %kc2 = [kc2 c1(i)];
            kc2(count) = c1(i);
            count = count + 1;
        end
    end

    % construct the subject basis index
    c2base = zeros(1, lenc1);
    kccount = 1;
    for i = 1:lenc1
        if (i ~= ssv2col)
            c2base(i) = kc2(kccount);
            kccount = kccount + 1;
        else

        end
        
    end
    
    % create all subject indices
    lengamma = length(gamma(:, 1));
    c2Indices = zeros(lengamma, length(c2base));
    for i = 1:length(gamma(:, 1))
    
        c2 = c2base + gamma(i, :);
        c2Indices(i, :) = c2;
        
    end
    
    
    
function ctargIndices = bsplinen_getCTargets(polybasis, ssvcol, m)


    ctargIndices = [];

    % now put all values in the polybasis with the value 'm' at the
    % position of 'mpos' in controlIndices.
    for i = 1:length(polybasis(:, ssvcol))
        if (polybasis(i, ssvcol) == m)
            ctargIndices = [ctargIndices; polybasis(i, :)];
        end
    end
    
    
function [continuityConditions, Cfilter] = bsplinen_continuityConditions(polybasis, edge, gammas, r, Cfilter, CIndexArr, options)


    % columns: 1 = c1, 2 = c2 + gamma, 3 = gamma
    continuityConditions = cell(1, 3);
    condcount = 1;
   
    size_polybasis = length(polybasis(:, 1));
    
    % Construct the continuity conditions for every order up to and
    % including 'order'.

    for order = 0:r

        % get the precalculated gamma's
        gamma = gammas{order + 1};

        % get the target control net coefficients based on the column of the ssv1
        ctargets = bsplinen_getCTargets(polybasis, edge.cssv1, order);

        % get the subject coefficients in simplex1
        for i = 1:length(ctargets(:, 1))

            c1 = ctargets(i, :);
            [c1index c1indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex1, c1);
            % get all subject control net coefficients
            csubjects = bsplinen_getCSubjects(polybasis, edge.cssv1, edge.cssv2, c1, gamma);            
            [c2index c2indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex2, csubjects);

            % store the calculated data in continuity conditions list
            continuityConditions{condcount, 1} = order;
            continuityConditions{condcount, 2} = c1;
            continuityConditions{condcount, 3} = c1index;
            continuityConditions{condcount, 4} = c1indexlocal;
            continuityConditions{condcount, 5} = csubjects;
            continuityConditions{condcount, 6} = c2index;
            continuityConditions{condcount, 7} = c2indexlocal;
            continuityConditions{condcount, 8} = gamma;

            condcount = condcount + 1;

        end

    end
    
    
    
    
    