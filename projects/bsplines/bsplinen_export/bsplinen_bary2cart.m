% BSPLINEN_BARY2CART.m
%
%   Syntax:
%       X = bsplinen_bary2cart(simplex, lambda)
%
%   Description:
%       Function converts the barycentric coordinates of LAMBDA with
%       respect to the vertices of SIMPLEX to (global) cartesian
%       coordinates X 
%
%       Input parameters are: 
%           - simplex   : double [n + 1 x n]
%           - lambda    : double [Npoints  x n + 1]
%
%           SIMPLEX contains the vertex coordinates of a simplex in n
%           space. The rows in SIMPLEX are the vertices, the columns the
%           coordinates. Therefore SIMPLEX always has n+1 rows and n
%           columns. 
%
%           LAMBDA a matrix holding barycentric coordinates with respect to
%           SIMPLEX. LAMBDA must have n+1 columns, with n the dimension of
%           SIMPLEX. 
%
%       Output from BSPLINEN_BARY2CART is:
%           - X         : double [Npoints x n]
%
%           X the global cartesian coordinates of the point LAMBDA. When
%           LAMBDA contains only positive values, X is within the convex
%           hull of SIMPLEX. If LAMBDA contains one or more negative
%           values, then X is outside of the convex hull of SIMPLEX.
%
%   -------------------------------
%   File part of ALW spline toolbox
%   C.C. de Visser & E. de Weerdt
%   07-08-2009
%   -------------------------------
function X = bsplinen_bary2cart(simplex, lambda)

%   Determine number of data points and initialize X
Lcount  = size(lambda, 1);
X       = zeros(Lcount, size(lambda, 2) - 1);

%   Extract the vectices of the simplex
v0      = simplex(1, :);
vcount  = size(simplex, 1);

%   Perform the transformation.
for i = 2:vcount
    X = X + lambda(:, i) * (simplex(i, :) - v0);
end
X = X + ones(Lcount,1)*v0;

return
%--------------------------- end of file ----------------------------------