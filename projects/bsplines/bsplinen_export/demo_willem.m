close all;
clear all;
clc;

plotting = true;

%% Initialization
load('psf.mat'); % Loads PSFS variable (64x64x64). Contains 64 different values for defocus.
c = 1; % index in PSF defocus

bsplinen_structures;
rand('twister', 101);

% polynomial polybasis properties
n = 2; % spline dimension
d = 4; % polynomial degree
r = 0; % continuity order


%% Setup data

Z = zeros(64^2, 1);
XI = zeros(64^2, 2);
i = 1;
for rr = 1:64
  for cc = 1:64
      Z(i) = PSFS(c, rr, cc);
      XI(i, :) = [rr / 64. cc / 64.];
      i = i + 1;
  end
end


XIeval = XI;
Zeval = Z;


%% Type I triangulation
N = 15;

PHI = zeros(N*N,2);
TRI = zeros((N-1)*(N-1)*2,3);
i = 1;
j = 1;
for yy=1:N
    for xx=1:N
        PHI(i, :) = [(xx-1)/(N-1) (yy-1)/(N-1)];
        if yy ~= N
            if xx ~= N
                TRI(j, :) = [i i+1 i+N+1];
                j = j + 1;
            end
        end
        
        if yy ~= 1
            if xx ~= N
                TRI(j, :) = [i i+1 i-N];
                j = j + 1;
            end
        end
        i = i + 1;
    end
end

        
TRI = sortrows(TRI); % sort vertex indices
TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)

% the number of simplices
T = size(TRI, 1);

%%       Estimate B-coefficients
disp('Building B coeffs matrices');
tic;

polybasis = bsplinen_constructBasis(n, d);
dhat      = size(polybasis, 1);

% create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI}, {});

% create the matrix holding the smoothness constraints
H = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, r);
% Href = rref(H);

% create the regression matrix for the given dataset, triangulation, and degree.
[X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis);
toc

% Use iterative solver to find b coefs
disp('Running iterative solver');
tic;
Q = X' * X;
F = X' * Y;
options = struct_bsplinen_options;
options.solver_outputmsg = 1;

bcoefs = bsplinen_iterateC(Q, H, F, [], [], options);
toc

% Construct spline function structure
spline = struct_bsplinen;
% Set the bsplinen structure properties
spline.PHI        = PHI;
spline.TRI        = TRI;
spline.degree     = d;
spline.continuity = r;
spline.dim        = n;
spline.coefs      = bcoefs;



%%       Evaluate spline function
disp('Evaluating spline');

tic;
[XIeval, Zeval, Beval] = bsplinen_evalMat(XIeval, Zeval, spline);
val_spline = Beval * spline.coefs;
toc



err_spline      = Zeval - val_spline;
rms_spline      = sqrt(mse(err_spline));
rmsrel_spline   = rms_spline / sqrt(mse(Zeval));

p_errors = abs(err_spline) ./ val_spline;
fprintf('Max error %.2f', max(p_errors) * 100.);



fprintf('\n---------------------------------------------------------------------\n');
fprintf('Spline error RMS = %d, relative error RMS = %2.2f [%s]\n', rms_spline, 100*rmsrel_spline, '%');
fprintf('---------------------------------------------------------------------\n');


%%                       Do the plots
if plotting
    
% calculate locations of B-coefficients
Bcoords = bsplinen_calcBCoefCoordinates(spline);

close all;

%Plot surface plot of current PSF
figure(1)
surf(squeeze(PSFS(c, :, :)));
TRIeval = delaunayn(XIeval);

plotID = 101;
figure(plotID);
set(plotID, 'Position', [1 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
trimesh(TRI, PHI(:, 1), PHI(:, 2), 'Color', 'b');
axis([-.2 1.2 -.2 1.2]);
count = 1;
bind = 1;
plot(XI(:,1), XI(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 0 0],'MarkerSize', 3);% make it look nice
for i = 1:size(Bcoords, 1)
    plot(Bcoords(i,1), Bcoords(i,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 1 0],'MarkerSize', 1);% make it look nice
    Bcoef = sprintf('c_%d_%d_%d', polybasis(bind, 1), polybasis(bind, 2), polybasis(bind, 3));
    bind = bind + 1;
    if (count == 1)
        offset = [-.025 -.045];
    elseif (count == 2)
        offset = [-.08 .035];
    elseif (count == 3)
        offset = [.025 .035];
    end
    %text(Bcoords(i, 1)+offset(1), Bcoords(i, 2)+offset(2), Bcoef);
    if (bind > dhat)
        count = count + 1;
        bind = 1;
    end
end
for i = 1:size(TRI, 1)
    str = sprintf('t_{%d}', i);
    xytext = bsplinen_bary2cart(PHI(TRI(i, :), :), [1/3 1/3 1/3]);
    text(xytext(1), xytext(2), str);
end
for i = 1:size(PHI, 1)
    str = sprintf('v_{%d}', i);
    text(PHI(i, 1)+.025, PHI(i, 2), str);
end
xlabel('x');
ylabel('y');
titstr = sprintf('Triangulation consisting of %d simplices', T);
title(titstr);

plotID = 1001;
figure(plotID);
set(plotID, 'Position', [500 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_spline);
maxz = max(val_spline);
trisurf(TRIeval, XIeval(:,1), XIeval(:,2), val_spline);
%poslight = light('Position',[1.5 2.5 7],'Style','local');
%material([.3 .8 .9 25]);
%lighting phong;
%shading interp;
view(-23, 36);
end

%% Chi square test
num_photons = 3e6;
val_clipped = max(val_spline, 0.);

expected = num_photons * Zeval / sum(Zeval);
measured = num_photons * val_clipped / sum(val_clipped);

chi = 0;
for i=1:length(val_clipped)
    chi = chi + (expected(i) - measured(i))^2 / expected(i);
end

chi
chi2cdf(chi, length(expected) - 1, 'upper')

