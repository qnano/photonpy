
% BSPLINEN_ITERATEC iteratively finds a solution to the Lagrangian
%   non-symmetric saddle point problem.
% 
%                          Author: C.C. de Visser, 2007
%   
%   The iterative method was proposed by [Awanou et al., 2005] and [Awanou
%   and Lai, 2005]. It iteratively solves the non-symmetric Lagrangian saddle
%   point problem:
%           
%       |B^T A| = |F|
%       | 0  B|   |G|
%
%   The iteration is terminated when either the solution converges (the
%   gradient of the residue drops below a given threshold), the residue
%   itself drops below a given threshold or the maximum number of
%   iterations is exceeded. For large system matrices (>2000x2000) the
%   iterative method is faster than the LSQLIN solver.
%
%   Input parameters are:
%
%       A the data location matrix.
%
%       B the smoothness/constraint matrix. When there are no constraints
%           imposed, B equals the smoothness matrix H.
%
%       F the data value vector (allowed to be empty).
%
%       G the constraint value vector (allowed to be empty).
%
%       L0 the initial guess for the Lagrangian multiplier. This vector is
%         allowed to be empty; if so a zero value will be taken as initial guess.
%         the size of L0 should be length(B(:, 1)) x 1.
%
%   Output from BSPLINEN_ITERATEC is:
%
%       C the solution to the Lagrangian saddle point problem; the
%         B-coefficient vector.
%
function [C, iterate] = bsplinen_iterateC(A, B, F, G, L0, options)

    %B, H, fb, [], sparse(length(H(:, 1)), 1), options
    C = [];
    
    if (nargin < 6)
        bsplinen_structures; % load structures
        options = struct_bsplinen_options; % create default options structure
    end
    
    if (isempty(options))
        bsplinen_structures; % load structures
        options = struct_bsplinen_options; % create default options structure
    end  
    
    if (isempty(A) == 1)
        warning('Data Location Matrix A must be non-empty, returning...');
        return;
    end
    if (isempty(F) == 1)
        F = sparse(length(A(:, 1)), 1);
    end

    % if the B matrix is empty we do not need to iterate
    if (isempty(B) == 1)
        C = A \ F;
        iterate = 1;
        return;
    end

    if (isempty(L0) == 1)
        L0 = sparse(size(B, 1), 1);
    end
    
    % set the iterator options
    eps         = options.solver_eps;
    GTOL        = options.solver_gradtol * norm(F,1); % note that GTOL should scale with the norm of F!
    maxIts      = options.solver_maxiter;
    outputmsg   = options.solver_outputmsg;
        
    % start the iterative solving of the posed problem    
    Bt = B';
    K = A + 1/eps * Bt*B; % K = symmetric, positive definite
  
    if (isempty(G) == 1)
        b0 = F - Bt*L0;
    else
        b0 = F + 1/eps*Bt*G - Bt*L0;
    end
    

    % the first C value: C0
    C0 = K \ b0;
    
    iterate = 1;
    oldNormDC = 1e99;
    NormDC = 1e99;
    
    % Iteratively calculate C
    if (outputmsg == 1), fprintf('BSPLINEN_ITERATEC: Starting iterative search for C; threshold gradient = %d\n', GTOL); end
    while iterate > 0
        %tic;
        % calculate the new iterate
        Ac = A * C0;
        if (~isempty(G))
            Ac = Ac + 1/eps * Bt * G;
        end
        Ck = K \ Ac; % most expensive operation, but K = symmetric positive definite, so uses CHOLMOD = very efficient!
                
        % get the difference between old and new B-coefficient set
        dC = abs(C0 - Ck);
        oldNormDC = NormDC;
        NormDC = norm(dC,1);
        
%         % get the worst converging B-coefficient
        if (outputmsg == 1), fprintf('BSPLINEN_ITERATEC(%d): %d Norm DC = %e, old Norm DC = %e\n', iterate, NormDC, oldNormDC);end
        
        % compare the new gradient with the old, and see if we are
        % 1) converging
        % 2) diverging
        % 3) reached the maximum number of iterations
        
        % check if we have converged for ALL B-coefficients
        if (NormDC < GTOL)
            % we have converged towards a (local) minimum
            if (outputmsg == 1), fprintf('BSPLINEN_ITERATEC: Solution converged in %d iterations with Norm DC = %e\n', iterate, NormDC);end
            break;
        end
        % check for divergence
        if (iterate > 1)
            if (oldNormDC < NormDC)
                % the old norm of the difference between new bcoefs and old bcoefs is smaller than the new one indicating divergence
                if (outputmsg == 1), fprintf('BSPLINEN_ITERATEC: Solution passed over minimum in %d iterations with Norm DC = %e\n', iterate, NormDC);end
                break;
            end
        end
        % check for max number of iterations
        if (iterate > maxIts)
            % terminate the iteration when the maximum number of
            % iterations have been exceeded. 
            warning('Maximum number of iterations reached: %d; %d coefficients did not converge, terminating iteration with Norm DC = %e...', maxIts, length(C0), NormDC);
            break;
        end
        
        C0 = Ck;
        
        iterate = iterate + 1;
        %toc;
    end
    
    C = C0;
    
    