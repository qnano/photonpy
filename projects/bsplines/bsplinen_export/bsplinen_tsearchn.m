
% BSPLINEN_TSEARCHN performs a partitioned simplex membership search for the data
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2008
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%
%   [INCLUSIONMAP LAMBDADATA] = BSPLINEN_TSEARCHN(PHI, TRI, XI, PARTSIZE)
%    performs a partitioned simplex membership search like TSEARCHN, but
%    with a built in data partitioning system for large (i.e. N>250000)
%    datasets, which produce out of memory errors when using TSEARCHN. 
%
%   Input parameters of BSPLINEN_TSEARCHN are.
%
%       PHI a matrix holding vertex coordinates, every row is corresponds
%        with a single vertex.
%
%       TRI a tesselation matrix as produced with DELAUNAYN
%
%       XI the data points, each row corresponds with a single observation
%
%       OPTIONS is a BSPLINEN_OPTIONS structure, or alternatively, a [1x1]
%        double holding the size of the partition.
%
%
function [InclusionMap LambdaData] = bsplinen_tsearchn(PHI, TRI, XI, options)
    
    InclusionMap = [];
    LambdaData = [];
    
    if (nargin > 3)
        if (isstruct(options))
            if (isempty(options.data_partsize))
                partsize = 250000;
            else
                partsize = options.data_partsize;
            end
        else
            bsplinen_structures;
            partsize = options;
            options = struct_bsplinen_options;
        end
    else
        bsplinen_structures;
        options = struct_bsplinen_options;
        partsize = options.data_partsize;
    end
    
    % Check whether a parallel Matlab session should be started
    if (options.parallelCores)
        % check whether some matlab pool is already running
        if (matlabpool('size') == 0)
            try
                % nope, so fire up some Matlab pools
                matlabpool(options.parallelCores);
            catch
            end
        end
    end
    
    % maximum partition size
    XIsize = size(XI, 1);

    % determine DELTA
    if (XIsize <= partsize || partsize < 1)
        % Dataset XI is small enough to handle all at once
        if (options.native_tsearchn)
            try
                addpath(options.native_dir);
                % initialize and run native membership search
                [InclusionMap LambdaData] = cbsplinen_tsearchn(PHI, TRI-1, options.native_gridres, XI);
                InclusionMap = InclusionMap + 1;
                % clean environment
                cbsplinen_tsearchn();
            catch
                warning('Could not run external function <cbsplinen_tsearchn>, returning');
                try
                    % clean environment
                    cbsplinen_tsearchn();
                catch
                end
                return;
            end
        else
            [InclusionMap LambdaData] = tsearchn(PHI, TRI, XI);
        end
    else
        xistart = 1;
        xiend = xistart + partsize;
        partcount = ceil(XIsize / partsize);
        n = size(XI, 2);
        %if (options.outputmsg == 1), disp(sprintf('BSPLINEN_TSEARCHN: Dataset is partitioned in to %d partitions of size %d.', partcount, partsize));end
        InclusionMap = zeros(XIsize, 1);
        LambdaData = zeros(XIsize, n+1);
        % perform parallel membership search, if parallelCores > 0
        if (options.parallelCores == 0)
            disp(sprintf('BSPLINEN_TSEARCHN: Dataset is partitioned in to %d partitions of size %d.', partcount, partsize));
 
            if (options.native_tsearchn)
                try
                    addpath(options.native_dir);
                    % initialize and run native membership search
                    [InclusionMap LambdaData] = cbsplinen_tsearchn(PHI, TRI-1, options.native_gridres, XI);
                    InclusionMap = InclusionMap + 1;
                    % clean environment
                    cbsplinen_tsearchn();
                catch
                    warning('Could not run external function <cbsplinen_tsearchn>, returning');
                    try
                        % clean environment
                        cbsplinen_tsearchn();
                    catch
                    end
                    return;
                end
            else
                for i = 1:partcount
                    %if (options.outputmsg == 1), disp(sprintf('BSPLINEN_TSEARCHN: Performing simplex membership search for data partition %d...', i));end
                    disp(sprintf('BSPLINEN_TSEARCHN: Performing simplex membership search for data partition %d...', i));
                    [IMaptmp LambdaTmp] = tsearchn(PHI, TRI, XI(xistart:xiend, :));
                    InclusionMap(xistart:xiend, :) = IMaptmp;
                    LambdaData(xistart:xiend, :) = LambdaTmp;
                    xistart = xiend + 1;
                    xiend = xiend + partsize;
                    if (xiend > XIsize)
                        xiend = XIsize;
                    end
                end
            end 
        else
            
            disp(sprintf('BSPLINEN_TSEARCHN: Parallel Membership Search for %d partitions of size %d.', partcount, partsize));
            
            IncCell = cell(partcount, 1);
            LamdaCell = cell(partcount, 1);
            IndexArray = zeros(partcount, 2);
            for i = 1:partcount
                IndexArray(i, :) = [xistart xiend];
                xistart = xiend + 1;
                xiend = xiend + partsize;
                if (xiend > XIsize)
                    xiend = XIsize;
                end
            end
            
            if (options.native_tsearchn)
                try
                    addpath(options.native_dir);
                    parfor i = 1:partcount
                        %disp(sprintf('BSPLINEN_TSEARCHN: Performing simplex membership search for data partition %d...', i));
                        % Initialize and run native membership search
                        [IMaptmp LambdaTmp] = cbsplinen_tsearchn(PHI, TRI-1, options.native_gridres, XI(IndexArray(i, 1):IndexArray(i, 2), :));
                        % clean environment
                        cbsplinen_tsearchn();
                        
                        IncCell{i} = IMaptmp + 1;
                        LamdaCell{i} = LambdaTmp;               
                    end
                catch
                    warning('Could not run external function <cbsplinen_tsearchn>, returning');
                    try
                        % clean environment
                        cbsplinen_tsearchn();
                    catch
                    end
                    return;
                end
            else
                parfor i = 1:partcount
                    %disp(sprintf('BSPLINEN_TSEARCHN: Performing simplex membership search for data partition %d...', i));
                    [IMaptmp LambdaTmp] = tsearchn(PHI, TRI, XI(IndexArray(i, 1):IndexArray(i, 2), :));
                    IncCell{i} = IMaptmp;
                    LamdaCell{i} = LambdaTmp;               
                end
            end
            
            % assemble the output arrays
            for i = 1:partcount
                InclusionMap(IndexArray(i, 1):IndexArray(i, 2), :) = IncCell{i};
                LambdaData(IndexArray(i, 1):IndexArray(i, 2), :) = LamdaCell{i};
            end
            
        end
        
    end
