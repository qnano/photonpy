% BSPLINEN_GETFULLTRIANGULATIONEDGES returns the internal and external edge facets of a triangulation, together
%   with external and internal vertex indices.
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%                          version 2.0
%
%   [TRIedges PHIedges edgeSimps edgeVerts Edges] = BSPLINEN_GETFULLTRIANGULATIONEDGES(TRI, PHI, EDGES, ALLEDGES)
%
%       The principle of the algorithm is that any facet that is not an edge facet, is an 
%       edge of the triangulation.
function [TRIedges PHIedges edgeSimps edgeVerts Edges] = bsplinen_getFullTriangulationEdges(TRI, PHI, Edges, alledges)

    bsplinen_structures;
    
    TRIedges = {};
    PHIedges = {};
    edgeSimps = [];
    edgeVerts = [];
    
    if (nargin < 3)
        % get all edges in the triangulation
        Edges = bsplinen_getEdges(PHI, TRI);
        alledges  = 1;
    elseif (nargin < 4)
        if (isempty(Edges))
            Edges = bsplinen_getEdges(PHI, TRI);
        end            
        alledges = 1;
    elseif (nargin == 4)
        if (isempty(Edges))
            Edges = bsplinen_getEdges(PHI, TRI);
        end            
    end
    
    % if only external edges are required, then return the results from the (simpeler) function bsplinen_getTriangulationEdges.
    if (~alledges)
        [TRIedges PHIedges edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);
        return;
    end
    
    % dimension of TRI
    n = size(PHI, 2);
    
    T = size(TRI, 1);

    
    % compile a list that translates simplices to edges
    SimpEdgeList = cell(T, 1);
    for i = 1:size(Edges, 1)
        edge = Edges{i};
        SimpEdgeList{edge.simplex1} = [SimpEdgeList{edge.simplex1}; i];
        SimpEdgeList{edge.simplex2} = [SimpEdgeList{edge.simplex2}; i];
    end
    
    idxset = (1:n+1)';
    % the permutation vector for the edge facet indices    
    facetidx = nchoosek(idxset, n);

    % the triangulation edges are all edge facets that are not in Edges...
    TRIedges = cell(T, 1);
    PHIedges = cell(T, 1);
    edgeSimps = zeros(T, 1);
    edgeVerts = zeros((n+1)*T, 1);
    
    % go through all simplices
    evcount = 0;
    for i = 1:T
        % vertex indices of simplex i
        simpidx = TRI(i, :);
        % get the facets of simplex i
%         facets = simpidx(facetidx);
        facets = simpidx(facetidx); % CDV 16-01-2012
        
        % The principle is that any facets that are not in any edge structure, are edge facets!

        edgeidx = SimpEdgeList{i};
        if (isempty(edgeidx))
            % no edges, so all facets are edge facets!
            TRIedges{i} = facets;
            PHIedges{i} = PHI(facets', :);
            edgeVerts((evcount+1):(evcount+numel(facets))) = facets(:);
            evcount = evcount + numel(facets); % total number of vertex indices for this facet
            continue;
        end
        
        % Go through all facets and see if they are edge facets
        edgefacetlist = zeros(size(facets, 1), 1);
        edgefacetcount = 1;
        hasedgefacets = 0;
        for fi = 1:size(facets, 1)
            facet = sort(facets(fi, :));
            isedgefacet = 1;
            % go through all edges of simplex 'simp'
            for j = 1:length(edgeidx)
                edge = Edges{edgeidx(j)};
%                 ev = edge.edgevertices;
                ev = sort(edge.edgevertices); % CDV 16-01-2012

                % find the mutual vertex indices in the current facet and the edge vertices
%                 mutualverts = intersect(facet, ev); % slow function
%                 if (length(mutualverts) == n)
%                     % there are n mutual vertices, so this facet is actually an edge facet!
%                     isedgefacet = 0;
%                     break;
%                 end
                if (~any(facet' - ev))
                    % there are n mutual vertices, so this facet is actually an edge facet!
                    isedgefacet = 0;
                    break;
                end
            end
            if (isedgefacet)
                edgefacetlist(edgefacetcount) = fi;
                edgefacetcount = edgefacetcount + 1;
                hasedgefacets = 1;
            end
            
        end
        
        if (hasedgefacets)
            edgefacetlist = edgefacetlist(1:edgefacetcount-1);
            % insert edge facets
            edgefacets = facets(edgefacetlist, :);
            TRIedges{i} = edgefacets;
            PHIedges{i} = PHI(TRIedges{i}', :);
            edgeSimps(i) = 1;
            edgeVerts(evcount+1:evcount+numel(edgefacets)) = edgefacets(:);
            evcount = evcount + numel(edgefacets); % total number of vertex indices for this facet
        end
        
        
    end
    
    % cutoff the edge vertex list, and make it unique
    edgeVerts = unique(edgeVerts(1:evcount));

