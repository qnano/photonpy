
% BSPLINEN_GETSTAR gets the structure of simplices all sharing a single
%   vertex. 
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2008
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.0
%
%   STAR = BSPLINEN_GETSTAR(PHI, TRI, V) determines the structure of
%       simplices all sharing a single vertex. This structure is called the
%       Star of vertex V (see e.g. [Spline Functions on Triangulations, 2007].
%
%   Input parameters are:
%
%       TRI the triangulation holding vertex indices as columns and simplex
%           indices as rows.
%
%       V the vertex index of the vertex for which the Star will be determined
%
%   Output of BSPLINEN_GETSTAR is:
%
%       STAR an index vector or cell array of index vectors.
%           When V is a single vertex then STAR is an index vector into TRI 
%           holding all simplex indices that form the Star of V. 
%           When V is an array containing multiple vertices, STAR is a cell array where
%           every index in star corresponds with the vertex V(i).
%
function Star = bsplinen_getStar(TRI, V)

    if (length(V) == 1)
        [Star cind] = find(TRI == V);
    else
        Star = cell(length(V), 1);
        for i = 1:length(V)
           [star cind] = find(TRI == V(i));
           Star{i} = star;
        end
    end
        
    