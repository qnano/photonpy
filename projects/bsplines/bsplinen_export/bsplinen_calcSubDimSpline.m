
% bsplinen_calcSubDimSpline calculates the B-coefficients of a spline function
%   of dimension n-m > 0 given an input spline function of dimension n, and
%   and output spline function of dimension m < n. Note: this function uses
%   the output from bsplinen_calcSubDimPoly.m to generate the per-simplex
%   transform matrix Tj_MD_ND.
% 
%                          Author: C.C. de Visser, 2015
%   
%   Description:
%       This function takes an n-D spline function as input (splineND) together with an m-D
%       spline function prototype, with m<n, indicated as splineMD. That is, the 
%       splineMD is a spline function of lower dimension than splineND. From theory,
%       we find that splineMD.coefs \in splineND.coefs. This means there must exist
%       a mapping between splineMD.coefs and splineND.coefs. This function aims to determine
%       this mapping. In particular, if:
%           s_nD(x) = B_nD(x)c_nD
%           s_mD(x) = B_mD(x)c_mD
%       then there exists a transformation matrix T_MD_ND \in R^[dhat_mD x dhat_nD] such that:
%           c_mD = T_MD_ND * c_nD.
%
%   Inputs:
%       splineND: [struct_bsplinen] structure; this is the n-D spline from which
%           a slice is taken resulting in splineMD.
%
%       splineMD: [struct_bsplinen] structure; this is the m-D spline with m<n 
%           which is a sub-dimensional spline.
%
%   Outputs:
%       T_MD_ND: [dhat_mD x dhat_nD] matrix; transformation matrix such that:
%           s_nD(x) = B_nD(x)c_nD
%           s_mD(x) = B_mD(x)c_mD
%           c_mD = T_MD_ND * c_nD.
%       splineMD: [struct_bsplinen] structure, same as input, but now with B-coefficients
%           determined using the transformation matrix:
%           c_mD = T_MD_ND * c_nD.
%
function [T_MD_ND, splineMD] = bsplinen_calcSubDimSpline(splineND, splineMD)

    
    T_MD_ND = [];
    