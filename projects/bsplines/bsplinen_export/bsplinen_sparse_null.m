%   sparse_null.m
%
%   description:
%       function creates a sparse basis for the null space of A using a
%       recursive algorithm
%
%   Copyright: H.J.Tol, C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%                     h.j.tol@tudelft.nl                  
%                                                                                       
%


function nullA = sparse_null(A)


numcol = size(A,2); %number of columns

%bound on nonzero values for nummerical stability
epsilon = 0.0000001; 
%number of states removed
count = 0;

for i =1:size(A,1)    
    
    %check if the current row contains nonzero indices
    if isempty(find(A(1,:)<-epsilon | A(1,:)>epsilon));
        A = A(2:end,:); 
        continue
    end      
    %number of removed states + 1
    count = count+1;  
    
    %find the index of the first non-zero entry in A(i,:);
    ix = find(A(1,:)<-epsilon | A(1,:)>epsilon); %for nummerical stability
    ix = ix(1);  
    
    %construct transformation V_i such that A(i,:)x= A(i,:)*V_i*x'_i = 0       
    V_i = eye(numcol - count);  %initialize the transformation for row i 
    
    %construct new row such that x(i) = newrow x'_i
    newrow = zeros(1,numcol-count);  %initialize 
    newrow(1,ix:end) = (-1/A(1,ix))*A(1,ix+1:end);
    
    %insert row
    V_i = insertrow(V_i, newrow, ix);
    
    if i ==1 % initialize nullA
        nullA = V_i;
    else %update recursively
        nullA = nullA*V_i;        
    end
    
    %remove first row
    A = A(2:end,:);
    
    %apply transformation
    A = A*V_i;       
end

end

%function inserts a new row in A at the location specified by index 
function An = insertrow(A,newrow,index)
    An              = zeros(size(A,1)+1, size(A,2));
    An(1:index-1,:)	= A(1:index-1,:);
    An(index,:)     = newrow;
    An(index+1:end,:) = A(index:end,:);
end
