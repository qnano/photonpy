% bsplinen_constructCauchyConstraints a matrix holding Cauchy boundary conditions of a given degree normal to the triangulation edges
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%              version 1.0

function [DC, dc, EdgeCoefs] = bsplinen_constructCauchyConstraints(TRI, PHI, d, order, constraintval, fullrank)

    method = 'bcoefs';
    
    if (nargin < 6)
        fullrank = 0;
    end
    
    % the differential constraint matrix
    DC = [];
    % the differential constraint values
    dc = [];

%     d = fn.degree;
%     n = fn.dim;
%     T = size(TRI, 1);
    n = size(PHI, 2);
    T = size(TRI, 1);
    dhat = bsplinen_calcdHat(n, d);
    
    % total number of constraints for given order, dimension and degree
    numConstraints = factorial(d - order + n - 1)/(factorial(n-1)*factorial(d-order));

    % return the edges of the triangulation
    [TRIedges, PHIedges, edgeSimps] = bsplinen_getTriangulationEdges2(TRI, PHI);
    
    % obtain normal vectors to all triangulation edges
    Normals = bsplinen_calcEdgeNormals(TRI, PHI, PHIedges);
    
    % obtain B-coefficients on the triangulation edges
    EdgeCoefs = bsplinen_getBCoefsOnFacets(TRI, PHI, TRIedges, d);    
    
    % construct the full differential constraint matrix by combining differential constraints for each simplex edge
    for i = 1:length(TRIedges)
        if (isempty(TRIedges{i}))
            continue;
        end
        trie = TRIedges{i};
        norme = Normals{i};
        EC = EdgeCoefs{i};
        
        % test whether we should build differential constraints, or ordinary constraints
        if (order > 0)
            % Construct Cauchy boundary conditions
            % some simplices may have multiple external edges
            for k = 1:size(trie, 1)

                % retrieve normal vector to TRI edge
                Udiff = norme(k, :);
                % retrieve B-coef coordinates
                Xconstraints = EC{k}(:, [3 end]);

                % construct the differential constraint matrix
                DCj = bsplinen_constructDifferentialMatrix3(TRI, PHI, d, Xconstraints, Udiff, order);
                dcj = constraintval*ones(size(DCj, 1), 1);
                DC = [DC; DCj];
                dc = [dc; dcj];
            end
            
        else
            % set Dirichlet boundary conditions (i.e. directly bound PDE solution)
            for k = 1:size(trie, 1)
                totalBcoefs = size(EC{k}, 1);
                DCj = sparse([1:totalBcoefs]', EC{k}(:,2), 1, totalBcoefs, T*dhat);
                dcj = constraintval*ones(size(DCj, 1), 1);
                DC = [DC; DCj];
                dc = [dc; dcj];
            end
        end

    end
        

    % make DC of full rank (slow)
    if (fullrank)
        if (~isempty(DC))
            DCorig = DC;
            DCrref = rref([DC dc]);
            rankDC = rank(DCrref);
            DC = DCrref(1:rankDC, 1:end-1);
            dc = DCrref(1:rankDC, end);
        end
    end
    %fprintf('Required n/o constraints: %d, current n/o constraints: %d (d=%d, m=%d)\n', numConstraints*length(constrainedTRI), length(dc), d, m);

    
    
    
    