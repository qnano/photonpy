%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Construct de Casteljau matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ndata = size(XI, 1); 

Bx = sparse(Ndata, dhatA*T);
By = sparse(Ndata, dhatA*T);
Zx = zeros(Ndata, 1);
Zy = zeros(Ndata, 1);
Zidx = zeros(Ndata, 1);
if (dA > 1 && d > 1  && useCurvatureData)
    Bx2 = sparse(Ndata, dhatA*T);
    By2 = sparse(Ndata, dhatA*T);
    Zx2 = zeros(Ndata, 1);
    Zy2 = zeros(Ndata, 1);
    Zidx2 = zeros(Ndata, 1);
end

% Go through all simplices, and construct the de Casteljau matrix for each
partdatacount = 0;
partdatacountC = 0;
for t = 1:T
    simpt = PHI(TRI(t, :), :);
    simplex = DELTA{t};
    ndatat = IData(t, 3);
    newpartdatacount = partdatacount + ndatat;

    % Calculate the barycentric coordinates of U with respect to simplex t.
    Ux = [1 0];
    Uy = [0 1];
    nZx = simpt(1,:) - Ux; % normalized relative direction
    nZy = simpt(1,:) - Uy; % normalized relative direction
    alpha = bsplinen_cart2bary(simpt, simpt(1,:));
    betax = bsplinen_cart2bary(simpt, nZx);
    betay = bsplinen_cart2bary(simpt, nZy);
    % the directional coordinate for simplex t
    ax = alpha - betax;     
    ay = alpha - betay;   

    % Build the de d-to-(d-1) de Casteljau matrix using the directional coordinates ax and ay
    CastelX1 = bsplinen_constructDeCasteljauMatExt(ax, dA);
    CastelY1 = bsplinen_constructDeCasteljauMatExt(ay, dA);                    
    % Determine value of B-matrix for d > 1 (nonlinear WFR)
    Btk = ones(ndatat, 1);
    Islopest = IData((t), 1):IData((t), 2);
    if (dA > 1)
        bdat = bsplinen_cart2bary(simpt, DataLocations(Islopest, :));
        Btk = derivmultfact * bsplinen_constructBtk(polybasisM1, basisKtM1, bdat);
    end
    % Construct slopes regression matrix
    Bx((partdatacount+1):newpartdatacount, dhatA*(t-1)+1:dhatA*t) = Btk * CastelX1;
    By((partdatacount+1):newpartdatacount, dhatA*(t-1)+1:dhatA*t) = Btk * CastelY1;
    % Compile the slope dataset
    Zx((partdatacount+1):newpartdatacount)                      = OBJspline_dx_val(Islopest);
    Zy((partdatacount+1):newpartdatacount)                      = OBJspline_dy_val(Islopest);
    % Save the indices into the slope vector
    Zidx((partdatacount+1):newpartdatacount)                    = Islopest;

    % if there is curvature data, include it in the regression
    if (dA > 1 && d > 1 && useCurvatureData)
        ndatatC = IData((t), 3);
        newpartdatacountC = partdatacountC + ndatatC;

        % Build the d-to-(d-2) de Casteljau matrix using the directional coordinates ax and ay
        CastelX2 = bsplinen_constructDeCasteljauMatExt(ax, dA-1) * CastelX1;
        CastelY2 = bsplinen_constructDeCasteljauMatExt(ay, dA-1) * CastelY1;
        % Determine value of B-matrix for d > 1 (nonlinear WFR)
        Btk2 = ones(ndatatC, 1);
        Icurvest = IData((t), 1):IData((t), 2);
        if (d-1 > 1)
            bdat2 = bsplinen_cart2bary(simpt, DataLocations(Icurvest, :));
            Btk2 = derivmultfact2 * bsplinen_constructBtk(polybasisM2, basisKtM2, bdat2);
        end

        % Construct curves regression matrix
        Bx2((partdatacountC+1):newpartdatacountC, dhatA*(t-1)+1:dhatA*t) = Btk2 * CastelX2;
        By2((partdatacountC+1):newpartdatacountC, dhatA*(t-1)+1:dhatA*t) = Btk2 * CastelY2;
        % Compile the curve dataset
        Zx2((partdatacountC+1):newpartdatacountC)                      = OBJspline_dx2_val(Icurvest);
        Zy2((partdatacountC+1):newpartdatacountC)                      = OBJspline_dy2_val(Icurvest);
        % Save the indices into the slope vector
        Zidx2((partdatacountC+1):newpartdatacountC)                    = Icurvest;
        partdatacountC = newpartdatacountC;
    end

    partdatacount = newpartdatacount;

end
