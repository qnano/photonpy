%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% demo_DSABRE solves time invariate first and second order differential equations
%   based on measurements made on the first and second order directional derivatives of the
%   solution function.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.0
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


close all;

% load structures from disk
bsplinen_structures;

% init random number generator
rand('twister', 101);

printfigs           = 0; % print figures switch

% polynomial polybasis properties
n = 2; % spline dimension
d = 1; % polynomial degree
r = 0; % continuity order


ParallelCores       = 0;

MaxIterations       = 200; % maximum number of iterations for the dual ascent method
MinInnovation       = 1e-9; % minimum innovation; dual ascent will terminate below this threashold

simpdatacount       = 20; % data content per simplex
noiseMag            = 0;

doAnimation         = 1;

PartitionCountSet   = [2 2];
TRIgridres          = 8; % number of vertices used in triangulation
TRItype             = 1; % number of vertices used in triangulation
StarLevelSet        = 1;
StarMode            = 1; % 0: star of anchor- and control simplices are taken, 1: star of all edge simplices is taken

evalres             = 50; % resolution for eval
idres               = 100; % resolution for data generation using Turbulence data

efficientcalculation = 1; % 0: pseudo inverse estimator is used, 1: direct solver using constraint reduced regression matrix

% view angle and elevation
viewaz = -132;
viewel = 48;

figpath = '.\figures\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set spline function options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (ParallelCores)
    % check whether some matlab pool is already running
    if (matlabpool('size') == 0)
        try
            % nope, so fire up some Matlab pools
            matlabpool(ParallelCores);
        catch
        end
    end
else
    if (matlabpool('size') > 0)
        matlabpool close;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set spline function options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
NParts = prod(PartitionCountSet);

if (NParts == 1)
    StarLevelSet = 0;
end

bopts = struct_bsplinen_options;
bopts.outputmsg             = 0;
bopts.solver_maxiter        = 200;
bopts.solver_tol            = 1e-6;
bopts.solver_gradtol        = 1e-10;
bopts.solver_eps            = 1e-6;
bopts.solver_doplots        = 0;
bopts.solver_outputmsg      = 0;
bopts.solver                = 'iterative';%'ls''iterative''direct';
bopts.filterH               = 1;
bopts.saveH                 = 0;
bopts.loadH                 = 0;
bopts.fullrankH             = 0;
bopts.sortedH               = 1;
bopts.outputH               = 0;
bopts.outputBfb             = 0;
bopts.stat_outputX          = 0;
bopts.stat_outputCOV        = 0;
bopts.stat_calc             = 0;
bopts.stat_COVmethod        = 'LS';
bopts.saveMatPath           = '.\\Matrix\Temp';
bopts.tri_partitioncount    = PartitionCountSet(1,:);
bopts.tri_partitionmode     = 'symgrid'; % symmetric grid partition
bopts.distrib_starlevel     = StarLevelSet;
bopts.distrib_starmode      = StarMode; % 0: star of anchor- and control simplices are taken, 1: star of all edge simplices is taken
bopts.distrib_mode          = 1; % 1: simple per simplex data averaging, 2: inclusion of all measurements in LS problem
bopts.distrib_smoothmode    = 2; % 1: piston removal by averaging single B-coefs, 2: piston removal by averaging over multiple overlapping simplices
bopts.distrib_symmetricpart = 1; % 0: assymetric partitioning, 1: symmetric partitioning
bopts.parallelCores         = 0;
bopts.native_eval           = 1;
bopts.native_eval_newbcoefs = 1;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Calculate polynomial basis function multi-index permutations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

polybasis = bsplinen_constructBasis(n, d);
dhat      = size(polybasis, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Construct triangulation in ([0,1],[0,1])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

XItmp = [0 0; 1 1;];
[TRI PHI] = bsplinen_CDVtriangulateExt(XItmp, TRIgridres, TRItype);
% the number of simplices
T = size(TRI, 1);

[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI});

fprintf('Total number of simplices: %d\n', T);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% turbdatafilename        = 'TurbulenceSet_R10_N100.mat';
% turbdatafilename        = 'TurbulenceSet_R20_N100.mat'; 
turbdatafilename        = 'TurbulenceSet_R33_N100.mat'; 
% turbdatafilename        = 'TurbulenceSet_R50_N100.mat'; 

% figpath = './figures/';
% figpath     = 'D:\ccdevisser\Postdoc\Publications\splineWFR\splineWFR_v1\figures\';
figpath = 'D:\ccdevisser\UD\Publications\distribsplineWFR\distribsplineWFR_CDC2013\figures\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Data generating functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% fx = 'x1.^2.*sin(5*x1 - 5*x2.^2)';
% fx = 'x1.^2.*cos(10*x1 - 15*x2)';
fx = 'exp(-(15*(x1-.5).^2 + 10*(x2-.5).^2))';
%fx = 'x1.^2.*cos(10*x1 - 15*x2) - x2.^2.*sin(10*x1 + 15*x2)';
%fx = '(x1-1).^2.*cos(10*(x1-1) - 15*(x2-1)) - (x2-1).^2.*sin(10*(x1-1) + 15*(x2-1))';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evaluation locations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
% evaluation locations (square grid)
[xxe yye] = ndgrid((0:1/(evalres-1):1)', (0:1/(evalres-1):1)');
XIeval = [xxe(:), yye(:)];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Dataset generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate the data locations
XI = zeros(T*simpdatacount, 2);

% use barycentric coordinate rules to create uniformly distributed random dataset inside each simplex
for i = 1:T
    % generate data in barycentric coordinates
    Set = rand(simpdatacount, 3);
    % normalize the barycentric coordinates
    sumSet = sum(Set, 2);
    Set = Set./[sumSet sumSet sumSet];
    % transform to cartesian coords
    Xt = bsplinen_bary2cart(PHI(TRI(i, :), :), Set);

    XI((i-1)*simpdatacount+1:i*simpdatacount,:) = Xt;
end   

% function values
x1 = sym('x1');
x2 = sym('x2');

% create identification dataset with some matlab magic
x1 = XI(:,1);  x2 = XI(:,2);
Z = eval(fx);

% Z = Z + noiseMag*range(Z)*(rand(size(Z, 1), 1)-0.5);
Z = Z + noiseMag*abs(Z).*(rand(size(Z, 1), 1)-0.5);

% create evaluation dataset
x1 = XIeval(:,1);  x2 = XIeval(:,2);
Zeval = eval(fx);
    


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Estimate B-coefficients of Global model for comparison
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI});

% create the matrix holding the smoothness constraints
[H IHedges] = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, r, bopts);

% create the regression matrix for the given dataset, triangulation, and degree.
[X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis);

% dispersion matrix construction
B = X'*X;
% construct complete KKT matrix
M = full([B H'; H sparse(size(H, 1), size(H, 1))]);

% simple constrained least squares estimator for B-coefficients & Lagrange multipliers (very inefficient!)
bcoefs_Lmult = pinv(M) * [X'*Y; zeros(size(H,1), 1)];

bcoefs       = bcoefs_Lmult(1:size(B, 1)); % bcoeffice

% Construct spline function structure
spline = struct_bsplinen;
% Set the bsplinen structure properties
spline.PHI        = PHI;
spline.TRI        = TRI;
spline.degree     = d;
spline.continuity = r;
spline.dim        = n;
spline.coefs      = bcoefs;

BCoords = bsplinen_calcBCoefCoordinates(spline);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Create distributed spline structure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[DSPLINES GlobalSpline PartitionData H EDGES] = bsplinen_createDistributedSpline(PHI, TRI, d, r, bopts);
GlobalSpline0 = GlobalSpline;
NParts = size(PartitionData, 1);


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Estimate initial values for Distributed splines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% calculate unconstrained solution for each partition
AnimationData = cell(NParts, 1);

for i = 1:NParts  
    % NOTE: For further optimization, the bsplinen_reduceRegressionMat can be used to remove constraints from the local equations.
    %       However, this requires a new function that only partly removes smoothness constraints. 
    %       This is because the B-coef innovation innovationck = Hsub(:, Iglobal)' * ykT; must be calculated, for all B-coefs
    %       that are in continuity conditions that connect partitions.
    %       This can be done by not removing any smoothness constraints which contain B-coefficient indices that 
    %       are in the list IAllglobalcontrolRn!
    spl = DSPLINES{i}.spline0;
    spltmp = spl;
    spltmp.TRI = [spl.TRI; PartitionData{i, 5}]; 
    [imap bdat] = tsearchn(spltmp.PHI, spltmp.TRI, XI);
    idx = find(~isnan(imap));
    if (~isempty(idx))
        % LS estimation 
        [Xpart, Zpart] = bsplinen_genregExt(XI(idx, :), Z(idx), spltmp.PHI, spltmp.TRI, polybasis);
        XptXp = Xpart' * Xpart;
        spltmp.coefs = (XptXp)\Xpart'*Zpart; % LS estimator
    end
    spl.coefs           = spltmp.coefs(1:size(spl.TRI, 1)*dhat);
    
    DSPLINES{i}.spline0 = spl;
    DSPLINES{i}.spline  = spl;
    GlobalSpline0.coefs(DSPLINES{i}.Iglobal) = spl.coefs;
    
    if (doAnimation)
        [imap bdat] = tsearchn(spl.PHI, spl.TRI, XIeval);
        idxe = find(~isnan(imap));
        trixi = delaunayn(XIeval(idxe, :));
        AnimationData{i, 1} = idxe;
        AnimationData{i, 2} = trixi;
    end

end
% set neighbor splines coefs
for i = 1:NParts
    if (~isempty(DSPLINES{i}.Icontrolparts))
        DSPLINES{i}.NBspline = DSPLINES{DSPLINES{i}.Icontrolparts}.spline;
    end
end

% evaluate local spline partitions
DSABREResults = cell(i, 3);
for i = 1:NParts
    DSABREResults{i,1} = bsplinen_evalExt(DSPLINES{i}.spline0, XIeval);
end

DSPLINES0 = DSPLINES;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Test Dual Ascent method in PARALLEL mode: Stage 2: Inter-partition smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
GlobalSpline = GlobalSpline0;

% test initial norm of H*c
testInitialResHKc = H * GlobalSpline.coefs;
rmsInitialResHKc  = sqrt(mse(testInitialResHKc));

alpha = 5e-1;%20e-2; % 20e-2, 10e-2 produce good results; alpha is the weight of the dual increment
itt = 1;
plotID = 2;
close all;
figure(plotID);
set(plotID, 'Position', [600 150 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
terminatecount = 0;
SharedDataStruct = cell(NParts, 1);
tic;
while (itt < MaxIterations)
    % uncomment the following for per-partition smoothing
    hold off;
    for i = 1:NParts
        DSPL        = DSPLINES{i};
        spl0        = DSPL.spline0; 
        spl         = DSPL.spline;
        HsubLocal   = DSPL.HsubLocal;
        Iglobal     = DSPL.Iglobal; 
        ykT         = DSPL.dualC;

        % Calculate B-coefficient innovation
        innovationck = HsubLocal' * ykT;
        spl.coefs    = spl0.coefs - innovationck; 
       
        % For this step we need a shared data structure from wich the write is done in a bounded nonoverlapping partition of the data struct.             
        % SharedDataStruct{i} = spl.coefs; % SharedDataStruct acts as shared data structure
        GlobalSpline.coefs(Iglobal) = spl.coefs; % Should be removed for distributed form       
        DSPL.spline = spl;
        DSPLINES{i} = DSPL;
    
        if (doAnimation)
            idx = AnimationData{i, 1};
            trixi = AnimationData{i, 2};
            res = bsplinen_evalExt(spl, XIeval(idx, :));
            trisurf(trixi, XIeval(idx, 1), XIeval(idx, 2), res);
            hold on;
        end
    end
    % gather data and update duals
    for i = 1:NParts
        DSPL            = DSPLINES{i};
        HsubCo          = DSPL.HsubCo;
        Ic              = DSPL.IAllglobalcontrolRn;
        ykpT            = DSPL.dualC;
        mseinnovationyk = DSPL.dualInnovationMSE;
        
        if (mseinnovationyk < MinInnovation)
            if (DSPL.convergedH == 0)
                DSPL.convergedH = 1;
                terminatecount = terminatecount + 1;
            end
            continue;
        end
        
        % Calculate Dual innovation for the current partition and its direct neighbors
        % translate global Ic to local B-coef indices. For this step we need a shared data structure from wich the read is done 
        % on an overlapping part of the shared data structure.
        Hc           = HsubCo * GlobalSpline.coefs(Ic); % Has to be translated into distributed form
        innovationyk = alpha * Hc; 
        ykpT         = ykpT + innovationyk; 
        
%         mseinnovationyk0 = mse(innovationyk);  % MSE is very slow
        mseinnovationyk = 1/length(innovationyk) * (innovationyk'*innovationyk);  % MSE is very slow
        
        DSPL.dualC              = ykpT;
        DSPL.dualInnovationMSE  = mseinnovationyk;
        DSPLINES{i}             = DSPL;

    end
    
    if (doAnimation)
        view(-217,24);
        title(sprintf('Iteration %d', itt));
        pause(.1);
        drawnow;
    end

    if (terminatecount >= NParts)
        break;
    end
        
    itt = itt + 1;
    
end
time2 = toc;
Gitt = itt;

%%
% test = HK * ckp1;
testResHKc = H * GlobalSpline.coefs;
rmsResHKc  = sqrt(mse(testResHKc));

% evaluate local spline partitions
for i = 1:NParts
    DSABREResults{i,3} = bsplinen_eval(DSPLINES{i}.spline, XIeval);
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Evaluate spline function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

val_spline = bsplinen_evalExt(spline, XIeval);
err_spline = Zeval - val_spline;
rms_spline = sqrt(mse(err_spline));

ResultSet = cell(NParts, 4);
for i = 1:NParts
    DSPL = DSPLINES{i};
    spl = DSPL.spline;
    spl0 = DSPL.spline0;
    spl0prePME = DSPLINES0{i}.spline0;
    [imap bdat] = tsearchn(spl.PHI, spl.TRI, XIeval);
    idx = find(~isnan(imap));
    if (~isempty(idx))
        ResultSet{i, 1} = bsplinen_evalExt(spl, XIeval(idx, :));
        ResultSet{i, 2} = XIeval(idx, :);
        ResultSet{i, 3} = delaunayn(XIeval(idx, :));        
        ResultSet{i, 4} = bsplinen_evalExt(spl0, XIeval(idx, :));
        ResultSet{i, 5} = bsplinen_evalExt(spl0prePME, XIeval(idx, :));
    end
end

val_gspline = bsplinen_evalExt(GlobalSpline, XIeval);
% offset = mean(val_gspline) - mean(Zeval);
% val_gspline = val_gspline - offset;
err_gspline = Zeval - val_gspline;
rms_gspline = sqrt(mse(err_gspline - mean(err_gspline)));

rms_diffgspline = sqrt(mse(val_gspline - val_spline - mean(val_gspline - val_spline)));

% calculate locations of B-coefficients
Bcoords = bsplinen_calcBCoefCoordinates(spline);
% triangulate B-coef locations
TRIBcoords = delaunayn(Bcoords);



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Do the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;

az = -13;
el = 29;

%%
colcount = 1;
rowcount = 1;
plotID = 10;
figure(plotID);
set(plotID, 'Position', [1 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
center = [0.5 0.5];
for j = 1:NParts
    trisubidx = PartitionData{j, 2}; 
    Isimpstar = PartitionData{j, 4};
    Isimpedge = PartitionData{j, 6};
    Ivertedge = PartitionData{j, 7};
    
    trisub = TRI(trisubidx, :);   
    triedge = trisub(Isimpedge, :);
    tristar = TRI(Isimpstar, :);
    
    if (mod(j, 3) == 0)
        color = 'r';
    elseif (mod(j, 3) == 1)
        color = 'g';
    elseif (mod(j, 3) == 2)
        color = 'b';
    end
    trisurf(tristar, PHI(:,1), PHI(:,2), -1*ones(size(PHI, 1), 1), 'EdgeColor', 'none', 'FaceColor', [.6 .6 .6], 'LineWidth', 1);
    trimesh(triedge, PHI(:,1), PHI(:,2), 'Color', [1 0 0], 'LineWidth', 3);
    trimesh(trisub, PHI(:,1), PHI(:,2), 'Color', color);
    plot(PHI(Ivertedge,1), PHI(Ivertedge,2), 'ro');
    
    colcount = colcount + 1;
    if (colcount > PartitionCountSet(1))
        colcount = 1;
        rowcount = rowcount + 1;
    end
end

xlabel('x');
ylabel('y');
title(sprintf('Triangulation (T=%d) Partitioned into %d Partitions (anchor simplices in blue)', T, NParts));
if (printfigs == 10 || printfigs == 1)
    fpath = sprintf('fig_TriangulationPart_SL%d_T%d_NP%d',StarLevel, T,NParts);
    savefname = strcat(figpath, fpath);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname); % no Matlabfragging necessary because no text is present!->WRONG, we need the text to produce non-vectorized EPS...
    fprintf('Printed <%s>\n', savefname);
end



plotID = 101;

figure(plotID);
set(plotID, 'Position', [400 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
trimesh(TRI, PHI(:, 1), PHI(:, 2), 'Color', 'b');
plot(PHI(:, 1), PHI(:, 2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0], 'MarkerSize', 5);
axis([-.2 1.2 -.2 1.2]);
count = 1;
bind = 1;
xlabel('x');
ylabel('y');
titstr = sprintf('Triangulation consisting of %d simplices', T);
title(titstr);
if (printfigs == 101 || printfigs == 1)
    fname = sprintf('fig_Triangulation_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end


%%
colcount = 1;
rowcount = 1;
plotID = 150;
figure(plotID);
set(plotID, 'Position', [1 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
center = [0.5 0.5];
for j = 1:size(PartitionData, 1)
    trisubidx = PartitionData{j, 2}; 
    trisub = TRI(trisubidx, :);
    if (mod(j, 3) == 0)
        color = 'r';
    elseif (mod(j, 3) == 1)
        color = 'g';
    elseif (mod(j, 3) == 2)
        color = 'b';
    end
    phi = PHI;
    centerphisub = mean(PHI(trisub, :));
    phiscale = centerphisub - center;
    phiscale = phiscale * 1.4;
    phi(trisub,:) = phi(trisub,:) + ones(size(phi(trisub,:)))*diag(phiscale-centerphisub+center);

    trimesh(trisub, phi(:,1), phi(:,2), 'Color', color);

    colcount = colcount + 1;
    if (colcount > PartitionCountSet(1))
        colcount = 1;
        rowcount = rowcount + 1;
    end
end

xlabel('x');
ylabel('y');
title(sprintf('Triangulation (T=%d) Partitioned into %d Partitions (anchor simplices in blue)', T, NParts));
if (printfigs == 106 || printfigs == 1)
    fpath = sprintf('fig_TriangulationControlOverlap_SL%d_T%d_NP%d',StarLevel, T,NParts);
    savefname = strcat(figpath, fpath);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname); % no Matlabfragging necessary because no text is present!->WRONG, we need the text to produce non-vectorized EPS...
    fprintf('Printed <%s>\n', savefname);
end


%%
plotID = 1000;

figure(plotID);
set(plotID, 'Position', [200 150 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_spline);
maxz = max(val_spline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_spline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
titstr = sprintf('Single Step Spline function of degree %d, continuity order %d, on %d Triangles', d, r, T);
title(titstr);
if (printfigs == 1000 || printfigs == 1)
    fname = sprintf('fig_SplineResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 2001;

figure(plotID);
set(plotID, 'Position', [200 600 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_gspline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_gspline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
titstr = sprintf('Global Parallel Dual Ascent Spline, d=%d, r=%d, T=%d, converged in %d iterations', d, r, T, Gitt);
title(titstr);
if (printfigs == 2001 || printfigs == 1)
    fname = sprintf('fig_SplineResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end


%%
plotID = 3001;
figure(plotID);
set(plotID, 'Position', [600 150 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
for i = 1:NParts
    trisurf(ResultSet{i, 3}, ResultSet{i, 2}(:, 1), ResultSet{i, 2}(:, 2), ResultSet{i, 5});
end
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
% titstr = sprintf('Parallel Dual Ascent Spline of degree %d, continuity order %d, on %d Triangles', d, r, T);
titstr = sprintf('Parallel Dual Ascent Spline, d=%d, r=%d, T=%d, before PME', d, r, T);
title(titstr);
if (printfigs == 3001 || printfigs == 1)
    fname = sprintf('fig_SplinePrePME_NP%d_d%dr%d_T%d', NParts, d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 3002;
figure(plotID);
set(plotID, 'Position', [600 400 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
for i = 1:NParts
    trisurf(ResultSet{i, 3}, ResultSet{i, 2}(:, 1), ResultSet{i, 2}(:, 2), ResultSet{i, 4});
end
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
% titstr = sprintf('Parallel Dual Ascent Spline of degree %d, continuity order %d, on %d Triangles', d, r, T);
titstr = sprintf('Parallel Dual Ascent Spline, d=%d, r=%d, T=%d, at first DA iteration', d, r, T);
title(titstr);
if (printfigs == 3002 || printfigs == 1)
    fname = sprintf('fig_SplineFirstDA_NP%d_d%dr%d_T%d', NParts, d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 3003;

figure(plotID);
set(plotID, 'Position', [600 600 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
for i = 1:NParts
    trisurf(ResultSet{i, 3}, ResultSet{i, 2}(:, 1), ResultSet{i, 2}(:, 2), ResultSet{i, 1});
end
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
% titstr = sprintf('Parallel Dual Ascent Spline of degree %d, continuity order %d, on %d Triangles', d, r, T);
titstr = sprintf('Parallel Dual Ascent Spline, d=%d, r=%d, T=%d, converged in %d iterations', d, r, T, Gitt);
title(titstr);
if (printfigs == 3003 || printfigs == 1)
    fname = sprintf('fig_SplineFinalDA_NP%d_d%dr%d_T%d', NParts, d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 3004;

figure(plotID);
set(plotID, 'Position', [600 600 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
surf(xxe, yye, reshape(val_gspline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
% titstr = sprintf('Parallel Dual Ascent Spline of degree %d, continuity order %d, on %d Triangles', d, r, T);
titstr = sprintf('Parallel Dual Ascent Spline, d=%d, r=%d, T=%d, converged in %d iterations', d, r, T, Gitt);
title(titstr);
if (printfigs == 3004 || printfigs == 1)
    fname = sprintf('fig_GlobalSplineDA_NP%d_d%dr%d_T%d', NParts, d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 4002;

figure(plotID);
set(plotID, 'Position', [1200 500 600 400], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_spline-val_gspline);
maxz = max(val_spline-val_gspline);
% trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
err = val_spline-val_gspline;
surf(xxe, yye, reshape(err-mean(err), size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(153, 22);
xlabel('x');
ylabel('y');
titstr = sprintf('Error between single step and Parallel Dual Ascent methods');
title(titstr);
if (printfigs == 4002 || printfigs == 1)
    fname = sprintf('fig_SplineResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%

plotID = 5011;

figure(plotID);
set(plotID, 'Position', [1200 50 600 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
plot(spline.coefs, 'b', 'LineWidth', 2);
plot(GlobalSpline.coefs + mean(spline.coefs-GlobalSpline.coefs), 'r');
legend('Single Step', 'DA');
xlabel('B-coefficient nr');
ylabel('Value ');
titstr = sprintf('B-coefficient values of PME+DA-spline and single step spline');
title(titstr);
if (printfigs == 5011 || printfigs == 1)
    fname = sprintf('fig_DASplineBcoefs_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end



plotID = 5013;

figure(plotID);
set(plotID, 'Position', [1200 150 600 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
plot(spline.coefs-GlobalSpline.coefs - mean(spline.coefs-GlobalSpline.coefs), 'b');
% plot(GlobalSpline.coefs, 'r');
% legend('Single Step', 'DA');
xlabel('B-coefficient nr');
ylabel('Value ');
titstr = sprintf('Difference in B-coefficient values between PME+DA-spline and single step spline');
title(titstr);
if (printfigs == 5013 || printfigs == 1)
    fname = sprintf('fig_DASplineBcoefs_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Finally, some outputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n-------------------------------------------------------------------------------------------------\n');
fprintf('Parallel Dual Ascent Spline (d=%d,r=%d,T=%d,NParts=%d)\n', d, r, T, NParts)
fprintf('Stage 2 (Smooth) terminated after %d iterations in %2.4f [s]\n', Gitt, time2)
fprintf('Single step RMS: %d, Parallel Dual Ascent RMS: %d\n', rms_spline, rms_gspline);
fprintf('RMS of Pre-DA H*c = %d\n', rmsInitialResHKc);
fprintf('RMS of Post-DA H*c = %d\n', rmsResHKc);
fprintf('---------------------------------------------------------------------------------------------------\n');



