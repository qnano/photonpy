% BSPLINEN_GETLINESIMPLEXINTERSECTIONS returns the intersection points between a line and a simplex
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2010
%              email: c.c.devisser@tudelft.nl
%                          version 1.1
%
%
%   Input to BSPLINEN_GETLINESIMPLEXINTERSECTIONS is:
%       simplex = [n+1 x n] matrix of vertex coordinates
%       xline = [2 x n] matrix of vertex coordinates of a line (e.g. line = [[x_1;x_2], [y_1;y_2]])
function [Xint FacetIntIdx] = bsplinen_getLineSimplexIntersections(Simplex, Xline)

    % determine dimension
    n = size(Simplex, 2);
    epsilon = 1e-15;

    % any simplex has at most 2 intersection points with a line
    Xint = nan(2, n);
    % there at most 2 facets that are intersected with a line
    FacetIntIdx = nan(2, 1);
    
    % go through all n+1 edge facets
    facetidx = 1:n;
    icount = 0;
    for i = 1:n+1
        % find intersections with facet planes, if any
        xint = bsplinen_intersectionHyperplane(Simplex(facetidx, :), Xline);
        if (all(~isnan(xint)))
            % now check whether the intersection point is within the simplex, because the facet plane is unbounded!
            barytest = bsplinen_cart2bary(Simplex, xint);
            if (all(barytest >= -epsilon))
                % we have found an intersection point within the simplex!
                % now check whether we already have this intersection point in our list
                exists = 0;
                for nints = 1:icount
                    if (abs(Xint(nints, :)-xint) < epsilon)
                        exists = 1;
                        break;
                    end
                end
                % nope, so add it!
                if (~exists)
                    Xint(icount+1, :) = xint;
                    FacetIntIdx(icount+1) = i;
                    icount = icount + 1;
                end
            end
        end
        
        % increment facet index
        facetidx = facetidx + ones(1, n);
        facetidx(facetidx > n+1) = 1;

    end
    
    if (icount == 0)
        Xint = [];
        FacetIntIdx = [];
    else
        Xint = Xint(1:icount, :);
        FacetIntIdx = FacetIntIdx(1:icount);
    end
    
    
