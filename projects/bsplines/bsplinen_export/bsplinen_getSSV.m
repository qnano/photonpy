
% BSPLINEN_GETSSV returns the index and non-zero index column of the SSV of
%   simplex1 with respect to the edge between simplex1 and simplex2.
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2008
%              email: c.c.devisser@tudelft.nl
%                          
%                   Version: 2.0
%
function [ssvIndex ssvCol] =  bsplinen_getSSV(simplex1, simplex2)

    % simplex 1 and simplex 2 must both be row vectors!
    
    % find the ssv: the ssv is not shared between simplices!
    ssvIndex = 0;
    ssvCol = 0;
    for i = 1:length(simplex1)
    
        edgevert = find(simplex1(i) == simplex2(:));
        if (isempty(edgevert) == 1)
            ssvIndex = simplex1(i);
            ssvCol = i;
            break;
        end
    
    end