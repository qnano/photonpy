
% BSPLINEN_FINDNEIGHBORS2 N-D closest neighbour simplex search.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2009
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%   NB, NI = BSPLINEN_FINDNEIGHBORS2(TRI) finds the set of closest neighbours for every
%   simplex in TRI. 
%
%   Input parameters are  
%
%       TRI is an m-by-n matrix, having as much rows as there
%           are simplices and as much columns as there are dimensions. Every row in TRI 
%           contains the vertex indices of a unique simplex. TRI is fully compatible 
%           with the simplex index arrays produced by DELAUNAY, DELAUNAY3 and DELAUNAYN. 
%   
%       NCOUNT specifies the required number of shared 
%           vertices for simplices to be considered neighbours. NCOUNT must be at least 1,
%           and at most N-1. When NCOUNT is 1, simplices are considered neigbours
%           because they share just 1 vertex. When NCOUNT is N-1 (or larger)
%           simplices are considered neigbours if they share an N-1 face. The
%           maximum number of shared neighbours is N-1, when N is the dimension of
%           the simplices. Theoretically the number of shared vertices could equal
%           N, but this would imply TRI is an invalid triangulation.
%
%   Output from BSPLINEN_FINDNEIGHBORS2 is
%       
%       NB is a cell array holding the vertex indices of the neigbouring
%           simplices for every simplex in TRI. The number of
%           neighbours can differ between simplices deep within the convex hull of
%           TRI and simplices on the outer edges. The minimum number of neighbours
%           for a triangulization TRI with more than one simplex is 1, the maximum
%           N+1.
%       
%       NI is a cell array holding the simplex indices in TRI of the neigbouring
%            simplices for every simplex in TRI.
%
%       NEV is a cell array holding the vertex indices of the vertices in
%           the edge.
function [NB, NI, NEV] = bsplinen_findneighbors2(TRI, nCount)
    
    % declare cell arrays
    NB = {};    % vertex index array
    NI = {};    % simplex index array
    NEV = {};   % edge vertex array
    
    % The simplex dimension
    n = size(TRI, 2) - 1; 
    % Total number of simplices
    T = size(TRI, 1);
    % maximum simplex index
    vidxMax = max(TRI(:));

    % Return if there are less than 2 simplices in TRI or nCount has been
    % chosen smaller than 1.
    if (length(TRI) <= 1 || nCount < 1)
        disp('FINDNEIGHBOURS2: ERROR, the number specifying the minimum vertex count is too small, returning...');
        return;
    end
    
    % If the required number of shared vertices is larger than or equal to the simplex
    % dimension n, it must be reduced to n-1, as it does not make sense to
    % try and match every vertex between two simplices (which only happens
    % when the triangulation is erroneous).
    if nCount > n
        disp(sprintf('FINDNEIGHBOURS2: WARNING, The dimension of the simplices in TRI is %d. \n The required number of shared vertices was chosen to be %d. The required shared vertex count will be reduced to %d', n, nCount, n));
        nCount = n;
    end
   
    
    % create the row indices into the sparse adjacency matrix
    zeros(T, n+1);
    for i = 1:n+1
        rowIdx(:, i) = (1:T)';
    end
    rowIdxLin = rowIdx(:);

    % create the sparse adjacency matrix
    TRIADJ = sparse(rowIdxLin, TRI(:), 1, T, vidxMax);
    
    
    % now go through TRI and retrieve all simplices having nCount equal
    % vertices
    for i = 1:T
        % the to-be-tested simplex
        SIMPTEST = TRI(i, :);
%         % the adjacency information for the to-be-tested simplex
%         SIMPTESTIDX = TRIADJ(i, :);
        
        % the columns holding the vertex indices of SIMPTEST
        colIdx = ones(T, 1) * SIMPTEST;
        colIdxLin = colIdx(:);
        
        % the binary adjacency matrix for a single simplex
        TRIADJTEST = sparse(rowIdxLin, colIdxLin, 1, T, vidxMax);
        
        % perform the boolean adjacency test
        TRIBOOL = TRIADJ & TRIADJTEST;
        % count the number of vertex matches per simplex
        TRICOUNT = sum(TRIBOOL, 2);
        % the neigboring simplices have n matching vertices
        NbSimpsIdx = find(TRICOUNT == nCount);
                
        % counter keeping track of neighbour count
        ncount = 0;
        
        % add the data of the neigboring simplices of SIMPTEST to the lists 
        for j = 1:length(NbSimpsIdx)
            ncount = ncount + 1;
            % insert the neigbouring simplex itself
            nsimp = TRI(NbSimpsIdx(j), :);
            NB{i, ncount} = nsimp;
            % and insert its index
            NI{i, ncount} = NbSimpsIdx(j);
            % add the edge vertices
            NEV{i, ncount} = find(TRIBOOL(NbSimpsIdx(j),:))';
        end
        
       
        
    end
    
    
    
    