
% bsplinen_evalMat calculate the evaluation matrix for a given set of evaluation locations
%   such that Y = B*c
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%
%   Input:
%       XI: the evaluation locations in global cartesian coordinates
%       fn: a struct_bsplinen structure containing valid <fn.TRI, fn.PHI, fn.coefs, fn.degree, fn.dim> fields.
%
%   Output: 
%       B: sparse evaluation matrix such that Y = B * fn.coefs
%
function [XIeval, Zeval, B] = bsplinen_evalMat(XI, Z, fn)
    

    B = sparse(size(XI, 1), length(fn.coefs));
    
    global STRUCTURES_LOADED;
    global struct_bsplinen_options;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    else
        if (isempty(STRUCTURES_LOADED))
            bsplinen_structures;
        end
    end
    options = struct_bsplinen_options;

    if (isempty(XI))
        warning('The global data location vector is empty, returning...');
        return;
    end
    
    % sort the simplex indices
    fn.TRI = sort(fn.TRI, 2);
    % the number of simplices
    J = size(fn.TRI, 1);
    
    % precalculate the polynomial normalization coefficient
    % Kt = |kappa|! / kappa!
    [polybasis basisKt] = bsplinen_constructBasisKt(fn.dim, fn.degree);
        
    % the number of B-coefficients per simplex
    [dhat nb] = size(polybasis);
    % perform the datapoint membership search and construct B
    [IMap LDat] = tsearchn(fn.PHI, fn.TRI, XI); % TSEARCHN is a relatively efficient MATLAB membership search method
   
    % remove any datapoints that are outside of the triangulation
    nind = find(~isnan(IMap));
    IMap = IMap(nind);
    LDat = LDat(nind, :);
    XIeval = XI(nind, :);
    Zeval = Z(nind, :);

    N = length(IMap);
    % create the regression/evaluation matrix
    B = lrsplinen_buildB(IMap, LDat, N, J, polybasis, basisKt, dhat);
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
    
function [basis basisKt] = bsplinen_constructBasisKt(PolyBasis, d)

    if (nargin == 1)
        %   Polynomial degree
        d       = sum(PolyBasis(1,:),2);

        %   Calc the basis coefficient vector |kappa|! / kappa! 
        basisKt =  factorial(d)./prod(factorial(PolyBasis),2);
    elseif (nargin == 2)
        n = PolyBasis;
        basis = bsplinen_constructBasis(n, d);
        basisKt =  factorial(d)./prod(factorial(basis),2);
    end
    
    
    
function Btk = bsplinen_constructBtk(polybasis, basisKt, Lambda)

    % the total number of B-coefs
    sizeBt = size(polybasis, 1);

    if (isempty(Lambda))
        Btk = [];
        return 
    end
    
    % the total number of datapoints
    sizeX = size(Lambda, 1);

    %   Computation of Btk
    Btk = zeros(sizeX, sizeBt);
    for i = 1:sizeBt
        Btk(:,i)     = basisKt(i)*prod(Lambda.^(ones(sizeX,1)*polybasis(i,:)),2);
    end
    

% Helper function that constructs the evaluation matrix; this is heavily (Matlab) optimized code.
function B = lrsplinen_buildB(IncMap, LDat, N, J, polybasis, basisKt, dhat)

    % the vector holding the per-simplex data volume
    dvol = zeros(J, 1);
    % vector holding offsets in the SORTED Inclusion map
    OS = zeros(J, 4);
    % the global regressor vector in barycentric coordinates
    B = sparse(N, dhat * J);

    % sort the data index map
    [IncSort Ind] = sortrows([IncMap (1:length(IncMap))']); % add second column for inverse sorting
    [tmp invInd] = sortrows(IncSort(:,2)); % inverse sorting index
    LambdaSort = LDat(Ind, :);

    % count the number of datapoints per simplex
    for i = 1:N
        index = IncSort(i, 1);
        dvol(index) = dvol(index) + 1;
    end
    % get the indices of the per-simplex data blocks
    offset = 1;
    for t = 1:J
        newoffset = offset + dvol(t);
        OS(t, :) = [offset (newoffset-1) ((t-1)*dhat+1) t*dhat];
        offset = newoffset;
    end

    % go through all simplices and define the per-simplex dispersion matrix blocks
    for t = 1:J
        % the per simplex regressor vector
        Bt = bsplinen_constructBtk(polybasis, basisKt, LambdaSort(OS(t, 1):OS(t, 2), :));
        % insert the per-simplex blok Bt into the global matrix B using efficient sparse blok indexing
        if (~isempty(Bt))
            B(OS(t,1):OS(t,2), OS(t,3):OS(t,4)) = Bt;  
        end
    end
    
    % De-sort B to match inputs
    B = B(invInd, :);


