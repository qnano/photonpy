
% BSPLINEN_CONSTRUCTHFROMTRI constructs the sparse full rank smoothness matrix H 
%   using only the triangulation, vertex set, and polynomial properties as inputs
%
%              Copyright: C.C. de Visser, Delft University of Technology, 2011
%              email: c.c.devisser@tudelft.nl
%              version 3.0
% 
%   H = BSPLINEN_CONSTRUCTHFROMTRI(PHI, TRI, D, R, OPTIONS)
%   constructs the sparse full rank smoothness matrix H holding the continuity
%   equations in the form Hc = 0 as presented by Awanou in [Awanou et al., 2005]. 
%   The smoothness matrix is constructed based on the edges of the
%   set of simplices DELTA, the per-simplex polynomial basis function Btk and the
%   required order of continuity R. This function makes use of Lai's
%   graphical representation of the continuity equations, see [Lai, 1997].
%   The continuity functions are sorted based on their order R, producing a
%   R-banded matrix H. Matrix H is guaranteed to be of full rank. The way
%   the algorithm works is by checking the estimated condition number of 
%   [H; newrow]*[H; newrow]' using CONDEST, where newrow is the new continuity equation.
%   If CONDEST is smaller than a preset tolerance (e.g. 1e10) [H; newrow] is
%   assumed to be non-singular, and the new equation 'newrow' is non-redundant
%
%   Input parameters for BSPLINEN_CONSTRUCTHFROMTRI are:
%
%       PHI the vertex data array, or the array holding the coordinates of
%           all vertices. PHI must have size V x d, with V the total number
%           of vertices (note that V > T) and d the number of dimensions.
%           This matrix is NOT the vertex index array as produced by
%           DELAUNAYN, but rather the input to this method. For instance
%           when there are three coordinate dimensions x, y, z then
%           PHI = [x y z].
%       
%       TRI the vertex index array where TRI(i,:) are indices into PHI coding
%           for one simplex. Can be constructed with DELAUNAYN(PHI).
%
%       D the degree of the spline function.
%
%       R the required order of continuity. R must be one order smaller
%           than the order of the polynomial basis function.
%
%       OPTIONS a STRUCT_BSPLINEN_OPTIONS structure holding user definable
%           options for constructing the H matrix.
%
%   Output from BSPLINEN_CONSTRUCTHFROMTRI is
%
%       H the full rank smoothness matrix. H is a sparse matrix with size 
%           K x (dhat*T) with K < E*|ctk| the number of physical edges,
%           |ctk| the total number of continuity equations per edge, dhat the
%           total number of B-coefficients per simplex and T the total number
%           of simplices.
%
%       IHedges contains the index of the edge for each row in H. IHedges is a 
%           K x 1, with K < E*|ctk| the number of physical edges, 
%
function [H, IHedges] = bsplinen_constructHfromTRI(PHI, TRI, d, r, options)

    % default empty (non-sparse at this stage) matrix
    H = [];
    n = size(PHI, 2);
    
    % load structures
    global STRUCTURES_LOADED;
    global struct_bsplinen_options;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    end
        
    % if no options were supplied, then load the default options
    if (nargin < 5)
        options = struct_bsplinen_options;
    end        
    if (isempty(options))
        options = struct_bsplinen_options;
    end
    
    if (options.fullrankH == 1)
        options.filterH = 1; % automatically enable C0 filtering
    end
    if (options.fullrankH == 2)
        if (isempty(Stars))
            warning('STARS should contain at least 1 star structure, returning...');
            return;
        end
    end


    % the preset condition number tolerance
    condTol = 1e9;
    
    % polynomial basis
    polybasis = bsplinen_constructBasis(n, d);

    % create the triangulation structure, neighbor search etc
    [tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI});

    % number of simplices
    T = size(DELTA, 1);
    % number of dimensions
    n = size(PHI, 2);
    % polynomial order
    d = sum(polybasis(1, :));
    % number of edges
    E = length(EDGES);
    % the total number of B-coefficients
    dhat = bsplinen_calcdHat(n, d);
    
    % define the sparse smoothness matrix H
    contCount = bsplinen_calcContCount(n, d, r);
    rowsH = E * contCount;
    colsH = T * dhat;
    % Predefine H. Note that rowsH is the most conservative estimate on the
    % true number of linearly independent rows of H.
    H = sparse(rowsH, colsH);
    IHedges = zeros(rowsH, 1);
    
    % the array used to filter the 0-th order continuity equations
    Cfilter = sparse(colsH, 1);
    % create a sparse matrix using the index total value of the
    % B-coefficient indices as keys to retrieve (very fast) the index
    % value of B-coefficients in the global B-vector
    CIndexArr = bsplinen_constructCIndexArray(polybasis);
    
    % precalculate the gamma indexer
    gammas = {};
    for order = 0:r
        gammas{order+1} = bsplinen_constructBasis(n, order);
    end

    % precalculate the polynomial normalization coefficients
    % Kt = |kappa|! / kappa! for every order up to r.
    kappas = {};
    for order = 0:r
        kappas{order+1} = bsplinen_constructBasisKt(gammas{order+1});
    end
               
    % Disable full rank H and filter H when n = 1; we have no rank issues for n = 1!
    if (n == 1)
        options.fullrankH = 0;
        options.filterH = 0;
    end
    
    % perform 0-th order filtering of the continuity conditions to remove redundant 0-th order
    % continuity conditions based on an analysis of internal vertices.
    ItransmatInternal = [];
    Iall = [];
    if (options.filterH || options.fullrankH)
        % get internal vertices
        [Iinternal Iall Iedge] = bsplinen_getInternalVertices(DELTA, PHI, EDGES);
        % count number of edges per internal vertex
        ImatInternal = sparse(Iall(end), 3); % index matrix from 1 to max(Iall) = Iall(end)
        
        for i = 1:length(EDGES)
            edge = EDGES{i};
            ImatInternal(edge.edgevertices) = ImatInternal(edge.edgevertices) + 1;
        end
        % ImatInternal now holds the total number of edges (nedges) per internal vertex. For 0-th order continuity
        % every internal vertex should be associated with nedges-1 contintuity equations
        ImatInternal(Iinternal, 2) = 1;
    end
    
    % Formulate the Continuity Matrix H.
    % There are 3 main options for constructing the continuity matrix
    % depending on the value of options.fullrankH:
    %   0 : a rank deficient continuity matrix is constructed 
    %   1 : a full rank continuity matrix is constructed using the condition number algorithm
    %   2 : a full rank continuity matrix is constructed using the STAR algorithm (NOT FUNCTIONAL AT THIS TIME!)
    if (options.fullrankH == 0)
        
        if (options.sortedH == 0)
            % Go through all edges and formulate continuity conditions
            hrow = 1;
            for i = 1:E

                % the edge for which the continuity equations will be formulated
                edge = EDGES{i};
                simplex2 = DELTA{edge.simplex2, 1};

                % Formulate the continuity equations for this edge
                % columns: 1 = order, 2 = c1, 3 = c1 in global c, 4 = c1 in local, 5 = c2 +
                % gamma, 6 = c2 + gamma in global c, 7 = c2 + gamma in local, 8 = gamma
                [continuityConds Cfilter] = bsplinen_continuityConditions(polybasis, edge, gammas, r, Cfilter, CIndexArr, options);
                if (isempty(continuityConds{1, 1} == 1))
                    continue;
                end

                % precalculate all polynomial values for this edge
                %ssv1 = PHI(edge.ssv1, :);
                ssv1 = bsplinen_cart2bary(simplex2.vertices, PHI(edge.ssv1, :)); % in barycentric coords
                Btks = cell(r+1, 1);
                Btks{1} = 1;
                for order = 1:r
                    Btks{order+1} = bsplinen_constructBtk(gammas{order+1}, kappas{order+1}, ssv1);
                end

                IHedges(hrow:hrow+size(continuityConds, 1) - 1, 1) = i;
                % create sub-H matrix

                % now insert the continuity equations for this edge in the H matrix
                for k = 1:size(continuityConds, 1)

                    condition = continuityConds(k, :);
                    c1globind = condition{3};
                    c2globind = condition{6};
                    % insert the constant
                    H(hrow, c1globind) = -1;
                    % calculate and insert the polynomial values for ssv1
                    Btk = Btks{condition{1} + 1};
                    H(hrow, c2globind) = Btk;

                    hrow = hrow + 1;

                end

            end
        else
            % Go through all edges and formulate continuity conditions sorted
            % on continuity order.
            hrow = 1;
            % go through all continuity orders up to r
            for m = 0:r
                % go through all edges
                
                % Filtering is now based on per-equation filtering instead of per-edge filtering!!!
                if (options.filterH)
                    % set the filter to be equal to the list of maximum allowed equations per internal vertex
                    Cfilter = ImatInternal;
                end
                
                for i = 1:E

                    % the edge for which the continuity equations will be formulated
                    edge = EDGES{i};
                    simplex2 = DELTA{edge.simplex2, 1};
                    
                    % Formulate the continuity equations for this edge
                    % columns: 1 = order, 2 = c1, 3 = c1 in global c, 4 = c1 in local, 5 = c2 +
                    % gamma, 6 = c2 + gamma in global c, 7 = c2 + gamma in local, 8 = gamma
                    [continuityConds Cfilter] = bsplinen_continuityConditions_M(polybasis, edge, gammas, m, Cfilter, CIndexArr, options);
                    if (isempty(continuityConds{1, 1} == 1))
                        continue;
                    end

                    % precalculate all polynomial values for this edge
                    Btk = bsplinen_constructBtk(gammas{m+1}, kappas{m+1}, edge.ssv1bary);
                    
                    IHedges(hrow:hrow+size(continuityConds, 1) - 1, 1) = i;
                    
                    % create sub-H matrix
                    
                    % now insert the continuity equations for this edge in the H matrix
                    for k = 1:size(continuityConds, 1)

                        condition = continuityConds(k, :);
                        c1globind = condition{3};
                        c2globind = condition{6};
                        % insert the constant
                        H(hrow, c1globind) = -1;
                        % insert the polynomial values
                        H(hrow, c2globind) = Btk;
                        hrow = hrow + 1;

                    end

                end
            end
        end

        % resize H if necessary
        if (hrow-1 ~= rowsH)
            if (options.outputmsg == 1)
                disp(sprintf('BSPLINEN_CONSTRUCTH: Removed %d redundant continuity equations from H', rowsH - (hrow - 1)));
            end
            H = H(1:(hrow-1), :);
            IHedges = IHedges(1:(hrow-1));
        end

    elseif (options.fullrankH == 1)
        
        if (options.outputmsg == 1), disp(sprintf('BSPLINEN_CONSTRUCTH: Starting formulation of Full Rank H using the condition number algorithm...')); end;
        RemoveCount = zeros(1, r+1);
        EqCount = zeros(1, r+1);
        % Go through all edges and formulate continuity conditions sorted
        % on continuity order.
        hrow = 1;
        % go through all continuity orders up to r
        for m = 0:r
            
            if (options.outputmsg == 1), disp(sprintf('BSPLINEN_CONSTRUCTH: Starting formulation of C%d continuity equations...', m)); end;

            % Filtering is now based on per-equation filtering instead of per-edge filtering!!!
            if (options.filterH)
                % set the filter to be equal to the list of maximum allowed equations per internal vertex
                Cfilter = ImatInternal;
            end
                
            % go through all edges
            for i = 1:E

                % the edge for which the continuity equations will be formulated
                edge = EDGES{i};

                % Formulate the continuity equations for this edge
                % columns: 1 = order, 2 = c1, 3 = c1 in global c, 4 = c1 in local, 5 = c2 +
                % gamma, 6 = c2 + gamma in global c, 7 = c2 + gamma in local, 8 = gamma
                [continuityConds Cfilter] = bsplinen_continuityConditions_M(polybasis, edge, gammas, m, Cfilter, CIndexArr, options);
                if (isempty(continuityConds{1, 1} == 1))
                    continue;
                end

                % precalculate all polynomial values for this edge
                Btk = bsplinen_constructBtk(gammas{m+1}, kappas{m+1}, edge.ssv1bary);

                IHedges(hrow:hrow+size(continuityConds, 1) - 1, 1) = i;
                % create sub-H matrix

                % if m == 0 use equation filtering instead of rank estimation algorithm
                if (m == 0)
                    % now insert the continuity equations for this edge in the H matrix
                    for k = 1:size(continuityConds, 1)

                        condition = continuityConds(k, :);
                        c1globind = condition{3};
                        c2globind = condition{6};
                        % insert the constant
                        H(hrow, c1globind) = -1;
                        % insert the polynomial values
                        H(hrow, c2globind) = Btk;
                        hrow = hrow + 1;
                        EqCount(m+1) = EqCount(m+1) + 1;

                    end
                else

                    % now insert the continuity equations for this edge in the H matrix
                    for k = 1:size(continuityConds, 1)

                        condition = continuityConds(k, :);
                        c1globind = condition{3};
                        c2globind = condition{6};

                        % reset the new H row
                        newHrow = sparse(1, colsH);

                        % insert the constant
                        newHrow(c1globind) = -1;

                        % insert the polynomial values
                        newHrow(c2globind) = Btk;

                        % test the new equation
                        Htest = [H(1:hrow-1, :); newHrow];
                        % make the square test matrix
                        %HtestSq = Htest * Htest';

                        % Calculate the condition number of the square
                        % Htest*Htest'.                    
                        cond = condest(Htest * Htest');


                        % Test the condition number. if the condition number is smaller than the tolerance, then H*H' is assumed to 
                        % be invertible, and thus of full rank.
                        if (cond < condTol)
                            % add the new equation to the global H matrix
                            H(hrow, :) = newHrow;                        
                            IHedges(hrow) = i;
                            hrow = hrow + 1;
                        else
                            RemoveCount(m+1) = RemoveCount(m+1) + 1;
                        end

                        EqCount(m+1) = EqCount(m+1) + 1;

                    end % continuity condition loop
                    
                end % m == 0 switch

            end % edge loop
            
        end % continuity order loop

        %C0removed = (rowsH-hrow+1) - sum(RemoveCount);
        if (options.outputmsg == 1), disp(sprintf('BSPLINEN_CONSTRUCTH: Removed %d of %d C0 equations.', RemoveCount(1), EqCount(1)));end;
        for i = 1:r
            if (options.outputmsg == 1), disp(sprintf('BSPLINEN_CONSTRUCTH: Removed %d of %d C%d equations.', RemoveCount(i+1), EqCount(i+1), i));end;
        end

        % resize H to its full rank size
        H = H(1:(hrow-1), :);
        IHedges = IHedges(1:(hrow-1));

        if (options.outputmsg == 1), disp(sprintf('BSPLINEN_CONSTRUCTH: Finished formulation of Full Rank [%dx%d] H using the condition number algorithm.', size(H, 1), size(H, 2))); end;
    
        
    end
    

    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
function Lambda = bsplinen_cart2bary(simplex, X)
    
    % The reference vertex is always chosen as the first simplex vertex.
    % This can be done because barycentric coordinates are not dependent on
    % the reference point.
    v0      = simplex(1, :);
    vcount2 = size(simplex, 1) - 1;
    Xcount  = size(X, 1);
    
    Lambda = zeros(Xcount, vcount2 + 1);
    %vcount2 = length(simplex(:, 1)) - 1;
    
    % assemble matrix A
    A = zeros(vcount2, vcount2);
    count = 1;
    for i = 2:length(simplex(:, 1))
        A(:, count) = (simplex(i, :) - v0)';
        count = count + 1;
    end
    
    
    for i = 1:Xcount
        % relative coordinates of x
        p = (X(i, :) - v0)';

        % the last (n) barycentric coordinates. 
        lambda1 = A \ p;

        % the first barycentric coordinate; lambda0
        lambda0 = 1 - sum(lambda1);

        % insert lambda0 into the Lambda vector
        Lambda(i, 1) = lambda0;
        Lambda(i, 2:end) = lambda1;
    end

    
    
function X = bsplinen_bary2cart(simplex, lambda)

    Lcount = size(lambda, 1);
    X = zeros(Lcount, size(lambda, 2) - 1);
    
    v0 = simplex(1, :);
    vcount2 = size(simplex, 1);
    
    for j = 1:Lcount
        for i = 2:vcount2
            X(j, :) = X(j, :) + lambda(j, i) * (simplex(i, :) - v0);
        end
        X(j, :) = X(j, :) + v0;
    end



function ContCount = bsplinen_calcContCount(n, d, r)

    ContCount = 0;
    
    for m = 0:r
        
        ContCount = ContCount + factorial(d-m+n-1) / (factorial(n-1) * factorial(d-m));
    
    end
    
    
function cval = bsplinen_calcIndexTotalValue(c_index)

    r = length(c_index);
    power = r - 1;
    
    cval = 0;
    for j = 1:r
        cval = cval + c_index(j) * 10^power;
        power = power - 1;
    end

    
    
   
    
    
 function CIndexArr = bsplinen_constructCIndexArray(polybasis)

    dhat = length(polybasis(:, 1));
    
    CIndexArr = sparse(dhat, 1);
    
    for i = 1:dhat
    
        ctotval = bsplinen_calcIndexTotalValue(polybasis(i, :));
        CIndexArr(ctotval) = i;
    
    end
    
        
    
function dHat = bsplinen_calcdHat(n, d)

    dHat = factorial(n + d) ./ (factorial(n) * factorial(d));
    

function PolyBasis = bsplinen_constructBasis(n, d)

    
    % the polynomial orders 0 and 1 require no calculations
    if (d == 0)
        PolyBasis = zeros(1, n+1);
        return;
    elseif (d == 1)
        PolyBasis = eye(n+1, n+1);
        return;
    end
    
    % gamma is the increment basis, which is always an identity matrix of
    % size [n+1] x [n+1].
    gamma = eye(n+1, n+1);
    basis0 = gamma;
    
    % now construct the basis from the ground up by sequentially adding the
    % the rows of gamma to each row of basis0...
    for degree = 2:d
        count = 1;
        % allocate the minimum amount of space needed for the basis
        % function indices.
        basis = zeros(length(basis0(:, 1))*(n+1), n+1);
        % go through every basis row
        for k = 1:length(basis0(:, 1))
            % go through every gamma row -1
            for r = 1:(n+1)
                % the newly proposed B-coefficient index
                cindex = basis0(k, :) + gamma(r, :);
                % NOTE: basis will contain lots of duplicates which will be removed later.
                basis(count, :) = cindex;
                count = count + 1;
                 
            end
        end

        % Filter basis0 for duplicates, because otherwise the size of the
        % basis would explode.
        
        % sort the basis in descending order
        basis = flipud(sortrows(basis));
        oldind = -1 * ones(1, n+1);
        count = 1;
        basis0 = zeros(bsplinen_calcdHat(n, degree), n+1);
        for i = 1:length(basis(:, 1))
            cind = basis(i, :);
            found = 0;
            % Compare the new index 'cind' with the previous 'oldind'.
            for j = 1:length(cind)
                if (cind(j) == oldind(j))
                    found = found + 1;
                end
            end
            % put the first found value in the basis0 matrix
            if (found < n+1)
                basis0(count, :) = cind;
                count = count + 1;
                oldind = cind;
            end
        end
        
    end
    
    PolyBasis = basis0;
   
        
function basisKt = bsplinen_constructBasisKt(polybasis)

    % the total number of B-coefs
    [sizeBt n] = size(polybasis);
    d = sum(polybasis(1, :));
    
    basisKt = zeros(sizeBt, 1);

    % calc the basis coefficient vector |kappa|! / kappa! , which is equal for all points
    factd = factorial(d);
    for j = 1:sizeBt
        basisKt(j) = factd / prod(factorial(polybasis(j, :)));
    end

    
function [IndexC, IndexLocal] = bsplinen_getcindex(polybasis_size, CIndexArr, simplex_index, c_local_indices)

    clocal_size = length(c_local_indices(:, 1));
    
    if (clocal_size == 1)
        ctotval = bsplinen_calcIndexTotalValue(c_local_indices);

        IndexLocal = CIndexArr(ctotval);

        IndexC = (simplex_index - 1) * polybasis_size + IndexLocal;
    else
        
        IndexC = zeros(clocal_size, 1);
        IndexLocal = zeros(clocal_size, 1);
        for cind = 1:clocal_size
            clocal = c_local_indices(cind, :);
            ctotval = bsplinen_calcIndexTotalValue(clocal);

            IndexLocal(cind) = CIndexArr(ctotval);

            IndexC(cind) = (simplex_index - 1) * polybasis_size + IndexLocal(cind);
        end
    end


    
    
    
function c2Indices = bsplinen_getCSubjects(polybasis, ssv1col, ssv2col, c1, gamma)

    % get the index values at all columns but the ssv1 column
    kc2 = zeros(1, length(c1)-1);
    count = 1;
    lenc1 = length(c1);
    for i = 1:lenc1
        if (i ~= ssv1col)
            %kc2 = [kc2 c1(i)];
            kc2(count) = c1(i);
            count = count + 1;
        end
    end

    % construct the subject basis index
    c2base = zeros(1, lenc1);
    kccount = 1;
    for i = 1:lenc1
        if (i ~= ssv2col)
            c2base(i) = kc2(kccount);
            kccount = kccount + 1;
        else

        end
        
    end
    
    % create all subject indices
    lengamma = length(gamma(:, 1));
    c2Indices = zeros(lengamma, length(c2base));
    for i = 1:length(gamma(:, 1))
    
        c2 = c2base + gamma(i, :);
        c2Indices(i, :) = c2;
        
    end
    
    
    
function ctargIndices = bsplinen_getCTargets(polybasis, ssvcol, m)


    ctargIndices = [];

    % now put all values in the polybasis with the value 'm' at the
    % position of 'mpos' in controlIndices.
    for i = 1:length(polybasis(:, ssvcol))
        if (polybasis(i, ssvcol) == m)
            ctargIndices = [ctargIndices; polybasis(i, :)];
        end
    end
    
    
function [NB, NI, NEV] = bsplinen_findneighbors(TRI, nCount)
    
    % declare cell arrays
    NB = {};    % vertex index array
    NI = {};    % simplex index array
    NEV = {};   % edge vertex array
    
    % The simplex dimension
    n = size(TRI, 2) - 1; 
    % Total number of simplices
    T = size(TRI, 1);
    % maximum simplex index
    vidxMax = max(TRI(:));

    % Return if there are less than 2 simplices in TRI or nCount has been
    % chosen smaller than 1.
    if (length(TRI) <= 1 || nCount < 1)
        disp('FINDNEIGHBOURS2: ERROR, the number specifying the minimum vertex count is too small, returning...');
        return;
    end
    
    % If the required number of shared vertices is larger than or equal to the simplex
    % dimension n, it must be reduced to n-1, as it does not make sense to
    % try and match every vertex between two simplices (which only happens
    % when the triangulation is erroneous).
    if nCount > n
        disp(sprintf('FINDNEIGHBOURS2: WARNING, The dimension of the simplices in TRI is %d. \n The required number of shared vertices was chosen to be %d. The required shared vertex count will be reduced to %d', n, nCount, n));
        nCount = n;
    end
   
    
    % create the row indices into the sparse adjacency matrix
    zeros(T, n+1);
    for i = 1:n+1
        rowIdx(:, i) = (1:T)';
    end
    rowIdxLin = rowIdx(:);

    % create the sparse adjacency matrix
    TRIADJ = sparse(rowIdxLin, TRI(:), 1, T, vidxMax);
    
    
    % now go through TRI and retrieve all simplices having nCount equal
    % vertices
    for i = 1:T
        % the to-be-tested simplex
        SIMPTEST = TRI(i, :);
%         % the adjacency information for the to-be-tested simplex
%         SIMPTESTIDX = TRIADJ(i, :);
        
        % the columns holding the vertex indices of SIMPTEST
        colIdx = ones(T, 1) * SIMPTEST;
        colIdxLin = colIdx(:);
        
        % the binary adjacency matrix for a single simplex
        TRIADJTEST = sparse(rowIdxLin, colIdxLin, 1, T, vidxMax);
        
        % perform the boolean adjacency test
        TRIBOOL = TRIADJ & TRIADJTEST;
        % count the number of vertex matches per simplex
        TRICOUNT = sum(TRIBOOL, 2);
        % the neigboring simplices have n matching vertices
        NbSimpsIdx = find(TRICOUNT == nCount);
                
        % counter keeping track of neighbour count
        ncount = 0;
        
        % add the data of the neigboring simplices of SIMPTEST to the lists 
        for j = 1:length(NbSimpsIdx)
            ncount = ncount + 1;
            % insert the neigbouring simplex itself
            nsimp = TRI(NbSimpsIdx(j), :);
            NB{i, ncount} = nsimp;
            % and insert its index
            NI{i, ncount} = NbSimpsIdx(j);
            % add the edge vertices
            NEV{i, ncount} = find(TRIBOOL(NbSimpsIdx(j),:))';
        end
        
       
        
    end

    
function EDGES = bsplinen_getEdges(PHI, TRI)

    global STRUCTURES_LOADED;
    global struct_edge2;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    end
    
    T = size(TRI, 1);

    % the number of vertices per simplex
    v_count = length(TRI(1, :));
    % dimension
    n = v_count - 1;

    %EdgeMap = sparse(length(TRI(:, 1)), length(TRI(:, 1)));
    EdgeMap = sparse(size(TRI, 1), size(TRI, 1));
    EDGES = {};

    % find the natural neighbours for every simplex in TRI
    [NB, NI, NEV] = bsplinen_findneighbors(TRI, n);
    
    
    if (isempty(NB))
        return;
    end
    
    edgeCount = 1;
    % go through all simplices
    % for i = 1:T
    for i = 1:size(NB, 1)
        % define the ssv's for this edge
        % go through all neighboring simplices of the simplex at 'i'
        for nb = 1:size(NB, 2)

            if (~isempty(NB{i, nb}))

                % setup the edge structure
                edge = struct_edge2;
                edge.index = edgeCount;                
                edge.simplex1 = i;
                edge.simplex2 = NI{i, nb};
                edge.edgevertices = NEV{i, nb};
                
                % get the ssv of the first simplex 
                [edge.ssv1 edge.cssv1] = bsplinen_getSSV(TRI(i, :), NB{i, nb});
                % get the ssv of the second simplex 
                [edge.ssv2 edge.cssv2] = bsplinen_getSSV(NB{i, nb}, TRI(i, :));

                % calculate the barycentric coordinates of the SSV's with
                % respect to the target simplices
                edge.ssv1bary = bsplinen_cart2bary(PHI(TRI(edge.simplex2, :), :), PHI(edge.ssv1, :));
                edge.ssv2bary = bsplinen_cart2bary(PHI(TRI(edge.simplex1, :), :), PHI(edge.ssv2, :));

                % check whether we already have an edge for the same
                % set of simplices (the dual edge...).
                s1 = edge.simplex1;
                s2 = edge.simplex2;
                if (s1 > s2)
                    s1 = edge.simplex2;
                    s2 = edge.simplex1;
                end
                if (EdgeMap(s1, s2) == 0)
                    EdgeMap(s1, s2) = 1;
                    EDGES{edgeCount, 1} = edge;
                    edgeCount = edgeCount + 1;
                end
                
            end

        end 

    end    
    
    
    test=1;
    
    
function [TRIedges, PHIedges, edgeSimps, edgeVerts, EDGES] = bsplinen_getFullTriangulationEdges(TRI, PHI, EDGES, alledges)

    bsplinen_structures;
    
    TRIedges = {};
    PHIedges = {};
    edgeSimps = [];
    
    if (nargin < 3)
        % get all edges in the triangulation
        EDGES = bsplinen_getEdges(PHI, TRI);
        alledges  = 1;
    elseif (nargin < 4)
        if (isempty(EDGES))
            EDGES = bsplinen_getEdges(PHI, TRI);
        end            
        alledges = 1;
    end
    
    
    % dimension of TRI
    n = size(PHI, 2);
    
    T = size(TRI, 1);

    
    % compile a list that translates simplices to edges
    SimpEdgeList = cell(T, 1);
    for i = 1:size(EDGES, 1)
        edge = EDGES{i};
        SimpEdgeList{edge.simplex1} = [SimpEdgeList{edge.simplex1}; i];
        SimpEdgeList{edge.simplex2} = [SimpEdgeList{edge.simplex2}; i];
    end
    
    idxset = (1:n+1)';
    % the permutation vector for the edge facet indices    
    facetidx = nchoosek(idxset, n);

    % the triangulation edges are all edge facets that are not in EDGES...
    TRIedges = cell(T, 1);
    PHIedges = cell(T, 1);
    edgeSimps = zeros(T, 1);
    edgeVerts = zeros((n+1)*T, 1);
    
    % go through all simplices
    evcount = 0;
    for i = 1:T
        % vertex indices of simplex i
        simpidx = TRI(i, :);
        % get the facets of simplex i
        facets = simpidx(facetidx);
        
        % The principle is that any facets that are not in any edge structure, are edge facets!

        edgeidx = SimpEdgeList{i};
        if (isempty(edgeidx))
            % no edges, so all facets are edge facets!
            TRIedges{i} = facets;
            PHIedges{i} = PHI(facets', :);
            edgeVerts((evcount+1):(evcount+numel(facets))) = facets(:);
            evcount = evcount + numel(facets); % total number of vertex indices for this facet
            continue;
        end
        
        % Go through all facets and see if they are edge facets
        edgefacetlist = zeros(size(facets, 1), 1);
        edgefacetcount = 1;
        hasedgefacets = 0;
        for fi = 1:size(facets, 1)
            facet = facets(fi, :);
            isedgefacet = 1;
            % go through all edges of simplex 'simp'
            for j = 1:length(edgeidx)
                edge = EDGES{edgeidx(j)};
                ev = edge.edgevertices;
                % find the mutual vertex indices in the current facet and the edge vertices
%                 mutualverts = intersect(facet, ev); % slow function
%                 if (length(mutualverts) == n)
%                     % there are n mutual vertices, so this facet is actually an edge facet!
%                     isedgefacet = 0;
%                     break;
%                 end
                if (~any(facet' - ev))
                    % there are n mutual vertices, so this facet is actually an edge facet!
                    isedgefacet = 0;
                    break;
                end
            end
            if (isedgefacet)
                edgefacetlist(edgefacetcount) = fi;
                edgefacetcount = edgefacetcount + 1;
                hasedgefacets = 1;
            end
            
        end
        
        if (hasedgefacets)
            edgefacetlist = edgefacetlist(1:edgefacetcount-1);
            % insert edge facets
            edgefacets = facets(edgefacetlist, :);
            TRIedges{i} = edgefacets;
            PHIedges{i} = PHI(TRIedges{i}', :);
            edgeSimps(i) = 1;
            edgeVerts(evcount+1:evcount+numel(edgefacets)) = edgefacets(:);
            evcount = evcount + numel(edgefacets); % total number of vertex indices for this facet
        end
        
        
    end
    
    % cutoff the edge vertex list, and make it unique
    edgeVerts = unique(edgeVerts(1:evcount));
    

    
function [Iinternal, Iall, Iedge] = bsplinen_getInternalVertices(DT, PHI, EDGES)

    Iinternal = [];
    Iall      = [];
    Iedge     = [];
    
    n = size(PHI, 2);
    
    % check whether the input DT is a cell array holding simplices (DELTA), or a vertex index array (TRI)
    if (iscell(DT))
        % translate DELTA to TRI
        TRI = zeros(length(DT), n+1);
        for i = 1:length(DT)
            TRI(i, :) = DT{i}.vertexindices;
        end
    else
        TRI = DT;
    end
    
    % get the triangulation edges, if they have not been supplied yet
    if (nargin < 3)
        [TRIedges PHIedges edgeSimps Iedge] = bsplinen_getFullTriangulationEdges(TRI, PHI);  
    else
        [TRIedges PHIedges edgeSimps Iedge] = bsplinen_getFullTriangulationEdges(TRI, PHI, EDGES);  
    end
    
    % the internal vertices is the set difference between the vertices in TRI and the external vertices
    Iall = unique(TRI(:));
    Iinternal = setdiff(Iall, Iedge);

    
    
function Btk = bsplinen_constructBtk(polybasis, basisKt, Lambda)

    % the total number of B-coefs
    sizeBt = size(polybasis, 1);

    if (isempty(Lambda))
        Btk = [];
        return 
    end
    
    % the total number of datapoints
    sizeX = size(Lambda, 1);

    %   Computation of Btk
    Btk = zeros(sizeX, sizeBt);
    for i = 1:sizeBt
        Btk(:,i)     = basisKt(i)*prod(Lambda.^(ones(sizeX,1)*polybasis(i,:)),2);
    end
    
    
function [continuityConditions, Cfilter] = bsplinen_continuityConditions_M(polybasis, edge, gammas, m, Cfilter, CIndexArr, options)


    % columns: 1 = c1, 2 = c2 + gamma, 3 = gamma
    continuityConditions = cell(1, 3);
    condcount = 1;
   
    size_polybasis = size(polybasis, 1);
    %n = np1-1;
    
    % Construct the continuity conditions for every order up to and
    % including 'order'.

    order = m;

    % get the precalculated gamma's
    gamma = gammas{order + 1};

    % get the target control net coefficients based on the column of the ssv1
    ctargets = bsplinen_getCTargets(polybasis, edge.cssv1, order);

    % Filter the continuity conditions on uniqueness if required. Note
    % that this will (at this point) only work for 0-th order continuity!
    if (options.filterH == 1 && order == 0)
        % get the subject coefficients in simplex1
        vertexcount = 0;
        for i = 1:size(ctargets, 1) %length(ctargets(:, 1))
            c1 = ctargets(i, :);

            % filtering is only neccessary for B-coefs located at simplex vertices (for m = 0!)
            if (nnz(c1) == 1)
                vertexcount = vertexcount + 1;
                % check whether the vertex at this B-coef is an inner vertex
                if (Cfilter(edge.edgevertices(vertexcount), 2) == 1) 
                    Cfilter(edge.edgevertices(vertexcount), 3) = Cfilter(edge.edgevertices(vertexcount), 3) + 1;
                    if (Cfilter(edge.edgevertices(vertexcount), 3) >= Cfilter(edge.edgevertices(vertexcount), 1))
                        % skip this equation
                        continue;
                    end
                end
            end
            [c1index c1indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex1, c1);
            % get all subject control net coefficients
            csubjects = bsplinen_getCSubjects(polybasis, edge.cssv1, edge.cssv2, c1, gamma);            
            [c2index c2indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex2, csubjects);

            % store the calculated data in continuity conditions list
            continuityConditions{condcount, 1} = order;
            continuityConditions{condcount, 2} = c1;
            continuityConditions{condcount, 3} = c1index;
            continuityConditions{condcount, 4} = c1indexlocal;
            continuityConditions{condcount, 5} = csubjects;
            continuityConditions{condcount, 6} = c2index;
            continuityConditions{condcount, 7} = c2indexlocal;
            continuityConditions{condcount, 8} = gamma;

            condcount = condcount + 1;

        end
    else
        % get the subject coefficients in simplex1
        for i = 1:size(ctargets, 1) %length(ctargets(:, 1))

            c1 = ctargets(i, :);
            [c1index c1indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex1, c1);
            % get all subject control net coefficients
            csubjects = bsplinen_getCSubjects(polybasis, edge.cssv1, edge.cssv2, c1, gamma);            
            [c2index c2indexlocal] = bsplinen_getcindex(size_polybasis, CIndexArr, edge.simplex2, csubjects);

            % store the calculated data in continuity conditions list
            continuityConditions{condcount, 1} = order;           
            continuityConditions{condcount, 2} = c1;
            continuityConditions{condcount, 3} = c1index;
            continuityConditions{condcount, 4} = c1indexlocal;
            continuityConditions{condcount, 5} = csubjects;
            continuityConditions{condcount, 6} = c2index;
            continuityConditions{condcount, 7} = c2indexlocal;
            continuityConditions{condcount, 8} = gamma;

            condcount = condcount + 1;

        end
    end    


     



    
