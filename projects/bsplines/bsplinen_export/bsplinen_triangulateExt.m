% BSPLINEN_TRIANGULATEEXT determines the simplices, the simplex edges and the
%  Star structures from a given set of vertices or a given triangulation.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2009
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.2
%               
%   [TRI, DELTA, EDGES] = BSPLINEN_TRIANGULATE2(X, DELTA, OPTIONS) 
%       constructs the structure of simplices (DELTA), the edges between
%       simplices (EDGES) of the given vertex set. 
%
function [TRI, DELTA, Edges] = bsplinen_triangulateExt(X, DELTA, bopts)
    
    % get the structures
    global STRUCTURES_LOADED;
    global struct_bsplinen_options;
    global struct_simplex;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    else
        if (isempty(STRUCTURES_LOADED))
            bsplinen_structures;
        end
    end
    
    % load default options structure if none is supplied
    if (nargin < 3)
        bopts = struct_bsplinen_options;
    end        
    if (isempty(bopts))
        bopts = struct_bsplinen_options;
    end
    if (nargin < 2)
        DELTA = {};
    end

    % retrieve input from input parameters
    if (iscell(X) == 1)
        if (length(X) == 2)
            if (isempty(X{1}) == 1)
                warning('Invalid input parameter, the parameter X must contain 2 non-empty sets: PHI and TRI, returning...');
                return;
            end            
            PHI = X{1};
            if (isempty(X{2}) == 0)
                TRI = X{2};
            else
                TRI = delaunayn(PHI, bopts.tri_delaunayopts);           
            end
        else
            warning('Invalid input parameter, the parameter X must contain 2 sets: PHI and TRI, returning...');
            return;
        end
    else
        PHI = X;
        TRI = delaunayn(PHI, options.tri_delaunayopts);
    end
    
    
    % sort the vertex indices in TRI (IMPORTANT!!!)
    TRI = sort(TRI, 2);
    % the total number of simplices
    T = length(TRI(:, 1));
    % the number of vertices per simplex
    v_count = length(TRI(1, :));
    % dimension
    n = v_count - 1;

    % build the simplex list, but only if DELTA is empty
    if (isempty(DELTA))
        DELTA = cell(T, 1);
        for i = 1:T

            simplex = struct_simplex;
            simplex.index = i;
            simplex.n = n;

            simplex.vertices = PHI(TRI(i, :), :);
            simplex.vertexindices = TRI(i, :); %1:n+1; % CDV 29-09-2011
            DELTA{i, 1} = simplex;

        end
    else
        if (size(DELTA, 1) ~= T)
            warning('BSPLINEN_TRIANGULATE: Given DELTA (%d simplices) is incompatible with given TRI (%d simplices): ', size(DELTA, 1), T);
        end
    end

    % retrieves all full edges in the triangulation TRI
    Edges = bsplinen_getEdges(PHI, TRI);
 
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               HELPER FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
function [NB, NI, NEV] = bsplinen_findneighbors(TRI, nCount)
    
    % declare cell arrays
    NB = {};    % vertex index array
    NI = {};    % simplex index array
    NEV = {};   % edge vertex array
    
    % The simplex dimension
    n = size(TRI, 2) - 1; 
    % Total number of simplices
    T = size(TRI, 1);
    % maximum simplex index
    vidxMax = max(TRI(:));

    % Return if there are less than 2 simplices in TRI or nCount has been
    % chosen smaller than 1.
    if (length(TRI) <= 1 || nCount < 1)
        disp('FINDNEIGHBOURS2: ERROR, the number specifying the minimum vertex count is too small, returning...');
        return;
    end
    
    % If the required number of shared vertices is larger than or equal to the simplex
    % dimension n, it must be reduced to n-1, as it does not make sense to
    % try and match every vertex between two simplices (which only happens
    % when the triangulation is erroneous).
    if nCount > n
        disp(sprintf('FINDNEIGHBOURS2: WARNING, The dimension of the simplices in TRI is %d. \n The required number of shared vertices was chosen to be %d. The required shared vertex count will be reduced to %d', n, nCount, n));
        nCount = n;
    end
   
    
    % create the row indices into the sparse adjacency matrix
    zeros(T, n+1);
    for i = 1:n+1
        rowIdx(:, i) = (1:T)';
    end
    rowIdxLin = rowIdx(:);

    % create the sparse adjacency matrix
    TRIADJ = sparse(rowIdxLin, TRI(:), 1, T, vidxMax);
    
    
    % now go through TRI and retrieve all simplices having nCount equal
    % vertices
    for i = 1:T
        % the to-be-tested simplex
        SIMPTEST = TRI(i, :);
%         % the adjacency information for the to-be-tested simplex
%         SIMPTESTIDX = TRIADJ(i, :);
        
        % the columns holding the vertex indices of SIMPTEST
        colIdx = ones(T, 1) * SIMPTEST;
        colIdxLin = colIdx(:);
        
        % the binary adjacency matrix for a single simplex
        TRIADJTEST = sparse(rowIdxLin, colIdxLin, 1, T, vidxMax);
        
        % perform the boolean adjacency test
        TRIBOOL = TRIADJ & TRIADJTEST;
        % count the number of vertex matches per simplex
        TRICOUNT = sum(TRIBOOL, 2);
        % the neigboring simplices have n matching vertices
        NbSimpsIdx = find(TRICOUNT == nCount);
                
        % counter keeping track of neighbour count
        ncount = 0;
        
        % add the data of the neigboring simplices of SIMPTEST to the lists 
        for j = 1:length(NbSimpsIdx)
            ncount = ncount + 1;
            % insert the neigbouring simplex itself
            nsimp = TRI(NbSimpsIdx(j), :);
            NB{i, ncount} = nsimp;
            % and insert its index
            NI{i, ncount} = NbSimpsIdx(j);
            % add the edge vertices
            NEV{i, ncount} = find(TRIBOOL(NbSimpsIdx(j),:))';
        end
        
       
        
    end

    
    
function Lambda = bsplinen_cart2bary(simplex, X)
    
    % The reference vertex is always chosen as the first simplex vertex.
    % This can be done because barycentric coordinates are not dependent on
    % the reference point.
    v0      = simplex(1, :);
    vcount2 = size(simplex, 1) - 1;
    Xcount  = size(X, 1);
    
    Lambda = zeros(Xcount, vcount2 + 1);
    %vcount2 = length(simplex(:, 1)) - 1;
    
    % assemble matrix A
    A = zeros(vcount2, vcount2);
    count = 1;
    for i = 2:length(simplex(:, 1))
        A(:, count) = (simplex(i, :) - v0)';
        count = count + 1;
    end
    
    
    for i = 1:Xcount
        % relative coordinates of x
        p = (X(i, :) - v0)';

        % the last (n) barycentric coordinates. 
        lambda1 = A \ p;

        % the first barycentric coordinate; lambda0
        lambda0 = 1 - sum(lambda1);

        % insert lambda0 into the Lambda vector
        Lambda(i, 1) = lambda0;
        Lambda(i, 2:end) = lambda1;
    end

 
function [ssvIndex ssvCol] =  bsplinen_getSSV(simplex1, simplex2)

    % simplex 1 and simplex 2 must both be row vectors!
    
    % find the ssv: the ssv is not shared between simplices!
    ssvIndex = 0;
    ssvCol = 0;
    for i = 1:length(simplex1)
    
        edgevert = find(simplex1(i) == simplex2(:));
        if (isempty(edgevert) == 1)
            ssvIndex = simplex1(i);
            ssvCol = i;
            break;
        end
    
    end    
    
    
function Edges = bsplinen_getEdges(PHI, TRI)

    global STRUCTURES_LOADED;
    global struct_edge2;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    end
    
    T = size(TRI, 1);

    % the number of vertices per simplex
    v_count = length(TRI(1, :));
    % dimension
    n = v_count - 1;

    %EdgeMap = sparse(length(TRI(:, 1)), length(TRI(:, 1)));
    EdgeMap = sparse(size(TRI, 1), size(TRI, 1));
    Edges = {};

    % find the natural neighbours for every simplex in TRI
    [NB, NI, NEV] = bsplinen_findneighbors(TRI, n);
    
    
    if (isempty(NB))
        return;
    end
    
    edgeCount = 1;
    % go through all simplices
    % for i = 1:T
    for i = 1:size(NB, 1)
        % define the ssv's for this edge
        % go through all neighboring simplices of the simplex at 'i'
        for nb = 1:size(NB, 2)

            if (~isempty(NB{i, nb}))

                % setup the edge structure
                edge = struct_edge2;
                edge.index = edgeCount;                
                edge.simplex1 = i;
                edge.simplex2 = NI{i, nb};
                edge.edgevertices = NEV{i, nb};
                
                % get the ssv of the first simplex 
                [edge.ssv1 edge.cssv1] = bsplinen_getSSV(TRI(i, :), NB{i, nb});
                % get the ssv of the second simplex 
                [edge.ssv2 edge.cssv2] = bsplinen_getSSV(NB{i, nb}, TRI(i, :));

                % calculate the barycentric coordinates of the SSV's with
                % respect to the target simplices
                edge.ssv1bary = bsplinen_cart2bary(PHI(TRI(edge.simplex2, :), :), PHI(edge.ssv1, :));
                edge.ssv2bary = bsplinen_cart2bary(PHI(TRI(edge.simplex1, :), :), PHI(edge.ssv2, :));

                % check whether we already have an edge for the same
                % set of simplices (the dual edge...).
                s1 = edge.simplex1;
                s2 = edge.simplex2;
                if (s1 > s2)
                    s1 = edge.simplex2;
                    s2 = edge.simplex1;
                end
                if (EdgeMap(s1, s2) == 0)
                    EdgeMap(s1, s2) = 1;
                    Edges{edgeCount, 1} = edge;
                    edgeCount = edgeCount + 1;
                end
                
            end

        end 

    end    
    
    
    
    
    
    
    