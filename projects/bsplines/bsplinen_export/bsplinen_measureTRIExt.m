% BSPLINEN_MEASURETRIExt measures angles and volumes of all simplices in a
%  given triangulation
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2010
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
function [Angles, Volumes, Spheres, Metric, Data] = bsplinen_measureTRIExt(PHI, TRI, XI)
    
    
    n           = size(PHI, 2);
    np1         = n + 1;
    T           = size(TRI, 1);

    Angles      = zeros(T, 2);
    Volumes     = zeros(T, 1);
    Spheres     = zeros(T, np1);
    Data        = zeros(T, 1);
    Metric      = 0;

    dodatasearch = 0;
    if (nargin > 2)
        if (~isempty(XI))
            dodatasearch = 1;
        end
    end
    
    RSum = 0;
    % go through all simplices and measure properties
    for i = 1:T
        simp = PHI(TRI(i, :), :);
        % get the min/max angles and the simplex volume
        [minangle maxangle vol] = bsplinen_measureSimplex(simp);
        % get the center and radius of the circumsphere of the simplex
        [center radius] = bsplinen_getCircumSphere(simp);

        Angles(i, :)    = [minangle maxangle];
        Volumes(i)      = vol;
        Spheres(i, :)   = [center radius];

        RSum = RSum + radius;
    end

    % The Delaunay performance metric: the sum of radii of all circumspheres
    % of all simplices divided by the total number of simplices.
    Metric = RSum / T;
    
    if (dodatasearch)
        IMap = bsplinen_tsearchn(PHI, TRI, XI);
        IMap = IMap(~isnan(IMap));
        for i = 1:size(IMap)
            Data(IMap(i)) = Data(IMap(i)) + 1;
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %           Helper Functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [center radius] = bsplinen_getCircumSphere(simp)

        n = size(simp, 2);

        % the last vertex
        vN = simp(end, :);
        % First solve for the circumcenter coordinates by subtracting the
        % radius equations from each other and solving the resulting linear
        % system: 
        % A * x = B, with x the circumcenter coords
        A = zeros(n, n);
        B = zeros(n, 1);
        for i = 1:n
            A(i, :) = 2*vN - 2*simp(i, :);
            B(i) = sum(vN.^2) - sum(simp(i, :).^2);
        end
        % solve for center
        center = (A \ B)'; % Beautiful!
        % now determine the radius
        radius = sqrt(sum((vN - center).^2));


    function [minAngle maxAngle Vol minEdge maxEdge minEdgeIdx maxEdgeIdx] = bsplinen_measureSimplex(simpPHI)

        minAngle = 0;
        maxAngle = 0;
        Vol = 1;

        n = size(simpPHI, 1) - 1;
        if (n == 1)
            % calculate only the length of the simplex
            Vol = norm(simpPHI(2,:) - simpPHI(1,:));
        elseif (n == 2)
            % calculate angles with cosine rule
            a = norm(simpPHI(2,:) - simpPHI(1,:));
            b = norm(simpPHI(3,:) - simpPHI(1,:));
            c = norm(simpPHI(3,:) - simpPHI(2,:));
            alpha(1) = acosd((a^2 + b.^2 - c^2) / (2*(a*b)));
            alpha(2) = acosd((b^2 + c.^2 - a^2) / (2*(b*c)));
            alpha(3) = 180 - alpha(1) - alpha(2);

            minAngle = min(alpha);
            maxAngle = max(alpha);
            Vol = 1/2 * a * b*abs(sind(alpha(1)));
        else
            % This is the general formula for the volume of a simplex
            % assemble matrix A
            v0      = simpPHI(1, :);
            A  = (simpPHI(2:end,:)-ones(size(simpPHI,1)-1,1)*v0)';

            Vol = 1 / factorial(n) * det(A);
        end

        % measure edgelengths of simplex
        maxEdge = -1;
        minEdge = inf;

        for i = 1:size(simpPHI, 1)-1
            vi = simpPHI(i, :);
            for j = (i+1):size(simpPHI, 1)
                vj = simpPHI(j, :);
                rad = norm(vi - vj);
                if (rad > maxEdge)
                    maxEdge = rad;
                    maxEdgeIdx = [i j];
                end
                if (rad < minEdge)
                    minEdge = rad;
                    minEdgeIdx = [i j];
                end
            end
        end
    
    
    
    
    
    
    