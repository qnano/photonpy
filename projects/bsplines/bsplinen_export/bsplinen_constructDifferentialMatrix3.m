% bsplinen_constructDifferentialMatrix3.m
%
%   Syntax:
%       [DC, dfactor, CasteljauMats] = bsplinen_constructDifferentialMatrix3(TRI, PHI, d, X, U, Ddegree)
%
%   Description:
%       This function constructs differential constraints for the global
%       B-coefficients of a spline function FN at the points in X such that DC*fn.coefs = K.
%
%       Input parameters:
%           - TRI       : double [J x N+1]
%           - PHI       : double [V x N]
%           - d         : double [1 x 1]
%           - X         : double [M x N]
%           - U         : double [1 x N]
%
%           TRI     The triangulation in the form of vertex indices.  
%           PHI     The set of vertices.
%           d       The polynomial degree
%           X       points at which differential constraints hold. Rows of
%                   X are the points, columns of X the dimensional components. 
%           U       the direction of the derivative in global Cartesian coordinates.
%
%       Output parameters:
%           - DC        : double [M x dhat*J]
%
%           DC are the differential constraints such that DC*fn.coefs = K,
%           with DC containing as much rows as there are points in X. K is
%           a constant vector holding the actual constraint values.
%
%   -------------------------------
%   File part of Simplex spline toolbox
%   C.C. de Visser 
%   20-12-2010
%   updated 10-10-2014
%   -------------------------------
function [DC, dfactor, CasteljauMats] = bsplinen_constructDifferentialMatrix3(TRI, PHI, d, X, U, Ddegree)
    
    n       = size(PHI, 2);
    T       = size(TRI, 1);
    dhat    = bsplinen_calcdHat(n, d);
    
    CasteljauMats = cell(T, 1);
    
    % build derivative polybasis
    derivbasis = bsplinen_constructBasis(n, d-Ddegree);
    derivKt =  bsplinen_constructBasisKt(derivbasis);

    IMap = cell(T, 1);
    LDat = cell(T, 1);
    totalDataCount = 0;
    
    % go through all simplices in TRI and construct the per-simplex de
    % Casteljau matrix and Btk matrices.
    for t = 1:T
        
        % get barycentric coordinates of points X with respect to simplex t
        [imap ldat] = bsplinen_tsearchn(PHI, TRI(t, :), X, 100000);
        
        if (all(isnan(imap)))
            continue;
        end
        nanidx = isnan(imap);
        imap(nanidx) = [];
        ldat(nanidx, :) = [];
      
        % set barycentric coordinates of subset X with respect to T
        IMap{t} = imap;
        LDat{t} = ldat;
        totalDataCount = totalDataCount + length(imap);
        
        % Calculate the barycentric coordinates of U with respect to simplex t.
        U = U./norm(U, 2); % normalize U
        simpt = PHI(TRI(t,:), :); % vertices of simplex t
        Z = simpt(1,:) - U; % normalized relative direction
        alpha = bsplinen_cart2bary(simpt, simpt(1,:));
        beta = bsplinen_cart2bary(simpt, Z);
        % the directional coordinate for simplex t
        a = alpha - beta;     
        
        % Build de Casteljau matrix using the directional coordinate a
        CastelTot = bsplinen_constructDeCasteljauMatExt(a, d);
        for j = 1:Ddegree-1
            Castel = bsplinen_constructDeCasteljauMatExt(a, d-j);
            CastelTot = Castel * CastelTot;
        end
        CasteljauMats{t} = CastelTot;        
        
    end
    
    
    % set the derivative factor as in theorem 2.15 of [Lai and Schumaker, 2007]
    dfactor  = factorial(d)/(factorial(d-Ddegree));

%     % now go through all datapoints and construct polynomial basis
%     DC = sparse(size(X,1), T * dhat);
%     for i = 1:size(X, 1)
%        
%         simpidx = IMap(i);
%         if (isnan(simpidx))
%             continue;
%         end
%         
%         % construct the polynomial basis values of X
%         Btk = bsplinen_constructBtk(derivbasis, derivKt, LDat(i, :));
%         
%         % fill the DC matrix
%         idx = (simpidx-1)*dhat+1:simpidx*dhat;
%         DC(i, idx) = dfactor * Btk * CasteljauMats{simpidx};
%         
%     end
    
    % now go through all simplices, and construct DC matrix for all points contained by it
    DC = sparse(totalDataCount, T * dhat);
    DCrows = 1;
    for t = 1:T
       
%         simpidx = IMap(i);
%         if (isnan(simpidx))
%             continue;
%         end
        ldat = LDat{t};
        if (isempty(ldat))
            continue;
        end
        
        % construct the polynomial basis values of X
        Btk = bsplinen_constructBtk(derivbasis, derivKt, ldat);
        
        % fill the DC matrix
        idx = (t-1)*dhat+1:t*dhat;
        DC(DCrows:(DCrows+size(ldat, 1)-1), idx) = dfactor * Btk * CasteljauMats{t};
        DCrows = DCrows + size(ldat, 1);
    end    
    
    
    
    
