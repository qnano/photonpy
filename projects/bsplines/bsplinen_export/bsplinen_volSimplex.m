% bsplinen_volSimplex calculates the volume of an N-Simplex
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2015
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%
function Vol = bsplinen_volSimplex(simpPHI)

    Vol = 1;
    
    n = size(simpPHI, 1) - 1;

    % This is the general formula for the volume of a simplex
    v0      = simpPHI(1, :);
    A  = (simpPHI(2:end,:)-ones(size(simpPHI,1)-1,1)*v0)';

    Vol = 1 / factorial(n) * det(A);

    


