% bsplinen_calcTriangulationEdgeNormals calculates the normal vectors to a given set of edges
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2014
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%   Normals = bsplinen_getSimplexNormals(Edges, PHIedges)
%
function Normals = bsplinen_calcEdgeNormals(TRI, PHI, PHIedges, doOutwardDir)
    
    if (nargin < 4)
        % disable direction finding of the normal (inward or outward)
        doOutwardDir = -1;
    end

    E = length(PHIedges);

    % the vector with normals
    Normals = cell(E,1);
    
    % go through all edges and determine normals
    for i = 1:E
        if (isempty(PHIedges{i}))
            continue;
        end
        phiedge = PHIedges{i};
        n = size(phiedge, 2);
        
        % calculate normals by taking pairs of n+1 vertices in phiedge (some simplices may have more than a single external edge)
        normvectors = zeros(size(phiedge, 1)/n, n);
        count = 1;
        for j = 1:n:size(phiedge,1)
            phie = phiedge(j:j+n-1, :);
            
            % formulate hyperplane equations of the form:
            % a1*x1 + a2*x2 + ... + an*xn - c = 0, with ai the plane coefficients and with xi the points in Rn. 
            % In matrix form this becomes:
            % [x1 -1; x2 -1; ... -1; xn -1] * [a1; a2; ...; an; c]; -> = X * ac = 0; -> ac \in null(X)
            A = [phie -1*ones(size(phie, 1), 1)];
            nA = null(A);
            
            normvect = nA(1:n)';
            normvect = normvect./norm(normvect);
                
            % check direction of normal, and switch it to requested dir
            % check direction of normal, and switch it to requested dir
            if (doOutwardDir > -1)
                simp = PHI(TRI(i, :), :);
                midpoint = mean(phie, 1);
                dn = 1/1000 * norm(phie(1,:)-midpoint);
                pos = midpoint + normvect * dn; % positive direction
                npos = midpoint - normvect * dn; % negative direction

                barypos = bsplinen_cart2bary(simp, [pos; npos]);
                if (all(barypos(1,:) > 0))
                    % for positive direction normal points INSIDE simplex
                    if (doOutwardDir)
                        normvect = -normvect;
                    end
                else
                    % for positive direction normal points OUTSIDE simplex
                    if (~doOutwardDir)
                        normvect = -normvect;
                    end
                end
                % use for checking
                if (all(barypos(2,:) > 0))
                    % for negative direction normal points INSIDE simplex
                else
                    % for negative direction normal points OUTSIDE simplex
                end

            end
            normvectors(count, :) = normvect;

            count = count + 1;
            
%             figure; axis equal; line(phie(:,1), phie(:,2)); hold on; line([vert1(1) vert1(1)+normvect(1)], [vert1(2) vert1(2) + normvect(2)],'color', 'r');
%             vert1 = phiedge(j, :);
%             vert2 = phiedge(j+1, :);
%             check = (vert2-vert1) * normvect;

        end
        
        Normals{i} = normvectors;
        
    end
    

    