% BSPLINEN_EVALEXT.m Evaluates an N-variate B-Spline function
%
%   Syntax:
%       [V InclusionMap LambdaData] = bsplinen_evalExt(fn, X, options)
%
%   Description:
%       This function evaluates an N-variate B-spline FN at locations X by
%       directly evaluating the B-form, instead of running the  de
%       Casteljau algorithm. The resulting evaluation is many times faster
%       than the method proposed by Hu in [Hu et al., 2007]. This function
%       uses  the built in Matlab function TSEARCHN to assign the
%       evaluation points to  their respective simplices, and calculates
%       the barycentric coordinates  of the points with respect to their
%       enveloping simplex.  When an evaluation point X is outside of the
%       convex hull of the basis  of FN, then the B-spline will evaluate as
%       NAN. This function differs from BSPLINEN_EVAL by not requiring any
%       external functions, and by not offering native (MEX) evaluation support.
%
%   Input parameters are:
%       - fn      : bspline structure
%       - X       : double [N x n] or cell [1 x 1] or cell [1 x 2]
%       - options : STRUCT_BSPLINEN_OPTIONS struct (optional)
%
%       FN a STRUCT_BSPLINEN structure
%
%       X Can be one of the following:
%           1)  X is an [N x n] matrix holding locations at which the N-variate
%                 B-spline will be evaluated.
%           2)  X is a [1x1] cell array holding STRUCT_SIMPLEX structures. The
%                 structures are the same as used by BSPLINEN.
%           3)  X is a [1x2] cell array with the following structure:
%                 X = {InclusionMap, LambdaData}. That is, the first
%                 element holds the simplex membership indices and the
%                 second element barycentric coordinates.
%
%       OPTIONS is a STRUCT_BSPLINEN_OPTIONS struct holding various switches, see bsplinen_structures.m
%
%       Output from BSPLINEN_EVAL1 is:
%           - V             : double [N x 1]
%           - InclusionMap  : double [N x T]
%           - LambdaData    : doubel [N x n+1]
%
%       V             the values of the N-variate B-spline FN at locations
%                     X.
%       INCLUSIONMAP  is a matrix denoting which simplex contains each data
%                     point
%       LAMBDADATA    are the lambda coordinates per data point
%                     correpsponding to the simplex which contains the data
%                     point.
%
%   See also bsplinen_evalBform
%
%   -------------------------------
%   File part of Simplex spline toolbox
%   C.C. de Visser
%   10-08-2010
%   -------------------------------
function [V InclusionMap LambdaData] = bsplinen_evalExt(fn, X, options)

    
    V            = [];
    InclusionMap = [];
    LambdaData   = [];

    %   Checking is show is defined. If not then set to false
    if (nargin < 2)
        error('BSPLINEN_EVALEXT Requires at least 2 arguments: FN the STRUCT_BSPLINEN structure, and X wich are the evaluation locations');
    end
    if (nargin < 3)
        % load default options structure
        options = bsplinen_constructOptionsStructExt();
    end
 
    %   Checking if X is a cell array or not
    if (iscell(X) == 0)
        %   X is not a cell array

        %   Determine amount of data points
        Xsize = size(X, 1);
        if (options.outputmsg == 1 && (Xsize > 1000)) 
            fprintf('BSPLINEN_EVALEXT: Evaluating Simplex Spline at %d locations...\n', Xsize);
        end

        
        % Evaluate the spline function
        
        % Calculate the inclusions of X, together with barycentric coordinates of X. 
        partsize = 250000;
        if (Xsize < partsize)
            %   Dataset X is small enough to handle all at once
            [InclusionMap, LambdaData] = tsearchn(fn.PHI, fn.TRI, X);
        else
            %   Dataset X is too large to handle all at once. Perform the
            %   operations is parts
            partcount   = ceil(Xsize / partsize);

            %   Display information in command window
            if options.outputmsg == 1 
                fprintf('BSPLINEN_EVALEXT: Dataset is partitioned in to %d partitions of size %d.\n', partcount, partsize);
            end

            %   Initialization of the variables
            InclusionMap    = zeros(size(X,1),1);
            LambdaData      = zeros(size(X,1),size(X,2)+1);

            %   Performing the operations for each subpart of X...
            %   .. initialize indices
            xistart     = 1;
            xiend       = xistart + partsize;
            for i = 1:partcount
                %   ... Display information in command window
                if options.outputmsg == 1
                    fprintf('BSPLINEN_EVALEXT: Performing simplex membership search for data partition %d...\n', i);
                end

                %   ... Compute the inclusion map and barycentric coordinates
                %   of a part of X
                [IMaptmp LDattmp]= tsearchn(fn.PHI, fn.TRI, X(xistart:xiend, :));

                %   ... Store results
                InclusionMap(xistart:xiend, :) = IMaptmp;
                LambdaData(xistart:xiend, :)   = LDattmp;

                %   ... Update indices
                xistart = xiend + 1;
                xiend   = xiend + partsize;
                if (xiend > Xsize)
                    xiend = Xsize;
                end
            end
        end

        %   Evaluate the B-spline!
        V = bsplinen_evalBformExt(fn, InclusionMap, LambdaData);
        
    else
        % X is a cell array
        if (length(X) == 1)
            % X is a cell array hold STRUCT_BSPLINEN structures.
            % bsplinen_structures;
            DELTA = X{1};

            %   Determine total amount of bsplinen_structures in DELTA
            T = length(DELTA);

            %   First go through all simplices and count the total amount of
            %   data.
            dcount = 0;
            for i = 1:T
                dcount = dcount + DELTA{i}.datacount;
            end

            %   Initialize the InclusionMap and LambdaData variables
            InclusionMap    = zeros(dcount, 1);
            LambdaData      = zeros(dcount, fn.dim+1);

            %   Fill the InclusionMap and LambdaData
            dcount = 0;
            for i = 1:T
                %   Extracting simplex from DELTA
                simplex     = DELTA{i};

                %   Filling variables
                InclusionMap(dcount+1:dcount+simplex.datacount,:) = i;
                LambdaData(dcount+1:dcount+simplex.datacount,:)   = simplex.databary;

                %   Updating dcount
                dcount = dcount + simplex.datacount;
            end

        elseif (length(X) == 2)
            %   The inclusionmap and lambda data are supplied by X
            InclusionMap = X{1};
            LambdaData   = X{2};
            if (length(InclusionMap) ~= length(LambdaData(:,1)))
                warning('BSPLINEN_EVAL: When X is a cell array with two elements, than both elements MUST be matrices with an equal number of rows, returning...');
                V   = [];
                return;
            end
            
        end
        
        % Evaluate the spline function using the Matlab evaluator
        V = bsplinen_evalBformExt(fn, InclusionMap, LambdaData);

        
    end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the B-form polynomial
function V = bsplinen_evalBformExt(fn, InclusionMap, LambdaData)

    %   Determine the total number of evaluation points
    Xcount  = size(InclusionMap, 1);

    %   Initialize output
    %   (Multiplication with *coefs1(1,1)*coefs2(1,1) performed to
    %    ensure compatibility with interval computations)
    V       = nan(Xcount, 1)*fn.coefs(1);

    %   Determine number of simplices in the spline
    T       = size(fn.TRI,1);


    %   Initialize basis, basisKt, and d, dHat;
    [basis basisKt] = bsplinen_constructBasisExt(fn.dim,fn.degree(1));
    d       = ones(T,1)*fn.degree(1);
    dHat    = size(basis,1);

    %   Determine the subsets of the coefficients which correspond to each simplex.
    flagCoefsIdx = false;
    if isfield(fn,'coefsIdx') && size(fn.coefsIdx,1) == size(fn.TRI,1)
        %   indices of the subsets are available
        flagCoefsIdx = true;
    end

    % Check whether the spline function is a derivative from another,
    % in that case the B-form should be multiplied with the
    % differentiation factor. 
    dfactor     = fn.deriv_factor;
    if (dfactor == 0)
        dfactor = 1;
    end

    %   Cycle over all simplices
    counter = 0;
    for t = 1:T
        %   Check if any data points lie in the current simplex
        idx     = InclusionMap==t;
        if any(idx)
            %   Some data points lie in the current simplex

            %   Compute the output of the spline
            basisBt     = zeros(sum(idx),size(basis,1));
            for i = 1:size(basis,1)
                basisBt(:,i) = prod(LambdaData(idx,:).^(ones(sum(idx),1)*basis(i,:)),2);
            end

            V(idx,1) = dfactor*basisBt*(basisKt.*fn.coefs(counter+1:counter+dHat,1));
        end

        %   Update counter
        counter = counter + dHat;
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the polynomial basis and multinomial coefficients
    function [Basis BasisKt] = bsplinen_constructBasisExt(n, d)
        
        % the polynomial orders 0 and 1 require no calculations
        if (d == 0)
            Basis = zeros(1, n+1);
            BasisKt = 1;
            return;
        end

        Basis = eye(n+1, n+1);
        if (d == 1)
            BasisKt = ones(n+1, 1);
            return;
        end

        % Recursively increase the degree of the polynomial basis. This method
        % requires no sorting or removal of nonunique rows. 
        for i = 2:d
            dhat = size(Basis, 1);
            % increment vector of 1's
            inc = ones(dhat, 1);
            % the new matrix of polynomial basis indices
            basisnew = zeros(dhat * (n+1), n+1);
            % Go through all dimensions and add the increment vector (1's) to all
            % elements in the j-th column.
            for j = 1:n+1
                idx = (j-1)*dhat+1:j*dhat;
                basisnew(idx, j+1:end) = Basis(:, j+1:end);
                basisnew(idx, j) = Basis(:, j) + inc;
            end

            % remove all terms that do not sum up to d
            Basis = basisnew(sum(basisnew, 2) == i, :);

        end

        %   Calc the basis coefficient vector |kappa|! / kappa! 
        BasisKt =  factorial(d)./prod(factorial(Basis),2);
        
        
        
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the default STRUCT_BSPLINEN_OPTIONS structure
    function struct_bsplinen_options = bsplinen_constructOptionsStructExt()
        
        struct_bsplinen_options = struct('scheme', 'ls', 'solver', 'iterative', ...
            'parallelCores', 0, ...
            'filterH', 0, 'sortedH', 0, 'fullrankH', 0, ...
            'saveDELTA', 0, 'saveH', 0, 'saveBfb', 0, 'saveCOV', 0, 'saveX', 0, ...
            'loadDELTA', 0, 'loadH', 0, 'loadBfb', 0, 'loadCOV', 0, 'loadX', 0, ...
            'outputmsg', 0, ...
            'outputDELTA', 0, 'outputH', 0, 'outputBfb', 0, 'outputLambda', 0, ...
            'outputInclusionMap', 1,...
            'stat_calc', 0, 'stat_COVmethod', 'LS', 'stat_check', 0, 'stat_outputmsg', 0, ...
            'stat_outputX', 0, 'stat_outputCOV', 0, 'stat_outputSIGMA', 0, 'stat_correlationRadius', .2, ...
            'mreg_reduceMode', 'svd', 'mreg_outputMinv', 1, 'mreg_simpleMode', 0,...
            'resan_calc', 0, 'resan_userelativeRadius', 1, 'resan_radius', .1, ...
            'resan_calcAutocorr', 0, 'resan_perSimplex', 0, 'resan_outputmsg', 0, 'resan_plotstatistics', 0, ...
            'resan_evalGridCount', 10, 'resan_interpGridCount', 50,...
            'data_partsize', 100000, ...
            'bbuild_maxblkdiagsize', 500, 'bbuild_checkrank', 0,...
            'phi_optimizePHI', 0, 'phi_protectCHull', 1, 'phi_normalizedata', 1,...
            'phi_complexityMode', 1, 'phi_complexityLevel', .3, ...
            'phi_minGridCount', 2, 'phi_maxGridCount', 64,...
            'phi_cvhullres', 0.15, 'phi_gridres', 5, ...
            'tri_delaunayopts', [], 'tri_optimizePHI', 0, 'tri_protectCHull', 1, ...
            'tri_mindata', 50, 'tri_maxdata', -1, 'tri_cvhullres', 0.15, 'tri_gridres', 0.2, ...
            'tri_perfreq', 1e-2, 'tri_refinemode', 3, 'tri_refineMaxIts', 25, ...
            'tri_minangle', 20, 'tri_minvolume', .01, 'tri_maxvolume', .25, ...
            'tri_partitionmode', 'grid', 'tri_partitioncount', [], 'tri_partitionoverlap', 0, ...
            'solver_tol', 1e-6, 'solver_gradtol', 1e-10, 'solver_eps', 1e-6, 'solver_maxiter', 250, ...
            'solver_outputmsg', 0, 'solver_maxEpochs', 250, ... 
            'distrib_mode', 0, 'distrib_starlevel', 1, 'distrib_trisubsets', [], 'distrib_deltasubsets', [], ...
            'distrib_outputmsg', 1, ...
            'native_eval', 0, 'native_tsearchn', 0, 'native_construct', 0, 'native_gridres', 32, ...
            'native_fulltsearchn', 0, 'native_dir', '.\\bin', ...
            'saveMatPath', '.\\Matrix');

%------------------------------ end of file -------------------------------
