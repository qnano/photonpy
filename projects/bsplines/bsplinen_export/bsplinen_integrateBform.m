% bsplinen_integrateBform calculates the integral of the given spline function
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2015
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%   Inputs: 
%       fn: [struct_bsplinen] structure; the spline function which will be 
%           integrated over its entire domain, or otherwise on a selected set of 
%           simplices.
%       simplexindices: [length(simplexindex) x 1] vector; is allowed to be empty, otherwise 
%           contains a list of indices of simplices that will be integrated. When 
%           empty, all simplices will be integrated.
%
%   Outputs:
%       intSx: [length(simplexindex) x 1] vector; contains all integrated B-forms, or if simplexindex
%           is empty, a scalar value containing the integral of the entire spline domain.
function intSx = bsplinen_integrateBform(fn, simplexindex)
    
    intall = 0;
    if (nargin == 1)
        intall = 1;
        simplexindex = [1:size(fn.TRI, 1)]';
    end
    
    intSx = zeros(length(simplexindex), 1);
    for i = 1:length(simplexindex)
        simp = fn.PHI(fn.TRI(i, :), :);
        % calculate simplex volume
        volsimp = bsplinen_volSimplex(fn.PHI);
        % calculate simplex integral (based on Lai 2007)
        intSx(i) = volsimp / bsplinen_calcdHat(fn.dim, fn.degree) * sum(fn.coefs);
    end
    
    % if the integral over the entire triangulation is requested...
    if (intall)
        intSx = sum(intSx);
    end

