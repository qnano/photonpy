% BSPLINEN_DERIVEXT constructs the directional derivative of a multivariate spline in B-form.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2009
%              email: c.c.devisser@tudelft.nl
%                          Version: 3.0
%
%   FN_DU = BSPLINEN_DERIV(FN, M, U) constructs the B-form of the directional derivative
%       of order M of the multivariate spline FN. The algorithm is based on
%       Theorem 15 in [Lai and Schumaker, 2007] and makes use of the de
%       Casteljau algorithm.
%
%   Input parameters are:
%
%       FN a multivariate simplex B-spline in the form of a STRUCT_BSPLINEN structure
%
%       M the order of the derivative
%
%       U the direction vector specifying in which direction the directional
%           derivative will determined.
%
%   Output from BSPLINEN_DERIVEXT
%
%       FN_DU the analytical derivative (in B-form) of FN of order M, in the direction
%           of U.
%
function fn_du = bsplinen_derivExt(fn, m, U)

    % set the derivative spline to the default 
    global STRUCTURES_LOADED;
    global struct_bsplinen;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    else
        if (isempty(STRUCTURES_LOADED))
            bsplinen_structures;
        end
    end
    fn_du = struct_bsplinen;
    
    if (m == 0)
        fn_du = fn;
        return;
    end
    
    % normalize U
    U = U./norm(U, 2);
    
    % spline order
    d = fn.degree;
    % spline dimension
    n = fn.dim;
    % set the constant properties of the derivative spline
    fn_du.PHI           = fn.PHI;
    fn_du.TRI           = fn.TRI;
    fn_du.degree        = d - m;
    fn_du.continuity    = fn.continuity - m;
    fn_du.dim           = n;

    if (fn_du.degree < 0)
        return;
    end
    
    % set the derivative factor as in theorem 2.15 of [Lai and Schumaker, 2007]
    if (fn.deriv_factor == 0)        
        fn_du.deriv_factor  = factorial(d)/(factorial(d-m));
    else
        fn_du.deriv_factor  = fn.deriv_factor*factorial(d)/(factorial(d-m));
    end
    
    if (fn.continuity < -1)
        fn.continuity = -1;
    end
    if (m > fn.degree)
        warning('The derivative order must be equal to or less than the spline degree.');
        return;
    end
    
    T = length(fn.TRI(:,1));
    
    dhat = bsplinen_calcdHat(n, d);
    dhatM = bsplinen_calcdHat(n, fn_du.degree);
    
    fn_du.coefs = zeros(dhatM*T, 1);
    
    % calculate the directional coordinates a of u with respect to each of
    % the simplices in fn.TRI, and perform the de Casteljau algorithm for m
    % steps using the directional coordinates.
    for i = 1:T
        simp = fn.TRI(i, :);
        simpverts = fn.PHI(simp, :);
        % just take the first vertex as reference!
        W = fn.PHI(simp(1),:);
        Z = W - U;
        % calculate the barycentric coordinates of W and Z
        alpha = bsplinen_cart2bary(simpverts, W);
        beta = bsplinen_cart2bary(simpverts, Z);
        % the directional coordinate
        a = alpha - beta;        
    
        % Now calculate the new B-coefficient vector c using the values in a.
        % first get the indices in the global B-coefficient vector
        cstart = (i - 1) * dhat + 1;
        cend = (i - 1) * dhat + dhat;
        cstartM = (i - 1) * dhatM + 1;
        cendM = (i - 1) * dhatM + dhatM;
        % then perform M steps with the de Casteljau algorithm to determine
        % the new B-coefficient values
        CMa = bsplinen_deCasteljau(fn.coefs(cstart:cend), n, d, m, a);
        % and set the new B-coefficients for the derivative
        fn_du.coefs(cstartM:cendM) = CMa;
        
    end
    
      
        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % The de Casteljau iteration
    function Cd = bsplinen_deCasteljau(C0, n, d, its, b)

        % Note: b are the barycentric coordinates of the evaluation point!

        basisd0     = bsplinen_constructBasisExt(n, d);
        kappa_d0    = zeros(n+1, n+1);
        Ikappa      = eye(n+1);
        for m = 1:its
            dm      = d - m;
            basisdm = bsplinen_constructBasisExt(n, dm);
            dhatd0  = length(basisd0(:,1));
            dhatdm  = length(basisdm(:,1));

            % calculate all new C values (Cd)
            Cd = zeros(dhatdm, 1);
            for j = 1:dhatdm
                % get the new index
                kappa_d = basisdm(j, :);
                % calculate the indices of the right hand term

                indices = zeros(n+1, 1);
                % calculate the kappa values in kappa_d0 and find their indices
                % in basisd0.
                for k = 1:n+1
                    kappa_d0(k, :) = kappa_d + Ikappa(k, :);
                    % find the indices in basisd0
                    for p = 1:dhatd0
                        if (basisd0(p, :) == kappa_d0(k, :))
                            indices(k) = p;
                            break;
                        end
                    end
                end

                % and calculate the value of Cd
                for k = 1:n+1
                    Cd(j) = Cd(j) + b(k)*C0(indices(k));
                end
            end

            % the new C0 is now equal to Cd
            C0      = Cd;
            basisd0 = basisdm;
        end

  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the polynomial basis and multinomial coefficients
    function [Basis BasisKt] = bsplinen_constructBasisExt(n, d)
        
        % the polynomial orders 0 and 1 require no calculations
        if (d == 0)
            Basis = zeros(1, n+1);
            return;
        end

        Basis = eye(n+1, n+1);
        if (d == 1)
            return;
        end

        % Recursively increase the degree of the polynomial basis. This method
        % requires no sorting or removal of nonunique rows. 
        for i = 2:d
            dhat = size(Basis, 1);
            % increment vector of 1's
            inc = ones(dhat, 1);
            % the new matrix of polynomial basis indices
            basisnew = zeros(dhat * (n+1), n+1);
            % Go through all dimensions and add the increment vector (1's) to all
            % elements in the j-th column.
            for j = 1:n+1
                idx = (j-1)*dhat+1:j*dhat;
                basisnew(idx, j+1:end) = Basis(:, j+1:end);
                basisnew(idx, j) = Basis(:, j) + inc;
            end

            % remove all terms that do not sum up to d
            Basis = basisnew(sum(basisnew, 2) == i, :);

        end

        %   Calc the basis coefficient vector |kappa|! / kappa! 
        BasisKt =  factorial(d)./prod(factorial(Basis),2);
    

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Barycentric coordinate transformations
    function lambda = bsplinen_cart2bary(simplex, X)

        % The reference vertex is always chosen as the first simplex vertex.
        % This can be done because barycentric coordinates are not dependent on
        % the reference point.
        v0      = simplex(1, :);

        %   Determine size of the data
        vcount  = size(simplex, 1);
        Xcount  = size(X, 1);

        %   Initialize output lambda
        lambda  = zeros(Xcount, vcount);

        %   Assemble matrix A and compute its inverse
        A    = (simplex(2:end,:)-ones(vcount-1,1)*v0)';
        invA = inv(A);

        %   Perform the transformation
        %   ... compute the relative coordinates
        p   = X - ones(Xcount,1)*v0;

        %   ... compute first part of lambda
        lambda(:,2:end) = (invA*p')';

        %   ... compute second part of lambda
        lambda(:,1)     = 1-sum(lambda(:,2:end),2);

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate total number of polynomial terms
    function dHat = bsplinen_calcdHat(n, d)

        dHat = factorial(n + d) ./ (factorial(n) * factorial(d));
        
        
        
