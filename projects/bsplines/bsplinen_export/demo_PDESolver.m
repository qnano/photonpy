%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bsplinen_demoPDESolver solves time invariate first and second order differential equations
%   based on measurements made on the first and second order directional derivatives of the
%   solution function.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2012
%              email: c.c.devisser@tudelft.nl
%                          Version: 1.0
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;

% load structures from disk
bsplinen_structures;

% init random number generator
rand('twister', 101);

printfigs     = 0; % print figures switch


% polynomial polybasis properties
n = 2; % dimension of objective function
d = 6; % polynomial degree of objective function
r = 2; % continuity order of objective function
dA = 6; % degree of approximator function
rA = 0; % continuity order of approximator function

useCurvatureData = 0; % 0: only use slope data; 1: use slope data and curvature data

simpdatacount = 50; % data content per simplex

vertex_count  = 10; % number of vertices used in triangulation

evalres       = .01; % resolution for eval
gridres       = .01; % data resolution for gridded data


% view angle and elevation
viewaz = -132;
viewel = 48;

figpath = '.\figures\';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Calculate polynomial basis function multi-index permutations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

polybasis = bsplinen_constructBasis(n, d); % Objective function polynomial basis indices

[polybasisA basisKtA] = bsplinen_constructBasis(n, dA); % Approximator function polynomial basis indices

[polybasisM1, basisKtM1] = bsplinen_constructBasis(n, dA-1); % Approximator function first degree reduced polynomial basis indices
derivmultfact         = factorial(dA) / factorial(dA-1);

if (dA > 1 && useCurvatureData)
    [polybasisM2, basisKtM2] = bsplinen_constructBasis(n, dA-2); % Approximator function second degree reduced polynomial basis indices
    derivmultfact2 = factorial(dA) / factorial(dA-2);
else
    polybasisM2 = [];
    derivmultfact2 = 0;
end

% size of basii
dhat                  = size(polybasis, 1);
dhatA                 = size(polybasisA, 1);
dhatM1                = size(polybasisM1, 1);
dhatM2                = size(polybasisM2, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%	Construct random triangulation in ([0,1],[0,1])
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PHI = rand(vertex_count, 2); % generate vertex array
TRI = delaunayn(PHI); % create triangulation
TRI = sortrows(TRI); % sort vertex indices

TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)

% the number of simplices
T = size(TRI, 1);

fprintf('Total number of simplices: %d, total number of vertices: %d\n', T, vertex_count);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Data generating functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%fx = 'x1.^2.*sin(5*x1 - 5*x2.^2)';
%fx = 'x1.^2.*cos(10*x1 - 15*x2)';
%fx = 'exp(-(15*(x1-.5).^2 + 10*(x2-.5).^2))';
fx = 'x1.^2.*cos(10*x1 - 5*x2) - x2.^2.*sin(10*x1 + 5*x2)';
%fx = '(x1-1).^2.*cos(10*(x1-1) - 15*(x2-1)) - (x2-1).^2.*sin(10*(x1-1) + 15*(x2-1))';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Evaluation locations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
% evaluation locations (square grid)
[xxe yye] = ndgrid((0:evalres:1)', (0:evalres:1)');
XIeval = [xxe(:), yye(:)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Dataset generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generate the data locations
XI = zeros(T*simpdatacount, 2);

% use barycentric coordinate rules to create uniformly distributed random dataset inside each simplex
for i = 1:T
    % generate data in barycentric coordinates
    Set = rand(simpdatacount, 3);
    % normalize the barycentric coordinates
    sumSet = sum(Set, 2);
    Set = Set./[sumSet sumSet sumSet];
    % transform to cartesian coords
    Xt = bsplinen_bary2cart(PHI(TRI(i, :), :), Set);
    
    XI((i-1)*simpdatacount+1:i*simpdatacount,:) = Xt;
end   

% function values
x1 = sym('x1');
x2 = sym('x2');
    
% create identification dataset with some matlab magic
x1 = XI(:,1);  x2 = XI(:,2);
Z = eval(fx);
x1 = XIeval(:,1);  x2 = XIeval(:,2);
Zeval = eval(fx);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Estimate B-coefficients of OBJECTIVE FUNCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI}, {});

% create the matrix holding the smoothness constraints
H = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, r);

% create the regression matrix for the given dataset, triangulation, and degree.
[X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis);


% dispersion matrix construction
B = X'*X;
% construct complete KKT matrix
M = full([B H'; H sparse(size(H, 1), size(H, 1))]);

% simple constrained least squares estimator for B-coefficients & Lagrange multipliers (very inefficient!)
bcoefs_Lmult = pinv(M) * [X'*Y; zeros(size(H,1), 1)];

% estimated b-coefficients are located in the top T*dhat rows of bcoefs_Lmult
bcoefs       = bcoefs_Lmult(1:size(B, 1)); 

% Construct Objective function spline function structure
OBJspline = struct_bsplinen;
% Set the bsplinen structure properties
OBJspline.PHI        = PHI;
OBJspline.TRI        = TRI;
OBJspline.degree     = d;
OBJspline.continuity = r;
OBJspline.dim        = n;
OBJspline.coefs      = bcoefs;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Evaluate OBJspline function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

val_OBJspline = bsplinen_evalExt(OBJspline, XIeval);
err_OBJspline = Zeval - val_OBJspline;
rms_OBJspline = sqrt(mse(err_OBJspline));
rmsrel_OBJspline = rms_OBJspline / sqrt(mse(Zeval));

% calculate locations of B-coefficients
Bcoords = bsplinen_calcBCoefCoordinates(OBJspline);
% triangulate B-coef locations
TRIBcoords = delaunayn(Bcoords);

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Calculate spatial derivatives of objective spline function 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OBJspline.coefs = OBJspline.coefs-OBJspline.coefs(1); % remove piston mode

% WF OBJspline model slopes
OBJspline_dx = bsplinen_derivExt(OBJspline, 1, [1 0]);
OBJspline_dy = bsplinen_derivExt(OBJspline, 1, [0 1]);
OBJspline_dx_val = bsplinen_evalExt(OBJspline_dx, XI);
OBJspline_dy_val = bsplinen_evalExt(OBJspline_dy, XI);

% WF OBJspline model curvatures
if (d > 1 && useCurvatureData)
    OBJspline_dx2 = bsplinen_derivExt(OBJspline, 2, [1 0]);
    OBJspline_dy2 = bsplinen_derivExt(OBJspline, 2, [0 1]);
    OBJspline_dx2_val = bsplinen_evalExt(OBJspline_dx2, XI);
    OBJspline_dy2_val = bsplinen_evalExt(OBJspline_dy2, XI);
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Assign Data to simplices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DataLocations = [];
IData = zeros(T, 3);
for t = 1:T
    imap = tsearchn(PHI, TRI(t, :), XI);
    idx = find(~isnan(imap));
    ndata = length(idx);

    istart = size(DataLocations, 1) + 1;
    DataLocations = [DataLocations; XI(idx, 1) XI(idx, 2)]; % slow but robust ;)
    IData(t, :) = [istart istart+ndata-1 ndata];
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Construct de Casteljau matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ndata = size(XI, 1); 

Bx = sparse(Ndata, dhatA*T);
By = sparse(Ndata, dhatA*T);
Zx = zeros(Ndata, 1);
Zy = zeros(Ndata, 1);
Zidx = zeros(Ndata, 1);
if (dA > 1 && d > 1  && useCurvatureData)
    Bx2 = sparse(Ndata, dhatA*T);
    By2 = sparse(Ndata, dhatA*T);
    Zx2 = zeros(Ndata, 1);
    Zy2 = zeros(Ndata, 1);
    Zidx2 = zeros(Ndata, 1);
end

% Go through all simplices, and construct the de Casteljau matrix for each
partdatacount = 0;
partdatacountC = 0;
for t = 1:T
    simpt = PHI(TRI(t, :), :);
    simplex = DELTA{t};
    ndatat = IData(t, 3);
    newpartdatacount = partdatacount + ndatat;

    % Calculate the barycentric coordinates of U with respect to simplex t.
    Ux = [1 0];
    Uy = [0 1];
    nZx = simpt(1,:) - Ux; % normalized relative direction
    nZy = simpt(1,:) - Uy; % normalized relative direction
    alpha = bsplinen_cart2bary(simpt, simpt(1,:));
    betax = bsplinen_cart2bary(simpt, nZx);
    betay = bsplinen_cart2bary(simpt, nZy);
    % the directional coordinate for simplex t
    ax = alpha - betax;     
    ay = alpha - betay;   

    % Build the de d-to-(d-1) de Casteljau matrix using the directional coordinates ax and ay
    CastelX1 = bsplinen_constructDeCasteljauMatExt(ax, dA);
    CastelY1 = bsplinen_constructDeCasteljauMatExt(ay, dA);                    
    % Determine value of B-matrix for d > 1 (nonlinear WFR)
    Btk = ones(ndatat, 1);
    Islopest = IData((t), 1):IData((t), 2);
    if (dA > 1)
        bdat = bsplinen_cart2bary(simpt, DataLocations(Islopest, :));
        Btk = derivmultfact * bsplinen_constructBtk(polybasisM1, basisKtM1, bdat);
    end
    % Construct slopes regression matrix
    Bx((partdatacount+1):newpartdatacount, dhatA*(t-1)+1:dhatA*t) = Btk * CastelX1;
    By((partdatacount+1):newpartdatacount, dhatA*(t-1)+1:dhatA*t) = Btk * CastelY1;
    % Compile the slope dataset
    Zx((partdatacount+1):newpartdatacount)                      = OBJspline_dx_val(Islopest);
    Zy((partdatacount+1):newpartdatacount)                      = OBJspline_dy_val(Islopest);
    % Save the indices into the slope vector
    Zidx((partdatacount+1):newpartdatacount)                    = Islopest;

    % if there is curvature data, include it in the regression
    if (dA > 1 && d > 1 && useCurvatureData)
        ndatatC = IData((t), 3);
        newpartdatacountC = partdatacountC + ndatatC;

        % Build the d-to-(d-2) de Casteljau matrix using the directional coordinates ax and ay
        CastelX2 = bsplinen_constructDeCasteljauMatExt(ax, dA-1) * CastelX1;
        CastelY2 = bsplinen_constructDeCasteljauMatExt(ay, dA-1) * CastelY1;
        % Determine value of B-matrix for d > 1 (nonlinear WFR)
        Btk2 = ones(ndatatC, 1);
        Icurvest = IData((t), 1):IData((t), 2);
        if (d-1 > 1)
            bdat2 = bsplinen_cart2bary(simpt, DataLocations(Icurvest, :));
            Btk2 = derivmultfact2 * bsplinen_constructBtk(polybasisM2, basisKtM2, bdat2);
        end

        % Construct curves regression matrix
        Bx2((partdatacountC+1):newpartdatacountC, dhatA*(t-1)+1:dhatA*t) = Btk2 * CastelX2;
        By2((partdatacountC+1):newpartdatacountC, dhatA*(t-1)+1:dhatA*t) = Btk2 * CastelY2;
        % Compile the curve dataset
        Zx2((partdatacountC+1):newpartdatacountC)                      = OBJspline_dx2_val(Icurvest);
        Zy2((partdatacountC+1):newpartdatacountC)                      = OBJspline_dy2_val(Icurvest);
        % Save the indices into the slope vector
        Zidx2((partdatacountC+1):newpartdatacountC)                    = Icurvest;
        partdatacountC = newpartdatacountC;
    end

    partdatacount = newpartdatacount;

end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Setup Constraint Matrices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% create the matrix holding the smoothness constraints
HA = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasisA, rA);

K = zeros(1, dhatA*T);
K(1, 1) = 1; % The Anchor Constraint: constrain the first b-coef from simplex 1


HK = HA;
% setup the complete constraint matrix HK = [HA; K] and the constraint values G = [0; gb];
if (~isempty(HA))
    if (~isempty(K))
        HK = [HK; K];
        G = zeros(1+size(HA, 1), 1);
    end
else
    if (~isempty(K))
        HK = K;
        G = 0;
    end
end

% construct the complete regression matrices
B = [];
Z = [];
rangeB = 0;
B = [B; Bx; By;];
Z = [Z; Zx; Zy];
if (dA > 1 && d > 1 && useCurvatureData)
    B = [B; Bx2; By2;];
    Z = [Z; Zx2; Zy2];
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Solve Linear Regression Problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% eliminate constraints
[Br Ired2full Ifull2red NullHK] = bsplinen_reduceRegressionMat(B, HK, dA, rA);
Brt = Br';

% Create the dispersion matrix, which for the curvature modeling must be weighted
if (~useCurvatureData)
    % create dispersion matrix
    BrtWBr = Brt * Br;
else
    % create weighting matrix such that curvature has a relative importance of 0.5 with respect to slope
    weight = .5 * range([Bx(:); By(:)]) / range([Bx2(:); By2(:)]); 
    Val = [ones(size(Bx,1)+size(By,1),1); weight*ones(size(Bx2,1)+size(By2,1),1)];
    W = sparse(1:size(B,1), 1:size(B,1), Val);
    % create dispersion matrix
    Brt = Br' * W;
    BrtWBr = Brt * Br;
end

iBrtBr = BrtWBr \ eye(size(BrtWBr));%inv(full(BrtBr));                
% LS estimator for B-coefficients
iBrtBrBrt = iBrtBr * Brt; 
coefsred = iBrtBrBrt * Z;
coefs = zeros(T*dhatA, 1); 
if (isempty(NullHK))
    coefs(Ifull2red) = coefsred(Ired2full);    
else
    coefs = NullHK * coefsred;    
end

% Construct spline function
PDEspline = struct_bsplinen;
% Set the bsplinen structure properties
PDEspline.PHI        = PHI;
PDEspline.TRI        = TRI;
PDEspline.degree     = dA;
PDEspline.continuity = rA;
PDEspline.dim        = n;
PDEspline.coefs      = coefs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Evaluate PDEspline function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

val_PDEspline = bsplinen_evalExt(PDEspline, XIeval);
err_PDEspline = val_OBJspline - val_PDEspline;
goodidx = ~isnan(err_PDEspline);
meanerr_PDEspline = mean(err_PDEspline(goodidx)); 
rms_PDEspline = sqrt(mse(err_PDEspline(goodidx) - meanerr_PDEspline));
rmsrel_PDEspline = rms_PDEspline / sqrt(mse(val_OBJspline(goodidx)));

val_PDEspline = val_PDEspline + meanerr_PDEspline; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Do the plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
close all;

%%
plotID = 10;

figure(plotID);
set(plotID, 'Position', [1 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
trimesh(TRI, PHI(:, 1), PHI(:, 2), 'Color', 'b');
axis([-.2 1.2 -.2 1.2]);
count = 1;
bind = 1;
plot(XI(:,1), XI(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 0 0],'MarkerSize', 3);% make it look nice
for i = 1:size(Bcoords, 1)
    plot(Bcoords(i,1), Bcoords(i,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0 1 0],'MarkerSize', 1);% make it look nice
    Bcoef = sprintf('c_%d_%d_%d', polybasis(bind, 1), polybasis(bind, 2), polybasis(bind, 3));
    bind = bind + 1;
    if (count == 1)
        offset = [-.025 -.045];
    elseif (count == 2)
        offset = [-.08 .035];
    elseif (count == 3)
        offset = [.025 .035];
    end
    %text(Bcoords(i, 1)+offset(1), Bcoords(i, 2)+offset(2), Bcoef);        
    if (bind > dhat)
        count = count + 1;
        bind = 1;
    end
end
for i = 1:size(TRI, 1)
    str = sprintf('t_{%d}', i);
    xytext = bsplinen_bary2cart(PHI(TRI(i, :), :), [1/3 1/3 1/3]);
    text(xytext(1), xytext(2), str);            
end
for i = 1:size(PHI, 1)
    str = sprintf('v_{%d}', i);
    text(PHI(i, 1)+.025, PHI(i, 2), str);            
end
xlabel('x');
ylabel('y');
titstr = sprintf('Triangulation consisting of %d simplices with %d PDE sampling points', T, size(XI, 1));
title(titstr);
if (printfigs == 10 || printfigs == 1)
    fname = sprintf('fig_Triangulation_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 101;
figure(plotID);
set(plotID, 'Position', [0 150 500 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
spy(B'*B);
title('Constraint eliminated $X_r^\top X_r$');
if (printfigs == 101 || printfigs == 100)
    fname = sprintf('fig_XrtXr_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

plotID = 102;
figure(plotID);
set(plotID, 'Position', [500 150 500 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
spy(BrtWBr);
title('Unmodified $X^\top X$');
if (printfigs == 102 || printfigs == 100)
    fname = sprintf('fig_XtX_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

plotID = 103;
figure(plotID);
set(plotID, 'Position', [0 150 500 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
spy(Br);
title('Constraint eliminated regression matrix $X$');
if (printfigs == 103 || printfigs == 100)
    fname = sprintf('fig_Xr_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

plotID = 104;
figure(plotID);
set(plotID, 'Position', [500 150 500 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
spy(B);
title('Unmodified regression matrix $X$');
if (printfigs == 104 || printfigs == 100)
    fname = sprintf('fig_X_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end


plotID = 105;
figure(plotID);
set(plotID, 'Position', [1000 150 500 500], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
spy(HA);
title('Smoothness matrix $H$');
if (printfigs == 105 || printfigs == 100)
    fname = sprintf('fig_H_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 1001;

figure(plotID);
set(plotID, 'Position', [500 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_OBJspline);
maxz = max(val_OBJspline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_OBJspline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(-23, 36);
title(sprintf('True Solution Spline Function (d=%d,r=%d,T=%d)', d, r, T));
if (printfigs == 1001 || printfigs == 1)
    fname = sprintf('fig_SplineResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 1005;

figure(plotID);
set(plotID, 'Position', [1000 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_PDEspline);
maxz = max(val_PDEspline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_PDEspline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(-23, 36);
title(sprintf('Spline Approximation to PDE Solution (d=%d,r=%d,T=%d)', dA, rA, T));
if (printfigs == 1005 || printfigs == 1)
    fname = sprintf('fig_SplinePDEResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end

%%
plotID = 2001;

figure(plotID);
set(plotID, 'Position', [0 350 600 600], 'defaultaxesfontsize', 10, 'defaulttextfontsize', 10, 'PaperPositionMode', 'auto');
hold on;
grid on;
minz = min(val_PDEspline-val_OBJspline);
maxz = max(val_PDEspline-val_OBJspline);
trimesh(TRI, PHI(:, 1), PHI(:, 2), minz*ones(size(PHI,1),1), 'EdgeColor', 'b');
surf(xxe, yye, reshape(val_PDEspline-val_OBJspline, size(xxe)));
poslight = light('Position',[1.5 2.5 7],'Style','local');
material([.3 .8 .9 25]);
lighting phong;
shading interp;
view(-23, 36);
title(sprintf('Residual of Spline Approximation to PDE Solution (d=%d,r=%d,T=%d)', dA, rA, T));
if (printfigs == 2001 || printfigs == 1)
    fname = sprintf('fig_SplinePDEResults_d%dr%d_T%d', d, r, T);
    savefname = strcat(figpath, fname);
    print(plotID, '-dpng', '-r300', savefname);
    saveas(plotID, savefname);
    matlabfrag(savefname);
    fprintf('Printed <%s>\n', savefname);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       Finally, some outputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n---------------------------------------------------------------------\n');
fprintf('PDE Solution Spline Model Relative Error RMS = %d\n', rmsrel_PDEspline);
fprintf('---------------------------------------------------------------------\n');



