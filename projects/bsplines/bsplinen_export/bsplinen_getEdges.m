
% BSPLINEN_GETEDGES2 returns the simplex edges in a triangulation as an array 
%   of STRUCT_EDGE structures. 
% 
%                          Author: C.C. de Visser, 2007
%   
%   EDGES = BSPLINEN_GETEDGES(TRI) creates a collection of all full edges
%   in a given triangulation. An edge is a full edge for two N-simplices
%   T1 and T2 if and only if N vertices in T1 and T2 overlap. An edge is
%   not defined in terms of its edge vertices, but rather in terms of the
%   Smoothness Subject Vertices (SSV) of both simplices T1 and T2. Edge
%   data is stored in STRUCT_EDGE structures as defined in BSPLINEN_STRUCTURES.m.
%    
%   Input parameter is
%
%       TRI is a triangulation of a dataspace created with for instance
%           DELAUNAYN. The entries in TRI are the indices of vertices in the
%           dataspace. Edges are found with a nearest neighbor search algorithm. 
%              
%   OUTPUT from BSPLINE_GETEDGES2 is
%       
%       EDGES a cell array containing STRUCT_EDGE structures holding
%           edge data. Every physical edge in the triangulation will have 
%           just one STRUCT_EDGE structure associated with it.       
%
function Edges = bsplinen_getEdges(PHI, TRI)

    global STRUCTURES_LOADED;
    global struct_edge2;
    if (~exist('STRUCTURES_LOADED', 'var'))
        bsplinen_structures;
    end
    
    T = size(TRI, 1);

    % the number of vertices per simplex
    v_count = length(TRI(1, :));
    % dimension
    n = v_count - 1;

    %EdgeMap = sparse(length(TRI(:, 1)), length(TRI(:, 1)));
    EdgeMap = sparse(size(TRI, 1), size(TRI, 1));
    Edges = {};

    % find the natural neighbours for every simplex in TRI
    [NB, NI, NEV] = bsplinen_findneighbors(TRI, n);
    
    
    if (isempty(NB))
        return;
    end
    
    edgeCount = 1;
    % go through all simplices
    % for i = 1:T
    for i = 1:size(NB, 1)
        % define the ssv's for this edge
        % go through all neighboring simplices of the simplex at 'i'
        for nb = 1:size(NB, 2)

            if (~isempty(NB{i, nb}))

                % setup the edge structure
                edge = struct_edge2;
                edge.index = edgeCount;                
                edge.simplex1 = i;
                edge.simplex2 = NI{i, nb};
                edge.edgevertices = NEV{i, nb};
                
                % get the ssv of the first simplex 
                [edge.ssv1 edge.cssv1] = bsplinen_getSSV(TRI(i, :), NB{i, nb});
                % get the ssv of the second simplex 
                [edge.ssv2 edge.cssv2] = bsplinen_getSSV(NB{i, nb}, TRI(i, :));

                % calculate the barycentric coordinates of the SSV's with
                % respect to the target simplices
                edge.ssv1bary = bsplinen_cart2bary(PHI(TRI(edge.simplex2, :), :), PHI(edge.ssv1, :));
                edge.ssv2bary = bsplinen_cart2bary(PHI(TRI(edge.simplex1, :), :), PHI(edge.ssv2, :));

                % check whether we already have an edge for the same
                % set of simplices (the dual edge...).
                s1 = edge.simplex1;
                s2 = edge.simplex2;
                if (s1 > s2)
                    s1 = edge.simplex2;
                    s2 = edge.simplex1;
                end
                if (EdgeMap(s1, s2) == 0)
                    EdgeMap(s1, s2) = 1;
                    edge.ID = bsplinen_createEdgeHash(s1, s2);
                    Edges{edgeCount, 1} = edge;
                    edgeCount = edgeCount + 1;
                end
                
            end

        end 

    end
    
    
    
