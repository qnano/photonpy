% BSPLINEN_GETTRIANGULATIONEDGES2 returns the internal and external edge facets of a triangulation
%  
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2010
%              email: c.c.devisser@tudelft.nl
%                          version 1.0
%
%   [TRIedges edgeSimps] = BSPLINEN_GETTRIANGULATIONEDGES2(TRI, PHI, EDGES, ALLEDGES)
%
function [TRIedges, PHIedges, edgeSimps, Edges] = bsplinen_getTriangulationEdges2(TRI, PHI, Edges, alledges)

    bsplinen_structures;
    
    TRIedges = {};
    PHIedges = {};
    edgeSimps = [];
    
    if (nargin < 3)
        % get all edges in the triangulation
        Edges = bsplinen_getEdges(PHI, TRI);
        alledges = 1;
    elseif (nargin < 4)
        alledges = 1;
    end
    
    % dimension of TRI
    n = size(PHI, 2);
    
    T = size(TRI, 1);

    
    % compile a list that translates simplices to edges
    SimpEdgeList = cell(T, 1);
    for i = 1:size(Edges, 1)
        edge = Edges{i};
        SimpEdgeList{edge.simplex1} = [SimpEdgeList{edge.simplex1}; i];
        SimpEdgeList{edge.simplex2} = [SimpEdgeList{edge.simplex2}; i];
    end
    
    idxset = (1:n+1)';
    % the permutation vector for the edge facet indices    
    facetidx = nchoosek(idxset, n);

    % the triangulation edges are all edge facets that are not in Edges...
    TRIedges = cell(T, 1);
    PHIedges = cell(T, 1);
    edgeSimps = zeros(T, 1);
    
    EdgeIdxList = zeros(size(Edges, 1), 1);
    
    % go through all simplices
    for i = 1:T
        % vertex indices of simplex i
        simpidx = TRI(i, :);
        % get the facets of simplex i
        facets = simpidx(facetidx);
        
        % The principle is that any facets that are not in any edge structure, are edge facets!

        edgeidx = SimpEdgeList{i};
        if (isempty(edgeidx))
            % no edges, so all facets are edge facets!
            TRIedges{i} = facets;
            PHIedges{i} = PHI(facets', :);
            continue;
        end
        
        % Go through all facets and see if they are edge facets
        edgefacetlist = zeros(size(facets, 1), 1);
        edgefacetcount = 1;
        hasedgefacets = 0;
        for fi = 1:size(facets, 1)
            facet = facets(fi, :);
            isedgefacet = 1;
            % go through all edges of simplex 'simp'
            for j = 1:length(edgeidx)
                edge = Edges{edgeidx(j)};
                ev = edge.edgevertices;
                % find the mutual vertex indices in the current facet and the edge vertices
                mutualverts = intersect(facet, ev);
                if (length(mutualverts) == n)
                    % there are n mutual vertices, so this facet is actually an internal edge facet rather than a triangulation edge facet!
                    isedgefacet = 0;
                    break;
                end
            end
            if (isedgefacet)
                % this facet is a triangulation edge facet
                edgefacetlist(edgefacetcount) = fi;
                edgefacetcount = edgefacetcount + 1;
                hasedgefacets = 1;
            end
            
        end
        
        if (hasedgefacets)
            edgefacetlist = edgefacetlist(1:edgefacetcount-1);
            % insert edge facets
            TRIedges{i} = facets(edgefacetlist, :);
            PHIedges{i} = PHI(TRIedges{i}', :);
            edgeSimps(i) = 1;
        end
        
        
    end
    
    


