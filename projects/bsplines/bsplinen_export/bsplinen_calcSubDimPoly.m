% bsplinen_calcSubDimPoly calculates the B-coefficients of a B-form polynomial
%   of dimension n-m > 0 given an input B-form polynomial of dimension n, and
%   and output B-form polynomial of dimension m < n.
% 
%                          Author: C.C. de Visser, 2015
%   
%   Description:
%       This function takes an n-D B-form polynomial as input (polyND) together with an m-D
%       B-form polynomial prototype, with m<n, indicated as polyMD. That is, the 
%       polyMD is a B-form polynomial of lower dimension than polyND. From theory,
%       we find that polyMD.coefs \in polyND.coefs. This means there must exist
%       a mapping between polyMD.coefs and polyND.coefs. This function aims to determine
%       this mapping. In particular, if:
%           p_nD(x) = B_nD(x)c_nD
%           p_mD(x) = B_mD(x)c_mD
%       then there exists a transformation matrix Tj_MD_ND \in R^[dhat_mD x dhat_nD] such that:
%           c_mD = Tj_MD_ND * c_nD.
%
%   Inputs:
%       polyND: [struct_bsplinen] structure; this is the n-D B-form polynomial from which
%           a slice is taken resulting in polyMD.
%
%       polyMD: [struct_bsplinen] structure; this is the m-D B-form polynomial with m<n 
%           which is a sub-dimensional spline.
%
%   Outputs:
%       Tj_MD_ND: [dhat_mD x dhat_nD] matrix; transformation matrix such that:
%           p_nD(x) = B_nD(x)c_nD
%           p_mD(x) = B_mD(x)c_mD
%           c_mD = Tj_MD_ND * c_nD.
%       polyMD: [struct_bsplinen] structure, same as input, but now with B-coefficients
%           determined using the transformation matrix:
%           c_mD = Tj_MD_ND * c_nD.
%       BcoordsMD: [dhat_MD x ND] matrix; location of B-coefficients of MD spline in R^ND.
%       BcoordsMD_MD: [dhat_MD x MD] matrix; location of B-coefficients of MD spline in R^MD.
%
function [Tj_MD_ND, polyMD, BcoordsMD, BcoordsMD_MD] = bsplinen_calcSubDimPoly(polyND, polyMD, Xint)
    
    Tj_MD_ND = [];
    
    if (polyMD.dim >= polyND.dim)
        warning('We must have polyND.dim > polyMD.dim, which is not the case here, returning...');
        return;
    end
    if (polyMD.degree ~= polyND.degree)
        warning('bsplinen_calcSubDimPoly requires polyMD.degree == polyND.degree which was not the case; modifying degree of polyMD ...');
        polyMD.degree = polyND.degree;
    end
    if (polyMD.dim > 1)
        warning('Currently, this function is only implemented for polyMD.dim = 1 (1-D simplex splines), returning...');
        return;
    end

    d           = polyND.degree;
    simplexND   = polyND.PHI(polyND.TRI(1,:), :);
    simplexMD   = polyMD.PHI(polyMD.TRI(1,:), :);

    %%
    % Step 1: Determine barycentric coordinates of pMD (the 1-D spline) (WARNING: NOT GENERAL IN DIMENSION!) 

    [polybasisND, basisKtND] = bsplinen_constructBasis(polyND.dim, d);
    dhatND = size(polybasisND, 1);

    [polybasisMD, basisKtMD] = bsplinen_constructBasis(polyMD.dim, d);
    dhatMD = size(polybasisMD, 1);

    dBC_MD = diff(Xint) / (dhatMD-1);
    % check for zero difference in one of the coordinates, which indicates parallel-to-axis lines
    if (dBC_MD(1) == 0)
        x2 = (Xint(1,2):dBC_MD(2):Xint(2,2))';
        BcoordsMD = [ones(size(x2))*Xint(1,1) x2];
    elseif (dBC_MD(2) == 0)
        x1 = (Xint(1,1):dBC_MD(1):Xint(2,1))';
        BcoordsMD = [x1 ones(size(x1))*Xint(1,2)];
    else
        BcoordsMD = [(Xint(1,1):dBC_MD(1):Xint(2,1))' (Xint(1,2):dBC_MD(2):Xint(2,2))'];
    end
    BcoordsMD_bary = bsplinen_cart2bary(simplexND, BcoordsMD);

    BcoordsMD_MD = (linspace(0, 1, dhatMD))'; % only works with lines!
    BcoordsMD_MD_bary = bsplinen_cart2bary(simplexMD, BcoordsMD_MD);

    %%
    % Step 2a: Calculate B_N^d at b(c^M) (N-D basis polynomial value at B-coefs) (GENERAL IN DIMENSION)

    BtND = zeros(dhatMD, dhatND);
    for i = 1:dhatND
        BtND(:,i) = basisKtND(i)*prod(BcoordsMD_bary.^(ones(dhatMD,1)*polybasisND(i,:)),2);
    end

    % Step 2b: Calculate B_M^d at b(c^M) (M-D basis polynomial value at B-coefs)
    BtMD = zeros(dhatMD, dhatMD);
    for i = 1:dhatMD
        BtMD(:,i) = basisKtMD(i)*prod(BcoordsMD_MD_bary.^(ones(dhatMD,1)*polybasisMD(i,:)),2);
    end

    %%
    % Step 3: Calculate Transformation matrix Tj_MD_ND from polyND to polyMD, and B-coefficients of polyMD (GENERAL IN DIMENSION)
    Tj_MD_ND = BtMD \ BtND; % oh, yeah! Note that the inverse must exist!

    polyMD.coefs = Tj_MD_ND * polyND.coefs;







    