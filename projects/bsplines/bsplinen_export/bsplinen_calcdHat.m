
% BSPLINEN_CALCDHAT calculates the number of B-coefficients for a single
%   simplex for a given dimension and polynomial order.
% 
%                          Author: C.C. de Visser, 2007
%
%   DHAT = BSPLINEN_CALCDHAT(N, D) calculates the total number of
%       B-coefficients for a single simplex for a given dimension and
%       polynomial basis function order.
%       WARNING: due to unknown reasons the Matlab function FACTORIAL is
%       EXTREMELY SLOW as compared to the product series. Do not use this
%       function in nested loops, but precalculate the dHat value in that case.
%
%   Input parameters are.
%
%       N the dimension of the simplex
%
%       D the order of the polynomial basis function
%
%   Output from BSPLINEN_CALCHDAT is.
%
%       DHAT the total number of B-coefficients for a given dimension and
%       order
function dHat = bsplinen_calcdHat(n, d)

    dHat = factorial(n + d) ./ (factorial(n) * factorial(d));
