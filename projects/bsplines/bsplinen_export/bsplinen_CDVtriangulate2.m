% BSPLINEN_CDVTRIANGULATE2 creates a symmetric hypercube based triangulation
%   based on a given dataset and required hypercube count or knotset.
% 
%              Copyright: C.C. de Visser, Delft University of Technology, 2010
%              email: c.c.devisser@tudelft.nl
%                          version 1.1
%
%   [TRI PHI KNOTSET PHIINT] = BSPLINEN_CDVTRIANGULATE2(XI, GRID_KNOT, TYPE, UNIQUEVERTICES, CVHULL, FILLTYPE) 
%       Creates a symmetric Type I or Type II triangulation with guaranteed conforming simplex edges. 
%       Advanced modes allow the filling of an arbitrary shaped convex hull with the Type I/II triangulation.
%       WARNING: Triangulation consistency is only proved for n < 4. This triangulation method 
%       circumvents a known bug in QHull, which under certain circumstances produces triangulations with
%       non-conforming simplex edges (see [http://www.qhull.org/news/]).
%       BSPLINEN_CDVTRIANGULATE2 replaces BSPLINEN_CDVTRIANGULATE and is 10x (small triangulations) till
%       200x (large triangulations) faster with UNIQUEVERTICES = 1. If UNIQUEVERTICES = 0, then this
%       method is more than 250x faster than BSPLINEN_CDVTRIANGULATE.
%
%      04-10-2010: WARNING: there is a bug in bsplinen_CDVtriangulate2 Mode-3: duplicate SIMPLICES are Produced!
%
%   Input for BSPLINEN_CDVTRIANGULATE2 is:
%
%       XI an [N x n] matrix of data locations
%
%       GRID_KNOT is one of the following:
%           1) a [1 x n] vector specifying the number of hypercubes per
%               dimension. This option will create a uniform, symmetric set
%               of hypercubes and simplices.
%           2) a CELL array with cells holding the actual hypercube
%               gridpoint locations. This option will create a user
%               specified set of hypercubes, and simplices, of possible varying size.
%
%       TYPE a [1 x 1] scalar:
%           TYPE = 1  (default): produces a Type I triangulation with diagonals in the +y direction
%           TYPE = 2:  produces a Type II triangulation
%           TYPE = 11: produces a Type I triangulation with diagonals in the -y direction (ONLY FOR N = 2!)
%           TYPE = 3:  produces a Type II triangulation with one additional diagonal (ONLY FOR N = 2!)
%
%       UNIQUEVERTICES a [1 x 1] scalar
%           UNIQUEVERTICES = 0: vertices in PHI are not guaranteed to be unique
%           UNIQUEVERTICES = 1 (default): vertices in PHI are guaranteed to be unique, and sorted according to their indices
%           UNIQUEVERTICES = 2 vertices in PHI are guaranteed to be unique, and are sorted according to their coordinates rather than their indices
%
%       CVHULL a (1 x 5) cell array used only when FILLTYPE = 1.
%           CVHULL{1} a [K x n] matrix holding the coordinates of the convex hull in n-space.
%           CVHULL{2} a [k x 1] vector of indices into CVHULL{2} that make up the convex hull of CVHULL{1}. 
%               CVHULL{2} can be the direct output of the CONVHULLN Matlab method: CVHULL{2} = convhulln(CVHULL{1});
%           CVHULL{3} a [m x 1] vector of indices in CVHULL{1}, or convex hull sample radius.
%               When m > 1, then CVHULL{3} are the sample coordinates at which the convex hull will be sampled for FILLTYPE = 1. 
%               When m = 0, then CVHULL{3} is the maximum distance between 2 sample points on the convex hull.
%               If empty then CVHULL{3} = 1:K.
%           CVHULL{4} a [1 x 1] scalar holding the sample mode switch. Used for FILLTYPE = 1. If empty, CVHULL{4} = 0
%               CVHULL{4} = 0 (default): All intersection points of grid and the convex hull are used in triangulation
%               CVHULL{4} = 1: Intersection points that are within CVHULL{5} of grid or convex hull points are removed
%               CVHULL{4} = 2: Intersection points or convex hull points that are within CVHULL{5} of grid replace 
%                 respective gridpoint.
%           CVHULL{5} a [1 x n], vector or string holding minimum absolute distance between any points in PHI. 
%               If CVHULL{5} = [] (default), then an automatic distance measure of 1/2nd of the smallest grid interval is used.
%               If CVHULL{5} is a [1 x 1] scalar, then same distance measure is used in all dimensions.
%           
%
%       FILLTYPE a (1 x 1) scalar:
%           FILLTYPE = 0 (default): fills the convex hull with with the hypercube pattern such that the resulting triangulation 
%             is guaranteed to be contained by the convex hull. ANY simplices that have one or more vertices outside
%             of the convex hull will be removed. 
%           FILLTYPE = 1: Exactly fills the convex hull with with the hypercube pattern such that the resulting triangulation 
%             is guaranteed to be contained by the convex hull. ANY simplices that have one or more vertices outside
%             of the convex hull will be removed. After that, the convex hull will be sampled at the locations 
%             specified in CVHULL{3} or at the interval specfified in CVHULL{3}.
%           FILLTYPE = 2: fills the convex hull with the hypercube pattern such that the resulting triangulation 
%             is guaranteed to contain all points on the convex hull. Simplices will only be removed if ALL their
%             vertices are outside of the convex hull, and no convex hull points are in the list of removal 
%             candidate simplices.
%
%   Output from BSPLINEN_CDVTRIANGULATE2 is:
%
%       TRI a [T x n+1] matrix which is the triangulation: an index array
%         into PHI holding vertex indices.
%
%       PHI a [P x n] matrix of vertex locations in n-space.
%       
%       KNOTSET a [n x 1] cell array holding the knot locations of the hypercube grid, e.g. [Grid{:}] = ndgrid(KNOTSET{:})
%           
%       PHIINT a [P2 x n] matrix holding the locations of the intersection points of the hypercube grid and the 
%         given convex hull. This return is non-empty ONLY when FILLTYPE = 1.
%
%       PHIHULL a [P3 x n] matrix holding the final locations on the convex hull that will be used in the triangulation.
%         This only returns non-empty when FILLTYPE = 1.
%
%   EXAMPLES:
%       % Default: Type I triangulation on rectangular grid
%       xrect = [0 0; 0 1; 1 0; 1 1];
%       [TRI1 PHI1] = bsplinen_CDVtriangulate2(xrect, [10 8]);
%       figure(1), hold on, trimesh(TRI1, PHI1(:,1), PHI1(:,2), 'Color', 'b'), plot(PHI1(:,1), PHI1(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
%       % Type II triangulation in 2-D on rectangular grid
%       type = 2; % triangulation type
%       uniquevertices = 1; % we want all vertices in PHI to be unique
%       [TRI2 PHI2] = bsplinen_CDVtriangulate2(xrect, [10 8], type, uniquevertices);
%       figure(2), hold on, trimesh(TRI2, PHI2(:,1), PHI2(:,2), 'Color', 'b'),  plot(PHI2(:,1), PHI2(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
%
%       % More advanced modes: Convex hull fillings 
%       % First generate some data and find convex hull
%       xdata = rand(20, 2);% generate random dataset
%       chull = convhulln(xdata); % get convex hull of dataset
%       PHIhull = xdata(unique(chull(:)), :); TRIhull = delaunayn(PHIhull); % triangualate convex hull
%       % Method 1: minimal Type II filling
%       filltype = 0; % filltype 0 simply fills the convex hull such that all simplices are contained by it
%       chullsamplepoints = []; % specifies which points on the hull must be used, if empty all points in chull are used
%       removemode = []; % removemode is only used when filltype = 1
%       removerad = []; % removerad is only used when filltype = 1
%       CVHULL = {xdata, chull, chullsamplepoints, removemode, removerad}; % build convex hull data structure
%       type = 2; % triangulation type
%       uniquevertices = 1; % we want all vertices in PHI to be unique
%       [TRI3 PHI3] = bsplinen_CDVtriangulate2(xrect, [10 8], type, uniquevertices, CVHULL, filltype);
%       figure(3), hold on, trimesh(TRI3, PHI3(:,1), PHI3(:,2), 'Color', 'b'), trisurf(TRIhull, PHIhull(:, 1), PHIhull(:, 2), -.1*ones(size(PHIhull, 1), 1), 'FaceColor', [.85 .8 1], 'EdgeColor','none'), plot(PHI3(:,1), PHI3(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
%       
%       % Method 2: Overfitting Type II filling
%       filltype = 2; % filltype 2 fills the convex hull such that it is guaranteed to fit within the triangulation
%       chullsamplepoints = []; % specifies which points on the hull must be used, if empty all points in chull are used
%       removemode = []; % removemode is only used when filltype = 1
%       removerad = []; % removerad is only used when filltype = 1
%       CVHULL = {xdata, chull, chullsamplepoints, removemode, removerad}; % build convex hull data structure
%       type = 2; % triangulation type
%       uniquevertices = 1; % we want all vertices in PHI to be unique
%       [TRI4 PHI4] = bsplinen_CDVtriangulate2(xrect, [10 8], type, uniquevertices, CVHULL, filltype);
%       figure(4), hold on, trimesh(TRI4, PHI4(:,1), PHI4(:,2), 'Color', 'b'), trisurf(TRIhull, PHIhull(:, 1), PHIhull(:, 2), -.1*ones(size(PHIhull, 1), 1), 'FaceColor', [.85 .8 1], 'EdgeColor','none'), plot(PHI4(:,1), PHI4(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
%
%       % Method 3: Exact Type II filling
%       filltype = 1; % filltype 1 exactly triangulates the convex hull
%       chullsamplepoints = .05; % specifies which points on the hull must be used, if empty all points in chull are used
%       removemode = 1; % removemode is only used when filltype = 1
%       removerad = .05; % removerad is only used when filltype = 1
%       CVHULL = {xdata, chull, chullsamplepoints, removemode, removerad}; % build convex hull data structure
%       type = 2; % triangulation type
%       uniquevertices = 1; % we want all vertices in PHI to be unique
%       [TRI5 PHI5] = bsplinen_CDVtriangulate2(xrect, [10 8], type, uniquevertices, CVHULL, filltype);
%       figure(5), hold on, trimesh(TRI5, PHI5(:,1), PHI5(:,2), 'Color', 'b'), trisurf(TRIhull, PHIhull(:, 1), PHIhull(:, 2), -.1*ones(size(PHIhull, 1), 1), 'FaceColor', [.85 .8 1], 'EdgeColor','none'), plot(PHI5(:,1), PHI5(:,2), 'o', 'MarkerEdgeColor','k', 'MarkerFaceColor',[1 0 0],'MarkerSize', 4);
%
function [TRI PHI KnotSet PHIint PHIhull] = bsplinen_CDVtriangulate2(XI, grid_knot, type, uniquevertices, CVHULL, filltype)
    

    cvhullfillmode = 0;
    TRI = [];
    PHI = [];
    PHIint = [];
    PHIhull = [];
    KnotSet = {};

    % input checking
    if (nargin < 2)
        warning('At least 2 arguments must be supplied: XI and GRID_KNOT, returning...');
        return;
    end
    if (nargin == 2)
        type = 1;
        uniquevertices = 1;
    end
    if (nargin == 3)
        uniquevertices = 1;
    end
    if (nargin > 4)
        cvhullfillmode = 1;
        if (nargin == 5)
            filltype = 0;
        end
        if (~iscell(CVHULL))
            warning('CVHULL argument must be cell array with CVHULL{1} the indices in the vertex array CVHULL{2} of the convex hull of CVHULL{2}, returning...');
            return;
        end
        % Check the CVHULL variable for consistency
        if (length(CVHULL) < 3 && filltype == 1)
            CVHULL{3} = []; %uses all points on the convex hull
            CVHULL{4} = 0;
        end
        if (length(CVHULL) < 4 && filltype == 1)
            CVHULL{4} = 0;
        end
        if (length(CVHULL) < 5 && filltype == 1)
            if (CVHULL{4} ~= 0)
                CVHULL{5} = [];
            end
        end
    end
    
    % determine dimension
    n = size(XI, 2);
    
    if (n > 2)
        if (type == 2)
            warning('3-dimensional Type II triangulations created with BSPLINEN_CDVTRIANGULATE2 currently lead to unconforming triangulations');
        end
        if (type == 11)
            type = 1;
        end
        if (type == 3)
            type = 1;
        end
    end
    % knotseqtype 0: the user supplied the knot sequence
    % knotseqtype 1: the user supplied the number of cells per dimension
    knotseqtype = 0;
    if (~iscell(grid_knot))
        knotseqtype = 1;
        hypercube_count = zeros(n, 1);
        if (length(grid_knot) == 1)
        	hypercube_count(:) = grid_knot;
        else 
            hypercube_count = grid_knot;
        end
    else
        hypercube_count = zeros(n, 1);
        for i = 1:n
            hypercube_count(i) = length(grid_knot{i}) - 1;
        end
    end
    
    % get the min and max bounds
    Extremes = zeros(n, 2);
    HCUnityGridSet = cell(n, 1);
    for i = 1:n
        Extremes(i, 1) = min(XI(:, i));
        Extremes(i, 2) = max(XI(:, i));
    end

    % create the initial vertex set
    KnotSet = cell(n, 1); % the set of knots containing the hypercube coordinates
    KnotSetTII = cell(n, 1); % the set of knots for a Type II triangulation (Only used during convex hull intersection calculation!)
    HCKnotSet = cell(n, 1); % set of knots used to create the index array of hypercube indices
    HCSizeSet = cell(n, 1); % contains, per dimension, the size of the hypercubes
%     HCCoordsGridSet = cell(n, 1);
    KnotSetUnity = cell(n, 1);
    
    % go through all dimensions and construct knot sets for the hypercube grid
    for i = 1:n
        %KnotSet{i} = (Extremes(i, 1):(gridres(i)*(Extremes(i, 2)-Extremes(i, 1))):Extremes(i, 2))';
        hcsize = (Extremes(i, 2)-Extremes(i, 1)) / hypercube_count(i);
        %KnotSet{i} = (Extremes(i, 1):(1/hypercube_count(i)):Extremes(i, 2))';
        if (knotseqtype == 0)
            KnotSet{i} = grid_knot{i};
        elseif (knotseqtype == 1)
            KnotSet{i} = Extremes(i, 1):hcsize:Extremes(i, 2);
        end
        if (type == 1 || type == 11)
            KnotSetTII{i} = KnotSet{i};
        elseif (type == 2 || type == 3)
            KnotSetTII{i} = Extremes(i, 1):hcsize/2:Extremes(i, 2); % double grid resolution with Type II
        end
        HCKnotSet{i} = 1:hypercube_count(i);
        HCSizeSet{i} = KnotSet{i}(2:end) - KnotSet{i}(1:end-1);
        KnotSetUnity{i} = [0; 1];
    end
    % construct the grids with hypercube corner coordinates
    % build the unity hypercube triangulation
    [HCUnityGridSet{:}] = ndgrid(KnotSetUnity{:});

    if (n == 1)
        PHI = (KnotSet{:})';
        TRI = [(1:size(PHI,1)-1)' (2:size(PHI,1))'];
        return;
    end
      
    
    % construct the prototype hypercube triangulation and vertex set
    PHIproto = zeros(length(HCUnityGridSet{1}(:)), n);
    for i = 1:n
        PHIproto(:, i) = HCUnityGridSet{i}(:);
    end
    % a Type II triangulation has a single extra vertex at the center of the hypercube
    if (type == 2 || type == 3)
        centervertex = zeros(1, n);
        for i = 1:n
            centervertex(i) = (max(PHIproto(:, i)) - min(PHIproto(:, i))) / 2;
        end
        PHIproto = [PHIproto; centervertex];
        if (type == 3 && n == 2)
            PHIproto = [PHIproto; .5 0; 0 .5; 1 .5; .5 1];
        end
    end
    % sort rows of vertices and vertex indices
    PHIproto = sortrows(PHIproto);
    if (type == 1 || type == 2)
        TRIproto = delaunayn(PHIproto, {'Qt','Qbb','Qc', 'Qz', 'Qx'}); % 'Qt' and 'Qz are necessary!
    elseif (type == 11)
        TRIproto = [1 2 3; 2 3 4]; % only defined for N = 2
    elseif (type == 3)
        TRIproto = [1 4 5; 1 2 5; 2 3 5; 3 5 6; 4 5 7; 5 7 8; 5 8 9; 5 6 9];
    end
    Vproto   = size(PHIproto, 1);
    Tproto   = size(TRIproto, 1);
    %TRIproto = sortrows(TRIproto);

    
    % Do the CDV triangulation, which means that the hypergrid is triangulated
    % on a per-hypercube basis. This triangulation method assumes that the prototype
    % triangulation is symmetric, thus leading to a symmetric fill-pattern
    
    % Create the hypercube indexer
    HCGridSet = cell(n, 1);
    [HCGridSet{:}] = ndgrid(HCKnotSet{:});
    HCCount = numel(HCGridSet{1});
    HCIndexer = zeros(HCCount, n);
    for i = 1:n
        HCIndexer(:, i) = HCGridSet{i}(:);
    end
    
    % preallocate the global triangulation 
    TRIglobal = zeros(HCCount * Tproto, n+1);
    PHIglobal = zeros(HCCount * Vproto, n);
    
    % go through all hypercubes, and insert triangulated hypercube prototype
    for i = 1:size(HCIndexer, 1)
        % index of the i-th hypercube
        hcidx = HCIndexer(i, :);
        
        % Offset and scale the triangulated hypercube prototype to the current (i-th) hypercube
        PHIscaled = PHIproto;
        for j = 1:n
            PHIscaled(:, j) = PHIproto(:, j) * HCSizeSet{j}(hcidx(j)) + KnotSet{j}(hcidx(j));
        end
        
        % insert the prototype triangulation + offset into the global triangulation
        TRIglobal((i-1)*Tproto+1:i*Tproto, :) = TRIproto + (i-1)*Vproto;
        % insert the scaled and offset vertex set into the global vertex set
        PHIglobal((i-1)*Vproto+1:i*Vproto, :) = PHIscaled;
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
    % Test whether user wants a guaranteed unique vertex matrix PHI
    if (~uniquevertices)
        PHI = PHIglobal;
        TRI = sort(TRIglobal, 2);
        return;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Get rid of duplicate vertices! (which is a surprisingly difficult operation!)
    [TRIglobal PHIglobal] = bsplinen_removeDuplicateVerticesFromTRI(TRIglobal, PHIglobal);
    
    % assign to output matrices
%    TRI = TRIglobal;
    TRI = sort(TRIglobal, 2);
    PHI = PHIglobal;
    
    % sort vertices according to their coordinates, if required
    if (uniquevertices == 2)
        [PHI Isort] = sortrows(PHI);
        [tmp Iinvsort] = sort(Isort);
        TRIL = Iinvsort(TRI(:));
        TRI = sort(reshape(TRIL, size(TRI)), 2);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % check whether the user wants to do a convex hull fill
    if (~cvhullfillmode)
        return;
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Do the Convex hull fill
    T = size(TRIglobal, 1);

    % coordinates of the convex hull
    Xhull   = CVHULL{1};
    % index vector of convex hull faces
    Chull = CVHULL{2};
    % sample interval of the convex hull
    ChullSampInt = 0; 
    ChullLoc = unique(Chull(:));
    if (~isempty(CVHULL{3}))
        if (length(CVHULL{3}) == 1)
            ChullSampInt = CVHULL{3};
        else
            ChullLoc = CVHULL{3};
        end
    end
    % get true convex hull points
    PHIhull = Xhull(ChullLoc, :);
    % rebuild the convex hull from PHIhull
    Chull = convhulln(PHIhull);
    % resample the convex hull, if required
    if (ChullSampInt ~= 0)
        % sample the convex hull if a minimum vertex distance has been given
        [PHIhull Chull] = bsplinen_sampleConvexHull(PHIhull, Chull, ChullSampInt);  
%     else
%         % rebuild the convex hull from PHIhull
%         Chull = convhulln(PHIhull);
    end
    % triangulate the convex hull
    TRIhull = delaunayn(PHIhull, {'Qt','Qbb','Qc', 'Qz', 'Qx'});
    
    % parameters used for filltype = 1
    RemoveMode = 0;   % specifies how vertices and simplices are removed
    RemoveDist = -1;  % specifies the minimum distance at which vertices will be removed
    % If filltype = 1 the convex hull of the data will be used to generate the triangulation. The 
    % minimum distance at which vertices will be removed is set here.
    if (filltype == 1)
        % user supplied set of locations at which the hull must be sampled, or alternatively 
        % a scalar specifying sampling interval on the hull

        % if RemoveMode > 0, then a RemoveDist parameter must be supplied which specifies the minimum
        % distance between grid-cvhull intersections and cvhull points/gridpoints.
        RemoveMode = CVHULL{4};
        if (isempty(CVHULL{5}))
            % calculate optimal distance from knotset (only used when RemoveMode > 0)
            RemoveDist = zeros(1, n);
            for i = 1:n
                RemoveDist(i) = .5 * min(HCSizeSet{i});
            end
        else
            RemoveDist = CVHULL{5};
            if (length(RemoveDist) ~= n)
                RemoveDist = RemoveDist(1) * ones(1, n);
            end
        end
    end
    
    % the set of all vertices in the global triangulation
    PHIset = PHIglobal(TRIglobal', :);% transpose is important!!!

    % go through all simplices and check whether there are any that are outside of the CVHull
    %for i = 1:T
    IMap = bsplinen_tsearchn(PHIhull, TRIhull, PHIset);
    %end
    exCVhull = zeros(T, 1);
    
    % For filltype 0 and 1, all simplices that have one or more vertices outside the convex hull will be removed. 
    % For filltype 2, no simplices will be removed that have at least one vertex inside the convex hull
    if (filltype < 2)
        for i = 1:T
            if (any(isnan(IMap((i-1)*(n+1)+1:i*(n+1)))))
                exCVhull(i) = 1;
            end
        end
    elseif (filltype >= 2)
        for i = 1:T
            if (all(isnan(IMap((i-1)*(n+1)+1:i*(n+1)))))
                exCVhull(i) = 1;
            end
        end
    end
    % These are the indices of the simplices that are outside of the CVHull. However, this list may be to unconservative
    % that is, it may contain simplices which vertices may lay outside of CVHull, but which may contain part of the 
    % CVHull. We don't want to be cutting corners, at least not with FILLTYPE = 1 ;) !
    exCVhullidx = find(exCVhull);

    if (filltype == 0)
        % Remove all simplices which have ANY (at least one) of their vertices outside of the convex hull.
        % This filltype underfits the Convex hull!
        removevertexlist = unique(TRIglobal(exCVhullidx, :));
        % filter the removal list
        TRItmp = TRIglobal;
        TRItmp(exCVhullidx, :) = [];
        % don't remove vertices that are still part of some other simplex
        notremove = intersect(TRItmp(:), removevertexlist);
        doremove = setxor(notremove, removevertexlist);
        
        % remove vertices, and recalculate TRI
        [TRIglobal PHI] = bsplinen_removeVertexFromTRI(TRIglobal, PHIglobal, doremove);
        TRIglobal(exCVhullidx, :) = [];   
        
    elseif (filltype == 1)
        % Remove all simplices which have ANY (at least one) of their vertices outside of the convex hull.
        % This filltype underfits the Convex hull!
        removevertexlist = unique(TRIglobal(exCVhullidx, :));
        % filter the removal list
        TRItmp = TRIglobal;
        TRItmp(exCVhullidx, :) = [];
        % don't remove vertices that are still part of some other simplex
        notremove = intersect(TRItmp(:), removevertexlist);
        doremove = setxor(notremove, removevertexlist);
        
        % remove vertices, and recalculate TRI
        [TRIglobal PHIglobal] = bsplinen_removeVertexFromTRI(TRIglobal, PHIglobal, doremove);
        TRIglobal(exCVhullidx, :) = []; 
        
        % now start placing vertices on the convex hull by either sampling the hull, or by using the hull itself
        if (type == 2)
            PHIint = bsplinen_getConvexhullGridIntersection(Chull, PHIhull, KnotSetTII);
        else
            PHIint = bsplinen_getConvexhullGridIntersection(Chull, PHIhull, KnotSet);
        end
        
        % the total current size of the global vertex array
        Vglobal = size(PHIglobal, 1);
        
        if (RemoveMode == 0)
            % RemoveMode = 0: just append the list of vertices, no removal
            PHIglobal = [PHIglobal; PHIhull; PHIint];
            % create a temporal triangulation from the vertices including the convex hull vertices
            TRItmp = delaunayn(PHIglobal, {'Qt','Qbb','Qc', 'Qz', 'Qx'});
            % now append the triangulation only with simplices that contain at least 1 new vertex
            [ridx cidx] = find(TRItmp > Vglobal);
            %TRIglobal = TRItmp; 
            TRIglobal = [TRIglobal; TRItmp(ridx, :)];
            PHI = PHIglobal;
        elseif (RemoveMode == 1)
            % RemoveMode = 1: first replace gridpoints that are too close to cvhull points with cvhull points, then
            %   replace vertices that are too close to gridpoints or cvhull points with cvhull points
            
            % first remove intersectpoints that are too close to the cvhull points, also remove intersection points
            % that are too close to other intersection points
            PHIhulltmp = [PHIhull; zeros(size(PHIint))];
            hullsize = size(PHIhull, 1);
            for gpidx = 1:size(PHIint, 1)
                gphi = PHIint(gpidx, :);
                goodgphi = 1;
                for hpidx = 1:size(PHIhulltmp, 1)
                    hphi = PHIhulltmp(hpidx, :);
                    % if any of the distance coordinates are smaller than RemoveDist, then add the grid vertex to the removal list.
                    if (all(abs(gphi-hphi) < RemoveDist))
                        goodgphi = 0;
                        break;
                    end
                            
                end
                % this gridpoint was a good gridpoint, and will be added to the temporary list
                if (goodgphi)
                    hullsize = hullsize + 1;
                    PHIhulltmp(hullsize, :) = gphi;
                end
            end
            
            % truncate convex hull vertex matrix
            PHIhull = PHIhulltmp(1:hullsize, :);
                        
            % now remove gridpoints that are too close to the cvhull points
            remidx = zeros(size(PHIglobal, 1), 1);
            remcount = 0;
            for gpidx = 1:size(PHIglobal, 1)
                gphi = PHIglobal(gpidx, :);
                for hpidx = 1:size(PHIhull, 1)
                    hphi = PHIhull(hpidx, :);
                    % if any of the distance coordinates are smaller than RemoveDist, then add the grid vertex to the removal list.
                    if (all(abs(gphi-hphi) < RemoveDist))
                        remcount = remcount + 1;
                        remidx(remcount, 1) = gpidx;
                        break;
                    end
                end
            end
            % truncate removal vector
            remidx = remidx(1:remcount, :);
            
            % remove bad gridpoints from PHI and TRI, and also return the reduced TRI
            [TRIglobal PHIglobal TRIreduced] = bsplinen_removeVertexFromTRI(TRIglobal, PHIglobal, remidx);
            Vglobal = size(PHIglobal, 1);
            
            % Add the hull points to the reduced grid point set
            PHIglobal = [PHIglobal; PHIhull];

            % Get rid of duplicate vertices! (which is a surprisingly difficult operation!)
            [TRIglobal PHIglobal] = bsplinen_removeDuplicateVerticesFromTRI(TRIreduced, PHIglobal);
            
            % create a temporal triangulation from the vertices including the convex hull vertices
            TRItmp = delaunayn(PHIglobal, {'Qt','Qbb','Qc', 'Qz', 'Qx'});
            % now append the triangulation only with simplices that contain at least 1 new vertex; this
            % will prevent the painstakenly created Type I/II triangulation from being destroyed
            [ridx cidx] = find(TRItmp > Vglobal);
            TRIglobal = [TRIglobal; TRItmp(ridx, :)];
            PHI = PHIglobal;

            
        elseif (RemoveMode == 2)
            % RemoveMode = 2: replace vertices that are too close to gridpoints with gridpoints or cvhull points
            % first remove intersectpoints that are too close to the cvhull points, also remove intersection points
            % that are too close to other intersection points
            PHIhulltmp = [PHIhull; zeros(size(PHIint))];
            hullsize = size(PHIhull, 1);
            for gpidx = 1:size(PHIint, 1)
                gphi = PHIint(gpidx, :);
                goodgphi = 1;
                for hpidx = 1:size(PHIhulltmp, 1)
                    hphi = PHIhulltmp(hpidx, :);
                    % if any of the distance coordinates are smaller than RemoveDist, then add the grid vertex to the removal list.
                    if (all(abs(gphi-hphi) < RemoveDist))
                        goodgphi = 0;
                        break;
                    end
                            
                end
                % this gridpoint was a good gridpoint, and will be added to the temporary list
                if (goodgphi)
                    hullsize = hullsize + 1;
                    PHIhulltmp(hullsize, :) = gphi;
                end
            end
            
            % truncate convex hull vertex matrix
            PHIhull = PHIhulltmp(1:hullsize, :);
            
            % now move gridpoints that are too close to the cvhull points
            remidx = zeros(size(PHIglobal, 1), 2);
            remcount = 0;
            for gpidx = 1:size(PHIglobal, 1)
                gphi = PHIglobal(gpidx, :);
                for hpidx = 1:size(PHIhull, 1)
                    hphi = PHIhull(hpidx, :);
                    % if any of the distance coordinates are smaller than RemoveDist, then add the grid vertex to the removal list.
                    if (all(abs(gphi-hphi) < RemoveDist))
                        remcount = remcount + 1;
                        remidx(remcount, 1) = gpidx;
                        remidx(remcount, 2) = hpidx; % vertex gpidx will be replaced with cvhull vertex hpidx!
                        break;
                    end
                end
            end
            % truncate removal vector
            remidx = remidx(1:remcount, :);
            
            % remove grid vertices and replace with cvhull vertices
            PHIglobal(remidx(:,1), :) = PHIhull(remidx(:,2), :);
            % Get rid of duplicate vertices! (which is a surprisingly difficult operation!)
            [TRIglobal PHIglobal] = bsplinen_removeDuplicateVerticesFromTRI(TRIglobal, PHIglobal);
            Vglobal = size(PHIglobal, 1);
            
            % add the vertices in PHIhull that were not replaced to PHIglobal
            notremidx = setxor((1:size(PHIhull, 1))', remidx(:,2));
            PHIglobal = [PHIglobal; PHIhull(notremidx, :)];
            % Get rid of duplicate vertices! (which is a surprisingly difficult operation!)
            [TRIglobal PHIglobal] = bsplinen_removeDuplicateVerticesFromTRI(TRIglobal, PHIglobal);
            
            % create a temporal triangulation from the vertices including the convex hull vertices
            TRItmp = delaunayn(PHIglobal, {'Qt','Qbb','Qc', 'Qz', 'Qx'});
            % now append the triangulation only with simplices that contain at least 1 new vertex
            [ridx cidx] = find(TRItmp > Vglobal);
            %TRIglobal = TRItmp; 
            TRIglobal = [TRIglobal; TRItmp(ridx, :)];
            PHI = PHIglobal;
       
        end
        
    elseif (filltype == 2)
        % Go through the convex hull points and find the simplices in which they are contained, if any.
        % These simplices will not be removed, even though all their vertices are outside of the convex hull! 
        % This filltype will overfit the Convex hull!
        IMaphull = bsplinen_tsearchn(PHIglobal, TRIglobal, PHIhull);
        % find the indices of the simplices that are completely outside of the CVHULL
        removeidx = setxor(exCVhullidx, intersect(exCVhullidx, IMaphull));

        % remove the indices from exCVhullidx that are also in IMaphull;
        removevertexlist = unique(TRIglobal(removeidx, :));
        % filter the removal list
        TRItmp = TRIglobal;
        TRItmp(removeidx, :) = [];
        % don't remove vertices that are still part of some other simplex
        notremove = intersect(TRItmp(:), removevertexlist);
        doremove = setxor(notremove, removevertexlist);
        
        % remove vertices, and recalculate TRI
        [TRIglobal PHI] = bsplinen_removeVertexFromTRI(TRIglobal, PHIglobal, doremove);
        % remove simplices
        TRIglobal(removeidx, :) = [];
    
    end
    
    %TRI = TRIglobal;
    TRI = sort(TRIglobal, 2);
    
    


