%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluate the B-form polynomial
function V = bsplinen_evalBformExt(fn, InclusionMap, LambdaData)

    %   Determine the total number of evaluation points
    Xcount  = size(InclusionMap, 1);

    %   Initialize output
    %   (Multiplication with *coefs1(1,1)*coefs2(1,1) performed to
    %    ensure compatibility with interval computations)
    V       = nan(Xcount, 1)*fn.coefs(1);

    %   Determine number of simplices in the spline
    T       = size(fn.TRI,1);


    %   Initialize basis, basisKt, and d, dHat;
    [basis basisKt] = bsplinen_constructBasisExt(fn.dim,fn.degree(1));
    d       = ones(T,1)*fn.degree(1);
    dHat    = size(basis,1);

    %   Determine the subsets of the coefficients which correspond to each simplex.
    flagCoefsIdx = false;
    if isfield(fn,'coefsIdx') && size(fn.coefsIdx,1) == size(fn.TRI,1)
        %   indices of the subsets are available
        flagCoefsIdx = true;
    end

    % Check whether the spline function is a derivative from another,
    % in that case the B-form should be multiplied with the
    % differentiation factor. 
    dfactor     = fn.deriv_factor;
    if (dfactor == 0)
        dfactor = 1;
    end

    %   Cycle over all simplices
    counter = 0;
    for t = 1:T
        %   Check if any data points lie in the current simplex
        idx     = InclusionMap==t;
        if any(idx)
            %   Some data points lie in the current simplex

            %   Compute the output of the spline
            basisBt     = zeros(sum(idx),size(basis,1));
            for i = 1:size(basis,1)
                basisBt(:,i) = prod(LambdaData(idx,:).^(ones(sum(idx),1)*basis(i,:)),2);
            end

            V(idx,1) = dfactor*basisBt*(basisKt.*fn.coefs(counter+1:counter+dHat,1));
        end

        %   Update counter
        counter = counter + dHat;
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the polynomial basis and multinomial coefficients
    function [Basis BasisKt] = bsplinen_constructBasisExt(n, d)
        
        % the polynomial orders 0 and 1 require no calculations
        if (d == 0)
            Basis = zeros(1, n+1);
            BasisKt = 1;
            return;
        end

        Basis = eye(n+1, n+1);
        if (d == 1)
            BasisKt = ones(n+1, 1);
            return;
        end

        % Recursively increase the degree of the polynomial basis. This method
        % requires no sorting or removal of nonunique rows. 
        for i = 2:d
            dhat = size(Basis, 1);
            % increment vector of 1's
            inc = ones(dhat, 1);
            % the new matrix of polynomial basis indices
            basisnew = zeros(dhat * (n+1), n+1);
            % Go through all dimensions and add the increment vector (1's) to all
            % elements in the j-th column.
            for j = 1:n+1
                idx = (j-1)*dhat+1:j*dhat;
                basisnew(idx, j+1:end) = Basis(:, j+1:end);
                basisnew(idx, j) = Basis(:, j) + inc;
            end

            % remove all terms that do not sum up to d
            Basis = basisnew(sum(basisnew, 2) == i, :);

        end

        %   Calc the basis coefficient vector |kappa|! / kappa! 
        BasisKt =  factorial(d)./prod(factorial(Basis),2);
        
        
        
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the default STRUCT_BSPLINEN_OPTIONS structure
    function struct_bsplinen_options = bsplinen_constructOptionsStructExt()
        
        struct_bsplinen_options = struct('scheme', 'ls', 'solver', 'iterative', ...
            'parallelCores', 0, ...
            'filterH', 0, 'sortedH', 0, 'fullrankH', 0, ...
            'saveDELTA', 0, 'saveH', 0, 'saveBfb', 0, 'saveCOV', 0, 'saveX', 0, ...
            'loadDELTA', 0, 'loadH', 0, 'loadBfb', 0, 'loadCOV', 0, 'loadX', 0, ...
            'outputmsg', 0, ...
            'outputDELTA', 0, 'outputH', 0, 'outputBfb', 0, 'outputLambda', 0, ...
            'outputInclusionMap', 1,...
            'stat_calc', 0, 'stat_COVmethod', 'LS', 'stat_check', 0, 'stat_outputmsg', 0, ...
            'stat_outputX', 0, 'stat_outputCOV', 0, 'stat_outputSIGMA', 0, 'stat_correlationRadius', .2, ...
            'mreg_reduceMode', 'svd', 'mreg_outputMinv', 1, 'mreg_simpleMode', 0,...
            'resan_calc', 0, 'resan_userelativeRadius', 1, 'resan_radius', .1, ...
            'resan_calcAutocorr', 0, 'resan_perSimplex', 0, 'resan_outputmsg', 0, 'resan_plotstatistics', 0, ...
            'resan_evalGridCount', 10, 'resan_interpGridCount', 50,...
            'data_partsize', 100000, ...
            'bbuild_maxblkdiagsize', 500, 'bbuild_checkrank', 0,...
            'phi_optimizePHI', 0, 'phi_protectCHull', 1, 'phi_normalizedata', 1,...
            'phi_complexityMode', 1, 'phi_complexityLevel', .3, ...
            'phi_minGridCount', 2, 'phi_maxGridCount', 64,...
            'phi_cvhullres', 0.15, 'phi_gridres', 5, ...
            'tri_delaunayopts', [], 'tri_optimizePHI', 0, 'tri_protectCHull', 1, ...
            'tri_mindata', 50, 'tri_maxdata', -1, 'tri_cvhullres', 0.15, 'tri_gridres', 0.2, ...
            'tri_perfreq', 1e-2, 'tri_refinemode', 3, 'tri_refineMaxIts', 25, ...
            'tri_minangle', 20, 'tri_minvolume', .01, 'tri_maxvolume', .25, ...
            'tri_partitionmode', 'grid', 'tri_partitioncount', [], 'tri_partitionoverlap', 0, ...
            'solver_tol', 1e-6, 'solver_gradtol', 1e-10, 'solver_eps', 1e-6, 'solver_maxiter', 250, ...
            'solver_outputmsg', 0, 'solver_maxEpochs', 250, ... 
            'distrib_mode', 0, 'distrib_starlevel', 1, 'distrib_trisubsets', [], 'distrib_deltasubsets', [], ...
            'distrib_outputmsg', 1, ...
            'native_eval', 0, 'native_tsearchn', 0, 'native_construct', 0, 'native_gridres', 32, ...
            'native_fulltsearchn', 0, 'native_dir', '.\\bin', ...
            'saveMatPath', '.\\Matrix');

%------------------------------ end of file -------------------------------