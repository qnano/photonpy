"""
Script for plotting .csv data as point cloud in 3D.
"""

import pptk
import pandas as pd

import argparse


# point size for PPTK viewer
_POINT_SIZE = .01


def plot_pandas_data( data, x_name:str, y_name:str, z_name:str,
                      xy_pixelsize:float=1.0 ):
    """Plot pandas data as cloud points in 3D (see description below)"""

    data_loc = data
    data_loc[x_name] *= xy_pixelsize
    data_loc[y_name] *= xy_pixelsize

    v = pptk.viewer( data_loc[[x_name, y_name, z_name]] )
    v.attributes( data_loc[z_name] )
    v.set( point_size=_POINT_SIZE )


def plot_csv_file( filename:str, x_name:str, y_name:str, z_name:str,
                   xy_pixelsize:float=1.0 ):
    """Plot .csv file as cloud points in 3D

    :param filename: (str) .csv filename
    :param x_name, y_name, z_name: (str) names of x,y,z data columns inside the
        .csv file
    :param xy_pixelsize: (float) multiplication coefficient
        which will be applied to X,Y coordinates before the plotting
        (Usually, it is pixel size in nm or um,
        because x,y is in pixels and z in nm or um).
    """

    data = pd.read_csv( filename )
    plot_pandas_data( data, x_name, y_name, z_name, xy_pixelsize )


if __name__ == '__main__':
    # this filename will be rewritten by arguments
    #filename = 'example.csv'
    # this pixelsize will be rewritten by arguments
    pixelsize = 100 # nm

    parser = argparse.ArgumentParser( description="Plot .csv as 3D point cloud" )

    parser.add_argument( "-i", "--input", required=True,
                         help="Input CSV file (.csv)" )

    parser.add_argument( "-p", "--pixelsize", default=None, type=float,
                         help="Pixelsize for X,Y" )

    args = parser.parse_args()

    filename = args.input
    if args.pixelsize is not None:
        pixelsize = args.pixelsize

    plot_csv_file( filename=filename,
                   x_name="x_pix", y_name="y_pix", z_name="z_nm",
                   xy_pixelsize=pixelsize )

    print( 'DONE!' )