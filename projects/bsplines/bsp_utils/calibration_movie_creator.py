"""
Creates calibration movie file from separate calibration PSFs.

Basically, insert PSFs into an array in special order
(see class description below).
Produced 3D array can be saved in .tif file in order to simulate a real
calibration movie.
"""

import numpy as np
import math

import argparse
from pathlib import Path
import tifffile
import scipy.io


class CalibrationMovieCreator:
    """Creates the movie file from calibration PSF beads (z-stack beads).

    Place every calibration PSF bead in the place in the big movie array
    which can be easily saved as .tif file (using tifffile package, for ex.).

    Output movie file will be always a square one for X,Y direction.
    In Z direction number of layers will be the same as for PSFs.

    Number of PSF columns in the movie file is ceil( sqrt( Nbeads ) ).

    Example for 5 calibration beads:

    ------------------------------------------------------------------------
    |         ^                                                            |
    |         | dist                                                       |
    |   dist  v             dist                                           |
    | <------> ---------- <------> ---------- <------> ---------- <------> |
    |          |        |          |        |          |        |          |
    |          |  PSF_0 |          |  PSF_1 |          |  PSF_3 |          |
    |          |        |          |        |          |        |          |
    |          ----------          ----------          ----------          |
    |         ^                                                            |
    |         | dist                                                       |
    |         v                                                            |
    |          ----------          ----------                              |
    |          |        |          |        |                              |
    |          |  PSF_4 |          |  PSF_5 |                              |
    |          |        |          |        |                              |
    |          ----------          ----------                              |
    |         ^                                                            |
    |         | dist                                                       |
    |         v                                                            |
    ------------------------------------------------------------------------

    Attributes:
        background (float): background rate for image. Will be add to all
            pixels without the PSF information.

        pixel_distance (int): number of pixels between PSFs and between a PSF
            and a border. If it is not set, the ROI/2 will be used by default.

        use_poisson (bool): if True, Poisson noise will be used for
            background pixels.
    """

    def __init__( self, background:float,
                  pixel_distance:int=-1,
                  use_poisson:bool=True ):
        self._FTYPE = np.float32

        self.background = background
        self.dist = pixel_distance
        self.use_poisson_noise = use_poisson

    def movie( self, PSFs ):
        """Produce the movie with calibrations PSFs without changing the PSFs

        :param PSFs: (4D array) [bead idx][z slice idx][row idx][column idx]
            Array with all PSFs that will be placed in the calibration movie
            file.

        :return 3D numpy array [z slice idx][row idx][column idx]
            The calibration PSfs movie that contains all calibration PSFs.
        """

        # PSFs [bead idx][z slice idx][y idx][x idx]
        Nbeads = len( PSFs )
        if Nbeads <= 0:
            raise Exception( "PSFs in CalibrationMovieCreator should contain "
                             "at least one bead. "
                             f"Now it is {Nbeads} beads" )

        Nz_slices = len( PSFs[0] )
        if Nz_slices <= 0:
            raise Exception( "PSFs in CalibrationMovieCreator should contain"
                             " at least one z slice. "
                             f"Now it is {Nz_slices} z slices" )

        ROI = len( PSFs[0][0] )
        if ROI <= 0:
            raise Exception( "PSFs in CalibrationMovieCreator should have "
                             "ROI more than 0. "
                             f"Now ROI is {ROI} pixels" )

        # if distance between PSFs isn't set, set it as half of the ROI size
        dist = self.dist
        if self.dist < 0:
            dist = ( ROI + 1 ) // 2

        # number of PSFs in one row in the movie array
        Ncol = math.ceil( math.sqrt( Nbeads ) )
        Npix = Ncol * ROI + ( Ncol + 1 ) * dist

        # fill by background and use Poisson noise (if needed)
        cmovie = np.zeros( ( Nz_slices, Npix, Npix ), dtype=self._FTYPE )
        cmovie.fill( self.background )
        if self.use_poisson_noise:
            movie = np.random.poisson( cmovie )

        # place beads into the movie
        for ibead in range( Nbeads ):
            # index for a bead in the movie file (in beads)
            icol = ibead % Ncol
            irow = ibead // Ncol

            # pixel index for the bead in the movie file (in pixels)
            icol_pix = dist + icol * ( dist + ROI )
            irow_pix = dist + irow * ( dist + ROI )

            cmovie[:, irow_pix : irow_pix + ROI, icol_pix : icol_pix + ROI ] =\
                PSFs[ibead]

        return cmovie



    def movie_from_normalized( self, PSFs_normalized, intensities ):
        """Produce the movie with calibrations PSFs from normalized PSFs.

        Changes PSFs first according to the intensities and background and
        only then places the PSFs into the movie.
            add_Poisson_noise( PSF*intensity + bg )

        :param PSFs_normalized: (4D array)
            [bead idx][z slice idx][row idx][column idx]
            Array with all PSFs that will be placed in the calibration movie
            file. PSFs should be normalized, for example, by division to
            sum for in-focus slice.

        :param intensities: (1D float array) [bead idx]
            Intensity for every calibration bead.

        :return 3D numpy array [z slice idx][row idx][column idx]
            The calibration PSfs movie that contains all calibration PSFs.
        """
        PSFs = np.array( PSFs_normalized, dtype=self._FTYPE )
        PSFs *= intensities
        PSFs += self.background

        if self.use_poisson_noise:
            PSFs = np.random.poisson( PSFs )

        return self.movie( PSFs )


if __name__ == '__main__':
    # some parameters
    background = 700.0
    dist_between_PSFs = -1
    use_poisson = True

    parser = argparse.ArgumentParser(
        description="Test for the calibration movie creator" )

    parser.add_argument( "-i", "--input", required=True,
                         help="Input file with calibration beads" )
    parser.add_argument( "-o", "--output", required=True,
                         help="Output .tif calibration movie file" )
    parser.add_argument( "-b", "--bg", default=None, type=float,
                         help="Backgound" )
    parser.add_argument( "-d", "--dist", default=None, type=int,
                         help="Distance between PSFs in pixels" )

    parser.add_argument( "--poissonoff", default=False, action="store_true",
                         help="If presented, Poisson noise will not be used" )

    args = parser.parse_args()

    if args.poissonoff:
        use_poisson = False

    if args.bg is not None:
        background = args.bg

    if args.dist is not None:
        dist_between_PSFs = args.dist

    input_file = args.input
    output_file = args.output

    # check input file extension
    file_ext = Path( input_file ).suffix
    if file_ext != '.mat':
        raise Exception( "Input file extension should be .mat. Or you should "
                         "create additional code here for more extensions." )

    print( f"input_file = {input_file}" )
    print( f"output_file = {output_file}\n" )

    print( f"background = {background}" )
    print( f'use Poisson = {use_poisson}' )
    print( f"distance between PSFs = {dist_between_PSFs}\n" )

    # load calibration PSfs
    print( 'Read input file...' )
    mat = scipy.io.loadmat( input_file )
    psfs = mat['PSFs']

    # create movie with calibration PSFs
    print( 'Creating calibration movie file...' )
    cmovie_creator = CalibrationMovieCreator( background=background,
                                              pixel_distance=dist_between_PSFs,
                                              use_poisson=use_poisson )

    cmovie = cmovie_creator.movie( PSFs=psfs )

    # saving
    print( 'Saving the calibration movie file...' )
    with tifffile.TiffWriter( output_file ) as tif:
        tif.save( cmovie, compress=2 )

    print( 'DONE!' )
