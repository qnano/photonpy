"""
Auxilary functions for B-spline utils.
"""

import os
import numpy as np
import tifffile


def cropping_spots_array( arr, new_ROI:int ):
    """Cropping 3D array to new ROI size symmetrically

    :param arr: (3D array) [spot idx][x idx][y idx]
    :param new_ROI: (int) new number of values through x or y
    :return: 3D array with cropped x and y size
    """

    ROI_X = arr.shape[1]
    ROI_Y = arr.shape[2]

    if ROI_X < new_ROI or ROI_Y < new_ROI:
        raise Exception( 'Old ROI size of the 3D array should be bigger '
                         'than new ROI size, because of cropping' )

    from_idx_x = ROI_X // 2 - new_ROI // 2
    to_idx_x = ROI_X // 2 + (new_ROI + 1) // 2
    from_idx_y = ROI_Y // 2 - new_ROI // 2
    to_idx_y = ROI_Y // 2 + (new_ROI + 1) // 2

    arr_new = arr[:, from_idx_x : to_idx_x, from_idx_y : to_idx_y ]

    return arr_new


def save_PSFs_separately( PSFs, folder_name:str, base_name='stack' ):
    """Saves beads from 4D numpy array to separate files"""

    if PSFs is None:
        raise Exception( 'PSF stacks should be set for '
                         'saving them separately into .tif files' )

    for i_slice, stack in enumerate( PSFs ):
        filename = os.path.join( folder_name, f'{base_name}_{i_slice}.tif' )
        with tifffile.TiffWriter( filename ) as tiff:
            for i in stack:
                tiff.save( i.astype( np.int32 ) )


def shift_index_clockwise_for_3D_arr( arr_3d ):
    """Shifts index clockwise for 3D numpy array

    Args:
        arr_3d (numpy[][][]): 3 dimensional array
    Returns:
        3 dim numpy array with shifted indices -
            [i][j][k] -> [k][i][j]
    """

    return np.moveaxis( arr_3d, len( arr_3d.shape ) - 1, 0 )


def change_last_two_indices_for_3D_arr( arr_3d ):
    """Changes last 2 indices of 3 dim array like [i][j][k] -> [i][k][j]

    Args:
        arr_3d (numpy[][][]): 3 dimensional array
    Returns:
        3 dim numpy array with shifted indices -
            [i][j][k] -> [i][k][j]
    """

    return np.moveaxis( arr_3d, 1, 2 )
