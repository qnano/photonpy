% Function for launching and debugging B-spline fitting stage.

close all;
clear all;
clc;

step_02_fit_spline( ...
    '../../../OUTPUT/stacks.mat', ... % input .mat file with calibration 
    ...                               % PSFs
    '../../../OUTPUT/calibration_bspline.mat', ... % output .mat file with 
    ...                                            % all B-spline parameters
    5, ... % polynomial degree
    5, ... % number of cubes for a triangulation per direction (X,Y,Z)
    1 ... % continuity order
);
