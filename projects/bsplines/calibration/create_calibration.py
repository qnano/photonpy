import sys
import os
if __name__ == '__main__':
    sys.path.append( '../..' )

import argparse
from tqdm import tqdm
import tempfile
import numpy as np
import matplotlib.pyplot as plt
import tifffile
import pickle
import time
from pathlib import Path
import scipy.optimize
from scipy.optimize import least_squares
import scipy.io
from multiprocessing import Pool

from photonpy.gaussian.fitters import fit_sigma_3d, Params3D
from photonpy.cpp.lib import SMLM
from photonpy.cpp.context import Context
import photonpy.cpp.spotdetect as spotdetect
from photonpy.utils.peaks import get_peak_positions, get_rois, get_rois_corner
from photonpy.cpp.gaussian import Gaussian

if __name__ == '__main__':
    sys.path.append( '..' )
from bsplines.bsp_utils.movie import load_movie

try:
    from storm_analysis.spliner.measure_psf_beads import measurePSFBeads
    from storm_analysis.spliner.measure_psf_utils import alignPSFs
    from storm_analysis.spliner.psf_to_spline import psfToSpline
except ImportError as e:
    print( "storm analysis toolbox needs to be installed: https://github.com/ZhuangLab/storm-analysis/releases" )
    raise e

# if True, calibration PSF stacks will be saved in .tif files "stack_x.tif"
# where x is the number of a calibration stack
_SAVE_CALIBR_BEADS = False

############################## C spline ########################################

def localize_peaks(frame, roi_size):
    # roi_size = 14
    positions = get_peak_positions(frame)
    rois = get_rois(frame, positions, roi_size=roi_size)

    result = []
    for roi, pos in zip(rois, positions):
        r = fit_sigma_3d(roi)
        x = pos[1] - roi_size / 2.0
        y = pos[0] - roi_size / 2.0

        result.append((r[Params3D.X] + x, r[Params3D.Y] + y))

    return np.asarray(result)


def calibrate_c_spline(movie_name, output, args):
    range_nm = args.zrange

    time_cspline_calib_beg = time.time()
    movie = load_movie(movie_name)

    # z_range in micrometers
    z_range = np.linspace(-range_nm * 1e-3, range_nm * 1e-3, num=len(movie))
    in_focus = movie[int(len(movie) / 2)]
    positions = localize_peaks( in_focus, roi_size=args.roi )

    beads_file = tempfile.NamedTemporaryFile("w", delete=False)
    for pos in positions:
        beads_file.write("%f %f\n" % (pos[0], pos[1]))
    # closing temporary file in that manner can cause the removal of  the file
    beads_file.close()

    z_file = tempfile.NamedTemporaryFile("w", delete=False)
    for z in z_range:
        z_file.write("1 %f\n" % z)
    # closing temporary file in that manner can cause the removal of  the file
    z_file.close()

    psf_file = tempfile.NamedTemporaryFile("w", delete=False)
    # closing temporary file in that manner can cause the removal of  the file
    psf_file.close()

    z_rng = float(np.max(z_range))
    z_step = np.diff(z_range)[0]

    aoi_size = 7
    measurePSFBeads(
        movie_name, z_file.name, beads_file.name, psf_file.name, z_range=z_rng, z_step=z_step, aoi_size=aoi_size
    )
    psfToSpline(psf_file.name, output, 2*aoi_size - 1)

    time_cspline_calib_end = time.time()
    time_cspline_calib = time_cspline_calib_end - time_cspline_calib_beg
    print( f'time of C-spline calibration = {time_cspline_calib:.2f} sec' )

    # save data in .npz format
    data_C_spline = pickle.load( open( output, 'rb' ) )
    np.savez( args.output,
              zmin=float( z_range[0] * 1e3 ),
              zmax=float( z_range[-1] * 1e3 ),
              coeff=data_C_spline["coeff"] )


############################## B splines #######################################

class BsplineCalibrationStackResult:
    """Result of the calibration.

    This structure was created in order to return calibration result
    not only in .mat file, but also as usual python return value.

    Attributes:
        roisize (int): number of pixels for PSF in Ox, Oy directions.
        n_z_slices (int): number of slices in Z stack.
        z_range (float): total z range in um. It means that the z calibration
            stacks are going from -z_range/2 to +z_range/2
        stacks (4D numpy array): all calibration stacks
            [stack index][x pixel index][y pixel index][z slice index].
        shifts (2D numpy array): x,y,z shifts per calibration stack
            [stack index][axis index (0-x, 1-y, 2-z)].
        dx (float): shift according to the average PSF for X axis (?)
        dy (float): shift according to the average PSF for Y axis (?)
        dz (float): shift according to the average PSF for Z axis (?)
        PSF_average (3D numpy array): the average calibration PSF stack
            [x pixel index][y pixel index][z slice index].
    """

    def __init__(self, roisize:int, n_z_slices:int,
                 z_range:float,
                 stacks,
                 shifts,
                 dx, dy, dz,
                 PSF_average):
        self.roisize = roisize
        self.n_z_slices = n_z_slices
        self.z_range = z_range
        self.stacks = stacks
        self.shifts = shifts
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.PSF_average = PSF_average

    def to_mat_file( self, mat_filename ):
        """Saves the structure's data to .mat file"""

        store = {
            'n_pixels': float( self.roisize ),
            'n_z_slices': float( self.n_z_slices ),
            'z_range': float( self.z_range ),
            'stacks': self.stacks,
            'shifts': self.shifts,
            'dx': self.dx,
            'dy': self.dy,
            'dz': self.dz,  # TODO: convert to microns
            'PSF_average': self.PSF_average
        }

        scipy.io.savemat( mat_filename, store )



def calculate_bleaching_rate( movie, plot=True ):
    """ Calculate bleaching rate for every z slice.

    Function uses scipy minimization algorithm for fit function
    I = v*( a*exp(-b*t) + c*exp(-d*t) )
    into current data set. Instead of time z slice index is used.
    v,a,b,c,d - independent variables of minimization algorithm.

    The intensity is calculated as sum of all pixel values for one z slice.

    Args:
        movie (numpy array[][][]): 3D array with calibration beads.
        plot (bool): if true, intensity(z slice index) plot will be shown

    Returns:
        1D array with the bleach rate. Size is equal to the number of z slices
            in movie array.
    """

    # initial intensity of every z slice
    Is = np.sum(movie, axis=(1, 2))
    # an array filled with indices from 0 to (number of z slices - 1)
    z = np.arange(0, len(Is))

    # minimization stage. a, b, c, d values will be calculated
    def fun(x):
        _, a, b, c, d = x
        br = a * np.exp(-b * z) + c * np.exp(-d * z)
        return br

    def obj(x):
        I = fun(x) * x[0]
        return np.sum((I - Is)**2)

    x0 = [Is[0], 0.5, 1e-4, 0.5, 1e-4]
    r = scipy.optimize.minimize(obj, x0)
    br = fun(r.x)

    if plot:
        plt.figure()
        plt.plot(Is, '.', label='Intensity')
        plt.plot(r.x[0] * br, '--', label='Fitted bleaching rate')
        plt.xlabel('Frame #')
        plt.ylabel('Intensity [photons]')
        plt.legend()
        plt.show()

    return br


def clipping_calibration_stack( movie,
                                zrange:float,
                                zstep:float,
                                infocusIdx:int ):
    """ Clip calibration stack to the slices around the in-focus slice.

    Clips z slices to the interval
    from (infocusIdx - int(zrange/zstep)) to (infocusIdx + int(zrange/zstep))

    Args:
        movie (numpy array [][][]): 3D array with calibration stacks.
        zrange (float): z range in nm around in-focus slice
        zstep (float): z step in nm for estimating number of slices in the z range.
            if it is less than 0, it is considered like not set.
        infocusIdx (int): index of the z slice which is considered to be located
            in the focus of the lens

    Returns: (array[][][], int)
        Clipped to the z range around in-focus slice 3D array with
        calibration stacks.
        If z step is less than 0, input movie array will be return.

        Also return new in-focus slice idx
    """

    if zstep > 0:
        num_slices = int( zrange / zstep )

        if num_slices <= infocusIdx and \
                ( num_slices + infocusIdx ) < len(movie):

            clipped_movie = movie[infocusIdx - num_slices : infocusIdx + num_slices + 1]
            return clipped_movie, int( len(clipped_movie) / 2 )

        else:
            print( 'WARNING: check step size!',
                   '    Now zrange/zstep is more than inFocus index',
                   '    in focus idx = ' + str( infocusIdx ),
                   '    zrange/zstep = ' + str( num_slices ), '', sep='\n' )
            assert( num_slices <= infocusIdx )
            assert( ( num_slices + infocusIdx ) < len(movie) )
    else:
        return movie, infocusIdx


def find_infocus_slice_idx( movie ) -> int:
    """Function finds z slice which is in focus"""

    max_intensity_in_slices = np.max( movie, axis=( 1, 2 ) )
    foc = np.argmax( max_intensity_in_slices )

    #return int( len( movie ) / 2 )
    return foc


def spot_detection(roisize:int, in_focus:int, movie):
    """Detects spot in the movie file

    Args:
        roisize (int): number of pixels in one PSF through Ox, Oy axis.
            For example, ROI=14 => PSF is 14x14 pixels.
        in_focus (int): index of z slice that is located in the focus.
        movie (3D numpy array): full field of view with all calibration beads.
            [z slice index][x pixel index][y pixel index].

    Returns:
        psfs (4D numpy array): separated calibration stacks.
            [stack index][z slice index][x pixel index][y pixel index].
            For x,y pixel indices the min value is 0 and the max value is
            ROI-1.
        theta (1D numpy array): 4 float numbers. Unfortunately,
            I don't know anything about internal structure of theta.
    """

    with SMLM(debugMode=False) as smlm, Context(smlm) as ctx:
        psfSigma = 2
        min_intensity = 50
        spotDetectorConfig = spotdetect.SpotDetectorConfig(psfSigma, roisize, minIntensity=min_intensity)
        processFrame = spotdetect.SpotDetectionMethods(ctx).ProcessFrame
        rois, cornerpos, scores = processFrame(in_focus, spotDetectorConfig, 25)

        print(cornerpos.shape[0], 'beads found')
        print(cornerpos)

        gaussian = Gaussian(ctx)
        psf = gaussian.CreatePSF_XYIBg(roisize, psfSigma, False)
        theta, _, tr = psf.ComputeMLE(rois, maxiterations=100, levmarInitialAlpha=5.0)
        theta = theta[0] # what is theta?

    positions = np.flip(cornerpos, axis=1)
    psfs = get_rois_corner(movie, positions, roisize)

    return psfs, theta


def estimate_calibration_beads_shifts( psfs, dx:float, dy:float, verbose=True ):
    """ Estimates calibration beads shift considering each other.

    Args:
        psfs (numpy array[][][][]): 4D array with calibration beads.
            psfs[i] is a calibration 3D stack (1 bead).
        dx (float): estimated correction for X (?).
        dy (float): estimated correction for Y (?).

    Returns:
        shifts (numpy arr[][]): x,y,z shift for every calibration bead.
        dx (float): adjusted correction for X (?).
        dy (float): adjusted correction for Y (?).
        Average PSF (numpy arr[][][]): as 3D numpy array (calibration bead size).
    """
    if len(psfs) > 1:
        average_stack, score, shifts = alignPSFs( psfs, verbose=verbose )
    else:
        average_stack = psfs[0]
        shifts = np.zeros( (1, 3), dtype=float )

    if verbose:
        print( shifts.shape )
    shifts[:, [0, 1, 2]] = shifts[:, [1, 0, 2]]

    if verbose:
        print( "Pre correction" )
        print( shifts[:, :2] )

    # Shift of average stack vs 0
    dx_a = dx + shifts[0, 0]
    dy_a = dy + shifts[0, 1]

    # Position of stacks
    shifts = np.array( [dx_a, dy_a, 0] ) - shifts

    if verbose:
        print( "Post correction" )
        print( shifts )

    return shifts, dx_a, dy_a, average_stack


def save_separate_beads_into_files( psfs, out_dir ):
    """Saves beads from 4D numpy array to separate files - one file per bead"""

    for i_slice, stack in enumerate( psfs ):
        filename = os.path.join( out_dir, f'stack_{i_slice}.tif' )
        with tifffile.TiffWriter( filename ) as tiff:
            for i in stack:
                tiff.save( i.astype( np.int32 ) )


def calibrate_b_spline( movie_name, mat_outfile,
                        roisize, zrange, zstep,
                        offset, emGain, conversion,
                        shifts_replacement=None ):
    # movie - array from .tif file. [z_layer][y_pix][x_pix]
    # all calibration beads should be localized
    out_dir_name = os.path.dirname( mat_outfile )
    file_ext = Path( movie_name ).suffix

    if file_ext == '.tif':
        movie = load_movie(movie_name)
        movie = movie.astype(np.float64) # TODO: is it necessary to use double?

        adu2phot = conversion / emGain
        movie = (movie - offset) * adu2phot

        # All pixels with value < 0 should be clipped to 0
        if np.min(movie) < 0:
            print("Warning: movie contains counts < 0. Clipping!")
            persent_clipped = 100. * np.sum(movie < 0) / np.size(movie)
            print("%.2f%% of pixels will be clipped" % persent_clipped)
            movie[movie < 0] = 0.

        # fix bleaching
        br = calculate_bleaching_rate( movie )
        for i in range(len(movie)):
            movie[i] /= br[i]

        # finding in-focus index slice
        in_focus_idx = find_infocus_slice_idx( movie )

        # if step is set, use it to cut the movie file
        movie, in_focus_idx = clipping_calibration_stack(
            movie, zrange=zrange, zstep=zstep, infocusIdx=in_focus_idx
        )

        in_focus = movie[in_focus_idx]

        # spot detection
        psfs, theta = spot_detection( roisize, in_focus, movie )

    elif file_ext == '.mat':
        print( f'Calibration PSFs will be loaded from .mat file\n{movie_name}' )

        mat = scipy.io.loadmat( movie_name )
        psfs = mat[ 'PSFs' ]
        in_focus_idx = ( len( psfs[0] ) - 1 ) // 2
        theta = [ (roisize / 2.0 - 0.5), (roisize / 2.0 - 0.5) ]

    else:
        raise Exception( 'Unknown movie file format. Only .tif and .mat files '
                         'can be used as movie files for calibration' )

    psfs = psfs.astype( np.float32 )

    # Write debug tiffs
    if _SAVE_CALIBR_BEADS:
        save_separate_beads_into_files( psfs, out_dir_name )

    # Subtract minimum value (as background?)
    for i in range(len(psfs)):
        psfs[i] -= np.min(psfs[i])

    # Scale to sum of central slice
    print("In focus slice", in_focus_idx)
    for i in range(len(psfs)):
        slice_sum = np.sum(psfs[i, in_focus_idx, :, :])
        psfs[i] /= slice_sum

    # Calculate shifts
    dx = theta[0] - (roisize / 2.0 - 0.5)
    dy = theta[1] - (roisize / 2.0 - 0.5)
    shifts, dx, dy, shifted_psfs = estimate_calibration_beads_shifts(
                                                   psfs, dx, dy, verbose=False )
    # replacement of estimated calibration stack subpixel positions (shifst)
    # for test some stuff. Usually it used if we know the subpixel positions
    # (for example, from simulations)
    if shifts_replacement is not None:
        shifts = shifts_replacement

    print( '\nEstimated shifts:' )
    print( shifts )

    # save all necessary info in .mat file
    z_range_2x = 2 * zrange * 1e-3

    calib_res = BsplineCalibrationStackResult(
        roisize=roisize,
        n_z_slices=len(psfs[0]),
        z_range=z_range_2x,
        stacks=np.rollaxis(psfs, 1, 4),
        shifts=shifts,
        dx=dx, dy=dy, dz=shifts[0, 2],
        PSF_average=np.rollaxis(shifted_psfs, 0, 3)
    )

    # save results to .mat file
    if len( mat_outfile ) > 0:
        calib_res.to_mat_file( mat_outfile )

    # Write avg stack to tiff
    if _SAVE_CALIBR_BEADS:
        with tifffile.TiffWriter( os.path.join( out_dir_name, "stack_avg.tif" ) ) \
                as tiff:
            for i in shifted_psfs:
                tiff.save((1e5 * i).astype(np.int32))

    return calib_res

############################## Gaussian 3D #####################################

def run_frame(inp):
    frame, positions, z = inp
    roi_size = 14  # TODO: Turn into input

    sigmas = []
    rois = get_rois(frame, positions, roi_size=roi_size)
    for roi in rois:
        r = fit_sigma_3d(roi)
        if min(r[:2]) > 2 and max(r[:2]) < (roi_size - 2):
            sigmas.append((z, r[Params3D.SIGMA_X], r[Params3D.SIGMA_Y]))

    return sigmas


def calibrate_gaussian_3D(movie, output, args):
    z_range = np.linspace(-args.zrange * 1e-3, args.zrange * 1e-3, num=len(movie))
    in_focus = movie[int(len(movie) / 2)]

    positions = get_peak_positions(in_focus)
    inputs = []

    # shifts = []
    # with SMLM(debugMode=False) as smlm, Context(smlm) as ctx:
    #     gaussian = Gaussian(ctx)
    #     psf = gaussian.CreatePSF_XYIBgSigmaXY(roisize, (2, 2), False)

    #     for p in psfs:
    #         theta, _, tr = psf.ComputeMLE(1e5 * p)
    #         ss = theta[:, -2] + theta[:, -1]
    #         ss[theta[:, 0] < 3] = np.inf
    #         ss[theta[:, 0] > roisize - 2] = np.inf
    #         ss[theta[:, 1] < 3] = np.inf
    #         ss[theta[:, 1] > roisize - 2] = np.inf

    #         ii = np.argmin(ss)
    #         t = theta[ii]
    #         shifts.append([t[0] - (roisize / 2 - 0.5),
    #                        t[1] - (roisize / 2 - 0.5),
    #                        ii - len(p) / 2])

    for i, z in enumerate(z_range):
        inputs.append((movie[i], positions, z))

    with Pool() as p:
        results = p.map(run_frame, inputs)

    zs = []
    sigma_xs = []
    sigma_ys = []
    for r in results:
        for z, sigma_x, sigma_y in r:
            zs.append(z)
            sigma_xs.append(sigma_x)
            sigma_ys.append(sigma_y)

    def f(p, z):
        s0, gamma, d, A = p
        return s0 * np.sqrt(1 + (z - gamma) ** 2 / d ** 2 + A * (z - gamma) ** 3 / d ** 2)

    def func(p, z, y):
        return f(p, z) - y

    bounds = ((0.1, -600, 0, 1e-6), (10, 600, 1e3, 1e-1))
    p0 = [2, 0, 3e2, 1e-5]
    p_x = least_squares(func, p0, loss="huber", bounds=bounds, args=(zs, sigma_xs))
    p_y = least_squares(func, p0, loss="huber", bounds=bounds, args=(zs, sigma_ys))

    calibration = {"x": p_x.x, "y": p_y.x}

    print(positions)
    print(calibration)
    np.save(output, calibration)

    # plt.plot(zs, sigma_xs, 'x', label='\sigma_x')
    # plt.plot(zs, sigma_ys, 'x', label='\sigma_y')
    # plt.plot(zs, f(p_x.x, zs), '--', label='x fit')
    # plt.plot(zs, f(p_y.x, zs), '--', label='y fit')
    # plt.ylim([0, max(np.max(sigma_xs), np.max(sigma_ys))])
    # plt.legend()
    # plt.show()

################################ main ##########################################

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process TIFF file")

    parser.add_argument(
        "-i", "--input", required=True,
        help="Input TIFF file (.tif)")

    parser.add_argument(
        "-o", "--output", required=True,
        help="Calibration output file")

    parser.add_argument(
        "--emgain", required=True, type=float,
        help="EM gain" )

    parser.add_argument(
        "--offset", default=0, type=int,
        help="Offset" )

    parser.add_argument(
        "--conversion", default=1, type=float,
        help='Conversion camera factor')

    parser.add_argument(
        "--roi", required=True, type=int,
        help="ROI size in pixels")

    parser.add_argument(
        "--method", default="bspline",
        help="Proccessing method")

    parser.add_argument(
        "--step", default=-1, type=int,
        help="Z step of stack in nm. If < 0, it will not be considered as set."
             "if set, only z slices from -zrange/step to +zrange/step"
             "from focus will be processed")

    parser.add_argument(
        "--zrange", type=int, required=True,
        help="Z range in nm. Goes from -range to +range")

    args = parser.parse_args()

    # Print info to user
    print( 'infile =', args.input )
    print( 'outfile =', args.output, '\n' )
    print( 'Camera parameters:' )
    print( 'EmGain =', args.emgain )
    print( 'offset =', args.offset )
    print( 'conversion factor =', args.conversion, '\n' )
    print( 'Calibration stack parameters:' )
    print( 'method =', args.method )
    print( 'ROI =', args.roi, 'x', args.roi )
    if args.step > 0:
        print( 'z step =', args.step, ' nm' )
    print( 'zrange = -', args.zrange, ' nm', '..+', args.zrange, ' nm', sep='' )
    print( '' )

    if args.method == "gaussian3d":
        movie = load_movie(args.input)
        calibrate_gaussian_3D(movie, args.output, args)
    elif args.method == "cspline":
        calibrate_c_spline(args.input, args.output, args)
    elif args.method == "bspline":
        calibrate_b_spline(movie_name=args.input,
                           mat_outfile=args.output,
                           roisize=args.roi,
                           zrange=args.zrange,
                           zstep=args.step,
                           offset=args.offset,
                           emGain=args.emgain,
                           conversion=args.conversion)

    print( 'Calibration stage is finished!' )
