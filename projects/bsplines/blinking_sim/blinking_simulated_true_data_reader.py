import numpy as np


class BlinkingSimTrueDataReader:
    """Class for saving and loading true data after the simulation

    Attributes:
        real_positions (2D float array): [bead idx][axis idx: 0->x, 1->y, 2->z]
            real position of the beads in the global coordinate system.
            In this system in-focal plane is in z=0, the field of view
            corner is in x=0,y=0 and only x>=0, y>=0 can be inside the FOV.

            ^ Y
            |
            |   FOV
            |
            ---------> X

        corner_indices (2D int array): [bead idx][0->row, 1->column]
            index of every bead ROI corner pixel for placing in the movie
            array and then into the .tif movie file.

            ------------> column
            |
            |
            |
            v row

        subpixel_shifts (2D float array): [bead idx][0->row, 1->column]
                  |
                  |
            ------|-----> column
                  |
                  v row

            shift (0,0) means that a spot located right in the center of ROI.

        intensities (1D float array): [bead idx]
            intensity for every spot.

        background (float): background level for every spot.
    """

    def __init__( self, real_positions=None, corner_indices=None,
                  subpixel_shifts=None,
                  intensities=None, background:float=None ):

        self.real_positions = real_positions
        self.corner_indices = corner_indices
        self.subpixel_shifts = subpixel_shifts
        self.intensities = intensities
        self.background = background

    def check_parameters( self ):
        if self.real_positions is None:
            raise Exception( 'real_positions field should be set' )
        if self.corner_indices is None:
            raise Exception( 'corner_indices field should be set' )
        if self.subpixel_shifts is None:
            raise Exception( 'subpixel_shifts field should be set' )
        if self.intensities is None:
            raise Exception( 'intensities field should be set' )
        if self.background is None:
            raise Exception( 'background field should be set' )

    def to_file( self,  filename:str ):
        self.check_parameters()

        f = open( filename, 'w+' )
        f.write( 'idx\t'
                 'x\ty\tz\t'
                 'array_idx_0\tarray_idx_1\t'
                 'shift_x\tshift_y\t'
                 'intensity\tbackground\n' )

        i = 0
        for crd, inds, t_shifts, intens \
                in zip( self.real_positions, self.corner_indices,
                        self.subpixel_shifts, self.intensities ):
            f.write( f'{i}\t{crd[0]}\t{crd[1]}\t{crd[2]}\t'
                     f'{inds[0]}\t{inds[1]}\t'
                     f'{t_shifts[1]}\t{t_shifts[0]}\t'
                     f'{intens}\t{self.background}\n')
            i += 1

        f.close()

    @classmethod
    def from_file( cls, filename:str ):
        f = open( filename, 'r' )
        content = f.readlines()

        # first line with columns' names
        colnames = content[0].split()

        # read all like strings
        real_data = []
        for line in content[1:]:
            real_data.append( line.split() )
        real_data = np.array( real_data )
        Nspots = len( real_data )

        # check that background is the same for all points
        bgs = real_data[:, 9].astype( float )
        bg = np.average( bgs )
        if np.sum( np.abs( bgs - bg ) ) != 0.0:
            raise Exception( 'Error! background level should be the same for'
                             'all points, but it is different for '
                             'some of them' )

        # shifts. need to change axes
        shifts = np.zeros( (Nspots, 2), dtype=float )
        shifts[:, 0] = real_data[:, 7].astype( float )
        shifts[:, 1] = real_data[:, 6].astype( float )

        return BlinkingSimTrueDataReader(
            real_positions=real_data[:, 1:4].astype( float ),
            corner_indices=real_data[:, 4:6].astype( int ),
            subpixel_shifts=shifts,
            intensities=real_data[:, 8].astype( float ),
            background=bg
        )
