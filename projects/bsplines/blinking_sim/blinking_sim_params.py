
class BlinkingSimulationParameters:
    """Parameters for generating a blinking movie simulation"""

    def __init__( self ):
        # aberration parameters
        #     https://en.wikipedia.org/wiki/Zernike_polynomials
        #     find Noll indices
        self.noll_aberration_index = -1
        # amplitude for chosen Zernike polynomial. 1.0 is OK
        self.aberration_amplitude = 0.0

        # general PSF generator parameters
        #    mu = PSF * intensity + bg
        #    with applied Poisson noise on it.
        self.intensity = 0.0
        #    for real data intensity of every bead can vary. For this simulation
        #    normal distribution of intensities around one value is used
        self.sigma_intensity = 0.0
        self.background = 0.0
        self.use_poisson_noise = True

        # more specific generator PSF parameters
        #    Number of elements for one axis in Fourier domain for FFT
        self.fft_resolution = -1
        self.lambda_um = 0.6
        #    refractive index of immersion fluid
        self.ref_imm_fluid = 1.5
        #    refractive index of the sample
        self.ref_sample = 1.33
        self.NA = 1.49

        # for generating PSF in the blinking movie
        #    number of blinking beads which will be used for the movie
        self.Nspots = 0
        #    point coordinates generators. Usually it is something that
        #    uniformly distributes points on some surface or line
        self.p_generators = []
        #    number of beads per points generator
        self.Nspots_on_gen = [] # sum should be equal to self.Nspots
        #    number of pixels for one axis for simulating one PSF in the movie
        #    Also used as the main ROI size for PSF generator.
        #    Should be bigger or equal than ROI for calibration PSF stacks.
        self.ROI_sim = 0
        #    size of one pixel in micrometers
        self.pixelsize = 0.0 # um

        # the movie parameters
        #    number of frames in the output movie .tif file
        self.Nframes = 0
        #    number of pixels in the movie for X axis
        self.FOV_pixels_x = 0
        #    number of pixels in the movie for Y axis
        self.FOV_pixels_y = 0

        # PSF calibration stacks' parameters
        #    Simulated PSF calibration stack is changing in [-z_range, +z_range]
        self.z_range = 0.0 # in micrometers
        #    Number of z slices (layers) for one PSF calibration stack
        self.n_z_slices = 0
        #    Number of calibration beads which will be generated
        self.Ncalibration_beads = 0
        #    Number of pixels for X,Y axes for one PSF in output calibration
        #    stack file. The generation will be performed for the full
        #    ROI_sim and then the calibration PSF will be cropped to this value
        self.ROI_calibration = 0
        #    Subpixel shifts in pixels (for z in z-slices) for generated PSFs
        #    will be chosen from the interval
        #    [-random_range_for_shift, +random_range_for_shift]
        #    For every axis it is different. Should be less than 0.5
        self.random_range_for_shift = [0.0, 0.0, 0.0] # in pixels

        # blinking events' parameters
        #    average number of molecules in one frame
        #    (it is not true, but pretend that it is true)
        self.average_on_time_life = 1.0
        #    (1-this) means chance of the molecule to go to off state.
        self.blinks_on_fraction = 0.0005
