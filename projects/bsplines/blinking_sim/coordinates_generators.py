"""
File contains different types of coordinates generators for N points in 3D space.
Usually, this generators are used for simulations.
"""
import numpy as np
import math


class CoordinatesGenerator:
    """Abstract class for coordinate generators"""

    def generate_coordinates( self, N:int ):
        """Generates N points for chosen distribution class"""

        pass


class SphereGenerator(CoordinatesGenerator):
    """Generates uniformly distributed 3D coordinates of molecules on sphere.

    Attributes:
        radius (float): radius of the sphere

        shift_vec (1D float array with 3 elements): shift for the center of the
            sphere
    """

    def __init__( self, radius:float, shift_vec=[0.0, 0.0, 0.0] ):
        if radius < 0:
            raise Exception('Radius for sphere generator should be >= 0')

        self.R = radius
        self.shift_vec = shift_vec

    def generate_coordinates( self, N:int ):
        # generate uniformly distributed angles for spherical coordinates
        # with fixed radius
        phi_arr = np.random.uniform( 0.0, 2.0 * np.pi, size=N )
        theta_arr = np.random.uniform( 0.0, np.pi, size=N )

        sin_theta_arr = np.sin( theta_arr )

        coordinates = np.zeros( shape=(N,3), dtype=np.float32 )

        # generated x coordinate
        coordinates[:,0] = self.R * sin_theta_arr * np.cos( phi_arr ) + \
                           self.shift_vec[0]
        # generated y coordinate
        coordinates[:,1] = self.R * sin_theta_arr * np.sin( phi_arr ) + \
                           self.shift_vec[1]
        # generated z coordinate
        coordinates[:,2] = self.R * np.cos( theta_arr ) + \
                           self.shift_vec[2]

        return coordinates


class FlattenedSphereGenerator( SphereGenerator ):
    """Generated uniformly distributed points on flattened sphere surface

    Attributes:
        radius (float): radius of the sphere

        shift_vec (1D float array with 3 elements): shift for the center of the
            sphere

        z_coef (float): coefficient for flattening the sphere in z direction.
            If this coefficient is equal to 1.0, then this will be a usual
            sphere surface.
    """

    def __init__( self, radius:float, shift_vec=[0.0, 0.0, 0.0],
                  z_coef:float = 1.0 ):
        SphereGenerator.__init__( self, radius, shift_vec )
        self.z_coef = z_coef

    def generate_coordinates( self, N:int ):
        coord = SphereGenerator.generate_coordinates( self, N )
        coord[:, 2] *= self.z_coef

        return coord


class StraightFunctionGenerator(CoordinatesGenerator):
    """Generate points uniformly distributed on the line and apply function.

    Attributes:
        start_p: [3 float numbers] coordinate of the start point in 3D space

        direction: [3 float numbers] vector from the start point to the end
            point of the line. The length of direction vector is equal
            to the distance between the start and the end points.

        func: [function] parametric function that receive 1 float number (t) as
            an argument and produces 2 float (x,y) as an output.
            It represents the function in the plane which is orthogonal
            to the line.
            t (argument) used as a parameter.

            example of func:
                 def spiral( t ):
                    x = 0.3 * math.sin( 25.2 * t )
                    y = 0.3 * math.cos( 25.2 * t )
                    return x, y

                 func = spiral

               ^ z         |\
               |           | \ plane orthogonal
                           |  \ to the line
                           |   \
                 e_x_plane |^ ^ \ e_y_plane
                           | \|  |
            *--------------|--*--|------------------------------------->*
            start_p        |     |          direction                 end point
                           \     |
                            \    |
                             \   |
                              \  |
                               \ |
                                \|

        e_x_plane: [3 float numbers] unit vector in the orthogonal plane to
            the line.
            e_x_plane = [e_z x direction] / | [e_z x direction] |

        e_y_plane: [3 float numbers] unit vector in the orthogonal plane to
            the line.
            e_y_plane = [e_x_plane x direction] / | [e_x_plane x direction] |

        if e_x_plane is 0 (not defined) then it will be defined as
        unit vector in usual cartesian coordinates (1,0,0).
    """

    def _init_plane_basis_vectors( self ):
        """Calculates basis vectors for the function plane.

        e_x vector in the plane is calculated like
        e_x_plane = [e_z x direction] / | [e_z x direction] |
        where e_z = ( 0, 0, 1 )

        e_y vector in the plane is calculated like
        e_y_plane = [e_x_plane x direction] / | [e_x_plane x direction] |

        If the direction of the straight line has only z coordinate,
        then e_x_plane and e_y_plane will be equal to e_x, e_y of the global
        coordinates ( (1,0,0), (0,1,0) ).
        """

        dir_x = self.direction[0]
        dir_y = self.direction[1]
        dir_z = self.direction[2]

        if dir_x == 0 and dir_y == 0:
            self.e_x_plane = np.array( [1.0, 0.0, 0.0], dtype=float )
            self.e_y_plane = np.array( [0.0, 1.0, 0.0], dtype=float )
        else:
            # calculate e_x_plane and normalize
            self.e_x_plane = np.array(
                [-dir_y, dir_x, 0.0], dtype=float
            )

            self.e_x_plane /= math.sqrt( self.e_x_plane[0]**2 + \
                                         self.e_x_plane[1]**2 )

            # calculate e_y_plane and normalize
            self.e_y_plane = np.array( [dir_x * dir_z,
                                        dir_y * dir_z,
                                        dir_x**2 + dir_y**2],
                                       dtype=float )

            self.e_y_plane /= math.sqrt( self.e_y_plane[0]**2 + \
                                         self.e_y_plane[1]**2 + \
                                         self.e_y_plane[2]**2 )


    def __init__(self, start_point, end_point, function_on_straight):
        self.func = function_on_straight
        self.start_p = np.array( start_point, dtype=float )

        direction = np.array( [end_point[0] - start_point[0],
                               end_point[1] - start_point[1],
                               end_point[2] - start_point[2]],
                              dtype=float )

        L = math.sqrt( direction[0]**2 + direction[1]**2 + direction[2]**2 )
        if L <= 0:
            raise Exception( "Wrong start and end points for "
                             "StraightFunctionGenerator.\n"
                             "Line segment length should be more than 0 "
                             "between end and start points.\n"
                             f"start point = {start_point}\n"
                             f"end point = {end_point}" )

        try:
            xs_test, ys_test = self.func( 0.0 )
        except:
            print( "function_on_straight argument in StraightFunctionGenerator "
                   "should have one float argument and should return 2 float "
                   "numbers (x,y in the plane which is orthogonal to the "
                   "straight direction vector)" )
            raise Exception

        self.direction = np.array( direction, dtype=float )

        self._init_plane_basis_vectors()


    def generate_coordinates( self, N:int ):
        coordinates = np.zeros( shape=(N, 3), dtype=float )

        params = np.random.uniform( 0.0, 1.0, size=N )

        func_xy = np.zeros( (N, 2), dtype=float )

        for i in range( N ):
            res = self.func( params[i] )
            func_xy[i, 0] = res[0]
            func_xy[i, 1] = res[1]

        coordinates[:] = self.start_p
        coordinates[:] += params[:].reshape( N, 1 ) * self.direction + \
                          func_xy[:,0].reshape( N, 1 ) * self.e_x_plane + \
                          func_xy[:,1].reshape( N, 1 ) * self.e_y_plane

        return np.array( coordinates, dtype=np.float32 )


class SpiralFunctionGenerator( StraightFunctionGenerator ):
    """Generate points uniformly distributed on a spiral"""

    def __init__( self, start_point, end_point,
                  amplitude, w_freq:float, phase:float=0.0 ):

        if hasattr( amplitude, "__len__" ):
            A_x = amplitude[0]
            A_y = amplitude[1]
        else:
            A_x = amplitude
            A_y = amplitude

        def spiral( t ):
            x = A_x * math.sin( w_freq * t + phase )
            y = A_y * math.cos( w_freq * t + phase )
            return x, y

        super( self.__class__, self ).__init__(
            start_point=start_point,
            end_point=end_point,
            function_on_straight=spiral
        )


class LineUniformGenerator( StraightFunctionGenerator ):
    """Generate points uniformly on a line"""

    def __init__( self, start_point, end_point ):
        def line( t ):
            return 0.0, 0.0

        super( self.__class__, self ).__init__(
            start_point=start_point,
            end_point=end_point,
            function_on_straight=line
        )
