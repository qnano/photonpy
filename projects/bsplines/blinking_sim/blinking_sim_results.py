import os.path
import tifffile
import numpy as np
import pptk
import scipy.io
from bsplines.blinking_sim.blinking_simulated_true_data_reader import BlinkingSimTrueDataReader
from bsplines.bsp_utils.aux_functions import cropping_spots_array, save_PSFs_separately


class BlinkingSimulationResults:
    """Results of the blinking simulation. Can save data in different ways

    Attributes:
        movie (3D float array): [frame idx][row pixel idx][col pixel idx]
            Value of pixels in the simulated movie file. This file
            contains frames with blinking molecules.
        PSFs_calibr (4D float array): [bead idx][z index][row idx][col idx]
            Value of pixels in the simulated calibration PSF stacks.
            For every PSF the whole z-stack is generated.
        real_positions (2D float array): [spot idx][axis: 0->x, 1->y, 2->z]
            Generated coordinates for every point in usual cartesian
            coordinate system.
        subpixel_shifts (2D float array): [bead idx][axis: 0->x, 1->y]
            subpixel shifts for generated points' positions and for
            chosen pixelsize.
        corner_indices (2D int array): [bead idx][0->rows, 1->columns]
            corner indices for placing movie PSFs into the movie field of view.
            It is transfered to .tif file coordinate system.
        intensities (1D float array): [bead idx]
            intensity of every spot.
        background (float): background level.
        movie_PSFs_normalized (3D float array):
            [spot idx][row pixel idx][col pixel idx]
            Normalized generated PSF z-slice for every spot in order to
            modify it like Poisson( PSF*I + bg ) and place into the movie.
            Have the same coordinate system as .tif for easier placement.
    """

    def __init__( self,
                  movie=None,
                  PSFs_calibr=None,
                  real_positions=None,
                  subpixel_shifts=None,
                  corner_indices=None,
                  intensities=None,
                  background:float=None,
                  movie_PSFs_normalized=None ):

        self.movie = movie
        self.PSFs_calibr = PSFs_calibr
        self.real_positions = real_positions
        self.subpixel_shifts = subpixel_shifts
        self.corner_indices = corner_indices
        self.intensities = intensities
        self.background = background
        self.movie_PSFs_normalized = movie_PSFs_normalized

    def save_real_positions( self, filename:str ):
        """Saving simulation real data in the text file"""

        data = BlinkingSimTrueDataReader(
            real_positions=self.real_positions,
            corner_indices=self.corner_indices,
            subpixel_shifts=self.subpixel_shifts,
            intensities=self.intensities,
            background=self.background
        )
        data.to_file( filename )

    def save_calibration_PSFs( self, filename:str ):
        """Save calibration PSFs in a file"""

        if self.PSFs_calibr is None:
            raise Exception( 'Calibration PSF stacks should be set for '
                             'saving them into .mat file' )

        # \todo: create a class for simulation results
        store = {
            'PSFs': self.PSFs_calibr
        }

        scipy.io.savemat( filename, store )

    def save_calibration_PSFs_separately( self, folder_name:str,
                                          base_name='stack' ):
        """Saves beads from 4D numpy array to separate files"""

        save_PSFs_separately( self.PSFs_calibr, folder_name, base_name )

    def save_movie_spots_separately( self, filename:str,
                                     intensities, bg:float, use_poisson:bool,
                                     ROI_to_save:int=-1 ):
        """Save spots separately into .tif file

        This structure already has normalized PSF z-slice for every spot.
        So for saving separated spots we need to apply intensity, background
        and Poisson noise to them.

        :param filename: (str) filename.

        :param intensities: (1D float array) [spot idx] intensity for every
            spot that will be used for creating the separate spots .tif file.

        :param bg: (float) background.

        :param use_poisson: (bool) if true, Poisson noise also will be used
            for producing a PSF spot.

        :param ROI_to_save: (int) size in pixels of spots to save.
        """

        if self.movie_PSFs_normalized is None:
            raise Exception( 'The movie\'s normalized PSfs should be set in '
                             'order to print them separately into .tif file' )

        # use intensity, bg and Poisson for normalized PSFs
        if intensities is None:
            PSFs_sep = self.movie_PSFs_normalized + bg
        else:
            PSFs_sep = self.movie_PSFs_normalized * intensities[:, None, None]+\
                       bg
        if use_poisson:
            PSFs_sep = np.random.poisson( PSFs_sep )

        # cropping ROI
        if ROI_to_save > 0:
            PSFs_sep = cropping_spots_array( PSFs_sep, ROI_to_save )

        PSFs_sep = PSFs_sep.astype( np.float32 )

        # save to the file
        with tifffile.TiffWriter( filename ) as tif:
            tif.save( PSFs_sep, compress=2 )

    def save_movie( self, filename:str ):
        """Save the blinking sumulation movie file"""

        if self.movie is None:
            raise Exception( 'For saving a movie file you need a movie data' )

        with tifffile.TiffWriter( filename ) as tif:
            tif.save( self.movie, compress=2 )

    def save_movie_summation( self, filename:str ):
        """Save summed movie (blurred real movie if we sum all blinking slides)"""

        summed_movie = self.movie.sum( axis=0 )
        with tifffile.TiffWriter( filename ) as tif:
            tif.save( summed_movie, compress=2 )

    def launch_3D_viewer( self ):
        """Launch PPTK 3D viewer for current real positions"""

        if self.real_positions is None:
            raise Exception( 'Real positions shouldn\'t be None for launching '
                             '3D viewer' )

        v = pptk.viewer( self.real_positions )
        v.attributes( self.real_positions )
        v.set( point_size=.01 )
