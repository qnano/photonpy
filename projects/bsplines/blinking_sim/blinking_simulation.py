import numpy as np
import time

from bsplines.blinking_sim.blinking_sim_executor import BlinkingSimulationExecutor
from bsplines.blinking_sim.blinking_sim_params import BlinkingSimulationParameters
import bsplines.blinking_sim.coordinates_generators as coordinates_generators


# if True, the special .tif file will be saved with every movie spot with their
# x,y shift and z position as one slice inside the .tif file
_SAVE_SPOTS_SEPARATELY = True
# ROI size for saving separate spots
_SEPARATE_SPOTS_ROI = 11
# if True, every calibration simulated PSF will be saved in stack_idx.tif file
_SAVE_CALIBRATION_PSF_INTO_SEPARATE_TIF_FILES = True
# if True, the sum of all slices in the movie file will be saved as
# 2D image (.tif)
_SAVE_SUMMED_MOVIE_FILE = True


def initialize_blinking_sim_params():
    sp = BlinkingSimulationParameters()

    # aberration parameters
    sp.noll_aberration_index = 6
    sp.aberration_amplitude = 1.0

    # general PSF generator parameters
    sp.intensity = 1e5
    sp.sigma_intensity = 0.0
    sp.background = 700.0 # 20.0
    sp.use_poisson_noise = True #True

    # more specific generator PSF parameters
    sp.fft_resolution = -1
    sp.lambda_um = 0.6
    sp.ref_imm_fluid = 1.5
    sp.ref_sample = 1.33
    sp.NA = 1.49

    # for generating PSF in the blinking movie
    sp.Nspots = 1000

    sp.Nspots_on_gen.append( sp.Nspots // 2 )
    sp.p_generators.append(
        coordinates_generators.SpiralFunctionGenerator(
            start_point=[0.5, 0.5, 0.0],
            end_point=[10.5, 10.5, 0.0],
            amplitude=0.3,
            w_freq=25.132741228718345,
            phase=0.0
        )
    )

    sp.Nspots_on_gen.append( sp.Nspots - sp.Nspots_on_gen[0] )
    sp.p_generators.append(
        coordinates_generators.LineUniformGenerator(
            start_point=[0.5, 10.5, 0.0],
            end_point=[5.5, 0.5, 0.0]
        )
    )

    # sp.Nspots_on_gen( Nsphere )
    # sp.p_generators.append(
    #     coordinates_generators.FlattenedSphereGenerator(
    #         radius=4.5,
    #         shift_vec=[0.1 * 120 / 2.0],
    #         z_coef=0.09
    #     )
    # )

    sp.ROI_sim = 33 # 33
    sp.pixelsize = 0.1

    # the movie parameters
    sp.Nframes = 40000
    sp.FOV_pixels_x = 120
    sp.FOV_pixels_y = 120

    # PSF calibration stacks' parameters
    sp.z_range = 0.6 # in micrometers [-z_range, +z_range]
    sp.n_z_slices = 151

    sp.Ncalibration_beads = 6
    sp.ROI_calibration = 13
    sp.random_range_for_shift = [0.4, 0.4, 0.08]
    #sp.random_range_for_shift = [0.0, 0.0, 0.0]

    # blinking events' parameters
    sp.average_on_time_life = 1.0
    sp.blinks_on_fraction = 0.0005

    return sp


if __name__ == '__main__':
    time_start_main = time.time()

    # OUTPUT FILES parameters
    folder_name = '../../../../OUTPUT/'
    movie_filename = folder_name + 'simulation_movie.tif'
    PSFcalib_filename = folder_name + 'simulation_calibration_PSFs.mat'
    real_pos_filename = folder_name + 'simulation_real_positions.txt'
    summed_movie_file = folder_name + 'simulation_movie_summed.tif'
    movie_sep_spots_file = folder_name + 'movie_simulated_separate_spots.tif'

    # initialize parameters
    sim_prms = initialize_blinking_sim_params()
    np.random.seed( 0 )

    # do simulation
    ex = BlinkingSimulationExecutor( sim_prms )
    time_initialization = time.time() - time_start_main
    ex_res, ex_times = ex.create_movie()

    # save the results in files
    time_saving_start = time.time()
    print( f'Saving real points positions in file\n\t{real_pos_filename}' )
    ex_res.save_real_positions( real_pos_filename )

    print( f'Saving calibration PSFs in file\n\t{PSFcalib_filename}\n' )
    ex_res.save_calibration_PSFs( PSFcalib_filename )

    if _SAVE_CALIBRATION_PSF_INTO_SEPARATE_TIF_FILES:
        print( f'Saving calibration PSFs into separate .tif files...\n' )
        ex_res.save_calibration_PSFs_separately( folder_name=folder_name,
                                                 base_name='stack' )

    if _SAVE_SPOTS_SEPARATELY:
        print( f'Saving spots separately in '
               f'the file...\n\t{movie_sep_spots_file}' )

        ex_res.save_movie_spots_separately(
            filename=movie_sep_spots_file,
            intensities=ex.intensities,
            bg=sim_prms.background,
            use_poisson=sim_prms.use_poisson_noise,
            ROI_to_save=_SEPARATE_SPOTS_ROI
        )

    print( f'Saving movie file in\n\t{movie_filename}' )
    ex_res.save_movie( movie_filename )

    if _SAVE_SUMMED_MOVIE_FILE:
        print( f'Saving summed blurred movie in\n\t{summed_movie_file}' )

        ex_res.save_movie_summation( summed_movie_file )

    time_saving = time.time() - time_saving_start

    print( '\nTIMES:' )
    print( f'Initialization time = {time_initialization:.2f} sec\n' )
    print( ex_times )
    print( f'saving files time = {time_saving:.2f} sec' )
    sum_time = time_initialization + ex_times.sum() + time_saving
    print( f'TOTAL TIME = {sum_time:.2f} sec' )

    time_main = time.time() - time_start_main
    print( f'TOTAL TIME (because of some losses) = {time_main:.2f} sec\n' )

    print( 'DONE!' )
