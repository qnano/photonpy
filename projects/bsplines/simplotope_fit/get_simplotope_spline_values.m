% The simplotopes spline is a curve which is defined in some area.
% However, because it is a spline, it has values in the whole area 
% everywhere (continuous function).
% This function calculates the values of spline for all points defined
% in the input array.
%
% Args:
%   coordinates (2D float array (point idx, dim idx) ) - array with points
%       coordinates in which the user wants to calculate the spline values.
%       If this array is 1D, it will be reshaped automatically to 2D.
%       dim should be max the same as sum( Spline.nu ) - total spline 
%       dimension.
%
%   simplotope_spline_mat_file (str) - name of .mat file with Spline object
%       and Simplotopes array. This fields should be obtained before by
%       some spline fit function
%       (for example, by DistributedSimplotopeEstimatorForPSF() function).
%
% Returns:
%   values (1D float arr) - the spline values in the coordinates points.
%
function [values] = get_simplotope_spline_values( ...
    coordinates, ...
    simplotope_spline_mat_file ...
)
    %% Add paths
    folder_path = get_folder_path();
    spline_mat_folder = folder_path + ...
        "../../../../3rd_party/Simplotopes_code/distributed-splines/";
    addpath( spline_mat_folder );

    addpath( spline_mat_folder + "./HelperFunctions" );
    addpath( spline_mat_folder + "./HelperFunctions/MsplToolbox" );
    addpath( spline_mat_folder + "./Dataset" );
    addpath( spline_mat_folder + "../Cpp Source/cbsplinen_simplotope" );
    addpath( spline_mat_folder + "../Cpp Source/ADMM_Iterative/ADMM_Iterative" );
    
    %% Read .mat file
    smpl_file = load( simplotope_spline_mat_file );
    
    % total dimension number
    nDim = sum( smpl_file.Spline.nu );
    
    %% Reshape back
    if size( coordinates, 1 ) == 1
        N = size( coordinates, 2 );
        coordinates = reshape( coordinates, [nDim, N / nDim] );
        coordinates = coordinates';
    end
    
    %% Do the approximation
    values = GetSplineValues( coordinates, ...
                              smpl_file.Spline, smpl_file.Simplotopes );
    
end

% Returns folder name of the folder with this MATLAB script
% Very useful for 'addpath()' functions, because then this script can be
% launched from any folder.
function folder_path = get_folder_path()
    s = mfilename('fullpath');
    
    for i=length(s):-1:1
        if s(i) == '/' || s(i) == '\'
            folder_path = extractBetween( s, 1, i );
            break;
        end
    end
    folder_path = folder_path{1};
    
    fprintf( '\nMATLAB script path:\n%s\n\n', folder_path );
end
