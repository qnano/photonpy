"""
Class for launching Simplotope fitting stage in MATLAB from python.
"""

import os                # for adding path with this script to MATLAB path
import matlab.engine     # launch MATLAB script from python
import argparse          # parse arguments
import scipy.io          # load .mat file
import numpy as np


class SimplotopeFitter:
    """Do Simplotope fitting for several calibration PSF pixel stacks

    http://resolver.tudelft.nl/uuid:df32613c-5f3c-484e-84ea-99672939d6a5

    Produce B-coefficients for PSF model using PSf calibration stacks as
    data points. One pixel == one data point. Because of different subpixel
    shifts, pixels in PSF calibration stacks are slightly moved according to
    each over => more points for B-coefficients estimation.

    Input .mat file should contain:

    z_range (float) - full z range for one PSF stack. PSF changes in
                      [-z_range/2, +z_range/2]
    stacks (4D float array) - [bead idx, x index, y index, z index]
        PSF stacks with values in every pixel
    shifts (2D float array) - [bead idx][axis idx: 0->x, 1->y, 2->z]
        subpixel positions according to the center of the ROI for every
        PSF calibration stack.

    Other parameters are unnecessary for B-spline fitter.

    Attributes:
        input_calibr_PSFs_file (str) - name of .mat file with PSF calibration
            stacks parameters

        output_spline_file (str) - output .mat file name with 'Spline' object
            and 'Simplotopes' array.

        layers_dimension (int 1D arr) - array with dimension of every
            Simplotopes' layers.

        polynomial_degrees (int 1D arr) - B-spline polynomial degree of every
            layer. Bigger degree requires more input PSF calibration stacks
            with different subpixel positions.

        n_mesh_cubes (int 1D arr) - number of n-cubes of every dimension
            (size of the array is equal to sum(layers_dimension) ).
            One cube can contain several simplices.
            Bigger number of cubes requires more input PSF calibration stacks
            with different subpixel positions.

        continuity_orders (int 1D arr) - continuity order between simplices of
            every layer. Bigger continuity order requires less number
            of PSF calibration stacks.

        partitions (int 1D arr) - number of partitions in every layer.
            It can be used for reducing the computation time.

        use_matlab_async (bool) - if True, MATLAB simplotopes fitting stage will
            be launched asynchronously. method synchronize() should be used
            in order to wait the fitting stage results.

        future - future for MATLAB engine. Used for synchronization.

        eng - MATLAB engine. Used as a field in order to not be deleted
            before synchronization.

        input_coords (2D float array [point idx][axis idx: 0->x, 1->y, 2->z]) -
            array with input coordinates after subpixel shifts applied.
            This array is used as an input for MATLAB fitting function.

        input_values (1D float array [point idx]) - values of every point
            in input_coords array. Used as an input array for MATLAB
            fitting function
    """

    def __init__( self, input_calibr_PSFs_file: str,
                  output_spline_file: str,
                  layers_dimension,
                  polynomial_degrees,
                  n_mesh_cubes,
                  continuity_orders,
                  partitions,
                  use_matlab_async: bool=False ):

        self.input_calibr_PSFs_file = input_calibr_PSFs_file
        self.output_spline_file = output_spline_file
        self.layers_dims = layers_dimension
        self.poly_degrees = polynomial_degrees
        self.n_mesh_cubes = n_mesh_cubes
        self.continuity_orders = continuity_orders
        self.partitions = partitions

        self.use_matlab_async = use_matlab_async
        self.future = None
        self.eng = None

        self.folder_with_matlab_script = os.path.dirname(__file__) + \
            '/../../../../3rd_party/Simplotopes_code/distributed-splines'

        self.input_coords = None
        self.input_values = None

    def _preprocessing( self ):
        """Data preprocessing for MATLAB fitting stage

        Read .mat file and change position of data poins in PSF stacks
        according to subpixel shifts
        """

        # reading .mat file
        mat = scipy.io.loadmat( self.input_calibr_PSFs_file )

        PSFs = mat['stacks']
        shifts = mat['shifts']

        PSFs = np.array( PSFs )
        shifts = np.array( shifts )

        # number of calibration PSF stacks (beads)
        Nbeads = PSFs.shape[0]
        # number of pixels for X and Y axes
        Npix_x = PSFs.shape[1]
        Npix_y = PSFs.shape[2]

        nDim = len(PSFs.shape) - 1
        if nDim == 3:
            N_zslices = PSFs.shape[3]
            z_range = mat['z_range'][0][0]
        else:
            N_zslices = 1


        # total number of data points available
        Npts = np.prod( PSFs.shape )

        # X,Y coordinates in PIXELS
        x_crd = np.linspace( 1.0, Npix_x, Npix_x )
        y_crd = np.linspace( 1.0, Npix_y, Npix_y )

        # z coordinate in MICROMETERS
        if nDim == 3:
            z_crd = np.linspace( -z_range/2.0, z_range/2.0, N_zslices )

        # allocating memory for data points arrays
        input_coords = np.zeros( (Npts, nDim), dtype=float )
        input_values = np.zeros( Npts, dtype=float )

        # placing bead coordinates and values as data points in the arrays
        # with subpixel shifts as part of coordinates
        for ibead in range( Nbeads ):
            for ix in range( Npix_x ):
                x_shifted = x_crd[ix] - shifts[ibead, 0]

                for iy in range( Npix_y ):
                    y_shifted = y_crd[iy] - shifts[ibead, 1]

                    for iz in range( N_zslices ):
                        idx = iz + iy*N_zslices + ix*N_zslices*Npix_y + \
                              ibead*N_zslices*Npix_y*Npix_x

                        # add shifts. If you ask why minus - I don't know.
                        # I did it the same way as Willem did, so need to be
                        # checked.
                        input_coords[idx][0] = x_shifted
                        input_coords[idx][1] = y_shifted
                        if nDim == 3:
                            input_coords[idx][2] = z_crd[iz] + shifts[ibead, 2] * 0.001
                            input_values[idx] = PSFs[ibead, ix, iy, iz]
                        else:
                            input_values[idx] = PSFs[ibead, ix, iy]

        return input_coords, input_values

    def do_fitting( self ):
        """Launch Simplotopes fitting using MATLAB"""

        input_coords, input_values = self._preprocessing()
        self.input_coords = input_coords
        self.input_values = input_values

        self.eng = matlab.engine.start_matlab()
        self.eng.addpath( self.folder_with_matlab_script )

        reshaped_coord = input_coords.reshape(np.prod(input_coords.shape))

        input_coords_mat = matlab.double(
            list( reshaped_coord )
        )

        input_values_mat = matlab.double(
            list( input_values )
        )

        self.future = self.eng.DistributedSimplotopeEstimatorForPSF(
            input_coords_mat,
            input_values_mat,

            matlab.double( list(self.layers_dims) ),
            matlab.double( list(self.poly_degrees) ),
            matlab.double( list(self.continuity_orders) ),
            matlab.double( list(self.n_mesh_cubes) ),
            matlab.double( list(self.partitions) ),
            self.output_spline_file,

            nargout=0,
            background=self.use_matlab_async
        )

    def synchronize( self ):
        """Method which should be used if MATLAB is launched in async mode

        Waits MATLAB to finish Simplotopes fitting stage in order to produce
        .mat file with Spline object and Simplotopes array.
        """

        if self.use_matlab_async:
            check_that_matlab_is_over = self.future.done()

            if not check_that_matlab_is_over:
                print(f'\nWAITING MATLAB TO FINISH\n')

            self.future.result()


if __name__ == '__main__':
    sm_fitter = SimplotopeFitter(
        input_calibr_PSFs_file='../../../../OUTPUT/stacks.mat',
        output_spline_file='../../../../OUTPUT/smpl_fit.mat',
        layers_dimension=[2, 1],
        polynomial_degrees=[5, 5],
        n_mesh_cubes=[2, 2, 2],
        continuity_orders=[1, 1],
        partitions=[1, 1],
        use_matlab_async=False
    )

    sm_fitter.do_fitting()
    # for sync execution will do nothing
    sm_fitter.synchronize()

    print( 'DONE!' )
