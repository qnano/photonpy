"""
Class for getting values of the simplotope spline in several points.
"""

import os                # for adding path with this script to MATLAB path
import matlab.engine     # launch MATLAB script from python
import argparse          # parse arguments
import scipy.io          # load .mat file
import numpy as np


class SimplotopeValuesGetter:
    """Class launches MATLAB part of code to get values in several points

    The simplotopes spline is a curve which is defined in some area.
    However, because it is a spline, it has values in the whole area everywhere.
    This function calculates the values of spline for all points defined in the
    input array using MATLAB.

    spline_mat_file should contain Spline object and Simplotopes array
    (they can be defined using DistributedSimplotopeEstimatorForPSF() MATLAB
    function for a simplotopes spline fit).

    How to use:
        See if __name__ == '__main__' part of this script.

    Attributes:
        coordinates ([N, dim] float arr) - coordinates of points for which the
            user want to calculate the values of the simplotope spline.
            First index - point index, second index - dimension (x,y,z, etc.).

        spline_mat_file (str) - .mat file with Spline object and Simplotopes
            array obtained by simplotopes spline fit before.

        use_matlab_async (bool) - if True, MATLAB function will
            be launched asynchronously. method get_values() should be used
            in order to wait the fitting stage results.

        future - future for MATLAB engine. Used for synchronization.

        eng - MATLAB engine. Used as a field in order to not be deleted
            before synchronization.

        values (1D float arr) - output values field. Used as field in order to
            allow asynchronous MATLAB function execution.

        folder_with_matlab_script - folder for adding path to the MATLAB engine
            in order to receive an access to the MATLAB function for calculating
            the spline values.

        finished_exec (bool) - flag if the MATLAB execution is finished.
            Need for using get_values() method several times.
    """

    def __init__( self, coordinates, spline_mat_file: str,
                  use_matlab_async: bool = False ):
        self.coordinates = coordinates
        self.spline_mat_file = spline_mat_file

        self.use_matlab_async = use_matlab_async
        self.future = None
        self.eng = None

        self.values = None

        self.folder_with_matlab_script = os.path.dirname( __file__ )

        self.finished_exec = False

    def _postproccesing_for_values( self, vals ):
        """Reshaping values array to a normal 1D float numpy array"""

        self.values = np.array( vals, dtype=float )
        self.values = np.reshape( self.values, np.prod(self.values.shape) )

    def calculate_values( self ):
        """Launch MATLAB function for calculating the spline values in points"""

        # launch MATLAB engine
        self.eng = matlab.engine.start_matlab()
        self.eng.addpath(self.folder_with_matlab_script)

        # transform coordinates array for MATLAB
        coord = np.array( self.coordinates, dtype=float )

        coord_mat = matlab.double(
            list( coord.reshape( np.prod( coord.shape ) ) )
        )

        # launch MATLAB function
        res = self.eng.get_simplotope_spline_values(
            coord_mat,
            self.spline_mat_file,
            nargout=1,
            background=self.use_matlab_async
        )

        if self.use_matlab_async:
            self.future = res
        else:
            self.values = res

    def get_values( self ):
        """Method which should be used if MATLAB is launched in async mode

        Waits MATLAB to finish the getting values procedure.
        If it was used in sync mode, then just return values.

        In any case, this method will return values.
        """

        # if this method is called not for the first time
        if self.finished_exec:
            return self.values

        # if user hasn't called calculate function first
        if self.eng is None:
            self.calculate_values()

        if self.use_matlab_async:
            check_that_matlab_is_over = self.future.done()

            if not check_that_matlab_is_over:
                print(f'\nWAITING MATLAB TO FINISH\n')

            self._postproccesing_for_values( self.future.result() )
        else:
            self._postproccesing_for_values( self.values )

        self.finished_exec = True

        return self.values


if __name__ == '__main__':
    # number of points
    N = 2
    # spline and points dimension
    dim = 3

    # coordinates array for points
    coords = np.zeros( (N, dim), dtype=float )
    coords[0, :] = 0.1
    coords[1, :] = 0.2

    # create class for getting spline values
    getter = SimplotopeValuesGetter(
        coordinates=coords,
        spline_mat_file='example_smpl_fitted.mat',
        use_matlab_async=False
    )

    # launch the calculation. This should be launched for async launch
    getter.calculate_values()
    # wait the result for async launch. For sync it is also returns the result
    vals = getter.get_values()

    print( '\nfrom python: (values)' )
    print( vals )

    print( 'DONE!' )
