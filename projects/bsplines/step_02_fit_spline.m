% this step creates B-spline using 3-simplices (tetrahedrons) on a
% mesh which consists of cubes (NxNxN cubes).
% Every cube contains 6 tetrahedrons (tets).
%
% Spline degree, number of cubes and continuity order can be adjusted.


function step_02_fit_spline( ...
    input_calibration_PSFs_stack_file, ... % input .mat file with 
    ...                                    % calibration PSFs
    output_b_spline_file, ... % output .mat file with 
    ...                       % all B-spline parameters 
    polynomial_degree, ... % B-spline polynomial degree
    Ncubes, ... % number of cubes for a mesh
    continuity_order ... % continuity order of B-splines between tets
)
    folder_path = get_folder_path();
    addpath( strcat( folder_path, '../../../3rd_party/bsplinen_export' ) );

    %% Printing parameters
    disp( 'Start fitting B-spline function' );
    fprintf( 'Parameters:\n' );
    fprintf( 'input file = %s\n', input_calibration_PSFs_stack_file );
    fprintf( 'output file = %s\n\n', output_b_spline_file );
    fprintf( 'polynomial degree = %d\n', polynomial_degree );
    fprintf( 'Ncubes = %d\n', Ncubes );
    fprintf( 'continuity order = %d\n\n', continuity_order );

    %% Initialization
    % call for defining all GLOBAL structures for B-splines
    bsplinen_structures;
    spline_dimension = 3; % spline dimension DO NOT CHANGE IT 

    %% Load stack file
    fprintf( 'Loading stack file...\n' );
    stackFile = load( input_calibration_PSFs_stack_file );

    [XX,YY,ZZ] = meshgrid( 1:stackFile.n_pixels, ...
                           1:stackFile.n_pixels, ...
                           1:stackFile.n_z_slices );

    % translation from z-stack indices to z range from
    % -0.5*range to 0.5*range
    z = ZZ(:);
    z = ( (z - 1) / (stackFile.n_z_slices - 1) - 0.5 ) * stackFile.z_range;

    % Sizes of the calibration beads stack
    % (1) - number of calibration beads
    % (2) - number of pixels on Ox axis
    % (3) - number of pixels on Oy axis
    % (4) - number of pixels on Oz axis
    sizeOfStackFileStack = size( stackFile.stacks );
    n_stacks = sizeOfStackFileStack(1);

    % number of pixels per one bead ( sizeX * sizeY * sizeZ )
    N = sizeOfStackFileStack(2) * sizeOfStackFileStack(3) * sizeOfStackFileStack(4);
    Z = zeros( N * n_stacks, 1 );
    % all points from all calibration stack beads with applied shifts.
    XI = zeros( N * n_stacks, 3 );

    % applying shifts to the beads points.
    for i = 1 : n_stacks
        XI( ( i - 1 ) * N + 1 : i*N, : ) = ...
            [ XX(:) - stackFile.shifts(i, 1), ...
              YY(:) - stackFile.shifts(i, 2), ...
              z + stackFile.shifts(i, 3) * 10e-3 ];

        % one bead stack
        stack = stackFile.stacks(i, :, :, :);

        % paste from i*N element to (i+1)*N element the stack after already
        % pasted beads' stacks
        Z( ( i - 1 ) * N + 1 : i * N ) = stack(:);
    end




    %% Kuhn triangulation
    fprintf( 'Building Kuhn triangulation...\n' );
    % As far as I understood, this is a number of cubes
    % Every cube contains 6 tetrahedrons in Type I triangulation
    N = Ncubes;
    N_x = N;
    N_y = N;
    N_z = N;

    % one cube vertices
    VERTICES = [0 0 0
                0 0 1
                0 1 0
                0 1 1
                1 0 0
                1 0 1
                1 1 0
                1 1 1];

    % tetrahedrons of a one cube
    % number inside is a cube vertex
    EDGES = [1 5 6 8
             1 5 7 8
             1 3 7 8
             1 2 6 8
             1 2 4 8
             1 3 4 8];

    % array of all vertices for a chosen spatial triangulation
    % v0_x v0_y v0_z
    % v1_x v1_y v1_z
    % ...
    % vN_x vN_y vN_z
    PHI = zeros(N_x*N_y*N_z,3);

    % array of all tetrahedrons for a chosen spatial triangulation
    % every row contains 4 indices of vertices from array PHI
    TRI = zeros(1,4);

    % very straightforward algorithm for finding all vertices inside
    % this cubic partition of the space:
    %   1. build vertices for one cube (VERTICES array)
    %   2. create an array with all vertices for all cubes by just 
    %      appending cubes vertices with offset.
    %   3. delete all simular vertices in the array.
    i = 1;
    vertices_offsets = VERTICES;
    for yy=1:N_y
        for xx=1:N_x
            for zz=1:N_z
                tmpval = double( repmat([xx - 1, yy - 1, zz - 1], 8, 1) );
                vertices_offsets = VERTICES + tmpval;

                PHI((i-1)*8 + 1:i*8, :) = vertices_offsets;
                i = i + 1;
            end
        end
    end

    PHI = unique(PHI, 'rows');

    % creating all tetrahedrons
    % this algorithm for every cube and for every tetrahedron inside the cube
    % (6 tets per cube) creates an entity, translating local vertex indices
    % into global vertex index numbers
    i = 1;
    for yy=1:N_y
        for xx=1:N_x
            for zz=1:N_z
                vertices_offsets = VERTICES + double( repmat([xx - 1, yy - 1, zz - 1], 8, 1) );

                for ii=1:6
                    a = find( ismember( PHI, vertices_offsets(EDGES(ii, 1), :), 'rows' ) == true );
                    b = find( ismember( PHI, vertices_offsets(EDGES(ii, 2), :), 'rows' ) == true );
                    c = find( ismember( PHI, vertices_offsets(EDGES(ii, 3), :), 'rows' ) == true );
                    d = find( ismember( PHI, vertices_offsets(EDGES(ii, 4), :), 'rows' ) == true );

                    TRI(i, :) = [a b c d];
                    i = i + 1;
                end
            end
        end
    end

    % normalize vertices' coordinates to [0,1]
    PHI(:, 1) = PHI(:, 1) .* double(1.0 / N_x);
    PHI(:, 2) = PHI(:, 2) .* double(1.0 / N_y);
    PHI(:, 3) = PHI(:, 3) .* double(1.0 / N_z);


    PHI(:, 1) = PHI(:, 1) .* ( max(XI(:, 1)) - min(XI(:, 1)) ) + min(XI(:, 1));
    PHI(:, 2) = PHI(:, 2) .* ( max(XI(:, 2)) - min(XI(:, 2)) ) + min(XI(:, 2));
    PHI(:, 3) = PHI(:, 3) .* ( max(XI(:, 3)) - min(XI(:, 3)) ) + min(XI(:, 3));

    % sort tetrahedrons according to vertex indices in ascending lexicographical order
    TRI = sortrows(TRI);
    % sort vertices for every tetrahedron (VERY IMPORTANT FOR CONTINUITY!)
    TRI = sort(TRI, 2);



    %%       Estimate B-coefficients
    fprintf( 'Estimating B-coefficients...\n' );
    tic;
    fprintf( '\tconstructing B-spline basis...\n' );
    polybasis = bsplinen_constructBasis(spline_dimension, polynomial_degree);

    options = struct_bsplinen_options;
    options.stat_outputmsg = 1;
    options.data_partsize = 10000000;

    [tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI}, {});
    H = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, continuity_order, options);

    % create the regression matrix for the given dataset, triangulation, and degree.
    [X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis, options);
    toc;

    % Use iterative solver to find b coefs
    disp('Start iterative solver');
    tic;
    Q = X' * X;
    F = X' * Y;
    options = struct_bsplinen_options;
    options.solver_outputmsg = 1;

    % cre
    % bcoefs = bsplinen_iterateC(Q, [], F, [], [], options);
    bcoefs = bsplinen_iterateC(Q, H, F, [], [], options);
    toc

    % Construct spline function structure
    spline = struct_bsplinen;
    % Set the bsplinen structure properties
    spline.PHI        = PHI;               % vertices with coordinates
    spline.TRI        = TRI;               % tetrahedrons (3-simplices)
    spline.degree     = polynomial_degree;
    spline.continuity = continuity_order;
    spline.dim        = spline_dimension;
    spline.coefs      = bcoefs;            % B-coefficients


    %% Save resuls
    % Create splines with 1st order directional derivatives
    spline_x = bsplinen_derivExt(spline, 1, [1 0 0]);
    spline_y = bsplinen_derivExt(spline, 1, [0 1 0]);
    spline_z = bsplinen_derivExt(spline, 1, [0 0 1]);

    n_zslices = stackFile.n_z_slices;
    n_pixels = stackFile.n_pixels;
    z_range = stackFile.z_range;
    save( output_b_spline_file, ...
          'spline', ...
          'spline_x', ...
          'spline_y', ...
          'spline_z', ...
          'XI', ...
          'Z', ...
          'n_pixels', ...
          'n_stacks', ...
          'n_zslices', ...
          'z_range' );
    disp(numel(spline.coefs));
end


% Returns folder name of the folder with this MATLAB script
% Very useful for 'addpath()' functions, because then this script can be
% launched from any folder.
function folder_path = get_folder_path()
    s = mfilename('fullpath');
    
    for i=length(s):-1:1
        if s(i) == '/' || s(i) == '\'
            folder_path = extractBetween( s, 1, i );
            break;
        end
    end
    folder_path = folder_path{1};
    
    fprintf( '\n\nmy path:\n%s\n\n', folder_path );
end
