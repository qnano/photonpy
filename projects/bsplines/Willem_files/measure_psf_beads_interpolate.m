close all;

INPUT_MOVIE = 'C:\Users\Willem Melching\Dropbox\Thesis\data\z-stack-Beads-AS-Exp-as-stack\sequence-as-stack-Beads-AS-Exp.tif';
plotting = false;
% TODO: read from files
z_range = -0.75:0.01:0.75; % In microns

POSITIONS = [25.05051088 20.37593071
             93.43403236 28.25086383
             43.50957276 71.04008986
             28.84621702 119.5179242
             90.83092481 111.3964048
             122.6776528 62.16889649];
         
         
BACKGROUND = 108.5392;
         
% TODO: Subpixel peak finding
info = imfinfo(INPUT_MOVIE);
img = double(imread(INPUT_MOVIE, round(length(info) / 2)));
% threshold = max(img(:)) * 0.5;
% peaks = FastPeakFind(img, threshold);
% POSITIONS = [peaks(1:2:end) peaks(2:2:end)];

%imagesc(img); hold on;
%plot(peaks(1:2:end),peaks(2:2:end),'r+');
%%

NUM_BEADS = size(POSITIONS, 1);
     
AOI_SIZE = 15; 
IMG_SIZE = 2*AOI_SIZE+1;

info = imfinfo(INPUT_MOVIE);
PSFS = zeros(NUM_BEADS, length(info), IMG_SIZE, IMG_SIZE);
PSFS_non_norm = zeros(NUM_BEADS, length(info), IMG_SIZE, IMG_SIZE);
photon_counts = zeros(NUM_BEADS, length(info));

for frame=1:length(info)
    img = double(imread(INPUT_MOVIE, frame));
    
    for p=1:NUM_BEADS
        pos = POSITIONS(p, :);
        x = pos(1);
        y = pos(2);
        
        F = griddedInterpolant(img, 'cubic');
        yy = y - AOI_SIZE:y + AOI_SIZE;
        xx = x - AOI_SIZE:x + AOI_SIZE;
                
        psf = F({yy, xx}); 
        PSFS_non_norm(p, frame, :, :) = psf;
        
%         psf = psf - BACKGROUND;
%         psf(psf < 0) = 0;
        photon_counts(p, frame) = sum(psf(:));
        
        % Normalize each frame since each frame should contain approx the
        % same number of photons
        psf = psf ./ sum(psf(:));
        PSFS(p, frame, :, :) = psf;
        
        if plotting
            figure(1);
            subplot(2, 3, p);
            imagesc(psf);
        end
        
    end
    if plotting
        figure(10);
        imagesc(img);
    end
end

% Normalize each bead stack so the brightest pixel is 1
for p=1:NUM_BEADS
    psf = PSFS(p, :, :, :);
    PSFS(p, :, :, :) = PSFS(p, :, :, :) ./ max(psf(:));
end

%%
Z_meas = zeros(NUM_BEADS * length(info) * (IMG_SIZE)^2, 1);
X_meas = zeros(NUM_BEADS * length(info) * (IMG_SIZE)^2, 3);


i = 1;
for p=1:NUM_BEADS
    for frame=1:length(info)        
        for y=1:(2*AOI_SIZE+1)
            for x=1:(2*AOI_SIZE+1)
                Z_meas(i) = PSFS(p, frame, y, x);
                X_meas(i, :) = [x, y, z_range(frame)];
                i = i + 1;
            end
        end
    end
end