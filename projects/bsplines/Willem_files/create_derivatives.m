% Create splines with 1st order directional derivatives
spline_x = bsplinen_derivExt(spline, 1, [1 0 0]);
spline_y = bsplinen_derivExt(spline, 1, [0 1 0]);
spline_z = bsplinen_derivExt(spline, 1, [0 0 1]);