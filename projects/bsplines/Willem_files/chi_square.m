close all;
REAL_PHOTON_COUNT = 4e5; % This is the sum of one ROI of the input data. Is the gain 1?
chis = zeros(1, N);
BEAD = 3;

z_index = 20;

for z_index=1:151
    z = z_range(z_index);

    bead_spline_x = zeros(64^2, 3);
    
    i = 1;
    for xx = 1:IMG_SIZE
        for yy = 1:IMG_SIZE
          bead_spline_x(i, :) = [xx, yy, z];
          Z(i) = 0;
          i = i + 1;
        end
    end

    Z = zeros(64^2, 1);
    [XIeval3, Zeval3, Beval3] = bsplinen_evalMat(bead_spline_x, Z, spline);
    bead_spline_img = Beval3 * spline.coefs;
    bead_spline_img = reshape(bead_spline_img, IMG_SIZE, IMG_SIZE);

    bead_img = squeeze(PSFS(BEAD, z_index, :, :)); 
    bead_img_scaled = bead_img ./ (sum(bead_img(:)) / REAL_PHOTON_COUNT);

    N = 15;
    i = 1;
    for i=1:N
        [bead_img_scaled, ~] = do_binosplit(bead_img_scaled, 0.5);
        bead_spline_img_scaled = bead_spline_img .* (sum(bead_img_scaled(:) / sum(bead_spline_img(:))));

        chi = sum((bead_img_scaled(:) - bead_spline_img_scaled(:)).^2 ./ bead_spline_img_scaled(:));
        deg_freedom  = (IMG_SIZE-1)^2;
        p_val = chi2cdf(deg_freedom, chi);

        chis(i) = chis(i) + chi;
        i = i + 1;
    end
end

%%
figure(2);
% subplot(2,1,1);
expected_chi_square = (IMG_SIZE^2) * 151;
semilogx(intensities, ones(size(intensities)) .* expected_chi_square, '--k');
hold on;
semilogx(intensities, chis);
title('\chi^2');
xlabel('Photon count');
legend('Expected \chi^2', '\chi^2', 'location', 'northwest');
ylim([0, expected_chi_square * 3]);
xlim([min(inensities), max(intensities)]);


% subplot(2,1,2);
% semilogx(intensities, p_vals);
% title('P value');
% xlabel('Photon count');


function [imoutA,imoutB] = do_binosplit(imin,psplit)
% This function splits an input image in two parts using the binomial
% random distribution. If the noise on imin follows Poison-statistics then
% the noise on imoutA and imoutB also follow Poisson-statistics with rates
% that are psplit x and (1-psplit)x less. The parameter psplit must satisfy
% 0<psplit<1.
%
% copyright Sjoerd Stallinga, TU Delft, 2018

epsy = 100*eps;
tempim = round(imin);
diffim = imin-tempim;
imoutA = binornd(tempim,psplit);
imoutA = imoutA+(imoutA./(imin+epsy)).*diffim; 
imoutB = imin-imoutA;

end
