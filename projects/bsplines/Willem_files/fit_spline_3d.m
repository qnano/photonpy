close all;
clc;

%% Initialization
plotting = true;
bsplinen_structures;

% polynomial polybasis properties
spline_dimension = 3; % spline dimension
polynomial_degree = 5; % polynomial degree
continuity_order = 1; % continuity order


%% Setup data
XI = X_meas;
Z = Z_meas;

XIeval = XI;
Zeval = Z;


%% Kuhn triangulation
disp('Triangulation');
N_x = 6;
N_y = 6;
N_z = 6;

VERTICES = [0 0 0
            0 0 1
            0 1 0
            0 1 1
            1 0 0
            1 0 1
            1 1 0
            1 1 1];
EDGES = [1 5 6 8
         1 5 7 8
         1 3 7 8
         1 2 6 8
         1 2 4 8
         1 3 4 8];

PHI = zeros(N_x*N_y*N_z,3);
TRI = zeros(1,4);


i = 1;
for yy=1:N_x
    for xx=1:N_y
        for zz=1:N_z
            vertices_offsets = VERTICES + repmat([xx - 1, yy - 1, zz - 1], 8, 1);
            PHI((i-1)*8 + 1:i*8, :) = vertices_offsets;
            i = i + 1;
        end
    end
end

PHI = unique(PHI, 'rows');

i = 1;
for yy=1:N_x
    for xx=1:N_y
        for zz=1:N_z
            vertices_offsets = VERTICES + repmat([xx - 1, yy - 1, zz - 1], 8, 1);

            for ii=1:6
                a = find(ismember(PHI, vertices_offsets(EDGES(ii, 1), :), 'rows')==1);
                b = find(ismember(PHI, vertices_offsets(EDGES(ii, 2), :), 'rows')==1);
                c = find(ismember(PHI, vertices_offsets(EDGES(ii, 3), :), 'rows')==1);
                d = find(ismember(PHI, vertices_offsets(EDGES(ii, 4), :), 'rows')==1);
                TRI(i, :) = [a b c d];
                i = i + 1;
            end
        end
    end
end


PHI(:, 1) = PHI(:, 1) .* (1/N_x);
PHI(:, 2) = PHI(:, 2) .* (1/N_y);
PHI(:, 3) = PHI(:, 3) .* (1/N_z);
PHI(:, 1) = PHI(:, 1) .* (max(XI(:, 1)) - min(XI(:, 1))) + min(XI(:, 1));
PHI(:, 2) = PHI(:, 2) .* (max(XI(:, 2)) - min(XI(:, 2))) + min(XI(:, 2));
PHI(:, 3) = PHI(:, 3) .* (max(XI(:, 3)) - min(XI(:, 3))) + min(XI(:, 3));

TRI = sortrows(TRI); % sort vertex indices
TRI = sort(TRI, 2); % sort vertices (VERY IMPORTANT FOR CONTINUITY!)

if plotting
    tetramesh(TRI, PHI);
    camorbit(20,0)
end
% the number of simplices
T = size(TRI, 1);

%t = tsearchn(PHI, TRI, XI);

%%       Estimate B-coefficients
disp('Estimate B-coefficients');
tic;
polybasis = bsplinen_constructBasis(spline_dimension, polynomial_degree);

options = struct_bsplinen_options;
options.stat_outputmsg = 1;
options.data_partsize = 10000000;

% create the object oriented simplex collection (DELTA) and the edge collection (EDGES)
[tmp, DELTA, EDGES] = bsplinen_triangulateExt({PHI, TRI}, {});

% create the matrix holding the smoothness constraints

H = bsplinen_constructHExt(DELTA, PHI, EDGES, polybasis, continuity_order, options);

% create the regression matrix for the given dataset, triangulation, and degree.
[X, Y] = bsplinen_genregExt(XI, Z, PHI, TRI, polybasis, options);
toc

% Use iterative solver to find b coefs
disp('Start iterative solver');
tic;
Q = X' * X;
F = X' * Y;
options = struct_bsplinen_options;
options.solver_outputmsg = 1;


bcoefs = bsplinen_iterateC(Q, H, F, [], [], options);
toc

% Construct spline function structure
spline = struct_bsplinen;
% Set the bsplinen structure properties
spline.PHI        = PHI;
spline.TRI        = TRI;
spline.degree     = polynomial_degree;
spline.continuity = continuity_order;
spline.dim        = spline_dimension;
spline.coefs      = bcoefs;

%%
%spline = bsplinen_derivExt(spline, 1, [0 0 1]);


%%       Evaluate spline function
disp('Evaluating spline');
tic;
[XIeval2, Zeval2, Beval2] = bsplinen_evalMat(XIeval, Zeval, spline);
val_spline = Beval2 * spline.coefs;
toc

%TRIeval = delaunayn(XIeval);

err_spline      = Zeval2 - val_spline;
rms_spline      = sqrt(mse(err_spline));
rmsrel_spline   = rms_spline / sqrt(mse(Zeval2));

p_errors = abs(err_spline) ./ val_spline;
fprintf('Max error %.2f', max(p_errors) * 100.);


fprintf('\n---------------------------------------------------------------------\n');
fprintf('Spline error RMS = %d, relative error RMS = %2.2f [%s]\n', rms_spline, 100*rmsrel_spline, '%');
fprintf('---------------------------------------------------------------------\n');


%%                       Do the plots
if plotting
   % close all;
    figure(100);
    N = 4;
    
    for y=1:N
        for x=1:N
            plot_id = N * (y-1) + x;
            c = (plot_id / N^2) * length(z_range);
            z = z_range(round(c));
            
            XIeval = zeros(64^2, 3);
            i = 1;
            for xx = 1:IMG_SIZE
                for yy = 1:IMG_SIZE
                  XIeval(i, :) = [xx, yy, z];
                  i = i + 1;
                end
            end

            [XIeval3, Zeval3, Beval3] = bsplinen_evalMat(XIeval, Zeval, spline);
            val_spline = Beval3 * spline.coefs;
            %Bcoords = bsplinen_calcBCoefCoordinates(spline);

            subplot(4, 4, plot_id);
            imagesc(reshape(val_spline, IMG_SIZE-1, IMG_SIZE-1));
            title(sprintf('z = %.2f', z));
    
        end
    end
end
