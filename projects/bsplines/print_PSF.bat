cd utils
py print_PSF.py -i ../../../../OUTPUT/calibration_bspline.mat -o ../../../../OUTPUT/print_PSF_calibration_bspline_PSF.tif
py print_PSF.py -i ../../../../OUTPUT/stack_avg.tif -o ../../../../OUTPUT/print_PSF_average_stack_PSF.tif
cd ..