import numpy as np                  # working with arrays
import sys                          # add directory to utils for PSFs generator
import tifffile

sys.path.append( '../psfsim/' )
from simulate_PSF_Vectoral_model_launcher import PSFVectorialSim
from simulation_result_reader import SimulationDataReader

sys.path.append( '../../' )
from create_calibration import estimate_calibration_beads_shifts

sys.path.append( '../utils/' )
from print_PSF import print_PSF_from_3d_array

# INI filename that will be used for launching the shifts test
INI_FILENAME = 'test_shift_finding.ini'


def correct_shifts( shifts ):
    """The same as Willem did for X and Y shifts.

    All Z shifts minus Z shift of 0-index bead.
    """

    z0_shift = shifts[0][2]
    for i in range( shifts.shape[0] ):
        shifts[i][2] = shifts[i][2] - z0_shift

    return shifts


def comparing_shifts( true_shifts, est_shifts, consider_as_zero=1e-7 ):
    """Function compares true shifts used in simulation and estimated shifts

    We need consider_as_zero parameter in order to not divide by 0.

    Returns:
        maximum absolute difference and
        maximum relative difference which is calculated as
            rel. err. = max for i |est_shifts[i]-true_shifts[i]| / true_shifts[i]
    """

    print( 'True shifts:' )
    print( true_shifts )

    print( '\nEstimated shifts:' )
    print( est_shifts )

    # Comparing shifts
    delta = est_shifts - true_shifts
    print( '\nDifference:' )
    print( delta )

    # max absolute value of shifts difference
    max_abs_diff = np.maximum( abs( delta.max( axis=0 ) ),
                               abs( delta.min( axis=0 ) ) )

    print( "\nERRORS:" )
    print( 'Max absolute difference X:', max_abs_diff[0] )
    print( 'Max absolute difference Y:', max_abs_diff[1] )
    print( 'Max absolute difference Z:', max_abs_diff[2] )
    print( '' )

    # finding max relative error
    max_rel_shift = [0,0,0]
    for bead_idx in range( len( true_shifts ) ):
        loc_rel_err = 0

        for axis in range( true_shifts.shape[1] ):
            gr_trth_shift = true_shifts[bead_idx][axis]

            if abs( gr_trth_shift ) > consider_as_zero:
                loc_rel_err = abs( est_shifts[bead_idx][axis] - gr_trth_shift ) / \
                              abs( gr_trth_shift )

            if loc_rel_err > max_rel_shift[axis]:
                max_rel_shift[axis] = loc_rel_err

    print( 'Max relative difference in shift X:', max_rel_shift[0]*100.0, r'%' )
    print( 'Max relative difference in shift Y:', max_rel_shift[1]*100.0, r'%' )
    print( 'Max relative difference in shift Z:', max_rel_shift[2]*100.0, r'%' )

    av = np.average( true_shifts[1:,2] / est_shifts[1:,2] )
    print( 'average multiply coefficient =', av )

    return max_abs_diff, max_rel_shift


class SimulationForShiftsResults:
    """ Structure for results after one MATLAB simulation and shift finding algo.

    Attributes:
        est_shifts (numpy 2D array): [bead idx][axis idx]
            all estimated by the shift finding algo shifts for all beads.

        true_shifts (numpy 2D array): [bead idx][axis idx]
            real shifts that were used during MATLAB simulation.

        max_abs_error (1D list): maximum absolute difference between estimated
            and true shifts for every axis.

        max_rel_error (1D list): maximum relative difference between estimated
            and true shifts for every axis. It is calculated as
            max for i: |true_shift[i] - est_shift[i]| / |true_shift[i]|.
    """

    def __init__(self):
        self.est_shifts = []
        self.true_shifts = []
        self.max_abs_error = [0.0, 0.0, 0.0]
        self.max_rel_error = [0.0, 0.0, 0.0]


def normalize_PSF( PSFs ):
    """Use some kind of normalization for PSF

    Now it uses division to the max value.
    We need that, because for big intensities like 1e9, 1e10 the shift
    finding algorithm in cross-correlation part stuck with overflow in float
    (which is actually quite weird, but still).
    """

    I_max = np.max( PSFs )
    return PSFs / I_max


def simulation_test_for_shifts( normalized_PSFs, sim_reader, sim_launcher, save_movie:bool ):
    """Simulate one case in MATLAB and compare results with ground truth"""

    print( '\nSimulating PSFs...' )

    # add intensity and background
    photon_PSFs = normalized_PSFs * sim_launcher.intensity + \
                  sim_launcher.background
    # add poisson noise
    if sim_launcher.use_poisson_noise:
        for i in range( len( photon_PSFs ) ):
            photon_PSFs[i] = np.random.poisson( photon_PSFs[i] )

    print( 'Simulation PSFs is done' )

    # saving movie file with all beads for debug
    if save_movie:
        print( 'Saving PSFs movie...' )
        old_PSF = sim_reader.PSFs
        sim_reader.PSFs = photon_PSFs
        movie = sim_reader.toMovie( sim_launcher.background,
                                    sim_launcher.use_poisson_noise )
        sim_reader.PSFs = old_PSF

        # save the movie file into .tif
        with tifffile.TiffWriter( sim_launcher.outfile + '.tif' ) as tif:
            tif.save( movie, compress=2 )
        print( 'Saving PSFs movie is complete' )

        # printing PSFs with ZX, ZY slice
        tiffname = sim_launcher.outfile + "_0.tif"
        print_PSF_from_3d_array( photon_PSFs[0],
                                 tiffname )

    # Estimating shifts using storm-analysis algo
    print( '\nFinding shifts...' )
    # I don't know the purpose of this parameters
    dx = 0.0
    dy = 0.0

    photon_PSFs = normalize_PSF( photon_PSFs )

    shifts, dx_a, dy_a, average_stack = \
        estimate_calibration_beads_shifts( photon_PSFs, dx, dy,
                                           verbose=False )
    print( 'Finding shifts is done' )

    # Correcting shifts. Here we suppose that the 0-index bead has no shift
    shifts = correct_shifts( shifts )

    # comparing shifts with ground truth shifts
    abs_err, rel_err = comparing_shifts( true_shifts=sim_reader.true_shifts,
                                         est_shifts=shifts )

    # creating output structure
    res = SimulationForShiftsResults()
    res.est_shifts = shifts
    res.true_shifts = sim_reader.true_shifts
    res.max_abs_error = abs_err
    res.max_rel_error = rel_err

    return res


def test_shift_finding_algo( inifilename ):
    """Checks shift finding algorithm

    Simulates PSFs using Vectorial model with some shifts.
    After that uses shift finding algorithm from storm analysis.
    Compares true shifts with estimated by algorithm shifts.
    """

    ############################################################################
    # some hardcoded constants for the big experiments
    N_EXPERIMENTS_PER_BEAD = 1
    AXIS = 2
    SAVE_MOVIE = True
    INTENSITY_MULTIPLIER = float( 1e4 )
    NUMBER_OF_INTENSITIES = 1
    ############################################################################

    # reading configurations for simulation
    sim_launcher = PSFVectorialSim()
    sim_launcher.read_from_config( inifilename )

    # some preparations before changing some parameters in cycle
    I_beg = sim_launcher.intensity
    I_end = sim_launcher.intensity * INTENSITY_MULTIPLIER
    all_intensities = np.logspace( np.log10( I_beg ), np.log10( I_end ),
                                   num=NUMBER_OF_INTENSITIES )

    fl = open( 'precision_and_accuracy_Poisson.txt', 'w' )
    s_top = 'intensity'
    for i in range( sim_launcher.Nbeads ):
        s_top = s_top + f'\tprec_bead_{i}'
    for i in range( sim_launcher.Nbeads ):
        s_top = s_top + f'\tacc_bead_{i}'

    fl.write( f'{s_top}\n' )

    sim_launcher.call_MATLAB_launcher()
    sim_reader = SimulationDataReader( sim_launcher.outfile + '.mat' )
    normalized_PSFs = sim_reader.PSFs

    true_shifts = sim_launcher.defined_shifts

    for intensity in all_intensities:
        est_shifts = np.zeros( [sim_launcher.Nbeads, N_EXPERIMENTS_PER_BEAD] )

        sim_launcher.intensity = intensity

        for iexp in range( N_EXPERIMENTS_PER_BEAD ):
            # simulate PSFs and check the shift finding error
            test_res = simulation_test_for_shifts( normalized_PSFs,
                                                   sim_reader,
                                                   sim_launcher,
                                                   save_movie=SAVE_MOVIE )

            # copy to error array with all experiments
            for i in range( sim_launcher.Nbeads ):
                est_shifts[i][iexp] = test_res.est_shifts[i][AXIS]

            print( f'EXPERIMENT for {iexp} from '
                   f'{N_EXPERIMENTS_PER_BEAD} is finished\n' )

        aver_est = np.average( est_shifts, axis=1 )
        sum_prec = np.zeros( [sim_launcher.Nbeads] )
        sum_acc = np.zeros( [sim_launcher.Nbeads] )

        for ibead in range( sim_launcher.Nbeads ):
            for iexp in range( N_EXPERIMENTS_PER_BEAD ):
                sum_prec[ibead] = sum_prec[ibead] \
                    + ( est_shifts[ibead][iexp] - true_shifts[ibead][AXIS] )**2

                sum_acc[ibead] = sum_acc[ibead] \
                    + ( est_shifts[ibead][iexp] - aver_est[ibead] )**2

        sum_prec = np.sqrt( sum_prec / N_EXPERIMENTS_PER_BEAD )
        sum_acc = np.sqrt( sum_acc / N_EXPERIMENTS_PER_BEAD )

        # s = f'{n_z_slices}'
        s = f'{intensity}'
        for i in range( sim_launcher.Nbeads ):
            s = s + f'\t{sum_prec[i]}'
        for i in range( sim_launcher.Nbeads ):
            s = s + f'\t{sum_acc[i]}'

        fl.write( f'{s}\n' )
        fl.flush()

        print( f'\nIntensity {intensity} from '
               f'{all_intensities[len(all_intensities)-1]} IS FINISHED' )
        print( '===========================================================' )

    fl.close()

    print( 'Test is finished!' )


if __name__ == '__main__':
    test_shift_finding_algo( INI_FILENAME )
