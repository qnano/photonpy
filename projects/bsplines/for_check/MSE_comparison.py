"""
Script for comparing calibration PSFs with PSF model such as B-spline PSF
or C-spline PSF, or Gaussian 3D PSF.

(flag -m, --model)
The PSF model should be presented in .tif file.

(flag -c, --calibration)
The PSFs calibration stacks should be placed into .mat file using MATLAB
or scipy.io and have PSF stacks in 4D array called 'PSFs'.
[bead idx][z slice idx][y idx][x idx]

(flag -d, --diff)
If user wants, the difference .tif files can be written.
"""

import numpy as np
import argparse
import scipy.io
import tifffile
import math
import matplotlib.pyplot as plt


# if True, the plot for sum intensity in every z slice for all PSFs will be
# produced
_DEBUG_PLOT_SUM_INTENSITIES = True


def MSE_comparison_for_PSF_stacks( PSFmodel, PSF_stacks ):
    """MSE comparison between calibration PSFs and PSF model"""

    Nbeads = len( PSF_stacks )

    MSE = np.sum( np.square( PSF_stacks - PSFmodel ), axis=(1,2,3) )

    aver_MSE = np.average( MSE )
    err_MSE = math.sqrt(
        1.0 / ( Nbeads - 1.0 ) * np.sum( np.square( MSE - aver_MSE ) )
    )

    return MSE, aver_MSE, err_MSE


def read_MAT_simulation_PSFs_file( mat_filename:str ):
    """Read .mat file produced by blinking_simulation.py"""

    mat = scipy.io.loadmat( mat_filename )
    PSFs = mat['PSFs']

    return np.array( PSFs, dtype=float )


def read_TIF_PSF_model_file( tif_filename:str ):
    """Read PSF from .tif with modeled PSF"""

    with tifffile.TiffFile( tif_filename ) as tif:
        PSFs = tif.asarray()

    return np.array( PSFs, dtype=float )


def normalize_PSF( PSF ):
    """Normalize PSF using in-focus slice calculated as argmax intensity"""

    in_focus_idx = np.argmax( np.max( PSF, axis=(1,2) ) )

    sum_intensity_focus = np.sum( PSF[in_focus_idx] )

    PSF /= sum_intensity_focus

    return in_focus_idx


def plot_summation_of_intensities( PSF, desc:str=None ):
    """Plot for sum of intensities"""

    sumI = np.sum( PSF, axis=(1,2) )

    Npoints = len( sumI )

    x_arr = np.linspace( 0, Npoints - 1, num=Npoints )

    if desc is not None:
        plt.title( desc )

    plt.plot( x_arr, sumI )
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Computes errors between PSF model and PSF "
                    "calibration stacks"
    )

    parser.add_argument( "-m", "--model", required=True,
                         help="Input TIFF file (.tif) for PSF model" )

    parser.add_argument( "-c", "--calibration", required=True,
                         help="Input MATLAB file for calibration PSFs" )

    parser.add_argument( "-d", "--diff", default=None,
                         help="start of the name for .tif file where "
                              "difference will be placed" )

    args = parser.parse_args()

    if not args.calibration.endswith( ".mat" ):
        raise Exception( "Unknown calibration file extension. Use .mat" )

    print( f'\nmodel PSF file = {args.model}' )
    print( f'calibration PSF stacks file = {args.calibration}\n' )

    # reading files
    print( 'Reading calibration PSFs file...' )
    PSFs_calibration = read_MAT_simulation_PSFs_file( args.calibration )
    print( 'Reading model PSF file...' )
    PSFs_model = read_TIF_PSF_model_file( args.model )

    # normalize all PSFs
    print( '\nNormalizing PSFs...' )
    print( f'Nz_slices = {len(PSFs_model)}' )
    for ibead in range( len( PSFs_calibration ) ):
        in_focus_idx = normalize_PSF( PSFs_calibration[ibead] )
        print( f'in-focus idx {in_focus_idx} for bead {ibead}' )

    in_focus_idx = normalize_PSF( PSFs_model )
    print( f'\nin-focus idx {in_focus_idx} for model PSF\n' )

    # sum intensities plots
    if _DEBUG_PLOT_SUM_INTENSITIES:
        for ibead in range( len( PSFs_calibration ) ):
            plot_summation_of_intensities( PSFs_calibration[ibead],
                                           desc=f'sum intensity bead {ibead}' )

        plot_summation_of_intensities( PSFs_model,
                                       desc='sum intensity PSF model' )

    # MSE comparison
    print( 'MSE comparison between stacks:' )
    MSE, aver_MSE, err_MSE = MSE_comparison_for_PSF_stacks(
        PSFmodel=PSFs_model,
        PSF_stacks=PSFs_calibration
    )
    Nbeads = len( MSE )
    for ibead in range( Nbeads ):
        print( f'MSE for bead {ibead} = {MSE[ibead]}' )
    print( '' )
    print( f'average MSE = {aver_MSE}' )
    print( f'error MSE = {err_MSE}\n' )

    # printing difference .tif-s
    if args.diff is not None:
        print( 'Saving difference between calibration PSFs and the model PSF' )
        difPSFs = PSFs_calibration - PSFs_model
        difPSFs = difPSFs.astype( np.float32 )

        for ibead in range( Nbeads ):
            filename = f'{args.diff}_{ibead}.tif'

            with tifffile.TiffWriter( filename ) as tif:
                tif.save( difPSFs[ibead], compress=2 )
            print( f'\t{filename} is saved' )

    print( 'DONE!' )