"""
Test for the simplotopes fitting stage.
Also, getting spline values function was tested here.

This script can be used as an example of how to use the fitting simplotopes
spline class.

PSFs + shifts -> simplotope spline
"""

import numpy as np
from tqdm import tqdm
import scipy.io
import tifffile

import bsplines.psfsim.fftpupil_psf as zola3d
from bsplines.simplotope_fit.simplotope_fitter import SimplotopeFitter
from bsplines.simplotope_fit.get_simplotope_spline_values import SimplotopeValuesGetter
#from bsplines.bsp_utils.print_PSF import print_PSF_from_3d_array


# flag for debug - some additional info will be added to log if it is true
_DEBUG = True


def calc_z_step( z_range:float, n_z_slices:int ):
    """Calc z step for z range [-z_range, +z_range] and num slices n_z_slices"""

    return 2.0 * z_range / (n_z_slices - 1)


def create_subpixel_shifts( range_for_shifts, N_calibr:int ):
    """Create subpixel random shifts for PSF calibration stacks"""

    random_subpix_shifts = np.zeros( (N_calibr, 3), dtype=np.float32 )

    for i in range( len( range_for_shifts ) ):
        random_subpix_shifts[:, i] = np.random.uniform( -range_for_shifts[i],
                                                        range_for_shifts[i],
                                                        size=N_calibr )

    return random_subpix_shifts


def create_PSF_stacks( N_calibr:int, n_z_slices:int, ROI:int,
                       pixelsize:float, z_range:float, subpix_shifts ):
    """Generate several PSF stacks with subpixel shifts using Zola3D model"""

    NOLL_IDX = 6

    zernike_coeff = np.zeros( 20, dtype=np.float32 )
    zernike_coeff[NOLL_IDX - 2] = 1.0

    psf_gen = zola3d.PSF(
        psfsize=ROI,
        lam=0.6,
        pixelsize=pixelsize,
        NA=1.49,
        maxnoll=len( zernike_coeff ) + 1,
        ref_imm_fluid=1.5,
        ref_sample=1.33
    )

    z_step = calc_z_step( z_range, n_z_slices )

    if _DEBUG:
        print( 'Generated subpixel positions for calibration PSFs:' )
        print( 'x\ty\tz' )
        print( subpixel_shifts, '\n' )

    random_subpix_pos_um = np.copy( subpix_shifts )
    random_subpix_pos_um[:, 0] *= pixelsize
    random_subpix_pos_um[:, 1] *= pixelsize
    random_subpix_pos_um[:, 2] *= z_step

    PSF_calibr_stacks = np.zeros(
        (N_calibr, n_z_slices, ROI, ROI ), dtype=np.float32
    )

    for istack in tqdm( range( N_calibr ), desc='Gen. calib. PSFs' ):
        PSF_calibr_stacks[istack] = psf_gen.psf3D(
            offsetXYZ=random_subpix_pos_um[istack, :],
            zern_coeff=zernike_coeff,
            z_range=z_range,
            n_z_slices=n_z_slices,
            normalize=True
        )

    return PSF_calibr_stacks


def save_PSF_stacks( psf_stacks_file:str, psfs, z_range:float, shifts ):
    """Save PSF stacks for Simplotope fitting stage"""

    n_pixels = psfs.shape[2] # ROI size
    n_z_slices = psfs.shape[1]

    store = {
        'stacks': psfs,
        'z_range': z_range*2.0,
        'shifts': shifts,
        'n_pixels': n_pixels,
        'n_z_slices': n_z_slices
    }

    scipy.io.savemat( psf_stacks_file, store )

def comparing_the_results( data_pts_input_values, data_smpl_values ):
    """Comparing real data used for fitting and approximated by spline"""

    val_difference = data_pts_input_values - data_smpl_values

    max_diff = np.max( np.abs( val_difference ) )
    aver_diff = 1.0/val_difference.shape[0] * np.sum( np.abs( val_difference ) )

    max_value = np.max( data_pts_input_values )
    rel_max_diff = max_diff / max_value
    rel_aver_diff = aver_diff / max_value

    print( f'max_difference = {max_diff}' )
    print( f'average difference = {aver_diff}' )
    print( f'relative to max difference = {rel_max_diff}' )
    print( f'relative to max average difference = {rel_aver_diff}' )


def print_simplotope_tif( tif_filename:str, ROI_calibr:int,
                          n_z_slices:int, z_range:float,
                          fitted_spline_file:str,
                          use_matlab_async:bool ):
    Npts = ROI_calibr * ROI_calibr * n_z_slices
    z_step = calc_z_step( z_range, n_z_slices )

    psf_print_coordinates = np.zeros( (Npts, 3), dtype=float )
    idx = 0
    for iz in range( n_z_slices ):
        for iy in range( ROI_calibr ):
            for ix in range( ROI_calibr ):
                psf_print_coordinates[idx][0] = ix
                psf_print_coordinates[idx][1] = iy
                psf_print_coordinates[idx][2] = iz * z_step - z_range
                idx += 1

    for_psf_getter = SimplotopeValuesGetter(
        coordinates=psf_print_coordinates,
        spline_mat_file=fitted_spline_file,
        use_matlab_async=use_matlab_async
    )
    for_psf_getter.calculate_values()
    simplotope_PSF_values = for_psf_getter.get_values()

    simplotope_PSF = np.reshape( simplotope_PSF_values,
                                 (n_z_slices, ROI_calibr, ROI_calibr) )
    simplotope_PSF[simplotope_PSF < 0] = 0

    with tifffile.TiffWriter( tif_filename ) as tif:
        tif.save( np.array( simplotope_PSF, dtype=np.float32 ), compress=0 )


if __name__ == '__main__':
    ### Filenames
    psf_stacks_file = 'psf_stacks_with_shifts.mat'
    fitted_spline_file = 'simplotope_spline.mat'
    tif_simplotope_file = 'simplotope_PSF.tif'

    ### Calibration stack parameters
    # number of calibration PSFs to generate
    N_calibr = 3 # 6
    # intensity for generation calibration PSFs
    I_calibr = 1e5 # 1e5
    # background level for generation calibration PSFs
    bg_calibr = 20 # 20
    # if True, Poisson noise will be used for calibration PSF stacks
    use_poisson_noise_for_calibr = True
    # z range for full PSF stack in um. PSF changes in [-z_range, +z_range]
    z_range = 0.6
    # number of z slices for one PSF stack
    n_z_slices = 151
    # number of pixels for one PSF z slice for x or y direction
    ROI_calibr = 15
    # range in pixels for random subpixel position for calibration PSF stacks
    # [-range_for_shifts[i], +range_for_shifts[i]]
    range_for_shifts = [0.4999, 0.4999, 0.08] # [0.4999, 0.4999, 0.08]
    # pixelsize in X,Y in um
    pixelsize = 0.1

    ### Simplotopes spline parameters
    layers_dimension = [2, 1]
    polynomial_degrees = [5, 5]
    n_mesh_cubes = [2, 2, 2]
    continuity_orders = [1, 1]
    partitions = [1, 1]
    use_matlab_async = False
    ############################################################################

    # creating calibration stacks
    print( 'Generating subpixel shifts...' )
    subpixel_shifts = create_subpixel_shifts( range_for_shifts, N_calibr )
    print( f'{subpixel_shifts}' )

    print( '\nGenerating PSF stacks...' )
    psfs = create_PSF_stacks( N_calibr, n_z_slices, ROI_calibr,
                              pixelsize, z_range, subpixel_shifts )

    print( f'Saving PSF stacks into\n\t{psf_stacks_file}' )
    save_PSF_stacks( psf_stacks_file, psfs, z_range, subpixel_shifts )

    # fitting simplotope spline
    print( 'Simplotopes spline fitting...' )
    smpl_fitter = SimplotopeFitter(
        input_calibr_PSFs_file=psf_stacks_file,
        output_spline_file=fitted_spline_file,
        layers_dimension=layers_dimension,
        polynomial_degrees=polynomial_degrees,
        n_mesh_cubes=n_mesh_cubes,
        continuity_orders=continuity_orders,
        partitions=partitions,
        use_matlab_async=use_matlab_async
    )

    smpl_fitter.do_fitting()
    smpl_fitter.synchronize()

    data_pts_coords = smpl_fitter.input_coords
    data_pts_input_values = smpl_fitter.input_values
    print( 'Simplotopes spline fitting is finished' )

    # get approximated by spline values of data points
    print( '\nGetting spline values in data points...' )
    smpl_getter = SimplotopeValuesGetter(
        coordinates=data_pts_coords,
        spline_mat_file=fitted_spline_file,
        use_matlab_async=use_matlab_async
    )

    smpl_getter.calculate_values()
    data_smpl_values = smpl_getter.get_values()
    print( 'Spline values are received' )

    # compare the result
    print( '\nComparing real data and the spline approximation:' )
    comparing_the_results( data_pts_input_values, data_smpl_values )

    # print .tif file
    print_simplotope_tif(
        tif_filename=tif_simplotope_file,
        ROI_calibr=ROI_calibr,
        n_z_slices=n_z_slices,
        z_range=z_range,
        fitted_spline_file=fitted_spline_file,
        use_matlab_async=use_matlab_async
    )

    print( '\nTest is finished!' )
