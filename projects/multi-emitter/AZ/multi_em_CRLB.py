# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 16:26:24 2020

@author: Owner
"""


from photonpy.cpp.context import Context
import numpy as np
import matplotlib.pyplot as plt
import photonpy.cpp.multi_emitter as multi_emitter
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
from photonpy.cpp.simflux import SIMFLUX
import photonpy.cpp.com as com
from photonpy.cpp.estim_queue import EstimQueue




# Simflux modulation patterns
mod = np.array([
     [0, 1.8, 0, 0.95, 0, 1/6],
     [1.9, 0, 0, 0.95, 0, 1/6],
     [0, 1.8, 0, 0.95, 2*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, 1.8, 0, 0.95, 4*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 4*np.pi/3, 1/6]
])



def generate_positions(N,E,K,roisize,bg,I):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 4
    for k in range(E):
        if K==3: # 2D case
            pos = np.random.uniform([border,border,0.9*I],[roisize-1-border,roisize-1-border,1.1*I],size=(N,K))
        if K==4: # 3D case
            pos = np.random.uniform([border,border,0,0.9*I],[roisize-border-1,roisize-border-1,0,1.1*I],size=(N,K))

        pts[:,np.arange(1,K+1)+k*K] = pos

    return pts

def generate_constpositions(N,E,roisize,bg,I):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    K = 3
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 3
    for k in range(E):
        if k ==0:
            pos = np.random.uniform([roisize//2,roisize//2,0.9*I],[roisize//2,roisize//2,1.1*I],size=(N,K))
        if k ==1:
            pos = np.random.uniform([border,border,0.9*I],[border+1,border+1,1.1*I],size=(N,K))
        if k ==2:
            pos = np.random.uniform([roisize-border,border,0.9*I],[roisize-border,border+2,1.1*I],size=(N,K))
        if k ==3:
            pos = np.random.uniform([border,roisize-border,0.9*I],[border+1,roisize-border+1,1.1*I],size=(N,K))
        if k ==4:
            pos = np.random.uniform([roisize-border,roisize-border,0.9*I],[roisize-border+1,roisize-border+1,1.1*I],size=(N,K))

        pts[:,np.arange(1,3+1)+k*3] = pos

    return pts

def generate_2emitdist(N,E,roisize,bg,I,dist):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    K = 3
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 4
    for k in range(E):
        if k ==0:
            pos = np.random.uniform([border,roisize//2,0.9*I],[border,roisize//2,1.1*I],size=(N,K))
        if k ==1:
            pos = np.random.uniform([border+dist,roisize//2,0.9*I],[border+dist,roisize//2,1.1*I],size=(N,K))
        
        

        pts[:,np.arange(1,3+1)+k*3] = pos

    return pts

def result_to_str(result):
    K = 3
    E = len(result)//K
    xpos = result[1::K]
    ypos = result[2::K]
    I = result[3::K]  # in 3D it would be 4::K
        
    prev = np.get_printoptions()['precision']
    np.set_printoptions(precision=1)
    s = f"{E} emitters: X={xpos}, Y={ypos}, I={I}"
    np.set_printoptions(precision=prev)
    return s


def plot_traces(traces, K):
    numE = traces.shape[1]//K

    fig,axes=plt.subplots(K, 1, sharex=True)

    def plot_param(idx, name):
        ax=axes[idx-1]
        numit = traces.shape[0]
        for e in range(numE):
            ax.plot(np.arange(numit), traces[:,e*K+idx], label=f'{name}{idx}')
        ax.set_title(f'Trace for {name} - {numE} emitters')
        ax.legend()
        ax.set_ylabel(name)
    
    plot_param(1, 'X')
    plot_param(2, 'Y')
    if K==3:    
        plot_param(3, 'I')
    else:
        plot_param(3,'Z')
        plot_param(4,'I')
    
    axes[-1].set_xlabel('Iterations')
    
def compute_error(A, B, K):
    # Make an array of [numemitters, K]. 
    spotsA = A[1:].reshape((-1,K))
    spotsB = B[1:].reshape((-1,K))
    
    # Compute all distances from spots A to B
    sqerr = (spotsA[:,None] - spotsB[None,:])**2
    dist = np.sqrt(np.sum(sqerr[:,:,[0,1]], -1))
    
    # Rearrange the spots based on shortest XY distance
    matching = np.argmin(dist,0)
    return spotsA[matching] - spotsB




def MFA_simflux(N,I,E,bg,roisize,dist,usesf=True,plot=False):
    with Context(debugMode=False) as ctx:
        
        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
        sf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, mod, roisize, len(mod), True)
        K = psf.NumParams()-1
    

        if usesf:     #simflux?
            psf = sf
            bg /= len(mod) # in simflux convention, background photons are spread over all frames
        
        border = 3
        minParam = [border, border, 200, 2]
        maxParam = [roisize-1-border,roisize-1-border, 4000, 20]
    
        # Create a list of estimator objects, one for each number of emitters
        max_emitters = E
        estimators = [multi_emitter.CreateEstimator(psf, k, ctx, minParam, maxParam) for k in range(1+max_emitters)]
        for e in estimators:
            e.SetLevMarParams(1e-19, 100) # Lambda, max number of iterations
            
        # true_pos = generate_positions(N, E, K, roisize, bg,I)
        true_pos = generate_2emitdist(N,E,roisize,bg,I,dist)
        smp = estimators[E].GenerateSample(true_pos)
        # CRLB = estimators[E].CRLB(true_pos)
        
        if plot == True:
            plt.figure()
            if usesf: 
                plt.imshow(np.concatenate(smp[0],-1))
            else:
                plt.imshow(smp[0])
        
     
        com_estim = com.CreateEstimator(roisize,ctx)
        
        emittercount = np.ones(N,dtype=np.int32)
        totresults = []
        tot_init = []
        # if plot == True:
        #         plt.figure()
        #         plt.imshow(np.hstack(smp))
       
        for roi in range(N): # go through all ROIs
            
    
            accepted = False
            momidx = 0
            momidx2 = 0
            while accepted == False:
                roi_smp = smp[roi]
                tot_initroi = []
                
        
                # First state is only background
                current = [np.mean(roi_smp)]
                current_ev = np.ones(psf.sampleshape) * current[0] # times avg background
                
                numEmitters = 0
                
                for c in range(1, max_emitters+1):
 
                    
                    moments_pos = list(np.linspace(1,1.5,25))
                    moments_neg = list(np.linspace(0.99,0.55,20))
                    moments = moments_pos + moments_neg + moments_pos + moments_neg
                    moments2 = moments_pos + moments_neg + moments_neg + moments_pos
                    mom = moments[momidx]
                    mom2 = moments2[momidx2]
                    residual = np.maximum(0, roi_smp - current_ev)
                                
                    residual_com = psf.Estimate([residual])[0][0]
                    
                    img = np.concatenate([residual, roi_smp],-1)
                    
                 
                    # init = init_est_peakval(np.sum(residual,0))
                    
                    
                    # if sf == psf:
                    #     plt.imshow(np.sum(img, 0))
                    # else:
                    #     plt.imshow(img)
                    # plt.scatter(current[1::K], current[2::K], label=f'Current state',marker='X', s=50,color='b')
                    # plt.scatter(roisize+true_pos[roi,1::K], true_pos[roi,2::K], label=f'True positions',s=50, color='k')
                    # # plt.scatter([residual_com[0]], [residual_com[1]], label=f'Added emitter',marker='x', color='r')
                    # plt.scatter([init[0]], [init[1]], label=f'Added emitter',marker='x', color='r')
                    # plt.title(f"Residual to initialize {c} emitters")
                    # plt.legend()
        
                    if K==4: # 3D Case
                        current = [*current, residual_com[0], residual_com[1], 0, residual_com[2]]
                    else:   # 2D Case
                        current = [*current, residual_com[0]*mom, residual_com[1]*mom2, residual_com[2]*mom]
                        # current = [*current, init[0], init[1], init[2]]
        
                    print(f"Initial estimate for {c} emitters: {residual_com}")
                    print(f"mom: {mom},{mom2}")
                    current = np.array(current)
        
                    results_, _, traces = estimators[c].Estimate([roi_smp], initial=[current])
                    results = results_[0]
                            
                    current_ev = estimators[c].ExpectedValue([results])[0]
                    # CRLB = estimators[c].CRLB([true_pos[0]])[0]
        
                    
                    chisqrd = (roi_smp-current_ev)**2 / (current_ev+1e-9)
                    
                    
                    chisq = np.sum( (roi_smp-current_ev)**2 / (current_ev+1e-9))
                    std_chisq = np.sqrt(2*psf.samplecount + np.sum(1/np.mean(current_ev)))
                
                    # If chi-square < expected value + 2 * std.ev, 
                    # then the current estimate is a good representation of the sample
                    chisq_threshold = psf.samplecount + 2*std_chisq
                    
                    accepted = chisq < chisq_threshold+25
                    
                    print(f"chisq: {chisq:.1f} < threshold({chisq_threshold:.1f}): {accepted}")
                    
                    if c == max_emitters and accepted == False:
                        momidx +=1
                        momidx2 +=1
                        c = 1
                        if momidx == len(moments)  or momidx2 == len(moments2):
                            momidx = 0
                            momidx2 = 1
                        
                    img = np.concatenate([current_ev, roi_smp],-1)
                    if plot == True:
        
                        if sf == psf:
                            fig,ax=plt.subplots(1)
                            ax.imshow(np.sum(img, 0))
                            scax = ax
                        else:
                            plt.figure()
                            plt.imshow(img)
                            scax = plt.gca()
                        scax.set_title(f'In the roi #{roi+1} - {c} emitters: Chi-sq: {chisq:.1f}')
                        scax.scatter(roisize+current[1::K], current[2::K], label=f'Previous + Residual',color='b',marker='x', s=100)
                        scax.scatter(true_pos[roi,1::K], true_pos[roi,2::K], label=f'True positions',color='k', s=100)
                        # scax.scatter(residual_com[0],residual_com[1],color='g',marker='x', s=100, label = f'Residual Initial')
                        scax.scatter(results[1::K], results[2::K], marker= 'x', s=100, label=f'Estimate for {c} emitters',color='r')
                        scax.legend(loc='upper center', bbox_to_anchor=(0.5,-0.1),fancybox=True, shadow=True, ncol=3)
                        # plt.pause(0.5)
        
                    current = results
                    tot_initroi.append(residual_com[:2])
                    
                   
                    numEmitters += 1
                    
                
                    if accepted:
                        break
                
                

                
            
            tot_init.append(tot_initroi)
            totresults.append(results)
            
            
            print(f'Final result: {result_to_str(results)}')
            print(f'True: {result_to_str(true_pos[roi])}')
            print(f'Intensity = {I}')
    
            # if E==numEmitters:
            #     err=compute_error(results, true_pos[roi],K)
            #     print(err)
            # TOTPOS,TD,err,error = error_comp(totresults,true_pos)
        TOTPOS,TD,err,error=error_comp(totresults,true_pos)
        var = variance_calc(err,error)
    
    return totresults,true_pos,error,var
            
        
    
#%%    
def emitters_found(result,ground_true):
    K = 3
    E = len(result)//K
    xpos_res = result[1::K]
    ypos_res = result[2::K]
    xpos_true = ground_true[1::K]
    ypos_true = ground_true[2::K]
    
    res = [xpos_res,ypos_res]
    gt = [xpos_true,ypos_true]
    
    res = np.vstack(res).T
    GT = np.vstack(gt).T
    RES = ROW_sort(res)
    GT = ROW_sort(GT)
    

    return RES, GT 

def ROW_sort(array):
    if not isinstance(array,list):
        array=list(array)
        
    isSorted = False
    while isSorted is False:
        isSorted = True
        for i in range(len(array)-1):
            if (array[i][0])>(array[i+1][0]):
                array[i+1], array[i] = array[i], array[i+1] 
                isSorted = False
    return array

def PN_count(results,true,TP=0,FP=0,FN=0):
    rho = sigma/2  # for now
    totpos = []
    truedetec = []
    while TP+FP+FN < len(true):
        if len(results) == len(true):
            totpos.append(results)
            truedetec.append(true)
            totpos=totpos[0]
            truedetec=truedetec[0]
            for i in range(len(results)):
                
                diff = max(abs(results[i]-true[i]))
                if diff < rho:
                    TP +=1
                else:
                    FP +=1
        elif len(results) < len(true):
            FN +=len(true)-len(results)

            for i in range(len(results)):
                min_dif = float('inf')
                for j in range(len(true)):
                    new_min = min(max(abs(results[i]-true[j])),min_dif)
                    if new_min < min_dif:
                        k =j
                    min_dif = new_min
                
                if min_dif < rho:
                    TP +=1
                    totpos.append(results[i])
                    truedetec.append(true[k])
                    true.pop(k)
                else:
                    FP +=1    
                    totpos.append(results[i])
                    truedetec.append(true[k])
        
            truedetec = np.vstack(truedetec)
            totpos = np.vstack(totpos)
    return TP,FP,FN, totpos,truedetec

def JAC_(results,true):
    tot_TP = 0
    tot_FP = 0
    tot_FN = 0
    N = len(results)
    
    for i in range(N):
        RES, GT = emitters_found(results[i],true[i])
        TP,FP,FN,totpos,td = PN_count(RES,GT)
        tot_TP +=TP
        tot_FP +=FP
        tot_FN +=FN
    
    P = tot_TP /(tot_FP+tot_TP)*100         # Positive prediction value
    R = tot_TP /(tot_FN+tot_TP)*100         # Sensitivity recall
    JAC = tot_TP /(tot_FN+tot_FP+tot_TP)*100    # Jaccard Index
    print(f"--> TP: {tot_TP}, FP: {tot_FP}, FN: {tot_FN}")
    print(f"--> Positive prediction value: {P}\n--> Recall reate: {R} \n--> Jaccard Index: {JAC}")
    
    return P,R,JAC

def error_comp(results,true):
    TOTPOS = []             # [#roi][#emit][xy]
    TD = []                 # [#roi][#emit][xy]
    err = []                # Diff in detected and actual position
    for i in range(N):
        RES, GT = emitters_found(results[i],true[i])
        TP,FP,FN,totpos,td = PN_count(RES,GT)
        err.append(np.sqrt((np.sum((np.array(totpos)-np.array(td))**2))/len(totpos)))
        TOTPOS.append(totpos)
        TD.append(td)
    
    error = sum(err)/N
    return TOTPOS,TD,err,error

def variance_calc(results,loc):
    var = sum((xi - loc)**2 for xi in results) / (len(results)-1)
    return np.sqrt(var)

# Based on max intensity
def init_est_peakval(image):
    estimate = np.unravel_index(np.argmax(image, axis=None), image.shape)
    maxint = np.max(image)
    estimate = [estimate[1],estimate[0],maxint]
    
    return estimate
    

#%%
intensity = np.linspace(100,3500,10)
# intensity = [500,1000]
sigma = 1.8 # a fixed Gaussian spot width of 119 nm
roisize = 20
E = 2
N = 20 # Rois
bg = 10
max_emit = 6

#%%
# with Context(debugMode=False) as ctx:
#     useSimflux = True
       
#     psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
#     sf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, mod, roisize, len(mod), True)
#     K = psf.NumParams()-1
   

#     if useSimflux:     #simflux?
#         psf = sf
#         bg /= len(mod) # in simflux convention, background photons are spread over all frames
   
#     border = 3
#     minParam = [border, border, 200, 2]
#     maxParam = [roisize-1-border,roisize-1-border, 4000, 20]
   
#     # Create a list of estimator objects, one for each number of emitters
#     max_emitters = E
#     estimators = [multi_emitter.CreateEstimator(psf, k, ctx, minParam, maxParam) for k in range(1+max_emitters)]
#     for e in estimators:
#         e.SetLevMarParams(1e-19, 100) # Lambda, max number of iterations
#     CRLB_tot=np.zeros((E,N,len(intensity)))
#     CRLB_1 = []
#     CRLB_2 = []
#     CRLB_3 = []
#     CRLB_4 = []
    
            
    
            
#     true_pos = generate_constpositions(N,2,roisize,bg,1000)
#     smp = estimators[2].GenerateSample(true_pos)
    
#     roi_smp = smp[0]
#     tot_initroi = []
    
#     # First state is only background
#     current = [np.mean(roi_smp)]
#     current_ev = np.ones(psf.sampleshape) * current[0]
#     residual = np.maximum(0, roi_smp - current_ev)
                            
#     residual_com = psf.Estimate([residual])[0][0]
    
#     #%%
#     for e in range(1,E+1):
#         for i in range(len(intensity)):
#             # true_pos = generate_positions(N, e, K, roisize, bg,intensity[i])
#             true_pos = generate_constpositions(N,e,roisize,bg,intensity[i])
#             smp = estimators[e].GenerateSample(true_pos)
#             CRLB = estimators[e].CRLB(true_pos)
#             CRLB = np.array(CRLB)
#             if e==1:
#                 CRLB_1.append(CRLB[:,1])
               
#             elif e==2:
#                 CRLB_2.append((CRLB[:,1]+CRLB[:,4])/2)
               
#             elif e==3:
#                 CRLB_3.append((CRLB[:,1]+CRLB[:,4]+CRLB[:,7])/3)
               
#             elif e==4:
#                 CRLB_4.append((CRLB[:,1]+CRLB[:,4]+CRLB[:,7]+CRLB[:,10])/4)
               
               
# CRLB_1 = np.array(CRLB_1)          
# CRLB_2 = np.array(CRLB_2)
# CRLB_3 = np.array(CRLB_3)
# CRLB_4 = np.array(CRLB_4)

# crlb_1 = np.sum(CRLB_1,1)/len(CRLB_1)
# crlb_2 = np.sum(CRLB_2,1)/len(CRLB_2)
# crlb_3 = np.sum(CRLB_3,1)/len(CRLB_3)
# crlb_4 = np.sum(CRLB_4,1)/len(CRLB_4)
               
               
        
#%%
# fig= plt.figure(figsize=(9, 8))

# # plt.figure()
# plt.plot(intensity,crlb_1*100,'r-',label= '1 Emitter')
# plt.plot(intensity,crlb_2*100,'g-',label= '2 Emitters')
# plt.plot(intensity,crlb_3*100,'b-',label= '3 Emitters')
# plt.plot(intensity,crlb_4*100,'c-',label= '4 Emitters')

# plt.legend()
# plt.yscale('log')
# # plt.xscale('log')
# plt.title('CRLB comparison')
# plt.xlabel('Intensity [Photons]')
# plt.ylabel('Localization Precision [nm]')

# sub_axes = plt.axes([.40, .55, .25, .25]) 
# lft = 24
# right = 26
# # sub_axes.plot(intensity[lft:right], crlb_1[lft:right]*100, c = 'r')
# sub_axes.plot(intensity[lft:right], crlb_2[lft:right]*100, c = 'g')
# sub_axes.plot(intensity[lft:right], crlb_3[lft:right]*100, c = 'b')
# sub_axes.plot(intensity[lft:right], crlb_4[lft:right]*100, c = 'c')

#%% Accuracy dist


#%% dist
# for d in range(len(dist)):
#     totresults,true_pos,error,varerr=MFA_simflux(N,1000,E,bg,roisize,dist[d],True)
#     errtot.append(error)
#     varerror.append(varerr)
    

#%%
# dist = np.linspace(1,12,8)
dist = [1,2,3,4,5,7,8,10,12]
errtot = []
varerror = []
errtot_sf = []
varerror_sf = []
for d in range(len(dist)):
    totresults,true_pos,error,varerr=MFA_simflux(N,1000,E,bg,roisize,dist[d],usesf=False,plot=False)
    errtot.append(error)
    varerror.append(varerr)
    totresults,true_pos,error,varerr=MFA_simflux(N,1000,E,bg,roisize,dist[d],usesf=True,plot=False)
    errtot_sf.append(error)
    varerror_sf.append(varerr)



#%%
# totresults,true_pos,error,varerr=MFA_simflux(N,1000,E,bg,roisize,3,usesf=True,plot=True)
plt.figure()
plt.plot(dist,errtot, 'r-*',label= 'Without Simflux')
plt.plot(dist,errtot_sf, 'g-*',label= 'Simflux')
plt.title('Two emitters at varoius separation distance')
plt.xlabel('Distance [Pixel]')
plt.ylabel('Localization Precision [Pixel]')
plt.legend()
plt.grid()
# print(errtot)
                    
                
                
                

