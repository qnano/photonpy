# -*- coding: utf-8 -*-
"""
Created on Thu Apr  16 12:26:24 2020

@author: Owner
"""


from photonpy.cpp.context import Context
import numpy as np
import matplotlib.pyplot as plt
import photonpy.cpp.multi_emitter as multi_emitter
import photonpy.cpp.spotdetect as spotdetect
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
import photonpy.smlm.util as smlmutil
from photonpy.cpp.simflux import SIMFLUX
import photonpy.cpp.com as com

import math
import time
import tqdm
import random
import Support as sup

useSimflux = False

# Simflux modulation patterns
mod = np.array([
     [0, 1.8, 0, 0.95, 0, 1/6],
     [1.9, 0, 0, 0.95, 0, 1/6],
     [0, 1.8, 0, 0.95, 2*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, 1.8, 0, 0.95, 4*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 4*np.pi/3, 1/6]
])



def generate_positions(N,E,K,roisize,bg):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 4
    for k in range(E):
        if K==3: # 2D case
            pos = np.random.uniform([border,border,400],[roisize-1-border,roisize-1-border,600],size=(N,K))
        if K==4: # 3D case
            pos = np.random.uniform([border,border,0,400],[roisize-border-1,roisize-border-1,0,600],size=(N,K))

        pts[:,np.arange(1,K+1)+k*K] = pos

    return pts

def result_to_str(result):
    K = 3
    E = len(result)//K
    xpos = result[1::K]
    ypos = result[2::K]
    I = result[3::K]  # in 3D it would be 4::K
        
    prev = np.get_printoptions()['precision']
    np.set_printoptions(precision=1)
    s = f"{E} emitters: X={xpos}, Y={ypos}, I={I}"
    np.set_printoptions(precision=prev)
    return s


def plot_traces(traces, K):
    numE = traces.shape[1]//K

    fig,axes=plt.subplots(K, 1, sharex=True)

    def plot_param(idx, name):
        ax=axes[idx-1]
        numit = traces.shape[0]
        for e in range(numE):
            ax.plot(np.arange(numit), traces[:,e*K+idx], label=f'{name}{idx}')
        ax.set_title(f'Trace for {name} - {numE} emitters')
        ax.legend()
        ax.set_ylabel(name)
    
    plot_param(1, 'X')
    plot_param(2, 'Y')
    if K==3:    
        plot_param(3, 'I')
    else:
        plot_param(3,'Z')
        plot_param(4,'I')
    
    axes[-1].set_xlabel('Iterations')
    
def compute_error(A, B, K):
    # Make an array of [numemitters, K]. 
    spotsA = A[1:].reshape((-1,K))
    spotsB = B[1:].reshape((-1,K))
    
    # Compute all distances from spots A to B
    sqerr = (spotsA[:,None] - spotsB[None,:])**2
    dist = np.sqrt(np.sum(sqerr[:,:,[0,1]], -1))
    
    # Rearrange the spots based on shortest XY distance
    matching = np.argmin(dist,0)
    return spotsA[matching] - spotsB

# def reord_results(Results):
#     pass

    
    

sigma = 1.8 # a fixed Gaussian spot width of 119 nm
roisize = 20
E = 2
# N = 1 # Rois

#%%
def generate_storm_movie(gaussian, emitterList, numframes=100, imgsize=512, intensity=500, bg=2, sigma=1.5, p_on=0.1):
    frames = np.zeros((numframes, imgsize, imgsize), dtype=np.uint16)
    emitters = np.array([[e[0], e[1], sigma, sigma, intensity] for e in emitterList])

    on_counts = np.zeros(numframes, dtype=np.int32)

    for f in range(numframes):
        frame = bg * np.ones((imgsize, imgsize), dtype=np.float32)
        frame_emitters = emitters * 1
        on = np.random.binomial(1, p_on, len(emitters))
        frame_emitters[:, 4] *= on

        frame = gaussian.Draw(frame, frame_emitters)
        frames[f] = frame
        on_counts[f] = np.sum(on)

    return frames, on_counts,emitters



psfSigma = 1.8
w = 64
roisize = 20
# par = [roisize, psfSigma, pixelsize]

N = 8  # Total emitters in frame
numframes = 1
R = np.random.normal(0, 0.2, size=N) + int(w * 0.25)
angle = np.random.uniform(0, 2 * math.pi, N)
emitters = np.vstack((R * np.cos(angle) + w / 2, R * np.sin(angle) + w / 2)).T

#%%
with Context(debugMode=False) as ctx:
    gaussian = Gaussian(ctx)
    
    # True and observerd frame
    mov_true, on_counts,pos= generate_storm_movie(gaussian, emitters, numframes, imgsize=w, sigma=psfSigma, p_on=1) # default = 10/N
    mov_p = np.random.poisson(mov_true)
    
    plt.imshow(mov_p[0])
    
    
    psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
    sf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, mod, roisize, len(mod), True)
    K = psf.NumParams()-1
    
    bg = 10
    if useSimflux:     #simflux?
        psf = sf
        bg /= len(mod) # in simflux convention, background photons are spread over all frames

    
    
    
    # Detecting Roi's
    spotDetector = spotdetect.SpotDetector(psfSigma, roisize, minIntensity=1.4)
    print(f"Maxfiltersize: {spotDetector.maxFilterSize}")
    spotDetector.maxFilterSize = int((3*(2*psfSigma)+1)/1.5) # adjust to detect spots closer together (default int(sigma*5))

    processFrame = spotdetect.SpotDetectionMethods(ctx).ProcessFrame
    
    numspots=0
    t0 = time.time()
    allrois = []
    allcornerpos = []
    for f in range(len(mov_p)):
        rois, cornerpos, scores, spotz= processFrame(mov_p[f], spotDetector,roisize=roisize,maxSpotsPerFrame= 100)
        plt.imshow(mov_p[f])
        plt.title(f'Frame {f}')
        plt.colorbar()
        smlmutil.imshow_hstack(rois)
        plt.title(f'{len(rois)} rois detected in Frame {f}')
        plt.pause(1e-5)                                              ## util
        
        allrois.append(rois)
        allcornerpos.append(cornerpos)
        
    allrois= np.concatenate(allrois)
    allcornerpos= np.concatenate(allcornerpos)

    t1 = time.time()
    print(f"Time: {t1-t0} s. {len(mov_p)/(t1-t0)} fps. numspots={len(allrois)}")

    
    psf = gaussian.CreatePSF_XYIBg(roisize, psfSigma, True)                     ## Gaussian
    
    results_, diag, traces = psf.Estimate(allrois)
    
    results = results_.copy()
    # ROI coordinates use python indexing (y,x)
    results[:,0] += results_[:,0]+ allcornerpos[:,1]
    results[:,1] += results_[:,1] + allcornerpos[:,0]
    plt.imshow(mov_p[0])
    # for i in range(len(results)):
    #     sup.draw_roi(results[i,:2],roisize,1, 'r--')
 
    border = 3
    minParam = [border, border, 200, 2]
    maxParam = [roisize-1-border,roisize-1-border, 4000, 20]
    
    

    # Create a list of estimator objects, one for each number of emitters
    max_emitters = 4
    estimators = [multi_emitter.CreateEstimator(psf, k, ctx, minParam, maxParam) for k in range(1+max_emitters)]
    for e in estimators:
        e.SetLevMarParams(1e-19, 100) # Lambda, max number of iterations
    R = len(allrois)  
    # true_pos = generate_positions(R, E, K, roisize, bg)
    # smp = estimators[E].GenerateSample(true_pos)  # Produces simflux patterns

    # plt.figure()
    # plt.imshow(np.concatenate(smp[0],-1))

    com_estim = com.CreateEstimator(roisize,ctx)
    
    emittercount = np.ones(N,dtype=np.int32)
    totresults = []
    tot_init = []

            # Number of ROIs
    for roi in range(R):    # go through all ROIs

        roi_smp = allrois[roi]
        tot_initroi = []
        

        # First state is only background
        current = [np.mean(roi_smp)]
        current_ev = np.ones(psf.sampleshape) * current[0] # times avg background
        
        numEmitters = 0
        # plt.figure()
        # plt.imshow(np.hstack(roi_smp))
 
        for c in range(1, max_emitters+1):
            residual = np.maximum(0, roi_smp - current_ev)
                        
            residual_com = psf.Estimate([residual])[0][0]
            # plt.figure()
            
            img = np.concatenate([residual, roi_smp],-1)
            # if sf == psf:
            #     plt.imshow(np.sum(img, 0))
            # else:
            #     plt.imshow(img)
            # plt.scatter(current[1::K], current[2::K], label=f'Current state',marker='X', s=50,color='b')
            # plt.scatter(roisize+true_pos[roi,1::K], true_pos[roi,2::K], label=f'True positions',s=50, color='k')
            # plt.scatter([residual_com[0]], [residual_com[1]], label=f'Added emitter',marker='x', color='r')
            # plt.title(f"Residual to initialize {c} emitters")
            # plt.legend()

            if K==4: # 3D Case
                current = [*current, residual_com[0], residual_com[1], 0, residual_com[2]]
            else:   # 2D Case
                current = [*current, residual_com[0], residual_com[1], residual_com[2]]

            print(f"Initial estimate for {c} emitters: {residual_com}")
            current = np.array(current)

            results_, _, traces = estimators[c].Estimate([roi_smp], initial=[current])
            results = results_[0]
     
            

            # if True:
            #     plot_traces(traces[0], K)
                    
            current_ev = estimators[c].ExpectedValue([results])[0]

            
            chisqrd = (roi_smp-current_ev)**2 / (current_ev+1e-9)
            chisq = np.sum( (roi_smp-current_ev)**2 / (current_ev+1e-9))
            std_chisq = np.sqrt(2*psf.samplecount + np.sum(1/np.mean(current_ev)))
        
            # If chi-square > expected value + 2 * std.ev, 
            # then the current estimate is a good representation of the sample
            chisq_threshold = psf.samplecount + 2*std_chisq
            
            accepted = chisq < chisq_threshold
            print(f"chisq: {chisq:.1f} < threshold({chisq_threshold:.1f}): {accepted}")         
            img = np.concatenate([current_ev, roi_smp],-1)
            if sf == psf:
                fig,ax=plt.subplots(1)
                ax.imshow(np.sum(img, 0))
                scax = ax
            else:
                plt.figure()
                plt.imshow(img)
                scax = plt.gca()
            scax.set_title(f'In the roi #{roi+1} - {c} emitters: Chi-sq: {chisq:.1f}')
            scax.scatter(roisize+current[1::K], current[2::K], label=f'Previous + Residual',color='b',marker='x', s=100)
            # scax.scatter(true_pos[roi,1::K], true_pos[roi,2::K], label=f'True positions',color='k', s=100)
            # scax.scatter(residual_com[0],residual_com[1],color='g',marker='x', s=100, label = f'Residual Initial')
            scax.scatter(results[1::K], results[2::K], marker= 'x', s=100, label=f'Estimate for {c} emitters',color='r')
            scax.legend(loc='upper center', bbox_to_anchor=(0.5,-0.1),fancybox=True, shadow=True, ncol=3)
            # plt.pause(0.5)

            current = results
            tot_initroi.append(residual_com[:2])
            numEmitters += 1
            
            
            if accepted:
                break
        
        tot_init.append(tot_initroi)
        totresults.append(results)
        
        
        # print(f'Final result: {result_to_str(results)}')
        # print(f'True: {result_to_str(true_pos[roi])}')

        # if E==numEmitters:
        #     err=compute_error(results, true_pos[roi],K)
        #     print(err)
            
        
        
            
    #%%
    
    
    def fromroi_toframe(results,cornerpoints):
        num_emit = int(len(results)/3)
        pts = np.zeros((num_emit,2))
        for i in  range(num_emit):
            pts[i,0] = results[1+3*i] + allcornerpos[:,1]
            pts[i,1] = results[2+3*i] + allcornerpos[:,0]
        return pts
            
    points = fromroi_toframe(totresults[0],allcornerpos)
    def emitters_found(result,ground_true):
        K = 3
        E = len(result)//K
        xpos_res = result[1::K]
        ypos_res = result[2::K]
        xpos_true = ground_true[1::K]
        ypos_true = ground_true[2::K]
        
        res = [xpos_res,ypos_res]
        gt = [xpos_true,ypos_true]
        
        res = np.vstack(res).T
        GT = np.vstack(gt).T
        RES = ROW_sort(res)
        GT = ROW_sort(GT)
        

        return RES, GT 
    
    def ROW_sort(array):
        if not isinstance(array,list):
            array=list(array)
            
        isSorted = False
        while isSorted is False:
            isSorted = True
            for i in range(len(array)-1):
                if (array[i][0])>(array[i+1][0]):
                    array[i+1], array[i] = array[i], array[i+1] 
                    isSorted = False
        return array

    def PN_count(results,true,TP=0,FP=0,FN=0):
        rho = sigma/2  # for now
        totpos = []
        truedetec = []
        while TP+FP+FN < len(true):
            if len(results) == len(true):
                totpos.append(results)
                truedetec.append(true)
                totpos=totpos[0]
                truedetec=truedetec[0]
                for i in range(len(results)):
                    
                    diff = max(abs(results[i]-true[i]))
                    if diff < rho:
                        TP +=1
                    else:
                        FP +=1
            elif len(results) < len(true):
                FN +=len(true)-len(results)

                for i in range(len(results)):
                    min_dif = float('inf')
                    for j in range(len(true)):
                        new_min = min(max(abs(results[i]-true[j])),min_dif)
                        if new_min < min_dif:
                            k =j
                        min_dif = new_min
                    
                    if min_dif < rho:
                        TP +=1
                        totpos.append(results[i])
                        truedetec.append(true[k])
                        true.pop(k)
                    else:
                        FP +=1    
                        totpos.append(results[i])
                        truedetec.append(true[k])
            
                truedetec = np.vstack(truedetec)
                totpos = np.vstack(totpos)
        return TP,FP,FN, totpos,truedetec
    
    def JAC_(results,true):
        tot_TP = 0
        tot_FP = 0
        tot_FN = 0
        
        for i in range(N):
            RES, GT = emitters_found(results[i],true[i])
            TP,FP,FN,totpos,td = PN_count(RES,GT)
            tot_TP +=TP
            tot_FP +=FP
            tot_FN +=FN
        
        P = tot_TP /(tot_FP+tot_TP)         # Positive prediction value
        R = tot_TP /(tot_FN+tot_TP)         # Sensitivity recall
        JAC = tot_TP /(tot_FN+tot_FP+tot_TP)    # Jaccard Index
        print(f"--> TP: {tot_TP}, FP: {tot_FP}, FN: {tot_FN}")
        print(f"--> Positive prediction value: {P}\n--> Recall reate: {R} \n--> Jaccard Index: {JAC}")
        
        return P,R,JAC
    
    def error_comp(results,true):
        TOTPOS = []             # [#roi][#emit][xy]
        TD = []                 # [#roi][#emit][xy]
        err = []                # Diff in detected and actual position
        for i in range(N):
            RES, GT = emitters_found(results[i],true[i])
            TP,FP,FN,totpos,td = PN_count(RES,GT)
            err.append(np.sqrt((np.sum((np.array(totpos)-np.array(td))**2))/len(totpos)))
            TOTPOS.append(totpos)
            TD.append(td)
        
        error = sum(err)/N
        return TOTPOS,TD,err,error
    
        
    
    # JAC_(totresults,true_pos)
    # totpos,truedetec,err,error = error_comp(totresults,true_pos)
    # print(f"The average distance error over {N} rois is: {error}")
    
    # #%%
    # smpsum = np.sum(roi_smp,0)
    # A = np.sum(smpsum)/(400)
    # print(A)
        
    


        
                        
                        
                    
                    
                
                
                

