# -*- coding: utf-8 -*-
"""
 Main 
"""

#%%
from photonpy.cpp.context import Context
import numpy as np
import matplotlib.pyplot as plt
import photonpy.cpp.multi_emitter as multi_emitter
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
from photonpy.cpp.simflux import SIMFLUX
from photonpy.cpp.estimator import Estimator
from photonpy.cpp.estim_queue import EstimQueue,EstimQueue_Results
import photonpy.cpp.com as com
from typing import Callable
import tqdm
import time

#%%
def addaxes(x, numaxes):
    """ Add np.newaxis axes: addaxes(x,2) returns x[:,None,None] """
    return x[tuple((np.s_[:], *((None,)*numaxes)))]

class MultiEmitterProcessor:
    """
    Takes in sample images (small ROIs) and a known PSF, 
    and runs MLE on the samples with various initial values and emitter counts.
    All estimations results and chi-square values are stored for later selection.
    """
    def __init__(self, psf:Estimator, maxEmitters=6):
        self.psf = psf
        self.estimators = []
        self.dims = psf.NumParams() - 2  # Number of dimensions (2 or 3)
        self.emK = self.dims+1 # Number of parameters per emitter
        self.smpaxes = tuple(1+np.arange(len(psf.sampleshape)))
                
        self.ctx = self.psf.ctx
        
        self.batchSize = 256
        
        # Multi-emitter fitting seems to be much less stable.
        # Setting limits for each parameter is key
        border = 3
        roisize = psf.sampleshape[-1]
        minParam = [border, border, 200, 2]
        maxParam = [roisize-1-border,roisize-1-border, 4000, 20]
        
        for k in range(maxEmitters):
            numEmitters = k+1
            estim = multi_emitter.CreateEstimator(psf, numEmitters, self.ctx, minParam, maxParam)
            estim.SetLevMarParams(1e-16, 50) # Lambda, max number of iterations
            self.estimators.append(estim)
            
    def GetEstimator(self, emitterCount) -> Estimator:
        assert emitterCount>0
        return self.estimators[emitterCount-1]
    
    def AddEmitterToEstim(self, rois, numEmitters, prevEstim=None):
        # Compute residual
        if numEmitters == 1:
            # Background
            prevEstim = np.mean(rois,axis=self.smpaxes)[:,None]
            residual = np.ones(rois.shape)*np.mean(rois, axis=self.smpaxes, keepdims=True)
        else:
            prev_estim_expval = self.GetEstimator(numEmitters-1).ExpectedValue(prevEstim)
            residual = np.maximum(0, rois - prev_estim_expval)
            
        # Run regular single emitter localization on the residual
        singleEmEstim = self.psf.Estimate(residual)[0][:,:self.emK]
        
        #print(singleEmEstim)

        # Build a new initial estimate with an extra emitter
        estim = np.zeros((len(rois), 1+numEmitters*self.emK))
        estim[:,:prevEstim.shape[1]] = prevEstim
        estim[:,prevEstim.shape[1]:] = singleEmEstim

        return estim        
    
    def ProcessROIs(self, rois, ids,momidx):
        """
        - Send rois into 1 emitter estimator
        - Loop:
            If any queue has results finished, process results (ie, send to next queue or stop)
            If any queue has a queue length < batchsize, flush
        """
        prevEstim = None
        result_list = []
        
        # Momentum to espace local minima
        moments_pos = list(np.linspace(1,1.5,25))
        moments_neg = list(np.linspace(0.99,0.55,25))
        moments = moments_pos + moments_neg + moments_pos + moments_neg
        mom  = moments[momidx]

        for i in range(len(self.estimators)):
            e = self.estimators[i]
            
            with EstimQueue(e) as q, tqdm.tqdm(total=len(rois)) as pb:
                estim = self.AddEmitterToEstim(rois, i+1, prevEstim)*mom
                q.Schedule(rois, ids=ids, initial=estim, constants=rois*0)
                q.Flush() # Make sure its not waiting for more stuff to process

                pb.set_description(f"Fitting to {i+1}-emitter model")

                lastcount = 0
                while True:
                    rc = q.GetResultCount()
                    pb.update(rc - lastcount)
                    
                    if rc == len(rois):
                        break
                    
                    lastcount = rc
                    time.sleep(0.1)
                
                results = q.GetResults()
            prevEstim = results.estim
            result_list.append(results)
        
        return result_list

            
    
    def Destroy(self):            
        for e in self.estimators:
            e.Destroy()
            
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.Destroy()        



def generate_positions(N,E,K,roisize,bg):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 4
    for k in range(E):
        if K==3: # 2D case
            pos = np.random.uniform([border,border,800],[roisize-1-border,roisize-1-border,1000],size=(N,K))
        if K==4: # 3D case
            pos = np.random.uniform([border,border,0,400],[roisize-border-1,roisize-border-1,0,600],size=(N,K))

        pts[:,np.arange(1,K+1)+k*K] = pos

    return pts

def generate_constpositions(N,E,K,roisize,bg,I,dist):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 3
    K = 3
    for k in range(E):
        if K==3: # 2D case
            if k ==0:
                pos = np.random.uniform([border,roisize/2,0.9*I],[border,roisize/2,1.1*I],size=(N,K))
            if k ==1:
                pos = np.random.uniform([border+dist,roisize/2,0.9*I],[border+dist,roisize/2,1.1*I],size=(N,K))
        if K==4: # 3D case
            pos = np.random.uniform([border,border,0,400],[roisize-border-1,roisize-border-1,0,600],size=(N,K))

        pts[:,np.arange(1,K+1)+k*K] = pos

    return pts



def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)


def generate_test_data(psf, numEmitters, numROIs, bg,I,dist):
    
    # ground_truth = generate_positions(numROIs, numEmitters, 3, psf.sampleshape[-1], bg)
    ground_truth = generate_constpositions(numROIs, numEmitters, 3, psf.sampleshape[-1], bg,I,dist)
    
    with multi_emitter.CreateEstimator(psf, numEmitters) as me_estim:
        rois = me_estim.GenerateSample(ground_truth)
        
    return rois, ground_truth



def plot_result(estim, roi, expval_estim, gt):
    
    if len(roi.shape)==3: # For simflux, we sum all patterns
        roi = np.sum(roi,0)
        expval_estim = np.sum(expval_estim,0)
        expval_true = sum(mep.estimators[-1].ExpectedValue(ground_truth[0])[0],0)
    
    emK = 3 #X,Y,I
    plt.figure()
    plt.imshow(np.concatenate([roi,expval_estim],-1))
    plt.scatter(gt[1::emK],gt[2::emK],label=f'Ground truth', s=100,color='k')
    plt.scatter(estim[1::emK],estim[2::emK],label=f'Estimate',marker='x', s=100,color='r')
    
 
#%% Metrics

def emitters_found(result,ground_true):
    K = 3
    E = len(result)//K
    xpos_res = result[1::K]
    ypos_res = result[2::K]
    xpos_true = ground_true[1::K]
    ypos_true = ground_true[2::K]
    
    res = [xpos_res,ypos_res]
    gt = [xpos_true,ypos_true]
    
    res = np.vstack(res).T
    GT = np.vstack(gt).T
    RES = ROW_sort(res)
    GT = ROW_sort(GT)
    return RES, GT 

def ROW_sort(array):
    if not isinstance(array,list):
        array=list(array)
        
    isSorted = False
    while isSorted is False:
        isSorted = True
        for i in range(len(array)-1):
            if (array[i][0])>(array[i+1][0]):
                array[i+1], array[i] = array[i], array[i+1] 
                isSorted = False
    return array

def PN_count(results,true,TP=0,FP=0,FN=0):
    rho = sigma/2  # for now
    totpos = []
    truedetec = []
    while TP+FP+FN < len(true):
        if len(results) == len(true):
            totpos.append(results)
            truedetec.append(true)
            totpos=totpos[0]
            truedetec=truedetec[0]
            for i in range(len(results)):
                
                diff = max(abs(results[i]-true[i]))
                if diff < rho:
                    TP +=1
                else:
                    FP +=1
        elif len(results) < len(true):
            FN +=len(true)-len(results)

            for i in range(len(results)):
                min_dif = float('inf')
                for j in range(len(true)):
                    new_min = min(max(abs(results[i]-true[j])),min_dif)
                    if new_min < min_dif:
                        k =j
                    min_dif = new_min
                
                if min_dif < rho:
                    TP +=1
                    totpos.append(results[i])
                    truedetec.append(true[k])
                    true.pop(k)
                else:
                    FP +=1    
                    totpos.append(results[i])
                    truedetec.append(true[k])
        
            truedetec = np.vstack(truedetec)
            totpos = np.vstack(totpos)
    return TP,FP,FN, totpos,truedetec

def JAC_(results,true):
    tot_TP = 0
    tot_FP = 0
    tot_FN = 0
    N = len(results)
    
    for i in range(N):
        RES, GT = emitters_found(results[i],true[i])
        TP,FP,FN,totpos,td = PN_count(RES,GT)
        tot_TP +=TP
        tot_FP +=FP
        tot_FN +=FN
    
    P = tot_TP /(tot_FP+tot_TP)*100         # Positive prediction value
    R = tot_TP /(tot_FN+tot_TP)*100         # Sensitivity recall
    JAC = tot_TP /(tot_FN+tot_FP+tot_TP)*100    # Jaccard Index
    print(f"--> TP: {tot_TP}, FP: {tot_FP}, FN: {tot_FN}")
    print(f"--> Positive prediction value: {P}\n--> Recall reate: {R} \n--> Jaccard Index: {JAC}")
    
    return P,R,JAC

def reOrd(results,true):
    TOTPOS = []             # [#roi][#emit][xy]
    TD = []                 # [#roi][#emit][xy]
    for i in range(N):
        RES, GT = emitters_found(results[i],true[i])
        TP,FP,FN,totpos,td = PN_count(RES,GT)
        TOTPOS.append(totpos)
        TD.append(td)
 
    return TOTPOS,TD

def error_compfilt(results,true):
    import numpy as np
    # TOTPOS = []             # [#roi][#emit][xy]
    # TD = []                 # [#roi][#emit][xy]
    err = []                # Diff in detected and actual position
    N = len(true)
    for i in range(N):
        val = np.sqrt((np.sum((np.array(results[i])-np.array(true[i]))**2))/len(results[i]))
        if np.isnan(val):
            continue
        err.append(val)
    

    error = sum(err)/len(err)

    return err,error

def variance_calc(results,loc):
    var = sum((xi - loc)**2 for xi in results) / (len(results)-1)
    return np.sqrt(var)

def filter_(true,totpos):
        true = np.array(true)
        totpos = np.array(totpos)
        err_or = abs(true-totpos)
        filtered = []
        tot_true = []
        

        for i in range(len(true)):
            filt_roi = []
            found_true = []
            
            for j in range(len(true[0])):
                if sum(err_or[i][j])/2 < 1.5:
                    filt_roi.append(totpos[i][j])
                    found_true.append(true[i][j])                   
            tot_true.append(found_true)
            filtered.append(filt_roi)
                    
        return filtered,tot_true
#%%
def MFA_q(psf,E,rois,gound_truth):
    accepted = False
    momidx = 0

    with MultiEmitterProcessor(psf, E) as mep:

        # the order of the spots gets changed during processing, 
        # so a list of identifiers is kept to keep track of them
        
        while accepted == False:
            ids = np.arange(len(rois))
            r = mep.ProcessROIs(rois, ids,momidx)
            results = r[-1].estim
            
            expval = mep.estimators[-1].ExpectedValue(r[-1].estim)
            for j in range(len(rois)):
                chisqrd = np.sum((rois[j]-expval[j])**2 / (expval[j]+1e-9))
                std_chisq = np.sqrt(2*psf.samplecount + np.sum(1/np.mean(expval[j])))
                # If chi-square < expected value + 3 * std.ev, 
                # then the current estimate is a good representation of the sample
                chisq_threshold = psf.samplecount + 3*std_chisq
                
                accepted = chisqrd < chisq_threshold
                if accepted:
                    TOTRES.append(results[j])
                if accepted == False:
                    momidx +=1
                    print(f"momentum {momidx}")
                    rois = rois[j:]
                    break

    # Errors
    TOTPOS,TD=reOrd(TOTRES,ground_truth)
    filt_r,ft = filter_(TOTPOS,TD)
    err,error=error_compfilt(filt_r,ft)
    var=variance_calc(err,error)
    return error,var
    
##% Simflux modulation patterns
mod = np.array([
     [0, 1.8, 0, 0.95, 0, 1/6],
     [1.9, 0, 0, 0.95, 0, 1/6],
     [0, 1.8, 0, 0.95, 2*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, 1.8, 0, 0.95, 4*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 4*np.pi/3, 1/6]
])


# if __name__ == "__main__":
with Context(debugMode=False) as ctx:    
    sigma = 1.8
    roisize = 20
    E = 2
    pixelsize=0.1
    N = 10
    bg = 2
    I = 1000
    
    TOTRES = []
    toterr = []
    totvar = []
    
    dist = np.arrange(1,5,4)  # Seperations distance (in pixels) between two emitters
    # psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
    psf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, mod, roisize, len(mod), True)
    psf.SetLevMarParams(1e-16, 50)
    
    # --> De issue is dus by een seperation distance van  bijv 5 zoals hieronder, komt er een
    # kleinere value uit, dan als ik een array van distance maak en daarover evalueer
    for d in dist:  
        rois, ground_truth = generate_test_data(psf, E, N, bg,I,5)
        Error,var = MFA_q(psf,E,rois,ground_truth)
        toterr.append(Error)
    
    # totvar.append(Var)
    print(f"Total error:  {toterr}")


    #%%

        
    

            