# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 23:05:01 2020

@author: Ahmed Zahran
"""

#%% ---- Imports
import numpy as np
import matplotlib.pyplot as plt
from photonpy.cpp.gaussian import Gaussian
from photonpy.cpp.context import Context
import photonpy.cpp.spotdetect as spotdetect
import photonpy.smlm.util as smlmutil
import photonpy
import math
import time
import tqdm
import random


##-
# import Emitters as emit
# import LM_algorithm as lma
# import Support as sup
# import Regions_ident as km

#%%%% -----------[ Parameters ]-----------
# intensity = np.arange(100,2000,50)    
intensity = np.linspace(100,2000,5)                 # Intensity range
I = 1000                                           # Intensity used
bg = 10                                           # Background intensity
# roisize = 15                                      # Size region of interrest
sigma = 2                                           # PSF blob width
K = 40                                              # grid size
# roi = 20
D = 30                                              # samples per each emitter
pixelsize = 100                                    # [nm]
M = 10                                               # Number of measurements
Region = int(3*(2*sigma)+1)                         # Side size, region of interrest
E = 1


#%% Theta
def generate_storm_movie(gaussian, emitterList, numframes=100, imgsize=512, intensity=500, bg=2, sigma=1.5, p_on=0.1):
    frames = np.zeros((numframes, imgsize, imgsize), dtype=np.uint16)
    emitters = np.array([[e[0], e[1], sigma, sigma, intensity] for e in emitterList])

    on_counts = np.zeros(numframes, dtype=np.int32)

    for f in range(numframes):
        frame = bg * np.ones((imgsize, imgsize), dtype=np.float32)
        frame_emitters = emitters * 1
        on = np.random.binomial(1, p_on, len(emitters))
        frame_emitters[:, 4] *= on

        frame = gaussian.Draw(frame, frame_emitters)
        frames[f] = frame
        on_counts[f] = np.sum(on)

    return frames, on_counts



psfSigma = 1.8
w = 64
roisize = int(w/2.0)
par = [roisize, psfSigma, pixelsize]

N = 8
numframes = 1
R = np.random.normal(0, 0.2, size=N) + int(w * 0.25)
angle = np.random.uniform(0, 2 * math.pi, N)
emitters = np.vstack((R * np.cos(angle) + w / 2, R * np.sin(angle) + w / 2)).T

def clustering(img,th,plot=False):
    treshold = np.median(img)*th
    est = []
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i,j] > treshold:
                if (i and j >= 3) and (i and j <= len(img)-3): 
                    est.append([j,i])
                    
    estimates = est              
    WSS = []
    for i in range(1,20):
        KMEAN = km.Kmean_cluster(i)
        X_std = np.array(estimates)
        KMEAN.compute_kmean(X_std)
        centroids = KMEAN.centroids
   
        WSS.append(KMEAN.error) 
    ratio = []
    for i in range(len(WSS)-1):
        ratio.append(WSS[i+1]/WSS[i])
        if ratio[i] > 0.55:
            K = i+1 # Optimal number of emitters
            print(f"found {i+1}")
            break
    
    KMEAN = km.Kmean_cluster(K)
    X_std = np.array(estimates)
    KMEAN.compute_kmean(X_std)
    centroids = KMEAN.centroids    
    if plot == True:
        fig, ax = plt.subplots(figsize=(6, 6))
    
        plt.imshow(img)
        for k in range(len(centroids)):
            plt.scatter(X_std[KMEAN.labels == k, 0], X_std[KMEAN.labels == k, 1],
                    c=colormap[k], label=f'Region {k+1}',s=25)
            plt.scatter(centroids[k][0],centroids[k][1],c=colormap[k],marker='o',edgecolor='k',s=100,label=f'Centroid Region {k+1}')
      
    return centroids

#%%
with Context() as ctx:
    gaussian = Gaussian(ctx)
    mov_true, on_counts= generate_storm_movie(gaussian, emitters, numframes, imgsize=w, sigma=psfSigma, p_on=1) # default = 10/N
    mov_p = np.random.poisson(mov_true)
    
    spotDetector = spotdetect.SpotDetector(psfSigma, roisize, minIntensity=1.4)
    print(f"Maxfiltersize: {spotDetector.maxFilterSize}")
    spotDetector.maxFilterSize = int((3*(2*psfSigma)+1)/2) # adjust to detect spots closer together (default int(sigma*5))

    processFrame = spotdetect.SpotDetectionMethods(ctx).ProcessFrame

    numspots=0
    t0 = time.time()
    allrois = []
    allcornerpos = []
    for f in range(len(mov_p)):
        rois, cornerpos, scores, spotz= processFrame(mov_p[f], spotDetector,roisize=roisize,maxSpotsPerFrame= 100)
        plt.imshow(mov_p[f])
        plt.title(f'Frame {f}')
        plt.colorbar()
        smlmutil.imshow_hstack(rois)
        plt.title(f'{len(rois)} rois detected in Frame {f}')
        plt.pause(1e-5)                                              ## util
        
        allrois.append(rois)
        allcornerpos.append(cornerpos)
        
    allrois= np.concatenate(allrois)
    allcornerpos= np.concatenate(allcornerpos)

    t1 = time.time()
    print(f"Time: {t1-t0} s. {len(mov_p)/(t1-t0)} fps. numspots={len(allrois)}")

    
    psf = gaussian.CreatePSF_XYIBg(roisize, psfSigma, True)                     ## Gaussian
    
    results, diag, traces = psf.Estimate(allrois)
    
    
    # ROI coordinates use python indexing (y,x)
    results[:,0] += allcornerpos[:,1]
    results[:,1] += allcornerpos[:,0]
    plt.imshow(mov_p[0])
    # for i in range(len(results)):
    #     sup.draw_roi(results[i,:2],roisize,1, 'r--')

    
    print(f"Fitted results ({len(results)}):")
    print(results)
    
    # plt.figure()
    # plt.scatter(emitters[:,0],emitters[:,1],s=5,label='True Positions',color='g')
    # plt.scatter(results[:,0],results[:,1],s=20,label='Fitted coordinates',color='r')
 
    # plt.title('Fitted coordinates and True positions (X,Y)')
    # plt.legend()

#%%
# Clustering
colormap = {0:'r', 1: 'g',2:'b',3:'m',4:'k',5:'w',6:'y'}
with Context() as ctx:
    gaussian = Gaussian(ctx)   

    def MFA(image):
        image_true = image
        count = 0
        emittot = []
        threshold = 5
        # centroids = clustering(image,threshold,True)
        # print(centroids[0])
        # for i in range(len(centroids)):
        #         sup.draw_roi(centroids[i],roisize/3,1, 'w--')
       
    
        while np.max(image)> np.median(image_true)*11 and count <4:
            
            for i in range(len(rois)):
                clustering(rois[i],threshold)
            com = sup.CoM_estimate(image)
            theta_init = np.array([[com[0],com[1],400,0]])
            # theta_init = np.array([[centroids[i][0],centroids[i][1],500,0]])
        
            # plt.figure()
            # plt.imshow(image)
            # plt.scatter(theta_init[0][0],theta_init[0][1],marker='X',color='r',s=200,label='Initial estimate')
            num_iter = 1000
            fin,mu_,xmle_tot,iters = lma.LM_MLE_Ibg(gaussian,theta_init,num_iter,image, [roisize, psfSigma, pixelsize])
            emittot.append(fin[0][:2])
            resi = image-mu_[0]
            resi[resi<0]=0
            image = resi
            count += 1
        print(f"The emitter count in this roi is: {count}")
        Etot = []
        for i in emittot:
                if i[0] < image.shape[0] and i[1] < image.shape[1]:
                    Etot.append(i)
        
        return Etot,fin
    
    emittot = []
    for i in range(len(rois)):
        res,fin = MFA(rois[i])
        emittot.append(np.array(res))

    #%%

Results = np.array(emittot)
# ROI coordinates use python indexing (y,x)
for i in range(len(rois)):
    Results[i][:,0] += allcornerpos[:,1][i]
    Results[i][:,1] += allcornerpos[:,0][i]
Results = np.vstack(Results)
plt.figure()
plt.imshow(mov_p[0])
plt.scatter(emitters[0][0],emitters[0][1],marker='X',color='k',s=100,label='Ground True Positions')
plt.scatter(Results[0][0],Results[0][1],marker='X',color='r',s=75,label ='LM estimations')  
for j in range(len(emitters)):
    plt.scatter(emitters[j][0],emitters[j][1],marker='X',color='k',s=100)
for i in range(len(Results)):
    plt.scatter(Results[i][0],Results[i][1],marker='X',color='r',s=75)
  
plt.legend()
    #%% Test convergence with LM -- Works
# with Context() as ctx:
    # gaussian = Gaussian(ctx)
    
    # test = gaussian.CreatePSF_XYIBg(roisize, psfSigma, True)
    
    # theta = np.array([[roisize/4,roisize/4,500,3]])
    # dmu_,mu_ = test.Derivatives(theta) 
    # smp_ = np.random.poisson(mu_)

    # num_iter = 1000
    
    # com = sup.CoM_estimate(smp_[0])
    # theta_init = np.array([[com[0],com[1],400,0]])
    # plt.figure()
    # plt.imshow(smp_[0])
    # plt.scatter(theta_init[0][0],theta_init[0][1],marker='X',color='r',s=200,label='Initial estimate')
    # fin,mu,xmle_tot,iters = lma.LM_MLE_Ibg(gaussian,theta_init,num_iter,smp_[0], par)
    # plt.scatter(fin[0][0],fin[0][1],marker='X',color='g',s=150,label='LM estimate')
    # plt.legend()
    
            

    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

