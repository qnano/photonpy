# -*- coding: utf-8 -*-

import numpy as np


def uniform_sphere(R, shape):
    phi = np.random.uniform(0,np.pi*2,shape)
    costheta = np.random.uniform(-1,1,shape)
    u = np.random.uniform(0,1,shape)
    
    theta = np.arccos(costheta)
    r = R * u ** (1/3)

    if type(shape) == int:
        shape = [shape]
        
    xyz = np.zeros((*shape,3))
    xyz[...,0] = r * np.sin(theta) * np.cos(phi)
    xyz[...,1] = r * np.sin(theta) * np.sin(phi)
    xyz[...,2] = r * np.cos(theta)
        
    return xyz 

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    xyz = uniform_sphere(10, 500)
    plt.figure()
    plt.scatter(xyz[:,0],xyz[:,1])
    plt.figure()
    plt.scatter(xyz[:,1],xyz[:,2])
    
    