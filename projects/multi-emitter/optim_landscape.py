# -*- coding: utf-8 -*-

from photonpy.simflux import SIMFLUX
from photonpy import Context

import numpy as np
import matplotlib.pyplot as plt

roisize = 30
sigma = 1.9

k = 1.8
pitch = 2*np.pi/k
mod = np.array([
     [0, k, 0, 0.95, 0, 1/6],
     [k, 0, 0, 0.95, 0, 1/6],
     [0, k, 0, 0.95, 2*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, k, 0, 0.95, 4*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 4*np.pi/3, 1/6]
])

num_patterns = len(mod)

with Context() as ctx:
    psf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, num_patterns, roisize, num_patterns)
    
    gt = [roisize/2,roisize/2,1000,1]
    smp = psf.GenerateSample([gt],constants=[mod])
    plt.imshow(np.concatenate(smp[0],-1))
    
    # generate a grid of positions to evaluate likelihood at
    W = 100
    crange = np.linspace(roisize/2-pitch*2,roisize/2+pitch*2,W)
    X,Y = np.meshgrid(crange,crange)

    evalpos = np.zeros((W*W, 4))
    evalpos[:,0] = X.flatten()
    evalpos[:,1] = Y.flatten()
    evalpos[:,2] = gt[2]
    evalpos[:,3] = gt[3]

    ev = psf.ExpectedValue(evalpos, constants=mod.flatten()[None])
    ll = np.sum(np.log(ev)*smp - ev, (1,2,3))
    ll = ll.reshape((W,W))
    plt.figure()
    plt.imshow(ll)

    plt.figure()
    plt.plot(crange,ll[len(ll)//3])
