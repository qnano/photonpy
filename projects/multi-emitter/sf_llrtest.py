# Likelihood ratio test of 2 approaching emitters vs 1 for SIMFLUX vs non-SIMFLUX
# 
# Generate N samples of 2 emitters moving apart
# Compute the likelihood of the 2 emitters vs likelihood of one in middle
# 
from photonpy import Context,GaussianPSFMethods
from photonpy.cpp.simflux import SIMFLUX
from photonpy.smlm.util import imshow_hstack
import numpy as np
import matplotlib.pyplot as plt


from photonpy.utils.ui.show_image import array_view

# Simflux modulation patterns
k = 1.8
mod = np.array([
     [0, k, 0, 0.95, 0, 1/6],
     [k, 0, 0, 0.95, 0, 1/6],
     [0, k, 0, 0.95, 2*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, k, 0, 0.95, 4*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 4*np.pi/3, 1/6]
])

#mod[:,3] = 0

roisize = 20
psf_sigma = 1.8

def logl(smp,mu):
    return smp*np.log(mu)-mu

with Context() as ctx:
    
    psf = GaussianPSFMethods(ctx).CreatePSF_XYIBg(roisize, psf_sigma, cuda=True)
        
    sf_psf = SIMFLUX(ctx).CreateEstimator_Gauss2D(psf_sigma, len(mod), roisize, len(mod))
    
    intensity = 1000
    bg = 10
    
    def g2d_eval(pos):
        return psf.ExpectedValue(pos)
    
    def sf_eval(pos):
        pos_=pos*1
        pos_[:,3] /= len(mod)
        ev = sf_psf.ExpectedValue(pos_,constants=mod.flatten()[None])
        return np.concatenate(ev.transpose((1,0,2,3)),-1)
    

    N = 500
    R = 6

    evalfn = [g2d_eval, sf_eval]
    
    dist = np.linspace(0,R,N)
    
    llr = np.zeros((2, N))
    ll_se = np.zeros((2, N))
    ll_me = np.zeros((2, N))

    labels = ['SMLM', 'SIMFLUX']

    import tifffile
    for m in range(2):
        # Compute the double emitter expected value
        pos = np.zeros((N, 4))
        pos[:,1] = roisize/2
        pos[:,0] = roisize/2 + dist/2
        pos[:,2] = intensity
        pos[:,3] = 0
        expval_me = evalfn[m](pos)
        pos[:,0] = roisize/2 - dist/2
        expval_me += evalfn[m](pos)
        expval_me += bg

        # Sample is based on the double emitter    
        smp = np.random.poisson(expval_me)

        plt.figure()
        plt.imshow(smp[N//2])
        array_view(smp)
        tifffile.imsave(f'method-{labels[m]}.tif', smp)

        # Single emitter expected value to compare against
        pos[:,0] = roisize/2
        pos[:,2] = intensity*2
        expval_se = evalfn[m](pos) + bg
        
        ll_se[m] = logl(smp, expval_se).sum((1,2))
        ll_me[m] = logl(smp, expval_me).sum((1,2))
        
        plt.figure()
        plt.plot(dist, ll_se[m,:], label = 'LL single emitter')
        plt.plot(dist, ll_me[m,:], label = 'LL double emitter')
        plt.xlabel('Emitter distance')
        plt.legend()
        plt.title(labels[m])

        llr[m] = ll_me[m]-ll_se[m]
        
    plt.figure()
    plt.plot(dist, llr[0,:], label='SMLM')
    plt.plot(dist, llr[1,:], label='SIMFLUX')
    plt.legend()
    plt.xlabel('Seperation distance')
    plt.ylabel('LLR two vs one emitter')
    
    
    