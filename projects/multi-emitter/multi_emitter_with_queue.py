# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 13:34:35 2020

@author: jcnossen1
"""


from photonpy.cpp.context import Context
import numpy as np
import matplotlib.pyplot as plt
import photonpy.cpp.multi_emitter as multi_emitter
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
from photonpy.cpp.simflux import SIMFLUX
from photonpy.cpp.estimator import Estimator
from photonpy.cpp.estim_queue import EstimQueue,EstimQueue_Results
import photonpy.cpp.com as com
from typing import Callable
import tqdm
import time

def addaxes(x, numaxes):
    """ Add np.newaxis axes: addaxes(x,2) returns x[:,None,None] """
    return x[tuple((np.s_[:], *((None,)*numaxes)))]

class MultiEmitterProcessor:
    """
    Takes in sample images (small ROIs) and a known PSF, 
    and runs MLE on the samples with various initial values and emitter counts.
    All estimations results and chi-square values are stored for later selection.
    """
    def __init__(self, psf:Estimator, mod, maxEmitters=6):
        self.psf = psf
        self.estimators = []
        self.dims = psf.NumParams() - 2  # Number of dimensions (2 or 3)
        self.emK = self.dims+1 # Number of parameters per emitter
        self.smpaxes = tuple(1+np.arange(len(psf.sampleshape)))
        self.mod = mod
        
        if psf.NumConstants()>0:
            self.mod_const = mod.flatten()[None]
        else:
            self.mod_const = None
                
        self.ctx = self.psf.ctx
        
        self.batchSize = 256
        
        # Multi-emitter fitting seems to be much less stable.
        # Setting limits for each parameter is key
        border = 3
        roisize = psf.sampleshape[-1]
        minParam = [border, border, 200, 2]
        maxParam = [roisize-1-border,roisize-1-border, 4000, 20]
        
        for k in range(maxEmitters):
            numEmitters = k+1
            estim = multi_emitter.CreateEstimator(psf, numEmitters, self.ctx, minParam, maxParam)
            estim.SetLevMarParams(1e-16, 40) # Lambda, max number of iterations
            self.estimators.append(estim)
            
    def GetEstimator(self, emitterCount) -> Estimator:
        assert emitterCount>0
        return self.estimators[emitterCount-1]
    
    def AddEmitterToEstim(self, rois, numEmitters, prevEstim=None):
        # Compute residual
        if numEmitters == 1:
            # Background
            prevEstim = np.mean(rois,axis=self.smpaxes)[:,None]
            residual = np.ones(rois.shape)*np.mean(rois, axis=self.smpaxes, keepdims=True)
        else:
            prev_estim_expval = self.GetEstimator(numEmitters-1).ExpectedValue(prevEstim,constants=self.mod_const)
            residual = np.maximum(0, rois - prev_estim_expval)
            
        # Run regular single emitter localization on the residual
        singleEmEstim = self.psf.Estimate(residual, constants=self.mod_const)[0][:,:self.emK]
        
        #print(singleEmEstim)

        # Build a new initial estimate with an extra emitter
        estim = np.zeros((len(rois), 1+numEmitters*self.emK))
        estim[:,:prevEstim.shape[1]] = prevEstim
        estim[:,prevEstim.shape[1]:] = singleEmEstim

        return estim        
    
    def ProcessROIs(self, rois, ids):
        prevEstim = None
        result_list = []

        for i in range(len(self.estimators)):
            e = self.estimators[i]
            
            with EstimQueue(e) as q, tqdm.tqdm(total=len(rois)) as pb:
                estim = self.AddEmitterToEstim(rois, i+1, prevEstim)
                q.Schedule(rois, ids=ids, initial=estim, constants=self.mod_const)
                q.Flush() # Make sure its not waiting for more stuff to process

                pb.set_description(f"Fitting to {i+1}-emitter model")

                lastcount = 0
                while True:
                    rc = q.GetResultCount()
                    pb.update(rc - lastcount)
                    
                    if rc == len(rois):
                        break
                    
                    lastcount = rc
                    # GetResultCount() will temporarily block C++ processing threads, so don't constantly call it
                    time.sleep(0.1)
                
                results = q.GetResults()
                results.SortByID(isUnique=True)
            prevEstim = results.estim
            result_list.append(results)
        
        return result_list

            
    
    def Destroy(self):            
        for e in self.estimators:
            e.Destroy()
            
    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.Destroy()        



def generate_positions(N,E,K,roisize,bg):
    """
    Generate a parameter vector with either 2D or 3D layout:
    2D: background, X0, Y0, I0, X1, Y1, I1, ....
    3D: background, X0, Y0, Z0, I0, X1, Y1, Z1, I1, ....
    
    N: Number of ROIs
    E: Number of emitters per ROI
    K: Number of parameters per emitter (3 for 2D or 4 for 3D)
    """
    pts = np.zeros((N,E*K+1))
    pts[:,0] = bg #bg
    border = 4
    for k in range(E):
        if K==3: # 2D case
            pos = np.random.uniform([border,border,500],[roisize-1-border,roisize-1-border,800],size=(N,K))
        if K==4: # 3D case
            pos = np.random.uniform([border,border,0,400],[roisize-border-1,roisize-border-1,0,600],size=(N,K))

        pts[:,np.arange(1,K+1)+k*K] = pos

    return pts



def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)



def generate_test_data(psf, mod, numEmitters, numROIs, bg):
    
    ground_truth = generate_positions(numROIs, numEmitters, 3, psf.sampleshape[-1], bg)
    
    with multi_emitter.CreateEstimator(psf, numEmitters) as me_estim:
        c = None
        if psf.NumConstants()>0:
            c = mod.flatten()[None]
            
        rois = me_estim.GenerateSample(ground_truth, constants=c)
        
    return rois, ground_truth


def plot_result(ax, estim, roi, expval_estim, gt):
    
    if len(roi.shape)==3: # For simflux, we sum all patterns
        roi = np.sum(roi,0)
        expval_estim = np.sum(expval_estim,0)
    
    emK = 3 #X,Y,I

    plt.figure()
    plt.imshow(np.concatenate([roi,expval_estim],-1))
    plt.scatter(gt[1::emK],gt[2::emK],label=f'Ground truth', s=50,color='b')
    plt.scatter(estim[1::emK],estim[2::emK],label=f'Estimate',marker='X', s=50,color='w')
    
    plt.legend()

    ax.imshow(np.concatenate([roi,expval_estim],-1), cmap='inferno')
    ax.scatter(estim[1::emK],estim[2::emK],label=f'Estimate',marker='X', s=50,color='w')
    ax.scatter(gt[1::emK],gt[2::emK],label=f'Ground truth', s=50,color='b')
    ax.legend()


# Simflux modulation patterns
mod = np.array([
     [0, 1.8, 0, 0.95, 0, 1/6],
     [1.9, 0, 0, 0.95, 0, 1/6],
     [0, 1.8, 0, 0.95, 2*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, 1.8, 0, 0.95, 4*np.pi/3, 1/6],
     [1.9, 0, 0, 0.95, 4*np.pi/3, 1/6]
])


if __name__ == "__main__":
    with Context(debugMode=False) as ctx:
        sigma = 1.8
        roisize = 20
        numEmitters = 3
        pixelsize=0.1
        N = 1
        bg = 2
        
        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
        #psf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, len(mod), roisize, True)

        psf.SetLevMarParams(1e-16, 30)

        rois, ground_truth = generate_test_data(psf, mod, numEmitters, N, bg)

        with MultiEmitterProcessor(psf, mod=mod, maxEmitters=3) as mep:

            # the order of the spots gets changed during processing, 
            # so a list of identifiers is kept to keep track of them
            ids = np.arange(len(rois))
            r = mep.ProcessROIs(rois, ids)

            # Plot a result
            expval = mep.estimators[-1].ExpectedValue(r[-1].estim,constants=mep.mod_const)
            for i in range(N):
                plt.figure()
                plot_result(plt.gca(),r[-1].estim[i], rois[i], expval[i], ground_truth[i])
            
            