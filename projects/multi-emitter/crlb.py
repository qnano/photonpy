
"""

Analysis of localizing highly overlapping sets of emiters with and without 2D simflux approach

"""


from photonpy.smlmlib.psf import PSF
from photonpy.smlmlib.gaussian import Gauss3D_Calibration,Gaussian


import matplotlib.pyplot as plt
import numpy as np
import time

from photonpy.smlmlib.context import Context
import photonpy.smlmlib.gaussian as gaussian
from photonpy.smlmlib.psf import PSF

import napari
from photonpy.smlmlib.cspline import CSpline_Calibration, CSpline


from random_uniform_sphere import uniform_sphere


def CreatePSF(ctx:Context,roisize):
    fn = "C:/data/beads/Tubulin-A647-cspline.mat"
    calib = CSpline_Calibration.from_file_nmeth(fn)
    
    psf = CSpline(ctx).CreatePSF_XYZIBg(roisize, calib, True)
    return psf


def SampleEmitters(N, R, minDist):
    """
    N: Number of molecules
    R: Radius of molecules
    """


def ComputeFisherMatrix(mu, jac):
    mu[mu<1e-9] = 1e-9
    K = len(jac)
    fi = np.zeros((K,K))
    for i in range(K):
        for j in range(K):            
            fi[i,j] = np.sum( 1/mu * (jac[i] * jac[j]))
            
    return fi
    print(fi)    
    return np.sqrt(np.linalg.inv(fi).diagonal())

def CombinedCRLB(psf: PSF, xyzI, bg):
    print(psf.ParamFormat())
    
    n = len(xyzI)
    theta=np.zeros((len(xyzI),5))
    theta[:,:4] = xyzI
    
    deriv, expval=psf.Derivatives(theta)
    print(deriv.shape)
    
    sumev = np.sum(expval,0)
    jac = np.ones((n*4+1,*psf.samplesize))
    for i in range(n):
        jac[i*4:(i+1)*4] = deriv[i,:4]
    
    return ComputeFisherMatrix(sumev,jac)

def FilterOnDistance(pts, mindist):
    which = np.sum((((xyzI[:,None,:,:3]-xyzI[:,:,None,:3])**2).sum(-1)< mindist**2), (1,2)) == pts.shape[1]
    return which

with Context() as ctx:
    roisize=12
    psf = CreatePSF(ctx,roisize)

    theta=np.zeros((500,5))
    theta[:,0:2] = roisize/2
    theta[:,2] = np.linspace(-2,2,len(theta))
    theta[:,3] = 1000
    theta[:,4] = 5
    
    #smp  =psf.GenerateSample(theta)
    #with napari.gui_qt():
    #    napari.view_image(smp)

    spotsInROI = 5
    numspots=20
    pixelsize=65
    R = 1000/pixelsize  # 1um / pixelsize

    xyzI = np.zeros((numspots, spotsInROI, 4))
    xyzI[...,:3] = uniform_sphere(R, (numspots,spotsInROI))
    xyzI[..., 3] = np.random.uniform(300, 800, size=(numspots,spotsInROI))
    
    minDist=20/pixelsize
    sel = FilterOnDistance(xyzI, minDist)
    xyzI = xyzI[sel]
    
    print(f"Positions: {xyzI}")

    crlb=[]
    for i in range(xyzI.shape[0]):
        fi = CombinedCRLB(psf, xyzI[i], 5)
        crlb.append (np.sqrt(np.linalg.inv(fi).diagonal()))
    
    
    
    
    
    