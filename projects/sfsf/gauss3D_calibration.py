

from photonpy.simflux import SimfluxProcessor
from photonpy import Context
import numpy as np
import os
import matplotlib.pyplot as plt
from photonpy.cpp.gaussian import GaussianPSFMethods, Gauss3D_Calibration
from photonpy import Context
import photonpy.cpp.spotdetect as spotdetect

import photonpy.smlm.util as smlmutil
import time
import tifffile
from scipy.optimize import least_squares

from scipy.optimize import curve_fit
import yaml

def make_summed(fn, imgs_per_plane=20):
    tif = tifffile.imread(fn)
    return tif.reshape((-1,imgs_per_plane,6,tif.shape[-2],tif.shape[-1]))


def calibrate_gaussian_3D(rois, zpos):    
    roisize = rois.shape[-1]
    with Context() as ctx:
        fitrois = rois.reshape((-1, *rois.shape[2:]))
        
        gpm = GaussianPSFMethods(ctx)
        psf = gpm.CreatePSF_XYIBgSigmaXY(roisize, 2, cuda=False)
        
        params = psf.Estimate(fitrois)[0]
        
        plt.figure()
        plt.plot(params[:,4:])
    
        def f(p, z):
            s0, gamma, d, A = p
            return s0 * np.sqrt(1 + (z - gamma)**2 / d**2 + A * (z - gamma)**3/d**2)
    
        def func(p, z, y):
            return f(p, z) - y
        
        sigma_xs = params[:,4]
        sigma_ys = params[:,5]
        
        bounds = ((1, -600, 0, 1e-5), (10, 600, 1e3, 1e-1))
        p0 = [2, 0, 3e2, 1e-5]
        p_x = least_squares(func, p0, loss='huber', bounds=bounds, args=(zpos, sigma_xs))
        p_y = least_squares(func, p0, loss='huber', bounds=bounds, args=(zpos, sigma_ys))
    
        print(p_x.x)
        print(p_y.x)
    
        plt.figure()
        plt.plot(zpos, sigma_xs, 'x', label='$\sigma_x$')
        plt.plot(zpos, sigma_ys, 'x', label='$\sigma_y$')
        plt.plot(zpos, f(p_x.x, zpos), '--', label='x fit')
        plt.plot(zpos, f(p_y.x, zpos), '--', label='y fit')
        #plt.ylim([0, 10])
        plt.legend()
        plt.show()
    
        calib = Gauss3D_Calibration(p_x.x, p_y.x)
    
        p = np.zeros((len(zpos), 5))
        p[:,[0,1,3,4]] = params[:,:4]
        p[:,2] = zpos
    
        psf_astig = gpm.CreatePSF_XYZIBg(rois.shape[-1], calib, cuda=False)
        ev = psf_astig.ExpectedValue(p)
        
        #show_napari(np.concatenate((fitrois, ev),-1))
        
        return calib


#path = 'C:/data/simflux/sim4_1/sim4_1.tif' #[1.885575  1.8277005]
#path = 'C:/data/sfsf/sf3D/gattabeads with steps/gatta_150_5.tif'

zstack_step = 50
#path = 'C:/data/sfsf/sf3D/period_cal_12-14/pos1/x0y0(518).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-16/pos1/x0y300(523).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-28/x0y0_6im_50nm_6(564).tif'
#path = 'C:/data/sfsf/sf3D/x0y0(533).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-14/pos2/x0y0(520).tif'

#path= 'C:/data/sfsf/sf3D/period_cal_12-17/pos2_agar/x0y0(531).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-17/pos1_agar_beads/x0y0(530).tif'
path = 'C:/data/sfsf/sf3D/period_cal_12-17/pos1_gattabeads/x0y0(534).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-17/pos5_gattabeads/x0y0(538).tif'
#path = 'C:/data/sfsf/sf3D/period_cal_12-17/pos2_gattabeads/x0y0(535).tif'

def show_napari(img):
    import napari
    with napari.gui_qt():
        viewer =napari.view_image(img)


#path = 'C:/data/sfsf/sf3D/period_cal_12-11/x0y0_1(512).tif'
mov = make_summed(path, 1)
mov = mov.mean((1,2))
tifffile.imsave(os.path.splitext(path)[0]+"_summed.tif", mov.astype(np.uint16))
#show_napari(mov)

roisize = 12

maxproj = np.max(mov, 0)

plt.figure()
plt.imshow(maxproj)

with Context(debugMode=False) as ctx:
    gaussian = GaussianPSFMethods(ctx)

    sdm = spotdetect.SpotDetectionMethods(ctx)
    spotDetector = spotdetect.SpotDetector(1.5, roisize, minIntensity=30)
   
    rois, cornerYX, scores, spotz = sdm.ProcessFrame(maxproj, spotDetector, roisize, maxSpotsPerFrame=200)
    
    cornerPos = np.zeros((len(rois), 3),dtype=int)
    cornerPos[:,1:] = cornerYX
    
    allrois = sdm.ExtractROIs(mov, [len(mov), roisize,roisize], cornerPos)
        
    #show_napari(allrois)
    
    sel_bead = 25 # high brightness one seems fit
    startplane = 6
    endplane = 39
    
    zstep = 0.05
    numsteps = allrois.shape[1]
    zpos = np.linspace(-zstep * numsteps//2, zstep * numsteps//2)
    calib = calibrate_gaussian_3D(allrois[[sel_bead]][:, startplane:endplane], zpos[startplane:endplane])
    calib.save(os.path.splitext(path)[0]+"_gausscalib.yaml")
