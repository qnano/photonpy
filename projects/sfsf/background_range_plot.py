# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 10:56:21 2020

@author: jelmer
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 12:10:20 2020

@author: jcnossen1
"""
from photonpy.cpp.simflux import SIMFLUX
import numpy as np
import matplotlib.pyplot as plt

from photonpy import Context
from photonpy import Gaussian

import tqdm

roisize = 40
sigma = 1.9

k = 1.8
period = 2*np.pi/k
mod = np.array([
     [0, k, 0, 0.9, 0, 1/6],
     [k, 0, 0, 0.9, 0, 1/6],
     [0, k, 0, 0.9, 2*np.pi/3, 1/6],
     [k, 0, 0, 0.9, 2*np.pi/3, 1/6],
     [0, k, 0, 0.9, 4*np.pi/3, 1/6],
     [k, 0, 0, 0.9, 4*np.pi/3, 1/6]
])

"""
mod = np.array([
    [-0.01284769,  1.7423911 ,  0.    , 0.9447144 ,  2.2674139 , 0.21993715],
     [-1.7920153 , -0.00334535,  0.        , 0.80955136,  0.03635875, 0.12363402],
       [-0.01284769,  1.7423911 ,  0.       , 0.7795346 , -1.3448972 , 0.202626],
       [-1.7920153 , -0.00334535,  0.        , 0.80133754,  2.1728442 , 0.12301771],
       [-0.01284769,  1.7423911 ,  0.        , 0.7805458 , -0.34536022, 0.21329196],
       [-1.7920153 , -0.00334535,  0.        , 0.8307313 , -2.060315  , 0.11749316]])
"""
backgrounds = np.logspace(0,np.log10(50),30)

framesList= [1, 2, 3, 6]
labels = ['Hexagon', 'Dual Triangles', 'Triple Pairs', 'SIMFLUX']

sepi_crlb = np.zeros((len(framesList),len(backgrounds),4))
modulated_crlb = np.zeros((len(framesList),len(backgrounds),4))


radius=9
offsets = np.zeros((6,2))
ang = np.linspace(0,2*np.pi, len(offsets), endpoint=False)
offsets[:,0] = radius*np.cos(ang)
offsets[:,1] = radius*np.sin(ang)

g_crlb = np.zeros((len(backgrounds),4))
ghbg_crlb=g_crlb*0

n = 200   
params = np.zeros((n,4))
params[:,:2] = roisize/2 + np.random.uniform(-period/2,period/2,size=(n,2))
params[:,2] = 1000

with Context() as ctx:
    psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
    sf_psf = SIMFLUX(ctx).CreateEstimator_Gauss2D(sigma, len(mod), roisize, len(mod))
    
    #g_fi = psf.FisherMatrix(params).mean(0)
    
    for i, bg in tqdm.tqdm(enumerate(backgrounds),total=len(backgrounds)):

        params[:,3] = bg
        
        # High background case
        params_hbg = params*1
        params_hbg[:,3] *= len(mod)
        ghbg_crlb[i] = psf.CRLB(params_hbg).mean(0)

        g_crlb[i] = psf.CRLB(params).mean(0)
        mod_params = np.repeat([mod.flatten()],n,0)

        for k,framesPerLoc in enumerate(framesList):
            patternsPerFrame = len(mod)//framesPerLoc
            #print(f"Patterns per frame: {patternsPerFrame}")
            
            params_cf = params*1
            params_cf[:,3] /= framesPerLoc
            with SIMFLUX(ctx).CreateCFSFEstimator(psf, offsets, patternsPerFrame, False) as sepi_psf:
                sepi_crlb[k,i] = sepi_psf.SumIntensities(sepi_psf.CRLB(sepi_psf.ExpandIntensities(params_cf))).mean(0)
    
            with SIMFLUX(ctx).CreateCFSFEstimator(psf, offsets, patternsPerFrame, True) as cf_psf:
                modulated_crlb[k,i] = cf_psf.CRLB(params_cf,constants=mod_params).mean(0)

plt.figure()

for k in range(len(framesList)):
    plt.semilogx(backgrounds,sepi_crlb[k,:,0], lw=2,label=f'{labels[k]} (seperately fitted intensities)')
    plt.semilogx(backgrounds,modulated_crlb[k,:,0],lw=2, label=f'{labels[k]} (modulated)')

#plt.axhline(y=g_crlb[0], linestyle='--', label='2D Gaussian')
#plt.axhline(y=ghbg_crlb[0], color='r', linestyle='--', label='2D Gaussian at 6x bg')
plt.legend()
plt.xlabel('Background [cumulative photons/pixel]')
plt.title(f'Hexagon CRLB vs Gaussian for Sigma={sigma:.2f} pixels')

plt.figure()
for k in range(len(framesList)):
    plt.semilogx(backgrounds,g_crlb[:,0]/modulated_crlb[k, :,0], label= f"{labels[k]} - bg divided over {framesList[k]} frames")
plt.title('Improvement factor of combined frame SIMFLUX vs 2D Gaussian')
plt.xlabel('Background [cumulative photons/pixel]')
plt.legend()

plt.figure()
for k in range(len(framesList)):
    plt.semilogx(backgrounds,sepi_crlb[k,:,0]/modulated_crlb[k,:,0],label=labels[k])
plt.title('Improvement factor modulation for the same shape')
plt.xlabel('Background [cumulative photons/pixel]')
plt.legend()

