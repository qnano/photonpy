from photonpy.simflux import SimfluxProcessor
from photonpy.cpp.context import Context
import numpy as np
import os

import tifffile

# Configuration for spot detection and 2D Gaussian fitting
cfg = {
    'sigma':[1.9,1.9],
   'roisize':20,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':28,
   'patternFrames': [[0,2,4],[1,3,5]],
   'gain': 2.2,
   'offset': 100,
   'pixelsize' : 65, # nm/pixel
   'startframe': 60,
   'maxframes': 0,
   
   'use3D': False,
   'wavelength': 640,
   'gauss3DCalib': 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/gausscalib.yaml'
}

path = 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/4_5ms.tif'

sp = SimfluxProcessor(path, cfg)

sp.detect_rois(chisq_threshold=10, ignore_cache=False)
#sp.estimate_sigma()

# Pattern estimation works here because samples are mostly 2D
#http://physicstasks.eu/1966/interference-of-two-plane-waves-propagating-in-different-directions 
sp.estimate_patterns(pitch_minmax_nm=[200,900],num_phase_bins=5)

""" This bit is now included in estimate_patterns:
k = sp.mod['k'] / cfg['pixelsize'] # k is now in rad/nm
kr = np.sqrt( (k[:,:2]**2).sum(-1) )
beamAngle = np.arcsin(kr * wavelen / (4*np.pi)) * 2
print(f"Beam angles w.r.t. optical axis: {np.rad2deg(beamAngle)}")
kz = 2*np.pi/wavelen*np.cos(beamAngle) * 1000 # rad/um
"""

pitch_z = 2*np.pi/sp.mod['k'][:,2] # rad/um

print(f"Z Pitch: {pitch_z*1000} nm")


sp.process(spotfilter=('moderror', 0.012))
sp.crlb_map(2000,10)

if False:
    
    sp.crlb_map(1000,1)
    
    sp.process(spotfilter=('moderror', 0.012))
    #sp.drift_correct(framesperbin=10)
    #sp.g_undrifted.save_txt(sp.resultsdir + "g_locs.txt")
    #sp.sf_undrifted.save_txt(sp.resultsdir + "sf_locs.txt")
    
    #sp.continuous_frc(5, freq=20)
    #sp.drift_correct(framesperbin=4)
    
    #sp.continuous_frc(5, freq=20)


if False: # need to install napari to run this:
    v = sp.view_rois(summed=True, indices=sp.result_indices, fits=[
        (sp.sf_results.get_estim(), {'name': 'SIMFLUX', 'face_color': 'red'}),
        (sp.g2d_results.get_estim(), {'name': '2D Gaussian', 'face_color': 'blue'})
    ])

