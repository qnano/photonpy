


from photonpy.simflux import SimfluxProcessor
from photonpy import Context,Gauss3D_Calibration
import numpy as np
import os

import tifffile

# Configuration for spot detection and 2D Gaussian fitting
cfg = {
   'roisize':15,
   'spotDetectSigma':4,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':5,
   'patternFrames': [[0,2,4],[1,3,5]],
   'gain': 0.45,
   'offset': 100,
   'pixelsize' : 65, # nm/pixel
   'startframe': 0,
   'maxframes': 0,
   'chisq_threshold': 10
}

calib_fn = 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/gausscalib.yaml'

calib = Gauss3D_Calibration.from_file(calib_fn)
cfg['psf_calib'] = calib_fn

path = 'C:/data/sfsf/sf3D/dmd_simflux_500nmpitch_3D_3Dpaint_100mlam_7/dmd_simflux_500nmpitch_3D_3Dpaint_100mlam_7_MMStack_Default.ome.tif'

sp = SimfluxProcessor(path, cfg)

sp.detect_rois(ignore_cache=False)
#sp.estimate_sigma()
sp.gaussian_fitting()

sp.estimate_patterns(pitch_minmax_nm=[210,1000],num_phase_bins=10)

sp.moderror_threshold=0.3
sp.process()#potfilter=('moderror', 0.012))
#   sp.crlb_map(2000,10)


    #sp.drift_correct(framesperbin=10)
    #sp.g_undrifted.save_txt(sp.resultsdir + "g_locs.txt")
    #sp.sf_undrifted.save_txt(sp.resultsdir + "sf_locs.txt")
    
    #sp.continuous_frc(5, freq=20)
    #sp.drift_correct(framesperbin=4)
    
    #sp.continuous_frc(5, freq=20)


if False: # need to install napari to run this:
    v = sp.view_rois(summed=True, indices=sp.result_indices, fits=[
        (sp.sf_results.get_estim(), {'name': 'SIMFLUX', 'face_color': 'red'}),
        (sp.g2d_results.get_estim(), {'name': '2D Gaussian', 'face_color': 'blue'})
    ])

