"""
Simple python implementation of Levenberg Marquardt according to
"Efficient maximum likelihood estimator fitting of histograms"
https://www.nature.com/articles/nmeth0510-338

Created on Fri Aug 27 17:03:07 2021

@author: jelmer
"""
import numpy as np
import scipy.special as sps
import matplotlib.pyplot as plt


def lm_alphabeta(mu, jac, smp):
    """
    mu: [batchsize, numsamples]
    jac: [numparams, batchsize, numsamples]
    smp: [batchsize, numsamples]
    """
    assert np.array_equal(smp.shape, mu.shape)
    K = jac.shape[0]
    sampledims = tuple(np.arange(1,len(smp.shape)))
    
    invmu = 1.0 / np.maximum(mu, 1e-9)
    af = smp*invmu**2

    # todo: use jacobian symmetry
    alphas = np.zeros((len(mu), K,K), dtype=np.float32)
    for i in range(K):
        for j in range(K):
            alphas[:,i,j] = (jac[i]*jac[j]*af).sum(sampledims)

    betas = np.zeros((len(mu), K), dtype=np.float32)
    bf = smp*invmu-1
    for i in range(K):
        betas[:,i] = (jac[i]*bf).sum(sampledims)

    return alphas, betas

def lm_mle(model, initial, smp, iterations=50, lambda_=1, callback=None):
    """
        model: 
            function that takes parameter array [batchsize, num_parameters]
            and returns (expected_value, jacobian).
            Expected value has shape [batchsize, sample dims...]
            Jacobian has shape [num_parameters, batchsize, sample dims...]

        initial: [batchsize, num_parameters]
        
        return value is a tuple with:
            estimates [batchsize,num_parameters]
            traces [iterations, batchsize, num_parameters]
    """
    smp = np.array(smp,dtype=np.float32)
    K = initial.shape[-1]
    cur = initial*1
    traces = []
    
    assert len(smp) == len(initial)
    #print(f"Solving {len(smp)} mle problems")
    
    for i in range(iterations):
        mu,jac = model(cur)
        
        alpha, beta = lm_alphabeta(mu, jac, smp)
        alpha += lambda_ * np.identity(K)
                
        steps = np.linalg.solve(alpha, beta)
        cur += steps

        if callback is not None:
            cur = callback (cur)
            
        traces.append(cur*1)
        
    return cur, np.array(traces)

def compute_crlb(mu, jac):
    mu[mu<1e-9] = 1e-9
    K = len(jac)
    sampledims = tuple(np.arange(1,len(mu.shape)))
    fi = np.zeros((len(mu),K,K))
    for i in range(K):
        for j in range(K):
            fi[:,i,j] = np.sum( 1/mu * (jac[i] * jac[j]), axis = sampledims)
            
    return np.sqrt(np.linalg.inv(fi).diagonal(axis1=1,axis2=2))


# testing code

def Gauss2DPSF(theta, numpixels):
    x = theta[:,0]
    y = theta[:,1]
    sigma = theta[:,4]
    pixelpos = np.arange(0, numpixels)

    OneOverSqrt2PiSigma = (1.0 / (np.sqrt(2 * np.pi) * sigma))[:,None,None]
    OneOverSqrt2Sigma = (1.0 / (np.sqrt(2) * sigma))[:,None,None]

    # Pixel centers
    Xc, Yc = np.meshgrid(pixelpos, pixelpos)
    Xexp0 = (Xc[None]-x[:,None,None]+0.5) * OneOverSqrt2Sigma
    Xexp1 = (Xc[None]-x[:,None,None]-0.5) * OneOverSqrt2Sigma 
    Ex = 0.5 * sps.erf(Xexp0) - 0.5 * sps.erf(Xexp1)
    dEx = OneOverSqrt2PiSigma * (np.exp(-Xexp1**2) - np.exp(-Xexp0**2))
    dEx_dSigma = (np.exp(-Xexp1**2) * Xexp1 - np.exp(-Xexp0**2)* Xexp0) / np.sqrt(np.pi)

    Yexp0 = (Yc[None]-y[:,None,None]+0.5) * OneOverSqrt2Sigma
    Yexp1 = (Yc[None]-y[:,None,None]-0.5) * OneOverSqrt2Sigma
    Ey = 0.5 * sps.erf(Yexp0) - 0.5 * sps.erf(Yexp1)
    dEy = OneOverSqrt2PiSigma * (np.exp(-Yexp1**2) - np.exp(-Yexp0**2))
    dEy_dSigma = (np.exp(-Yexp1**2) * Yexp1 - np.exp(-Yexp0**2)* Yexp0) / np.sqrt(np.pi)
    
    return Ex, Ey, dEx, dEy, dEx_dSigma, dEy_dSigma


def Gauss2DPSF_XYIBgSigma(theta, roisize):
    Ex,Ey,dEx,dEy,dEx_dSigma, dEy_dSigma=Gauss2DPSF(theta, roisize)

    mu = theta[:,2][:,None,None] * Ex*Ey + theta[:,3,None,None]
    dmu_x = theta[:,2,None,None] * Ey * dEx
    dmu_y = theta[:,2,None,None] * Ex * dEy    
    dmu_I = Ex*Ey
    dmu_bg = 1 + mu*0
    dmu_sigma = theta[:,2,None,None] * ( Ex * dEy_dSigma + dEx_dSigma * Ey )
    
    return mu, np.array( [dmu_x, dmu_y, dmu_I, dmu_bg, dmu_sigma] )

def Gauss2DPSF_XYIBg(theta, roisize, sigma):
    
    theta_ = np.zeros((len(theta),5))
    theta_[:,:4] = theta
    theta_[:,4] = sigma
    
    mu,jac = Gauss2DPSF_XYIBgSigma(theta_, roisize)
    return mu, jac[:-1]




if __name__ == '__main__':
    sigma = 1.5
    roisize = 10
    # Fit 1000 spots
    theta = np.zeros((1000, 4))
    theta[:,:2] = roisize/2-0.5
    theta[:,2] = 1000
    theta[:,3] = 1
    
    mu, jac = Gauss2DPSF_XYIBg(theta, roisize, sigma=sigma)
    smp = np.random.poisson(mu)
    
    plt.figure()
    plt.imshow(smp[:,:,0])

    crlb = compute_crlb(mu,jac)
    print(crlb)
    
    psf_model = lambda theta: Gauss2DPSF_XYIBg(theta, roisize, sigma)

    estimated, traces = lm_mle(psf_model, theta * np.random.uniform(0.9,1.1,size=theta.shape), smp)
    print(estimated - theta)
    
    
    