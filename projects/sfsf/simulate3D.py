from photonpy import Context,Dataset
from photonpy.simflux import SimfluxProcessor, view_napari
from photonpy.simflux.combined_frame_simflux import CombinedFrameSFProcessor
import numpy as np
import matplotlib.pyplot as plt
import os
import photonpy.smlm.process_movie as process_movie
import napari

from photonpy.utils.caching import read as read_cached

"""
cfg = {
    'sigma':[1.9,1.9], # can be removed in future, only used when saving drift results now
   'roisize':10,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':45,
   'patternFrames': [[0,2,4],[1,3,5]],
   'sdBackgroundSigma': 10,
   'sdXYFilterSize': 4,
   'gain': 1,
   'offset': 0,
   'pixelsize' : 100, # nm/pixel
   'startframe': 0,
   'maxframes': 0,
   
   'use3D': True,
   'wavelength': 640,
   'gauss3DCalib': 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/gausscalib.yaml'
}
"""



cfg = {
    'psf_calib':[1.6, 1.6], # from sp.estimate_sigma()
   'roisize':13,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':42,
   'patternFrames': [[0,2,4],[1,3,5]], 
   'gain': 1,
   'offset': 0,
   'pixelsize' : 65, # nm/pixel  ???
   'startframe': 0,
   'maxframes': 0,
   'wavelength': 640,
   'use3D': True,
   'sdXYFilterSize': 10,
   'sdBackgroundSigma': 20,
   'gauss3DCalib': 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/gausscalib.yaml',
    'patternsPerFrame': 1
}



path = 'test3D.tif'

offsets = np.zeros((6, 3 if cfg['use3D'] else 2))
cfg['patternsPerFrame'] = 1


if True:
    gen = CombinedFrameSFProcessor(path, cfg)
    
    np.random.seed(0)
    
    gen.set_offsets(offsets)
    
    gen.set_mod(pitch_nm=[400, 220], angle_deg=[90, 0], depth=0.9, z_angle=[40,10])
   
    if False:
        with Context(debugMode=False) as ctx:
            psf = gen.create_psf(ctx, True)
            params_ = [[5,5,1000, 1], [7,7,1000,1]]
            roipos = [[0,1,2], [0,4,2]]
            ev = psf.ExpectedValue(params_, constants=gen.mod.view(np.float32) ,roipos=roipos)
            
            sp=SimfluxProcessor(path, cfg)
            sp.mod = gen.mod
            sf_psf = sp.create_psf(ctx,True)
            
            ev2 =sf_psf.ExpectedValue(params_, constants=gen.mod.view(np.float32), roipos=roipos)
            view_napari(np.concatenate([ev,ev2],axis=-1))
                    
   
    print(gen.mod)
    #gen.set_mod(pitch_nm=[220, 220, 220], angle_deg=[0, 90], depth=0.9)    
    
    bg=10
    I=2000
    
    gen.crlb_map(I, bg)
    
    W = 150
    Z = 1
    ds = Dataset(1000, offsets.shape[1], (W,W))
    ds.pos[:,:2] = np.random.uniform(0,W,size=(len(ds),2))
    ds.pos[:,2] = np.random.uniform(-Z, Z, size=len(ds))
    ds.photons = I
    
    gen.simulate(20000, bg=5, gt=ds, output_fn=path, debugMode=False)
    
# Process the simulated data

if True:
    
    sp = CombinedFrameSFProcessor(os.path.abspath(path), cfg, debugMode=False)
    
    if True:
        sp.detect_rois(offsets, roi_batch_size=5000, 
                            numStreams=3, # change # of cuda streams
                            chisq_threshold=6)
    
    fn = 'simulated3D-temp.pickle'
    sp.save_pickle(fn)
    #sp.load_pickle(fn)
    
    
    sp.estimate_patterns(pitch_minmax_nm=[180,1000],num_phase_bins=10,dft_peak_search_range=0.04)
    sp.process(spotfilter=('moderror', 0.012), chisq_threshold=20)
    #sp.crlb_map()
    
