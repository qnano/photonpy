from photonpy.simflux import SimfluxProcessor
from photonpy.cpp.context import Context
import numpy as np
import os

import tifffile

# Configuration for spot detection and 2D Gaussian fitting
cfg = {
    'psf_calib':[1.8,1.8],
   'roisize':10,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':10,
   'patternFrames': [[0,2,4],[1,3,5]],
   'gain': 0.45,
   'offset': 100,
   'pixelsize' : 65, # nm/pixel
   'startframe': 0,
   'maxframes': 0,
   'spotDetectSigma': 2,
   'chisq_threshold': 8
}


#path = 'C:/data/simflux/1_2/1_2_MMStack_Pos0.tif'
path = 'C:/data/simflux/sim4_1/sim4_1.tif' #[1.885575  1.8277005]

#path = 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/4_5ms.tif' # also works

sp = SimfluxProcessor(path, cfg)

sp.detect_rois(ignore_cache=False)
sp.estimate_sigma()
sp.gaussian_fitting()

sp.estimate_patterns(pitch_minmax_nm=[210,1000],num_phase_bins=10)



sp.moderror_threshold = 0.2
sp.process()
sp.crlb_map(2000,10)


    #sp.drift_correct(framesperbin=10)
    #sp.g_undrifted.save_txt(sp.resultsdir + "g_locs.txt")
    #sp.sf_undrifted.save_txt(sp.resultsdir + "sf_locs.txt")
    
    #sp.continuous_frc(5, freq=20)
    #sp.drift_correct(framesperbin=4)
    
    #sp.continuous_frc(5, freq=20)


if False: # need to install napari to run this:
    v = sp.view_rois(summed=True, indices=sp.result_indices, fits=[
        (sp.sf_results.get_estim(), {'name': 'SIMFLUX', 'face_color': 'red'}),
        (sp.g2d_results.get_estim(), {'name': '2D Gaussian', 'face_color': 'blue'})
    ])

