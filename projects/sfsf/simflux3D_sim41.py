from photonpy.simflux.combined_frame_simflux import CombinedFrameSFProcessor 
from photonpy.cpp.context import Context
import numpy as np
import matplotlib.pyplot as plt
import os
import photonpy.smlm.process_movie as process_movie
import napari



def computeAPB(n):
    s = np.sqrt( (n[:,0]-n[:,1])**2 + (n[:,0]-n[:,2])**2 + (n[:,1]-n[:,2])**2)
    A = 2/3*np.sqrt(2)*np.sqrt(s)
    phi = -1/np.pi * np.arctan((-2*n[:,0]+n[:,1]+n[:,2] + np.sqrt(2)*s)/(np.sqrt(3)*(n[:,1]-n[:,2])))
    b = 1/3*(n.sum(1)-np.sqrt(2)*s)
    return A,phi,b

# Subtract the contribution to phase of x and y components of k
#kz*z +phase = phi - kx*x+ky*y
#
# refit pattern on z
# 1+d*sin(kz*z-phase)
#I0 = np.sum(I)
#Iw = np.sum(I * np.exp(-1j * spotphase))


cfg = {
    'psf_calib':[1.9,1.9], # can be removed in future, only used when saving drift results now
   'roisize':10,
   'maxSpotsPerFrame':2000,
   'detectionThreshold':25,
   'patternFrames': [[0,2,4],[1,3,5]],
   'sdBackgroundSigma': 20,
   'sdXYFilterSize': 10,
   'gain': 1/2.2,
   'offset': 100,
   'pixelsize' : 65, # nm/pixel
   'startframe': 0,
   'maxframes': 0,
   
   'use3D': False,
   'wavelength': 640,
   'gauss3DCalib': 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/gausscalib.yaml'
}

#path = 'C:/data/sfsf/sf3D/COS7 STORM 12-23-2020/4_5ms.tif'

path = 'C:/data/simflux/sim4_1/sim4_1.tif' #[1.885575  1.8277005]

offsets = np.zeros((6, 2))
cfg['patternsPerFrame'] = 1

sp = CombinedFrameSFProcessor(path, cfg, debugMode=False)

if True:
    sp.detect_rois(offsets, roi_batch_size=5000, 
                        numStreams=3, # change # of cuda streams
                        chisq_threshold=40)
    
    sp.save_pickle(os.path.splitext(path)[0]+"_results.pickle")
else:
    sp.load_pickle(os.path.splitext(path)[0]+"_results.pickle")
    sp.set_offsets(offsets)


sp.estimate_patterns(pitch_minmax_nm=[200,240],num_phase_bins=5)
#sp.crlb_map()
#sp.crlb_map_3D()

spotfilter = ('moderror', 0.012)
sp.pattern_plots([spotfilter])

# Run the simflux fit. Modulation is loaded from file.
sp.process(None, chisq_threshold=40)

#sp.drift_correct(framesperbin=100)

#sp.g_undrifted.save_txt(sp.resultsdir + "g_locs.txt")
#sp.sf_undrifted.save_txt(sp.resultsdir + "sf_locs.txt")

# View the ROIs that have been selected by the chi-square filter during sp.process()
#px,ev=sp.sfsf_view_rois(maxspots=1000, indices=sp.sf_results.src_indices)

