# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

from photonpy import Context, Estimator, GaussianPSFMethods
from photonpy.cpp.simflux import SIMFLUX



def generate_storm_movie(psf:Estimator, xyI, mod, numframes=100,
                         imgsize=512, bg=10, p_on=0.1):
    
    frames = np.zeros((numframes, imgsize, imgsize), dtype=np.float32)

    for f in range(numframes):
        on = np.random.binomial(1, p_on, len(xyI))

        roisize = psf.sampleshape[0]
        roipos = np.clip((xyI[:,[1,0]] - roisize/2).astype(int), 0, imgsize-roisize)
        theta = np.zeros((len(xyI),4)) # assuming xyI
        theta[:,0:3] = xyI
        theta[:,[1,0]] -= roipos
        on_spots = np.nonzero(on)[0]

        theta_frame = theta[on_spots]
        mod_params = np.repeat([mod.flatten()],len(theta_frame),0)
        rois = psf.ExpectedValue(theta_frame, constants=mod_params)
        
        frames[f] = ctx.smlm.DrawROIs((imgsize,imgsize), rois, roipos[on_spots])
        frames[f] += bg

    return frames

def view_movie(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)
        
        
# Modulation pattern
roisize = 40
sigma = 1.9

k = 1.8
period = 2*np.pi/k
mod = np.array([
     [0, k, 0, 0.95, 0, 1/6],
     [k, 0, 0, 0.95, 0, 1/6],
     [0, k, 0, 0.95, 2*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 2*np.pi/3, 1/6],
     [0, k, 0, 0.95, 4*np.pi/3, 1/6],
     [k, 0, 0, 0.95, 4*np.pi/3, 1/6]
])

# Hexagon shape
radius = 7
offsets = np.zeros((6,2))
ang = np.linspace(0,2*np.pi, len(offsets), endpoint=False)
offsets[:,0] = radius*np.cos(ang)
offsets[:,1] = radius*np.sin(ang)


# Where to put emitters?
N = 2000
w = 200
R = w * 0.3
angle = np.random.uniform(0, 2 * np.pi, N)
emitters = np.vstack((R * np.cos(angle) + w / 2, R * np.sin(angle) + w / 2)).T
numframes = 200

xyI = np.zeros((N,3))
xyI[:,:2] = emitters
xyI[:,2] = np.random.uniform(500,2000,size=N)


with Context() as ctx:
    psf = GaussianPSFMethods(ctx).CreatePSF_XYIBg(roisize, sigma, True)
    with SIMFLUX(ctx).CreateSFSFEstimator(psf, offsets, True) as hex_psf:
        frames = generate_storm_movie(hex_psf, xyI, mod, numframes, w, bg=10,p_on=0.01)

frames = np.random.poisson(frames)    

view_movie(frames)
