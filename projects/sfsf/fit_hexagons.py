"""


"""
import numpy as np
import matplotlib.pyplot as plt

import photonpy.smlm.util as su
from photonpy.cpp.context import Context
from photonpy.cpp.gaussian import Gaussian
from photonpy.cpp.spotdetect import SpotDetectionMethods, SpotDetector,PSFConvSpotDetector

import photonpy.cpp.com as com 
from photonpy.cpp.simflux import SIMFLUX
import tqdm
import tifffile
from photonpy.smlm.process_movie import create_calib_obj
from photonpy.smlm.util import imshow_hstack
import time

def view_napari(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)


images=[]

def debugImage(img,label):
    images.append((img,label))


def plotDebugImages():
    for img,label in images:
        plt.figure()
        plt.imshow( np.concatenate(img,-1) )
        plt.colorbar()
        plt.title(label)
        


def extract_template(img, pos, roisize=20):
    d = (img[pos[0]-roisize//2:pos[0]+roisize//2,
         pos[1]-roisize//2:pos[1]+roisize//2]).astype(np.float32)
    
    #d -= np.median(d)
    #d /= np.sum(d)
    return d

def process_movie(mov, spotDetector, roisize, cameraCalib, ctx:Context):
    imgshape = mov[0].shape
    roishape = [roisize,roisize]

    img_queue, roi_queue = SpotDetectionMethods(ctx).CreateQueue(imgshape, roishape, spotDetector, calib=cameraCalib)
    t0 = time.time()

    for img in mov:
        img_queue.PushFrame(img)
   
    while img_queue.NumFinishedFrames() < len(mov):
        time.sleep(0.1)
    
    dt = time.time() - t0
    print(f"Processed {len(mov)} frames in {dt:.2f} seconds. {len(mov)/dt:.1f} fps")
    
    rois, data = roi_queue.Fetch()
    roipos = np.array([rois['x'],rois['y']]).T

    plt.figure()
    plt.hist(rois['score'],bins=50)
    plt.title('Spot detection scores')
    return roipos, data



# Sigma and roisize is kinda tweaked to produce the 6 spots, probably fails with a smaller R
def estimate_template_properties(image, initialSigma, cameraGain, cameraOffset):
    with Context() as temp:
        roisize = 10
        camera_calib = create_calib_obj(cameraGain, cameraOffset, image.shape, temp)
        spotDetector = SpotDetector(initialSigma, roisize, minIntensity=2)
        results = SpotDetectionMethods(temp).ProcessFrame(image, spotDetector, roisize, 10, calib=camera_calib)
        
        estim = Gaussian(temp).CreatePSF_XYIBgSigma(roisize, initialSigma, cuda=True)
        estim.SetLevMarParams(-1)
        rois = results[0]
        theta = estim.Estimate(rois)[0]
        expval = estim.ExpectedValue(theta)
        plt.imshow(np.vstack(np.concatenate([rois,expval],-1)))
        
        yx = results[1]+theta[:,[1,0]]
        center = np.mean(yx,0)
        radius = np.mean( np.sqrt(np.sum((yx-center)**2,1)) )
        
    sigma = np.median(theta[:,4])
    print(f"Sigma: {sigma}. Radius : {radius}")
    offsets = yx-center
    #plt.figure()
    #plt.scatter(offsets[:,1],offsets[:,0])
    return sigma, offsets[:,[1,0]], radius


def fit_hexagons(data, radius, template_spot_xy, roisize, psf_sigma, detection_threshold, cameraGain, cameraOffset):

    if True:
        offsets = template_spot_xy
    else:
        offsets = np.zeros((6,2))
        ang = np.linspace(0,2*np.pi, len(offsets), endpoint=False)
        offsets[:,0] = radius*np.cos(ang)
        offsets[:,1] = radius*np.sin(ang)
    
    mod = np.zeros((len(offsets),6))
    
    with Context() as ctx:
        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, psf_sigma, cuda=True)
        estim = SIMFLUX(ctx).CreateSFSFEstimator(psf, mod, offsets, simfluxMode=False)
        
        print(f"Parameter format: {estim.ParamFormat()}")
    
        # The template used to do detection with
        detectionStack = estim.ExpectedValue(estim.ExpandIntensities([[roisize/2,roisize/2,1,0]]))
        
        plt.figure()
        plt.imshow(detectionStack[0])
        plt.title('Template used for spot detection')

        ctx.smlm.SetDebugImageCallback(debugImage)
    
        # this sets up the template-based spot detector. MinPhotons is not actually photons, still just AU.
        sd = PSFConvSpotDetector(detectionStack, bgimg=data[0]*0, minPhotons=detection_threshold, 
                                 maxFilterSizeXY=int(radius*3), debugMode=True)
        
        camera_calib = create_calib_obj(cameraGain, cameraOffset, data[0].shape, ctx)
        roiposXY, rois = process_movie(data, sd, roisize, camera_calib, ctx)
        
        plotDebugImages()
        
        print(f"{len(rois)} spots detected")
        
        if len(rois)>=4:
            # Show the first 5 spots
            plt.figure()
            plt.imshow(np.concatenate(rois[:5],-1))
    
        com_estim = com.CreateEstimator(roisize, ctx)
        com_estimates = com_estim.Estimate(rois)[0]
        
        # Turn [x,y,I,bg] into [x,y,I...,bg]
        initial_value = estim.ExpandIntensities(com_estimates)
        
        fitted = estim.Estimate(rois, initial=initial_value)[0]
    
        intensities = fitted[:,2:-1].reshape((len(fitted),6))
        xy = fitted[:,:2]
        
        ev = estim.ExpectedValue(fitted)
        
        crlb=estim.CRLB(fitted)

    return rois, xy, intensities, ev,crlb
    



data = tifffile.imread('DMD SIMFLUX.tif')
cameraGain = 2.2
cameraOffset =100

plt.figure()
plt.imshow(data[0])
    
template_pos_yx = [357,274]
roisize = 44

template_img = extract_template(data[0], template_pos_yx, roisize)
plt.figure()
plt.imshow(template_img)
plt.title('Hand picked template image')

sigma,spot_xy, radius= estimate_template_properties(template_img, initialSigma=3, 
                                                    cameraGain=cameraGain, cameraOffset=cameraOffset)

rois, xy, intensities, ev,crlb = fit_hexagons([data[0]], radius, spot_xy, roisize=38, psf_sigma=sigma, detection_threshold=100,
                               cameraGain=cameraGain, cameraOffset=cameraOffset)

print(intensities)

view_napari(rois)


    