"""
Some hacked code to estimate hexagon shape where the spots overlap with eachother
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import photonpy.smlm.util as su
from photonpy.cpp.context import Context
from photonpy.cpp.gaussian import Gaussian
from photonpy.cpp.spotdetect import SpotDetectionMethods, SpotDetector,PSFCorrelationSpotDetector

import photonpy.cpp.com as com 
from photonpy.cpp.simflux import SIMFLUX
import tqdm
import tifffile
from photonpy.smlm.process_movie import create_calib_obj
from photonpy.smlm.util import imshow_hstack
import time
from scipy.optimize import minimize
import pickle

def hexagon_with_radius(roisize, params,ctx):
    """
    A hexagon model where the offsets are fixed but radius and sigma are fitted
    
    Params: x,y,I,bg,radius,sigma
    """
    nspots=6
    gaussfn = Gaussian(ctx)
    with gaussfn.CreatePSF_XYIBgSigma(roisize, 3, cuda=False) as psf:
        psfparam = np.zeros((nspots,5))
        radius = params[4]
        ang = np.linspace(0,2*np.pi, nspots, endpoint=False)
        psfparam[:,0] = params[0] + radius*np.cos(ang + np.pi/2)
        psfparam[:,1] = params[1] + radius*np.sin(ang + np.pi/2)
        psfparam[:,4] = min(3, max(params[5],1))
        psfparam[:,2] = params[2]/nspots # I
        psfparam[:,3] = 0 #bg
        
        ev = psf.ExpectedValue(psfparam).sum(0) + params[3]
    return ev

def hexagon_with_offsets(roisize, params, ctx):
    """
    A hexagon model where the offsets are a set of X,Y coordinates
    Params: sigma,bg,I, x1,y1, x2...
    """
    nspots=6
    gaussfn = Gaussian(ctx)
    with gaussfn.CreatePSF_XYIBgSigma(roisize, 3, cuda=False) as psf:
        psfparam = np.zeros((nspots,5))
        x = params[3::2]
        y = params[4::2]
        sigma = params[0]
        psfparam[:,0] = x
        psfparam[:,1] = y
        psfparam[:,4] = min(3, max(sigma,1))
        psfparam[:,2] = params[2]/nspots # I
        psfparam[:,3] = 0 #bg
        
        ev = psf.ExpectedValue(psfparam).sum(0) + params[1]
    return ev

def hexagon_with_offsets_initial(img, radius, sigma):
    nspots = 6
    ang = np.linspace(0,2*np.pi, nspots, endpoint=False)
    params = np.zeros((3+nspots*2))
    params[3::2] = img.shape[1]/2 + radius*np.cos(ang + np.pi/2)
    params[4::2] = img.shape[0]/2 + radius*np.sin(ang + np.pi/2)
    params[0] = sigma
    params[1] = np.median(img)
    params[2] = np.sum(img)-np.median(img)*img.size
    return params

def estimate_pattern_from_template(template,ctx):
    """
    Fit a gaussian hexagon shape on a single image ('template')
    """
    roisize = template.shape[-1]
    
    initial = hexagon_with_offsets_initial(template, 6, 1.5)
    
    def metric(params):
        ev = hexagon_with_offsets(roisize, params,ctx)
        return np.sum( (ev-template)**2 )
        
    r = minimize(metric, initial, method='Nelder-Mead')
    
    sigma = r.x[0]
    print(f"Sigma: {sigma:.1f} px")

    ev=hexagon_with_offsets(roisize, r.x, ctx)
    fig,ax=plt.subplots(1,2,sharey=True)
    ax[0].imshow(ev)
    ax[1].imshow(template)

    x = r.x[3::2]
    y = r.x[4::2]

    center = [ np.mean(x), np.mean(y) ]
    offsets = np.array([ x-center[0], y-center[1] ]).T
    radius = np.mean(np.sqrt(np.sum(offsets**2,1)))
    
    print(f"Radius: {radius:.2f}")
    
    ax[0].set_title('Fitted model')
    ax[0].scatter(x, y,label= 'Offsets')
    ax[0].add_patch(patches.Circle(center, radius,color='black',fill=False))
    #ax[0].scatter(x, y,label= 'Offsets')

    ax[1].set_title('Template')
        
    return offsets,sigma, ev

def process_movie(mov, spotDetector, roisize, cameraCalib, ctx:Context):
    imgshape = mov[0].shape
    roishape = [roisize,roisize]

    img_queue, roi_queue = SpotDetectionMethods(ctx).CreateQueue(imgshape, roishape, spotDetector, calib=cameraCalib)
    t0 = time.time()

    for img in mov:
        img_queue.PushFrame(img)
   
    while img_queue.NumFinishedFrames() < len(mov):
        time.sleep(0.1)
    
    dt = time.time() - t0
    print(f"Processed {len(mov)} frames in {dt:.2f} seconds. {len(mov)/dt:.1f} fps")
    
    rois, data = roi_queue.Fetch()
    roipos = np.array([rois['x'],rois['y']]).T

    plt.figure()
    plt.hist(rois['score'],bins=50)
    plt.title('Spot detection scores')
    return roipos, data


def extract_rois_with_template(data, template_img, cameracalib, detection_threshold,ctx):
        # this sets up the template-based spot detector. MinPhotons is not actually photons, still just AU.
    sd = PSFCorrelationSpotDetector([template_img], bgimg=data[0]*0, 
            minPhotons=detection_threshold,
            bgFilterSize=template_img.shape[0],
            maxFilterSizeXY=template_img.shape[0], debugMode=False)
    
    roipos, roi_pixels = process_movie(data, sd, template_img.shape[0], cameracalib, ctx)

    return roipos, roi_pixels

def extract_template(img, pos, roisize=20):
    d = (img[pos[0]-roisize//2:pos[0]+roisize//2,
         pos[1]-roisize//2:pos[1]+roisize//2]).astype(np.float32)
    
    return d

def chisq_filter(roipos, roipix, template_img,percentile):
    expval = template_img[None,:,:]
    chisq = np.sum( (roipix-expval)**2 / (expval+1e-9), (1,2))
    
    idx = chisq < np.percentile(chisq, percentile)
    return roipos[idx], roipix[idx]    
    
def localize(roipix, offsets, sigma, parent_ctx):
    roisize = roipix.shape[-1]
    
    with parent_ctx.Create() as ctx:
        sf = SIMFLUX(ctx)
        psf = Gaussian(ctx).CreatePSF_XYIBg(roisize, sigma, True)
        
        sfe = sf.CreateSFSFEstimator(psf, offsets, False)
        # simflux psf with x,y,I,bg
                
        initial = np.zeros((len(roipix),4))
        initial[:,:2] = roisize/2
        initial[:,2] = np.sum(roipix,(1,2))
        initial[:,3] = 0

        estim = sfe.Estimate(roipix, initial=sfe.ExpandIntensities(initial))[0]
        
        chisq = sfe.ChiSquare(estim, roipix)

        return estim,chisq
                
def estimate_dm_pattern(roipix, estim, initial_sigma, offsets, parent_ctx):
    roisize = roipix.shape[-1]

    with parent_ctx.Create() as ctx:
        sf = SIMFLUX(ctx)
        gaussfn = Gaussian(ctx)
    
        def metric(params):
            # params: [sigma, x1, y1, x2...]
            sigma = params[0]
            offsets = np.array([params[1::2],params[2::2]]).T
            with gaussfn.CreatePSF_XYIBg(roisize, sigma, True) as psf:
                with sf.CreateSFSFEstimator(psf, offsets, False) as sfe:
                    return np.sum(sfe.ChiSquare(estim, roipix))

        x0 = np.zeros(1+2*len(offsets))
        x0[0]= initial_sigma
        x0[1::2] = offsets[:,0]
        x0[2::2] = offsets[:,1]
        r = minimize(metric, x0, method='Nelder-Mead')
        
        print(f"Result metric: {r.fun:.1f}")
        
        sigma = r.x[0]
        offsets = np.array([r.x[1::2],r.x[2::2]]).T
        
        return sigma,offsets
    
def estimate_hexagon(data, template_img, cameraGain, cameraOffset, output_fn):
    with Context() as ctx:
        camera_calib = create_calib_obj(cameraGain, cameraOffset, data[0].shape, ctx)
        roipos, roipix = extract_rois_with_template(data, template_img, camera_calib, 
                                                    detection_threshold=20,ctx=ctx)
        
        print(f"#rois: {len(roipos)}")
        
        # Make a slightly less shitty template
        template_img2 = roipix.mean(0)
    
        roipos, roipix = extract_rois_with_template(data, template_img2, camera_calib, 
                                                    detection_threshold=20,ctx=ctx)
    
        template_img3 = roipix.mean(0)
    
        roipos, roipix = chisq_filter(roipos, roipix, template_img3, 20)
        print(f"#rois: {len(roipos)}")
        
        #view_napari(roipix)
        template_img3 = roipix.mean(0)
        
        plt.figure()
        plt.imshow(template_img3)
        offsets,sigma,ev = estimate_pattern_from_template(template_img3,ctx)
        
        
        for i in range(8):
            # estimate all spot center positions using found offsets/radius
            estim,chisq = localize(roipix, offsets, sigma, ctx)
            
            ok = chisq<np.median(chisq)
 
            plt.figure()
            plt.hist(chisq,bins=100)
    
            # re-estimate sigma using more accurate offsets/spot centers
            sigma, offsets = estimate_dm_pattern(roipix[ok], estim[ok], sigma, offsets, ctx)
    
            print(f"Sigma: {sigma:.1f}")
            
        pat = (sigma,offsets)
        #output_fn(fn, pat)
        with open(output_fn,"wb") as f:
            pickle.dump(pat,f)
            
    return pat

def view_napari(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)

fn = '/data/hexagons/Gatta40nm_nophase_2_X1.tif'
data = tifffile.imread(fn)
cameraGain = 2.2
cameraOffset = 100

template_img = extract_template(data[0], [169,145], 26)
plt.imshow(template_img)

dmpat_fn='hexagon_pattern.pickle'

pat = estimate_hexagon(data, template_img, cameraGain,cameraOffset,dmpat_fn)

