import matplotlib.pyplot as plt
import numpy as np
import os
import time

import photonpy.smlm.util as su
from photonpy.cpp.context import Context
import photonpy.cpp.gaussian as gaussian

from photonpy.cpp.cspline import CSplineMethods,CSplineCalibration
from photonpy.cpp.simflux import SIMFLUX
from photonpy.cpp.estimator import Estimator
import photonpy.cpp.com  as com
import photonpy.cpp.phasor as phasor
import tqdm

import levmar

def mujac_sf_XYIBgSigma(theta, roisize, mod):
    gEx,gEy,gdEx,gdEy,gdEx_dSigma, gdEy_dSigma = levmar.Gauss2DPSF(theta, roisize)
        
    # 
    P = (1+mod[None,:,2] * np.sin(mod[None,:,0]*theta[:,0,None]+
                                  mod[None,:,1]*theta[:,1,None]-mod[None,:,3]))*mod[None,:,4]

    dPdx = mod[None,:,4] * mod[None,:,2] * mod[None,:,0] * np.cos(mod[None,:,0]*theta[:,0,None]+
                                                                  mod[None,:,1]*theta[:,1,None]-mod[None,:,3])
    dPdy = mod[None,:,4] * mod[None,:,2] * mod[None,:,1] * np.cos(mod[None,:,0]*theta[:,0,None]+
                                                                  mod[None,:,1]*theta[:,1,None]-mod[None,:,3])
    
    #print(Q)
    
    gExy = gEx*gEy
    
    # [roi, mod, y, x]
    mu = theta[:,2,None,None,None] * (P[:,:,None,None]*gExy[:,None]) + theta[:,3,None,None,None]
    dmu_x = theta[:,2,None,None,None] * (dPdx[:,:,None,None] * gExy[:,None] + P[:,:,None,None] * gdEx[:,None] * gEy[:,None])
    dmu_y = theta[:,2,None,None,None] * (dPdy[:,:,None,None] * gExy[:,None] + P[:,:,None,None] * gEx[:,None] * gdEy[:,None])
    dmu_I = P[:,:,None,None]*gExy[:,None]
    dmu_bg =  1+0*mu
    dmu_dsigma = theta[:,2,None,None,None] * P[:,:,None,None] * (gEx[:,None] * gdEy_dSigma[:,None] + gdEx_dSigma[:,None] * gEy[:,None])
    
    return mu, [dmu_x,dmu_y,dmu_I,dmu_bg, dmu_dsigma]

def mujac_sf_XYIBg(theta, roisize, sigma, mod):
    
    theta_ = np.zeros((len(theta),5))
    theta_[:,:4] = theta
    theta_[:,4] = sigma
    
    mu, jac = mujac_sf_XYIBgSigma(theta_, roisize, mod)
    return mu, jac[:-1]




def plot_traces(traces, theta, psf, axis):
    plt.figure()
    for k in range(len(traces)):
        plt.plot(np.arange(len(traces[k])),traces[k][:,axis])
        plt.plot([len(traces[k])],[theta[k,axis]],'o')
    plt.title(f"Axis {axis}[{psf.param_names[axis]}]")

def estimate_precision(psf:Estimator, estimate_fn, thetas, photons, mod):
    prec = np.zeros((len(photons),thetas.shape[1]))
    bias = np.zeros((len(photons),thetas.shape[1]))
    crlb = np.zeros((len(photons),thetas.shape[1]))
    
    Iidx = psf.ParamIndex('I')
                    
    for i in tqdm.trange(len(photons)):
        thetas_ = thetas*1
        thetas_[:, Iidx] = photons[i]
        
        roipos = np.random.randint(0,20,size=(len(thetas_), psf.indexdims))
        roipos[:,0] = 0
        
        smp = psf.GenerateSample(thetas_,roipos=roipos,constants=mod)
        roisize = smp.shape[-1]

        estim,diag,traces = estimate_fn(smp,roipos=roipos,constants=mod)
            
        if i == len(photons)-1:
            plot_traces(traces[:20], thetas_[:20], psf, axis=0)
            plot_traces(traces[:20], thetas_[:20], psf, axis=2)
            plot_traces(traces[:20], thetas_[:20], psf, axis=Iidx)

        crlb_ = psf.CRLB(thetas_,roipos=roipos, constants=mod)
        err = estim-thetas_
        prec[i] = np.std(err,0)
        bias[i] = np.mean(err,0)
        crlb[i] = np.mean(crlb_,0)

#    print(f'sigma bias: {bias[:,4]}')        
    return prec,bias,crlb



def view_movie(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)



pitch = 221/65
k = 2*np.pi/pitch

if True: #XY modulation
    mod = np.array([
               [0, k, 0, 0.95, 0, 1/6],
               [k, 0, 0, 0.95, 0, 1/6],
               [0, k, 0, 0.95, 2*np.pi/3, 1/6],
               [k, 0, 0, 0.95, 2*np.pi/3, 1/6],
               [0, k, 0, 0.95, 4*np.pi/3, 1/6],
               [k, 0, 0, 0.95, 4*np.pi/3, 1/6]
               ])
else: # Z modulation
    mod = np.array([
           [0, 0, 2, 0.95, 0, 1/3],
           [0, 0, 2, 0.95, 2*np.pi/3, 1/3],
           [0, 0, 2, 0.95, 4*np.pi/3, 1/3]
          ])

def cspline_calib_fn():
    cspline_fn = 'cspline-nm-astig.mat'
    #cspline_fn = "C:/data/beads/Tubulin-A647-cspline.mat"
    if not os.path.exists(cspline_fn):
        try:
            import urllib.request
            url='http://homepage.tudelft.nl/f04a3/Tubulin-A647-cspline.mat'
            print(f"Downloading {url}")
            urllib.request.urlretrieve(url, cspline_fn)
            
            if not os.path.exists(cspline_fn):
                print('Skipping CSpline 3D PSF (no coefficient file found)')
                cspline_fn = None
        finally:
            ...
    
    return cspline_fn



calib = gaussian.Gauss3D_Calibration()

with Context(debugMode=False) as ctx:
    g = gaussian.Gaussian(ctx)
      
    sigma=2.5
    roisize=12
    bg=10
    numspots = 2000
    theta=[[roisize/2, roisize/2, 10000, bg]]
    theta=np.repeat(theta,numspots,0)
    theta[:,[0,1]] += np.random.uniform(-pitch/2,pitch/2,size=(numspots,2))
      
    useCuda=True
      
    #    with g.CreatePSF_XYZIBg(roisize, calib, True) as psf:
    g_z_psf = g.CreatePSF_XYZIBg(roisize, calib, useCuda)
    g_z_psf.SetLevMarParams(1e-19, 30)
    
    sf = SIMFLUX(ctx)
    sf_psf = sf.CreateEstimator_Gauss2D(calib, len(mod), roisize, len(mod))
    sf_psf.SetLevMarParams(1e-20, iterations=100)
    
    theta_as=np.zeros((numspots,5)) # x,y,z,I,bg
    theta_as[:,[0,1]]=theta[:,[0,1]]
    theta_as[:,2] = np.random.uniform(-0.3,0.3,size=numspots)
    theta_as[:,3] = 1000
    theta_as[:,4] = bg
        
    theta_zs = theta_as*1
    theta_zs[:,:2] = roisize/2
    theta_zs[:,0] = np.linspace(5, 11, numspots)
    theta_zs[:,2] = 0

    theta_zs = theta_as*1
    theta_zs[:,:2] = roisize/2
    theta_zs[:,2] = np.linspace(calib.zrange[0], calib.zrange[1], numspots)
    #theta_zs[:,2] = 0
    
    as_zstack = g_z_psf.ExpectedValue(theta_zs)
    theta_zs[:,4] /= len(mod)
    sf_zstack = sf_psf.ExpectedValue(theta_zs, constants=mod.flatten()[None])

    #view_movie(as_smp)
    
    theta_sf = theta_as*1
    theta_sf[:,4] /= len(mod)
    
    photons = np.logspace(2, 5, 20)

    def sf_estim(smp,roipos,constants):
        summed = smp.sum(1)
        initial = g_z_psf.Estimate(summed, roipos[:,1:])[0]
        initial[:,4] =0#/= len(mod)
        return sf_psf.Estimate(smp, roipos, constants=constants, initial=initial)

    data = [
        (g_z_psf, 'Astig Gaussian',estimate_precision(g_z_psf, g_z_psf.Estimate, theta_as, photons, mod=None)),
        
        #(g_psf, "2D Gaussian with readnoise", estimate_precision(g_psf_noisy,g_psf_noisy, theta, photons)),
        #(com_psf, "Center of mass", estimate_precision(g_psf, com_psf, theta, photons)),
    ]
    
    if True:
        data.append(
            (sf_psf, "SIMFLUX", estimate_precision(sf_psf, sf_estim, theta_sf, photons, mod=mod.flatten()[None])),
        )

    axes=['x', 'z']#, 'I', 'bg']
    axes_unit=['pixels','nm', 'photons','photons/pixel']
    axes_scale=[1, 1000, 1, 1]
    for i,ax in enumerate(axes):
        plt.figure(dpi=150,figsize=(12,8))
        for psf,name,(prec,bias,crlb) in data:
            ai = psf.ParamIndex(ax)
            line, = plt.gca().plot(photons,axes_scale[i]*prec[:,ai],label=f'Precision {name}')
            plt.plot(photons,axes_scale[i]*crlb[:,ai],label=f'CRLB {name}', color=line.get_color(), linestyle='--')

        plt.title(f'{ax} axis - bg={bg} photons/px^2')
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel('Signal intensity [photons]')
        plt.ylabel(f"{ax} [{axes_unit[i]}]")
        plt.grid(True)
        plt.legend()

        """
        plt.figure()
        for psf,name,(prec,bias,crlb) in data:
            ai = psf.ParamIndex(ax)
            plt.plot(photons,bias[:,ai],label=f'Bias {name}')

        plt.title(f'{ax} axis')
        plt.grid(True)
        plt.xscale("log")
        plt.legend()
        plt.show()
        """
