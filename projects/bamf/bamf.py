# -*- coding: utf-8 -*-

import numpy as np
import numpy.ctypeslib as ctl

import ctypes

from photonpy.cpp.lib import NullableFloatArrayType, NullableIntArrayType

"""
Matched with C++ structure:

	float PSFsigma;      //Size of the PSF.
	int   N_Trials;      //Number of the jumps in the second part of the chain, which will be the output 
	int   N_Burnin;      //Number of the jumps in the first part of the chain, which will be thrown away    
	float I_stdFg, I_stdBg;         //The standard deviation of the normal-PDF which the random jumps in I are being taken from.
	float X_stdFg, X_stdBg;         //The standard deviation of the norma-PDF which the random jumps in position are taken from.
	float Rho;           //Mean number of enitters in ROI 
	float Icutoff;
	float Split_std;     //The standard deviation of the norm-PDf which the random Us are being taken from.
	float Bnd_out;       //The size of the area outside the image, where a proposed particle can be placed.
	int Grid_Zoom;       //The magnification of the output image.
	//float Split_std;     //The std of the normal distributions where you pick Us from.
	float FirstCall = 1;
	float BG_std, ABBG_std;
	float DX;
	float DriftX, DriftY;
	int IsBackg;

"""
class BAMF_Params(ctypes.Structure):
    _fields_ = [
        ("PSFsigma", ctypes.c_float),
    	("N_Trials", ctypes.c_int32),      
        ("N_Burnin", ctypes.c_int32),
    	("I_stdFg", ctypes.c_float),
        ("I_stdBg",ctypes.c_float),
    	("X_stdFg",ctypes.c_float),
        ("X_stdBg",ctypes.c_float),
    	("Rho",ctypes.c_float),
    	("Icutoff",ctypes.c_float),
    	("Split_std",ctypes.c_float),
    	("Bnd_out",ctypes.c_float),
    	("Grid_Zoom",ctypes.c_int32),
    	("BG_std",ctypes.c_float),
        ("ABBG_std",ctypes.c_float),
    	("DX",ctypes.c_float),
    	("DriftX",ctypes.c_float),
        ("DriftY",ctypes.c_float),
    	("IsBackg",ctypes.c_float),
    ]

    def __init__(self, r):
        for n in r.dtype.names:
            setattr(self, n, r[n][()])



class BAMF_RJMCMC:
    def __init__(self, lib):

        self.EmitterXYIType = np.dtype([('x','<f4',2),('y','<f4'),('I','<f4'), ('signal','<i4')])

        self.StateType = np.dtype([
            ('N', np.int32),
            ('LLR', np.float32),
            ('PR', np.float32),
            ('JumpType', np.int32),
            ('Accepted', np.int32),
            ('BG', np.float32),
            ('ABG', np.float32),
            ('BBG', np.float32)
            ])

        self._BAMF_RJMCMC_Run = lib.BAMF_RJMCMC_Run
                
        """ 
        BAMF_RJMCMC* BAMF_RJMCMC_Run(const float* data, 
            const BAMF_Emitter* E_active, int E_active_size, float E_active_bg, 
        	MCMC_ParametersBase* mcmc, const float* P_Burnin, 
            const float* P_Trials, const int* PSFsize,
        	int ROIxSize, int ROIySize,
        	const float* offsetPDF, int offsetPDFLen, //image
        	const float* signalPDF, int signalPDFLen,
        	const float* backgPDF, int backgPDFLen,
        	const float* sCMOSVar, // [ROIxSize,ROIySize]
        	const float* sampledPSF,
        	// output:
        	float* PImg,
        	float* PBg)
        """
        self._BAMF_RJMCMC_Run.argtypes = [
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"),
            ctl.ndpointer(self.EmitterXYIType, flags="aligned, c_contiguous"),
            ctypes.c_int32,
            ctypes.c_float, # E_active_bg
            ctypes.POINTER(BAMF_Params),  # p
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # P_Burnin float[8]
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # P_Trials float[8]
            ctypes.c_int32, # roix
            ctypes.c_int32, # roiy
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # offsetPDF 
            ctypes.c_int32, # offsetLPDFLen
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # signalPDF
            ctypes.c_int32, # signalPDFlen
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # backgPDF
            ctypes.c_int32, # backgPDFLen
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # sCMOSVar
            NullableFloatArrayType, # sampledPSF
            NullableIntArrayType, # PSFsize int4
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous"), # PImg 
            ctl.ndpointer(np.float32, flags="aligned, c_contiguous") # PBg 
        ]
        self._BAMF_RJMCMC_Run.restype = ctypes.c_void_p
        
        self._rjmcmc_delete  = lib.BAMF_Delete
        self._rjmcmc_delete.argtypes=[
            ctypes.c_void_p]
        
        #int BAMF_GetChainLen(BAMF_RJMCMC* r, int chain)
        #void BAMF_ReadChains(BAMF_RJMCMC* r, int chain, BAMF_State* states)
        #void BAMF_ReadChainEmitters(BAMF_RJMCMC* r, int chain, BAMF_Emitter* emitters)

        self._BAMF_GetChainLen = lib.BAMF_GetChainLen
        self._BAMF_GetChainLen.argtypes = [ctypes.c_void_p, ctypes.c_int32]
        self._BAMF_GetChainLen.restype = ctypes.c_int32
        
        self._BAMF_ReadChains = lib.BAMF_ReadChains
        self._BAMF_ReadChains.argtypes = [
            ctypes.c_void_p,
            ctypes.c_int32,
            ctl.ndpointer(self.StateType, flags="aligned, c_contiguous")
            ]
        self._BAMF_ReadChains.restype = ctypes.c_int32
        
        self._BAMF_ReadChainEmitters = lib.BAMF_ReadChainEmitters
        self._BAMF_ReadChainEmitters.argtypes =[
            ctypes.c_void_p,
            ctypes.c_int32,
            ctl.ndpointer(self.EmitterXYIType, flags="aligned, c_contiguous")
        ]
        
    def rjmcmc(self, image, params: BAMF_Params, E_active_bg, P_Burnin, 
               P_Trials, psf, offsetPDF, signalPDF, backgPDF, scmosVar):
        
        offsetPDF = np.ascontiguousarray(offsetPDF, dtype=np.float32)
        signalPDF = np.ascontiguousarray(signalPDF, dtype=np.float32)
        backgPDF = np.ascontiguousarray(backgPDF, dtype=np.float32)
        scmosVar = np.ascontiguousarray(scmosVar,dtype=np.float32)
        
        roix = image.shape[1]
        roiy = image.shape[0]
        image = np.ascontiguousarray(image,dtype=np.float32)
        pimg = np.zeros((roiy * params.Grid_Zoom, roix * params.Grid_Zoom ), dtype=np.float32)
        pbg = np.ascontiguousarray(pimg*0,dtype=np.float32)
        
        if psf is not None:
            psf = psf.ascontiguousarray(dtype=np.float32)
        
        emitters=np.zeros(shape=(0,),dtype=self.EmitterXYIType)
        r = self._BAMF_RJMCMC_Run(image, emitters, len(emitters), E_active_bg, params, 
                             P_Burnin, P_Trials, roix, roiy, 
                             offsetPDF, len(offsetPDF),
                             signalPDF, len(signalPDF),
                             backgPDF, len(backgPDF),
                             scmosVar,
                             psf,
                             psf.shape if psf is not None else None,
                             pimg, pbg
                             )
        
        def readChain(idx):
            chlen = self._BAMF_GetChainLen(r, idx)
            states = np.zeros(chlen, dtype=self.StateType)
            emitterCount = self._BAMF_ReadChains(r, idx, states)
            emitters = np.zeros(emitterCount, dtype=self.EmitterXYIType)
            self._BAMF_ReadChainEmitters(r, idx, emitters)
            return states, emitters
                    
        accepted = readChain(0)
        proposed = readChain(1)
        
        self._rjmcmc_delete(r)
        
        return pimg, pbg, accepted, proposed
    
        
        
        