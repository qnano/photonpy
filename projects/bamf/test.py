# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 05:25:15 2020

@author: jcnossen1
"""

import scipy.io as sio
import matplotlib.pyplot as plt
import numpy as np


from photonpy import Context
from bamf import BAMF_RJMCMC, BAMF_Params

I=1
call = sio.loadmat(f'rjmc-{I}-1-call.mat',squeeze_me=True)
result = sio.loadmat( f'rjmc-{I}-1-result.mat')

mcmc=call['MCMC']

#0 means using the analytical normal function as model 
#1 means use the input (4D) PSF 
optModel = call['OptModel']
sampledPSF = call['SampledPSF']
signalPDF = call['SignalPDF']
backgPDF = call['BackgPDF']
offsetPDF = call['OffSetPDF']
image = call['Data']
emitters = call['E_active']

stat = result['Stat']
proposedChain = result['ProposedChain']
acceptChain = result['AcceptChain']
PImg = result['PImg']
PBg = result['PBg']

print(len(acceptChain))

plt.figure()
plt.imshow(image)

#single(RJstruct.P_dP*(find(max(RJstruct.P_Offset)==RJstruct.P_Offset)-1)); %the uniform offset background

E_active_bg = call['E_active']['BG'][()]
P_Burnin =  mcmc['P_Burnin'][()]
P_Trials = mcmc['P_Trials'][()]
scmosVar = call['sCMOSVar']
    
print(f"Sampled psf shape:{sampledPSF.shape}")

if True:
    with Context(debugMode=False) as smlm:
        bamf = BAMF_RJMCMC(smlm.lib)
        
        params = BAMF_Params(mcmc)
        
        img, pbg, accepted,proposed = bamf.rjmcmc(image, params, E_active_bg, P_Burnin, P_Trials, 
                    None, offsetPDF, signalPDF, backgPDF, scmosVar )
        
        plt.figure()
        plt.imshow(np.concatenate([img,PImg],axis=-1))
        
        plt.figure()
        plt.plot(accepted[0]['N'])
        