""" 
Take a hi-res image and generate an SMLM movie from it at lower resolution and an alternative axis
"""
import numpy as np
import matplotlib.pyplot as plt

from photonpy.cpp.context import Context
from photonpy.cpp.gaussian import Gaussian, Gauss3D_Calibration
from photonpy.cpp.cspline import CSpline,CSpline_Calibration
import tqdm
import tifffile
import os


def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)



def generate_blinking_movie(psf, xyzI, numframes=100, 
                         imgsize=(512,512), bg=5, p_on=0.1):
    
    frames = np.zeros((numframes, *imgsize), dtype=np.float32)
    on_counts = np.zeros(numframes, dtype=np.int32)

    for f in tqdm.trange(numframes):
        on = np.random.binomial(1, p_on, len(xyzI))

        roisize = psf.sampleshape[0]
        roipos = np.clip((xyzI[:,[1,0]] - roisize/2).astype(int), 0, imgsize-roisize)
        theta = np.zeros((len(xyzI),5)) # assuming xyzIb
        theta[:,0:4] = xyzI
        theta[:,[1,0]] -= roipos
        on_spots = np.nonzero(on)[0]

        rois = psf.ExpectedValue(theta[on_spots])
        
        frames[f] = ctx.smlm.DrawROIs(imgsize, rois, roipos[on_spots])
        frames[f] += bg
        on_counts[f] = np.sum(on)
        
        frames[f] = np.random.poisson(frames[f])

    return frames, on_counts


def image_to_movie(img, image_axes, random_axis, intensity, on_spots_per_frame, psf, **kwargs):
    X,Y = np.meshgrid(np.arange(img.shape[1]), np.arange(img.shape[0])[::-1])
    
    xy = np.array([ X[img!=0].flatten(), Y[img!=0].flatten() ]).T
    pos = xy @ image_axes
    pos += np.random.uniform(size=len(xy))[:,None] * np.array(random_axis)[None,:]
    pos[:,0] += psf.sampleshape[-1]
    pos[:,1] += psf.sampleshape[-2]
        
    # Center Z
    z = pos[:,2]
    z = (z-np.min(z)) / (np.max(z)-np.min(z))
    pos[:,2] = psf.calib.z_min + z * (psf.calib.z_max-psf.calib.z_min)
    
    xyzI = np.zeros((len(pos),4))
    xyzI[:,:3] = pos
    xyzI[:,3] = np.random.uniform(intensity*0.5,intensity*1.5, size=len(pos))

    plt.figure()
    plt.scatter(pos[:,0], pos[:,1],s=0.05)    
    
    print(f"Number of spots: {len(pos)}")

    p_on = on_spots_per_frame / len(pos)
    imgsize = [
        int(np.max(pos[:,1])+psf.sampleshape[-2]),
        int(np.max(pos[:,0])+psf.sampleshape[-1])
            ]
    return generate_blinking_movie(psf=psf, xyzI=xyzI, imgsize=imgsize, p_on=p_on, **kwargs)[0]
    
def image_file_to_movie(imgfile, **kwargs):
    
    img = plt.imread(imgfile)
    img = img[:,:,0]
    img = np.max(img)-img

    plt.figure()
    plt.imshow(img,cmap='inferno')

    movie = image_to_movie(img, **kwargs)
    
    tifname = os.path.splitext(imgfile)[0]+"_storm_sim.tif"
    with tifffile.TiffWriter(tifname) as tif:
        for m in movie:
            tif.save(np.ascontiguousarray(m,dtype=np.uint16))
    
    return movie, tifname
    
if __name__ == "__main__":

    imgfn = 'helloworld.png'   

    if False:    
        with Context() as ctx:
            
            #calib = Gauss3D_Calibration()
            #psf = Gaussian(ctx).CreatePSF_XYZIBg(16, calib, True)
            calib = CSpline_Calibration.from_file_nmeth('simulated-tetrapod.mat')
            psf = CSpline(ctx).CreatePSF_XYZIBg(20, calib, True)
        
            axes = np.array([ [1,0,0], [0,0,1] ])*0.25
        
            mov, fn = image_file_to_movie(imgfn, 
                        image_axes= axes, 
                        random_axis = [0,40,0],
                        numframes=20000, 
                        intensity = 1000,
                        on_spots_per_frame=5,
                        psf=psf)
    else:
        from cspline_localize import localize
        from photonpy.smlm.render import render_gaussians
        
        fn = 'helloworld_storm_sim.tif'
        roisize = 16
        pixelsize = 0.100
        xyzI, imgshape = localize(fn, 'simulated-tetrapod.mat', pixelsize, roisize, chisq_threshold=4)
        z = xyzI[:,2]

        spots = np.zeros((len(xyzI),3))
        spots[:,0] = xyzI[:,0] * 4
        spots[:,1] = (z-np.min(z))/(np.max(z)-np.min(z)) * 360 + 20
        spots[:,2] = xyzI[:,3]

        rendered = render_gaussians(spots, (400,400), 1, 1)
        
        plt.figure()
        plt.imshow(rendered,origin='lower',cmap='inferno')
        plt.savefig(os.path.splitext(fn)[0] + '-result.png',dpi=300)
        