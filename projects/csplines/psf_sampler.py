# -*- coding: utf-8 -*-
"""
Testing procedure:
    
- Generate ground truth benchmark dataset
- Test precision and chi-squares for intensities on 2 different models:
    - Generated using Nature Methods tool
    - Generated using our alignment

"""

import pickle
from scipy.interpolate import interp1d,splrep,splev
import numpy as np
import matplotlib.pyplot as plt
import os
import tqdm
from pyotf.utils import fft_pad

from photonpy.smlm.psf import apply_shift_2D

from pyotf.phaseretrieval import ZernikeDecomposition
from pyotf.otf import HanserPSF

import copy
from pyotf.zernike import zernike,noll2name
import tifffile

class PSFGenerator:
    def __init__(self, model, phase_coeff, mag_coeff=None):

        self.phase_coeff = phase_coeff
        if mag_coeff is None:
            mag_coeff = np.zeros(len(phase_coeff))
            mag_coeff[0] = 1
        
        self.mag_coeff = mag_coeff
        
        assert len(mag_coeff)== len(phase_coeff)
        
        self.num_zerns=len(phase_coeff)

        # make a copy of the internal model
        self.model = copy.copy(model)
        
        model._gen_kr()
        r, theta = model._kr, model._phi
        self.r, self.theta = np.fft.fftshift(r), np.fft.fftshift(theta)

    def generate(self, roisize):
        """Fits the data to a number of zernikes"""
        # normalize r so that 1 = diffraction limit
        r, theta = self.r, self.theta
        r = r / (self.model.na / self.model.wl)
        # generate zernikes
        zerns = zernike(r, theta, np.arange(1, self.num_zerns + 1))
        self.zd_result = ZernikeDecomposition(self.mag_coeff, self.phase_coeff, zerns)

        self.complex_pupil=self.zd_result.complex_pupil()
        # generate the PSF from the reconstructed phase
        self.model._gen_psf(np.fft.ifftshift(self.complex_pupil))
        # reshpae PSF if needed in x/y dimensions
        psf = self.model.PSFi
        
        w = psf.shape[-1]
        psf = psf[:,w//2-roisize//2:w//2+roisize//2,w//2-roisize//2:w//2+roisize//2]
        

        return psf*1



def simulate_psf(zern, fn='psfsim.pickle'):

    if os.path.exists(fn):
        with open(fn, "rb") as f:
            d = pickle.load(f)
            if 'aberrations' in d and np.array_equal(zern,d['aberrations']):
                return d
            print(f"Zernike values have changed - regenerating psf\n")
        
        
    model = HanserPSF(wl=640, na=1.49, ni=1.517, res=50, size=1024,
                      zres=50, zsize=60, vec_corr="total")
    
    psfgen = PSFGenerator(model, zern)

    print(f"Generating PSF... ({fn})")
    psf = psfgen.generate(roisize=160)

    plt.figure()    
    plt.imshow( np.angle ( psfgen.complex_pupil) )
    
    d = dict({
        'psf': psf,
        'na': model.na,
        'ns' : model.ni,
        'wl' : model.wl,
        'zstep' : model.zres*0.001,
        'zrange': model.zrange*0.001,
        'pixelsize': model.res,
        'aberrations': zern
        })
    
    with open(fn, "wb") as f:
        pickle.dump(d, f)
        
    return d



def compute_com(img):
    X,Y=np.meshgrid(np.arange(img.shape[1]),np.arange(img.shape[0]))
                   
    #mean=np.mean(subimage)
    #var=np.sqrt(np.var(subimage))
    img = np.maximum(0,img-np.median(img))
    #subimage[subimage<(mean-var)]=0
    
    s = np.sum(img)
    if s > 0:
        comx = np.sum(img * X) / s
        comy = np.sum(img * Y) / s
    else:
        comx = img.shape[1]//2
        comy = img.shape[0]//2
        
    return comx,comy

def compute_crlb(mu,jac):
    K = len(jac)
    fi = np.zeros((K,K))
    for i in range(K):
        for j in range(K):            
            fi[i,j] = np.sum( 1/mu * (jac[i] * jac[j]))
            
    return np.sqrt(np.linalg.inv(fi).diagonal())

def crlb_with_numderiv(xyz, fun):
    """
    Compute CRLB of PSF model using numerical differentation
    """
    x,y,z = xyz

    h=1e-5
    xm = fun([x-h,y,z])
    xp = fun([x+h,y,z])
    ym = fun([x,y-h,z])
    yp = fun([x,y+h,z])
    zm = fun([x,y,z-h])
    zp = fun([x,y,z+h])
    center = fun(xyz)
    
    jac = [(xp-xm)/(2*h), (yp-ym)/(2*h), (zp-zm)/(2*h)]
    crlb = compute_crlb(center, jac)
    return crlb


class PSFSampler:
    """
    This uses the ground truth PSF stack to take samples from 
    """
    def __init__(self, zern, fn="psfsim.pickle"):
        self.d = simulate_psf(zern, fn)
        
        self.zrange=self.d['zrange']
        
        print(self.d.keys())
        
        #tck = interpolate.splrep(x_points, y_points)
        #return interpolate.splev(x, tck)
       
        psf = self.d['psf']
        W = psf.shape[-1]
        
        #spl = splrep(self.zrange, psf)
    
        self.compute_zplane = interp1d(self.zrange, self.d['psf'], axis=0, kind='cubic')
        
        self.M = 4
        self.imgshape = np.array(self.d['psf'].shape[1:])//self.M
        
        
    def sample(self, xyz, intensity, roisize=None):
        img = self.compute_zplane(xyz[2])
        
        M=self.M
        img = apply_shift_2D(img, [ xyz[1]*M, xyz[0]*M] ) 
        
        W = img.shape[-1]
        img = img.reshape((W//M,M,W//M,M)).sum((1,3))
        if roisize is not None:
            S=img.shape[0]
            img = img[S//2-roisize//2:S//2+roisize//2,
                      S//2-roisize//2:S//2+roisize//2]
        img = intensity * (img/np.sum(img))
        
        return img

    def crlb(self, xyz, intensity, roisize, background):
        
        return crlb_with_numderiv(xyz, 
                lambda pos: self.sample(pos,intensity,roisize)+background)

    #estimate_psf_fftalign(d)   
    
        