# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 00:30:08 2020

@author: jcnossen1
"""
import numpy as np
import matplotlib.pyplot as plt

from photonpy.cpp.context import Context
from photonpy.cpp.cspline import CSpline_Calibration, CSpline
from photonpy.cpp.gaussian import Gaussian,Gauss3D_Calibration
from photonpy.cpp.estimator import Estimator
from photonpy.cpp.estim_queue import EstimQueue
from photonpy.cpp.spotdetect import PSFConvSpotDetector, SpotDetectionMethods
import math
from photonpy.smlm.psf import psf_to_zstack
from photonpy.smlm.util import imshow_hstack
import time
import tqdm

from photonpy.utils.multipart_tiff import tiff_read_file, tiff_get_movie_size


def process_movie(mov, imgshape, num_frames, spotDetector, roisize, ctx:Context):
    roishape = [roisize,roisize]

    img_queue, roi_queue = SpotDetectionMethods(ctx).CreateQueue(imgshape, roishape, spotDetector)
    t0 = time.time()

    for i,img in mov:
        img_queue.PushFrame(img)
   
    while img_queue.NumFinishedFrames() < num_frames:
        time.sleep(0.1)
    
    dt = time.time() - t0
    print(f"Processed {num_frames} frames in {dt:.2f} seconds. {num_frames/dt:.1f} fps\n")
    
    rois, data = roi_queue.Fetch()
    roipos = np.array([rois['x'],rois['y'],rois['z']]).T
    return roipos, data


def localization(psf, rois, initial_guess):
    with tqdm.tqdm(total=len(rois)) as pb:
        est_queue = EstimQueue(psf, batchSize=1024)
        est_queue.Schedule(rois, ids=np.arange(len(rois)), initial=initial_guess)
        est_queue.Flush()
    
        last=0
        while True:
            rc = est_queue.GetResultCount()
            pb.update(rc-last)
            last = rc
            if (rc == len(rois)):
                break
        
    r = est_queue.GetResults()
    est_queue.Destroy()

    r.SortByID(isUnique=True)
    return r.estim, r.ids


def localize(fn, cspline_fn, pixelsize, roisize=18, detection_threshold=10, chisq_threshold=2):
    
    with Context() as ctx:
        cspline_calib = CSpline_Calibration.from_file_nmeth(cspline_fn)
        psf = CSpline(ctx).CreatePSF_XYZIBg(roisize, cspline_calib, cuda=True)
    
        psf_zrange = np.linspace(cspline_calib.zrange[0]*0.9,
                                 cspline_calib.zrange[1]*0.9, 10)
        psfstack = psf_to_zstack(psf, psf_zrange)

        imgshape, numframes = tiff_get_movie_size(fn)
        bgimg = np.zeros(imgshape)

        # this sets up the template-based spot detector. MinPhotons is not actually photons, still just AU.
        sd = PSFConvSpotDetector(psfstack, bgimg, minPhotons=detection_threshold, maxFilterSizeXY=5, debugMode=False)
        
        mov = tiff_read_file(fn)
        roipos, rois = process_movie(mov, imgshape, numframes, sd, roisize, ctx)
            
        plt.figure()
        hist = np.histogram(roipos[:,2],bins=len(psf_zrange),range=[0,len(psf_zrange)])
        plt.bar(psf_zrange, hist[0], width=(psf_zrange[-1]-psf_zrange[0])/len(hist[0]))
        plt.xlabel('Z position [um]')
        plt.title('Z position initial estimate from PSF convolutions')
    
        imshow_hstack(rois)
    
        initial_guess = np.ones((len(rois), 5)) * [roisize/2,roisize/2,0,0,1]
        initial_guess[:,2] = psf_zrange[roipos[:,2]]
        initial_guess[:,3] = np.sum(rois,(1,2))
    
        estim, ids = localization(psf, rois, initial_guess)
        rois = rois[ids]
        roipos = roipos[ids]
        
        # Filter out all ROIs with chi-square, this gets rid of ROIs with multiple spots.
        expval = psf.ExpectedValue(estim)
        chisq = np.sum( (rois-expval)**2 / (expval+1e-9), (1,2))
        
        std_chisq = np.sqrt(2*psf.samplecount + np.sum(1/np.mean(expval,0)))
    
        # Filter out all spots that have a chi-square > expected value + 2 * std.ev.
        chisq_threshold = psf.samplecount + chisq_threshold*std_chisq
        sel = chisq < chisq_threshold
        print(f"Chi-Square threshold: {chisq_threshold:.1f}. Removing {np.sum(sel==False)}/{len(rois)} spots")
    
        plt.figure()
        plt.hist(chisq,bins=100,range=[0,1000])
        plt.gca().axvline(chisq_threshold,color='r', label='threshold')
        plt.title('Chi-Square values for each localization')
        plt.legend()
    
        estim[:,[0,1]] += roipos[:,[0,1]]
        estim = estim[sel]
    
        plt.figure()    
        plt.scatter(estim[:,0]*pixelsize, estim[:,2],s=1.5, label='Estimated')
        plt.xlabel('X [microns]'); plt.ylabel('Z [microns]')
        plt.legend()
        plt.title(f'X-Z section [{len(estim)} spots]')
        print(f"#spots: {len(estim)}")
        
        return estim, imgshape