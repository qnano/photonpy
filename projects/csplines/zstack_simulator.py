# -*- coding: utf-8 -*-
"""
Created on Tue May  5 17:46:35 2020

@author: jcnossen1
"""
import numpy as np
import matplotlib.pyplot as plt
from psf_sampler import  PSFSampler,crlb_with_numderiv
import tqdm
import pickle
import os

from photonpy.cpp.phasor import Localize as PhasorLocalize
from photonpy.cpp.cspline import CSplineMethods, CSplineCalibration
from photonpy.cpp.context import Context
from photonpy.cpp.gaussian import Gaussian

from photonpy.smlm.psf import (apply_shift_3D, apply_shift_2D,
                               upscale_fft, find_shift)
from pyotf.utils import fft_pad

import tifffile

from photonpy.utils.findpeak1D import quadraticpeak,gaussianpeak



def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)

def generate_benchmark_data(sampler:PSFSampler, roisize, fn, nspots, zrange, xyrange):
    
    zpos=np.linspace(zrange[0],zrange[1],nspots)
    
    pos = np.random.uniform([-xyrange/2,-xyrange/2,0],
                            [xyrange/2,xyrange/2,0], size=(nspots,3))
    
    pos[:,2] = zpos

    data = np.zeros((nspots,roisize,roisize),dtype=np.float32)

    with tqdm.tqdm(total=nspots) as pb:
        for j in range(nspots):
            data[j] = sampler.sample(pos[j],1,roisize)
            pb.update(1)
            
    with open(fn, "wb") as f:
        pickle.dump((pos, data),f)
                
    return (pos,data)

def simulate_zstack(sampler, numbeads, zshift_range, zrange, intensity):
    xyz_max = np.array([2,2,zshift_range/2])
    xyz = np.random.uniform(-xyz_max,xyz_max,size=(numbeads,3))

    zstack = np.zeros((numbeads, len(zrange), *sampler.imgshape))
    
    for b in tqdm.trange(numbeads):
        for i in range(len(zrange)):
            pos = xyz[b]*1
            pos[2] += zrange[i]
            zstack[b,i] = sampler.sample(pos, intensity)
    
    return zstack, xyz

    

def export_zstack(zstack, fn, background):
    """
    Line up all the beads in a grid so we can fit it using other PSF fitting software
    (think zola, or jonas ries group csplines)
    """
    numbeads = zstack.shape[0]
    slices = zstack.shape[1]
    roi_h = zstack.shape[2]
    roi_w = zstack.shape[3]
    cols = int(np.sqrt(numbeads))
    
    pitch_w = roi_w*2
    pitch_h = roi_h*2
    
    w = pitch_w*cols
    h = ((numbeads + cols - 1) // cols) * pitch_h
    
    merged = np.random.poisson(background, size=(slices, h, w)).astype(np.uint16)
    for z in range(slices):
        for b in range(numbeads):
            y = (b//cols) * pitch_h + (pitch_h-roi_h)//2
            x = (b%cols) * pitch_w + (pitch_w-roi_w)//2
            merged[z, y:y+roi_h, x:x+roi_w] = zstack[b, z]

    with tifffile.TiffWriter(fn) as tif:
        for i in range(len(merged)):
            tif.save(merged[i])
    
    print(f"Written {fn}")
    

def load_simulated_zstack(zern, psf_fn):
    fn = os.path.splitext(psf_fn)[0]+"-calib-sim.pickle"
    if not os.path.exists(fn):
        sampler = PSFSampler(zern, psf_fn)
         
        intensity = 10000
        zshift_range=0.2
        numbeads = 100
        zsteps = 51
        background = 10
        
        zrange = np.linspace(sampler.zrange[0]+zshift_range/2,sampler.zrange[-1]-zshift_range/2,zsteps)
        print(f"Z step: {zrange[1]-zrange[0]:.2f} nm")
        zstack,xyz = simulate_zstack(sampler, numbeads, zshift_range, zrange=zrange, intensity=intensity)
        zstack = np.random.poisson(zstack+background)
        
        export_zstack(zstack, os.path.splitext(fn)[0] + ".tif", background)

        with open(fn, "wb") as f:
            pickle.dump({
                'zstack': zstack, 
                'zrange':zrange,
                'xyz':xyz,
                'psf': sampler.d}
                ,f)
            
    with open(fn,"rb") as f:
        d = pickle.load(f)
        
    return d

    


def test_3D_shift(zstack):
    
    true_shifts = np.random.uniform(-3,3,size=(50,3))
    
    errs = []
    
    for k in tqdm.trange(len(true_shifts)):
        zstack_sh = apply_shift_3D(zstack, true_shifts[k])
        sh = find_shift(zstack, zstack_sh, plotTitle=f"sh={true_shifts[k]}" if k==0 else None)
        
        errs.append(sh-true_shifts[k])
        
    print(np.std(errs,0))
    
def normalize_zstack(result):
    return result / np.sum(result, (1,2))[:,None,None]

def plot_crlb(zern, psf_fn):
    with Context() as ctx:

        # ground truth psf
        gt_psf = PSFSampler(zern,psf_fn)
        
        fn = 'psfsim.zstack'
        roisize= 20
        psf = CSplineMethods(ctx).CreatePSFFromFile(roisize, fn)
        calib=psf.calib
        
        calib2 = CSplineCalibration.from_file('psfsim-calib-sim_3Dcorr.mat')
        psf2 = CSplineMethods(ctx).CreatePSF_XYZIBg(roisize, calib2, True)
            
        gpsf = Gaussian(ctx).CreatePSF_XYIBg(roisize,sigma=1.5,cuda= True)
        
        
        N = 200
        ax=2
        bg=1
        theta = np.repeat([[roisize/2,roisize/2,0,1000,bg]], N, axis=0)
        theta[:,ax] += np.linspace(calib.zrange[0]*0.9,calib.zrange[1]*0.9,N)
        #theta[:,2] = np.linspace(-0.5,0.5, N)
        theta_gauss=theta[:,[0,1,3,4]]
        
        crlb = psf.CRLB(theta)
        crlb2 = psf2.CRLB(theta)
        crlb_gauss=gpsf.CRLB(theta_gauss)

        psf_crlb=np.zeros((len(theta),3))
        gt_crlb=np.zeros((len(theta),3))
        gt_ev=[]
        for i in tqdm.trange(len(theta)):
            xyz=theta[i,:3]*1
            xyz[:2] -= roisize/2
            gt_crlb[i] = gt_psf.crlb(xyz, theta[i,3], roisize, bg)
            gt_ev.append(gt_psf.sample(xyz,theta[i,3],roisize)+bg)
            nd_fun = lambda pos: psf.ExpectedValue([[pos[0]+roisize/2, pos[1]+roisize/2, pos[2], theta[i,3], bg]]).astype(np.float64)
            psf_crlb[i] = crlb_with_numderiv(xyz, nd_fun)
            
        smp = psf.ExpectedValue(theta)
        gauss = gpsf.ExpectedValue(theta_gauss)
            
        pixelsize=100
        plt.figure()
        plt.plot(theta[:,2], crlb[:,2] * 1000, label="Z (FFT align)")
        plt.plot(theta[:,2], crlb[:,0] * pixelsize, label="X (FFT align)")
        plt.plot(theta[:,2], crlb[:,1] * pixelsize, label="Y (FFT align)")
        #plt.plot(theta[:,2], psf_crlb[:,1] * pixelsize, label="Y (FFT align - nd check)")
    
        plt.plot(theta[:,2], crlb2[:,2] * 1000, '--', label="Z (NM)")
        plt.plot(theta[:,2], crlb2[:,0] * pixelsize,'--', label="X (NM)")
        plt.plot(theta[:,2], crlb2[:,1] * pixelsize, '--', label="Y (NM)")

        plt.plot(theta[:,2], gt_crlb[:,2] * 1000, ':', label="Z (ground truth)")
        plt.plot(theta[:,2], gt_crlb[:,0] * pixelsize,':', label="X (ground truth)")
        plt.plot(theta[:,2], gt_crlb[:,1] * pixelsize, ':', label="Y (ground truth)")

        #plt.plot(theta[:,2], crlb_gauss[:,0] * pixelsize, '.-r', label="XY (Gaussian)")
    
        plt.legend()
        plt.ylabel('CRLB [nm]')
        plt.xlabel('Z position [um]')
        plt.show()
        
        plt.figure()
        #zpos = np.repeat(np.linspace(-1,1,50))
        estim_z = psf.Estimate(smp)[0][:,ax]
        plt.plot(theta[:,ax], estim_z, label="Estimated")
        plt.plot(theta[:,ax], theta[:,ax], label='Ground truth')
        plt.xlabel("Z position [um]")
        plt.ylabel("Z position [um]")
        plt.title('Estimated Z vs ground truth Z')
        
        show_napari(np.concatenate( [gt_ev, smp],-1))
        

def benchmark_psfs():
    with open("tetrapod-benchmark.pickle", "rb") as f:
        gtpos, data = pickle.load(f)
        
    print(data.shape)
    
    # Open the ZStack used to build the cspline psf
    with open("tetrapod-zstack.pickle", "rb") as f:
        zstack = pickle.load(f)
    
    with Context() as ctx:
        psf = CSpline(ctx).CreatePSFFromZStack(roisize, 
                                         zstack['zstack'], 
                                         zstack['zrange'], cuda=True)


if __name__ == "__main__":
    #test_shifting()
    np.random.seed(0)
    max_phase=0.0
    phase_zc = np.zeros(25)#np.random.uniform(-max_phase,max_phase,size=25)
    phase_zc[11] = 5 # tetrapody
    psf_fn ='psfsim.pickle' 

    d = load_simulated_zstack(phase_zc, psf_fn)
    
    #test_3D_shift(d['zstack'][0])
    
    #zstack = align_zstack_fft(d['zstack'],2)
    
    #show_napari(zstack)

    #test_1D_shift(d['zstack'][0,0])
    #test_xy_find_shift(500)
    
    #test_apply_shift_xy(d['zstack'][0,0],200)
    
    #test_zshift(d['zstack'][2],100)
    
    if False:
        plot_crlb(phase_zc,psf_fn)

    if False: # generate the cspline zstack    
        result, aligned_zstacks, beadxy, beadz = align_zstacks(d['zstack'][:50])
        result = normalize_zstack(result)
        
        with open( os.path.splitext(psf_fn)[0]+ "-zstack.pickle", "wb") as f:
            pickle.dump({'zstack':result, 'zrange':d['zrange']},f)
            
        from photonpy.smlm.psf import save_zstack
        save_zstack(result,d['zrange'],os.path.splitext(psf_fn)[0]+".zstack")
    
        show_napari(result)
            
    if False: # generate benchmark data
        sampler=PSFSampler()
        gtpos,data = generate_benchmark_data(sampler, 40, "tetrapod-benchmark.pickle",  5000, sampler.zrange[[0,-1]], xyrange=6)

    if False: # benchmark using our zstack and nmeth psf
        benchmark_psfs()
    
    