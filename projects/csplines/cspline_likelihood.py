# Steps to run this (remove spyder if you use another UI):
# 
# conda create -n splinetestenv python=3.7 spyder
# conda activate splinetestenv
# pip install photonpy==1.0.32
#
import numpy as np
import matplotlib.pyplot as plt

from photonpy import Context, Estimator
from photonpy.cpp.cspline import CSpline_Calibration, CSpline
import time

class MultiEmitterLLEval:
    """
    Evaluates the loglikelihood of samples allowing multiple emitters per sample
    To be implemented in c++/cuda later to speed up
    """
    def __init__(self, maxEmitters, samples, psf:Estimator):
        self.maxEmitters = maxEmitters
        self.samples = samples
        self.psf = psf
        
    def ExpectedValue(self, emitterCounts, emitterParams, backgroundParams):
        roisize = psf.sampleshape[0] #assume squared ROIs
        X,Y = np.meshgrid(np.arange(roisize)-roisize/2, np.arange(roisize)-roisize/2)
        expval = backgroundParams[:,0,None,None] + X[None] * backgroundParams[:,1,None,None] + Y*backgroundParams[:,2,None,None]
        
        # Compute the expected value
        for i in range(self.maxEmitters):
            # which samples still need to have more PSF evaluations to compute the summed expected value?
            todo = np.where(i < emitterCounts)[0] 
            if len(todo) == 0:
                break
            
            # generate single-emitter parameters to evaluate the psf
            params = np.zeros( (len(todo), psf.numparams),dtype=np.float32 )  # 5 parameters: X,Y,Z,intensity,background
            params[:,:-1] = emitterParams[todo][:,i,:-1]
            
            ev = psf.ExpectedValue(params)
            expval[todo] += ev
            
        return expval
    
    def Loglikelihood(self, sampleIndices, emitterCounts, emitterParams, backgroundParams):
        """
        Computes the Poisson log-likelihood for given parameters+samples
        
        Parameters
        ----------
        sampleIndices : 
            array of int indices into the set of samples
            
        emitterCounts: 
            1D array [len(sampleIndices)]: number of emitters for each sample, length len(sampleIndices)
            
        emitterParams:
            3D parameter array: float32 [len(sampleIndices), maxEmitters, 4]
            each emitter has 4 parameters: X,Y,Z,Intensity
        
        backgroundParams:
            2D array with background parameters. float32 [len(sampleIndices), 3]
            each sample's background is defined by 3 parameters: a + b * x + c * y
            
        Returns float[len(sampleIndices)], the Poisson loglikelihood as computed by k*log(mu)-mu, 
        where mu is the expected value, and k is the number of photons
        """
        
        expval = self.ExpectedValue(emitterCounts, emitterParams, backgroundParams)
            
        # Compute log-likelihood. The factorial part is skipped as it will drop out anyway in a ratio test (and is slow)
        return self.samples[sampleIndices] * np.log(expval) - expval
    
    
    
with Context(debugMode=False) as ctx:
    roisize = 40

    # Helps debugging
    np.random.seed(0)
    
    psf_fn = 'tetrapod_0610.mat'

    cspline_api = CSpline(ctx)
    psf = cspline_api.CreatePSFFromFile(roisize, psf_fn)
    
    zrange = [-0.6,0.6]
    
    N = 10
    theta = np.zeros((N, psf.numparams))
    theta[:,3] = 2000 # 1000 photons/emitter 
    theta[:,4] = 10 # 10 photons/pixel background
    theta[:,[0,1]] = roisize/2  # in the middle of the ROI
    theta[:,2] = np.linspace(zrange[0],zrange[1],N)
    print(f"Z range for given psf: {psf.calib.zrange} [um]. Using range: {zrange}")
    
    # Generate N samples
    expval = psf.ExpectedValue(theta)
    smp = np.random.poisson(expval)
    
    plt.figure()
    plt.imshow(np.concatenate(smp,-1))

    ll_eval = MultiEmitterLLEval(4, smp, psf)

    n_eval = 10 #
    maxEmitters = ll_eval.maxEmitters
    # for the sake of flexibility the C++/CUDA code will allow you to specify which stored sample must be used to evaluate LL on.
    # we current just use one sample:
    smpIndices = np.ones(n_eval,int) * 3   # the 3rd sample
    emitterParams = np.zeros((n_eval, maxEmitters, 4))
    # some random values for emitters
    border = 10
    emitterParams[:,:,:2] = np.random.uniform(border, roisize-1-border, size=(n_eval,maxEmitters,2))  # X,Y [pixels]
    emitterParams[:,:,2] = np.random.uniform(zrange[0], zrange[1], (n_eval,maxEmitters))    # Z  [um]
    emitterParams[:,:,3] = np.random.uniform(200, 1000, size=(n_eval,maxEmitters))          # intensities
                  
    backgroundParams = np.zeros((n_eval, 3))
    backgroundParams[:,0] = 10 # photons/pixel, flat background

    counts = np.random.randint(1,maxEmitters,size=len(smpIndices))
    
    ev = ll_eval.ExpectedValue(counts, emitterParams, backgroundParams)
    plt.figure()
    plt.imshow(np.concatenate(ev,-1))
    
    # plot expected value 
    ll = ll_eval.Loglikelihood(smpIndices, counts, emitterParams, backgroundParams)
    print(f"Emitter counts in evaluations: {counts}")
    print(f"Loglikelihood for evaluations: {ll}")
    
    
    