# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 20:08:50 2020

@author: jelmer


2D definitions:
    
 01 ---- 11
 |        |
 |        |
 |        |
 00 ---- 10

"""

import numpy as np
import matplotlib.pyplot as plt

hermiteBasis = np.array([[2, -2, 1, 1],
     [-3, 3,-2,-1],
     [0,  0, 1, 0],
     [1,  0, 0, 0]])

catmullRom = np.array([[0,1,0,0],
              [0,0,1,0],
              [-0.5,0,0.5,0],
              [0,-0.5,0,0.5]])

def splineWeights1D(t):
    t2=t*t
    w = np.zeros((*t.shape,4))
    w[...,0] = 2*t2*t-3*t2+1
    w[...,1] = t2*t-2*t2+t
    w[...,2] = -2*t2*t+3*t2
    w[...,3] = t2*t-t2

    return w

def computeSpline(y,dydt, t):
    idx = np.array(t).astype(np.int32)
    w = splineWeights1D(t-idx)
    return w[...,0] * y[idx] + w[...,1] * dydt[idx] + w[...,2] * y[idx+1] + w[...,3] * dydt[idx+1]

def catmulRomWeights(t):
    

    t_ = np.zeros((*t.shape,4))
    t_[:,0] = t**3
    t_[:,1] = t**2
    t_[:,2] = t
    t_[:,3] = 1
    
    return t_ @ (hermiteBasis @ catmullRom)
    



def spline1D():
    L = 8
    pt = np.random.uniform(0,1,size=(L+1))
    t = np.linspace(0,L,200,endpoint=False)
    
    #y = computeSpline(pt, t)
    
    # Computing tangents from finite difference like this results in a Catmull-Rom spline.
    dydx = np.array([0, *( (pt[2:]-pt[:-2])/2 ), 0])
    
    y = computeSpline(pt, dydx, t)
    
    plt.figure()
    plt.plot(pt, 'o', label='Control points')
    plt.plot(t, y, label='Spline')
    plt.legend()

    w=catmulRomWeights(np.linspace(0,1,20))
    plt.figure()
    plt.plot(w[:,0])
    
def solveHermiteMatrix1D():
    # build matrix to solve A*w = b
    # where w are the hermite weights
    A = np.zeros((16,16))
    b = np.zeros(16)
    
    def t_(t):
        return [t**3, t**2,t,1]
    def t_deriv(t):
        return [3*t**2, 2*t, 1, 0]
    
    def setEq(i, tv, out):
        for j in range(4):
            A[4*i+j,4*j:(4*j)+4] = tv
            b[4*i+j] = out[j]
        
    setEq(0, t_(0), [1,0,0,0])
    setEq(1, t_(1), [0,1,0,0])
    setEq(2, t_deriv(0), [0,0,1,0])
    setEq(3, t_deriv(1), [0,0,0,1])
    
    w = np.linalg.solve(A,b)
    
    print(w.reshape((4,4)).T)
    
    
                 
    