
import math
import numpy as np
import numpy.linalg as npl
import matplotlib.pyplot as plt

from photonpy import Context,GaussianPSFMethods,Gauss3D_Calibration
from photonpy.cpp.dspline import DSplineMethods

import tqdm

def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)
        
        
def spline1D(x, weights):
    hermiteBasis = np.array([
  			[1, 0, 0, 0],
  			[0, 1,0, 0],
  			[-3,-2,3,-1],
  			[2, 1,-2, 1]
              ])
    
    vx = int(x)
    sx = x-vx
    
    val = 0
    for a in range(4):
        for i in range(4):
            val += sx**a * hermiteBasis[a,i] * weights[vx+(i//2), i%2]

    return val

def test1D():
    x = np.linspace(1.5,6)
    weights = np.zeros((10,2))
    weights[:,0] = np.random.uniform(size=10)
    y = [spline1D(x[i], weights) for i in range(len(x))]

    plt.clf()
    plt.plot(x,y)
    plt.plot(weights[:,0], 'o')
    
    

def spline2D(x,y, weights):
    hermiteBasis = np.array([
  			[1, 0, 0, 0],
  			[0, 1,0, 0],
  			[-3,-2,3,-1],
  			[2, 1,-2, 1]
              ])
    
    vx = int(x)
    sx = x-vx
    
    vy = int(y)
    sy = y -vy
    
    val = 0

    for a in range(4):
        for i in range(4):
            
            for b in range(4):
                for j in range(4):
#                    w = weights[vy+(j//2), vx+(i//2), (i+j*2)& 3  ]
                    w = weights[vy+(j//2), vx+(i//2), i%2 + (j%2)*2  ]
                    val += sx**a * sy**b * hermiteBasis[a,i] * hermiteBasis[b,j] * w

    return val


def test2D():
    
    L=60
    D=6
    X,Y = np.meshgrid(np.linspace(0,D-1.1,L), np.linspace(0,D-1.1,L))
    X=X.flatten()
    Y=Y.flatten()
    
    weights = np.zeros((D,D,4))
    weights[:,:,0] = np.random.uniform(size=weights.shape[:-1])

    y =np.array( [spline2D(X[i], Y[i], weights) for i in range(len(X))])

    fig,ax=plt.subplots(2)
    ax[0].imshow(weights[:,:,0])
    ax[1].imshow(y.reshape((L,L)))

def testSubsample():
    with Context(debugMode=True) as ctx:
        dsm = DSplineMethods(ctx)
    
        roisize = 1
        D = 10
        W = 6
    
        L=50
        X,Y = np.meshgrid(np.linspace(-1,D-1.1,L), np.linspace(-1,D-1.1,L))
        X=X.flatten()
        Y=Y.flatten()
    
        pos = np.zeros((len(X),3))
        pos[:,0] = X
        pos[:,1] = Y
        
        #weights = np.random.uniform(size=(D,W,W,8))
        weights = np.zeros((D,W,W,8),dtype=np.float32)
        #weights[:,2,2,0] = 1
        weights[:,:,:,0] = np.random.uniform(size=(D,W,W))
        #weights[:,2,2,0] = 1
        #X,Y = np.meshgrid(np.arange(W),np.arange(W))
        #weights[:,:,:,0] = np.exp(-(X**2+Y**2)/(10))[None,:,:]
    
        output = dsm.Eval(pos, roisize, weights)
        output = output[:,0,0].reshape((L,L))
        
        fig,ax=plt.subplots(2)
        ax[0].imshow(output)
        ax[0].set_title('Output')
        ax[1].imshow(weights[0,:,:,0])
        ax[1].set_title('Weights[...,0]')


def testSubsample2():
    with Context(debugMode=True) as ctx:
        dsm = DSplineMethods(ctx)
    
        roisize = 1
        D = 7
        W = 6
    
        L=50
        X,Y = np.meshgrid(np.linspace(-1,D-1.1,L), np.linspace(-1,D-1.1,L))
        X=X.flatten()
        Y=Y.flatten()
    
        pos = np.zeros((len(X),3))
        pos[:,0] = X
        pos[:,1] = Y
        
        #weights = np.random.uniform(size=(D,W,W,8))
        weights = np.zeros((D*2,W*2,W*2),dtype=np.float32)
        #weights[:,2,2,0] = 1
        weights[::2,::2,::2] = np.random.uniform(size=(D,W,W))
    
        output = dsm.Eval2(pos, roisize, weights)
        output = output[:,0,0].reshape((L,L))
        
        fig,ax=plt.subplots(2)
        ax[0].imshow(output)
        ax[0].set_title('Output')
        ax[1].imshow(weights[0,::2,::2])
        ax[1].set_title('Weights[...,0]')


def testROI2():
    with Context(debugMode=True) as ctx:
        dsm = DSplineMethods(ctx)
    
        roisize = 12
        D = 5
        W = 15
    
        X,Y = np.meshgrid(np.arange(W)-W//2,np.arange(W)-W//2)
        
        #weights = np.random.uniform(size=(D,W,W,8))
        weights = np.zeros((D*2,W*2,W*2),dtype=np.float32)
        weights[::2,::2,::2] = np.exp(-(X**2+Y**2)/(10))[None,:,:]
        
        #plt.figure()
        #plt.imshow(weights[0,:,:,0])
    
        N=40
        pos = np.zeros((N,3))
        pos[:,1] = roisize/2
        pos[:,0] = np.linspace(0,roisize-1,N)
    
        output = dsm.Eval2(pos, roisize, weights)

        fig,ax=plt.subplots(2)
        ax[0].imshow(output[0])
        ax[0].set_title('Output')
        ax[1].imshow(weights[0,::2,::2])
        ax[1].set_title('Weights[...,0]')
    
        show_napari(output)
        

def testEstimate():
    with Context(debugMode=True) as ctx:
        dsm = DSplineMethods(ctx)
        roisize = 12
        D = 5  # spline depth
        W = 12 # spline dimensions

        sigma=1.5
        g = Gaussian(ctx)
        #psf=g.CreatePSF_XYZIBg(roisize, Gauss3D_Calibration(), cuda=True)
        
    
        X,Y = np.meshgrid(np.arange(W)-W//2,np.arange(W)-W//2)
        
        #weights = np.random.uniform(size=(D,W,W,8))
        
        def makeWeights(sigma):
             
            weights = np.zeros((D*2,W*2,W*2),dtype=np.float32)
            psf = np.exp(-(X**2+Y**2)/(2*sigma**2))
            psf /= np.sum(psf)
            weights[::2,::2,::2] = psf[None,:,:]
            return weights
        
        weights = makeWeights(2)
        
        #plt.figure()
        #plt.imshow(weights[0,:,:,0])
    
        I=400
        bg=10
        N=40
        pos = np.zeros((N,3))
        pos[:,1] = roisize/2
        pos[:,0] = np.linspace(0,roisize-1,N)
        pos[:,2] = 0.2
    
        output = dsm.Eval2(pos, roisize, weights)
        
        smp = np.random.poisson(output*I+bg)
       
        pixelBg = np.ones(smp.shape)*bg
        intensities = np.ones(len(pos))*I
        
        weights = makeWeights(1)
        wg,psf_,roi_ll=dsm.ComputeWeightsGradient(pos, intensities, weights, smp, pixelBg)

        fig,ax=plt.subplots(2)
        ax[0].imshow(output[0])
        ax[0].set_title('Output')
        ax[1].imshow(weights[0,::2,::2])
        ax[1].set_title('Weights[...,0]')
    
        plt.figure()
        plt.plot(psf_[:,roisize//2,roisize//2],label='wg')
        plt.plot(output[:,roisize//2,roisize//2],label='output')
        plt.legend()
        #show_napari(wg[::2,::2,::2])

        plt.figure()
        plt.imshow(wg[0,::2,::2])

        show_napari( np.concatenate([output*I+bg,smp],-1))         
        return wg,psf_

def generate_movie(cb, nframes, fps):
    from matplotlib import animation, rc
        
    fig=plt.figure()
    mirror_anim = animation.FuncAnimation(fig, 
        cb, frames=nframes, interval=1/fps, blit=False,repeat=True)
        
    mirror_anim.save('zimflux-move-mirror.gif', fps=fps,writer='pillow')
            
    mirror_anim.save('zimflux-change-both.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
    

    
def optimizeWeights(pos, intensities, weights, smp, pixelBg,ctx:Context):
    
    dsm = DSplineMethods(ctx)

    lastscore = 0
    rejectCount = 0    
    stepsize = 1e-12
    prevWeights = None
    prev_ll = 0

    i = 0
    with tqdm.tqdm() as pb:
        while True:    
            weightsGradient,psf_,roi_ll=dsm.ComputeWeightsGradient(pos, intensities, weights, smp, pixelBg,ctx)
            total_ll = np.sum(roi_ll)
            
            if i == 0 or total_ll > prev_ll:
                info = f'{i}. Accepting step. LL:{total_ll:.2f}. Stepsize: {stepsize:.2e}'
            
                stepsize *= 2
                prevWeights = weights
                rejectCount=0
                prevWeightsGradient = weightsGradient
                prev_ll = total_ll
            else:
                if rejectCount == 20:
                    break
                info=f'{i}. Rejecting step. LL:{total_ll:.2f}. Stepsize: {stepsize:.2e}'
                weights = prevWeights
                weightsGradient = prevWeightsGradient
                stepsize *= 0.2
                rejectCount +=1

            print(info)
            #pb.set_description(info)
    
            weights += weightsGradient*stepsize
            #print(total_ll)
            i+=1
            #pb.update(1)
        
    return weights
    
        
def testGradient():
    with Context(debugMode=False) as ctx:
        dsm = DSplineMethods(ctx)
            
        roisize = 12
        D = 5  # spline depth
        W = 15 # spline dimensions

        g = GaussianPSFMethods(ctx)
        sigma = 2
        gauss_psf = g.CreatePSF_XYIBg(W, sigma, cuda=False)
    
        #X,Y = np.meshgrid(np.arange(W)-W//2,np.arange(W)-W//2)
        
        #weights = np.random.uniform(size=(D,W,W,8))
        
        def makeWeights(sigma):
            psf = gauss_psf.ExpectedValue([[W/2,W/2,1,0]])[0]
            
            weights = np.zeros((D*2,W*2,W*2),dtype=np.float32)
            weights[::2,::2,::2] = psf[None,:,:]
            return weights
        
        #plt.figure()
        #plt.imshow(weights[0,:,:,0])
    
        I=200
        bg=10
        N=1000
        pos = np.zeros((N,3))
        pos[:,1] = roisize/2
        pos[:,0] = np.linspace(0,roisize-1,N)
        pos[:,:2] += np.random.uniform(-1,1,size=(N,2))
        pos[:,2] = 0
    
        if False:
            weights = makeWeights(1)
            output = dsm.Eval(pos, roisize, weights)
            fig,ax=plt.subplots(2)
            ax[0].imshow(output[0])
            ax[0].set_title('Output')
            ax[1].imshow(weights[0,::2,::2])
            ax[1].set_title('Weights[...,0]')

        with g.CreatePSF_XYIBg(roisize, sigma, False) as psf:
            gpos = np.zeros((N,4))
            gpos[:,:2]=pos[:,:2]
            gpos[:,2] = I
            gpos[:,3] = bg
            smp=psf.GenerateSample(gpos)
       
        pixelBg = np.ones(smp.shape)*bg
        intensities = np.ones(len(pos))*I
        
        weights = makeWeights(2)*0
        wg,psf_,roi_ll=dsm.ComputeWeightsGradient(pos, intensities, weights, smp, pixelBg,ctx)
        
        print(f'Current LL: {np.sum(roi_ll):.1f}')

        pos[:,:2] += np.random.uniform(-0.2,0.2,size=(N,2)) # add some 'localization' noise
        weights = optimizeWeights(pos, intensities, weights, smp, pixelBg,ctx)
        estimated = dsm.Eval(pos, roisize, weights)
            
        #show_napari(wg[::2,::2,::2])

        plt.figure()
        fig,ax=plt.subplots(ncols=2)
        ax[0].imshow(estimated[len(smp)//2])#weights[0,::2,::2])
        ax[0].set_title('Estimated PSF')
        ax[1].imshow(smp[len(smp)//2])
        ax[1].set_title(f'1 Sample from the {len(smp)} samples')

        if False:
            psf = dsm.CreatePSF(weights, roisize)
            params = np.zeros((N,5))
            params[:,:2] = roisize/2
            params[:,2] = np.linspace(psf.calib.zrange[0],psf.calib.zrange[1],N)
            params[:,3] = 1
            ev=psf.ExpectedValue(params)
            show_napari(ev)

        img = dsm.RenderSection(1, 2, 0, weights, stepsize=0.05)        
        plt.figure()
        plt.imshow(img)
        plt.figure()
        plt.plot(img[len(img)//2])
        
        show_napari( np.concatenate([estimated*I+bg,smp],-1))         
        return weights

def testROI():
    with Context(debugMode=True) as ctx:
        dsm = DSplineMethods(ctx)
    
        roisize = 12
        D = 5
        W = 15
    
        X,Y = np.meshgrid(np.arange(W)-W//2,np.arange(W)-W//2)
        
        #weights = np.random.uniform(size=(D,W,W,8))
        weights = np.zeros((D,W,W,8),dtype=np.float32)
        #weights[:,2,2,0] = 1
        weights[:,:,:,0] = np.exp(-(X**2+Y**2)/(10))[None,:,:]
        
        #plt.figure()
        #plt.imshow(weights[0,:,:,0])
    
        N=40
        pos = np.zeros((N,3))
        pos[:,1] = roisize/2
        pos[:,0] = np.linspace(0,roisize-1,N)
    
        output = dsm.Eval(pos, roisize, weights)

        fig,ax=plt.subplots(2)
        ax[0].imshow(output[0])
        ax[0].set_title('Output')
        ax[1].imshow(weights[0,:,:,0])
        ax[1].set_title('Weights[...,0]')
    
        show_napari(output)

#if __name__ == "__main__":

if False:
    test1D()
    
if False:    

    test2D()
    

if False:
    r=testGradient()
