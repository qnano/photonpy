# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 21:18:42 2020

@author: jcnossen1
"""

import numpy as np
import matplotlib.pyplot as plt
from photonpy.smlmlib.context import Context
from photonpy.smlmlib.psf_queue import PSF_Queue
import photonpy.smlmlib.spotdetect as spotdetect
import math
import os
import tifffile
from photonpy.smlmlib.context import Context
from photonpy.smlmlib.cspline import CSpline_Calibration, CSpline
import napari

from scipy.ndimage import gaussian_filter 
from scipy.signal import fftconvolve

#fn= 'c:/data/sols/zstack-dm0-sel.tif'

fn= 'c:/data/sols/zstack-dm5-200-sel.tif'
#fn= 'c:/data/sols/zstack-dm11-100-sel.tif'
#fn= 'c:/data/sols/agarose-beads-zstack-dm5-200.tif'


def roi_idx(shape, pos, roisize):
    pos = np.array(pos)
    s = np.array(shape)
    tl = np.clip(pos-roisize//2, 0, s-1)
    br = np.clip(pos+roisize//2, 0, s-1)

    return np.s_[tl[0]:br[0],tl[1]:br[1]]    


def extract_stacks(mov, roisize, threshold=0.1):
    sump = np.sum(mov, 0)
    sump = (sump-np.mean(sump)) / np.std(sump)
    
    # Use the brightess spot as a template to find others
    smooth_sump = gaussian_filter(sump, roisize/2)
    sump = sump-smooth_sump
    mp = np.unravel_index(np.argmax(smooth_sump),smooth_sump.shape)

    spotidx = np.s_[mp[0]-roisize//2:mp[0]+roisize//2, mp[1]-roisize//2:mp[1]+roisize//2]
    spotimg = sump[spotidx]

    # Use 2D cross correlation to find the other spots
    xcor = fftconvolve(sump, spotimg, mode='same')
    xcor = xcor/np.max(xcor)

    print(xcor.shape)
    
    plt.figure()
    plt.imshow(sump)
    
    sump[spotidx] = 0
    
    
    spotimg = []
    while True:
        mp = np.unravel_index(np.argmax(xcor),xcor.shape)
        print(xcor[mp],mp)

        if xcor[mp] > threshold:
            
            spotidx = roi_idx(xcor.shape, mp, roisize)
            xcor[spotidx] = 0
            
            if xcor[spotidx].size < roisize**2:
                continue
            
            spotimg.append(mov[:,mp[0]-roisize//2:mp[0]+roisize//2, mp[1]-roisize//2:mp[1]+roisize//2])
        else:
            break
        
    plt.figure()
    plt.imshow(xcor)
        
    print(f"{len(spotimg)} spots found")
    return spotimg

            

def show_napari(img):
        
    with napari.gui_qt():
        napari.view_image(img)


if __name__ == "__main__":
    mov = tifffile.imread(fn)
    
    roisize = 160
    #show_napari(sump)    

    
    #plt.imshow(spotimg)
    r = extract_stacks(mov, roisize)
    
    show_napari(np.mean(r, 0))
    
    #show_napari(np.array(r))
 
#    show_napari(xcor)

    
    