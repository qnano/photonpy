# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 20:08:50 2020

@author: jelmer
"""

import numpy as np
import matplotlib.pyplot as plt

def splineWeights1D(t):
    t2=t*t
    w = np.zeros((*t.shape,4))
    w[...,0] = 2*t2*t-3*t2+1
    w[...,1] = t2*t-2*t2+t
    w[...,2] = -2*t2*t+3*t2
    w[...,3] = t2*t-t2

    return w

def computeSpline(y,dydt, t):
    idx = np.array(t).astype(np.int32)
    w = splineWeights1D(t-idx)
    return w[...,0] * y[idx] + w[...,1] * dydt[idx] + w[...,2] * y[idx+1] + w[...,3] * dydx[idx+1]

def catmulRomWeights(t):
    hermiteBasis = np.array([[2, -2, 1, 1],
         [-3, 3,-2,-1],
         [0,  0, 1, 0],
         [1,  0, 0, 0]])
    
    catmullRom = np.array([[0,1,0,0],
                  [0,0,1,0],
                  [-0.5,0,0.5,0],
                  [0,-0.5,0,0.5]])

    t_ = np.zeros((*t.shape,4))
    t_[:,0] = t**3
    t_[:,1] = t**2
    t_[:,2] = t
    t_[:,3] = 1
    
    return t_ @ (hermiteBasis @ catmullRom)    
    

def catmullRom(pt, t):

    hermiteBasis = np.array([[2, -2, 1, 1],
         [-3, 3,-2,-1],
         [0,  0, 1, 0],
         [1,  0, 0, 0]])
    
    catmullRom = np.array([[0,1,0,0],
                  [0,0,1,0],
                  [-0.5,0,0.5,0],
                  [0,-0.5,0,0.5]])

    A = ( hermiteBasis @ catmullRom )[::-1]

    idx = np.array(t).astype(np.int32)
    #idx = np.clip(idx, 0, len(pt)-1)
    t = np.clip(t-idx, 0, 1)
    #w = catmulRomWeights(t)

    idx_ = np.clip(((idx-1)[:,None] + np.arange(4)[None,:]), 0,  len(pt)-1)
    w = (t[:,None]**(np.arange(4)[None])) @ A

    return (pt[idx_] * w[:,:,None]).sum(1)    
    
L = 8
pt = np.random.uniform(0,1,size=(L+1))
t = np.linspace(0,L,200,endpoint=False)

#y = computeSpline(pt, t)

# Computing tangents from finite difference like this results in a Catmull-Rom spline.
dydx = np.array([0, *( (pt[2:]-pt[:-2])/2 ), 0])

y = computeSpline(pt, dydx, t)

plt.figure()
plt.plot(pt, 'o', label='Control points')
plt.plot(t, y, label='Spline')

y2 = catmullRom(pt[:,None], t)
plt.plot(t, y2, label='CR Spline')
plt.legend()
