import matplotlib.pyplot as plt
import numpy as np
import photonpy.smlm.drift_estimate as drift_estimate

from photonpy import Context,Dataset
from photonpy.cpp.lib import SMLM

import tqdm
import simulate_drift
from scipy.interpolate import InterpolatedUnivariateSpline

import os
from photonpy.cpp.postprocess import PostProcessMethods

from photonpy.smlm import picasso_hdf5


def phasor_localize(roi):
    fx = np.sum(np.sum(roi,0)*np.exp(-2j*np.pi*np.arange(roi.shape[1])/roi.shape[1]))
    fy = np.sum(np.sum(roi,1)*np.exp(-2j*np.pi*np.arange(roi.shape[0])/roi.shape[0]))
            
    #Get the size of the matrix
    WindowPixelSize = roi.shape[1]
    #Calculate the angle of the X-phasor from the first Fourier coefficient in X
    angX = np.angle(fx)
    if angX>0: angX=angX-2*np.pi
    #Normalize the angle by 2pi and the amount of pixels of the ROI
    PositionX = np.abs(angX)/(2*np.pi/WindowPixelSize)
    #Calculate the angle of the Y-phasor from the first Fourier coefficient in Y
    angY = np.angle(fy)
    #Correct the angle
    if angY>0: angY=angY-2*np.pi
    #Normalize the angle by 2pi and the amount of pixels of the ROI
    PositionY = np.abs(angY)/(2*np.pi/WindowPixelSize)
    
    return PositionX,PositionY

def drift_estim(measured_drift):
    """
    Given 3 frames:
        
    r01 = D1 - D0
    r02 = D2 - D0
    r12 = D2 - D1
    
    A = [ 
        -1 1 0
        -1 0 1
        0 -1 1
    ]
    
    """
    nbins = measured_drift.shape[0]
    nrows = nbins*(nbins-1)//2    
    indices = np.triu_indices(nbins,1)

    A = np.zeros((nrows,nbins))
    A[np.arange(nrows),indices[0]] = 1
    A[np.arange(nrows),indices[1]] = -1
#    print(A)
    
    r = measured_drift[indices]
    
    return np.linalg.pinv(A) @ r




def crosscorrelation(A, B):
    A_fft = np.fft.fft2(A)
    B_fft = np.fft.fft2(B)
    return np.fft.ifft2(A_fft * np.conj(B_fft))

def crosscorrelation_cuda(A, B, smlm: SMLM):
    return smlm.IFFT2(smlm.FFT2(A) * np.conj(smlm.FFT2(B)))



def findshift(cc, smlm:SMLM, plot=False):
    # look for the peak in a small subsections
    r = 6
    hw = 20
    cc_middle = cc[cc.shape[0] // 2 - hw : cc.shape[0] // 2 + hw, cc.shape[1] // 2 - hw : cc.shape[1] // 2 + hw]
    peak = np.array(np.unravel_index(np.argmax(cc_middle), cc_middle.shape))
    peak += [cc.shape[0] // 2 - hw, cc.shape[1] // 2 - hw]
    
    peak = np.clip(peak, r, np.array(cc.shape) - r)
    roi = cc[peak[0] - r + 1 : peak[0] + r, peak[1] - r + 1 : peak[1] + r]
    if plot:
        plt.figure()
        plt.imshow(cc_middle)
        plt.figure()
        plt.imshow(roi)


    px,py = phasor_localize(roi)
#    roi_top = fit_sigma_2d(roi, initial_sigma=2)[[0, 1]]
    #            roi_top = lsqfit.lsqfitmax(roi)
    return peak[1] + px - r + 1 - cc.shape[1] / 2, peak[0] + py - r + 1 - cc.shape[0] / 2

def test_fft():
    np.random.seed(0)
    x = np.random.uniform(size=(32,32))
    f_x = np.fft.fft2(x)
    with SMLM() as smlm:
        f_x_cuda = smlm.FFT2(x)
    print(f_x)
    print(f_x_cuda)


def findshift_pairs(images, pairs, smlm:SMLM):
    print("FFT'ing")
    w = images.shape[-1]
    fft_images = smlm.FFT2(images)
    
    fft_conv = np.zeros((len(pairs), w, w),dtype=np.complex64)
    for i, (a,b) in enumerate(pairs):
        fft_conv[i] = np.conj(fft_images[a]) * fft_images[b]
        
    print("IFFT'ing")
    #print(f"Data size: {fft_conv.size}")
    cc = [smlm.IFFT2(v) for v in fft_conv]
    cc = np.abs(np.fft.fftshift(cc, (-2, -1)))

    print("Finding cc peaks..")
    shift = np.zeros((len(pairs),2))
    for i in tqdm.trange(len(pairs)):
        shift[i] = findshift(cc[i], smlm, i==0)
        if i==0:
            plt.title(f"CC for {pairs[i]}")
    
    return shift

def interp_shift_xy(shift,framesperbin,nframes):

    t = (np.arange(len(shift))+0.5)*framesperbin,
    spl_x = InterpolatedUnivariateSpline(t, shift[:,0], k=2)
    spl_y = InterpolatedUnivariateSpline(t, shift[:,1], k=2)
    
    shift_interp = np.zeros((nframes,2))
    shift_interp[:,0] = spl_x(np.arange(nframes))
    shift_interp[:,1] = spl_y(np.arange(nframes))
    
    return shift_interp


def show_napari(img):
    import napari
        
    with napari.gui_qt():
        napari.view_image(img)

def save_hdf5(fn , xyI, framenum, w):
    xyIBg = np.zeros((len(xyI),4))
    xyIBg[:,:3] = xyI
    
    crlb= np.zeros((len(xyI),4))
    crlb[:,:2] = 0.1
    crlb[:,2] = 70
    crlb[:,3] = 0.1
    
    picasso_hdf5.save(fn, xyIBg, crlb, framenum, [w,w], 1.8, 1.8)


def drift_correct_hdf5(fn,framesperbin, pixelsize, smoothing):
    estim,framenum,crlb,imgshape,sx,sy = picasso_hdf5.load(fn)
    numframes = np.max(framenum)+1
    
    print(f"Loaded {len(estim)} spots. {len(estim)/numframes:.1f} spots/frame\n")

    timebins = 10
    drift_rcc,drift_bins_rcc,images=drift_estimate.rcc(estim[:,:3], framenum, 
                         timebins, rendersize=imgshape[0], wrapfov=1, zoom=1, sigma=1)

    useCuda=True
    
    with Context(debugMode=False) as ctx:
        print( "drift correcting...")
        xy = estim[:,:2]
        sigma = np.mean(crlb[:,:2])
        
        drift = drift_rcc*1# np.zeros((numframes,2))
                
        numIterations = 10000
        step = 0.000001

        set1 = xy[:,0]>np.median(xy[:,0])
        #set1 = np.arange(len(xy)) % 2 == 0
        set2 = np.logical_not(set1)

        ndrift,score = PostProcessMethods(ctx).GaussianOverlapDriftEstimate(
            xy, framenum//framesperbin, drift[::framesperbin], sigma, numIterations, step, 5, cuda=useCuda)
                
        ndrift_set1,score_set1 = PostProcessMethods(ctx).GaussianOverlapDriftEstimate(
            xy[set1], framenum[set1]//framesperbin, ndrift*1, sigma, numIterations, step, 5, cuda=useCuda)

        ndrift_set2,score_set2 = PostProcessMethods(ctx).GaussianOverlapDriftEstimate(
            xy[set2], framenum[set2]//framesperbin, ndrift*1, sigma, numIterations, step, 5, cuda=useCuda)

        plt.figure()
        plt.plot(score,label='Score (full set)')
        plt.plot(score_set1,label='Score (subset 1)')
        plt.plot(score_set2,label='Score (subset 2)')
        plt.title('Scores')
        plt.legend()

        ndrift_set1 -= ndrift_set1[0]
        ndrift_set2 -= ndrift_set2[0]
        ndrift -= ndrift[0]
        drift = ndrift

        # both of these use half the data (so assuming precision scales with sqrt(N)), 2x variance
        # subtracting two 2x variance signals gives a 4x variance estimation of precision
        diff = ndrift_set1-ndrift_set2 
        est_precision = np.std(diff,0)/2
        print(f"Estimated precision: {est_precision}")

        drift_smoothed = drift*1
        fig,axes=plt.subplots(2,1,sharex=True)
        for ax in range(drift.shape[1]):
            drift_smoothed[:,ax] = np.convolve(drift[:,ax], np.ones(smoothing)/smoothing, mode='same')
            axes[ax].plot(drift[:,ax], label='Raw drift estimate')
            axes[ax].plot(drift_smoothed[:,ax], label='Smoothed estimate')
        plt.legend()
            
        hf = drift-drift_smoothed
        vibration_std = np.sqrt(np.var(hf,0) - est_precision**2)
        print(f'Vibration estimate: X={vibration_std[0]*pixelsize:.1f} nm, Y={vibration_std[1]*pixelsize:.1f} nm')

        drift = interp_shift_xy(drift, framesperbin, numframes)
        drift_set1 = interp_shift_xy(ndrift_set1, framesperbin, numframes)
        drift_set2 = interp_shift_xy(ndrift_set2, framesperbin, numframes)

        estim[:,:2] -= drift[framenum]
        out_fn = os.path.splitext(fn)[0]+"-dc.hdf5"
        picasso_hdf5.save(out_fn, estim, crlb, framenum, imgshape, sx,sy)
        
        fig,ax=plt.subplots(2,1,sharex=True)
        ax[0].plot(drift_set1[:,0], lw=0.5,label='Drift X (MLC) - set1')
        ax[0].plot(drift_set2[:,0], lw=0.5,label='Drift X (MLC) - set2')
        ax[1].plot(drift_set1[:,1], lw=0.5,label='Drift Y (MLC) - set1')
        ax[1].plot(drift_set2[:,1], lw=0.5, label='Drift Y (MLC) - set2')
        ax[0].plot(drift_rcc[:,0], label='Drift X (RCC)')
        ax[1].plot(drift_rcc[:,1], label='Drift Y (RCC)')
        ax[0].legend()
        ax[1].legend()
        plt.title(f'Est. Precision: X/Y={est_precision[0]*pixelsize:.1f}/{est_precision[1]*pixelsize:.1f} nm ({est_precision[1]:.3f}/{est_precision[1]:.3f} pixels), ')
        
        return drift

def smooth_drift(d):
    import allantools
    t = np.logspace(0, 3, 50)  # tau values from 1 to 1000
    y = allantools.noise.white(10000)  # Generate some frequency data
    r = 12.3  # sample rate in Hz of the input data
    (t2, ad, ade, adn) = allantools.oadev(y, rate=r, data_type="freq", taus=t)  # Compute the overlapping ADEV
    plt.figure()
    fig = plt.loglog(t2, ad) # Plot the results
    
def test_optimal_sigma(xy, framenum, sigma, drift, maxDrift, true_drift):

    sig = np.logspace(np.log(sigma*0.005)/np.log(10), np.log(sigma*3)/np.log(10))    
    
    err = np.zeros(len(sig))
    
    for i,s in enumerate(sig):
        ndrift, scores = PostProcessMethods(ctx).GaussianOverlapDriftEstimate(
            xy, framenum, drift*1, s, iterations=20000, stepsize=1e-8, maxdrift=maxDrift, cuda=cuda)
        err[i] = np.std(true_drift-ndrift)
    
    plt.figure()
    plt.semilogx(sig, err)
    plt.xlabel(f'Sigma (true sigma={sigma})')
    plt.ylabel('Error')
    plt.title('Optimal sigma')
    
    
if __name__ == '__main__':
    
    if False:
        framesperbin=5
        #fn='C:/data/simflux/storm/results/1/'
        
        fn='C:/data/simflux/sim4_1/results/sim4_1/'
        fn = 'C:/data/sfsf/6-25-2020/results/8int_100ms_2_X1/' 
        fn = 'C:/data/sfsf/6-29-2020/results/8rad_4int_50ms_5_X1/'
        #fn='C:/data/sim4_1/results/sim4_1/g2d.hdf5'
        d_g2d = drift_correct_hdf5(os.path.split(fn)[0]+"/g2d_fits.hdf5",framesperbin,pixelsize=65,smoothing=100)
        d_sf = drift_correct_hdf5(os.path.split(fn)[0]+"/sfsf_fits.hdf5",framesperbin,pixelsize=65,smoothing=100)
        est_prec = np.std(d_sf-d_g2d)/np.sqrt(2)
        
        fig,ax=plt.subplots(2,1,sharex=True)
        ax[0].plot(d_g2d[:,0]); ax[0].plot(d_sf[:,0])
        ax[1].plot(d_g2d[:,1]); ax[1].plot(d_sf[:,1])
        ax[0].set_title('X drift'); ax[1].set_title('Y drift')
        plt.suptitle('Simflux vs Gaussian data drift correction')
        
        print(f"Estimated drift precision: {est_prec* 65:.2f}")

    else:
        
        timebins = 20
        numframes = 3000
    
        w = 128
        numspots = 400
        spots = np.random.uniform(-w/6,w/6,size=(numspots,2))
        spots[:,0] += w/2+w/6*np.cos(np.linspace(0,2*np.pi,numspots))
        spots[:,1] += w/2+w/6*np.sin(np.linspace(0,2*np.pi,numspots))
    
        loc_sigma=0.1
        xyI, crlb, framenum, oncounts, true_drift = simulate_drift.gen(
                numframes, spots, [w,w], 
                drift_mean=[0,0.001], 
                drift_sigma=[0.01,0.01], 
                k_on=0.01,
                loc_precision=loc_sigma)
        
        save_hdf5("test.hdf5", xyI,framenum,w)
            
        shift_rcc,shift_estim,images=drift_estimate.rcc(xyI, framenum, timebins, rendersize=w, wrapfov=1, zoom=1, sigma=1)
        plt.figure()
        plt.imshow(images[0])
        plt.title('image 0')
        
        nframes = np.max(framenum)+1
        
        drift=None
        
        cuda = True
        
        if True:
            with Context(debugMode=False) as ctx:
                framesperbin = 10
                                
                print( f"drift correcting... {len(xyI)/nframes:.1f} spots/frame")
                xy = xyI[:,:2]
                drift = shift_rcc*1#np.zeros((numframes//framesperbin,2))
                sigma = loc_sigma

                #test_optimal_sigma(xy, framenum, sigma, drift, 5, true_drift)
        
                numIterations = 80
                step = 0.0005
                
                set1 = np.arange(len(xy)) % 2 == 0
                set2 = np.logical_not(set1)
                
                def progcb(iteration, txt):
                    print(f"{txt.decode('utf-8')}",flush=True)
                    return True

                maxdrift=5
                #initial step with large sigma
                coarse_drift,scores = PostProcessMethods(ctx).MinEntropyDriftEstimate(
                    xy, framenum, drift*1, sigma, numIterations, step, maxdrift, framesPerBin=400, cuda=cuda, progcb=progcb)
                coarse_drift -= coarse_drift[0]

                ndrift,scores = PostProcessMethods(ctx).MinEntropyDriftEstimate(
                    xy[set1], framenum[set1], coarse_drift*1, sigma, numIterations, step, 5, framesPerBin=10, cuda=cuda, progcb=progcb)
        
                ndrift2,scores2 = PostProcessMethods(ctx).MinEntropyDriftEstimate(
                    xy[set2], framenum[set2], coarse_drift*1, sigma, numIterations, step, 5, framesPerBin=10,cuda=cuda, progcb=progcb)


                #final accurate step
                #drift_old, scores = PostProcessMethods(ctx).MinEntropyDriftEstimate(    
                #    xy, framenum, coarse_drift*1, sigma, numIterations, step, maxdrift, framesPerBin=1, cuda=cuda, progcb=progcb,flags=8)

                fbp = 4
                #final accurate step
                drift, scores = PostProcessMethods(ctx).MinEntropyDriftEstimate(
                    xy, framenum, coarse_drift*1, sigma, numIterations, step, maxdrift, framesPerBin=fbp, cuda=cuda, progcb=progcb)

                #final accurate step
                #crlb = np.ones(xy.shape)*loc_sigma
                
                drift_vc, scores = PostProcessMethods(ctx).MinEntropyDriftEstimate(
                    xy, framenum, coarse_drift*1, crlb, numIterations, step, maxdrift, framesPerBin=fbp, cuda=cuda, progcb=progcb)
        
                #drift_old -= drift_old[0]
                drift_vc  -= drift_vc[0]
                drift -= drift[0]
                drift2 = ndrift2 - ndrift2[0]
                
                #scores.append(score)
                
                plt.figure()
                plt.plot(scores, label='Score for set1')
                plt.plot(scores2, label='Score for set2')

        def plotdrift(dispframes):
            fig,ax=plt.subplots(2,1)
            ax[0].plot(true_drift[:dispframes,0], label='True x')
            ax[0].plot(coarse_drift[:dispframes,0], '--', label='SAME X (Coarse)')
            ax[0].plot(drift[:dispframes,0] + 0.1, label='SAME X + .1')
            ax[0].plot(drift_vc[:dispframes,0], label='SAME Variable CRLB X')
            ax[0].plot(shift_rcc[:dispframes,0], label='RCC x')
            ax[0].legend()
            #plt.scatter(shift_estim[:,2],shift_estim[:,0], marker='X',label='X')
            ax[1].plot(true_drift[:dispframes,1], label='True y')
            ax[1].plot(coarse_drift[:dispframes,1], '--', label='SAME Y (Coarse)')
            ax[1].plot(drift[:dispframes,1] + 0.1, label='SAME Y + .1')
            ax[1].plot(drift_vc[:dispframes,1], label='SAME Variable CRLB Y')
            ax[1].plot(shift_rcc[:dispframes,1], label='RCC y')
            #plt.scatter(shift_estim[:,2],shift_estim[:,1], marker='X',label='Y')
            #plt.plot(shift[:,1], label='y')
            ax[1].legend()
        
        plotdrift(dispframes=2000)
        
        plt.figure()
        errs = true_drift-drift
        errs -= np.mean(errs,0)
        plt.hist(errs[:,0], bins=50, range=[-0.1,0.1])
        plt.title('SAME Drift Errors')
        
        #smooth_drift(drift)

        #mgo_err = drift_old-true_drift
        #print(f"MGO bias: {np.mean(mgo_err)} precision: {np.std(mgo_err)}")
    
        rcc_err = shift_rcc-true_drift
        rcc_mae = np.mean(np.abs(rcc_err))
        print(f"RCC bias: {np.mean(rcc_err):.5f} precision: {np.std(rcc_err):.5f}")
        
        same_err = drift-true_drift
        same_mae = np.mean(np.abs(same_err))
        IF = rcc_mae / same_mae
        print(f"SAME bias: {np.mean(same_err):.5f} precision: {np.std(same_err):.5f}. Improvement factor: {IF:.2f}")

        same_vc_err = drift_vc-true_drift
        same_vc_mae = np.mean(np.abs(same_vc_err))
        IF_VC = rcc_mae / same_vc_mae
        print(f"SAME VC bias: {np.mean(same_vc_err):.5f} precision: {np.std(same_vc_err):.5f}. Improvement factor: {IF_VC:.2f}")
        
        