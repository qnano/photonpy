# -*- coding: utf-8 -*-

import numpy as np

def gen(
    numframes = 3000,
    spots = 1000,
    w = 400,
    drift_mean = [0, 0.0001], drift_sigma = [0.02,0.02],
    k_on = 0.1, k_off = 0.1, # high on rate for testing
    loc_precision = 0.1): # [pixels]
    
    if np.isscalar(spots):
        numspots=spots
        spots = np.random.uniform(0, w, size=(numspots,2))
    else:
        numspots = len(spots)
    
    p_on = k_on / (k_on+k_off)
    blinkstate = np.random.binomial(1, p_on, size=numspots)
        
    xyI = []
    crlb = []
    framenum = []
    true_drift = np.zeros((numframes,2))
    total_drift = np.zeros(2)
    oncounts = np.zeros(numframes,dtype=int)
    for f in range(numframes):
        true_drift[f] = total_drift

        turning_on = (1-blinkstate) * np.random.binomial(1, k_on, size=numspots)
        remain_on = blinkstate * np.random.binomial(1, 1-k_off, size=numspots)
        blinkstate = remain_on + turning_on
        
        c = np.sum(blinkstate)
        oncounts[f] = c
        framenum.append( f * np.ones(c,dtype=np.int) )
    
        # simulate that the localization error is due to photon counts and loc_precision corresponds to 1000 photons
        #photoncounts = np.random.normal( np.sqrt(np.random.poisson(10, size=c))
        #crlb_ = np.repeat(  (loc_precision * (np.sqrt(10)/ photoncounts ))[:,None],2,1)
        crlb_ = np.maximum(loc_precision*0.1, np.random.normal(loc_precision, loc_precision, size=(c,2)))
        
        loc_error = np.random.normal(0,crlb_,size=(c,2))
        xyI_frame = np.zeros((c,3))
        xyI_frame[:,[0,1]] = spots[blinkstate==1,:] + total_drift + loc_error
        xyI_frame[:,2] = 1 # ignored for now

        crlb.append(crlb_)        
        xyI.append(xyI_frame)
        drift = np.random.normal(drift_mean, drift_sigma,size=2)
        total_drift += drift

    crlb = np.concatenate(crlb)
    xyI = np.concatenate(xyI)
    framenum = np.concatenate(framenum)
    
    return xyI, crlb, framenum, oncounts, true_drift