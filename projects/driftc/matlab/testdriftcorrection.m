clear
close all
addpath binlocalizations
load drift_correction_cmp.mat

W=w;
fprintf('#spots=%d',size(xyf,1));
zoom = 4;
img = binlocalizations(xyf(:,1:2), W*zoom,W*zoom, zoom);
imshow(img*255);

[pos, estim_drift_fft2] = driftcorrection(xyf, W, zoom, timebins);
estim_drift_fft2 = estim_drift_fft2(2:end,:);

mse_fft = mean((estim_drift_fft-true_drift).^2);
mse_fft2 = mean((estim_drift_fft2-true_drift).^2);
mse_nn = mean((estim_drift_onetoall-true_drift).^2);

fprintf('MSE CC (python): %f\n', mse_fft); 
fprintf('MSE CC (matlab): %f\n', mse_fft2); 
fprintf('MSE NN: %f\n', mse_nn); 

figure; hold on;
plot(true_drift(:,1));
plot(estim_drift_fft(:,1));
plot(estim_drift_fft2(:,1));
%plot(estim_drift_seq(:,1));
plot(estim_drift_onetoall(:,1));
hold off
xlabel('Timebin'); ylabel('Drift [pixels]');
legend('true drift x', 'estim drift x (CC, python)', 'estim drift x (CC, matlab)', 'estim drift x (NN, 1-all)');
%legend('true drift x', 'estim drift x (FFT)', 'estim drift x (NN, seq)', 'estim drift x (NN, one2all)');

figure; hold on;
plot(true_drift(:,2));
plot(estim_drift_fft(:,2));
plot(estim_drift_fft2(:,2));
plot(estim_drift_onetoall(:,2));
xlabel('Timebin'); ylabel('Drift [pixels]');
hold off
legend('true drift y', 'estim drift y (CC, python)', 'estim drift y (CC, matlab)', 'estim drift y (NN, 1-all)');
