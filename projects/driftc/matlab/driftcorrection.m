%DRIFTCORRECTION   Estimate and correct for stage drift
%
% Correct localizations for stage drift during the acquisition. The drift
% is estimated by splitting the acquisition in time blocks and estimating
% the displacement between the images of each time block with the image of
% the first time block.
%
% SYNOPSIS:
%   [positions_corrected d] = driftcorrection(positions,size,zoomfactor,blocks,maxdrift)
%
%   size
%      Size of the binned images
%   zoom
%      Pixels per unit distance in the positions list in the binned images
%   blocks
%      Number of time blocks into which the dataset is split. 2 block is
%      the minimum accepted amount.
%   maxdrift
%       Maximum allowed during a time block
%
% DEFAULTS:
%   size : image edge corresponds to maximum x- and y-coordinates
%   zoomfactor  = 1
%   blocks = 20
%
% (C) Copyright 2012               Quantitative Imaging Group
%     All rights reserved          Faculty of Applied Physics
%                                  Delft University of Technology
%                                  Lorentzweg 1
%                                  2628 CJ Delft
%                                  The Netherlands
% Robert Nieuwenhuizen, Dec 2012

function varargout = driftcorrection(varargin)

d = struct('menu','FRC resolution',...
           'display','Correct for drift',...
           'inparams',struct('name',       {'positions',        'sz',           'zm',           'nblocks',                  'maxshift'},...
                             'description',{'Localizations',    'Image size',   'Zoom',         'Number of time blocks',    'Maximum drift per timeblock'},...
                             'type',       {'array',            'array',        'array',        'array',                    'array'},...
                             'dim_check',  {{[],[-1 2],[-1 3]}, {[],0},         {[],0},         {[],0},                     {[],0}},...
                             'range_check',{'R',                'N+',           [eps Inf],      'N+',                       'R+'},...
                             'required',   {0,                  0,              0,              0,                          0},...
                             'default',    {'[]',               '[]',           1,              20,                         5}...
                              ),...
           'outparams',struct('name',{'positions_corr',                     'd'},...
                              'description',{'Corrected localizations',     'Displacements estimated'},...
                              'type',{'array',                              'array'}...
                              )...
           );       
       
if nargin == 1
   s = varargin{1};
   if ischar(s) && strcmp(s,'DIP_GetParamList')
      varargout{1} = struct('menu','none');
      return
   end
end

try
   [positions, sz, zm, nblocks, maxshift] = getparams(d,varargin{:});
catch
   if ~isempty(paramerror)
      error(paramerror)
   else
      error('Parameter parsing was unsuccessful.')
   end
end


if isempty(zm)
   zm = 1;
end

% Check that the size of the binned images is provided
if isempty(sz)
   % Use a square bounding box as output size
   sz = ceil(zm*max(max(positions(:,1:2))));
end

% Check that a correct number of time blocks is specified
if isempty(nblocks)
    nblocks = 20;
end

if nblocks<2
    nblocks = 2;
    warning('driftcorrection:toofewblocks','Time series is split into 2 blocks.') 
end

if size(positions,2) == 2
   positions = [positions (1:length(positions))'];
end

% Check maxshift
if isempty(maxshift)
    maxshift =  5;                      % Maximum shift per step (SR pixels)
end    

%% Hardcoded parameters
r = 5;                             % Smoothing kernel size (SR pixels)
findshift_method = 'ncc';          % Method of shift estimation

%% Estimate displacements
maxt = max(positions(:,3));         % Frames in timeseries

tvec = linspace(0,maxt,nblocks+1);  % Starting times of the time intervals
dt = maxt/nblocks;                  % Lengths of the time intervals
d = zeros(nblocks,2);

% Define reference image
s = (positions(:,3)>=tvec(1))&(positions(:,3)<tvec(2));             % Selection vector; nonzero for tvec(1)<t<tvec(2)
im1 = binlocalizations(positions(s,1:2),sz,sz,zm);                  % Reference image


if strcmp(findshift_method,'ncc')
    % Prepare cross-correlation computations
    x0 = floor(size(im1)/2);                                           % Center of cross-correlation image
    xlims = x0(1)+round(maxshift*[-1 1]);
    ylims = x0(2)+round(maxshift*[-1 1]);
end

% Find displacements wrt reference image
for ii = 2:nblocks
    s = (positions(:,3)>=tvec(ii))&(positions(:,3)<tvec(ii+1));     % Selection vector
    im2 = binlocalizations(positions(s,1:2),sz,sz,zm);              % Bin localizations with tvec(ii)<t<tvec(ii+1)
    
    if strcmp(findshift_method,'ncc')
        % Compute full cross-correlation
        R = crosscorrelation(im1,im2);
        %fprintf('cc %d (%dx%d)\n', ii, size(im1,1), sz);
        
        % Find subpixel maximum location
        [~,xc] = max(R(xlims(1):xlims(2),ylims(1):ylims(2)));       % First find maximum with pixel precision assuming drift_x,y < maxshift
        xc = xc+[xlims(1) ylims(1)];
        xc = subpixlocation(R,xc,'parabolic','maximum');
        d(ii,:) = xc - x0;
        clear x_c R
    else
        d(ii,:) = findshift(im2,im1,findshift_method,r,maxshift);
    end
end

d = d / zm;

%% Estimate drift of each localization w.r.t t=0
drift_t = zeros(size(positions(:,1:2)));

% Drift velocities at times tvec(2:end-1)
V = (d(2:end,:)-d(1:end-1,:))/dt;           

% Drift for with t<dt/2
s = positions(:,3)<dt/2;
drift_t(s,:) = positions(s,3)*V(1,:);

% Drift for dt/2<t<t_end-dt/2
t_current = dt/2;               % Starting point of drift segment 1
d_current = dt/2* V(1,:);       % Current drift w.r.t. t = 0
d_max = max(d)+d_current;       % Maximum drift point

 for jj = 2:nblocks
    s = (positions(:,3)>=t_current)&(positions(:,3)<t_current+dt);
    drift_t(s,:) = repmat(d_current,[sum(s) 1])+(positions(s,3)-t_current)*V(jj-1,:);
    t_current = t_current+dt;
    d_current = d_current + dt*V(jj-1,:);
end

% Drift for t>=t_end-dt/2
s = positions(:,3)>=t_current;
drift_t(s,:) = repmat(d_current,[sum(s) 1])+(positions(s,3)-t_current)*V(end,:);

%% Correct localizations
positions_corr = positions;
drift_t = drift_t - repmat(mean(drift_t),[size(drift_t,1) 1]);
positions_corr(:,1:2) = positions_corr(:,1:2) - drift_t;

%% Outputs
varargout{1} = positions_corr;
varargout{2} = d;