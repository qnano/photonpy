
/*Compile the mex file:
 *
 *  mex -O cHistRecon.c 
 *
 *
 */


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mex.h"
#include "matrix.h"



/* Thread block size*/
#define CBLK 4000
#define pi 3.141592

#define max(a,b) ( (a) >= (b) ? (a) : (b) )  
#define min(a,b) ( (a) < (b) ? (a) : (b) )  


void mexFunction(int nlhs, mxArray *plhs[],	int	nrhs, const	mxArray	*prhs[])
{
    float *X,*Y;
    uint16_T *out;
    int i;
    int N,sizeX,sizeY;
    const mwSize *datasize;  
    int Xtmp,Ytmp,useHE;
    mwSize outsize[2];
    int *Hist;
    float *CDF;
    int sum;
    int cdf_min;
    int Npixels;
    float *out2;
    const int NBIN = 1023;
    
    if (nrhs!=5)
        mexErrMsgTxt("Must input sizeX,sizeY,X,Y,UseHistEqual!\n");
    for (i=2;i<4;i++)
    if (mxGetClassID(prhs[i])!=mxSINGLE_CLASS)
        mexErrMsgTxt("Array inputs must be comprised of single floats!\n");

    sizeX=(int)mxGetScalar(prhs[0]); 
    sizeY=(int)mxGetScalar(prhs[1]);
    useHE=(int)mxGetScalar(prhs[4]);
    
    X=(float *) mxGetData(prhs[2]);
    Y=(float *) mxGetData(prhs[3]);
   
    datasize=mxGetDimensions(prhs[2]);
    
    outsize[0]=sizeX;
    outsize[1]=sizeY;
    plhs[0]=mxCreateNumericArray(2,outsize,mxUINT16_CLASS,mxREAL);
    plhs[1]=mxCreateNumericArray(2,outsize,mxSINGLE_CLASS,mxREAL);
    
    out=(uint16_T *) mxGetData(plhs[0]);
    out2= (float *) mxGetData(plhs[1]);
    
    sum=0;
    for (i=0;i<datasize[0];i++){
        Xtmp=max(0,min(sizeX-1,(int)floor(X[i])));
        Ytmp=max(0,min(sizeY-1,(int)floor(Y[i])));
        out[Ytmp*sizeX+Xtmp]+=1;
    }    
    
    if (useHE) /*Histogram equalization*/
    {
        Hist = (int*) mxCalloc(NBIN,sizeof(int));
        CDF = (float*) mxCalloc(NBIN,sizeof(float));
        for (i=0; i<NBIN; i++) Hist[i]=0;
        
        Npixels=sizeX*sizeY;
        for (i=0;i<Npixels;i++) if(out[i]) Hist[out[i]]+=1; /*calc histogram*/
   
        i=1; 
        CDF[0] = 0;
        sum=0;
        while (sum<datasize[0])
        {
            CDF[i]=CDF[i-1]+(float)Hist[i];
            sum+=Hist[i]*i;
            i++;
        }
        N = (int) CDF[i-1];
        /*printf("%f %d %d\n",CDF[i-1],i-1,N);
        printf("%f %f %f %f %f %f\n",CDF[0],CDF[1],CDF[2],CDF[3],CDF[4],CDF[5]);
        convert pixels*/
        for (i=0;i<Npixels;i++) if(out[i]) {
            out2[i]= NBIN*(CDF[out[i]]-1)/(N-1);
            out[i]=  (uint16_T) NBIN*(CDF[out[i]]-1)/(N-1);
        }
        mxFree(Hist);
        mxFree(CDF);
    }
}














