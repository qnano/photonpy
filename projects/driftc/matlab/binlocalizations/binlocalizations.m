% BINLOCALIZATIONS Converts position data into a 2d histogram image
%     BINIMAGE = binlocalizations(POSITIONS,XSIZE,YSIZE,ZOOM) returns an image that is made by binning
%     the positions in the Mx2 matrix POSITIONS in pixel bins. The
%     resulting image has dimensions XSIZE x YSIZE. The top left corner of
%     the image corresponds to the coordinate (0,0). The bottom right
%     corner corresponds to the coordinate (XSIZE/ZOOM,YSIZE/ZOOM). In other
%     words, the pixel size of  the binned image is 1/ZOOM.

function binimage = binlocalizations(varargin)

% Check number of variables and apply defaults
if (nargin == 0)||(nargin>4)
    error('Wrong number of input arguments.')
end

positions = single(varargin{1});

if nargin >= 2
    xsize = varargin{2};
else
    xsize = 256;
end

if nargin >= 3
    ysize = varargin{3};
else
    ysize = xsize;
end
    
if nargin == 4
    zm = varargin{4};
else
    zm = 1;
end

% Test if emitter positions have been defined as input
if isempty(positions)
    binimage = zeros(xsize,ysize);
    return;
end

% Test if positions are in 2D
if (size(size(positions),2) ~= 2)
    binimage = zeros(xsize,ysize);
    return;
end

positions = positions + 0.5;

% Filter localizations outside the FOV
keep = positions(:,1)>=0;
keep = keep & positions(:,2)>=0;
keep = keep & positions(:,1)*zm<=xsize;
keep = keep & positions(:,2)*zm<=ysize;
positions = positions(keep,:);

binimage = cHistRecon(xsize,ysize,positions(:,1)*zm,positions(:,2)*zm,0)';