import sys
sys.path.append('../python')
import matplotlib.pyplot as plt
import numpy as np

from smlmlib.base import SMLM
import smlmlib.postprocess as postprocess
import scipy.io as sio

import drift_estimate_fft
import simulate_drift

w=100
timebins = 400
numframes = 1600
numspots=100
xyI, framenum, oncounts, true_drift = simulate_drift.gen(numframes,numspots,w=w)
framesperbin = numframes // timebins

with SMLM() as smlm:
    # One to all: (0-1, 0-2, 0-3, 0-4,...)
    framepairs = np.vstack( (np.zeros(timebins-1), np.arange(timebins-1)+1) ).T
    estim_drift1, mresults1 = postprocess.PostProcess(smlm).NearestNeighborDriftEstimate(
            xyI, framenum // framesperbin, framepairs, (w,w),searchDist=2, icpIterations=8)
    # Sequentially (0-1, 1-2, 2-3,...)
    framepairs = np.vstack( (np.arange(timebins-1), np.arange(timebins-1)+1) ).T
    estim_drift2, mresults2 = postprocess.PostProcess(smlm).NearestNeighborDriftEstimate(
            xyI, framenum // framesperbin, framepairs, (w,w),searchDist=2, icpIterations=8)
    estim_drift2 = estim_drift2.cumsum(0)
    
    estim_drift3 = drift_estimate_fft.estimate(xyI,framenum//framesperbin,w,smlm)

# driftcorrection.m format:
xyf = xyI
xyf[:,2] = framenum

true_drift = np.array( np.array_split(true_drift, timebins)[:-1] ).sum(1).cumsum(0)
drift_err = true_drift - estim_drift1

sio.savemat('matlab/drift_correction_cmp.mat', {'xyf': xyf, 'true_drift': true_drift, 
                'estim_drift_onetoall': estim_drift1, 'mresults_onetoall': mresults1,
                'estim_drift_seq': estim_drift2, 'mresults_seq': mresults2,
                'estim_drift_fft': estim_drift3, 'timebins': timebins, 'w': w, 'numspots': numspots, 'numframes': numframes} )
    
plt.figure()
plt.hist(oncounts)

plt.figure()
plt.plot( true_drift[:,0], label='True X Drift'  )
plt.plot( estim_drift1[:,0], label='Estim. X Drift (To first frame)'  )
plt.plot( estim_drift2[:,0], label='Estim. X Drift (Sequentially)'  )
plt.plot( estim_drift3[:,0], label='Estim. X Drift (CC)'  )

plt.plot( true_drift[:,1], label='True Y Drift'  )
plt.plot( estim_drift1[:,1], label='Estim. Y Drift (To first frame)'  )
plt.plot( estim_drift2[:,1], label='Estim. Y Drift (Sequentially)'  )
plt.plot( estim_drift3[:,1], label='Estim. Y Drift (CC)'  )
plt.legend()

mse1 = np.mean( ( true_drift-estim_drift1 )**2 )
mse2 = np.mean( ( true_drift-estim_drift2 )**2 )
mse3 = np.mean( ( true_drift-estim_drift3 )**2 )
print(f'MSE NN-AllToZero {mse1}')
print(f'MSE NN-Seq {mse2}')
print(f'MSE NN-CC {mse3}')

plt.figure()
plt.plot(mresults1, label='Point matches per timebin' )
plt.legend()


