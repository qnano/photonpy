function meritoffset = meritoffsetcalc(allspots,varfit)
% This function calculates the merit function offset for an accurate
% determination of the log-likelihood. The script can work for a single
% ROI data: size(allspots)=[Mx,My,Ncfg] or for multiple ROI data:
% size(allspots)=[Mx,My,Mz,Ncfg].
%
% copyright Sjoerd Stallinga, TU Delft, 2017
%

ndims = length(size(allspots));
Ncfg = size(allspots,ndims);
meritoffset = zeros(Ncfg,1);

if (ndims==3)
  for jcfg = 1:Ncfg
    dummat = allspots(:,:,jcfg);
    [Mx,My] = size(dummat);

    % set negative and zero pixels values to one/10 to avoid log-singularity
    dummat = max(dummat,ones(size(dummat))/10); 

    meritoffset(jcfg) = 0;
    for ii=1:Mx
      for jj = 1:My
          meritoffset(jcfg) = meritoffset(jcfg)-gammln(dummat(ii,jj)+1+varfit);
      end
    end
  end
end

if (ndims==4)
  for jcfg = 1:Ncfg
    dummat = allspots(:,:,:,jcfg);
    [Mx,My,Mz] = size(dummat);

    % set negative and zero pixels values to one/10 to avoid log-singularity
    dummat = max(dummat,ones(size(dummat))/10); 

    meritoffset(jcfg) = 0;
    for ii=1:Mx
      for jj = 1:My
        for kk = 1:Mz
          meritoffset(jcfg) = meritoffset(jcfg)-gammln(dummat(ii,jj,kk)+1+varfit);
        end
      end
    end
  end
end

end