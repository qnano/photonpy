%load('C:/research/chisquareeval/data/Exp003_correctedaberrationsand60mlambdaadded_Greenbead_180nm_processeddata_nobitconversion');

bgn=10;
n=30;
zs=21;
tn=301;

chiarraybg=zeros(bgn,n,zs,tn);

imgind=6;
if (imgind<10)
    %FileTif="C:/research/chisquareeval/data/505_515bead60mlambda/0"+num2str(imgind)+".tif";
    %FileTif="C:/research/chisquareeval/data/testimg4.tif";
    FileTif="C:/code/ExamplePhaseRetrieval/data/simdata/testimg.tif";
else
    FileTif="C:/research/chisquareeval/data/505_515bead60mlambda/"+num2str(imgind)+".tif";
end
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);

im=zeros(nImage,mImage,NumberImages,'uint16');
for i=1:NumberImages
   im(:,:,i)=imread(FileTif,'Index',i);
end
im=double(im);
im=im./1e6;
%im=poissrnd(im+3);


expdata=false;
matfile=load('C:/code/ExamplePhaseRetrieval/data/simdata/vector_fitter_amp.mat');
PSF=matfile.PSF;
[lx,ly,lz]=size(PSF);
thetauncor=matfile.thetauncor;
%I=thetauncor(end-1);
%bg=thetauncor(end);
bgarr=linspace(5,50,10);
imold=im;
rate=0.9;
I=1e4;
for bi=1:bgn
    bgtmp=bgarr(bi);
    for i_n=1:n
        Itmp=I*rate^(i_n);
        mu=PSF.*Itmp+bgtmp;
        for j=1:tn
            if (expdata)
                imnew=zeros(lx,ly,lz);
                for ii=1:lx
                    for jj=1:ly
                        for kk=1:lz
                            a=mnrnd(round(imold(ii,jj,kk)),[rate,1-rate]);
                            imnew(ii,jj,kk)=a(1)+poissrnd(round((1-rate)*bgtmp));
                        end
                    end
                end
             else
                imnew=poissrnd(im.*Itmp+bgtmp);
            end
            chiarraybg(bi,n-i_n+1,:,j)=sum((mu-imnew).^2./mu,[1,2]);
        end
        imold=imnew;
    end
end
exp=31*31*21;
Iall=zeros(n,1);
for i=1:n
    Iall(i)=I*rate^(i);
end
Iall=flip(Iall);
save('C:/code/ExamplePhaseRetrieval/data/simdata/chiarraybg10fine_amp','chiarraybg')
%chiarraybg=flip(chiarraybg,2);
s=sum(chiarraybg(1,:,:,:),3);
chimean=(squeeze(mean(s,4)));
chistd=(squeeze(std(s,1,4)));
errorbar(Iall,chimean,chistd);
hold on
errorbar(Iall,squeeze(ones(1,n))*exp,squeeze(ones(1,n))*sqrt(2*exp));
set(gca, 'XScale', 'log')

