%% ExamplePhaseRetrieval
% This codes shows an example of a TFS of a bead (175nm, green) with the
% deformable mirror (MIRAO52E) upon start up. the phase retrieval is used
% to estimate the Zernike coefficients. Subsequently, the negative values
% of these coefficients are then modulated by the DM and a second TFS is
% acquired. Phase retrieval on the corrected TFS shows that the Zernike
% modes are corrected.
% At each z-position 3 frames are acquired, resulting in 3 TFS per time.

% Marijn Siemons, 28-02-2020

% Fits correspond well within z-positions of +-500nm. The corrected TFS
% shows some minor deviations at large z-stage positions, probably due to 
% Zernike modes which are present in the wavefront, but not fitted by the 
% model. Also, the z-stage position might by slightly different in the
% experimental TFS which can lead to model errors.

clear all
close all

%% load

load('ExamplePhaseRetrieval.mat')

%% Perform fit of uncorrected TFS

parameters.Mx = size(TFSuncorexp,1);
parameters.My = size(TFSuncorexp,2);
parameters.Mz = size(TFSuncorexp,3);
parameters.Ncfg = size(TFSuncorexp,4);

parameters.xrange = parameters.pixelsize*parameters.Mx/2;
parameters.yrange = parameters.pixelsize*parameters.My/2;

[~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(parameters);
[XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);

% MLE fit
fprintf('Fitting uncorrected...\n')
thetainit = initialvalues(TFSuncorexp,XImage,YImage,parameters);
[thetastore,TFSuncorfit,~,~] = localization(TFSuncorexp,thetainit,parameters);
thetauncor = squeeze(thetastore(:,:,end));
Wrmsuncor = sqrt(sum(thetauncor(4:end-2,:).^2,1))./parameters.lambda*1000;

% matrix for plotting with dipshow
TFSuncorshow = zeros(2*(2*ROIsize+1),Ncfg*(2*ROIsize+1),parameters.Mz);
for jcfg = 1:parameters.Ncfg
    TFSuncorshow(1:2*(2*ROIsize+1),1+(jcfg-1)*(2*ROIsize+1):(jcfg)*(2*ROIsize+1),:) = [TFSuncorfit(:,:,:,jcfg); TFSuncorexp(:,:,:,jcfg)];
end
fprintf('Ready\n')

%%
%  After the initial TFS and phase retrieval, the (negative) esimated Zernike coefficients are 
%  send to the DM and a second TFS is acquired. The Zernike modes which are
%  corrected are: Z22, Z2-2, Z31, Z3-1, Z33, Z3-3, Z40, Z42, Z4-2, Z44,
%  Z4-4, Z51, Z5-1, Z53, Z5-3 and Z60. (NOT Z55 and Z5-5 and Z62 and higher).
%
%% Perform fit of corrected TFS

parameters.Mx = size(TFScorexp,1);
parameters.My = size(TFScorexp,2);
parameters.Mz = size(TFScorexp,3);
parameters.Ncfg = size(TFScorexp,4);

parameters.xrange = parameters.pixelsize*parameters.Mx/2;
parameters.yrange = parameters.pixelsize*parameters.My/2;

[XPupil,YPupil,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(parameters);
[XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);

% MLE fit
fprintf('Fitting TFS after correction...\n')
thetainit = initialvalues(TFScorexp,XImage,YImage,parameters);
[thetastore,TFScorfit,~,~] = localization(TFScorexp,thetainit,parameters);
thetacor = squeeze(thetastore(:,:,end));
Wrmscor = sqrt(sum(thetacor(4:end-2,:).^2,1))./parameters.lambda*1000;

% matrix for plotting with dipshow
TFScorshow = zeros(2*(2*ROIsize+1),Ncfg*(2*ROIsize+1),parameters.Mz);
for jcfg = 1:parameters.Ncfg 
    TFScorshow(1:2*(2*ROIsize+1),1+(jcfg-1)*(2*ROIsize+1):(jcfg)*(2*ROIsize+1),:) = [TFScorfit(:,:,:,jcfg); TFScorexp(:,:,:,jcfg)];
end
fprintf('Ready\n')


%% Plot

Ncfg = 3;
h1 = figure; hold on
errorbar(1:size(parameters.aberrations,1),mean(thetauncor(4:end-2,:),2)./parameters.lambda*1000,std(thetauncor(4:end-2,:),[],2)./parameters.lambda*1000./sqrt(Ncfg))
errorbar(1:size(parameters.aberrations,1),mean(thetacor(4:end-2,1:Ncfg),2)./parameters.lambda*1000,std(thetacor(4:end-2,1:Ncfg),[],2)./parameters.lambda*1000./sqrt(parameters.Ncfg))
set(gca,'xtick',1:size(parameters.aberrations,1));
Zfit_index = num2str(parameters.aberrations(:,1:2));
set(gca,'xticklabel',Zfit_index);
set(gca,'XTickLabelRotation',60)
xlabel('Zernike modes')
ylabel('RMS Aberration coefficient [m\lambda]')
grid on
xlim([0 size(parameters.aberrations,1)+1])
legend(['Uncorrected, Wrms = ',num2str(mean(Wrmsuncor)),' m\lambda' ],['Corrected, Wrms = ',num2str(mean(Wrmscor)),' m\lambda'])
title('Zernike coefficients before and after correction')

h2 = figure('position',[200 200 1600 800]);
zpos = linspace(parameters.zrange(1),parameters.zrange(2),parameters.Mz);
for jz = 1:parameters.Mz
subplot(4,parameters.Mz,jz)
imagesc(TFSuncorexp(:,:,jz,1))
axis square
set(gca,'xtick',[]); set(gca,'ytick',[])
title(num2str(zpos(jz)))
if jz == 1; ylabel('Exp uncor'); end

subplot(4,parameters.Mz,jz+parameters.Mz)
imagesc(TFSuncorfit(:,:,jz,1))
axis  square
set(gca,'xtick',[]); set(gca,'ytick',[])
if jz ==1; ylabel('Fit uncor'); end

subplot(4,parameters.Mz,jz+2*parameters.Mz)
imagesc(TFScorexp(:,:,jz,1))
axis  square
set(gca,'xtick',[]); set(gca,'ytick',[])
if jz ==1; ylabel('Exp cor'); end

subplot(4,parameters.Mz,jz+3*parameters.Mz)
imagesc(TFScorfit(:,:,jz,1))
axis  square
set(gca,'xtick',[]); set(gca,'ytick',[])
if jz ==1; ylabel('Fit cor'); end

end

suptitle('z-position (nm)')

hdip1 = dipshow([TFSuncorshow;TFScorshow],[min(TFScorshow(:)),max(TFScorshow(:))]);

