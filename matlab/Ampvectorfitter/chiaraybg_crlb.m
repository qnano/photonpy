
parameters.pixelsize=80;
parameters.samplingdistance = parameters.pixelsize;
parameters.beaddiameter = 180;
parameters.lambda = 690;
parameters.zrange = [-1000, 1000];
parameters.doetype='none';
parameters.dipoletype='free';


parameters.fitmodel='aberrations';
mat=load('C:/code/ExamplePhaseRetrieval/data/simdata/aberration_para.mat');
aberrations=mat.aberrationsp;
parameters.aberrations=aberrations;
if strcmp(parameters.fitmodel,'aberrationsamp') 
    parameters.aberrations=aberrations;
    numparams_phaseaberr=length(aberrations)-3;
    numparams_ampaberr=length(aberrations);
    parameters.numparams=5+numparams_ampaberr+numparams_phaseaberr;
    parameters.numparams_phaseaberr=numparams_phaseaberr;
    parameters.numparams_ampaberr=numparams_ampaberr;
elseif strcmp(parameters.fitmodel,'aberrations') 
    aberrations=aberrations(4:end,1:3);
    parameters.aberrations=aberrations;
    parameters.numparams=5+length(aberrations);
end

bgarr=linspace(5,50,10);
mat=load('C:/code/ExamplePhaseRetrieval/data/simdata/Icstrore_amp.mat');
Icstore=mat.Icstore;
if true
    n_pixels =31;
    hn=(n_pixels-1)/2;
    n_z_slices = 21;
    %aberrations = [1,-1,0,0; 1,1,0.0,0; 2,0,0,0.0; 2,-2,0,0.0; 2,2,50.0,0.0; 3,-1,0.0,0.0; 3,1,0.0,0; 4,0,0.0,0.0; 3,-3,-0.0,0; 3,3,0.0,0; 4,-2,0.0,0; 4,2,0.0,0; 5,-1,0.0,0; 5,1,0.0,0; 6,0,0.0,0; 4,-4,0.0,0; 4,4,0.0,0;  5,-3,0.0,0; 5,3,0.0,0;  6,-2,0.0,0; 6,2,0.0,0; 7,1,0.0,0; 7,-1,0.0,0; 8,0,0.0,0];
    
    parameters.NA = 1.49;

    parameters.xemit = 0;
    parameters.yemit = 0;
    parameters.zemit = 0;

    parameters.pixelsize=80;
    parameters.samplingdistance = parameters.pixelsize;
    parameters.beaddiameter = 180;
    parameters.lambda = 690;
    parameters.zrange = [-1000, 1000];
    parameters.doetype='none';
    parameters.dipoletype='free';
    parameters.Mx = n_pixels ;
    parameters.My = n_pixels ;
    parameters.Mz = n_z_slices;
    crlb=zeros(10,21,301);
    
    [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
                  get_pupil_matrix(parameters);
    
    for bi=1:10
        disp(bi);
        for i=1:21
            for j=1:101
                parameters.signalphotoncount=Icstore(bi,i,j);
                parameters.backgroundphotoncount=bgarr(bi);
                
                [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
                  get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);

                [PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,parameters);

                [FisherMatrix,CRLB] = get_fisher_crlb(PSF,PSFderivatives,parameters);
                crlb(bi,i,j)=CRLB(1);
            end
        end
    end

   
end