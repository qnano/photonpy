load('C:/research/chisquareeval/data/Exp003_correctedaberrationsand60mlambdaadded_Greenbead_180nm_processeddata_nobitconversion');
n_pixels =31;
hn=(n_pixels-1)/2;
n_z_slices = 11;

mat=load('C:/code/ExamplePhaseRetrieval/data/simdata/aberration_para.mat');
aberrations=mat.aberrations;
parameters.aberrations=aberrations;

FileTif="C:/code/ExamplePhaseRetrieval/data/simdata/testimg.tif";

InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
NumberImages=length(InfoImage);
im=zeros(mImage,mImage,NumberImages);

for i=1:NumberImages
   im(:,:,i)=imread(FileTif,'Index',i);
end
im=double(im);
im=im./1e6;
im=im.*5e3;
im=poissrnd(im+3);
TFSuncorexp=im;
parameters.Mx = size(TFSuncorexp,1);
parameters.My = size(TFSuncorexp,2);
parameters.Mz = size(TFSuncorexp,3);
parameters.Ncfg = size(TFSuncorexp,4);

parameters.xrange = parameters.pixelsize*parameters.Mx/2;
parameters.yrange = parameters.pixelsize*parameters.My/2;

parameters.fitmodel='xyz';
[~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(parameters);

[XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);

% MLE fit
fprintf('Fitting uncorrected...\n')
thetainit = initialvalues(TFSuncorexp,XImage,YImage,parameters);
[thetastore,TFSuncorfit_phase,~,~] = localization(TFSuncorexp,thetainit,parameters);
thetauncor = squeeze(thetastore(:,:,end));

