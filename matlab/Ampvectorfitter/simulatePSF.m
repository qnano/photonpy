load('C:/research/chisquareeval/data/Exp003_correctedaberrationsand60mlambdaadded_Greenbead_180nm.mat');

n_pixels =31;
hn=(n_pixels-1)/2;
n_z_slices = 11;
aberrations = [1,-1,0,0; 1,1,0.0,0; 2,0,0,0.0; 2,-2,0,0.0; 2,2,50.0,0.0; 3,-1,0.0,0.0; 3,1,0.0,0; 4,0,0.0,0.0; 3,-3,-0.0,0; 3,3,0.0,0; 4,-2,0.0,0; 4,2,0.0,0; 5,-1,0.0,0; 5,1,0.0,0; 6,0,0.0,0; 4,-4,0.0,0; 4,4,0.0,0;  5,-3,0.0,0; 5,3,0.0,0;  6,-2,0.0,0; 6,2,0.0,0; 7,1,0.0,0; 7,-1,0.0,0; 8,0,0.0,0];
parameters.aberrations=aberrations;
%parameters.samplingdistance = parameters.pixelsize;

parameters.NA = 1.49;

parameters.xemit = 0;
parameters.yemit = 0;

parameters.pixelsize=80;
parameters.samplingdistance = parameters.pixelsize;
parameters.fitmodel='aberrationsamp';
%aberrations = [1,-1,0,0; 1,1,0.0,0; 2,0,0,0.05; 2,-2,0,0.0; 2,2,50.0,0.01; 3,-1,0.0,0.0; 3,1,0.0,0; 4,0,0.0,0.1; 3,-3,-0.0,0; 3,3,0.0,0; 4,-2,0.0,0; 4,2,0.0,0; 5,-1,0.0,0; 5,1,0.0,0; 6,0,0.0,0; 4,-4,0.0,0; 4,4,0.0,0;  5,-3,0.0,0; 5,3,0.0,0;  6,-2,0.0,0; 6,2,0.0,0; 7,1,0.0,0; 7,-1,0.0,0; 8,0,0.0,0];
parameters.beaddiameter = 180;
parameters.lambda = 690;
parameters.zrange = [-1000, 1000];
parameters.doetype='none';
parameters.dipoletype='free';
parameters.Mx = n_pixels ;
parameters.My = n_pixels ;
parameters.Mz = n_z_slices;
if strcmp(parameters.fitmodel,'aberrationsamp') 
    parameters.aberrations=aberrations;
    numparams_phaseaberr=length(aberrations)-3;
    numparams_ampaberr=length(aberrations);
    parameters.numparams=5+numparams_ampaberr+numparams_phaseaberr;
    parameters.numparams_phaseaberr=numparams_phaseaberr;
    parameters.numparams_ampaberr=numparams_ampaberr;
elseif strcmp(parameters.fitmodel,'aberrations') 
    aberrations=aberrations(4:end,1:3);
    parameters.aberrations=aberrations;
    parameters.numparams=5+length(aberrations);
end

[~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
  get_pupil_matrix(parameters);
[~,~,FieldMatrix,FieldMatrixDerivatives] = ...
  get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);

[PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,parameters);

if false
    
    imgrow=(PSF.*1e6);
    %img=zeros(n_pixels*2,n_pixels*2,n_z_slices);
    %img(1:n_pixels,1:n_pixels,:)=imgrow;
    %img(1:n_pixels,n_pixels+1:2*n_pixels,:)=imgrow;
    %img(n_pixels+1:2*n_pixels,1:n_pixels,:)=imgrow;
    %img(n_pixels+1:2*n_pixels,n_pixels+1:2*n_pixels,:)=imgrow;
    img=uint16(imgrow);
    fn="C:/code/ExamplePhaseRetrieval/data/simdata/testimg.tif";

    for i=1:n_z_slices
        imwrite(img(:,:,i),fn,'WriteMode','append')
    end

end


if true
    imgall=zeros(n_pixels,n_pixels,n_z_slices,9);
    for i=1:9
        xshift=normrnd(0,0.15);
        yshift=normrnd(0,0.15);
        parameters.xemit = parameters.pixelsize / 2 - xshift * parameters.pixelsize;
        parameters.yemit = parameters.pixelsize / 2 - yshift * parameters.pixelsize;
        [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
          get_pupil_matrix(parameters);
        [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
          get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);
        [PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,parameters);
        img=(PSF.*5e3)+3;
        imgall(:,:,:,i)=img;
    end
    imgall=poissrnd(imgall);
    imgall=uint16(imgall);
    
    xshift=0.0;
    yshift=0.0;
    parameters.xemit = parameters.pixelsize / 2 - xshift * parameters.pixelsize;
    parameters.yemit = parameters.pixelsize / 2 - yshift * parameters.pixelsize;
    [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
      get_pupil_matrix(parameters);
    [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
      get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);
    [PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,parameters);
    imgl=(PSF.*5e3)+3;
    
    
    fn="C:/code/ExamplePhaseRetrieval/data/astig50/testimg";
    save(fn,'imgall','parameters','imgl');
    
end

if false
    
    nrow=3;
    img=zeros(nrow*nimgsub,nrow*nimgsub,n_z_slices);
    for in=1:3
        for jn=1:3
            
            xshift=normrnd(0,0.15);
            yshift=normrnd(0,0.15);
            parameters.xemit = parameters.pixelsize / 2 - xshift * parameters.pixelsize;
            parameters.yemit = parameters.pixelsize / 2 - yshift * parameters.pixelsize;
            [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
              get_pupil_matrix(parameters);
            [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
              get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,parameters);
            [PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,parameters);
            imgrow=(PSF.*5e3)+3;
            nimgsub=n_pixels+20;
            imgsub=zeros(nimgsub,nimgsub,51);
            imgsub(26-hn:26+hn,26-hn:26+hn,:)=imgrow;
            
            img((in-1)*nimgsub+1:(in)*nimgsub,(jn-1)*nimgsub+1:(jn)*nimgsub,:)=imgsub;
        end
    end
    img=poissrnd(img);
    %img=zeros(n_pixels*2,n_pixels*2,n_z_slices);
    %img(1:n_pixels,1:n_pixels,:)=imgrow;
    %img(1:n_pixels,n_pixels+1:2*n_pixels,:)=imgrow;
    %img(n_pixels+1:2*n_pixels,1:n_pixels,:)=imgrow;
    %img(n_pixels+1:2*n_pixels,n_pixels+1:2*n_pixels,:)=imgrow;
    img=uint16(img);
    fn="C:/code/ExamplePhaseRetrieval/data/astig50/testimg1.tif";

    for i=1:n_z_slices
        imwrite(img(:,:,i),fn,'WriteMode','append')
    end

end