classdef vector_fitter_class
    
    properties
        maxn;
        parameters;
        
    end
    
    
    methods
        
        function self = vector_fitter_class()
             mat=load('defult_parameters.mat');
             
             p=mat.parameters;
             p.NA=1.49;
             p.pixelsize=80;
             p.beaddiameter = 180;
             p.lambda = 690;
             p.zrange = [-1000, 1000];
             p.doetype='none';
             p.dipoletype='free';
             self.parameters=p;
             self.maxn=5;
        end
        
        
        function self=set_NA(self,NA)
            self.parameters.NA=NA;
        end
        
        function self=set_pixelsize(self,pixelsize)
            self.parameters.pixelsize=pixelsize;
            self.parameters.samplingdistance=pixelsize;
        end
        
        function self=set_lambda(self,lambda)
            self.parameters.lambda=lambda;
        end
        
        function self=set_zrange(self,zrange)
            self.parameters.zrange=zrange;
        end
        
        function self=set_beaddiameter(self,beaddiameter)
            self.parameters.beaddiameter=beaddiameter;
        end
        
        function self=set_refmed(self,refmed)
            self.parameters.refmed=refmed;
        end
        
        function self=set_refcov(self,refcov)
            self.parameters.refcov=refcov;
        end
        
        function self=set_refimm(self,refimm)
            self.parameters.refimm=refimm;
        end
        
        function self=set_refimmnom(self,refimmnom)
            self.parameters.refimmnom=refimmnom;
        end
        
        function NA=get_NA(self)
            NA=self.parameters.NA;
        end
        
        function pixelsize=get_pixelsize(self)
            pixelsize=self.parameters.pixelsize;
        end
        
        function lambda=get_lambda(self)
            lambda=self.parameters.lambda;
        end
        
        function zrange=get_zrange(self)
            zrange=self.parameters.zrange;
        end
        
        function beaddiameter=get_beaddiameter(self)
            beaddiameter=self.parameters.beaddiameter;
        end
        
        function aberrations=zero_aberrations(self)
            count=1;
            ma = self.maxn;
            for i=1:ma
                mind=-i:2:i;
                for j=1:length(mind)
                    aberrations(count,:)=[i,mind(j),0,0];
                    count=count+1;
                end
            end
            aberrations(3,2)=0;
            aberrations(4,2)=-2;
            
        end
        
        function aberrations_amp=calibrate_PSF_ampaberration(self,im)
            im=double(im);
            aberrations=zero_aberrations(self);
            aberrationsall=aberrations;
            aberrations_amp=aberrations;
            %aberrations_phase=aberrations;
            
            
            self.parameters.fitmodel='aberrations';

            self.parameters.Nitermax=120;
            if strcmp(self.parameters.fitmodel,'aberrationsamp') 
                self.parameters.aberrations=aberrations;
                numparams_phaseaberr=length(aberrations)-3;
                numparams_ampaberr=length(aberrations);
                self.parameters.numparams=5+numparams_ampaberr+numparams_phaseaberr;
                self.parameters.numparams_phaseaberr=numparams_phaseaberr;
                self.parameters.numparams_ampaberr=numparams_ampaberr;
            elseif strcmp(self.parameters.fitmodel,'aberrations') 
                aberrations=aberrations(4:end,1:3);
                self.parameters.aberrations=aberrations;
                self.parameters.numparams=5+length(aberrations);
            end


            %% Perform fit of uncorrected TFS

            self.parameters.Mx = size(im,1);
            self.parameters.My = size(im,2);
            self.parameters.Mz = size(im,3);
            self.parameters.Ncfg = 1;

            self.parameters.xrange = self.parameters.pixelsize*self.parameters.Mx/2;
            self.parameters.yrange = self.parameters.pixelsize*self.parameters.My/2;

            for iii=1:size(im,4)
                %TFSuncorexp=poissrnd(im.*2e3+3);
                TFSuncorexp=squeeze(im(:,:,:,iii));
                [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(self.parameters);

                [XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,self.parameters);

                % MLE fit
                fprintf('Fitting uncorrected...\n')
                thetainit = initialvalues(TFSuncorexp,XImage,YImage,self.parameters);
                [thetastore,~,~,~] = localization(TFSuncorexp,thetainit,self.parameters);
                thetauncor = squeeze(thetastore(:,:,end));

                for i=4:length(thetauncor)-2
                    aberrationsall(i,3)=aberrationsall(i,3)+thetauncor(i);
                end
            end
            
            aberrationsall(:,3)=aberrationsall(:,3)/size(im,4);
            aberrations=aberrationsall;
            self.parameters.fitmodel='aberrationsamp';
            if strcmp(self.parameters.fitmodel,'aberrationsamp') 
                self.parameters.aberrations=aberrations;
                numparams_phaseaberr=length(aberrations)-3;
                numparams_ampaberr=length(aberrations);
                self.parameters.numparams=5+numparams_ampaberr+numparams_phaseaberr;
                self.parameters.numparams_phaseaberr=numparams_phaseaberr;
                self.parameters.numparams_ampaberr=numparams_ampaberr;
            elseif strcmp(self.parameters.fitmodel,'aberrations') 
                aberrations=aberrations(4:end,1:3);
                self.parameters.aberrations=aberrations;
                self.parameters.numparams=5+length(aberrations);
            end

            [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(self.parameters);

            [XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,self.parameters);

            % MLE fit
            for iii=1:size(im,4)
                TFSuncorexp=squeeze(im(:,:,:,iii));
                thetainit = initialvalues(TFSuncorexp,XImage,YImage,self.parameters);
                [thetastore,~,~,~] = localization(TFSuncorexp,thetainit,self.parameters);
                thetauncor = squeeze(thetastore(:,:,end));

                if strcmp(self.parameters.fitmodel,'aberrationsamp') 
                    for i=4:length(aberrations)
                        aberrations(i,3)=thetauncor(i);
                        aberrations_amp(i,3)=aberrations_amp(i,3)+thetauncor(i);
                    end
                    for i=1:length(aberrations)
                        aberrations(i,4)=thetauncor(length(aberrations)+i);
                        aberrations_amp(i,4)=aberrations_amp(i,4)+thetauncor(length(aberrations)+i);
                    end
                elseif strcmp(self.parameters.fitmodel,'aberrations')    
                    for i=1:length(aberrations)
                        aberrations(i,3)=thetauncor(i+3);
                        aberrations_amp(i,3)=aberrations_amp(i,3)+thetauncor(i+3);
                    end
                end
            end
            aberrations_amp(:,3:4)=aberrations_amp(:,3:4)./size(im,4);
            self.parameters.aberrations=aberrations_amp;
            
        end
        
        function self=loadcalibrationdata(self,filepath)
            mat=load(filepath);
            self.parameters.aberrations=mat.aberrations;
            
        end
        
        function self=loadaberrations(self,aberrations)
            self.parameters.aberrations=aberrations;
        end
        
        function theta=VF_localization(self,im)
            im=double(im);
            self.parameters.Mx = size(im,1);
            self.parameters.My = size(im,2);
            self.parameters.Mz = size(im,3);
            self.parameters.Ncfg = size(im,4);

            self.parameters.xrange = self.parameters.pixelsize*self.parameters.Mx/2;
            self.parameters.yrange = self.parameters.pixelsize*self.parameters.My/2;
            self.parameters.numparams=5;

            self.parameters.fitmodel='xyz';
            [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = get_pupil_matrix(self.parameters);

            [XImage,YImage,~,~] = get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,self.parameters);
            thetainit = initxyz(im,XImage,YImage,self.parameters);
            [thetastore,~,~,~] = localization(im,thetainit,self.parameters);
            theta = squeeze(thetastore(:,:,end));      
        end
        
        function CRLB=get_crlb(self,x,y,z,I,bg)
            
            self.parameters.xemit = x;
            self.parameters.yemit = y;
            self.parameters.zemit = z;
            self.parameters.signalphotoncount=I;
            self.parameters.backgroundphotoncount=bg;
            
            [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
                  get_pupil_matrix(self.parameters);
            [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
              get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,self.parameters);

            [PSF,PSFderivatives] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,self.parameters);

            [~,CRLB] = get_fisher_crlb(PSF,PSFderivatives,self.parameters);
            
            
        end
        
        function saveparameter(self,filepath)
            save(filepath,self.parameters);
        end
        
        function PSF=get_Expectedvalue(self)
            
            [~,~,wavevector,wavevectorzimm,Waberration,allzernikes,PupilMatrix] = ...
              get_pupil_matrix(self.parameters);
            [~,~,FieldMatrix,FieldMatrixDerivatives] = ...
              get_field_matrix_derivatives(PupilMatrix,wavevector,wavevectorzimm,Waberration,allzernikes,self.parameters);

            [PSF,~] = get_psfs_derivatives(FieldMatrix,FieldMatrixDerivatives,self.parameters);
            
        end
        
    end
    
    
end