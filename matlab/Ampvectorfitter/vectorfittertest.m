matfile=load('chiarraybg_phase.mat');
chiarraybg_phase=matfile.chiarraybg;
matfile=load('C:/code/ExamplePhaseRetrieval/data/mexpdata/mdata_phaseresult.mat');

thetauncor=matfile.thetauncor_phase;
Ip=thetauncor(end-1);

matfile=load('chiarraybg_amp.mat');
chiarraybg_amp=matfile.chiarraybg;
matfile=load('C:/code/ExamplePhaseRetrieval/data/mexpdata/mdata_ampresult.mat');

thetauncor=matfile.thetauncor;
Ia=thetauncor(end-1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
matfile=load('chiarraybg_phase.mat');
chiarraybg_phase8=matfile.chiarraybg;
matfile=load('C:/code/ExamplePhaseRetrieval/data/expdata/vector_fitter_phase.mat');

thetauncor=matfile.thetauncor_phase;
Ip8=thetauncor(end-1);

matfile=load('chiarraybg_amp.mat');
chiarraybg_amp8=matfile.chiarraybg;
matfile=load('C:/code/ExamplePhaseRetrieval/data/expdata/vector_fitter_amp.mat');

thetauncor=matfile.thetauncor;
Ia8=thetauncor(end-1);


n=20;
exp=31*31*21;
Ialla=zeros(n,1);
Iallp=zeros(n,1);
Ialla8=zeros(n,1);
Iallp8=zeros(n,1);
for i=1:n
    Ialla(i)=Ia*rate^(i);
    Iallp(i)=Ip*rate^(i);
    
    Ialla8(i)=Ia8*rate^(i);
    Iallp8(i)=Ip8*rate^(i);
end
Ialla=flip(Ialla);
Iallp=flip(Iallp);
Ialla8=flip(Ialla8);
Iallp8=flip(Iallp8);

errorbar(Ialla8,squeeze(ones(1,n))*exp,squeeze(ones(1,n))*sqrt(2*exp));
hold on


s=sum(chiarraybg_amp8(1,:,:,:),3);
chimean=(squeeze(mean(s,4)));
chistd=(squeeze(std(s,1,4)));
errorbar(Ialla8,chimean,chistd);
hold on
s=sum(chiarraybg_phase8(1,:,:,:),3);
chimean=(squeeze(mean(s,4)));
chistd=(squeeze(std(s,1,4)));
errorbar(Iallp8,chimean,chistd);


set(gca, 'XScale', 'log')
ylim([0.9*exp,1.5*exp])
legend({'expect','amp10','phase10'})