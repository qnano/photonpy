function [thetastore,mustore,meritstore,Hessianstore,numiters] = localization(allspots,theta0,parameters)
% This function finds the fit parameters for fitting a single 2D-image
% with a fully vectorial PSF model. 
%
% copyright Sjoerd Stallinga, TU Delft, 2017
%

% parameter settings
dims = size(allspots);
Ncfg = parameters.Ncfg;
Mx = dims(1);
My = dims(2);
Mz = parameters.Mz;
numparams = parameters.numparams;
Nitermax = parameters.Nitermax;
varfit = parameters.varfit;
pixelsize = 2*parameters.xrange/parameters.Mx;
optmethod = parameters.optmethod;
tollim = parameters.tollim;
fitmodel = parameters.fitmodel;

roisizex = Mx*pixelsize;
roisizey = My*pixelsize;
zmin = parameters.zspread(1);
zmax = parameters.zspread(2);
deltaz = zmax-zmin;
zmin = zmin-0.5*deltaz;
zmax = zmax+0.5*deltaz;
lambdamin = parameters.lambdaspread(1);
lambdamax = parameters.lambdaspread(2);
deltalambda = lambdamax-lambdamin;
lambdamin = lambdamin-0.5*deltalambda;
lambdamax = lambdamax+0.5*deltalambda;
zernikecoefsmax = 0.25*parameters.lambda*ones(numparams-5,1);

% pre-allocation
mustore = zeros(Mx,My,Mz,Ncfg);
thetastore = zeros(numparams,Ncfg,Nitermax+1);
meritstore = zeros(Ncfg,Nitermax+1);
Hessianstore = zeros(numparams,Ncfg);
numiters = zeros(1,Ncfg);

p = gcp('nocreate');
if isempty(p)
  myparpool = parpool;
end

for jcfg = 1:Ncfg

% for jcfg = 1:Ncfg
  
  if Ncfg>1
    fprintf('fitting random instance # %i...\n',jcfg)
  else
    fprintf('fitting ...\n')
  end
  
  % parameters iteration loop
  monitor = 2*tollim;
  hescoef = 1;
  alamda = 1;
  alamdafac = 5;

  % rename the spot
  if Ncfg>1
    if length(dims)==3
      spotscfg = squeeze(allspots(:,:,jcfg));
    end
    if length(dims)==4
      spotscfg = squeeze(allspots(:,:,:,jcfg));
    end
  end
  if Ncfg==1
    spotscfg = allspots;
  end
  theta0temp = squeeze(theta0(:,jcfg));

  % initial values and max/min values
  x0 = theta0temp(1);
  y0 = theta0temp(2);
  Nph = theta0temp(numparams-1);
  bg = theta0temp(numparams);
  switch fitmodel
    case 'xy'
      thetainit = [x0,y0,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,Nph/10,bg/10];
    case 'xyz'
      z0 = theta0temp(3);
      thetainit = [x0,y0,z0,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,zmax,2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,zmin,Nph/10,bg/10];
    case 'xylambda'
      lambda = theta0temp(3);
      thetainit = [x0,y0,lambda,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,lambdamax,2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,lambdamin,Nph/10,bg/10];
    case 'xyzlambda'
      z0 = theta0temp(3);
      lambda = theta0temp(4);
      thetainit = [x0,y0,z0,lambda,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,zmax,lambdamax,2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,zmin,lambdamin,Nph/10,bg/10];
    case 'aberrations'
      z0 = theta0temp(3);
      zernikecoefs = theta0temp(4:numparams-2)';
      thetainit = [x0,y0,z0,zernikecoefs,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,zmax,zernikecoefsmax',2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,zmin,-zernikecoefsmax',Nph/10,bg/10];
      
    case 'aberrationsamp'
      z0 = theta0temp(3);
      zernikecoefs = theta0temp(4:numparams-2)';
      thetainit = [x0,y0,z0,zernikecoefs,Nph,bg];
      thetamax = [roisizex/2,roisizey/2,zmax,zernikecoefsmax',2*Nph,max(Nph/My/Mx/2,2*bg)];
      thetamin = [-roisizex/2,-roisizey/2,zmin,-zernikecoefsmax',Nph/10,bg/10];
  end

  % start iteration loop
  
% implementation with fmincon
%   options = optimoptions('fmincon','Algorithm','trust-region-reflective',...
%   'GradObj','on','Hessian','user-supplied','Display','off','TolFun',1e-6,'MaxIter',Nitermax);
%   ObjectiveFunction = @(theta)meritfun(theta,spotscfg,parameters);
%   [theta,merit] = fmincon(ObjectiveFunction,thetainit,[],[],[],[],thetamin,thetamax,[],options);
%   merit = -merit;

  iiter = 1;
  theta = thetainit;
  [mu,dmudtheta] = poissonrate(theta,parameters);
  [merit,grad,Hessian] = likelihood(spotscfg,mu,dmudtheta,varfit);
  thetaretry = (thetamax+thetamin)/2;
  meritprev = merit;
  
  thetatemp = zeros(numparams,Nitermax+1);
  merittemp = zeros(1,Nitermax+1);
  thetatemp(:,1) = theta;
  merittemp(1) = merit;
  
  while ((iiter<=Nitermax) && (monitor>tollim))

    switch optmethod
      case 'levenbergmarquardt'     
  % check for det(H)=0 in order to avoid inversion of H
        matty = hescoef*Hessian+alamda*diag(diag(Hessian));
        if (abs(det(matty))>2*eps) 
          thetatry = thetaupdate(theta,thetamax,thetamin,thetaretry,grad,Hessian,hescoef,alamda);
          [mu,dmudtheta] = poissonrate(thetatry,parameters);
          [merittry,gradtry,Hessiantry] = likelihood(spotscfg,mu,dmudtheta,varfit);
          dmerit = merittry-merit;
          if (dmerit<0)
            alamda = alamdafac*alamda;
          else
            alamda = alamda/alamdafac;
            theta = thetatry;
            merit = merittry;
            grad = gradtry;
            Hessian = Hessiantry;
            dmerit = merit-meritprev;
            monitor = abs(dmerit/merit);
            meritprev = merit;
            thetaretry = theta;
          end
        else
          alamda = alamdafac*alamda;
        end  
      case 'newtonraphson'
        hescoef = 0;
        alamda = 1;
        theta = thetaupdate(theta,thetamax,thetamin,thetaretry,grad,Hessian,hescoef,alamda);
        [mu,dmudtheta] = poissonrate(theta,parameters);
        [merit,grad,Hessian] = likelihood(spotscfg,mu,dmudtheta,varfit);
        dmerit = merit-meritprev;
        monitor = abs(dmerit/merit);
        meritprev = merit;
        thetaretry = theta;
    end

    % store values and update counter
    thetatemp(:,iiter+1) = theta;
    merittemp(iiter+1) = merit;
    iiter = iiter+1;
  end
 
% store values
  numiters(jcfg) = iiter-1;
  for jiter = iiter+1:Nitermax+1
    thetatemp(:,jiter) = theta;
    merittemp(jiter) = merit;
  end
  mustore(:,:,:,jcfg) = mu;
  thetastore(:,jcfg,:) = thetatemp;
  meritstore(jcfg,:) = merittemp;
  Hessianstore(:,jcfg) = sqrt(-diag(inv(Hessian)));
  
end

% add offset to merit function
meritoffset = meritoffsetcalc(allspots,parameters.varfit);
for jiter = 1:Nitermax+1
%   meritstore(:,jiter) = meritstore(:,jiter)+meritoffset;
end

% delete(myparpool)
