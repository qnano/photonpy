function thetanew = thetaupdate(thetaold,thetamax,thetamin,thetaretry,grad,Hessian,hescoef,alamda)
% This function calculates the new parameters in each iteration step.
%
% copyright Sjoerd Stallinga, TU Delft, 2017
%

% update of fit parameters via Levenberg-Marquardt (hescoef=1, alamda adapted)
% or Newton-Raphson (hescoef=0,alamda=1):
Bmat = hescoef*Hessian+alamda*diag(diag(Hessian));
dtheta = -Bmat\grad';
thetanew = thetaold+dtheta';

% enforce physical boundaries in parameter space.
for jj=1:length(thetaold)
  if ((thetanew(jj)>thetamax(jj))||(thetanew(jj)<thetamin(jj)))
    thetanew(jj) = thetaretry(jj);
  end
end

end
