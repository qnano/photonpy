# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 14:00:06 2021

@author: jelmer
"""
import numpy as np
import matplotlib.pyplot as plt
from photonpy import Context, Gauss3D_Calibration, Dataset, Estimator, GaussianPSFMethods
from photonpy.simflux import SIMFLUX
import photonpy.cpp.spotdetect as spotdetect
from photonpy.utils import multipart_tiff as read_tiff
from photonpy.utils.caching import  equal_cache_cfg, save_cache_cfg
import photonpy.smlm.process_movie as process_movie
import photonpy.smlm.blinking_spots as blinking_spots
import os
import tqdm

def detect_rois(image_fn, rois_fn, cfg, ignore_cache=False, 
                roi_batch_size=20000, background_img=None):

    imgshape = read_tiff.tiff_get_image_size(image_fn)

    spotDetector = spotdetect.SpotDetector(psfSigma=cfg['spot_detect_sigma'], 
                                           roisize=cfg['roisize'], 
                                           minIntensity=cfg['threshold'])

    if not equal_cache_cfg(rois_fn, cfg, image_fn) or ignore_cache:
        with Context(debugMode=cfg['debugmode']) as ctx:
            calib = process_movie.create_calib_obj(gain=1, offset=0, imgshape=imgshape, ctx=ctx)

            process_movie.detect_spots(spotDetector, calib,
                               read_tiff.tiff_read_file(image_fn, startframe=0, maxframes=0), 
                               cfg['num_tirf_angles'], rois_fn, batch_size = roi_batch_size, ctx=ctx)
        save_cache_cfg(rois_fn, cfg, image_fn)
        
    numrois = int(np.sum([len(ri) for ri,px in process_movie.load_rois_iterator(rois_fn)]))
    print(f"Num ROIs: {numrois}")
    
    if numrois == 0:
        raise ValueError('No spots detected')
        
    return numrois





def fitrois(rois_fn, excitation_pattern, estimator:Estimator, imgshape):
    excpat = excitation_pattern.reshape(excitation_pattern.size)[None,:]

    estim_list = []
    rois_info_list = []
    intensities_list = []
    rois = []
    
    for rois_info, pixels in process_movie.load_rois_iterator(rois_fn):
        estim, intensity_and_crlb, traces = estimator.Estimate(pixels, constants=excpat)
        
        rois_info_list.append(rois_info)
        estim_list.append(estim)
        
        rois.append(pixels)
        
        intensities_list.append(intensity_and_crlb)
            
    estim = np.concatenate(estim_list)
    rois_info = np.concatenate(rois_info_list)
    intensities = np.concatenate(intensities_list)
    
    ds = Dataset(numrois, 3, imgshape)
    ds.pos = estim[:,:3]
    ds.pos[:,0] += rois_info['x']
    ds.pos[:,1] += rois_info['y']
    ds.photons = estim[:,3]
    ds.background = estim[:,4]
    # should compute this with estimator.CRLB().. Needs to be nonzero otherwisee picasso render will not show them
    ds.crlb.pos = 0.1 
    return ds, intensities, np.concatenate(rois)


def view_movie(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)

def simulate(nframes, bg, gt: Dataset, output_fn, 
             psf, excitation_pattern, debugMode=False):
    
    excpat = excitation_pattern.reshape(excitation_pattern.size)[None,:]
    
    """
    Simulate SMLM dataset with excitation patterns. Very basic atm,
    spots are either off or on during the entire series of modulation patterns (6 frames for simflux)
    """
    with read_tiff.MultipartTiffSaver(output_fn) as tif, tqdm.tqdm(total=nframes) as pb:

        sampleframes = psf.sampleshape[0] if len(psf.sampleshape)==3 else 1
        
        for f, spot_ontimes in enumerate(blinking_spots.blinking(len(gt), nframes // sampleframes, subframe_blink=1)):
            
            
            which = np.where(spot_ontimes > 0)[0]
            
            params = np.zeros((len(which), 5),dtype=np.float32)
            params[:,:gt.dims] = gt.pos[which]
            params[:,-2] = gt.photons[which]

            roiposYX = (params[:,[1,0]] - psf.sampleshape[-2:]/2).astype(np.int32)
            params[:,:2] -= roiposYX[:,[1,0]]
            
            roipos = np.zeros((len(which),3),dtype=np.int32)
            roipos[:,1:] = roiposYX
            
            # this way the modulation pattern will automatically be broadcasted 
            # into the required (spots, npatterns, 6) shape
            rois = psf.ExpectedValue(params, roipos=roipos, 
                                     constants=excpat) 
                            
            for i in range(sampleframes):
                img = np.zeros(gt.imgshape, dtype=np.float32)
                ctx.smlm.DrawROIs(img, rois[:,i], roiposYX)
                img += bg
                tif.save(np.random.poisson(img))
                pb.update()
    
def view_movie(mov):
    import napari    
    
    with napari.gui_qt():
        napari.view_image(mov)


with Context(debugMode=False) as ctx:

    roisize = 10
    nframes = 4000
    
    tif_fn = 'tirfscan.tif'

    W = 200
    depth = 0.5
    ds = Dataset(200, 3, (W,W))
    ds.pos = np.random.uniform([0,0,0], [W,W,depth],size=(len(ds),3))
    ds.photons = 1000
    
    excitation_pattern = np.array([[0.5, 1], 
                                    [0.5, 5]]) # [intensity, exponent]

    cfg = {
        'roisize': roisize,
        'num_tirf_angles': len(excitation_pattern),
        'spot_detect_sigma': 2,
        'threshold': 10,
        'debugmode': False
         }
    
    imgshape = (W,W)

    psf_calib = Gauss3D_Calibration()

    estimator = SIMFLUX(ctx).CreateTIRFScanEstimator(psf_calib, 2, roisize=roisize)
    simulate(nframes, 10, ds, output_fn=tif_fn, excitation_pattern =excitation_pattern , psf=estimator)

    rois_fn = os.path.splitext(tif_fn)[0]+"_rois.pickle"
    locs_fn = os.path.splitext(tif_fn)[0]+"_locs.hdf5"
    numrois = detect_rois(tif_fn, rois_fn, cfg)
    print(f"Num rois: {numrois}")
    
    ds, intensities, rois = fitrois(rois_fn, excitation_pattern, estimator, imgshape)
    ds.save(locs_fn)    
    
    view_movie(rois)
    
    #excpat = excitation_pattern.reshape(excitation_pattern.size)[None,:]
    #smp = estimator.GenerateSample([[5,5, 0.2, 1000, 1], [6, 5, 0.2, 2000, 1]], constants=excpat)
    #plt.imshow(np.concatenate(smp[0], -1))

    

