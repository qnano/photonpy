# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 16:17:58 2021

@author: jelmer
"""

import numpy as np

def make_frame_bins(framenum, minBinsize):
    fn_sorted_idx = np.argsort(framenum)
    fn_s = framenum[fn_sorted_idx]
    curbin = []#list[int]()
    bins=[]
    nframes = max(fn_s)+1
    
    i = 0 # i and j index into the fn_s
    for f in range(nframes):
        # find all spots in this frame
        j = i
        while j<len(fn_s) and i<len(fn_s) and fn_s[j] == fn_s[i]:
            curbin.append(fn_sorted_idx[j])
            j += 1
            
        i=j

        if len(curbin) >= minBinsize:
            # advance frame
            bins.append(curbin)
            curbin = []
            
    if len(curbin)>0:
        bins.append(curbin)
        
    if len(bins[-1]) < minBinsize and len(bins)>1: #merge last bins if needed
        last = bins.pop()
        for i in last: bins[-1].append(i)
    return bins
